﻿using Business.Abstract;
using LogAction.Entities;

namespace LogAction.Abstract
{
    public interface ILogRepository : IRepository<Log>
    {
        void Truncate();
	}
}
