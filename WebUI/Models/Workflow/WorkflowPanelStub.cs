﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebUI.Models
{
    public class WorkflowPanelStub
    {
		public List<WorkflowPresentationStub> Workflows { get; set; }

        public WorkflowPanelStub()
        {

		}

		public WorkflowPanelStub(List<Workflow> workflows)
		{
			Workflows = ListMapper.MapList<Workflow, WorkflowPresentationStub>(workflows);
			SetWorkflowId(Workflows.First().Id);
		}

		public WorkflowPanelStub(List<Workflow> workflows, int workflowId)
		{
			Workflows = ListMapper.MapList<Workflow, WorkflowPresentationStub>(workflows);
			SetWorkflowId(workflowId);
		}

		public void SetWorkflowId(int workflowId)
		{
			foreach (WorkflowPresentationStub workflow in Workflows)
			{
				if (workflow.Id == workflowId)
				{
					workflow.Active = true;
				}
				else
				{
					workflow.Active = false;
				}
			}
		}

		public int? GetWorkflowId()
		{
			foreach (WorkflowPresentationStub workflow in Workflows)
			{
				if (workflow.Active == true)
				{
					return workflow.Id;
				}
			}
			return null;
		}
	}
}