﻿using Business.Entities;
using System;

namespace WebUI.Models
{
    public class WorkflowPresentationStub : BasePresentationStub<Workflow, WorkflowPresentationStub>
    {
        public int Id { get; set; }
        public string WorkflowType { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public Nullable<int> PreviousStep { get; set; }
        public string Approver { get; set; }
		public bool IsBastDownloadable { get; set; }
		public bool IsSapDownloadable { get; set; }
		public bool IsBastSignedUploadable { get; set; }
		public bool IsBastSignedDownloadable { get; set; }

		public bool Active { get; set; }

		public bool IsFirst
		{
			get
			{ return (PreviousStep == null); }
		}

		public bool IsLast
		{
			get
			{ return (Approver == null); }
		}

		public bool IsRevisable
		{
			get
			{ return (PreviousStep != null) && (Approver != null); }
		}

		public bool IsApprovable
		{
			get
			{ return (Approver != null); }
		}

		public WorkflowPresentationStub() : base()
        {

        }

        public WorkflowPresentationStub(Workflow dbObject) : base(dbObject)
        {
			if (Code == Common.Enums.Workflow.DRAFT.ToString())
			{
				Active = true;
			}
		}

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
            Active = false;
        }
    }
}