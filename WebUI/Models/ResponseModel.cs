﻿namespace WebUI.Models
{
    /// <summary>
    /// model untuk mengembalikan response ke client side via ajax
    /// </summary>
    public class ResponseModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }

        public ResponseModel() { }

        public ResponseModel(bool success)
        {
            Success = success;
        }

        public void SetFail(string message)
        {
            Success = false;
            Message = message;
        }
    }
}