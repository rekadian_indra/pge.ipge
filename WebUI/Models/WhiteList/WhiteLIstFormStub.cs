﻿using Business.Entities;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class WhitelistFormStub : BaseFormStub<Whitelist, WhitelistFormStub>
    {
        public int WhitelistId { get; set; }

        [DisplayName("Bandara")]
        public int AirportId { get; set; }

        [DisplayName("Tanggal Awal")]
        public DateTime StartDate { get; set; }

        [DisplayName("Tanggal Akhir")]
        public DateTime EndDate { get; set; }

        [DisplayName("Maskapai")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? CompanyId { get; set; }

        [DisplayName("Crew")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string CrewId { get; set; }

        public WhitelistFormStub() : base()
        {
        }

        public WhitelistFormStub(Whitelist dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            CompanyId = dbObject.Crew.Companies.Any() ? dbObject.Crew.Companies.FirstOrDefault().CompanyId : (int?)null;
        }

        public override void MapDbObject(Whitelist dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}