﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace WebUI.Models
{
    public class ProductionPlanFormStub : BaseFormStub<ProductionPlan, ProductionPlanFormStub>
	{
		public int Id { get; set; }
		public int PowerPlantId { get; set; }
		public int Year { get; set; }
		public string UtilType { get; set; }

		public List<ProductionPlanDetailFormStub> ProductionPlanDetails { get; set; }

		public ProductionPlanFormStub() : base()
		{

		}
		public ProductionPlanFormStub(int year) : this()
		{
			Year = year;
		}

		public ProductionPlanFormStub(ProductionUtilization utilType, int year) : this(year)
        {
			UtilType = utilType.ToString();
			ProductionPlanDetails = new List<ProductionPlanDetailFormStub>();

			foreach (Month month in (Month[])Enum.GetValues(typeof(Month)))
			{
				ProductionPlanDetails.Add(new ProductionPlanDetailFormStub(Year, (int)month)
				{
					ProductionPlanId = Id,
				});
			}
		}

		public ProductionPlanFormStub(ProductionPlan dbObject) : base(dbObject)
		{
			if (dbObject.ProductionPlanDetails == null || !dbObject.ProductionPlanDetails.Any(n=>n.ProductionPlanDailies.Any()))
			{
				ProductionPlanDetails = new List<ProductionPlanDetailFormStub>();

				foreach (Month month in (Month[])Enum.GetValues(typeof(Month)))
				{
					ProductionPlanDetails.Add(new ProductionPlanDetailFormStub(Year, (int)month)
					{
						ProductionPlanId = Id,
					});
				}
			}
            else
            {
                ProductionPlanDetails = ListMapper.MapList<ProductionPlanDetail, ProductionPlanDetailFormStub>(dbObject.ProductionPlanDetails);
            }
		}

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
            ProductionPlanDetails = new List<ProductionPlanDetailFormStub>();
        }
    }
}