﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace WebUI.Models
{
    public class ProductionPlanPresentationStub : ProductionPlanFormStub
	{
		public List<ProductionPlanDetailPresentationStub> ProductionPlanDetailsP { get; set; }

		public ProductionPlanPresentationStub() : base()
        {

        }

        public ProductionPlanPresentationStub(ProductionPlan dbObject) : base(dbObject)
        {
			
		}
		
        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}