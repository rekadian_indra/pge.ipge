﻿using Business.Entities;
using Business.Infrastructure;
using System.Collections.Generic;

namespace WebUI.Models
{
    public class DummyPresentationStub
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public DummyPresentationStub()
        {
        }

        public DummyPresentationStub(DummyTable dbItem)
        {
            ObjectMapper.MapObject<DummyTable, DummyPresentationStub>(dbItem, this);
        }

        public static List<DummyPresentationStub> MapList(IEnumerable<DummyTable> dbItems)
        {
            List<DummyPresentationStub> mItems = new List<DummyPresentationStub>();

            foreach (DummyTable m in dbItems)
                mItems.Add(new DummyPresentationStub(m));

            return mItems;
        }
    }
}