﻿using Business.Entities;
using System;
using System.ComponentModel;

namespace WebUI.Models
{
	public class ProductionPlanDailyFormStub : BaseFormStub<ProductionPlanDaily, ProductionPlanDailyFormStub>
	{
		public int Id { get; set; }
		public int ProductionPlanDetailId { get; set; }
		public int Day { get; set; }
		public double Value { get; set; }

		public ProductionPlanDailyFormStub() : base()
		{
			Value = 0;
		}

		public ProductionPlanDailyFormStub(ProductionPlanDaily dbObject) : base(dbObject)
		{

		}

		protected override void Init()
		{
			//TODO: Set the init object here to be called in all constructor
		}
	}
}