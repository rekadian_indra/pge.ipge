﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebUI.Models
{
    public class ExplorationViewStub
    {
        [Display(Name = "Area")]
        public int AreaId { get; set; }
        [Display(Name = "Cluster")]
        public Nullable<int> ClusterId { get; set; }
        [Display(Name = "Well")]
        public Nullable<int> WellId { get; set; }
        public ExplorationViewStub()
        {

        }
    }
}