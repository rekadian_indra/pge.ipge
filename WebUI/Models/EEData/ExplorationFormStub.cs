﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class ExplorationFormStub : BaseFormStub<Exploration, ExplorationFormStub>
    {
        public int Id { get; set; }
        public int ExplorationDetailId { get; set; }
        public int AreaId { get; set; }      
        public string AreaDescription { get; set; }
        public Nullable<int> ClusterId { get; set; }
        public Nullable<int> WellId { get; set; }
        public string ClusterDescription { get; set; }
        public string WellDescription { get; set; }

        [DisplayFormat(DataFormatString = DisplayFormat.JsSqlDateFormat, ApplyFormatInEditMode = true)]
        public DateTime ExplorationDate { get; set; }

        [RegularExpression(@"([0-9,-]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Range(int.MinValue, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double Mku { get; set; }

        [RegularExpression(@"([0-9,-]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Range(int.MinValue, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double Mkt { get; set; }

        [RegularExpression(@"([0-9,-]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Range(int.MinValue, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double Mdpl { get; set; }

        [RegularExpression(@"([0-9,-]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Range(int.MinValue, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double Sds { get; set; }

        [RegularExpression(@"([0-9,-]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Range(int.MinValue, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double Tds { get; set; }

        public ExplorationFormStub() : base()
        {

        }

        public ExplorationFormStub(Exploration dbObject) : base(dbObject)
        {

        }

        public override void MapDbObject(Exploration dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}