﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.EEData
{
    public class DownloadFilterStub
    {
        public int AreaId { get; set; }
        public string AreaDescription { get; set; }
        public int ClusterId { get; set; }
        public string ClusterDescription { get; set; }
        public int WellId { get; set; }
        public string WellDescription { get; set; }
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DownloadFilterStub()
        {
        }
    }
}