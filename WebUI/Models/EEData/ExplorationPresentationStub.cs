﻿using Business.Entities;
using System;

namespace WebUI.Models
{
    public class ExplorationPresentationStub : BasePresentationStub<Exploration, ExplorationPresentationStub>
    {
        public int Id { get; set; }
        public int AreaId { get; set; }
        public Nullable<int> ClusterId { get; set; }
        public Nullable<int> WellId { get; set; }
        public DateTime ExplorationDate { get; set; }

        public ExplorationPresentationStub() : base()
        {
            
        }

        public ExplorationPresentationStub(Exploration dbObject) : base(dbObject)
        {
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}