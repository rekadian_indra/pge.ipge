﻿using Business.Entities;
using System;

namespace WebUI.Models
{
    public class ExplorationDetailPresentationStub : BasePresentationStub<ExplorationDetail, ExplorationDetailPresentationStub>
    {
        public int Id { get; set; }
        public double Mku { get; set; }
        public double Mkt { get; set; }
        public double Mdpl { get; set; }
        public double Sds { get; set; }
        public double Tds { get; set; }


        public ExplorationDetailPresentationStub() : base()
        {

        }

        public ExplorationDetailPresentationStub(ExplorationDetail dbObject) : base(dbObject)
        {
            Mku = Math.Round(dbObject.Mku, 3);
            Mkt = Math.Round(dbObject.Mkt, 3);
            Mdpl = Math.Round(dbObject.Mdpl, 3);
            Sds = Math.Round(dbObject.Sds, 3);
            Tds = Math.Round(dbObject.Tds, 3);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}