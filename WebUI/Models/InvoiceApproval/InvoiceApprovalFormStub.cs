﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace WebUI.Models
{
    public class InvoiceApprovalFormStub : BaseFormStub<InvoiceApproval, InvoiceApprovalFormStub>
	{
		public int Id { get; set; }
		public int InvoiceId { get; set; }
		public int WorkflowId { get; set; }
		public string Notes { get; set; }

		public InvoiceApprovalFormStub() : base()
		{
			
		}

		public InvoiceApprovalFormStub(InvoiceApproval dbObject) : base(dbObject)
        {

        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}