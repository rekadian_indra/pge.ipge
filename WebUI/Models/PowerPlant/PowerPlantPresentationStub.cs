﻿using Business.Entities;
using Common.Enums;
using System;
using System.ComponentModel;

namespace WebUI.Models
{
    public class PowerPlantPresentationStub : BasePresentationStub<Business.Entities.PowerPlant, PowerPlantPresentationStub>
    {
        public int Id { get; set; }
        public Nullable<int> AreaId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ShipToPartyName { get; set; }
        public string Address { get; set; }

        [DisplayName("Sales Group")]
        public string SalesGroup { get; set; }

        [DisplayName("Shipping Point")]
        public string ShippingPoint { get; set; }
        public Nullable<int> SoldToPartyId { get; set; }

        [DisplayName("Sold To Party")]
        public string SoldToParty { get; set; }

        [DisplayName("Material")]
        public string Material { get; set; }
		[DisplayName("Material")]
		public string StrMaterial { get; set; }
		public string SOType { get; set; }
        public string Description { get; set; }
		public string TemplateBastFileName { get; set; }

		public string AgreementName { get; set; }
		public Nullable<System.DateTime> AgreementDate { get; set; }
		public double BasePrice { get; set; }
        

        public PowerPlantPresentationStub() : base()
        {

        }

        public PowerPlantPresentationStub(Business.Entities.PowerPlant dbObject) : base(dbObject)
        {
            SoldToParty = dbObject.SoldToParty.Description;
			StrMaterial = ((Material)Enum.Parse(typeof(Material), dbObject.Material)).ToDescription();
		}

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}