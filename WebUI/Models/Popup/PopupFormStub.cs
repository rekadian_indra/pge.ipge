﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace WebUI.Models
{
	public class PopupFormStub
	{
		public string PopupId { get; set; }
		public string PopupTitle { get; set; }

		public string FormId { get; set; }
		public string FormMethod { get; set; }
		public string FormAction { get; set; }

		public string FormMessage { get; set; }

		public List<FormElement> FormFields { get; set; }
		public List<FormElement> FormButtons { get; set; }

		public class FormElement
		{
			public enum ElementType
			{
				INPUT_HIDDEN,
				INPUT_TEXT,
				TEXTAREA,
				BUTTON,
				SUBMIT,
				A,
			}

			public ElementType Type { get; set; }
			public string Id { get; set; }
			public string Class { get; set; }
			public string Label { get; set; }
			public string Name { get; set; }
			public string Value { get; set; }
			public string Href { get; set; }
			public string Target { get; set; }
			public string OnClick { get; set; }
			public string OnChange { get; set; }
			public bool IsReadOnly { get; set; }
			public bool IsDisabled { get; set; }

			public FormElement()
			{
				Id = "";
				Class = "";
				Label = "";
				Name = "";
				Value = "";
				Href = "";
				Target = "";
				OnClick = "";
				OnChange = "";
				IsReadOnly = false;
				IsDisabled = false;
			}
		}

		public PopupFormStub()
		{
			PopupId = Guid.NewGuid().ToString();
			FormId = Guid.NewGuid().ToString();
			FormMessage = "";
			FormFields = new List<FormElement>();
			FormButtons = new List<FormElement>();
		}
	}
}