﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace WebUI.Models
{
    public class SalesOrderApprovalFormStub : BaseFormStub<SalesOrderApproval, SalesOrderApprovalFormStub>
	{
		public int Id { get; set; }
		public int SalesOrderId { get; set; }
		public int WorkflowId { get; set; }
		public string Notes { get; set; }

		public SalesOrderApprovalFormStub() : base()
		{
			
		}

		public SalesOrderApprovalFormStub(SalesOrderApproval dbObject) : base(dbObject)
        {

        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}