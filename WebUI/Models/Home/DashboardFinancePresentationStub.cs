﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Enums;
using wf =  Common.Enums.Workflow;

namespace WebUI.Models.Home
{
    public class DashboardFinancePresentationStub
    {
        public double? KOB { get; set; }
        public DateTime? KOBDate { get; set; }
        public double? Own { get; set; }
        public DateTime? OwnDate { get; set; }
        public double? Total { get; set; }
        public DateTime TotalDate { get; set; }
        public List<FinanceDashboardChartPresentaionStub> KOBChart { get; set; }
        public List<FinanceDashboardChartPresentaionStub> OwnChart { get; set; }
        public DashboardFinancePresentationStub() { }
        public DashboardFinancePresentationStub(List<Invoice> Invoices, List<Area> areas) {
            Invoices = Invoices.Where(m => m.Workflow.Code == wf.FINISH.ToString())?.ToList();
            KOB = 0;
            Own = 0;
            Total = 0;
            if (Invoices != null && Invoices.Any())
            {
                List<Invoice> dbObjects = new List<Invoice>();
                KOBChart = new List<FinanceDashboardChartPresentaionStub>();
                OwnChart = new List<FinanceDashboardChartPresentaionStub>();
               
                foreach (Area i in areas)
                {
                    FinanceDashboardChartPresentaionStub model = new FinanceDashboardChartPresentaionStub();
                    FinanceDashboardChartPresentaionStub subModel = new FinanceDashboardChartPresentaionStub();
                    dbObjects = Invoices.Where(m => m.SalesOrder.PowerPlant.Area.Id == i.Id && m.SalesOrder.PowerPlant.SOType == SOType.ZTS.ToString()).ToList();
                    if (dbObjects.Any())
                    {
                        model.AreaId = i.Id;
                        model.AreaName = i.Name;
                        var dsd = dbObjects.Select(m => m.SalesOrder.NetValue);
                        model.NetValue = dbObjects.Select(m => m.SalesOrder.NetValue).Sum();
                        model.NetValue = model.NetValue != null ? Math.Round(model.NetValue.Value, 0) : 0;
                        model.LastInvoiceDate = dbObjects.OrderByDescending(m => m.InvoiceAcceptDate).FirstOrDefault().InvoiceAcceptDate;
                        if(i.PowerPlants != null && i.PowerPlants.Any())
                        {
                            model.SubArea = new List<FinanceDashboardChartPresentaionStub>();
                            foreach (Business.Entities.PowerPlant j in i.PowerPlants)
                            {
                                dbObjects = Invoices.Where(m => m.SalesOrder.PowerPlant.Id == j.Id && m.SalesOrder.PowerPlant.SOType == SOType.ZTS.ToString()).ToList();
                                if (dbObjects.Any())
                                {
                                    subModel = new FinanceDashboardChartPresentaionStub();
                                    subModel.AreaId = j.Id;
                                    subModel.AreaName = j.Name;
                                    subModel.NetValue = dbObjects.Select(m => m.SalesOrder.NetValue).Sum();
                                    subModel.NetValue = subModel.NetValue != null ? Math.Round(subModel.NetValue.Value, 0) : 0;
                                    subModel.LastInvoiceDate = dbObjects.OrderByDescending(m => m.InvoiceAcceptDate).FirstOrDefault().InvoiceAcceptDate;
                                    model.SubArea.Add(subModel);
                                }
                            }
                        }

                        KOBChart.Add(model);
                        KOB += model.NetValue;
                    }
                    
                    model = new FinanceDashboardChartPresentaionStub();
                    dbObjects = Invoices.Where(m => m.SalesOrder.PowerPlant.Area.Id == i.Id && m.SalesOrder.PowerPlant.SOType == SOType.ZPGE.ToString()).ToList();
                    if (dbObjects.Any())
                    {
                        model.AreaId = i.Id;
                        model.AreaName = i.Name;
                        model.NetValue = dbObjects.Select(m => m.SalesOrder.NetValue).Sum();
                        model.NetValue = model.NetValue != null ? Math.Round(model.NetValue.Value, 0) : 0;
                        model.LastInvoiceDate = dbObjects.OrderByDescending(m => m.InvoiceAcceptDate).FirstOrDefault().InvoiceAcceptDate;

                        if (i.PowerPlants != null && i.PowerPlants.Any())
                        {
                            model.SubArea = new List<FinanceDashboardChartPresentaionStub>();
                            foreach (Business.Entities.PowerPlant j in i.PowerPlants)
                            {
                                dbObjects = Invoices.Where(m => m.SalesOrder.PowerPlant.Id == j.Id && m.SalesOrder.PowerPlant.SOType == SOType.ZPGE.ToString()).ToList();
                                if (dbObjects.Any())
                                {
                                    subModel = new FinanceDashboardChartPresentaionStub();
                                    subModel.AreaId = j.Id;
                                    subModel.AreaName = j.Name;
                                    subModel.NetValue = dbObjects.Select(m => m.SalesOrder.NetValue).Sum();
                                    subModel.NetValue = subModel.NetValue != null ? Math.Round(subModel.NetValue.Value, 0) : 0;
                                    subModel.LastInvoiceDate = dbObjects.OrderByDescending(m => m.InvoiceAcceptDate).FirstOrDefault().InvoiceAcceptDate;
                                    model.SubArea.Add(subModel);
                                }
                            }
                        }

                        OwnChart.Add(model);
                        Own += model.NetValue;
                    }
                }
                if (KOBChart.Any())
                {
                    KOBDate = KOBChart.OrderByDescending(m => m.LastInvoiceDate).FirstOrDefault().LastInvoiceDate;
                }
                if (OwnChart.Any())
                {
                    OwnDate = OwnChart.OrderByDescending(m => m.LastInvoiceDate).FirstOrDefault().LastInvoiceDate;
                }
                Total = KOB + Own;
            }
        }
    }   
}