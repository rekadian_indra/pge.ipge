﻿using Common.Enums;
using System.Collections.Generic;
using System.Linq;
using WebUI.Infrastructure;
using WebUI.Controllers;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Models.Home
{
    public class FinanceSalesOrderDocumentZPGEStub
	{
		private Business.Entities.Workflow WF1 { get; set; }
		private Business.Entities.Workflow WF2 { get; set; }
		private Business.Entities.Workflow WF3 { get; set; }
		private Business.Entities.Workflow WF4 { get; set; }
		private Business.Entities.Workflow WF5 { get; set; }

		public string NameWF1 { get; set; }
		public string NameWF2 { get; set; }
		public string NameWF3 { get; set; }
		public string NameWF4 { get; set; }
		public string NameWF5 { get; set; }

		public List<Row> Rows { get; set; }

		public class Row
		{
			public int SalesOrderId { get; set; }
			public string Periode { get; set; }
			public string Unit { get; set; }
			public List<string> BastFriendlyName { get; set; }
			public List<string> BastAbsolutePath { get; set; }
			public List<string> BastSignedFriendlyName { get; set; }
			public List<string> BastSignedAbsolutePath { get; set; }
			public bool IsWF1 { get; set; }
			public bool IsWF2 { get; set; }
			public bool IsWF3 { get; set; }
			public bool IsWF4 { get; set; }
			public bool IsWF5 { get; set; }

			public Row()
			{

			}

			public Row(Business.Entities.SalesOrder salesOrder)
			{
				SalesOrderId = salesOrder.Id;
				Periode = salesOrder.Periode?.ToString(DisplayFormat.MonthYearDateFormat);
				Unit = salesOrder.PowerPlant.Name;

				BastFriendlyName = new List<string>();
				BastAbsolutePath = new List<string>();
				foreach (Business.Entities.BastFile bastFile in salesOrder.BastFiles)
				{
					BastFriendlyName.Add(FileManagementController.FriendlyName(bastFile.FilePath));
					BastAbsolutePath.Add(new UrlHelper(HttpContext.Current.Request.RequestContext).Content(bastFile.FilePath));
				}

				BastSignedFriendlyName = new List<string>();
				BastSignedAbsolutePath = new List<string>();
				foreach (Business.Entities.BastFileSigned bastFileSigned in salesOrder.BastFileSigneds)
				{
					BastSignedFriendlyName.Add(FileManagementController.FriendlyName(bastFileSigned.FilePath));
					BastSignedAbsolutePath.Add(new UrlHelper(HttpContext.Current.Request.RequestContext).Content(bastFileSigned.FilePath));
				}
			}
		}

		public FinanceSalesOrderDocumentZPGEStub()
		{
			Rows = new List<Row>();
		}

		public FinanceSalesOrderDocumentZPGEStub(List<Business.Entities.SalesOrder> salesOrders, List<Business.Entities.Workflow> workflows)
		{
			WF5 = workflows?.FirstOrDefault(x => x.Approver == null) ?? null;
			WF4 = workflows?.FirstOrDefault(x => x.Id == WF5.PreviousStep) ?? null;
			WF3 = workflows?.FirstOrDefault(x => x.Id == WF4.PreviousStep) ?? null;
			WF2 = workflows?.FirstOrDefault(x => x.Id == WF3.PreviousStep) ?? null;
			WF1 = workflows?.FirstOrDefault(x => x.Id == WF2.PreviousStep) ?? null;

			NameWF1 = WF1?.Name;
			NameWF2 = WF2?.Name;
			NameWF3 = WF3?.Name;
			NameWF4 = WF4?.Name;
			NameWF5 = WF5?.Name;

			Rows = new List<Row>();

			foreach (Business.Entities.SalesOrder salesOrder in salesOrders)
			{
				Row row = new Row(salesOrder);

				row.IsWF5 = salesOrder.Workflow.Id == WF5?.Id;
				row.IsWF4 = row.IsWF5 || (salesOrder.Workflow.Id == WF4?.Id);
				row.IsWF3 = row.IsWF4 || (salesOrder.Workflow.Id == WF3?.Id);
				row.IsWF2 = row.IsWF3 || (salesOrder.Workflow.Id == WF2?.Id);
				row.IsWF1 = row.IsWF2 || (salesOrder.Workflow.Id == WF1?.Id);

				Rows.Add(row);
			}
		}
	}
}