﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.Home
{
    public class FinanceDocumentStub
	{
		public string SOType { get; set; }
		public string Material { get; set; }

		public List<Business.Entities.Workflow> SalesOrderWorkflows { get; set; }
		public List<Business.Entities.Workflow> InvoiceWorkflows { get; set; }

		public FinanceSalesOrderDocumentZPGEStub FinanceSalesOrderDocumentZPGE { get; set; }
		public FinanceSalesOrderDocumentZTSStub FinanceSalesOrderDocumentZTS { get; set; }
		public FinanceInvoiceDocumentElectricStub FinanceInvoiceDocumentElectric { get; set; }
		public FinanceInvoiceDocumentSteamStub FinanceInvoiceDocumentSteam { get; set; }

		public FinanceDocumentStub()
		{

		}

		public FinanceDocumentStub(Business.Entities.PowerPlant powerPlant)
		{
			SOType = powerPlant.SOType;
			Material = powerPlant.Material;
		}
	}
}