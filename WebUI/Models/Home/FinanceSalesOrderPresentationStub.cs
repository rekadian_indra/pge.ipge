﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Enums;
using Business.Entities.Views;

namespace WebUI.Models.Home
{
    public class FinanceSalesOrderPresentationStub
    {
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public string ShipToPartyName { get; set; }
        public string PowerPlantName { get; set; }
        public Nullable<int> PowerPlantId { get; set; }
        public string Material { get; set; }
        public Nullable<System.DateTime> TaskPeriode { get; set; }
        public string TaskStatus { get; set; }
        public Nullable<double> EnergyNett { get; set; }
        public Nullable<double> EnergyGross { get; set; }
        public Nullable<double> SteamUtilization { get; set; }
        public double NetPrice { get; set; }
        public Nullable<double> NetValue { get; set; }
        public Nullable<System.DateTime> SalesPeriode { get; set; }
        public Nullable<int> InvoiceId { get; set; }
        public string WorkFlowCode { get; set; }
        public string SOType { get; set; }
        public Nullable<double> Quantity { get; set; }
        public double Production { get; set; }
        public double ProductionPercent { get; set; }

        public FinanceSalesOrderPresentationStub() : base()
        {

        }

        public static List<FinanceSalesOrderPresentationStub> MapList(IEnumerable<Area> areas, List<ViewInvoice> dbItems)
        {
            List<FinanceSalesOrderPresentationStub> result = new List<FinanceSalesOrderPresentationStub>();
            foreach (var db in dbItems)
            {
                FinanceSalesOrderPresentationStub model = new FinanceSalesOrderPresentationStub();
                model.AreaId = db.AreaId;
                model.AreaName = db.AreaName;
                model.PowerPlantId = db.PowerPlantId;
                model.PowerPlantName = db.PowerPlantName;
                if (db.WorkFlowCode != null)
                {
                    model.WorkFlowCode = ((Common.Enums.Workflow)Enum.Parse(typeof(Common.Enums.Workflow), db.WorkFlowCode)).ToDescription();
                }               
                model.NetPrice = db.NetPrice.Value;
                if (areas.Count() > 1)
                {
                    model.Production = dbItems.Sum(n => n.NetValue).Value;
                }
                else
                {
                    foreach (var area in areas)
                    {
                        model.Production = dbItems.Where(n => n.AreaId == area.Id).Sum(n => n.NetValue).Value;
                    }
                }
                model.ProductionPercent = (model.Production / dbItems.Sum(n => n.NetValue)).Value * 100;
                model.ProductionPercent = double.IsNaN(model.ProductionPercent) || double.IsInfinity(model.Production) ? 0 : model.ProductionPercent;
                result.Add(model);

            }
            return result;
        }
    }
}