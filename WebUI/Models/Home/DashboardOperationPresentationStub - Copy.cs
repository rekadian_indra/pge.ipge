﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace WebUI.Models
{
    public class YearlyPlanModel {
        public int Year { get; set; }

        public double Value { get; set; }
    }

    public class AreaActualModel
    {
        public string AreaName { get; set; }

        public List<double> ActualValue { get; set; }
    }

    public class DashboardOperationPresentationStub : BasePresentationStub<ProductionPlan, DashboardOperationPresentationStub>
    {
        public int AreaId { get; set; }
        public int Year { get; set; }
        public string Material { get; set; }
        public string SOType { get; set; }

        public RangeData RangeData { get; set; }
        public DateTime LastDateProduction { get; set; }

        public bool HasData { get; set; }

        public bool ChartHasData { get; set; }

        public double PlanElectricBOEPD { get; set; }
        public double ActualElectricBOEPD { get; set; }
        public double PlanElectric { get; set; }
        public double ActualElectric { get; set; }

        //Month To Date
        public double PlanMonthElectric { get; set; }
        public double ActualMonthElectric { get; set; }
        public double PersenMonth { get {
                return ActualMonthElectric / PlanMonthElectric * 100;
            }
        }

        //Year To Date
        public double PlanYearElectric { get; set; }
        public double ActualYearElectric { get; set; }
        public double PersenYear
        {
            get
            {
                return ActualYearElectric / PlanYearElectric * 100;
            }
        }

        //Month To Date Finance
        public double PlanMonthElectricFinance { get; set; }
        public double ActualMonthElectricFinance { get; set; }
        public double PersenMonthFinance
        {
            get
            {
                return ActualMonthElectricFinance / PlanMonthElectricFinance * 100;
            }
        }

        //Year To Date Finance
        public double PlanYearElectricFinance { get; set; }
        public double ActualYearElectricFinance { get; set; }       
        public double PersenYearFinance
        {
            get
            {
                return ActualYearElectricFinance / PlanYearElectricFinance * 100;
            }
        }

        //monthy yearly Chart
        public List<double> JsonMonthlyPlan { get; set; }

        public List<YearlyPlanModel> JsonYearlyPlan { get; set; }

        public List<AreaActualModel> JsonMonthlyActual { get; set; }

        public List<AreaActualModel> JsonYearlyyActual { get; set; }

        public DashboardOperationPresentationStub()
        {

        }

        public void SetPlanYearlyMonthly(IEnumerable<ProductionPlan> model)
        {
            //kamus
            JsonMonthlyPlan = new List<double>();
            YearlyPlanModel yearlyPlanModel;
            double temp;

            foreach (Month val in Enum.GetValues(typeof(Month)))
            {
                temp = 0;
                foreach (ProductionPlan pp in model)
                {
                    temp += pp.ProductionPlanDetails.Where(x => x.Month == (int)val).Sum(x => x.Value);
                }

                JsonMonthlyPlan.Add(temp / 1000);
            }
            
            foreach (ProductionPlan pp in model)
            {
                if(JsonYearlyPlan.Where(x => x.Year == pp.Year).Any())
                {
                    yearlyPlanModel = JsonYearlyPlan.Where(x => x.Year == pp.Year).FirstOrDefault();
                    yearlyPlanModel.Value += pp.ProductionPlanDetails.Sum(x => x.Value) / 1000;
                }
                else
                {
                    yearlyPlanModel = new YearlyPlanModel();
                    yearlyPlanModel.Year = pp.Year;
                    yearlyPlanModel.Value = pp.ProductionPlanDetails.Sum(x => x.Value) / 1000;
                    JsonYearlyPlan.Add(yearlyPlanModel);
                }
            }
        }

        public void SetActualYearlyMonthly(IEnumerable<TaskReport> model, IEnumerable<Area> areas)
        {
            //kamus
            List<double> temp;
            AreaActualModel tempAreaActualModel;

            //iterate area
            foreach(Area a in areas)
            {
                //iterate month
                foreach (Month val in Enum.GetValues(typeof(Month)))
                {
                    temp = new List<double>();
                    tempAreaActualModel = new AreaActualModel();
                    foreach (TaskReport tr in model.Where(x => x.Periode >= new DateTime(DateTime.Now.Year,(int)val,1) && x.Periode < new DateTime(DateTime.Now.Year,((int)val)+1,1)))
                    {
                        if (tr.Pltps.Any())
                        {
                            if(tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id).Any())
                                temp.Add(tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id).Sum(x => x.EnergyNett).Value / 1000000);
                            else
                                temp.Add(0);
                        }
                        else
                            temp.Add(0);
                    }
                    tempAreaActualModel.AreaName = a.Name;
                    tempAreaActualModel.ActualValue = temp;
                    JsonMonthlyActual.Add(tempAreaActualModel);
                }
            }
        }

        protected override void Init()
        {
            Year = DateTime.Now.Year;
            JsonMonthlyPlan = new List<double>();
            JsonYearlyPlan = new List<YearlyPlanModel>();
            JsonMonthlyActual = new List<AreaActualModel>();
            JsonYearlyyActual = new List<AreaActualModel>();
        }
    }
}