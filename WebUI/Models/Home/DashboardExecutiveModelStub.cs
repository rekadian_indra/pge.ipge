﻿using Business.Entities;
using Business.Entities.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.Home
{
    public class DashboardExecutiveModelStub
    {
        public int? AreaId { get; set; }
        public int Month { get; set; }
        public int StartYear { get; set; }
        public int EndYear { get; set; }
        public string RangeData { get; set; }

        public double SumSteamUtilization { get; set; }
        public double SumEnergyNett { get; set; }
        public double SumEnergyGross { get; set; }
        public double SumFinancial { get; set; }


        public double SteamUtilization { get; set; }
        public double EnergyNettValue { get; set; }
        public double EnergyGrossValue { get; set; }

        public double Capacity { get; set; }
        public double Actual { get; set; }
        public double Productivity { get; set; }
        public string StrProductivity { get; set; }

        public string AreaName { get; set; }
        public string PowerPlantName { get; set; }

        public double SumEnergyPlan { get; set; }
        public double SumSteamPlan { get; set; }
        public double SumFinancialPlan { get; set; }
        public double SumFinancialPrognosa { get; set; }
        public double SumFinancialPlanBoepd { get; set; }
        public double SumFinancialActualBoepd { get; set; }
        public double EnergyActualNettBoepd
        {
            get
            {
                return SumEnergyNett * 1.971;
            }
        }
        public double EnergyPlanNettBoepd
        {
            get
            {
                return SumEnergyPlan * 1.971;
            }
        }
        public double EnergyActualPercentBoepd
        {
            get
            {
                return (EnergyActualNettBoepd / EnergyPlanNettBoepd) * 100;
            }
        }
        public double EnergyPlanPercentBoepd
        {
            get
            {
                return 100 - EnergyActualPercentBoepd;
            }
        }
        public List<DashboardExecutiveModelStub> ListAreas { get; set; }
        public double EnergyActual
        {
            get
            {
                double result = 0;
                result = (SumEnergyNett / SumEnergyPlan) * 100;
                result = double.IsInfinity(result) || double.IsNaN(result) ? 0 : result;
                return result;
            }
        }
        public double SteamActual
        {
            get
            {
                double result = 0;
                result = (SumSteamUtilization / SumSteamPlan) * 100;
                result = double.IsInfinity(result) || double.IsNaN(result) ? 0 : result;
                return result;
            }
        }
        public double FinancialActual
        {
            get
            {
                double result = 0;
                result = (SumFinancial / SumFinancialPlan) * 100;
                result = double.IsInfinity(result) || double.IsNaN(result) ? 0 : result;
                return result;
            }
        }

        public bool HasData { get; set; }

        public DashboardExecutiveModelStub()
        {

        }

        public DashboardExecutiveModelStub(List<TaskReport> tasks, List<ProductionPlan> plans, List<ViewPpi> energyPrices, List<SteamPowerPlantPrice> steamPrices, List<ViewCapacityFactor> capacityFactors, DateTime? periode = null)
        {
            //if (dbObjects.Any())
            //{
            //    SumEnergyNett = dbObjects.Sum(m => m.EnergyNett.Value);
            //    SumEnergyGross = dbObjects.Sum(m => m.EnergyGross.Value);
            //    SumSteamUtilization = dbObjects.Sum(n => n.SteamUtilization.Value);
            //    SumFinancial = dbObjects.Sum(n => n.TotalEnergyActual);

            //    EnergyNettValue = dbObjects.Sum(m => m.EnergyNettPriceNett);
            //    EnergyGrossValue = dbObjects.Sum(m => m.EnergyGrossPriceNett);
            //    SteamUtilization = dbObjects.Sum(n => n.SteamUtilization.Value);

            //    SumEnergyPlan = dbObjects.Sum(n => n.AverageEnergyPlan);
            //    SumSteamPlan = dbObjects.Sum(n => n.AverageSteamPlan);
            //    SumFinancialPlan = dbObjects.Sum(n => n.TotalEnergyPlan);

            //    SumFinancialPlanBoepd = SumFinancialPlan * 1.971;
            //    SumFinancialActualBoepd = SumFinancialActualBoepd * 1.971;

            //    IEnumerable<string> areas = dbObjects.Select(n => n.AreaName).Distinct();
            //    ListAreas = new List<DashboardExecutiveModelStub>();
            //    foreach (string area in areas)
            //    {
            //        DashboardExecutiveModelStub temp = new DashboardExecutiveModelStub();
            //        temp.AreaName = area;
            //        temp.SumEnergyNett = dbObjects.Where(n => n.AreaName == area).Sum(n => n.EnergyNett.Value);
            //        temp.SumSteamUtilization = dbObjects.Where(n => n.AreaName == area).Sum(n => n.SteamUtilization.Value);
            //        ListAreas.Add(temp);
            //    }        
            //    HasData = true;
            //}
            var temps = tasks.Where(n => n.Area.PowerPlants.Any(m => m.SOType == Common.Enums.SOType.ZPGE.ToString()));
            if (temps.Any())
            {
                SumEnergyNett = temps.Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString() && m.PowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString()).Sum(m => m.EnergyNett.Value / 1000));
                SumEnergyNett += temps.Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString() && m.PowerPlant.Material == Common.Enums.Material.GEOTHERMAL_STEAM.ToString()).Sum(m => m.EnergyGross.Value / 1000));
                SumEnergyGross = temps.Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString()).Sum(m => m.EnergyGross.Value / 1000));
                SumSteamUtilization = temps.Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString()).Sum(m => m.SteamUtilization.Value));

                if (periode.HasValue)
                {
                    SumEnergyPlan = plans.Where(n => n.UtilType == Common.Enums.ProductionUtilization.ELECTRIC.ToString() && n.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString()).Sum(n => n.ProductionPlanDetails.Where(m => m.Month == periode.Value.Month && m.ProductionPlan.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString()).Sum(m => m.Value));
                    SumSteamPlan = plans.Where(n => n.UtilType == Common.Enums.ProductionUtilization.STEAM.ToString() && n.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString()).Sum(n => n.ProductionPlanDetails.Where(m => m.Month == periode.Value.Month && m.ProductionPlan.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString()).Sum(m => m.Value));
                }
                else
                {
                    SumEnergyPlan = plans.Where(n => n.UtilType == Common.Enums.ProductionUtilization.ELECTRIC.ToString() && n.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString()).Sum(n => n.ProductionPlanDetails.Where(m => m.ProductionPlan.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString()).Sum(m => m.Value));
                    SumSteamPlan = plans.Where(n => n.UtilType == Common.Enums.ProductionUtilization.STEAM.ToString() && n.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString()).Sum(n => n.ProductionPlanDetails.Where(m => m.ProductionPlan.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString()).Sum(m => m.Value));
                }

                HasData = true;
            }

            //pricing actual
            var pltps = temps.SelectMany(n => n.Pltps);
            foreach (var task in pltps)
            {
                double elPrice = 0, stPrice = 0;

                ViewPpi energyPrice = energyPrices.FirstOrDefault(n => n.PowerPlantId == task.PowerPlantId && n.Month == task.TaskReport.Periode.Value.Month && n.Year == task.TaskReport.Periode.Value.Year);
                elPrice = energyPrice != null ? energyPrice.Price * 1000 : 0;
                if (task.PowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString())
                    SumFinancial += task.EnergyNett.Value / 1000 * elPrice;
                else
                    SumFinancial += task.EnergyGross.Value / 1000 * elPrice;

                SteamPowerPlantPrice steamPrice = steamPrices.FirstOrDefault(n => n.PowerPlantId == task.PowerPlantId && n.StartDate.Date <= task.TaskReport.Periode.Value.Date && n.EndDate.Date >= task.TaskReport.Periode.Value.Date);
                stPrice = steamPrice != null ? steamPrice.Price : 0;
                SteamUtilization += task.SteamUtilization.Value * stPrice;
            }

            //pricing plan
            var planDetails = plans.SelectMany(n => n.ProductionPlanDetails);
            planDetails = planDetails.Where(n => n.ProductionPlan.PowerPlant.SOType == Common.Enums.SOType.ZPGE.ToString());
            foreach (var plan in planDetails)
            {
                int quarter = plan.Month <= 3 ? 4 : (plan.Month <= 6 ? 1 : (plan.Month <= 9 ? 2 : 3));
                int year = quarter == 4 ? plan.ProductionPlan.Year - 1 : plan.ProductionPlan.Year;
                double elPrice = 0, stPrice = 0;

                if (plan.ProductionPlan.UtilType == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                {
                    ViewPpi energyPrice = energyPrices.FirstOrDefault(n => n.Month == plan.Month && n.Year == plan.ProductionPlan.Year);
                    elPrice = energyPrice != null ? energyPrice.Price : 0;
                    SumFinancialPlan += plan.Value * elPrice;
                }

                if (plan.ProductionPlan.UtilType == Common.Enums.ProductionUtilization.STEAM.ToString())
                {
                    int planMonth = DateTime.Now.Month;
                    if(plan.Month >= 1 && plan.Month <= 12)
                    {
                        planMonth = plan.Month;
                    }
                    DateTime planDate = new DateTime(plan.ProductionPlan.Year, planMonth, 1);
                    planDate = planDate.AddMonths(1).AddDays(-1);
                    SteamPowerPlantPrice steamPrice = steamPrices.FirstOrDefault(n => n.StartDate.Date <= planDate.Date && n.EndDate.Date >= planDate.Date);
                    stPrice = steamPrice != null ? steamPrice.Price : 0;
                    SumFinancialPlan += plan.Value * stPrice;
                }
            }

            SumFinancialPlanBoepd = SumFinancialPlan * 1.971;
            SumFinancialActualBoepd = SumFinancial * 1.971;

            if (capacityFactors.Any())
            {
                Capacity = capacityFactors.Sum(m => m.Capacity);
                Actual = capacityFactors.Sum(m => m.Actual);
                Productivity = (Actual / Capacity) * 100;
            }
        }

        public DashboardExecutiveModelStub(List<ViewProgressEnergy> dbObjects, List<ViewCapacityFactor> capacityFactors)
        {
            if (dbObjects.Any())
            {
                SumEnergyNett = dbObjects.Sum(m => m.EnergyNett.Value);
                SumEnergyGross = dbObjects.Sum(m => m.EnergyGross.Value);
                SumSteamUtilization = dbObjects.Sum(n => n.SteamUtilization.Value);
                SumFinancial = dbObjects.Sum(n => n.TotalEnergyActual);

                EnergyNettValue = dbObjects.Sum(m => m.EnergyNettPriceNett);
                EnergyGrossValue = dbObjects.Sum(m => m.EnergyGrossPriceNett);
                SteamUtilization = dbObjects.Sum(n => n.SteamUtilization.Value);

                SumEnergyPlan = dbObjects.Sum(n => n.AverageEnergyPlan);
                SumSteamPlan = dbObjects.Sum(n => n.AverageSteamPlan);
                SumFinancialPlan = dbObjects.Sum(n => n.TotalEnergyPlan);

                SumFinancialPlanBoepd = SumFinancialPlan * 1.971;
                SumFinancialActualBoepd = SumFinancialActualBoepd * 1.971;

                IEnumerable<string> areas = dbObjects.Select(n => n.AreaName).Distinct();
                ListAreas = new List<DashboardExecutiveModelStub>();
                foreach (string area in areas)
                {
                    DashboardExecutiveModelStub temp = new DashboardExecutiveModelStub();
                    temp.AreaName = area;
                    temp.SumEnergyNett = dbObjects.Where(n => n.AreaName == area && n.Material == Common.Enums.Material.ELECTRIC_POWER.ToString()).Sum(n => n.EnergyNett.Value);
                    temp.SumEnergyNett += dbObjects.Where(n => n.AreaName == area && n.Material == Common.Enums.Material.GEOTHERMAL_STEAM.ToString()).Sum(n => n.EnergyGross.Value);
                    temp.SumSteamUtilization = dbObjects.Where(n => n.AreaName == area).Sum(n => n.SteamUtilization.Value);
                    ListAreas.Add(temp);
                }


                HasData = true;
            }

            if (capacityFactors.Any())
            {
                Capacity = capacityFactors.Sum(m => m.Capacity);
                Actual = capacityFactors.Sum(m => m.Actual);
                Productivity = (Actual / Capacity) * 100;
            }
        }

        public static List<DashboardExecutiveModelStub> MapList(List<ViewCapacityFactor> dbObjects)
        {
            List<DashboardExecutiveModelStub> result = new List<DashboardExecutiveModelStub>();

            if (dbObjects.Any())
            {
                foreach (ViewCapacityFactor m in dbObjects)
                {
                    double productivity = m.Actual / m.Capacity;

                    if (double.IsInfinity(productivity) || double.IsNaN(productivity))
                    {
                        productivity = 0;
                    }
                    else
                    {
                        productivity = productivity * 100;
                    }
                    DashboardExecutiveModelStub temp = new DashboardExecutiveModelStub
                    {
                        AreaName = m.AreaName,
                        PowerPlantName = m.PowerPlantName,
                        Capacity = m.Capacity,
                        Actual = m.Actual,
                        Productivity = productivity,
                        StrProductivity = $"{productivity.ToString("#,###0.00")}%"
                    };

                    result.Add(temp);
                }
            }

            return result;
        }
    }
}