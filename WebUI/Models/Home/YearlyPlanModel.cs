﻿namespace WebUI.Models
{
    public class YearlyPlanModel
    {
        public int Year { get; set; }

        public double Value { get; set; }
    }
}