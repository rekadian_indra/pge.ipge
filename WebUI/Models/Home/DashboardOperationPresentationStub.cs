﻿using Business.Entities;
using Business.Entities.Views;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using WebUI.Areas.UserManagement.Models;

namespace WebUI.Models
{
    public class DashboardOperationPresentationStub : BasePresentationStub<ProductionPlan, DashboardOperationPresentationStub>
    {
        public int AreaId { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string ProductionUtilization { get; set; }
        public string SOType { get; set; }
        public string ProductionUtilizationChart { get; set; }

        public RangeData RangeData { get; set; }
        public DateTime? LastDateProduction { get; set; }

        public bool HasData { get; set; }

        public bool ChartHasData { get; set; }

        public double PlanElectricBOEPD { get; set; }
        public double ActualElectricBOEPD { get; set; }
        public double PersenBOEPD
        {
            get
            {
                float result = 0;
                result = (float)(ActualElectricBOEPD / PlanElectricBOEPD * 100);
                result = float.IsInfinity(result) || float.IsNaN(result) ? 0 : result;
                return result;
            }
        }
        
        public double PlanElectric { get; set; }
        public double ActualElectric { get; set; }

        //Month To Date
        public double PlanMonthElectric { get; set; }
        public double ActualMonthElectric { get; set; }
        public double PrognosaMonthElectric { get; set; }
        public double PersenMonth
        {
            get
            {
                float result = 0;
                result = (float)(ActualMonthElectric / PlanMonthElectric * 100);
                result = float.IsInfinity(result) || float.IsNaN(result) ? 0 : result;
                return result;
            }
        }

        //Year To Date
        public double PlanYearElectric { get; set; }
        public double ActualYearElectric { get; set; }
        public double PrognosaYearElectric { get; set; }
        public double PersenYear
        {
            get
            {
                float result = 0;
                result = (float)(ActualYearElectric / PlanYearElectric * 100);
                result = float.IsInfinity(result) || float.IsNaN(result) ? 0 : result;
                return result;
            }
        }

        //Month To Date Finance
        public double PlanMonthElectricFinance { get; set; }
        public double ActualMonthElectricFinance { get; set; }
        public double PrognosaMonthElectricFinance { get; set; }
        public double PersenMonthFinance
        {
            get
            {
                float result = 0;
                result = (float)(ActualMonthElectricFinance / PlanMonthElectricFinance * 100);
                result = float.IsInfinity(result) || float.IsNaN(result) ? 0 : result;
                return result;
            }
        }

        //Year To Date Finance
        public double PlanYearElectricFinance { get; set; }
        public double ActualYearElectricFinance { get; set; }
        public double PrognosaYearElectricFinance { get; set; }
        public double PersenYearFinance
        {
            get
            {
                float result = 0;
                result = (float)(ActualYearElectricFinance / PlanYearElectricFinance * 100);
                result = float.IsInfinity(result) || float.IsNaN(result) ? 0 : result;
                return result;
            }
        }

        //monthy yearly Chart
        public List<double> JsonDailyPlan { get; set; }
        public List<double> JsonMonthlyPlan { get; set; }
        public List<YearlyPlanModel> JsonYearlyPlan { get; set; }
        public List<AreaActualModel> JsonDailyActual { get; set; }
        public List<AreaActualModel> JsonMonthlyActual { get; set; }
        public List<AreaActualModel> JsonYearlyActual { get; set; }

        public List<AreaActualModel> JsonDailyActualPO { get; set; }
        public List<AreaActualModel> JsonMonthlyActualPO { get; set; }
        public List<AreaActualModel> JsonYearlyActualPO { get; set; }
        public List<int> ListYear { get; set; }
        public List<string> Categories { get; set; }

        public bool IsFirstTimeLoad { get; set; }

        public int YearChart { get; set; }
        public int MonthChart { get; set; }

        public string LastDateProductionString { get; set; }

        public string JsonChart { get; set; }

        public DashboardOperationPresentationStub()
        {

        }

        public void SetChartPlan(IEnumerable<ProductionPlan> plans, IEnumerable<ProductionPlanDetail> planDetails, IEnumerable<TaskReport> progresses, DateTime? periode = null)
        {
            double temp = 0;
            int totalDays = 0;
            double data = 0;
            ListYear = new List<int> { DateTime.Now.Year };
            JsonDailyPlan = JsonMonthlyPlan = new List<double>();
            JsonYearlyPlan = new List<YearlyPlanModel>();

            YearChart = YearChart == 0 ? DateTime.Now.Year : YearChart;
            MonthChart = MonthChart == 0 ? DateTime.Now.Month : MonthChart;

            totalDays = DateTime.DaysInMonth(YearChart, MonthChart);

            if (plans.Any() && progresses.Any())
            {
                //yearly
                ListYear = plans.Select(n => n.Year).Distinct().OrderBy(n => n).ToList();

                if (RangeData == RangeData.YEARLY)
                {
                    data = 0;
                    foreach (int y in ListYear)
                    {
                        temp = 0;
                        temp = plans.Where(n => n.Year == y).Sum(n => n.ProductionPlanDetails.Sum(m => m.Value));
                        temp = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ? temp / 1000 : temp;
                        //data += temp;
                        YearlyPlanModel yearlyPlanModel = new YearlyPlanModel
                        {
                            Year = y,
                            Value = temp
                        };
                        JsonYearlyPlan.Add(yearlyPlanModel);
                    }
                }

                //monthly
                else if (RangeData == RangeData.MONTHLY)
                {
                    data = 0;
                    foreach (Month val in Enum.GetValues(typeof(Month)))
                    {
                        temp = 0;
                        temp = planDetails.Where(n => n.ProductionPlan.Year == periode.Value.Year && n.Month == (int)val).Sum(n => n.Value);

                        temp = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ? temp / 1000 : temp;
                        //data += temp;

                        JsonMonthlyPlan.Add(temp);
                    }
                }

                //daily
                else
                {
                    data = 0;
                    for (int i = 1; i <= totalDays; i++)
                    {
                        temp = 0;
                        if (planDetails.Any(n => n.ProductionPlan.Year == periode.Value.Year && n.Month == periode.Value.Month && n.ProductionPlanDailies.Any(m => m.Day == i)))
                        {
                            temp = planDetails.Where(n => n.ProductionPlan.Year == periode.Value.Year && n.Month == periode.Value.Month).Sum(n => n.ProductionPlanDailies.Where(m => m.Day == i).Sum(m => m.Value));
                            //temp += planDetails.Where(n => !n.ProductionPlanDailies.Any()).Sum(n => n.Value / totalDays);

                            temp = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ? temp / 1000 : temp;
                            //data += temp;
                        }
                        else
                        {
                            temp = planDetails.Where(n => n.ProductionPlan.Year == periode.Value.Year && n.Month == periode.Value.Month).Sum(n => n.Value / totalDays);
                            temp = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ? temp / 1000 : temp;
                        }

                        JsonDailyPlan.Add(temp);
                        Categories.Add(i.ToString());

                    }

                }
            }
        }
        public void SetChartActual(IEnumerable<Area> areas, IEnumerable<ProductionPlan> plans, IEnumerable<ProductionPlanDetail> planDetails, IEnumerable<TaskReport> progresses)
        {
            List<double> temp, tempPO;
            double value;
            int totalDays = 0;
            JsonDailyActual = JsonMonthlyActual = JsonYearlyActual = new List<AreaActualModel>();
            JsonDailyActualPO = JsonMonthlyActualPO = JsonYearlyActualPO = new List<AreaActualModel>();

            YearChart = YearChart == 0 ? DateTime.Now.Year : YearChart;
            MonthChart = MonthChart == 0 ? DateTime.Now.Month : MonthChart;

            totalDays = DateTime.DaysInMonth(YearChart, MonthChart);
            var powerplants = areas.SelectMany(n => n.PowerPlants.Where(m=>m.IsDeleted == false));
            bool isArea = areas.Select(n => n.Id).Distinct().Count() == 1;

            foreach (Area a in areas)
            {
                if (plans.Any() && progresses.Any())
                {
                    if (isArea == false)
                    {
                        temp = new List<double>();
                        tempPO = new List<double>();

                        //yearly
                        if (RangeData == RangeData.YEARLY)
                        {

                            foreach (int y in ListYear)
                            {
                                value = 0;
                                if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                {

                                    value = progresses.Where(n => n.Periode.Value.Year == y && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.ELECTRIC_POWER.ToString()).Sum(m => m.EnergyNett.Value)) / 1000000;
                                    value += progresses.Where(n => n.Periode.Value.Year == y && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString()).Sum(m => m.EnergyGross.Value)) / 1000000;
                                }
                                else
                                {
                                    value = progresses.Where(n => n.Periode.Value.Year == y && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false).Sum(m => m.SteamUtilization.Value));
                                }

                                temp.Add(value);
                            }
                            JsonYearlyActual.Add(new AreaActualModel { AreaName = a.Name, ActualValue = temp });
                            JsonYearlyActualPO.Add(new AreaActualModel { AreaName = a.Name, ActualValue = temp });
                        }

                        //monthly
                        else if (RangeData == RangeData.MONTHLY)
                        {
                            foreach (Month val in Enum.GetValues(typeof(Month)))
                            {
                                int year = (int)val < 12 ? DateTime.Now.Year : DateTime.Now.AddYears(1).Year;
                                int month = (int)val < 12 ? (int)val + 1 : 1;

                                value = 0;
                                if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                {
                                    value = progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.ELECTRIC_POWER.ToString()).Sum(m => m.EnergyNett.Value)) / 1000000;
                                    value += progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString()).Sum(m => m.EnergyGross.Value)) / 1000000;
                                }
                                else
                                {
                                    value = progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false).Sum(m => m.SteamUtilization.Value));
                                }
                                temp.Add(value);

                                value = 0;
                                if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                {
                                    value = progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.ELECTRIC_POWER.ToString()).Sum(m => m.EnergyNett.Value)) / 1000000;
                                    value += progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString()).Sum(m => m.EnergyGross.Value)) / 1000000;
                                }
                                else
                                {
                                    value = progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false).Sum(m => m.SteamUtilization.Value));
                                }
                                tempPO.Add(value);
                            }
                            JsonMonthlyActual.Add(new AreaActualModel { AreaName = a.Name, ActualValue = temp });
                            JsonMonthlyActualPO.Add(new AreaActualModel { AreaName = a.Name, ActualValue = tempPO });
                        }


                        //daily
                        else
                        {
                            for (int i = 1; i <= totalDays; i++)
                            {         
                                value = 0;
                                if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                {
                                    value = progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.ELECTRIC_POWER.ToString()).Sum(m => m.EnergyNett.Value)) / 1000000;
                                    value += progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString()).Sum(m => m.EnergyGross.Value)) / 1000000;
                                }
                                else
                                {
                                    value = progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false).Sum(m => m.SteamUtilization.Value));
                                }
                                temp.Add(value);

                                value = 0;
                                if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                {
                                    value = progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.ELECTRIC_POWER.ToString()).Sum(m => m.EnergyNett.Value)) / 1000000;
                                    value += progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString()).Sum(m => m.EnergyNett.Value)) / 1000000;
                                }
                                else
                                {
                                    value = progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false).Sum(m => m.SteamUtilization.Value));
                                }
                                tempPO.Add(value);
                            }

                            JsonDailyActual.Add(new AreaActualModel { AreaName = a.Name, ActualValue = temp });
                            JsonDailyActualPO.Add(new AreaActualModel { AreaName = a.Name, ActualValue = tempPO });
                        }
                    }
                    else
                    {
                        foreach (var pl in powerplants)
                        {
                            temp = new List<double>();
                            tempPO = new List<double>();

                            //yearly
                            if (RangeData == RangeData.YEARLY)
                            {

                                foreach (int y in ListYear)
                                {
                                    value = 0;
                                    if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                    {

                                        value = progresses.Where(n => n.Periode.Value.Year == y && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlantId == pl.Id && m.PowerPlant.Material == Material.ELECTRIC_POWER.ToString()).Sum(m => m.EnergyNett.Value)) / 1000000;
                                        value += progresses.Where(n => n.Periode.Value.Year == y && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlantId == pl.Id && m.PowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString()).Sum(m => m.EnergyGross.Value)) / 1000000;
                                    }
                                    else
                                    {
                                        value = progresses.Where(n => n.Periode.Value.Year == y && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlantId == pl.Id).Sum(m => m.SteamUtilization.Value));
                                    }

                                    temp.Add(value);
                                }
                                JsonYearlyActual.Add(new AreaActualModel { AreaName = pl.Name, ActualValue = temp });
                            }

                            //monthly
                            else if (RangeData == RangeData.MONTHLY)
                            {
                                foreach (Month val in Enum.GetValues(typeof(Month)))
                                {
                                    int year = (int)val < 12 ? DateTime.Now.Year : DateTime.Now.AddYears(1).Year;
                                    int month = (int)val < 12 ? (int)val + 1 : 1;

                                    value = 0;
                                    if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                    {
                                        value = progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlantId == pl.Id && m.PowerPlant.Material == Material.ELECTRIC_POWER.ToString()).Sum(m => m.EnergyNett.Value)) / 1000000;
                                        value += progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlantId == pl.Id && m.PowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString()).Sum(m => m.EnergyGross.Value)) / 1000000;
                                    }
                                    else
                                    {
                                        value = progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlantId == pl.Id).Sum(m => m.SteamUtilization.Value));
                                    }
                                    temp.Add(value);

                                    value = 0;
                                    if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                    {
                                        value = progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlantId == pl.Id && m.PowerPlant.Material == Material.ELECTRIC_POWER.ToString()).Sum(m => m.EnergyNett.Value)) / 1000000;
                                        value += progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlantId == pl.Id && m.PowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString()).Sum(m => m.EnergyGross.Value)) / 1000000;
                                    }
                                    else
                                    {
                                        value = progresses.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlantId == pl.Id).Sum(m => m.SteamUtilization.Value));
                                    }
                                    tempPO.Add(value);

                                }
                                JsonMonthlyActual.Add(new AreaActualModel { AreaName = pl.Name, ActualValue = temp });
                            }


                            //daily
                            else
                            {
                                for (int i = 1; i <= totalDays; i++)
                                {
                                    value = 0;
                                    if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                    {
                                        value = progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.ELECTRIC_POWER.ToString() && m.PowerPlantId == pl.Id).Sum(m => m.EnergyNett.Value)) / 1000000;
                                        value += progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString() && m.PowerPlantId == pl.Id).Sum(m => m.EnergyGross.Value)) / 1000000;
                                    }
                                    else
                                    {
                                        value = progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlantId == pl.Id).Sum(m => m.SteamUtilization.Value));
                                    }
                                    temp.Add(value);

                                    value = 0;
                                    if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                    {
                                        value = progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.ELECTRIC_POWER.ToString() && m.PowerPlantId == pl.Id).Sum(m => m.EnergyNett.Value)) / 1000000;
                                        value += progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString() && m.PowerPlantId == pl.Id).Sum(m => m.EnergyNett.Value)) / 1000000;
                                    }
                                    else
                                    {
                                        value = progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false && m.PowerPlantId == pl.Id).Sum(m => m.SteamUtilization.Value));
                                    }
                                    tempPO.Add(value);
                                }

                                JsonDailyActual.Add(new AreaActualModel { AreaName = pl.Name, ActualValue = temp });
                            }
                        }
                    }
                }                
            }
        }

        public void SetChartPlan(IEnumerable<ProductionPlan> plans, IEnumerable<ProductionPlanDetail> planDetails, IEnumerable<ViewProgressEnergy> progresses)
        {
            double temp = 0;
            int totalDays = 0;
            ListYear = new List<int> { DateTime.Now.Year };
            JsonDailyPlan = JsonMonthlyPlan = new List<double>();
            JsonYearlyPlan = new List<YearlyPlanModel>();

            YearChart = YearChart == 0 ? DateTime.Now.Year : YearChart;
            MonthChart = MonthChart == 0 ? DateTime.Now.Month : MonthChart;

            totalDays = DateTime.DaysInMonth(YearChart, MonthChart);

            if (plans.Any() && progresses.Any())
            {
                //yearly
                ListYear = plans.Select(n => n.Year).Distinct().OrderBy(n => n).ToList();

                if (RangeData == RangeData.YEARLY)
                {
                    foreach (int y in ListYear)
                    {
                        temp = plans.Where(n => n.Year == y).Sum(n => n.ProductionPlanDetails.Sum(m => m.Value));
                        temp = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ? temp / 1000 : temp;
                        YearlyPlanModel yearlyPlanModel = new YearlyPlanModel
                        {
                            Year = y,
                            Value = temp
                        };
                        JsonYearlyPlan.Add(yearlyPlanModel);
                    }
                }

                //monthly
                else if (RangeData == RangeData.MONTHLY)
                {

                    foreach (Month val in Enum.GetValues(typeof(Month)))
                    {
                        temp = 0;
                        temp = planDetails.Where(n => n.Month == (int)val).Sum(n => n.Value);

                        temp = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ? temp / 1000 : temp;
                        JsonMonthlyPlan.Add(temp);
                    }
                }

                //daily
                else
                {
                    for (int i = 1; i <= totalDays; i++)
                    {
                        temp = 0;
                        temp += planDetails.Sum(n => n.ProductionPlanDailies.Where(m => m.Day == i).Sum(m => m.Value));
                        //temp += planDetails.Where(n => !n.ProductionPlanDailies.Any()).Sum(n => n.Value / totalDays);

                        temp = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ? temp / 1000 : temp;

                        JsonDailyPlan.Add(temp);
                        Categories.Add(i.ToString());

                    }

                }
            }
        }

        public void SetChartActual(IEnumerable<Area> areas, IEnumerable<ProductionPlan> plans, IEnumerable<ProductionPlanDetail> planDetails, IEnumerable<ViewProgressEnergy> progresses, IEnumerable<TaskReport> tasks)
        {
            List<double> temp, tempPO;
            double value;
            int totalDays = 0;
            JsonDailyActual = JsonMonthlyActual = JsonYearlyActual = new List<AreaActualModel>();
            JsonDailyActualPO = JsonMonthlyActualPO = JsonYearlyActualPO = new List<AreaActualModel>();

            YearChart = YearChart == 0 ? DateTime.Now.Year : YearChart;
            MonthChart = MonthChart == 0 ? DateTime.Now.Month : MonthChart;

            totalDays = DateTime.DaysInMonth(YearChart, MonthChart);

            foreach (Area a in areas)
            {

                temp = new List<double>();
                tempPO = new List<double>();
                if (plans.Any() && progresses.Any())
                {
                    //yearly
                    if (RangeData == RangeData.YEARLY)
                    {

                        foreach (int y in ListYear)
                        {
                            value = 0;
                            value = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ?
                                        progresses.Where(n => n.Year == y && n.AreaId == a.Id).Sum(n => n.EnergyNett.Value) / 1000 :
                                        progresses.Where(n => n.Year == y && n.AreaId == a.Id).Sum(n => n.SteamUtilization.Value);
                            temp.Add(value);

                            value = 0;
                            value = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ?
                                        tasks.Where(n => n.Periode.Value.Year == y && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false).Sum(m => m.EnergyNett.Value)) / 1000000 :
                                        tasks.Where(n => n.Periode.Value.Year == y && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false).Sum(m => m.SteamUtilization.Value));
                            tempPO.Add(value);
                        }
                        JsonYearlyActual.Add(new AreaActualModel { AreaName = a.Name, ActualValue = temp });
                        JsonYearlyActualPO.Add(new AreaActualModel { AreaName = a.Name, ActualValue = tempPO });
                    }

                    //monthly
                    else if (RangeData == RangeData.MONTHLY)
                    {
                        foreach (Month val in Enum.GetValues(typeof(Month)))
                        {
                            value = 0;
                            int year = (int)val < 12 ? DateTime.Now.Year : DateTime.Now.AddYears(1).Year;
                            int month = (int)val < 12 ? (int)val + 1 : 1;

                            value = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ?
                                        progresses.Where(n => n.Month == (int)val && n.AreaId == a.Id).Sum(n => n.EnergyNett.Value) / 1000 :
                                        progresses.Where(n => n.Month == (int)val && n.AreaId == a.Id).Sum(n => n.SteamUtilization.Value);
                            temp.Add(value);

                            value = 0;
                            value = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ?
                                        tasks.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false).Sum(m => m.EnergyNett.Value)) / 1000000 :
                                        tasks.Where(n => n.Periode.Value.Month == (int)val && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false).Sum(m => m.SteamUtilization.Value));
                            tempPO.Add(value);

                        }
                        JsonMonthlyActual.Add(new AreaActualModel { AreaName = a.Name, ActualValue = temp });
                        JsonMonthlyActualPO.Add(new AreaActualModel { AreaName = a.Name, ActualValue = tempPO });
                    }


                    //daily
                    else
                    {
                        for (int i = 1; i <= totalDays; i++)
                        {
                            value = 0;
                            value = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ?
                                        progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.EnergyNett.Value) / 1000 :
                                        progresses.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.SteamUtilization.Value);
                            temp.Add(value);

                            value = 0;
                            value = ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString() ?
                                        tasks.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false).Sum(m => m.EnergyNett.Value)) / 1000000 :
                                        tasks.Where(n => n.Periode.Value.Day == i && n.AreaId == a.Id).Sum(n => n.Pltps.Where(m => m.IsDeleted == false).Sum(m => m.SteamUtilization.Value));
                            tempPO.Add(value);
                        }

                        JsonDailyActual.Add(new AreaActualModel { AreaName = a.Name, ActualValue = temp });
                        JsonDailyActualPO.Add(new AreaActualModel { AreaName = a.Name, ActualValue = tempPO });
                    }
                }
            }
        }


        public void SetPlanYearlyMonthly(IEnumerable<ProductionPlan> model, IEnumerable<ProductionPlan> ppEd)
        {
            //kamus
            JsonMonthlyPlan = new List<double>();
            JsonDailyPlan = new List<double>();
            YearlyPlanModel yearlyPlanModel;
            double temp;
            ListYear = new List<int>();


            //yearly plan
            if (model.Any())
            {
                ListYear = model.Select(m => m.Year).Distinct().OrderByDescending(m => m).ToList();
                foreach (int y in ListYear)
                {

                    foreach (ProductionPlan pp in model.Where(m => m.Year == y))
                    {
                        if (JsonYearlyPlan.Where(x => x.Year == pp.Year).Any())
                        {
                            yearlyPlanModel = JsonYearlyPlan.Where(x => x.Year == pp.Year).FirstOrDefault();
                            if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                yearlyPlanModel.Value += pp.ProductionPlanDetails.Sum(x => x.Value) / 1000;
                            else
                                yearlyPlanModel.Value += pp.ProductionPlanDetails.Sum(x => x.Value);
                        }
                        else
                        {
                            yearlyPlanModel = new YearlyPlanModel();
                            yearlyPlanModel.Year = pp.Year;
                            if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                yearlyPlanModel.Value = pp.ProductionPlanDetails.Sum(x => x.Value) / 1000;
                            else
                                yearlyPlanModel.Value = pp.ProductionPlanDetails.Sum(x => x.Value);
                            JsonYearlyPlan.Add(yearlyPlanModel);
                        }
                    }
                }
            }
            else
            {
                ListYear = new List<int> { DateTime.Now.Year };
            }

            //monthly plan
            model = model.Where(m => m.Year == (int)DateTime.Now.Year);
            foreach (Month val in Enum.GetValues(typeof(Month)))
            {
                temp = 0;
                if (model.Any())
                {
                    foreach (ProductionPlan pp in model)
                    {
                        temp += pp.ProductionPlanDetails.Where(x => x.Month == (int)val).Sum(x => x.Value);
                    }
                }
                else
                {
                    temp = 0;
                }
                if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                    temp = temp / 1000;

                JsonMonthlyPlan.Add(temp);
            }

            //daily plan 
            temp = 0;
            if (model.Any())
            {
                double value = 0;
                double ed = 0;
                foreach (ProductionPlan pp in model)
                {
                    value = pp.ProductionPlanDetails.Where(x => x.Month == (int)DateTime.Now.Month).Sum(x => x.Value);
                    var edList = ppEd.Where(p => p.PowerPlantId == pp.PowerPlantId);
                    foreach (var item in edList)
                    {
                        ed = item.ProductionPlanDetails.Where(x => x.Month == (int)DateTime.Now.Month).Sum(x => x.Value);
                    }
                    if (ed != 0)
                    {
                        temp += value / ed;
                    }
                    else
                    {
                        temp += value;
                    }

                }
            }
            else
            {
                temp = 0;
            }
            if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                temp = temp / 1000;

            int totalDays = DateTime.DaysInMonth((int)DateTime.Now.Year, (int)DateTime.Now.Month);
            for (int i = 1; i <= totalDays; i++)
            {
                JsonDailyPlan.Add(temp);
            }
        }

        public void SetActualYearlyMonthly(IEnumerable<TaskReport> model, IEnumerable<Area> areas, int monthParam, int yearParam)
        {
            //kamus
            List<double> temp;
            AreaActualModel tempAreaActualModel;
            int totalDays = DateTime.DaysInMonth(yearParam, monthParam);

            if (string.IsNullOrEmpty(ProductionUtilizationChart))
                ProductionUtilizationChart = Common.Enums.ProductionUtilization.ELECTRIC.ToString();

            //iterate area
            foreach (Area a in areas)
            {
                //iterate month
                temp = new List<double>();
                tempAreaActualModel = new AreaActualModel();
                foreach (int y in ListYear)
                {
                    double value = 0;
                    foreach (TaskReport tr in model.Where(x => x.Periode != null && x.Periode.Value.Year == y))
                    {
                        if (tr.Pltps.Any())
                        {
                            if (tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Any() && ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                value += tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Sum(x => x.EnergyNett).Value / 1000000;
                            else if (tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Any() && ProductionUtilizationChart == Common.Enums.ProductionUtilization.STEAM.ToString())
                                value += tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Sum(x => x.SteamUtilization).Value;
                        }
                    }
                    temp.Add(value);
                }
                tempAreaActualModel.AreaName = a.Name;
                tempAreaActualModel.ActualValue = temp;
                JsonYearlyActual.Add(tempAreaActualModel);

                temp = new List<double>();
                tempAreaActualModel = new AreaActualModel();
                foreach (Month val in Enum.GetValues(typeof(Month)))
                {
                    double value = 0;
                    int year = (int)val < 12 ? DateTime.Now.Year : DateTime.Now.AddYears(1).Year;
                    int month = (int)val < 12 ? (int)val + 1 : 1;
                    foreach (TaskReport tr in model.Where(x => x.Periode >= new DateTime(DateTime.Now.Year, (int)val, 1) && x.Periode < new DateTime(year, month, 1)))
                    {
                        if (tr.Pltps.Any())
                        {
                            if (tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Any() && ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                value += tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Sum(x => x.EnergyNett).Value / 1000000;
                            else if (tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Any() && ProductionUtilizationChart == Common.Enums.ProductionUtilization.STEAM.ToString())
                                value += tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Sum(x => x.SteamUtilization).Value;
                        }
                    }
                    temp.Add(value);
                }
                tempAreaActualModel.AreaName = a.Name;
                tempAreaActualModel.ActualValue = temp;
                JsonMonthlyActual.Add(tempAreaActualModel);

                temp = new List<double>();
                tempAreaActualModel = new AreaActualModel();
                for (int i = 1; i <= totalDays; i++)
                {
                    double value = 0;
                    foreach (TaskReport tr in model.Where(x => x.Periode == new DateTime(DateTime.Now.Year, (int)DateTime.Now.Month, i)))
                    {
                        if (tr.Pltps.Any())
                        {
                            if (tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Any() && ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                value += tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Sum(x => x.EnergyNett).Value / 1000000;
                            else if (tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Any() && ProductionUtilizationChart == Common.Enums.ProductionUtilization.STEAM.ToString())
                                value += tr.Pltps.Where(x => x.PowerPlant.AreaId == a.Id && x.IsDeleted == false).Sum(x => x.SteamUtilization).Value;
                        }
                    }
                    temp.Add(value);
                }
                tempAreaActualModel.AreaName = a.Name;
                tempAreaActualModel.ActualValue = temp;
                JsonDailyActual.Add(tempAreaActualModel);
            }

            for (int i = 1; i <= totalDays; i++)
            {
                Categories.Add(i.ToString());
            }
        }

        public void SetPlanYearlyMonthly(IEnumerable<ViewProgressEnergy> model, IEnumerable<ProductionPlan> ppEd)
        {
            //kamus
            JsonMonthlyPlan = new List<double>();
            YearlyPlanModel yearlyPlanModel;
            double temp;
            ListYear = new List<int>();

            if (model.Any())
            {
                ListYear = model.Select(m => m.Year.Value).Distinct().OrderByDescending(m => m).ToList();
                foreach (int y in ListYear)
                {
                    foreach (ViewProgressEnergy pp in model.Where(m => m.Year == y))
                    {
                        if (JsonYearlyPlan.Where(x => x.Year == pp.Year).Any())
                        {
                            yearlyPlanModel = JsonYearlyPlan.Where(x => x.Year == pp.Year).FirstOrDefault();
                            if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                yearlyPlanModel.Value += (pp.AverageEnergyPlan / 1000);
                            else
                                yearlyPlanModel.Value += pp.AverageSteamPlan;
                        }
                        else
                        {
                            yearlyPlanModel = new YearlyPlanModel();
                            yearlyPlanModel.Year = pp.Year.Value;
                            if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                                yearlyPlanModel.Value += (pp.AverageEnergyPlan / 1000);
                            else
                                yearlyPlanModel.Value += pp.AverageSteamPlan;
                            JsonYearlyPlan.Add(yearlyPlanModel);
                        }
                    }
                }
            }
            else
            {
                ListYear = new List<int> { DateTime.Now.Year };
            }


            model = model.Where(m => m.Year == (int)DateTime.Now.Year);
            foreach (Month val in Enum.GetValues(typeof(Month)))
            {
                temp = 0;
                if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                    temp = model.Where(n => n.Periode.Value.Month == (int)val).Sum(n => n.AverageEnergyPlan) / 1000;
                else
                    temp = model.Where(n => n.Periode.Value.Month == (int)val).Sum(n => n.AverageSteamPlan);

                JsonMonthlyPlan.Add(temp);
            }

        }

        public void SetActualYearlyMonthly(IEnumerable<ViewProgressEnergy> model, IEnumerable<Area> areas)
        {
            //kamus
            List<double> temp;
            AreaActualModel tempAreaActualModel;

            if (string.IsNullOrEmpty(ProductionUtilizationChart))
                ProductionUtilizationChart = Common.Enums.ProductionUtilization.ELECTRIC.ToString();

            //iterate area
            foreach (var a in areas)
            {
                //iterate month
                temp = new List<double>();
                tempAreaActualModel = new AreaActualModel();
                foreach (int y in ListYear)
                {
                    double value = 0;
                    if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                        value = model.Where(x => x.Periode != null && x.Periode.Value.Year == y && x.AreaId == a.Id).Sum(n => n.AverageEnergy / 1000);
                    else
                        value = model.Where(x => x.Periode != null && x.Periode.Value.Year == y && x.AreaId == a.Id).Sum(n => n.AverageSteam);
                    temp.Add(value);
                }
                tempAreaActualModel.AreaName = a.Name;
                tempAreaActualModel.ActualValue = temp;
                JsonYearlyActual.Add(tempAreaActualModel);

                temp = new List<double>();
                tempAreaActualModel = new AreaActualModel();
                foreach (Month val in Enum.GetValues(typeof(Month)))
                {
                    double value = 0;
                    int year = (int)val < 12 ? DateTime.Now.Year : DateTime.Now.AddYears(1).Year;
                    int month = (int)val < 12 ? (int)val + 1 : 1;

                    if (ProductionUtilizationChart == Common.Enums.ProductionUtilization.ELECTRIC.ToString())
                        value = model.Where(x => x.Periode >= new DateTime(DateTime.Now.Year, (int)val, 1) && x.Periode < new DateTime(year, month, 1) && x.AreaId == a.Id).Sum(n => n.AverageEnergy / 1000);
                    else
                        value = model.Where(x => x.Periode >= new DateTime(DateTime.Now.Year, (int)val, 1) && x.Periode < new DateTime(year, month, 1) && x.AreaId == a.Id).Sum(n => n.AverageSteam);

                    temp.Add(value);
                }
                tempAreaActualModel.AreaName = a.Name;
                tempAreaActualModel.ActualValue = temp;
                JsonMonthlyActual.Add(tempAreaActualModel);
            }
        }

        protected override void Init()
        {
            Year = DateTime.Now.Year;
            Categories = new List<string>();
            JsonMonthlyPlan = new List<double>();
            JsonDailyPlan = new List<double>();
            JsonYearlyPlan = new List<YearlyPlanModel>();
            JsonDailyActual = new List<AreaActualModel>();
            JsonMonthlyActual = new List<AreaActualModel>();
            JsonYearlyActual = new List<AreaActualModel>();

            YearChart = DateTime.Now.Year;
            MonthChart = DateTime.Now.Month;
        }
    }

}