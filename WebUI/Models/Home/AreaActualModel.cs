﻿using System.Collections.Generic;

namespace WebUI.Models
{
    public class AreaActualModel
    {
        public string AreaName { get; set; }
        public string PowerPlant { get; set; }
        public List<double> ActualValue { get; set; }
    }
}