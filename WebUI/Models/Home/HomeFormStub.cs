﻿using System;
using System.ComponentModel;

namespace WebUI.Models
{
    public class HomeFormStub 
    {
       
        public DateTime NowDate { get; set; }
        public DateTime LastDate { get; set; }

        [DisplayName("Power Plant")]
        public string PowerPlant { get; set; }
        public int NowSum { get; set; }
        public int LastSum { get; set; }

        public string BebanJam { get; set; }
        public int FlowUap { get; set; }
        public int TekananUtama { get; set; }
        public int PemakaianSendiri { get; set; }
        public string AllKwh { get; set; }
        public string HourKwh { get; set; }

        public int Temperature { get; set; }
        public int TekananCondesasor { get; set; }
        public int SSC{ get; set; }
        public int MVar { get; set; }

        [DisplayName("Energi Listrik Gross")]
        public int EnergiListrikGross { get; set; }
        [DisplayName("Energi Listrik Net")]
        public int EnergiListrikNett { get; set; }

        [DisplayName("Pemanfaatan Uap")]
        public int UapPembangkit { get; set; }

        public int Buangan { get; set; }
        public int SSC2 { get; set; }

        [DisplayName("GPP Shutdown Hour")]
        public int GPPHour { get; set; }

        [DisplayName("Import Energy")]
        public int ImportEnergy { get; set; }

        [DisplayName("SGS Shutdown Hour")]
        public int SGSHour { get; set; }

        public HomeFormStub() : base()
        {
        }
    }
}