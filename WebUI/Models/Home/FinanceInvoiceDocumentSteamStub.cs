﻿using Common.Enums;
using System.Collections.Generic;
using System.Linq;
using WebUI.Infrastructure;
using WebUI.Controllers;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Models.Home
{
    public class FinanceInvoiceDocumentSteamStub
	{
		private Business.Entities.Workflow WF1 { get; set; }
		private Business.Entities.Workflow WF2 { get; set; }
		private Business.Entities.Workflow WF3 { get; set; }
		private Business.Entities.Workflow WF4 { get; set; }

		public string NameWF1 { get; set; }
		public string NameWF2 { get; set; }
		public string NameWF3 { get; set; }
		public string NameWF4 { get; set; }

		public List<Row> Rows { get; set; }

		public class Row
		{
			public int InvoiceId { get; set; }
			public int SalesOrderId { get; set; }
			public string Periode { get; set; }
			public string Unit { get; set; }
			public List<string> InvoiceFriendlyName { get; set; }
			public List<string> InvoiceAbsolutePath { get; set; }
			public bool IsWF1 { get; set; }
			public bool IsWF2 { get; set; }
			public bool IsWF3 { get; set; }
			public bool IsWF4 { get; set; }
			public bool IsCoverLetter { get; set; }

			public Row()
			{

			}

			public Row(Business.Entities.Invoice invoice)
			{
				InvoiceId = invoice.Id;
				SalesOrderId = invoice.SalesOrder?.Id ?? 0;
				Periode = invoice.SalesOrder?.Periode?.ToString(DisplayFormat.MonthYearDateFormat);
				Unit = invoice.SalesOrder?.PowerPlant?.Name;

				InvoiceFriendlyName = new List<string>();
				InvoiceAbsolutePath = new List<string>();
				foreach (Business.Entities.InvoiceFile invoiceFile in invoice.InvoiceFiles)
				{
					InvoiceFriendlyName.Add(FileManagementController.FriendlyName(invoiceFile.FilePath));
					InvoiceAbsolutePath.Add(new UrlHelper(HttpContext.Current.Request.RequestContext).Content(invoiceFile.FilePath));
				}

				IsCoverLetter = true;
			}
		}

		public FinanceInvoiceDocumentSteamStub()
		{
			Rows = new List<Row>();
		}

		public FinanceInvoiceDocumentSteamStub(List<Business.Entities.Invoice> invoices, List<Business.Entities.Workflow> workflows)
		{
			WF4 = workflows?.FirstOrDefault(x => x.Approver == null) ?? null;
			WF3 = workflows?.FirstOrDefault(x => x.Id == WF4.PreviousStep) ?? null;
			WF2 = workflows?.FirstOrDefault(x => x.Id == WF3.PreviousStep) ?? null;
			WF1 = workflows?.FirstOrDefault(x => x.Id == WF2.PreviousStep) ?? null;

			NameWF1 = WF1?.Name;
			NameWF2 = WF2?.Name;
			NameWF3 = WF3?.Name;
			NameWF4 = WF4?.Name;

			Rows = new List<Row>();

			foreach (Business.Entities.Invoice invoice in invoices)
			{
				if (invoice != null)
				{
					Row row = new Row(invoice);

					row.IsWF4 = invoice.Workflow.Id == WF4?.Id;
					row.IsWF3 = row.IsWF4 || (invoice.Workflow.Id == WF3?.Id);
					row.IsWF2 = row.IsWF3 || (invoice.Workflow.Id == WF2?.Id);
					row.IsWF1 = row.IsWF2 || (invoice.Workflow.Id == WF1?.Id);

					Rows.Add(row);
				}
			}
		}
	}
}