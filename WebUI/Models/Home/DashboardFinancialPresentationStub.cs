﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Enums;
using Business.Entities.Views;

namespace WebUI.Models.Home
{
    public class DashboardFinancialPresentationStub
    {
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public double Production { get; set; }
        public double ProductionActual { get; set; }
        public double ProductionPercent { get; set; }                
        public DateTime? Periode { get; set; }
        public string PeriodeStr { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string MonthDesc { get; set; }
        public int Day { get; set; }
        public bool IsExplode { get; set; }
        public double TotalNetValue { get; set; }
        public DashboardFinancialPresentationStub() { }

        public static List<DashboardFinancialPresentationStub> MapList(IEnumerable<Area> areas, List<ViewInvoice> dbItems)
        {
            List<DashboardFinancialPresentationStub> result = new List<DashboardFinancialPresentationStub>();
            foreach (var area in areas)
            {
                DashboardFinancialPresentationStub model = new DashboardFinancialPresentationStub();
                model.AreaId = area.Id;
                model.AreaName = area.Name;
                model.Production = dbItems.Where(n => n.AreaId == area.Id).Sum(n => n.NetValue).Value;
                model.ProductionPercent = (model.Production / dbItems.Sum(n => n.NetValue)).Value * 100;
                model.ProductionPercent = double.IsNaN(model.ProductionPercent) || double.IsInfinity(model.Production) ? 0 : model.ProductionPercent;
                model.IsExplode = true;
                model.TotalNetValue = dbItems.Sum(n => n.NetValue).Value;
                result.Add(model);
            }
            return result;
        }
        public static List<DashboardFinancialPresentationStub> MapListRevenue(IEnumerable<Area> areas, List<ViewInvoice> dbItems)
        {
            List<DashboardFinancialPresentationStub> result = new List<DashboardFinancialPresentationStub>();
            foreach (var area in areas)
            {
                DashboardFinancialPresentationStub model = new DashboardFinancialPresentationStub();
                model.AreaId = area.Id;
                model.AreaName = area.Name;
                model.Production = dbItems.Where(n => n.AreaId == area.Id).Sum(n => n.NetValue).Value;
                model.ProductionActual = dbItems.Where(n => n.AreaId == area.Id).Sum(n => n.NetValue).Value;
                model.IsExplode = true;
                result.Add(model);
            }
            return result;
        }
        public static List<DashboardFinancialPresentationStub> MapListPeriode(IEnumerable<Area> areas, List<ViewInvoice> dbItems, string range, int month, int year)
        {
            List<DashboardFinancialPresentationStub> result = new List<DashboardFinancialPresentationStub>();

            int lastDay = DateTime.DaysInMonth(year, month);
            DateTime lastDate = new DateTime(year, month, lastDay);

            if (range == RangeData.DAILY.ToString())
            {
                for (int d = 1; d <= lastDay; d++)
                {
                    List<ViewInvoice> datas = dbItems.Where(n => n.TaskPeriode.Value.Day == d).ToList();
                    result.AddRange(MapListObject(areas, datas, d.ToString()));
                }

            }
            else if (range == RangeData.MONTHLY.ToString())
            {
                IEnumerable<Month> monthList = EnumExtension.EnumToList<Month>();
                foreach (var m in monthList)
                {
                    List<ViewInvoice> datas = dbItems.Where(n => n.TaskPeriode.Value.Month == (int)m && n.TaskPeriode.Value.Year == year).ToList();
                    result.AddRange(MapListObject(areas, datas, m.ToDescription()));
                }
            }
            else
            {
                int startYear = dbItems.Where(n=>n.TaskPeriode.Value.Year > 0).Min(n => n.TaskPeriode.Value.Year);
                int endYear = dbItems.Where(n => n.TaskPeriode.Value.Year > 0).Max(n => n.TaskPeriode.Value.Year);
                for (int y = startYear; y <= endYear; y++)
                {
                    result.AddRange(MapListObject(areas, dbItems, y.ToString()));
                }
            }
            return result;
        }

        private static List<DashboardFinancialPresentationStub> MapListObject(IEnumerable<Area> areas, List<ViewInvoice> dbItems, string periode)
        {
            List<DashboardFinancialPresentationStub> result = new List<DashboardFinancialPresentationStub>();
            DashboardFinancialPresentationStub model = new DashboardFinancialPresentationStub();

            foreach (var area in areas)
            {
                model = new DashboardFinancialPresentationStub();
                var datas = dbItems.Where(n => n.AreaId == area.Id);
                if (datas.Any())
                {
                    model.AreaId = area.Id;
                    model.AreaName = area.Name;
                    model.Production = datas.Sum(n => n.NetValue).Value;
                    model.ProductionPercent = (model.Production / datas.Sum(n => n.NetValue)).Value * 100;
                    model.ProductionPercent = double.IsNaN(model.ProductionPercent) || double.IsInfinity(model.Production) ? 0 : model.ProductionPercent;
                    model.IsExplode = true;
                }
                else
                {
                    model.AreaId = area.Id;
                    model.AreaName = area.Name;
                    model.Production = 0;
                    model.ProductionPercent = 0;
                    model.IsExplode = true;
                }

                model.PeriodeStr = periode;
                result.Add(model);
            }

            return result;
        }
        
    }
}