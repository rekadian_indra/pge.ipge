﻿using Business.Entities;
using Business.Entities.Views;
using Common.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace WebUI.Models
{
    public class DashboardExecutivePresentationStub
    {
        public int? AreaId { get; set; }

        public DateTime? LastDateProduction { get; set; }
        public string LastDateProductionStringDate { get; set; }
        public Utilization EnergyUtilization { get; set; }
        public Utilization SteamUtilization { get; set; }
        public Utilization TotalEnergyCumulative { get; set; }
        public Utilization EnergyCumulative { get; set; }
        public Utilization SteamCumulative { get; set; }
        public double SumEnergyNett { get; set; }
        public double SumEnergyGross { get; set; }

        public string SourceChartJson { get; set; }

        public DashboardExecutivePresentationStub()
        {
        }
        public DashboardExecutivePresentationStub(List<ViewProgressEnergy> energyProgresses, List<Pltp> pltps,
            List<ViewProgressEnergy> progresses, IEnumerable<ProductionPlanDetail> planDetail)
        {
            double planEnergy = 0, planSteam = 0;
            double lastDay = 0;
            EnergyUtilization = new Utilization();
            EnergyUtilization.PlanProgress = new Progress();
            EnergyUtilization.ActualProgress = new Progress();

            SteamUtilization = new Utilization();
            SteamUtilization.ActualProgress = new Progress();
            SteamUtilization.PlanProgress = new Progress();
            if (energyProgresses.Any(n => n.Periode.HasValue))
            {
                LastDateProduction = energyProgresses.Where(n => n.Periode.HasValue).Max(n => n.Periode.Value);
                DateTime lastDate = new DateTime(LastDateProduction.Value.Year, LastDateProduction.Value.Month, 1);
                lastDate = lastDate.AddMonths(1).AddDays(-1);
                lastDay = lastDate.Day;
                planEnergy = planDetail.Where(n => n.Month == LastDateProduction.Value.Month && n.ProductionPlan.Year == LastDateProduction.Value.Year && n.ProductionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString()).Sum(n => n.Value / lastDay);
                planSteam = planDetail.Where(n => n.Month == LastDateProduction.Value.Month && n.ProductionPlan.Year == LastDateProduction.Value.Year && n.ProductionPlan.UtilType == ProductionUtilization.STEAM.ToString()).Sum(n => n.Value / lastDay);


                //energy           

                SumEnergyNett = energyProgresses.Sum(m => m.EnergyNett.Value);
                SumEnergyGross = energyProgresses.Sum(m => m.EnergyGross.Value);

                //actual

                EnergyUtilization.ActualProgress.Nett = energyProgresses.Sum(n => n.EnergyNett.Value) / (LastDateProduction.HasValue ? LastDateProduction.Value.Day : 0);
                EnergyUtilization.ActualProgress.Nett = double.IsNaN(EnergyUtilization.ActualProgress.Nett) || double.IsInfinity(EnergyUtilization.ActualProgress.Nett) ? 0 : EnergyUtilization.ActualProgress.Nett;
                EnergyUtilization.ActualProgress.Price = energyProgresses.Sum(n => n.TotalEnergyActual);

                //plan                
                //EnergyUtilization.PlanProgress.Nett = energyProgresses.Sum(n => n.AverageEnergyPlan);
                //EnergyUtilization.PlanProgress.Price = energyProgresses.Sum(n => n.TotalEnergyPlan);
                EnergyUtilization.PlanProgress.Nett = planEnergy;
                EnergyUtilization.PlanProgress.Price = energyProgresses.Sum(n => n.EnergyPrice.Value) * planEnergy * 1000;


                //steam            
               

                //actual
              
                SteamUtilization.ActualProgress.Nett = energyProgresses.Sum(n => n.SteamUtilization.Value) / energyProgresses.Count;
                SteamUtilization.ActualProgress.Nett = double.IsNaN(SteamUtilization.ActualProgress.Nett) || double.IsInfinity(SteamUtilization.ActualProgress.Nett) ? 0 : SteamUtilization.ActualProgress.Nett;
                SteamUtilization.ActualProgress.Price = energyProgresses.Sum(n => n.TotalSteamActual);

                //plan
                
                SteamUtilization.PlanProgress.Nett = planSteam;
                SteamUtilization.PlanProgress.Price = energyProgresses.Sum(n => n.SteamPrice.Value) * planSteam;
                //SteamUtilization.PlanProgress.Nett = energyProgresses.Sum(n => n.AverageSteamPlan);
                //SteamUtilization.PlanProgress.Price = energyProgresses.Sum(n => n.TotalSteamPlan);            


                SourceChartJson = JsonConvert.SerializeObject(ChartProgress.MapList(progresses));
            }
        }
    }

    public class ChartProgress
    {
        public int? AreaId { get; set; }
        public string Material { get; set; }
        public double? SteamNett { get; set; }
        public double? EnergyNett { get; set; }
        public DateTime Periode { get; set; }
        public string Month
        {
            get
            {
                return Periode.ToString("MMM");
            }
        }

        public List<ChartProgress> Progresses { get; set; }

        public ChartProgress() { }
        public ChartProgress(Pltp pltp)
        {            
            AreaId = pltp != null ? pltp.TaskReport.AreaId : null;
            Material = pltp != null ? pltp.PowerPlant.Material : null;
            EnergyNett = pltp.EnergyNett.HasValue ? pltp.EnergyNett / 1000 : null;
            SteamNett = pltp.SteamUtilization.HasValue ? pltp.SteamUtilization : null;

        }
        public static List<ChartProgress> MapList(List<ViewProgressEnergy> pltps)
        {
            DateTime now = DateTime.Now;
            List<ChartProgress> result = new List<ChartProgress>();
            ChartProgress chart = new ChartProgress();
            for (int i = 1; i <= 12; i++)
            {
                var listDatas = pltps.Where(n => n.Periode.Value.Month == i && n.Year == now.Year);
                if (listDatas.Any())
                {
                    chart = new ChartProgress();
                    foreach (var pltp in listDatas)
                    {                        
                        chart.Periode = new DateTime(now.Year, i, 1);
                        if (i <= now.Month)
                        {
                            chart.AreaId = pltp.AreaId;
                            chart.Material = pltp.Material;
                            chart.EnergyNett = listDatas.Sum(n=>n.EnergyNett);
                            chart.SteamNett = listDatas.Sum(n=>n.SteamUtilization);                            
                        }
                        result.Add(chart);
                    }
                }
                else
                {
                    chart = new ChartProgress();
                    chart.Periode = new DateTime(now.Year, i, 1);
                    if (i <= now.Month)
                    {
                        chart.EnergyNett = 0;
                        chart.SteamNett =  0;
                    }
                    result.Add(chart);
                }
            }
            return result;
        }
    }

    public class Utilization
    {
        public Progress PlanProgress { get; set; }
        public Progress ActualProgress { get; set; }
        public double Ratio
        {
            get
            {
                double result = ActualProgress.Nett / PlanProgress.Nett * 100;
                return double.IsNaN(result) || double.IsInfinity(result) ? 0 : result;
            }
        }

        public Utilization() { }
      
    }

    public class Progress
    {
        //unit ton untuk uap
        //unit MWh untuk listrik
        public double Nett { get; set; }

        //unit in ($)
        public double Price { get; set; }        
    }

}