﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.Home
{
    public class FinanceDashboardChartPresentaionStub
    {
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public double? NetValue { get; set; }
        public DateTime? LastInvoiceDate { get; set; }
        public List<FinanceDashboardChartPresentaionStub> SubArea { get; set; }

    }
}