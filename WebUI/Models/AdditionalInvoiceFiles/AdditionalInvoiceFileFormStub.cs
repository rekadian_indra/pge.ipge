﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.AdditionalInvoiceFiles
{
    public class AdditionalInvoiceFileFormStub : BaseFormStub<AdditionalInvoiceFile, AdditionalInvoiceFileFormStub>
    {

        #region "Properties"
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public string FilePath { get; set; }
        #endregion

        #region "Constructor"
        public AdditionalInvoiceFileFormStub() : base()
        {

        }

        public AdditionalInvoiceFileFormStub(AdditionalInvoiceFile dbObject) : base(dbObject)
        {

        }
        #endregion

        #region "Helper"
        public override void MapDbObject(AdditionalInvoiceFile dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
            
        }
        #endregion
    }
}