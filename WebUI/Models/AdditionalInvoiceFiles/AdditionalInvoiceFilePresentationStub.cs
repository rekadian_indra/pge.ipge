﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;
using WebUI.Models.AdditionalInvoiceFiles;

namespace WebUI.Models
{
    public class AdditionalInvoiceFilePresentationStub : AdditionalInvoiceFileFormStub
    {
		public string FriendlyName { get; set; }
		public string AbsolutePath { get; set; }

		public AdditionalInvoiceFilePresentationStub() : base()
        {

		}

		public AdditionalInvoiceFilePresentationStub(AdditionalInvoiceFile dbObject) : base(dbObject)
		{
			FriendlyName = WebUI.Controllers.FileManagementController.FriendlyName(FilePath);
			AbsolutePath = new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext).Content(FilePath);
		}
	}
}