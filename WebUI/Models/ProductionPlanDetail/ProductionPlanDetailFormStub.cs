﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace WebUI.Models
{
	public class ProductionPlanDetailFormStub : BaseFormStub<ProductionPlanDetail, ProductionPlanDetailFormStub>
	{
		public int Id { get; set; }
		public int ProductionPlanId { get; set; }
		public int Month { get; set; }
		public double Value { get; set; }
		public List<ProductionPlanDailyFormStub> ProductionPlanDailies { get; set; }

		public int Year { get; set; }

		public ProductionPlanDetailFormStub() : base()
		{

		}

		public ProductionPlanDetailFormStub(int year, int month) : base()
		{
			Year = year;
			Month = month;

			ProductionPlanDailies = new List<ProductionPlanDailyFormStub>();
			int daysInMonth = DateTime.DaysInMonth(year, month);
			for (int curDay = 1; curDay <= daysInMonth; curDay++)
			{
				ProductionPlanDailyFormStub curProductionPlanDaily = new ProductionPlanDailyFormStub()
				{
					Day = curDay,
					ProductionPlanDetailId = Id
				};

				ProductionPlanDailies.Add(curProductionPlanDaily);
			}
		}

		public ProductionPlanDetailFormStub(ProductionPlanDetail dbObject) : base(dbObject)
		{
			ProductionPlanDailies = new List<ProductionPlanDailyFormStub>();

			int daysInMonth = DateTime.DaysInMonth(dbObject.ProductionPlan.Year, dbObject.Month);
			for(int curDay = 1; curDay <= daysInMonth; curDay++)
			{
				ProductionPlanDaily curProductionPlanDailyDb = dbObject.ProductionPlanDailies.FirstOrDefault(x => x.Day == curDay);
				ProductionPlanDailyFormStub curProductionPlanDaily;
				if (curProductionPlanDailyDb == null)
				{
					curProductionPlanDaily = new ProductionPlanDailyFormStub()
					{
						Day = curDay,
						ProductionPlanDetailId = Id,
						Value = dbObject.Value / daysInMonth
					};
				}
				else
				{
					curProductionPlanDaily = new ProductionPlanDailyFormStub(curProductionPlanDailyDb);
				}

				ProductionPlanDailies.Add(curProductionPlanDaily);
			}
		}

		public ProductionPlanDailyFormStub GetDailyProductionPlan(int day)
		{
			if (ProductionPlanDailies != null)
			{
				return ProductionPlanDailies.FirstOrDefault(x => x.Day == day);
			}
			return null;
		}

		protected override void Init()
		{
            //TODO: Set the init object here to be called in all constructor
            ProductionPlanDailies = new List<ProductionPlanDailyFormStub>();

        }
	}
}