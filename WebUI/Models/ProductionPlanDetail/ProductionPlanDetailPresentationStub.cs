﻿using Business.Entities;
using System;
using System.ComponentModel;
using WebUI.Infrastructure;

namespace WebUI.Models
{
	public class ProductionPlanDetailPresentationStub : ProductionPlanDetailFormStub
	{
		public string StrValue { get; }

		public ProductionPlanDetailPresentationStub() : base()
		{

		}

		public ProductionPlanDetailPresentationStub(ProductionPlanDetail dbObject) : base(dbObject)
		{
			StrValue = DisplayFormat.NumberFormatNoDecimal(dbObject.Value);
		}

		protected override void Init()
		{
			//TODO: Set the init object here to be called in all constructor
		}
	}
}