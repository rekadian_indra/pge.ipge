﻿using System.IO;

namespace WebUI.Models
{
    public class DownloadParameterStub
    {
        public MemoryStream MemoryStream { get; set; }
        public string ContentType { get; set; }
        public string FileDownloadName { get; set; }

        public DownloadParameterStub()
        {
        }

        public DownloadParameterStub(MemoryStream file, string contentType, string fileDownloadName)
        {
            MemoryStream = file;
            ContentType = contentType;
            FileDownloadName = fileDownloadName;
        }
    }
}