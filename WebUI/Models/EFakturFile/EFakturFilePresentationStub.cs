﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Models
{
    public class EFakturFilePresentationStub : EFakturFileFormStub
	{
		public string FriendlyName { get; set; }
		public string AbsolutePath { get; set; }

		public EFakturFilePresentationStub() : base()
        {

		}

		public EFakturFilePresentationStub(Business.Entities.EFakturFile dbObject) : base(dbObject)
		{
			FriendlyName = WebUI.Controllers.FileManagementController.FriendlyName(FilePath);
			AbsolutePath = new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext).Content(FilePath);
		}
	}
}