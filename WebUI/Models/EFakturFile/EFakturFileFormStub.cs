﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Models
{
    public class EFakturFileFormStub : BaseFormStub<Business.Entities.EFakturFile, EFakturFileFormStub>
	{
		public int Id { get; set; }
		public int InvoiceId { get; set; }
		public string FilePath { get; set; }

		public EFakturFileFormStub() : base()
        {

		}

		public EFakturFileFormStub(Business.Entities.EFakturFile dbObject) : base(dbObject)
		{

		}

		public override void MapDbObject(Business.Entities.EFakturFile dbObject)
		{
			base.MapDbObject(dbObject);
		}

		protected override void Init()
		{
			//TODO: Set the init object here to be called in all constructor
		}
	}
}