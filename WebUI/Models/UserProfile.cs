﻿using Common.Enums;
using System.Web.Profile;
using System.Web.Security;

namespace WebUI.Models
{
    public class UserProfile : ProfileBase
    {
        public static UserProfile CurrentUser
        {
            get
            {
                if (Membership.GetUser() != null)
                    return Create(Membership.GetUser().UserName) as UserProfile;
                else
                    return null;
            }
        }

        public static UserProfile GetUserProfile(string username)
        {
            return Create(username) as UserProfile;
        }
    }
}