﻿using Business.Entities.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.FormMiro
{
    public class FormMiroPresentationStub : BasePresentationStub<ViewSalesOrderApproval, FormMiroPresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int SalesOrderId { get; set; }
        public int WorkflowId { get; set; }
        public string Notes { get; set; }
        public string WorkflowCode { get; set; }
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public DateTime Periode { get; set; }
        public string WorkflowName { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string SOType { get; set; }
        public string WorkflowType { get; set; }
        public string PowerPlantName { get; set; }
        public string Period { get; set; }
        
        #endregion

        #region "Constructor"
        public FormMiroPresentationStub() : base()
        {
            
        }

        public FormMiroPresentationStub(ViewSalesOrderApproval dbObjects) : base(dbObjects)
        {
            Period = $"{ToLongMonth(dbObjects.Month)} {dbObjects.Year.ToString()}";
        }
        #endregion


        protected override void Init()
        {
            
        }

        protected string ToLongMonth(int month)
        {
            switch (month)
            {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
                default:
                    return null;
            };
        }
    }
}