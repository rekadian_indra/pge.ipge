﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.FormMiro
{
    public class AutoNumberFormStub : BaseFormStub<AutoNumber, AutoNumberFormStub>
    {
        public int Id { get; set; }
        public string TransactionCode { get; set; }
        public string TransactionName { get; set; }
        public int Year { get; set; }
        public string Prefix { get; set; }
        public int IncrementNumber { get; set; }

        public AutoNumberFormStub() : base()
        {

        }

        public AutoNumberFormStub(AutoNumber dbObject) : base(dbObject)
        {

        }

        public override void MapDbObject(AutoNumber dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
        }
    }
}