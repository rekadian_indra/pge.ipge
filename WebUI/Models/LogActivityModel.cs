﻿using Business.Extension;
using Business.Infrastructure;
using System;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class LogActivityModel<T>
        where T : class
    {
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }

        private Principal User
        {
            get
            {
                return HttpContext.Current.User as Principal;
            }
        }

        public LogActivityModel()
        {
            SetCreated();
        }

        public LogActivityModel(T @object)
        {
            ObjectMapper.MapObject<T, LogActivityModel<T>>(@object, this);
            ToLocalTime();
            SetModified();
        }

        private void SetCreated()
        {
            DateTime now = DateTime.Now;

            CreatedBy = User.UserName;            
            CreatedDateTimeUtc = now;
            ModifiedBy = User.UserName;
            ModifiedDateTimeUtc = now;
            ToUtcTime();
        }

        private void SetModified()
        {
            ModifiedBy = User.UserName;            
            ModifiedDateTimeUtc = DateTime.Now;
            ToUtcTime();
        }

        private void ToLocalTime()
        {
            CreatedDateTimeUtc = CreatedDateTimeUtc.ToLocalDateTime();
            ModifiedDateTimeUtc = ModifiedDateTimeUtc.ToLocalDateTime();
        }

        private void ToUtcTime()
        {
            CreatedDateTimeUtc = CreatedDateTimeUtc.ToUtcDateTime();
            ModifiedDateTimeUtc = ModifiedDateTimeUtc.ToUtcDateTime();
        }
    }
}