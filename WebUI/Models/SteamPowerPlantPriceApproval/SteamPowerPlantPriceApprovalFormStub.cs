﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace WebUI.Models
{
    public class SteamPowerPlantPriceApprovalFormStub : BaseFormStub<SteamPowerPlantPriceApproval, SteamPowerPlantPriceApprovalFormStub>
	{
		public int Id { get; set; }
		public int SteamPowerPlantPriceId { get; set; }
		public int WorkflowId { get; set; }
		public string Notes { get; set; }

		public SteamPowerPlantPriceApprovalFormStub() : base()
		{
			
		}

		public SteamPowerPlantPriceApprovalFormStub(SteamPowerPlantPriceApproval dbObject) : base(dbObject)
        {

        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}