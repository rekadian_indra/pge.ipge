﻿namespace WebUI.Models
{
    public class FileManagementResponseModel
	{
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string FileNameSaved { get; set; }
        public string AbsolutePath { get; set; }
		public string UId { get; set; }
	}
}