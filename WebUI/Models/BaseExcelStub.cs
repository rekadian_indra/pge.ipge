﻿using Business.Entities;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using WebUI.Extension;

namespace WebUI.Models
{
    public abstract class BaseExcelStub
	{
		const int COL_MERGE_SIGNATURE = 2;
		const int ROW_MERGE_SIGNATURE = 4;

		#region Fields

		public XSSFWorkbook Workbook;

		protected short DataFormatNumeric;
		protected short DataFormatDecimalTwo;
		protected short DataFormatDecimalFive;

		#endregion

		#region Constructor

		public BaseExcelStub()
		{

		}

		public BaseExcelStub(XSSFWorkbook workbook)
		{
			//culture
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US"); //supaya file tidak corrupt
			Workbook = workbook;

			DataFormatNumeric = Workbook.CreateDataFormat().GetFormat("#,##0");
			DataFormatDecimalTwo = Workbook.CreateDataFormat().GetFormat("#,##0.00");
			DataFormatDecimalFive = Workbook.CreateDataFormat().GetFormat("#,##0.00000");
		}

		#endregion

		#region Cell Set Helper

		public void SetCellValue(XSSFSheet sheet, string colChars, int rowNum, string value, short? dataFormat = null)
		{
			int rowIdx = RowNumToIdx(rowNum);
			int colIdx = ColCharsToIdx(colChars);
			SetCellValue(sheet, rowIdx, colIdx, value, dataFormat);
		}

        //
        //public void WriteCellValue(XSSFSheet sheet, string colChars, int rowNum, string value, short? dataFormat = null, bool? isBold = null, bool? isItalic = null, bool? isUnderline = null)
        //{
        //    int rowIdx = RowNumToIdx(rowNum);
        //    int colIdx = ColCharsToIdx(colChars);
        //    WriteCellValue(sheet, rowIdx, colIdx, value, dataFormat, isBold, isItalic, isUnderline);
        //}

        public void SetCellValue(XSSFSheet sheet, string colChars, int rowNum, double value, short? dataFormat = null)
		{
			int rowIdx = RowNumToIdx(rowNum);
			int colIdx = ColCharsToIdx(colChars);
			SetCellValue(sheet, rowIdx, colIdx, value, dataFormat);
		}

		public void SetCellValue(XSSFSheet sheet, string colChars, int rowNum, DateTime value, short? dataFormat = null)
		{
			int rowIdx = RowNumToIdx(rowNum);
			int colIdx = ColCharsToIdx(colChars);
			SetCellValue(sheet, rowIdx, colIdx, value, dataFormat);
		}

		public void SetCellValue(XSSFSheet sheet, int rowIdx, int colIdx, string value, short? dataFormat = null)
		{
			XSSFRow row = (XSSFRow)sheet.GetRow(rowIdx);
			XSSFCell cell = (XSSFCell)row.GetCell(colIdx);

			cell.SetCellValue(value);
			cell.CellStyle.DataFormat = dataFormat ?? cell.CellStyle.DataFormat;
		}

        //
        //private void WriteCellValue(XSSFSheet sheet, int rowIdx, int colIdx, string value, short? dataFormat = null, bool? isBold = null, bool? isItalic = null, bool? isUnderline = null)
        //{
        //    XSSFRow row = (XSSFRow)sheet.GetRow(rowIdx);
        //    XSSFCell cell = (XSSFCell)row.GetCell(colIdx);
        //    XSSFFont font = (XSSFFont)Workbook.CreateFont();

        //    cell.SetCellValue(value);
        //    cell.CellStyle.DataFormat = dataFormat ?? cell.CellStyle.DataFormat;

        //    if (isBold != null)
        //    {
        //        if (isBold == true)
        //        {
        //            font.IsBold = true;
        //        } 
        //        else
        //        {
        //            font.IsBold = false;
        //        }
        //    }
        //    if (isBold != null)
        //    {
        //        if (isBold == true)
        //        {
        //            font.IsBold = true;
        //        }
        //        else
        //        {
        //            font.IsBold = false;
        //        }
        //    }
        //    if (isItalic != null)
        //    {
        //        if (isItalic == true)
        //        {
        //            font.IsItalic = true;
        //        }
        //        else
        //        {
        //            font.IsItalic = false;
        //        }
        //    }
        //    if (isUnderline != null)
        //    {
        //        if (isUnderline == true)
        //        {
        //            font.Underline = FontUnderlineType.Single;
        //        }
        //        else
        //        {
        //            font.Underline = FontUnderlineType.None;
        //        }
        //    }
        //}

        public void SetCellValue(XSSFSheet sheet, int rowIdx, int colIdx, double value, short? dataFormat = null)
		{
			XSSFRow row = (XSSFRow)sheet.GetRow(rowIdx);
			XSSFCell cell = (XSSFCell)row.GetCell(colIdx);

			cell.SetCellValue(value);
			cell.CellStyle.DataFormat = dataFormat ?? cell.CellStyle.DataFormat;
		}

		public void SetCellValue(XSSFSheet sheet, int rowIdx, int colIdx, DateTime value, short? dataFormat = null)
		{
			XSSFRow row = (XSSFRow)sheet.GetRow(rowIdx);
			XSSFCell cell = (XSSFCell)row.GetCell(colIdx);

			cell.SetCellValue(value);
			cell.CellStyle.DataFormat = dataFormat ?? cell.CellStyle.DataFormat;
		}

		public void SetCellImage(XSSFSheet sheet, string colChars1, int rowNum1, string colChars2, int rowNum2, string filePath)
		{
			int colIdx1 = ColCharsToIdx(colChars1);
			int rowIdx1 = RowNumToIdx(rowNum1);
			int colIdx2 = ColCharsToIdx(colChars2);
			int rowIdx2 = RowNumToIdx(rowNum2);

			string absolutePath = HttpContext.Current.Server.MapPath(filePath);
			byte[] data = File.ReadAllBytes(absolutePath);
			int picInd = Workbook.AddPicture(data, XSSFWorkbook.PICTURE_TYPE_JPEG);
			XSSFCreationHelper helper = Workbook.GetCreationHelper() as XSSFCreationHelper;
			XSSFDrawing drawing = sheet.CreateDrawingPatriarch() as XSSFDrawing;
			XSSFClientAnchor anchor = helper.CreateClientAnchor() as XSSFClientAnchor;
			anchor.Col1 = colIdx1;
			anchor.Row1 = rowIdx1;
			anchor.Col2 = colIdx2 + 1;
			anchor.Row2 = rowIdx2 + 1;
			XSSFPicture pict = drawing.CreatePicture(anchor, picInd) as XSSFPicture;

			// hitung skala ukuran image ke ukuran cell, biar ga overlap
			//float cellWidth = COL_MERGE_SIGNATURE * sheet.GetColumnWidthInPixels(colIdx);
			//XSSFRow curRow = (XSSFRow)sheet.GetRow(rowIdx);
			//float rowHeight = ROW_MERGE_SIGNATURE * curRow.Height;
			//System.Drawing.Image img = System.Drawing.Image.FromFile(absolutePath);
			//float scaleX = cellWidth / img.Width;
			//float scaleY = rowHeight / img.Height;
			//float resizeX, resizeY;
			//if (scaleX < scaleY)
			//{
			//	resizeX = scaleX * scaleY;
			//	resizeY = 1;
			//}
			//else
			//{
			//	resizeX = 1;
			//	resizeY = scaleX * scaleY;
			//}

			//pict.Resize();
			pict.Resize(1);
			//pict.Resize(scaleX/*Math.Min(scaleX, scaleY)*/);
		}

		#endregion

		#region Helper
		
		protected byte[] WorkbookToArray()
		{
			//write to byte[]
			MemoryStream ms = new MemoryStream();

			Workbook.Write(ms);

			return ms.ToArray();
		}

		#endregion

		#region Private Helper

		private int RowNumToIdx(int rowNum)
		{
			return rowNum - 1;
		}

		private int ColCharsToIdx(string colChars)
		{
			// upper case semua dulu
			colChars = colChars.ToUpper();
			// reverse buat di foreach biar diproses mulai dari "satuan"
			char[] arr = colChars.ToCharArray();
			Array.Reverse(arr);
			colChars = new string(arr);

			// hitung indexnya
			int multiplier = 0; // "puluhan, ratusan, ribuan" dari huruf
			int result = -1; // index dikurangi satu
			foreach (char colChar in colChars)
			{
				if ((colChar < 'A') || (colChar > 'Z')) continue;
				result += (multiplier * 26) + colChar - 'A' + 1;
				multiplier++;
			}
			return result;
		}

		#endregion
	}
}