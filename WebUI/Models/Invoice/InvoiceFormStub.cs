﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Areas.SalesOrder.Models;
using WebUI.Infrastructure;
using WebUI.Models;
using WebUI.Models.AdditionalInvoiceFiles;

namespace WebUI.Models
{
    public class InvoiceFormStub : BaseFormStub<Business.Entities.Invoice, InvoiceFormStub>
    {
		public int Id { get; set; }

		[DisplayName("Sales Order")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public int SalesOrderId { get; set; }

		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public int WorkflowId { get; set; }

		[DisplayName("Tanggal Terima BA")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public Nullable<System.DateTime> InvoiceUploadDate { get; set; }

		[DisplayName("Tanggal Terima Invoice")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public Nullable<System.DateTime> InvoiceSentDate { get; set; }

		[DisplayName("Tanggal Terima Pembayaran")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public Nullable<System.DateTime> InvoiceAcceptDate { get; set; }

		[DisplayName("Tanggal Kliring Dokumen")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public Nullable<System.DateTime> ClearingDocDate { get; set; }

        [DisplayName("Tanggal Kirim (ke PLN)")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public Nullable<System.DateTime> AcceptedDateBy { get; set; }

        [DisplayName("Target Tanggal dikembalikan (oleh PLN)")]
        public Nullable<System.DateTime> TargetSendBackDate { get; set; }

        [DisplayName("Realisasi Tanggal dikembalikan (oleh PLN)")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public Nullable<System.DateTime> ActualSendBackDate { get; set; }

        [DisplayName("No Invoice")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public string InvoiceNo { get; set; }

		public string InvoiceSequence { get; set; }

		[DisplayName("Nama Ttd")]
		public string SignatureName { get; set; }

		[DisplayName("Jabatan Ttd")]
		public string SignaturePosition { get; set; }

		public virtual SalesOrderPresentationStub SalesOrder { get; set; }

		[DisplayName("File E-Faktur")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public virtual List<EFakturFileFormStub> EFakturFiles { get; set; }

		[DisplayName("File Invoice")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public virtual List<InvoiceFileFormStub> InvoiceFiles { get; set; }

        [DisplayName("Additional File")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public virtual List<AdditionalInvoiceFileFormStub> AdditionalFiles { get; set; }

		public InvoiceFormStub() : base()
        {
            InvoiceUploadDate = DateTime.Now;
		}

		public InvoiceFormStub(Business.Entities.Invoice dbObject) : base(dbObject)
		{
			SalesOrder = new SalesOrderPresentationStub(dbObject.SalesOrder);

			if (dbObject.InvoiceFiles.Count > 0)
				InvoiceFiles = ListMapper.MapList<InvoiceFile, InvoiceFileFormStub>(dbObject.InvoiceFiles);
			if (dbObject.EFakturFiles.Count > 0)
				EFakturFiles = ListMapper.MapList<EFakturFile, EFakturFileFormStub>(dbObject.EFakturFiles);

            if (dbObject.AdditionalInvoiceFiles.Count > 0)
                AdditionalFiles = ListMapper.MapList<AdditionalInvoiceFile, AdditionalInvoiceFileFormStub>(dbObject.AdditionalInvoiceFiles);
        }

		public InvoiceFormStub(Business.Entities.SalesOrder salesOrder) : base()
		{
			SalesOrderId = salesOrder.Id;
			SalesOrder = new SalesOrderPresentationStub(salesOrder);
		}

		public override void MapDbObject(Business.Entities.Invoice dbObject)
		{
			base.MapDbObject(dbObject);
		}

		protected override void Init()
		{
			//TODO: Set the init object here to be called in all constructor
		}
	}
}