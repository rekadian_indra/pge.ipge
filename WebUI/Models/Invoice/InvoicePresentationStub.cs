﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Models
{
    public class InvoicePresentationStub : InvoiceFormStub
    {
		public string SOType;
		public string Material;
		public string Period;
        public DateTime? PeriodValue;
        public string Unit;
		public string Status;
		public string InvoiceSequence { get; set; }
		public string CreatedBy { get; set; }

		public bool IsRevisable;
		public bool IsApprovable;
		public bool IsFirst;
		public bool IsLast;

		public InvoicePresentationStub() : base()
        {

		}

		public InvoicePresentationStub(Business.Entities.Invoice dbObject) : base(dbObject)
		{
			SOType = dbObject.SalesOrder.SOType;
			Material = dbObject.SalesOrder.PowerPlant.Material;
            CultureInfo cultureinfo = new CultureInfo("id-ID");
            PeriodValue = dbObject.SalesOrder.Periode;
            Period = dbObject.SalesOrder.Periode?.ToString(DisplayFormat.MonthYearDateFormat, cultureinfo);
			Unit = dbObject.SalesOrder.PowerPlant.ShipToPartyName;
			Status = dbObject.Workflow.Name;
			CreatedBy = dbObject.CreatedBy;

			WorkflowPresentationStub workflow = new WorkflowPresentationStub(dbObject.Workflow);
			IsRevisable = workflow.IsRevisable;
			IsApprovable = workflow.IsApprovable;
			IsFirst = workflow.IsFirst;
			IsLast = workflow.IsLast;
		}

		protected override void Init()
		{
			//TODO: Set the init object here to be called in all constructor
		}
	}
}   