﻿using Newtonsoft.Json;
using System.Text;
using WebUI;
using WebUI.Infrastructure;

namespace System.Web
{
    public static class HttpCookieExtension
    {
        private const int MaxSize = 4000;
        private const string ValueKey = "[@0#]";
        private const string ExpiredKey = "[@1#]";

        public static void SetValue<T>(this HttpCookie cookie, T value)
        {
            ThrowIfNull(cookie);

            //lib
            string formatedValue;

            //algorithm
            if (typeof(T).IsClass)
            {
                formatedValue = JsonConvert.SerializeObject(value);
            }
            else
            {
                formatedValue = value.ToString();
            }

            cookie.Value = formatedValue;

            //limit cookie value size
            ThrowIfGreaterSize(cookie);
        }

        public static void SetExpiredValue<T>(this HttpCookie cookie, T value, double? expiredTime = null)
        {
            ThrowIfNull(cookie);

            //lib
            string formatedValue;
            string formatedExpired;
            DateTime expired = DateTime.UtcNow;

            //algorithm
            if (!expiredTime.HasValue)
                expiredTime = Settings.GetValue<double>(Settings.AuthCookieExpired);

            expired = expired.AddMinutes(expiredTime.Value);

            if (typeof(T).IsClass)
            {
                formatedValue = JsonConvert.SerializeObject(value);
            }
            else
            {
                formatedValue = value.ToString();
            }

            formatedValue = $"{ValueKey}{formatedValue}{ValueKey}";
            formatedExpired = $"{ExpiredKey}{expired}{ExpiredKey}";

            cookie.Value = $"{formatedValue}{formatedExpired}";
            cookie.Expires = expired;

            //limit cookie value size
            ThrowIfGreaterSize(cookie);
        }

        public static T GetValue<T>(this HttpCookie cookie, bool isEncrypted = true)
        {
            ThrowIfNull(cookie);
            
            //lib
            string value;

            //algorithm
            if (isEncrypted)
            {
                value = cookie.DecryptValue();
            }
            else
            {
                value = cookie.Value;
            }

            //algorithm
            if (typeof(T).IsClass)
            {
                return JsonConvert.DeserializeObject<T>(value);
            }
            else
            {
                if (typeof(T).IsEnum)
                {
                    return (T)Enum.Parse(typeof(T), value);
                }

                return (T)Convert.ChangeType(value, typeof(T));
            }
        }

        public static T GetExpiredValue<T>(this HttpCookie cookie, bool isEncrypted = true)
        {
            ThrowIfNull(cookie);
            
            //lib
            string value;

            //algorithm
            if (isEncrypted)
            {
                value = cookie.DecryptValue();
            }
            else
            {
                value = cookie.Value;
            }

            value = value.Between(ValueKey);

            if (typeof(T).IsClass)
            {
                return JsonConvert.DeserializeObject<T>(value);
            }
            else
            {
                if (typeof(T).IsEnum)
                {
                    return (T)Enum.Parse(typeof(T), value);
                }

                return (T)Convert.ChangeType(value, typeof(T));
            }
        }

        public static void EncryptValue(this HttpCookie cookie)
        {
            ThrowIfNull(cookie);

            //encrypt value
            cookie.Value = AesEncryption.Encrypt(cookie.Value);

            //limit cookie value size
            ThrowIfGreaterSize(cookie);
        }

        public static string DecryptValue(this HttpCookie cookie)
        {
            ThrowIfNull(cookie);

            return AesEncryption.Decrypt(cookie.Value);
        }

        public static bool HasExpired(this HttpCookie cookie, bool isEncrypted = true)
        {
            ThrowIfNull(cookie);

            string value;
            bool expired = false;

            if (isEncrypted)
            {
                value = AesEncryption.Decrypt(cookie.Value);
            }
            else
            {
                value = cookie.Value;
            }
            
            if (value.Contains(ValueKey) && value.Contains(ExpiredKey))
            {
                DateTime dateTime;

                value = value.Between(ExpiredKey);
                dateTime = ((object)value).ToDateTime();
                expired = dateTime < DateTime.UtcNow;
            }

            return expired;
        }

        private static void ThrowIfGreaterSize(HttpCookie cookie)
        {
            if (Encoding.UTF8.GetByteCount(cookie.Value) > MaxSize)
                throw new NotSupportedException();
        }

        private static void ThrowIfNull(HttpCookie cookie)
        {
            if (cookie == null)
                throw new ArgumentNullException();
        }
    }
}
