﻿using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Extension
{
    public abstract class WebViewPageExtensions : WebViewPage
    {
        public virtual new Principal User
        {
            get
            {
                return HttpContext.Current.User as Principal; ;
            }
        }

        public string DataSourceOption
        {
            get
            {
                return Context.Session[AppSession.DataSourceOption] != null ?
                       Context.Session[AppSession.DataSourceOption].ToString() : "null";
            }
        }
    }

    public abstract class WebViewPageExtensions<T> : WebViewPage<T>
    {
        public virtual new Principal User
        {
            get
            {
                return HttpContext.Current.User as Principal;
            }
        }

        public string DataSourceOption
        {
            get
            {
                return Context.Session[AppSession.DataSourceOption] != null ?
                       Context.Session[AppSession.DataSourceOption].ToString() : "null";
            }
        }
    }
}