﻿using System;

namespace WebUI.Extension
{
    public static class DateTimeExtensions
    {
        private const string FORMAT = "yyyy-MM-dd HH:mm:ss";

        public static string ToDbFormat(this DateTime dateTime)
        {
            return dateTime.ToString(FORMAT);
        }

        public static DateTime ToDbDateTime(this DateTime dateTime)
        {
            return Convert.ToDateTime(dateTime.ToDbFormat());
        }

        public static DateTime ParseExcelDate(this string s)
        {
            DateTime d;
            int i;
            string yyyy, mm, dd;
            const string F = "{0}-{1}-{2}";

            if (s.Contains("/") || s.Contains("-"))
            {
                DateTime.TryParse(s, out d);
                return d;
            }
            else
            {
                if (s.Length != 8)
                    return DateTime.FromOADate(double.Parse(s));
                else
                {
                    int.TryParse(s, out i);
                    if (i == 0)
                        return DateTime.MinValue;

                    yyyy = s.Substring(0, 4);
                    int.TryParse(yyyy, out i);
                    if (i <= 1900 || i >= 2100)
                        throw new NotSupportedException("Not valid string date format");
                    else
                    {
                        mm = s.Substring(4, 2);
                        int.TryParse(mm, out i);
                        if (i <= 31)
                        {
                            if (i > 12)
                            {
                                dd = mm;
                                mm = s.Substring(6, 2);
                                int.TryParse(mm, out i);
                                if (i > 12)
                                    throw new NotSupportedException("Not valid string date format");
                            }
                            else
                            {
                                dd = s.Substring(6, 2);
                                int.TryParse(dd, out i);
                                if (i > 31)
                                    throw new NotSupportedException("Not valid string date format");
                            }
                        }
                        else
                            throw new NotSupportedException("Not valid string date format");

                        if (!Valid(yyyy, mm, dd))
                            throw new NotSupportedException("Not valid string date format");

                        d = DateTime.Parse(string.Format(F, yyyy, mm, dd));

                        return d;
                    }
                }
            }
        }

        public static TimeSpan ParseExcelTime(this string s)
        {
            TimeSpan d = new TimeSpan(0);                                    

            if (s.Contains(":"))            
                TimeSpan.TryParse(s, out d);                                            
            else
                throw new NotSupportedException("Not valid string time format");

            return d;
        }

        private static bool Valid(string yyyy, string mm, string dd)
        {
            bool b = false;
            int i = int.Parse(dd);
            int y = int.Parse(yyyy);
            
            switch (mm)
            {
                case "01":
                    if (i <= 31)
                        b = true;
                    break;
                case "02":
                    if (DateTime.IsLeapYear(y))
                    {
                        if (i <= 29)
                            b = true;
                    }
                    else
                    {
                        if (i <= 28)
                            b = true;
                    }
                    break;
                case "03":
                    if (i <= 31)
                        b = true;
                    break;
                case "04":
                    if (i <= 30)
                        b = true;
                    break;
                case "05":
                    if (i <= 31)
                        b = true;
                    break;
                case "06":
                    if (i <= 30)
                        b = true;
                    break;
                case "07":
                    if (i <= 31)
                        b = true;
                    break;
                case "08":
                    if (i <= 31)
                        b = true;
                    break;
                case "09":
                    if (i <= 30)
                        b = true;
                    break;
                case "10":
                    if (i <= 31)
                        b = true;
                    break;
                case "11":
                    if (i <= 30)
                        b = true;
                    break;
                case "12":
                    if (i <= 31)
                        b = true;
                    break;
                default:
                    return false;
            }
            return b;
        }
    }
}
