﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using WebUI.Infrastructure.Abstract;
using WebUI.Models;

namespace WebUI.Infrastructure.Concrete
{
    public class CustomPrincipal : ICustomPrincipal
    {
        public IIdentity Identity { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public List<string> Roles { get; set; }
        public List<ModuleAction> Modules { get; set; }
        public UserProfile UserProfile { get; set; }
        public int? AreaId { get; set; }
        public string AreaName { get; set; }
        public string AreaCode { get; set; }
        public CustomPrincipal() { }

        public CustomPrincipal(string username)
        {
            Identity = new GenericIdentity(username);
        }

        public bool IsInRole(string role)
        {
            return Identity != null && Identity.IsAuthenticated &&
              !string.IsNullOrWhiteSpace(role) && System.Web.Security.Roles.IsUserInRole(Identity.Name, role);
        }

        public bool HasAccess(params string[] moduleNames)
        {
            //kamus
            bool isHas = false;

            //algoritma
            if (Modules != null)
            {
                isHas = Modules.Where(m => moduleNames.Contains(m.ModuleName)).Count() > 0;
            }

            return isHas;
        }

        public bool HasRoles(params string[] roles)
        {
            //kamus
            bool isHas = false;

            //algoritma
            if (Roles != null)
            {
                isHas = Roles.Any(m => roles.Contains(m));
            }

            return isHas;
        }

    }

    public class CustomPrincipalSerializeModel
    {
        public List<ModuleAction> Modules { get; set; }
        public List<string> Roles { get; set; }
        public UserProfile UserProfile { get; set; }

       
    }
}