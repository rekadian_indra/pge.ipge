﻿namespace WebUI.Infrastructure
{
    public static class ControllerSite
    {
        //reuseable controller
        public const string Home = "Home";
        public const string Dashboard = "Dashboard";
        public const string SalesOrderM = "SalesOrderM";
        public const string SalesOrder = "SalesOrder";
        public const string Invoice = "Invoice";
        public const string Report = "Report";

		//default controller
		public const string Authentication = "Authentication";
        public const string User = "User";
        public const string Role = "Role";
        public const string Module = "Module";

        //app controller
        public const string Data = "Data";
        public const string Cluster = "Cluster";
        public const string Well = "Well";
        public const string InputData = "InputData";
        public const string InputPlan = "InputPlan";
        public const string ReliabilityPlan = "ReliabilityPlan";
		public const string Whitelist = "Whitelist";
        public const string DataHistory = "DataHistory";
        public const string ShipToParty = "ShipToParty";
        public const string MaintainPriceElectric = "MaintainPriceElectric";
        public const string MaintainPriceSteam = "MaintainPriceSteam";
        public const string DownloadFormMiro = "DownloadFormMiro";
        public const string WellHistory = "WellHistory";
        public const string Monitoring = "Monitoring";
        public const string ReservoirReport = "ReservoirReport";
    }
}