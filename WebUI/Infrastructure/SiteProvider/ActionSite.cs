﻿namespace WebUI.Infrastructure
{
    public static class ActionSite
    {
        //reuseable action
        public const string Index = "Index";
        public const string Add = "Add";
        public const string Login = "Login";
        public const string Create = "Create";
        public const string Edit = "Edit";
        public const string Delete = "Delete";
        public const string Binding = "Binding";
        public const string Detail = "Detail";
		public const string Approve = "Approve";
		public const string Revise = "Revise";
		public const string BindingTaskReport = "BindingTaskReport";
        public const string BindingPltp = "BindingPltp";
        public const string BindingDkp = "BindingDkp";
        public const string BindingProduction = "BindingProduction";
        public const string BindingReinjection = "BindingReinjection";
        public const string BindingMonitoring = "BindingMonitoring";
        public const string BindingSeparator = "BindingSeparator";
        public const string BindingInterface = "BindingInterface";
        public const string BindingActualPlan = "BindingActualPlan";
        public const string BindingArea = "BindingArea";
        public const string BindingPowerPlant = "BindingPowerPlant";
		public const string BindingPltpHistory = "BindingPltpHistory";
        public const string BindingDkpHistory = "BindingDkpHistory";
        public const string BindingProductionHistory = "BindingProductionHistory";
        public const string BindingReinjectionHistory = "BindingReinjectionHistory";
        public const string BindingMonitoringHistory = "BindingMonitoringHistory";
        public const string BindingSeparatorHistory = "BindingSeparatorHistory";
        public const string BindingInterfaceScrubberHistory = "BindingInterfaceScrubberHistory";
        //public const string BindingDocumentSalesOrder = "BindingDocumentSalesOrder";
        //public const string BindingDocumentInvoice = "BindingDocumentInvoice";
        public const string BindingDocumentFinancialDashboard = "BindingDocumentFinancialDashboard";

		public const string BindingOption = "BindingOption";
        public const string PutOption = "PutOption";

        public const string Financial = "Financial";
        public const string Executive = "Executive";

        //app action
        public const string Download = "Download";
        public const string Upload = "Upload";
        public const string GetTemplate = "GetTemplate";

        public const string EdiTaskReport = "EdiTaskReport";

        public const string CreatePltp = "CreatePltp";
        public const string CreateDkp = "CreateDkp";
        public const string CreateProduction = "CreateProduction";
        public const string CreateReinjection = "CreateReinjection";
        public const string CreateMonitoring = "CreateMonitoring";
        public const string CreateSeparator = "CreateSeparator";
        public const string CreateInterface = "CreateInterface";
        public const string CreateActualPlan = "CreateActualPlan";

        public const string EditPltp = "EditPltp";
        public const string EditDkp = "EditDkp";
        public const string EditProduction = "EditProduction";
        public const string EditReinjection = "EditReinjection";
        public const string EditMonitoring = "EditMonitoring";
        public const string EditSeparator = "EditSeparator";
        public const string EditInterface = "EditInterface";
        public const string EditActualPlan = "EditActualPlan";

        public const string DeleteTaskReport = "DeleteTaskReport";
        public const string DeletePltp = "DeletePltp";
        public const string DeleteDkp = "DeleteDkp";
        public const string DeleteProduction = "DeleteProduction";
        public const string DeleteReinjection = "DeleteReinjection";
        public const string DeleteMonitoring = "DeleteMonitoring";
        public const string DeleteSeparator = "DeleteSeparator";
        public const string DeleteInterface = "DeleteInterface";
        public const string DeleteActualPlan = "DeleteActualPlan";
        public const string DeleteBastFile = "DeleteBastFile";
        public const string DeleteScreenshotSap = "ScreenshotSap";
        public const string DeleteBastFileSigned = "DeleteBastFileSigned";

		public const string PowerPlantHistory = "PowerPlantHistory";
        public const string DkpHistory = "DkpHistory";
        public const string WellHistory = "WellHistory";
        public const string SeparatorHistory = "SeparatorHistory";
        public const string InterfaceScrubberHistory = "InterfaceScrubberHistory";

        public const string PowerPlantCodeExists = "PowerPlantCodeExists";
        public const string PowerPlantNameExists = "PowerPlantNameExists";

		// sales order
		public const string BindingApproval = "BindingApproval";
		public const string BindingBastFiles = "BindingBastFiles";
        public const string BindingScreenshotSaps = "BindingScreenshotSaps";
        public const string BindingBastFileSigneds = "BindingBastFileSigneds";
		public const string BindingDetailProduction = "BindingDetailProduction";
		public const string LoadCreateZPGE = "LoadCreateZPGE";
		public const string LoadCreateZTS = "LoadCreateZTS";
		public const string CreateSOZPGE = "CreateSOZPGE";
		public const string CreateSOZTS = "CreateSOZTS";
		public const string CreateBastFileSigned = "CreateBastFileSigned";
		public const string EditSOZPGE = "EditSOZPGE";
		public const string EditSOZTS = "EditSOZTS";
		public const string ApproveSalesOrder = "ApproveSalesOrder";
		public const string ApproveSalesOrderZTS = "ApproveSalesOrderZTS";
		public const string ReviseSalesOrder = "ReviseSalesOrder";
		public const string ReviseSalesOrderZTS = "ReviseSalesOrderZTS";
		public const string DownloadBastTemplate = "DownloadBastTemplate";
		public const string DownloadSAP = "DownloadSAP";
        public const string HistorySO = "HistorySO";
        public const string BindingHistorySO = "BindingHistorySO";

        // maintain harga
        public const string Check = "Check";
		public const string BackToIndex = "BackToIndex";
		public const string CheckSOType = "CheckSOType";

		// input plan
		public const string BindingYear = "BindingYear";
		public const string BindingPlanSummary = "BindingPlanSummary";
		public const string BindingProductionPlan = "BindingProductionPlan";
		public const string DeletePlan = "DeletePlan";

		// reliability plan
		public const string BindingReliabilityPlan = "BindingReliabilityPlan";

		// invoice
		public const string Choose = "Choose";
		public const string CreateEFakturFile = "CreateEFakturFile";
		public const string CreateInvoiceFile = "CreateInvoiceFile";
        public const string CreateAdditionalInvoiceFile = "CreateAdditionalInvoiceFile";
        public const string DeleteEFakturFile = "DeleteEFakturFile";
		public const string DeleteInvoiceFile = "DeleteInvoiceFile";
        public const string DeleteAdditionalInvoiceFile = "DeleteAdditionalInvoiceFile";
        public const string BindingSalesOrders = "BindingSalesOrders";
		public const string BindingEFakturFiles = "BindingEFakturFiles";
		public const string BindingInvoiceFiles = "BindingInvoiceFiles";
        public const string BindingAdditionalInvoiceFiles = "BindingAdditionalInvoiceFiles";
        public const string DownloadCoverLetter = "DownloadCoverLetter";
		public const string GenerateCoverLetter = "GenerateCoverLetter";

		// user
		public const string CreateSignatureFile = "CreateSignatureFile";
		public const string DeleteSignatureFile = "DeleteSignatureFile";
		public const string GetUserInfo = "GetUserInfo";

		// report
		public const string Sales = "Sales";
		public const string DailyMonthly = "DailyMonthly";
		public const string Reliability = "Reliability";
		public const string Management = "Management";
		public const string Geoscience = "Geoscience";
        public const string BonusProduction = "BonusProduction";
        public const string KpiCollection = "KpiCollection";
        public const string BindingOwnOperateTransmition = "BindingOwnOperateTransmition";
        public const string BindingSummaryOwnOperateTransmition = "BindingSummaryOwnOperateTransmition";

        //default action
        public const string AntiForgery = "AntiForgery";
        public const string Http404 = "Http404";
        public const string Http403 = "Http403";
        public const string Http401 = "Http401";
        public const string AssignRoles = "AssignRoles";
        public const string AssignModules = "AssignModules";
        public const string RevokeRoles = "RevokeRoles";
        public const string RevokeModules = "RevokeModules";
        public const string BindingRoles = "BindingRoles";
        public const string BindingModules = "BindingModules";
        public const string UserNameExists = "UserNameExists";
        public const string EmailExists = "EmailExists";
        public const string ModuleNameExists = "ModuleNameExists";
        public const string RoleNameExists = "RoleNameExists";
        public const string EdiTaskReportSelected = "EdiTaskReportSelected";

        //Form Miro
        public const string DownloadZts = "DownloadZts";
        public const string DownloadZpge = "DownloadZpge";
        public const string BindingDataPopupZTS = "BindingDataPopupZTS";
        public const string CreateFormMiro = "CreateFormMiro";
        public const string History = "History";

        public const string BindingBonusProductionSteam = "BindingBonusProductionSteam";
        public const string BindingBonusProductionElectric = "BindingBonusProductionElectric";
        public const string BindingBonusProductionSummary = "BindingBonusProductionSummary";
        public const string BindingKpi = "BindingKpi";
        public const string BindingKpiTransmition = "BindingKpiTransmition";

        public const string CompareValue = "CompareValue";
        public const string UploadMultiple = "UploadMultiple";
    }
}