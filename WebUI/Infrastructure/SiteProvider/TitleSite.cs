﻿namespace WebUI.Infrastructure
{
    public static class TitleSite
    {
        //default title
        public const string Dashboard = "Dashboard";
        public const string UserManagement = "User Management";
        public const string Create = "Tambah";
        public const string Add = "Tambah";
        public const string Edit = "Edit";
        public const string Breadcrumb = "Breadcrumb";
        public const string User = "User";
        public const string Role = "Role";

        //app title
        public const string Crew = "Crew";
        public const string Whitelist = "Whitelist";

        public const string SalesOrder = "Sales Order";
        public const string CreateSalesOrderZPGE = "Create Sales Order ZPGE";
        public const string CreateSalesOrderZTS = "Create Sales Order ZTS";
		public const string EditSalesOrderZPGE = "Edit Sales Order ZPGE";
		public const string EditSalesOrderZTS = "Edit Sales Order ZTS";
        public const string HistorySO = "History Sales Order";

        public const string Invoice = "Invoice";

        public const string EEData = "Reservoir Data";
        public const string ShipToParty = "Ship To Party";

        public const string MaintainHargaListrik = "Maintain Harga Listrik";
        public const string MaintainHargaUap = "Maintain Harga Uap";

        public const string InputPlan = "List Plan";
        public const string ReliabilityPlan = "Reliability Plan";

		public const string ReportSales = "Sales Report";
		public const string ReportManagement = "Operation Management Report";
		public const string ReportDailyMonthly = "Operation Daily / Monthly Report";
		public const string ReportReliability = "Operation Reliability Report";
		public const string ReportGeoscience = "P&T Report";
        public const string ReportBonusProduction = "Bonus Production Report";
        public const string ReportKpi = "Invoice & Collect Periods Report";

        public const string Cluster = "Cluster";
        public const string Well = "Well";
        public const string WellHistory = "Well History";

        public const string FormMiro = "Download Form Miro";

        public const string Monitoring = "Monitoring";

        public const string ReservoirReport = "Reservoir Report";

        public const string Executive = "Executive";
        public const string Operation = "Operation";
    }
}