﻿namespace WebUI.Infrastructure
{
    public static class AreaSite
    {
        public const string UserManagement = "UserManagement";
        public const string DataMaster = "DataMaster";
        public const string OperationData = "OperationData";
        public const string SalesOrder = "SalesOrder";
        public const string Invoice = "Invoice";
        public const string Reporting = "Reporting";
	}
}