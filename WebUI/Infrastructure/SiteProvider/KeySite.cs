﻿namespace WebUI.Infrastructure
{
    public static class KeySite
    {
        //default key
        public const string Dashboard = "Dashboard";
        public const string DashboardUserManagement = "DashboardUserManagement";

        //app key
        public const string IndexEEData = "IndexEEData";
        public const string AddEEData = "AddEEData";
        public const string EditEEData = "EditEEData";

        public const string CreateCrew = "CreateCrew";
        public const string EditCrew = "EditCrew";

        public const string IndexWhitelist = "IndexWhitelist";
        public const string CreateWhitelist = "CreateWhitelist";
        public const string EditWhitelist = "EditWhitelist";

        public const string IndexShipToParty = "IndexShipToParty";
        public const string CreateShipToParty = "CreateShipToParty";
        public const string EditShipToParty = "EditShipToParty";

		public const string IndexSalesOrder = "IndexSalesOrder";
		public const string CreateSalesOrder = "CreateSalesOrder";
		public const string CreateSalesOrderZPGE = "CreateSalesOrderZPGE";
		public const string EditSalesOrderZPGE = "EditSalesOrderZPGE";
		public const string CreateSalesOrderZTS = "CreateSalesOrderZTS";
		public const string EditSalesOrderZTS = "EditSalesOrderZTS";
        public const string HistorySalesOrder = "HistorySalesOrder";

        public const string IndexMaintainHargaListrik = "IndexMaintainHargaListrik";
		public const string CreateMaintainHargaListrik = "CreateMaintainHargaListrik";
		public const string EditMaintainHargaListrik = "EditMaintainHargaListrik";

		public const string IndexMaintainHargaUap = "IndexMaintainHargaUap";
		public const string CreateMaintainHargaUap = "CreateMaintainHargaUap";
		public const string EditMaintainHargaUap = "EditMaintainHargaUap";

		public const string IndexInputPlan = "IndexInputPlan";
		public const string CreateInputPlan = "CreateInputPlan";
		public const string EditInputPlan = "EditInputPlan";

		public const string IndexReliabilityPlan = "IndexReliabilityPlan";
		public const string CreateReliabilityPlan = "CreateReliabilityPlan";
		public const string EditReliabilityPlan = "EditReliabilityPlan";

		public const string IndexInvoice = "IndexInvoice";
		public const string ChooseInvoice = "ChooseInvoice";
		public const string CreateInvoice = "CreateInvoice";
		public const string EditInvoice = "EditInvoice";
		public const string CreateInvoiceZPGE = "CreateInvoiceZPGE";
		public const string EditInvoiceZPGE = "EditInvoiceZPGE";
		public const string CreateInvoiceZTS = "CreateInvoiceZTS";
		public const string EditInvoiceZTS = "EditInvoiceZTS";

		public const string ReportSales = "ReportSales";
		public const string ReportDailyMonthly = "ReportDailyMonthly";
		public const string ReportReliability = "ReportReliability";
		public const string ReportManagement = "ReportManagement";
		public const string ReportGeoscience = "ReportGeoscience";
        public const string ReportBonusProduction = "ReportBonusProduction";
        public const string ReportKpi = "ReportKpi";

        public const string IndexUser = "IndexUser";
        public const string IndexRole = "IndexRole";

        public const string IndexCluster = "IndexCluster";
        public const string CreateCluster = "CreateCluster";
        public const string EditCluster = "EditCluster";

        public const string IndexWell = "IndexWell";
        public const string CreateWell = "CreateWell";
        public const string EditWell = "EditWell";

        public const string IndexWellHistory = "IndexWellHistory";
        public const string CreateWellHistory = "CreateWellHistory";
        public const string EditWellHistory = "EditWellHistory";

        public const string IndexFormMiro = "IndexFormMiro";

        public const string IndexMonitoring = "IndexMonitoring";

        public const string IndexReservoirReport = "IndexReservoirReport";
    }
}