﻿namespace WebUI.Infrastructure
{
    public static class PageSite
    {
        //Reusable
        public const string _Form = "_Form";
        public const string Detail = "Detail";

        //Input Data        
        public const string DownloadData = "DownloadData";
        public const string TaskReport = "_FormTaskReport";
        public const string Pltp = "FormPltp";
        public const string Dkp = "FormDkp";
        public const string Production = "FormProduction";
        public const string Reinjection = "FormReinjection";
        public const string Monitoring = "FormMonitoring";
        public const string Separator = "FormSeparator";
        public const string InterfaceScrubber = "FormInterfaceScrubber";
        public const string ActualPlan = "FormActualPlan";

        //Data history
        public const string PltpHistory = "FormPltpHistory";
        public const string DkpHistory = "FormDkpHistory";
        public const string WellHistory = "FormWellHistory";
        public const string SeparatorHistory = "FormSeparatorHistory";
        public const string InterfaceHistory = "FormInterfaceScrubberHistory";
    }
}