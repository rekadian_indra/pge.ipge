﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace WebUI.Infrastructure
{
    public static class AesEncryption
    {
        private const string DefaultCipher = ")>%r3K4d!a%<(";

        private static int SaltLength
        {
            get
            {
                return 8;
            }
        }

        private static byte[] RandomBytes
        {
            get
            {
                byte[] bytes = new byte[SaltLength];
                RandomNumberGenerator.Create().GetBytes(bytes);

                return bytes;
            }
        }

        public static string Encrypt(string text)
        {
            return Encrypt(text, DefaultCipher);
        }

        public static string Decrypt(string text)
        {
            return Decrypt(text, DefaultCipher);
        }

        public static string Encrypt(string text, string cipher)
        {
            string result;
            byte[] resultBytes;
            byte[] textBytes = Encoding.UTF8.GetBytes(text);
            byte[] cipherBytes = Encoding.UTF8.GetBytes(cipher);
            byte[] saltBytes = RandomBytes;
            byte[] encryptedBytes = new byte[saltBytes.Length + textBytes.Length];

            // Combine Salt + Text
            for (int i = 0; i < saltBytes.Length; i++)
            {
                encryptedBytes[i] = saltBytes[i];
            }
            for (int i = 0; i < textBytes.Length; i++)
            {
                encryptedBytes[i + saltBytes.Length] = textBytes[i];
            }

            // Hash the password with SHA256
            cipherBytes = SHA256.Create().ComputeHash(cipherBytes);

            resultBytes = Encrypt(encryptedBytes, cipherBytes);

            result = Convert.ToBase64String(resultBytes);

            return result;
        }

        public static string Decrypt(string text, string cipher)
        {
            string result;
            byte[] resultBytes;
            byte[] decryptedBytes;
            byte[] textBytes = Convert.FromBase64String(text);
            byte[] cipherBytes = Encoding.UTF8.GetBytes(cipher);

            // Hash the password with SHA256
            cipherBytes = SHA256.Create().ComputeHash(cipherBytes);

            decryptedBytes = Decrypt(textBytes, cipherBytes);

            // Remove salt
            resultBytes = new byte[decryptedBytes.Length - SaltLength];

            for (int i = 0; i < resultBytes.Length; i++)
            {
                resultBytes[i] = decryptedBytes[i + SaltLength];
            }

            result = Encoding.UTF8.GetString(resultBytes);

            return result;
        }

        private static byte[] Encrypt(byte[] encryptedBytes, byte[] cipherBytes)
        {
            byte[] result = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged aes = new RijndaelManaged())
                {
                    aes.KeySize = 256;
                    aes.BlockSize = 128;

                    Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(cipherBytes, saltBytes, 1000);
                    aes.Key = key.GetBytes(aes.KeySize / 8);
                    aes.IV = key.GetBytes(aes.BlockSize / 8);

                    aes.Mode = CipherMode.CBC;

                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(encryptedBytes, 0, encryptedBytes.Length);
                        cs.Close();
                    }

                    result = ms.ToArray();
                }
            }

            return result;
        }

        private static byte[] Decrypt(byte[] decryptedBytes, byte[] cipherBytes)
        {
            byte[] result = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged aes = new RijndaelManaged())
                {
                    aes.KeySize = 256;
                    aes.BlockSize = 128;

                    Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(cipherBytes, saltBytes, 1000);
                    aes.Key = key.GetBytes(aes.KeySize / 8);
                    aes.IV = key.GetBytes(aes.BlockSize / 8);

                    aes.Mode = CipherMode.CBC;

                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(decryptedBytes, 0, decryptedBytes.Length);
                        cs.Close();
                    }

                    result = ms.ToArray();
                }
            }

            return result;
        }
    }
}