﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace WebUI.Infrastructure.Validation
{
    public class DateGreaterAttribute : ValidationAttribute
    {
        public string SmallDate { get; private set; }
        private string SmallDateDisplayName { get; set; }

        public DateGreaterAttribute(string smallDate, params string[] propertyNames)
        {
            SmallDate = smallDate;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                DateTime bigDate;
                DateTime? smallDate = null;

                //algoritma
                if (value.GetType() == typeof(string))
                {
                    bigDate = DateTime.Parse((string)value);
                }
                else
                {
                    bigDate = (DateTime)value;
                }

                //small date
                var basePropertyInfo = validationContext.ObjectType.GetProperty(SmallDate);
                if (basePropertyInfo.PropertyType == typeof(string))
                {
                    string smallDateString = (string)basePropertyInfo.GetValue(validationContext.ObjectInstance, null);
                    if (smallDateString != null && smallDateString != "")
                        smallDate = DateTime.Parse(smallDateString);
                }
                else if ((basePropertyInfo.PropertyType == typeof(DateTime)) || (basePropertyInfo.PropertyType == typeof(Nullable<DateTime>)))
                {
                    smallDate = (DateTime)basePropertyInfo.GetValue(validationContext.ObjectInstance, null);
                }

                if (smallDate != null)
                {

                    if (bigDate < smallDate)
                    {
                        var displayAttribute = basePropertyInfo.GetCustomAttributes(typeof(System.ComponentModel.DisplayNameAttribute), true).
                            FirstOrDefault() as System.ComponentModel.DisplayNameAttribute;
                        if (displayAttribute != null)
                            SmallDateDisplayName = displayAttribute.DisplayName;

                        return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                    }
                }
            }
            return null;
        }

        public override string FormatErrorMessage(string name)
        {
            string displayName;
            if (SmallDateDisplayName == null)
                displayName = SmallDate;
            else
                displayName = SmallDateDisplayName;

            return string.Format(Resources.AppGlobalError.DateGreater, name, displayName);
        }
    }
}