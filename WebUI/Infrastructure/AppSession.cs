﻿namespace WebUI.Infrastructure
{
    public class AppSession
    {
        //public const string UserLogin = "USER_LOGIN";
        public const string DataSourceOption = "DATA_SOURCE_OPTION";
        public const string TaskReport = "TASK_REPORT_ID";
        public const string TargetTaskDetail = "TARGET_TASK_DETAIL";

        //public const string NavigationOption = "NAVIGATION_OPTION";
        //public const string OffsetHour = "OFFSET_HOUR";
        //public const string Model = "MODEL";
        //public const string ModelState = "_MODELSTATE";

        public static string UserLogin
        {
            get
            {
                return "USER_LOGIN";
            }
        }
    }
}