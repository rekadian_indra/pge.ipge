﻿using Business.Abstract;
using Business.Concrete;
using LogAction.Abstract;
using LogAction.Concrete;
using Ninject.Modules;
using WebUI.Infrastructure.Abstract;
using WebUI.Infrastructure.Concrete;

namespace WebUI.Infrastructure
{
    public class AppModule : NinjectModule
    {
        public static NinjectModule[] Modules
        {
            get
            {
                return new[] { new AppModule() };
            }
        }

        public override void Load()
        {
            //user management
            Kernel.Bind<IUserRepository>().To<EFUserRepository>();
            Kernel.Bind<IRoleRepository>().To<EFRoleRepository>();
            Kernel.Bind<IModuleRepository>().To<EFModuleRepository>();
            Kernel.Bind<IActionRepository>().To<EFActionRepository>();
            Kernel.Bind<IModulesInRoleRepository>().To<EFModulesInRoleRepository>();
            Kernel.Bind<IViewUserAreaRepository>().To<EFViewUserAreaRepository>();
            //others
            Kernel.Bind<ILogRepository>().To<EFLogRepository>();
            Kernel.Bind<IAuthProvider>().To<DummyAuthProvider>();

            //app
            Kernel.Bind<IViewExplorationRepository>().To<EFViewExplorationRepository>();
            Kernel.Bind<IClusterRepository>().To<EFClusterRepository>();
            Kernel.Bind<IMembershipRepository>().To<EFMembershipRepository>();
            Kernel.Bind<ICompanyRepository>().To<EFCompanyRepository>();
            Kernel.Bind<IAreaRepository>().To<EFAreaRepository>();
            Kernel.Bind<IPowerPlantRepository>().To<EFPowerPlantRepository>();
            Kernel.Bind<IPltpRepository>().To<EFPltpRepository>();
            Kernel.Bind<IDkpRepository>().To<EFDkpREpository>();
            Kernel.Bind<ITaskReportRepository>().To<EFTaskReportRepository>();
            Kernel.Bind<IWellRepository>().To<EFWellRepository>();
            Kernel.Bind<IMstSeparatorRepository>().To<EFMstSeparatorRepository>();
            Kernel.Bind<IInterfacePointRepository>().To<EFInterfacePointRepository>();
            Kernel.Bind<IScrubberRepository>().To<EFScrubberRepository>();
            Kernel.Bind<IProductionRepository>().To<EFProductionRepository>();
            Kernel.Bind<IReInjectionRepository>().To<EFReInjectionRepository>();
            Kernel.Bind<IMonitoringRepository>().To<EFMonitoringRepository>();
            Kernel.Bind<ISeparatorRepository>().To<EFSeparatorRepository>();
            Kernel.Bind<IInterfaceScrubberRepository>().To<EFInterfaceScrubberRepository>();
            Kernel.Bind<IActualPlanningRepository>().To<EFActualPlanningRepository>();
            Kernel.Bind<IExplorationRepository>().To<EFExplorationRepository>();
            Kernel.Bind<IExplorationDetailRepository>().To<EFExplorationDetailRepository>();
            Kernel.Bind<IWorkflowRepository>().To<EFWorkflowRepository>();
            Kernel.Bind<ISoldToPartyRepository>().To<EFSoldToPartyRepository>();
            Kernel.Bind<ISalesOrderRepository>().To<EFSalesOrderRepository>();
            Kernel.Bind<IBastFileRepository>().To<EFBastFileRepository>();
            Kernel.Bind<IBastFileSignedRepository>().To<EFBastFileSignedRepository>();
            Kernel.Bind<IScreenshotSapRepository>().To<EFScreenshotSapRepository>();
            Kernel.Bind<IPpiRepository>().To<EFPpiRepository>();
            Kernel.Bind<ISteamPowerPlantPriceRepository>().To<EFSteamPowerPlantPriceRepository>();
            Kernel.Bind<IProductionPlanRepository>().To<EFProductionPlanRepository>();
            Kernel.Bind<IProductionPlanDetailRepository>().To<EFProductionPlanDetailRepository>();
            Kernel.Bind<IProductionPlanDailyRepository>().To<EFProductionPlanDailyRepository>();
			Kernel.Bind<ISalesOrderApprovalRepository>().To<EFSalesOrderApprovalRepository>();
            Kernel.Bind<IPpiApprovalRepository>().To<EFPpiApprovalRepository>();
            Kernel.Bind<ISteamPowerPlantPriceApprovalRepository>().To<EFSteamPowerPlantPriceApprovalRepository>();
			Kernel.Bind<IInvoiceRepository>().To<EFInvoiceRepository>();
			Kernel.Bind<IInvoiceFileRepository>().To<EFInvoiceFileRepository>();
            Kernel.Bind<IAdditionalInvoiceFileRepository>().To<EFAdditionalInvoiceFileRepository>();
            Kernel.Bind<IEFakturFileRepository>().To<EFEFakturFileRepository>();
			Kernel.Bind<IInvoiceApprovalRepository>().To<EFInvoiceApprovalRepository>();
            Kernel.Bind<IViewProgressEnergyRepository>().To<EFViewProgressEnergyRepository>();
            Kernel.Bind<IViewProgressEnergyCumulativeRepository>().To<EFViewProgressEnergyCumulativeRepository>();
            Kernel.Bind<IViewSalesOrderApprovalRepository>().To<EFViewSalesOrderApprovalRepository>();
            Kernel.Bind<IAutoNumberRepository>().To<EFAutoNumberRepository>();
            Kernel.Bind<IReliabilityPlanRepository>().To<EFReliabilityPlanRepository>();

			Kernel.Bind<IWellHistoryRepository>().To<EFWellHistoryRepository>();
            Kernel.Bind<IViewWellRepository>().To<EFViewWellRepository>();

            Kernel.Bind<IViewPltpRepository>().To<EFViewPltpRepository>();
            Kernel.Bind<IViewProductionPlanDetailRepository>().To<EFViewProductionPlanDetailRepository>();
            Kernel.Bind<IViewPpiRepository>().To<EFViewPpiRepository>();

            Kernel.Bind<IViewDkpRepository>().To<EFViewDkpRepository>();
            Kernel.Bind<IViewReliabilityPlanRepository>().To<EFViewReliabilityPlanRepository>();
            Kernel.Bind<IViewReliabilityPlanYearlyRepository>().To<EFViewReliabilityPlanYearlyRepository>();
			Kernel.Bind<IViewReliabilityActualRepository>().To<EFViewReliabilityActualRepository>();

            Kernel.Bind<IViewInvoicingSalesOrderRepository>().To<EFViewInvoicingSalesOrderRepository>();
            Kernel.Bind<IViewInvoicingKpiRepository>().To<EFViewInvoicingKpiRepository>();
            Kernel.Bind<IViewCapacityFactorRepository>().To<EFViewCapacityFactorRepository>();
            Kernel.Bind<IViewInvoicingRepository>().To<EFViewInvoicingRepository>();
            Kernel.Bind<IViewSalesOrderFinanceRepository>().To<EFViewSalesOrderFinanceRepository>();
            Kernel.Bind<IViewInvoiceRepository>().To<EFViewInvoiceRepository>();
        }
    }
}
