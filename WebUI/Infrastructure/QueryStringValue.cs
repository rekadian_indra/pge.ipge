﻿namespace WebUI.Infrastructure
{
    public static class QueryStringValue
    {
        public const string Area = "areaId";
        public const string Cluster = "clusterId";
        public const string PowerPlant = "powerPlantId";
        public const string Well = "wellId";
    }
}