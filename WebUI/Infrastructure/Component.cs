﻿namespace WebUI.Infrastructure
{
    public static class Component
    {
        public const string OptionLabel = "Pilih...";
        public const string OptionLabelAreaAll = "Semua Area...";
        public const string OptionLabelPowerPlantAll = "Semua Power Plant...";
        public const string OptionLabelClusterAll = "Semua Cluster...";
        public const string OptionLabelWellAll = "Semua Well...";

        public static class Grid
        {
            public static class Command
            {
                public const string Edit = "Edit";
                public const string Delete = "Hapus";
                public const string Detail = "Detail";
                public const string Approve = "Approve";
                public const string History = "History";
                public const string Role = "Assign Role";
                public const string Download = "Download";
            }
        }
    }
}