﻿using System;
using System.Collections.Generic;

namespace WebUI.Infrastructure
{
    public class DateHelper
    {
        /**
         * mengambil date paling kecil
         * @param dates mungkin list kosong
         * @return null kalau dates adalah list kosong
         */
        public DateTime? MinDate(List<DateTime> dates)
        {
            DateTime? min = null;

            foreach (DateTime date in dates)
            {
                if (min == null)
                    min = date;
                else
                {
                    if (date < min)
                        min = date;
                }
            }

            return min;
        }

        /**
         * mengambil date paling besar
         * @param dates mungkin list kosong
         * @return null kalau dates adalah list kosong
         */
        public DateTime? MaxDate(List<DateTime> dates)
        {
            DateTime? max = null;

            foreach (DateTime date in dates)
            {
                if (max == null)
                    max = date;
                else
                {
                    if (date > max)
                        max = date;
                }
            }

            return max;
        }

		public static int MonthToQuarter(int month)
		{
			return (int)Math.Floor((decimal)(month - 1) / 3) + 1;
		}

		public static int QuarterToMonthStart(int quarterNum)
		{
			return ((quarterNum - 1) * 3) + 1;
		}

		public static int QuarterToMonthEnd(int quarterNum)
		{
			return QuarterToMonthStart(quarterNum) + 2;
		}

		public static int NextQuarter(int quarterNum)
		{
			return (quarterNum % 4) + 1;
		}

		public static int PrevQuarter(int quarterNum)
		{
			return ((quarterNum + 3) % 5) + 1;
		}

		public static int MonthToNextQuarter(int month)
		{
			return NextQuarter(MonthToQuarter(month));
		}

		public static int MonthToPrevQuarter(int month)
		{
			return PrevQuarter(MonthToQuarter(month));
		}

		public static int MonthYearToPrevQuarterYear(int month, int year)
		{
			return (MonthToPrevQuarter(month) == 4) ? year - 1 : year;
		}

		public static DateTime QuarterYearToDateTime(int quarterNum, int year)
		{
			return new DateTime(year, QuarterToMonthStart(quarterNum), 1);
		}
	}
}