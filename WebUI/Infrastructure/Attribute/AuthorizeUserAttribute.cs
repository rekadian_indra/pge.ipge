﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace WebUI.Infrastructure
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AuthorizeUserAttribute : AuthorizeAttribute
    {
        private const string ERR_MSG = "Not valid type. Type must be {0}.";

        public object ModuleName { get; set; }
        public new object[] Roles { get; set; }


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //base.OnAuthorization(filterContext);
            
            //kamus
            UserModule? moduleName = null;
            List<UserRole?> roles = new List<UserRole?>();

            //algoritma
            if (ModuleName != null)
            {
                if (ModuleName.GetType() != typeof(UserModule))
                    throw new InvalidCastException(string.Format(ERR_MSG, typeof(UserModule).Name));
                moduleName = ((UserModule)ModuleName);
            }

            if (Roles != null && Roles.Any())
            {
                foreach (object r in Roles)
                {
                    if (r.GetType() != typeof(UserRole))
                        throw new InvalidCastException(string.Format(ERR_MSG, typeof(UserRole).Name));
                    roles.Add(((UserRole)r));
                }
            }

            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                bool hasAccess = true;

                if (filterContext.HttpContext.User is Principal user)
                {
                    if (moduleName != null)
                        hasAccess = user.HasAccess(moduleName);
                    else if (roles.Any())
                        hasAccess = user.HasAccess(roles.ToArray());
                    else
                        hasAccess = false;

                    if (!hasAccess)
                    {
                        filterContext.Result = new RedirectToRouteResult(new
                            RouteValueDictionary(new
                            {
                                action = "Http401",
                                controller = "Error",
                                area = string.Empty,
                                url = filterContext.HttpContext.Request.Url.OriginalString
                            }));
                    }
                }
            }
        }

        //protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        //{
        //    HttpContext ctx = HttpContext.Current;

        //    // If the browser session has expired...
        //    if (!filterContext.HttpContext.Request.IsAuthenticated)
        //    {
        //        if (filterContext.HttpContext.Request.IsAjaxRequest())
        //        {
        //            // For AJAX requests, we're overriding the returned JSON result with a simple string,
        //            // indicating to the calling JavaScript code that a redirect should be performed.
        //            filterContext.Result = new JsonResult { Data = "_Logon_" };
        //        }
        //        else
        //        {
        //            // For round-trip posts, we're forcing a redirect to Home/TimeoutRedirect/, which
        //            // simply displays a temporary 5 second notification that they have timed out, and
        //            // will, in turn, redirect to the logon page.
        //            filterContext.Result = new RedirectToRouteResult(
        //                new RouteValueDictionary {
        //                    { "Controller", "Authentication" },
        //                    { "Action", "Login" },
        //                    { "Area", string.Empty }
        //            });
        //        }
        //    }
        //    else if (filterContext.HttpContext.Request.IsAuthenticated)
        //    {
        //        // Otherwise the reason we got here was because the user didn't have access rights to the
        //        // operation, and a 403 should be returned.
        //        filterContext.Result = new HttpStatusCodeResult(403);
        //    }
        //    else
        //    {
        //        base.HandleUnauthorizedRequest(filterContext);
        //    }
        //}
    }
}