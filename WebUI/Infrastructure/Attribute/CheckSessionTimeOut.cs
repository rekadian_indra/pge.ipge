﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace WebUI.Infrastructure.Attribute
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class CheckSessionTimeOut : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            // If the browser session or authentication session has expired...
            if (!filterContext.HttpContext.Request.IsAuthenticated)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    // For AJAX requests, we're overriding the returned JSON result with a simple string,
                    // indicating to the calling JavaScript code that a redirect should be performed.

                    //filterContext.HttpContext.Response.StatusCode = 401;
                    //filterContext.Result = new JsonResult
                    //{
                    //    Data = new
                    //    {
                    //        Error = "NotAuthorized",
                    //        LogOnUrl = FormsAuthentication.LoginUrl
                    //    },
                    //    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    //};
                    //filterContext.HttpContext.Response.Clear();
                    //filterContext.HttpContext.Response.End();
                }
                else
                {
                    // will redirect to the logon page.
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary {
                        { "Controller", "Authentication" },
                        { "Action", "Login" },
                        { "Area", string.Empty }
                    });
                }
            }

            base.OnActionExecuting(filterContext);

            //var context = filterContext.HttpContext;
            //if (context.Session != null)
            //{
            //    if (context.Session.IsNewSession)
            //    {
            //        if (context.Request.IsAjaxRequest())
            //        {

            //        }
            //        else
            //        {
            //            string sessionCookie = context.Request.Headers["Cookie"];
            //            if ((sessionCookie != null) && (sessionCookie.IndexOf("ASP.NET_SessionId") >= 0))
            //            {
            //                FormsAuthentication.SignOut();
            //                string redirectTo = "~/Authentication/Login";
            //                if (!string.IsNullOrEmpty(context.Request.RawUrl))
            //                {
            //                    redirectTo = string.Format("~/Authentication/Login?ReturnUrl={0}", HttpUtility.UrlEncode(context.Request.RawUrl));

            //                }
            //                filterContext.HttpContext.Response.Redirect(redirectTo, true);
            //            }
            //        }
            //    }
            //}
            //base.OnActionExecuting(filterContext);
        }
    }
}