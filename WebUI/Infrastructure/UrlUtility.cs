﻿using System.Globalization;
using System.Web;

namespace WebUI.Infrastructure
{
    public static class UrlUtility
    {
        public static string RewriteReturnUrl(string urlPathAndQuery)
        {
            string returnUrl = urlPathAndQuery;
            string applicationPath = HttpContext.Current.Request.ApplicationPath;

            if (applicationPath != "/")
            {
                string applicationPathEncode = HttpUtility.UrlEncode(applicationPath);

                returnUrl = returnUrl.Replace(applicationPath, "");
                returnUrl = returnUrl.Replace(applicationPathEncode, "");

                if (returnUrl.EndsWith("?returnUrl=", true, CultureInfo.CurrentCulture))
                {
                    returnUrl = $"{returnUrl}{HttpUtility.UrlEncode("/")}";
                }
            }

            return returnUrl;
        }
    }
}