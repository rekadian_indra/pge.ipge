﻿using System;
using System.IO;
using System.Web;

namespace WebUI.Infrastructure
{
    public static class DirectoryHelper
    {
        private const string DIRECTORY_ERR_FORMAT = "Can not create directory {0}";

        private static HttpServerUtility Server
        {
            get
            {
                if (HttpContext.Current == null)
                    throw new ArgumentNullException();

                return HttpContext.Current.Server;
            }
        }

        public static string Area(string area)
        {
            string path = "~/Uploads/"+area+"/";
            string serverPath = Server.MapPath(path);

            if (!Directory.Exists(serverPath))
            {
                try
                {
                    DirectoryInfo info = Directory.CreateDirectory(serverPath);

                    if (!info.Exists)
                        throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                }
                catch
                {
                    throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                }
            }

            return path;
        }

        public static string Upload
        {
            get
            {
                string path = "~/Uploads/";
                string serverPath = Server.MapPath(path);

                if (!Directory.Exists(serverPath))
                {
                    try
                    {
                        DirectoryInfo info = Directory.CreateDirectory(serverPath);

                        if (!info.Exists)
                            throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                    catch
                    {
                        throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                }

                return path;
            }
		}

		public static string Resources
		{
			get
			{
				string path = "~/Resources/";
				string serverPath = Server.MapPath(path);

				if (!Directory.Exists(serverPath))
				{
					try
					{
						DirectoryInfo info = Directory.CreateDirectory(serverPath);

						if (!info.Exists)
							throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
					}
					catch
					{
						throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
					}
				}

				return path;
			}
		}

        public static string Template
        {
            get
            {
                string path = "~/Template/";
                string serverPath = Server.MapPath(path);

                if (!Directory.Exists(serverPath))
                {
                    try
                    {
                        DirectoryInfo info = Directory.CreateDirectory(serverPath);

                        if (!info.Exists)
                            throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                    catch
                    {
                        throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                }

                return path;
            }
        }
    }
}