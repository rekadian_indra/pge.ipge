﻿namespace WebUI.Infrastructure.Abstract
{

    public interface IAuthProvider
    {
        string Message { get; }
        bool Authenticate(string username, string password);
        bool Logout();
    }
}
