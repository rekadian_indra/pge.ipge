﻿namespace WebUI.Infrastructure.Abstract
{
    public interface IObjectExtender
    {
        object Extend(object obj1, object obj2);
    }
}
