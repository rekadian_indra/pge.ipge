﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace WebUI.Infrastructure
{
    public class EmailHelper
    {
        public static bool SendEmail(string emailTo, string subject, string body)
        {
            string smtpServer = ConfigurationManager.AppSettings["smtpServer"];
            string smtpFrom = ConfigurationManager.AppSettings["smtpFrom"];
            string smtpUser = ConfigurationManager.AppSettings["smtpUser"];
            string smtpPassword = ConfigurationManager.AppSettings["passEncrypt"];

            bool result = false;
            try
            {
                MailMessage mailMessage = new MailMessage(smtpFrom, emailTo);
                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = body;
                SmtpClient smtpClient = new SmtpClient(smtpServer);
                smtpClient.Credentials = new NetworkCredential(smtpUser, smtpPassword);
                smtpClient.EnableSsl = false;
                smtpClient.Send(mailMessage);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
    }
}