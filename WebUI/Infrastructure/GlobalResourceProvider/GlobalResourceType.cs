﻿namespace WebUI.Infrastructure
{
    public static class GlobalResourceType
    {
        public const string AppGlobalMessage = "AppGlobalMessage";
        public const string AppGlobalError = "AppGlobalError";
    }
}
