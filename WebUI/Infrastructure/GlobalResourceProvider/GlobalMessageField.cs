﻿namespace WebUI.Infrastructure
{
    public static class GlobalMessageField
    {
        public const string ApprovedSuccess = "ApprovedSuccess";
        public const string ApprovedFailed = "ApprovedFailed";
		public const string CreateSuccess = "CreateSuccess";
        public const string DeleteConfirmation = "DeleteConfirmation";
        public const string DeleteFailed = "DeleteFailed";
        public const string DeleteFailedWithMessage = "DeleteFailedWithMessage";
        public const string DeleteSuccess = "DeleteSuccess";
        public const string DeleteSuccessWithParam = "DeleteSuccessWithParam";
        public const string EditSuccess = "EditSuccess";
		public const string EditFailed = "EditFailed";
		public const string FileUploadImageTypeError = "FileUploadImageTypeError";
        public const string FileUploadSizeError = "FileUploadSizeError";
        public const string ImportSuccess = "ImportSuccess";
        public const string SynchronizeSuccess = "SynchronizeSuccess";
        public const string UploadExcelSuccess = "UploadExcelSuccess";
        public const string UploadSuccess = "UploadSuccess";
        public const string UserNotRegister = "UserNotRegister";
        public const string PasswordError = "PasswordError";
        public const string Unique = "Unique";
    }
}
