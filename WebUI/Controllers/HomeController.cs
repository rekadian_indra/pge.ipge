﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using System.Web;
using Business.Infrastructure;
using Business.Entities;
using Business.Abstract;
using System.Linq;
using System;
using WebUI.Models.Home;
using Newtonsoft.Json;
using Business.Entities.Views;
using WebUI.Areas.Reporting.Models;
using System.Web.Script.Serialization;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Controllers
{
    [Authorize]
    [CheckSessionTimeOut]
    public class HomeController : BaseController
    {
        public HomeController(ILogRepository repoLog,
            ISalesOrderRepository repoSalesOrder,
            IAreaRepository repoArea,
            ITaskReportRepository repoTaskReport,
            IProductionPlanRepository repoProductionPlan,
            IInvoiceRepository repoInvoice,
            IPpiRepository repoPPIPrice,
            ISteamPowerPlantPriceRepository repoSteamPrice,
            IViewProgressEnergyRepository repoViewProgressEnergy,
            IViewProgressEnergyCumulativeRepository repoViewProgressEnergyCumulative,
            IWorkflowRepository repoWorkflow,
            IPowerPlantRepository repoPowerPlant,
            IPltpRepository repoPltp,
            IProductionPlanDetailRepository repoPlanDetail,
            IViewCapacityFactorRepository repoViewCapacityFactor,
            IViewDkpRepository viewDkpRepository,
            IViewReliabilityPlanRepository viewReliabilityPlanRepository,
            IViewPltpRepository viewPltpRepository,
            IViewPpiRepository repoViewPpi,
            IViewInvoicingRepository repoViewInvoicing,
            IViewSalesOrderFinanceRepository repoalesOrderFinance,
            IViewInvoiceRepository repoViewInvoice)
        {
            RepoLog = repoLog;
            RepoSalesOrder = repoSalesOrder;
            RepoArea = repoArea;
            RepoTaskReport = repoTaskReport;
            RepoProductionPlan = repoProductionPlan;
            RepoInvoice = repoInvoice;
            RepoPPI = repoPPIPrice;
            RepoSteamPowerPlantPrice = repoSteamPrice;
            RepoViewProgressEnergy = repoViewProgressEnergy;
            RepoViewProgressEnergyCumulative = repoViewProgressEnergyCumulative;
            RepoWorkflow = repoWorkflow;
            RepoPowerPlant = repoPowerPlant;
            RepoPltp = repoPltp;
            RepoProductionPlanDetail = repoPlanDetail;
            RepoViewCapacityFactor = repoViewCapacityFactor;
            RepoViewDkp = viewDkpRepository;
            RepoViewReliabilityPlan = viewReliabilityPlanRepository;
            RepoViewPltp = viewPltpRepository;
            RepoViewPpi = repoViewPpi;
            RepoViewInvoicing = repoViewInvoicing;
            RepoViewSalesOrderFinance = repoalesOrderFinance;
            RepoViewInvoice = repoViewInvoice;
        }

        //if using ModelBinder to get user login use SGUser parameter
        //if using Principal dont get user login SGUser parameter
        //public ActionResult Index(SGUser user)
        //{
        //}
        
        [MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span>", Key = "Dashboard", ParentKey = "Home")]
        public override async Task<ActionResult> Index()
        {
            BindingRegional();
            return await base.Index();
        }

        [HttpPost]
        public async Task<string> GetReliabilityData(int areaId, RangeData rangeData, int month, int year)
        {
            //get data
            Task<List<ViewDkp>> dkpsT;
            Task<List<ViewReliabilityPlan>> reliabilityPlansT;
            FilterQuery filterQuery = new FilterQuery();
            filterQuery.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZPGE);
            if (areaId > 0)
            {
                filterQuery.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
            }
            Task<List<Business.Entities.PowerPlant>> powerPlantDbsT = RepoPowerPlant.FindAllAsync(filterQuery);

            if (rangeData == RangeData.DAILY)
            {
                dkpsT = RepoViewDkp.FindAllByPeriodeAsync(year, month);
                reliabilityPlansT = RepoViewReliabilityPlan.FindAllByPeriodeAsync(year, month);
            }
            else if (rangeData == RangeData.MONTHLY)
            {
                month = 1;
                dkpsT = RepoViewDkp.FindAllByPeriodeAsync(year, 1);
                reliabilityPlansT = RepoViewReliabilityPlan.FindAllByPeriodeAsync(year, 1);
            }
            else
            {
                month = 1;
                year = DateTime.Now.Year;
                dkpsT = RepoViewDkp.FindAllAsync();
                reliabilityPlansT = RepoViewReliabilityPlan.FindAllAsync();
            }

            Task<List<ViewPltp>> pltpsT = RepoViewPltp.FindAllAsync();

            Task.WaitAll(powerPlantDbsT, dkpsT, reliabilityPlansT, pltpsT);

            List<Business.Entities.PowerPlant> powerPlantDbs = powerPlantDbsT.Result;
            List<ViewDkp> dkps = dkpsT.Result;
            List<ViewReliabilityPlan> reliabilityPlans = reliabilityPlansT.Result;
            List<ViewPltp> pltps = pltpsT.Result;

            ReliabilityStub model = new ReliabilityStub();

            if (rangeData == RangeData.YEARLY)
                model = new ReliabilityStub(powerPlantDbs, pltps, reliabilityPlans, dkps);
            else
                model = new ReliabilityStub(powerPlantDbs, pltps, reliabilityPlans, dkps, year, month, isCumulative: false);

            await Task.Delay(0);

            return JsonConvert.SerializeObject(model);
        }

        [HttpPost]
        public async Task<string> GetTopDashboardModel(int areaId, string puChart, RangeData rangeData, int monthChart, int yearChart)
        {
            DashboardOperationPresentationStub model = new DashboardOperationPresentationStub();

            model.AreaId = areaId;
            model.SOType = SOType.ZPGE.ToString();
            model.ProductionUtilizationChart = string.IsNullOrEmpty(puChart) ? ProductionUtilization.ELECTRIC.ToString() : puChart;
            model.RangeData = rangeData;
            model.Month = monthChart;
            model.Year = yearChart;

            PrepareDashboard(model);

            await Task.Delay(0);
            return JsonConvert.SerializeObject(model);
        }

        [HttpPost]
        public async Task<string> GetBottomDashboardModel(int areaId, string puChart, RangeData rangeData, int monthChart, int yearChart)
        {
            DashboardOperationPresentationStub model = new DashboardOperationPresentationStub();

            model.AreaId = areaId;
            model.ProductionUtilizationChart = puChart;
            model.RangeData = rangeData;
            model.MonthChart = monthChart;
            model.YearChart = yearChart;

            PrepareDashboardChart(model);

            await Task.Delay(0);
            return JsonConvert.SerializeObject(model);
        }

        [AllowAnonymous]
        [MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span>", Key = "Mobile", ParentKey = "Home")]
        public async Task<ActionResult> Mobile()
        {
            await Task.Delay(0);
            return View("Mobile");
        }

        //[MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span> Executive", Key = "DashboardExecutive", ParentKey = "Home")]
        public async Task<ActionResult> Executive()
        {
            await Task.Delay(0);
            return View("Executive");
        }

        [HttpPost]
        public async Task<string> GetTopDashboardExecutiveModel(int areaId, int month, int endYear, string rangeData)
        {
            FilterQuery filter = new FilterQuery(Field.Status, FilterOperator.Equals, DataStatus.APPROVED.ToString());
            List<TaskReport> dbObjects = RepoTaskReport.FindAll(filter);

            int startYear = dbObjects.OrderBy(m => m.Periode.Value.Year).Select(m => m.Periode.Value.Year).FirstOrDefault();

            DashboardExecutiveModelStub model = new DashboardExecutiveModelStub()
            {
                AreaId = areaId,
                Month = month,
                StartYear = startYear,
                EndYear = endYear,
                RangeData = rangeData
            };

            //model.AreaId = areaId;

            model = await InitDashboardExecutive(model);

            return JsonConvert.SerializeObject(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> Mobile(DashboardOperationPresentationStub model)
        {
            //kamus
            //if (model.AreaId == 0)
            //    return RedirectToAction("Index");

            model.IsFirstTimeLoad = false;

            PrepareDashboard(model);
            PrepareDashboardChart(model);
            await Task.Delay(0);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(DashboardOperationPresentationStub model)
        {
            //kamus
            //if (model.AreaId == 0)
            //    return RedirectToAction("Index");

            model.IsFirstTimeLoad = false;

            PrepareDashboard(model);
            PrepareDashboardChart(model);
            await Task.Delay(0);
            return View(model);
        }

        [AllowAnonymous]
        //[MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span> Financial", Key = "DashboardFinancial", ParentKey = "Home")]
        public async Task<ActionResult> Financial()
        {
            await Task.Delay(0);

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Executive(DashboardExecutiveModelStub model)
        {
            model = await InitDashboardExecutive(model);

            return View(model);
        }

        //public async Task<ActionResult> Executive(DashboardExecutivePresentationStub model)
        //{
        //    model = await InitDashboardExecutive(model);

        //    return View(model);
        //}

        [MvcSiteMapNode(Title = "Data Input", Key = "DataInput", ParentKey = "Dashboard")]
        public async Task<ActionResult> InputData()
        {
            await Task.Delay(0);
            List<SelectListItem> statusOption = DisplayFormat.EnumToSelectList<DataStatus>();
            return View("InputData");
        }

        [MvcSiteMapNode(Title = "Manual Input", Key = "ManualInput", ParentKey = "DataInput")]
        public async Task<ActionResult> FormInputData()
        {
            HomeFormStub model = new HomeFormStub();

            await Task.Delay(0);
            return View("FormInputData");
        }

        #region Binding 
        [HttpPost]
        public async Task<string> BindingFinance(int? year = null)
        {
            //lib
            Task<List<Area>> TaskAreas = null;
            Task<List<Invoice>> TaskInvoices = null;
            DashboardFinancePresentationStub model;
            List<Area> Areas = null;
            List<Invoice> Invoices = null;
            FilterQuery filter = new FilterQuery();

            //algorithm
            if (year != null && year > 0)
            {
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.GreaterThanOrEquals, new DateTime(year.Value, 01, 01).ToString());
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.LessThanOrEquals, new DateTime(year.Value, 12, 31).ToString());
            }
            else
            {
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.GreaterThanOrEquals, new DateTime(DateTime.Now.Year, 01, 01).ToString());
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.LessThanOrEquals, DateTime.Now.ToString());
            }
            TaskInvoices = RepoInvoice.FindAllAsync(filter);
            TaskAreas = RepoArea.FindAllAsync();


            //get and count data from db in background thread
            await Task.WhenAll(TaskAreas, TaskInvoices);

            //get callback from task
            Areas = TaskAreas.Result;
            Invoices = TaskInvoices.Result;

            //map db data to model
            model = new DashboardFinancePresentationStub(Invoices, Areas);

            return JsonConvert.SerializeObject(new { data = model });
        }

        protected virtual async Task<List<SelectListItem>> YearByInvoiceAsync()
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Invoice> invoices = await RepoInvoice.FindAllAsync();

            //algorithm
            if (invoices.Any())
            {
                options = invoices.Where(m => m.InvoiceAcceptDate != null && m.Workflow.Name == Common.Enums.Workflow.FINISH.ToString())?
                    .OrderByDescending(x => x.InvoiceAcceptDate)
                    .Select(x => new SelectListItem { Text = x.InvoiceAcceptDate.Value.Year.ToString(), Value = x.InvoiceAcceptDate.Value.Year.ToString() })
                    .Distinct().ToList();
            }
            return options.ToList();
        }

        public async Task<string> BindingPowerPlant()
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Business.Entities.PowerPlant> powerPlants = await RepoPowerPlant.FindAllAsync(new FilterQuery(Field.SOType, FilterOperator.Equals, SOType.ZPGE.ToString()));

            //algorithm
            powerPlants = powerPlants.OrderBy(x => x.Id).ToList();
            foreach (Business.Entities.PowerPlant s in powerPlants)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = s.Name,
                    Value = s.Id.ToString()
                };

                options.Add(o);
            }

            return JsonConvert.SerializeObject(options.ToList());
        }

        public async Task<ActionResult> BindingDocumentFinancialDashboard(int year, int powerPlantId)
        {
            // kamus
            if (powerPlantId != 0)
            {
                Business.Entities.PowerPlant powerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(powerPlantId);
                List<Task<Business.Entities.Invoice>> invoiceTasks = new List<Task<Invoice>>();
                List<Business.Entities.SalesOrder> salesOrders;
                List<Business.Entities.Invoice> invoices;
                List<Business.Entities.Workflow> salesOrderWorkflows;
                List<Business.Entities.Workflow> invoiceWorkflows;

                var salesOrderDocumentTable = new object();
                var invoiceDocumentTable = new object();

                // algo
                FinanceDocumentStub model = new FinanceDocumentStub(powerPlant);

                salesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(new DateTime(year, 1, 1), powerPlantId, RangeData.YEARLY);
                if (powerPlant.SOType == SOType.ZPGE.ToString())
                {
                    salesOrderWorkflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.ZPGE);
                    salesOrderDocumentTable = new FinanceSalesOrderDocumentZPGEStub(salesOrders, salesOrderWorkflows);
                }
                else
                {
                    salesOrderWorkflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.ZTS);
                    salesOrderDocumentTable = new FinanceSalesOrderDocumentZTSStub(salesOrders, salesOrderWorkflows);
                }
                model.SalesOrderWorkflows = salesOrderWorkflows;

                // invoices
                foreach (Business.Entities.SalesOrder salesOrder in salesOrders)
                {
                    invoiceTasks.Add(RepoInvoice.FindBySalesOrderIdAsync(salesOrder.Id));
                }
                Task.WaitAll(invoiceTasks.ToArray());
                invoices = invoiceTasks.Select(x => x.Result).ToList();
                if (powerPlant.Material == Material.ELECTRIC_POWER.ToString())
                {
                    invoiceWorkflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.INVEL);
                    invoiceDocumentTable = new FinanceInvoiceDocumentElectricStub(invoices, invoiceWorkflows);

                    // cek yg satu invoice group
                    foreach (FinanceInvoiceDocumentElectricStub.Row row in ((FinanceInvoiceDocumentElectricStub)invoiceDocumentTable).Rows)
                    {
                        List<Business.Entities.PowerPlant> sameGroupPowerPlants = await RepoPowerPlant.FindAllByInvoiceGroup(powerPlant.InvoiceGroup);
                        foreach (Business.Entities.PowerPlant sameGroupPowerPlant in sameGroupPowerPlants)
                        {
                            Business.Entities.Invoice curInvoice = null;
                            if (invoices != null && invoices.Any())
                            {
                                try
                                {
                                    curInvoice = invoices.FirstOrDefault(x => x.Id == row.InvoiceId);
                                }
                                catch (Exception)
                                {
                                    curInvoice = null;
                                }
                            }
                            if (curInvoice != null)
                            {
                                List<Business.Entities.SalesOrder> curSalesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(curInvoice.SalesOrder.Periode, sameGroupPowerPlant.Id);
                                if (curSalesOrders != null && curSalesOrders.Count > 0)
                                {
                                    Business.Entities.SalesOrder curSalesOrder = curSalesOrders.FirstOrDefault(x => x.Workflow.Approver == null);
                                    if (curSalesOrder != null)
                                    {
                                        if (curSalesOrder.Invoices.Count > 0)
                                        {
                                            continue;
                                        }
                                    }
                                }
                            }
                            row.IsCoverLetter = false;
                            break;
                        }
                    }
                }
                else
                {
                    invoiceWorkflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.INVSM);
                    invoiceDocumentTable = new FinanceInvoiceDocumentSteamStub(invoices, invoiceWorkflows);

                    // cek yg satu invoice group
                    foreach (FinanceInvoiceDocumentSteamStub.Row row in ((FinanceInvoiceDocumentSteamStub)invoiceDocumentTable).Rows)
                    {
                        List<Business.Entities.PowerPlant> sameGroupPowerPlants = await RepoPowerPlant.FindAllByInvoiceGroup(powerPlant.InvoiceGroup);
                        foreach (Business.Entities.PowerPlant sameGroupPowerPlant in sameGroupPowerPlants)
                        {
                            Business.Entities.Invoice curInvoice = null;
                            if (invoices != null && invoices.Any())
                            {
                                try
                                {
                                    curInvoice = invoices.FirstOrDefault(x => x.Id == row.InvoiceId);
                                }
                                catch (Exception)
                                {
                                    curInvoice = null;
                                }
                            }
                            if (curInvoice != null)
                            {
                                List<Business.Entities.SalesOrder> curSalesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(curInvoice.SalesOrder.Periode, sameGroupPowerPlant.Id);
                                if (curSalesOrders != null && curSalesOrders.Count > 0)
                                {
                                    Business.Entities.SalesOrder curSalesOrder = curSalesOrders.FirstOrDefault(x => x.Workflow.Approver == null);
                                    if (curSalesOrder != null)
                                    {
                                        if (curSalesOrder.Invoices.Count > 0)
                                        {
                                            continue;
                                        }
                                    }
                                }
                            }
                            row.IsCoverLetter = false;
                            break;
                        }
                    }
                }
                model.InvoiceWorkflows = invoiceWorkflows;

                return Json(new { soType = model.SOType, material = model.Material, salesOrderDocumentTable = salesOrderDocumentTable, invoiceDocumentTable = invoiceDocumentTable });
            }
            else
            {
                List<object> listData = new List<object>();
                List<Business.Entities.PowerPlant> powerPlants = RepoPowerPlant.FindAll(new FilterQuery(Field.SOType, FilterOperator.Equals, SOType.ZPGE.ToString()));

                foreach (Business.Entities.PowerPlant powerPlant in powerPlants)
                {
                    List<Business.Entities.SalesOrder> salesOrders;
                    List<Business.Entities.Invoice> invoices = new List<Invoice>();
                    List<Business.Entities.Workflow> salesOrderWorkflows;
                    List<Business.Entities.Workflow> invoiceWorkflows;

                    var salesOrderDocumentTable = new object();
                    var invoiceDocumentTable = new object();

                    // algo
                    FinanceDocumentStub model = new FinanceDocumentStub(powerPlant);

                    salesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(new DateTime(year, 1, 1), powerPlant.Id, RangeData.YEARLY);
                    if (powerPlant.SOType == SOType.ZPGE.ToString())
                    {
                        salesOrderWorkflows = RepoWorkflow.FindAll(new List<SortQuery>() { new SortQuery(Field.Id, SortOrder.Ascending) },
                            new FilterQuery(Field.WorkflowType, FilterOperator.Equals, WorkflowType.ZPGE.ToString()));
                        salesOrderDocumentTable = new FinanceSalesOrderDocumentZPGEStub(salesOrders, salesOrderWorkflows);
                    }
                    else
                    {
                        salesOrderWorkflows = RepoWorkflow.FindAll(new List<SortQuery>() { new SortQuery(Field.Id, SortOrder.Ascending) },
                            new FilterQuery(Field.WorkflowType, FilterOperator.Equals, WorkflowType.ZTS.ToString()));
                        salesOrderDocumentTable = new FinanceSalesOrderDocumentZTSStub(salesOrders, salesOrderWorkflows);
                    }
                    model.SalesOrderWorkflows = salesOrderWorkflows;

                    // invoices
                    foreach (Business.Entities.SalesOrder salesOrder in salesOrders)
                    {
                        invoices.Add(RepoInvoice.Find(new FilterQuery(Field.SalesOrderId, FilterOperator.Equals, salesOrder.Id)));
                    }
                    if (powerPlant.Material == Material.ELECTRIC_POWER.ToString())
                    {
                        invoiceWorkflows = RepoWorkflow.FindAll(new List<SortQuery>() { new SortQuery(Field.Id, SortOrder.Ascending) },
                            new FilterQuery(Field.WorkflowType, FilterOperator.Equals, WorkflowType.INVEL));
                        invoiceDocumentTable = new FinanceInvoiceDocumentElectricStub(invoices, invoiceWorkflows);

                        // cek yg satu invoice group
                        foreach (FinanceInvoiceDocumentElectricStub.Row row in ((FinanceInvoiceDocumentElectricStub)invoiceDocumentTable).Rows)
                        {
                            List<Business.Entities.PowerPlant> sameGroupPowerPlants = RepoPowerPlant.FindAll(new FilterQuery(Field.InvoiceGroup, FilterOperator.Equals, powerPlant.InvoiceGroup));
                            foreach (Business.Entities.PowerPlant sameGroupPowerPlant in sameGroupPowerPlants)
                            {
                                Business.Entities.Invoice curInvoice = null;
                                if (invoices != null && invoices.Any())
                                {
                                    try
                                    {
                                        curInvoice = invoices.FirstOrDefault(x => x.Id == row.InvoiceId);
                                    }
                                    catch (Exception)
                                    {
                                        curInvoice = null;
                                    }
                                }
                                if (curInvoice != null)
                                {
                                    List<Business.Entities.SalesOrder> curSalesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(curInvoice.SalesOrder.Periode, sameGroupPowerPlant.Id);
                                    if (curSalesOrders != null && curSalesOrders.Count > 0)
                                    {
                                        Business.Entities.SalesOrder curSalesOrder = curSalesOrders.FirstOrDefault(x => x.Workflow.Approver == null);
                                        if (curSalesOrder != null)
                                        {
                                            if (curSalesOrder.Invoices.Count > 0)
                                            {
                                                continue;
                                            }
                                        }
                                    }
                                }
                                row.IsCoverLetter = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        invoiceWorkflows = RepoWorkflow.FindAll(new List<SortQuery>() { new SortQuery(Field.Id, SortOrder.Ascending) },
                            new FilterQuery(Field.WorkflowType, FilterOperator.Equals, WorkflowType.INVSM));
                        invoiceDocumentTable = new FinanceInvoiceDocumentSteamStub(invoices, invoiceWorkflows);

                        // cek yg satu invoice group
                        foreach (FinanceInvoiceDocumentSteamStub.Row row in ((FinanceInvoiceDocumentSteamStub)invoiceDocumentTable).Rows)
                        {
                            List<Business.Entities.PowerPlant> sameGroupPowerPlants = RepoPowerPlant.FindAll(new FilterQuery(Field.InvoiceGroup, FilterOperator.Equals, powerPlant.InvoiceGroup));
                            foreach (Business.Entities.PowerPlant sameGroupPowerPlant in sameGroupPowerPlants)
                            {
                                Business.Entities.Invoice curInvoice = null;
                                if (invoices != null && invoices.Any())
                                {
                                    try
                                    {
                                        curInvoice = invoices.FirstOrDefault(x => x.Id == row.InvoiceId);
                                    }
                                    catch (Exception)
                                    {
                                        curInvoice = null;
                                    }
                                }
                                if (curInvoice != null)
                                {
                                    List<Business.Entities.SalesOrder> curSalesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(curInvoice.SalesOrder.Periode, sameGroupPowerPlant.Id);
                                    if (curSalesOrders != null && curSalesOrders.Count > 0)
                                    {
                                        Business.Entities.SalesOrder curSalesOrder = curSalesOrders.FirstOrDefault(x => x.Workflow.Approver == null);
                                        if (curSalesOrder != null)
                                        {
                                            if (curSalesOrder.Invoices.Count > 0)
                                            {
                                                continue;
                                            }
                                        }
                                    }
                                }
                                row.IsCoverLetter = false;
                                break;
                            }
                        }
                    }
                    model.InvoiceWorkflows = invoiceWorkflows;

                    listData.Add(new { soType = model.SOType, material = model.Material, salesOrderDocumentTable = salesOrderDocumentTable, invoiceDocumentTable = invoiceDocumentTable });
                }

                return Json(listData);
            }
        }

        public async Task<string> BindingProductionByWkp(int areaId, string rangeData, int year, int month)
        {
            int count;
            List<ViewCapacityFactor> dbObjects;
            Task<List<ViewCapacityFactor>> taskDbObjects = null;
            //Task<int> taskCount = null;
            List<DashboardExecutiveModelStub> models = new List<DashboardExecutiveModelStub>();
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (rangeData == RangeData.DAILY.ToString())
            {
                param.InstanceFilter();
                param.Filter.AddFilter(Field.Month, FilterOperator.Equals, month);
                param.Filter.AddFilter(Field.Year, FilterOperator.Equals, year);
            }
            if (rangeData == RangeData.MONTHLY.ToString())
            {
                param.InstanceFilter();
                param.Filter.AddFilter(Field.Year, FilterOperator.Equals, year);
            }
            if (rangeData == RangeData.YEARLY.ToString())
            {
                List<ProductionPlan> tasks = RepoProductionPlan.FindAll();
                int startYear = tasks.OrderBy(m => m.Year).Select(m => m.Year).FirstOrDefault();
                int lastYear = tasks.OrderByDescending(m => m.Year).Select(m => m.Year).FirstOrDefault();

                param.InstanceFilter();
                param.Filter.AddFilter(Field.Year, FilterOperator.GreaterThanOrEquals, startYear);
                param.Filter.AddFilter(Field.Year, FilterOperator.LessThanOrEquals, lastYear);
            }

            if (areaId > 0)
            {
                param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
            }
            else
            {
                if (param.Filter.Filters.Any())
                {
                    {
                        if (param.Filter.Filters.Exists(f => f.Field == Field.AreaId))
                        {
                            param.Filter.RemoveFilter(Field.AreaId);
                        }
                    }
                }
            }

            param.Filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZPGE.ToString());

            taskDbObjects = RepoViewCapacityFactor.FindAllAsync(param.Filter);

            await Task.WhenAll(taskDbObjects);

            dbObjects = taskDbObjects.Result;

            dbObjects = dbObjects.GroupBy(m => new { m.PowerPlantName, m.AreaName })
                .Select(o => new ViewCapacityFactor
                {
                    AreaName = o.Key.AreaName,
                    PowerPlantName = o.Key.PowerPlantName,
                    Capacity = o.Sum(x => x.Capacity),
                    Actual = o.Sum(x => x.Actual)
                }).ToList();


            models = DashboardExecutiveModelStub.MapList(dbObjects);
            count = models.Count();
            models = models.Skip(param.Skip).Take(param.Take).ToList();

            return JsonConvert.SerializeObject(new { data = models, total = count });
        }

        #endregion

        #region financial dashboard
        public async Task<string> BindingProduction(string range = null, int? areaId = null, int? year = null, int? month = null)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            List<DashboardFinancialPresentationStub> models = new List<DashboardFinancialPresentationStub>();
            List<ViewInvoice> datas = new List<ViewInvoice>();
            List<SortQuery> sorts = new List<SortQuery> { new SortQuery(Field.Id, SortOrder.Descending) };

            FilterQuery filterArea = new FilterQuery();
            FilterQuery filter = new FilterQuery();
            DateTime now = DateTime.Now;
            DateTime dateParam = DateTime.Now;

            string rangeData = string.IsNullOrEmpty(range) ? RangeData.DAILY.ToString() : range;
            int yearData = year == null ? now.Year : year.Value;
            int monthData = month == null ? now.Month : month.Value;
            int lastDay = DateTime.DaysInMonth(yearData, monthData);

            filter.AddFilter(Field.WorkflowCode, FilterOperator.Equals, WorkflowStatus.FINISH);

            if (areaId.HasValue)
            {
                filterArea.AddFilter(Field.Id, FilterOperator.Equals, areaId);
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId.Value);
            }

            if (rangeData == RangeData.DAILY.ToString())
            {
                dateParam = new DateTime(yearData, monthData, lastDay);
                filter.AddFilter(Field.TaskPeriode, FilterOperator.GreaterThanOrEquals, new DateTime(yearData, monthData, 1));
                filter.AddFilter(Field.TaskPeriode, FilterOperator.LessThanOrEquals, dateParam);
            }
            else if (rangeData == RangeData.MONTHLY.ToString())
            {
                filter.AddFilter(Field.TaskPeriode, FilterOperator.GreaterThanOrEquals, new DateTime(yearData, 1, 1));
                filter.AddFilter(Field.TaskPeriode, FilterOperator.LessThanOrEquals, new DateTime(yearData, 12, 31));
            }
           

            var areasTask = await RepoArea.FindAllAsync(sorts,filterArea);
            datas =await RepoViewInvoice.FindAllAsync(filter);

            var areas = areasTask.Where(n => n.PowerPlants.Any(m => m.SOType == SOType.ZPGE.ToString()));

            models = DashboardFinancialPresentationStub.MapListPeriode(areas, datas, rangeData, monthData, yearData);

            js.MaxJsonLength = Int32.MaxValue;
            return js.Serialize(models);

        }
        public async Task<string> BindingSales(string range = null, int? areaId = null, int? year = null, int? month = null)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            List<DashboardFinancialPresentationStub> models = new List<DashboardFinancialPresentationStub>();
            List<SortQuery> sorts = new List<SortQuery> { new SortQuery(Field.Id, SortOrder.Descending) };
            List<ViewInvoice> datas = new List<ViewInvoice>();

            FilterQuery filterArea = new FilterQuery();
            FilterQuery filter = new FilterQuery();
            DateTime now = DateTime.Now;
            DateTime dateParam = DateTime.Now;
            
            string rangeData = string.IsNullOrEmpty(range) ? RangeData.DAILY.ToString() : range;
            int yearData = year == null ? now.Year : year.Value;
            int monthData = month == null ? now.Month : month.Value;
            int lastDay = DateTime.DaysInMonth(yearData, monthData);            

            if (areaId.HasValue)
            {
                filterArea.AddFilter(Field.Id, FilterOperator.Equals, areaId);
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId.Value);
            }

            if (rangeData == RangeData.DAILY.ToString())
            {
                dateParam = new DateTime(yearData, monthData, lastDay);
                filter.AddFilter(Field.TaskPeriode, FilterOperator.GreaterThanOrEquals, new DateTime(yearData, monthData, 1));
                filter.AddFilter(Field.TaskPeriode, FilterOperator.LessThanOrEquals, dateParam);
            }
            else if (rangeData == RangeData.MONTHLY.ToString())
            {
                filter.AddFilter(Field.TaskPeriode, FilterOperator.GreaterThanOrEquals, new DateTime(yearData, 1, 1));
                filter.AddFilter(Field.TaskPeriode, FilterOperator.LessThanOrEquals, new DateTime(yearData, 12, 31));
            }
           
            var areasTask = await RepoArea.FindAllAsync(sorts, filterArea);
            datas = await RepoViewInvoice.FindAllAsync(filter);

            var areas = areasTask.Where(n => n.PowerPlants.Any(m => m.SOType == SOType.ZPGE.ToString()));
            datas = datas.Where(p => p.InvoiceId != null).ToList();

            models = DashboardFinancialPresentationStub.MapListPeriode(areas, datas, rangeData, monthData, yearData);

            js.MaxJsonLength = Int32.MaxValue;
            return js.Serialize(models);

        }
        public async Task<string> BindingRegional(string range = null, int? areaId = null, int? year = null, int? month = null)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            List<DashboardFinancialPresentationStub> models = new List<DashboardFinancialPresentationStub>();
            List<ViewInvoice> datas = new List<ViewInvoice>();
            FilterQuery filterArea = new FilterQuery();
            FilterQuery filter = new FilterQuery();                                    
            DateTime now = DateTime.Now;
            DateTime dateParam = DateTime.Now;

            string rangeData = string.IsNullOrEmpty(range) ? RangeData.DAILY.ToString() : range;
            int yearData = year == null ? now.Year : year.Value;
            int monthData = month == null ? now.Month : month.Value;            
            int lastDay = DateTime.DaysInMonth(yearData, monthData);

            if (areaId.HasValue)
            {
                filterArea.AddFilter(Field.Id, FilterOperator.Equals, areaId);
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId.Value);
            }

            filter.AddFilter(Field.WorkflowCode, FilterOperator.Equals, WorkflowStatus.FINISH);

            if (rangeData == RangeData.DAILY.ToString())
            {
                dateParam = new DateTime(yearData, monthData, lastDay);
                filter.AddFilter(Field.TaskPeriode, FilterOperator.GreaterThanOrEquals, new DateTime(yearData, monthData, 1));
                filter.AddFilter(Field.TaskPeriode, FilterOperator.LessThanOrEquals, dateParam);
            }
            else if(rangeData == RangeData.MONTHLY.ToString())
            {                
                filter.AddFilter(Field.TaskPeriode, FilterOperator.GreaterThanOrEquals, new DateTime(yearData, 1, 1));
                filter.AddFilter(Field.TaskPeriode, FilterOperator.LessThanOrEquals, new DateTime(yearData, 12, 31));
            }            

            var areasTask = await RepoArea.FindAllAsync(filterArea);
            datas = await RepoViewInvoice.FindAllAsync(filter);

            var areas = areasTask.Where(n=>n.PowerPlants.Any(m=>m.SOType == SOType.ZPGE.ToString()));

            models = DashboardFinancialPresentationStub.MapList(areas, datas);           

            js.MaxJsonLength = Int32.MaxValue;            
            return js.Serialize(models);            

        }

        public async Task<string> BindingRevenue(string range = null, int? areaId = null, int? year = null, int? month = null)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            List<DashboardFinancialPresentationStub> models = new List<DashboardFinancialPresentationStub>();
            List<ViewInvoice> datas = new List<ViewInvoice>();
            FilterQuery filterArea = new FilterQuery();
            FilterQuery filter = new FilterQuery();
            DateTime now = DateTime.Now;
            DateTime dateParam = DateTime.Now;

            string rangeData = string.IsNullOrEmpty(range) ? RangeData.DAILY.ToString() : range;
            int yearData = year == null ? now.Year : year.Value;
            int monthData = month == null ? now.Month : month.Value;
            int lastDay = DateTime.DaysInMonth(yearData, monthData);

            if (areaId.HasValue)
            {
                filterArea.AddFilter(Field.Id, FilterOperator.Equals, areaId);
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId.Value);
            }

            if (rangeData == RangeData.DAILY.ToString())
            {
                dateParam = new DateTime(yearData, monthData, lastDay);
                filter.AddFilter(Field.TaskPeriode, FilterOperator.GreaterThanOrEquals, new DateTime(yearData, monthData, 1));
                filter.AddFilter(Field.TaskPeriode, FilterOperator.LessThanOrEquals, dateParam);
            }
            else if (rangeData == RangeData.MONTHLY.ToString())
            {
                filter.AddFilter(Field.TaskPeriode, FilterOperator.GreaterThanOrEquals, new DateTime(yearData, 1, 1));
                filter.AddFilter(Field.TaskPeriode, FilterOperator.LessThanOrEquals, new DateTime(yearData, 12, 31));
            }

            var areasTask = await RepoArea.FindAllAsync(filterArea);
            datas = await RepoViewInvoice.FindAllAsync(filter);

            var areas = areasTask.Where(n => n.PowerPlants.Any(m => m.SOType == SOType.ZPGE.ToString()));
            datas = datas.Where(p => p.InvoiceId != null).ToList();

            models = DashboardFinancialPresentationStub.MapListRevenue(areas, datas);

            js.MaxJsonLength = Int32.MaxValue;
            return js.Serialize(models);

        }

        public async Task<string> BindingTable(string range = null, int? areaId = null, int? year = null, int? month = null)
        {
            int count;
            QueryRequestParameter param = QueryRequestParameter.Current;
            JavaScriptSerializer js = new JavaScriptSerializer();
            List<FinanceSalesOrderPresentationStub> models = new List<FinanceSalesOrderPresentationStub>();
            List<ViewInvoice> datas = new List<ViewInvoice>();
            FilterQuery filterArea = new FilterQuery();
            FilterQuery filter = new FilterQuery();
            DateTime now = DateTime.Now;
            DateTime dateParam = DateTime.Now;

            string rangeData = string.IsNullOrEmpty(range) ? RangeData.DAILY.ToString() : range;
            int yearData = year == null ? now.Year : year.Value;
            int monthData = month == null ? now.Month : month.Value;
            int lastDay = DateTime.DaysInMonth(yearData, monthData);

            if (areaId.HasValue)
            {
                filterArea.AddFilter(Field.Id, FilterOperator.Equals, areaId);
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId.Value);
            }

            if (rangeData == RangeData.DAILY.ToString())
            {
                dateParam = new DateTime(yearData, monthData, lastDay);
                filter.AddFilter(Field.TaskPeriode, FilterOperator.GreaterThanOrEquals, new DateTime(yearData, monthData, 1));
                filter.AddFilter(Field.TaskPeriode, FilterOperator.LessThanOrEquals, dateParam);
            }
            else if (rangeData == RangeData.MONTHLY.ToString())
            {
                filter.AddFilter(Field.TaskPeriode, FilterOperator.GreaterThanOrEquals, new DateTime(yearData, 1, 1));
                filter.AddFilter(Field.TaskPeriode, FilterOperator.LessThanOrEquals, new DateTime(yearData, 12, 31));
            }

            var areasTask = await RepoArea.FindAllAsync(filterArea);
            datas =await RepoViewInvoice.FindAllAsync(filter);

            var areas = areasTask.Where(n => n.PowerPlants.Any(m => m.SOType == SOType.ZPGE.ToString()));
            datas = datas.Where(p => p.InvoiceId != null).ToList();

            models = FinanceSalesOrderPresentationStub.MapList(areas, datas);
            count = models.Count();
            models = models.Skip(param.Skip).Take(param.Take).ToList();

            return JsonConvert.SerializeObject(new { data = models, total = count });

        }
        #endregion

        #region Helper

        private double CalculateProductionPlan(List<ProductionPlan> models, int? currentMonth = null, bool isCumulative = true)
        {
            //kamus
            double result = 0;

            foreach (ProductionPlan pp in models)
            {
                if (currentMonth != null)
                {
                    if (isCumulative)
                        result += pp.ProductionPlanDetails.Where(x => x.Month <= currentMonth.Value).Sum(x => x.Value);
                    else
                        result += pp.ProductionPlanDetails.Where(x => x.Month == currentMonth.Value).Sum(x => x.Value);
                }
                else
                    result += pp.ProductionPlanDetails.Sum(x => x.Value);
            }

            return result;
        }

        private double CalculatePricePlan(List<ProductionPlan> models, List<PPI> ppis, int? currentMonth = null, bool isCumulative = true)
        {
            //kamus
            double result = 0;
            double price = 0;
            IEnumerable<ProductionPlanDetail> plans;
            IEnumerable<PPI> prices;

            foreach (ProductionPlan pp in models)
            {
                prices = ppis.Where(n => n.PowerPlantId == pp.PowerPlantId);
                if (currentMonth != null)
                {
                    if (isCumulative)
                        plans = pp.ProductionPlanDetails.Where(n => n.Month <= currentMonth);
                    else
                        plans = pp.ProductionPlanDetails.Where(n => n.Month == currentMonth);
                }
                else
                    plans = pp.ProductionPlanDetails;

                foreach (ProductionPlanDetail pd in plans)
                {
                    price = 0;
                    price = prices.Any() ? prices.FirstOrDefault().NextPrice : 0;
                    result += (pd.Value * price * 1000);
                }
            }

            return result;
        }

        private double CalculatePricePlanSteam(List<ProductionPlan> models, List<SteamPowerPlantPrice> prices, int? currentMonth = null, bool isCumulative = true)
        {
            //kamus
            double result = 0;
            double price = 0;
            IEnumerable<ProductionPlanDetail> plans;
            IEnumerable<SteamPowerPlantPrice> steamPrices;

            foreach (ProductionPlan pp in models)
            {
                steamPrices = prices.Where(n => n.PowerPlantId == pp.PowerPlantId);
                if (currentMonth != null)
                {
                    if (isCumulative)
                        plans = pp.ProductionPlanDetails.Where(n => n.Month <= currentMonth);
                    else
                        plans = pp.ProductionPlanDetails.Where(n => n.Month == currentMonth);
                }
                else
                    plans = pp.ProductionPlanDetails;

                foreach (ProductionPlanDetail pd in plans)
                {
                    price = 0;
                    price = steamPrices.Any() ? steamPrices.FirstOrDefault().Price : 0;
                    result += (pd.Value * price);
                }
            }

            return result;
        }

        private double CalculateProductionActual(List<TaskReport> models, int? currentMonth = null)
        {
            //kamus
            double result = 0;

            foreach (TaskReport pp in models)
            {
                if (currentMonth != null)
                {
                    if (pp.Periode.Value.Month == currentMonth)
                        result += pp.Pltps.Sum(x => x.EnergyNett.Value);
                }
                else
                    result += pp.Pltps.Sum(x => x.EnergyNett.Value);
            }

            return result;
        }

        private double CalculateProductionActualSteam(List<TaskReport> models, int? currentMonth = null)
        {
            //kamus
            double result = 0;

            foreach (TaskReport pp in models)
            {
                if (currentMonth != null)
                {
                    if (pp.Periode.Value.Month == currentMonth)
                        result += pp.Pltps.Sum(x => x.SteamUtilization.Value);
                }
                else
                    result += pp.Pltps.Sum(x => x.SteamUtilization.Value);
            }

            return result;
        }

        private double CalculatePriceActual(List<TaskReport> models, List<PPI> ppis, int? currentMonth = null)
        {
            //kamus
            double result = 0;
            double price = 0;
            IEnumerable<Pltp> pltps;

            foreach (TaskReport pp in models)
            {
                if (currentMonth != null)
                    pltps = pp.Pltps.Where(n => n.TaskReport.Periode.Value.Month == currentMonth);
                else
                    pltps = pp.Pltps;

                foreach (Pltp pd in pltps)
                {
                    var temp = ppis.Where(n => n.PowerPlantId == pd.PowerPlantId);
                    price = 0;
                    price = temp.Any() ? temp.FirstOrDefault().NextPrice : 0;
                    result += (pd.EnergyNett.Value * price);
                }
            }

            return result;
        }

        private double CalculatePriceActualSteam(List<TaskReport> models, List<SteamPowerPlantPrice> prices, int? currentMonth = null)
        {
            //kamus
            double result = 0;
            double price = 0;
            IEnumerable<Pltp> pltps;

            foreach (TaskReport pp in models)
            {
                if (currentMonth != null)
                    pltps = pp.Pltps.Where(n => n.TaskReport.Periode.Value.Month == currentMonth);
                else
                    pltps = pp.Pltps;

                foreach (Pltp pd in pltps)
                {
                    var temp = prices.Where(n => n.PowerPlantId == pd.PowerPlantId);
                    price = 0;
                    price = temp.Any() ? temp.FirstOrDefault().Price : 0;
                    result += (pd.SteamUtilization.Value * price);
                }
            }

            return result;
        }

        private async void PrepareDashboard(DashboardOperationPresentationStub model)
        {
            FilterQuery filter = new FilterQuery(FilterLogic.And);
            FilterQuery filterMonth = new FilterQuery(FilterLogic.And);
            List<SortQuery> sorts = new List<SortQuery> { new SortQuery(Field.Periode, SortOrder.Descending) };
            DateTime now = DateTime.Now;
            double plan = 0, planMonth = 0, planMonthProg = 0, planYearProg = 0;

            Task<List<ViewProgressEnergy>> taskViewProgresses;
            Task<List<ProductionPlanDetail>> taskPlanDetail;

            IEnumerable<ViewProgressEnergy> viewProgressMonths, viewProgressMonthsProg;
            IEnumerable<ViewProgressEnergy> viewProgressYears, viewProgressYearsProg;

            IEnumerable<ProductionPlanDetail> plandetail;
            ViewProgressEnergy viewProgress;
            FieldHelper fh = new FieldHelper(Table.ProductionPlan, Field.IsDeleted);

            filter = new FilterQuery(FilterLogic.And);
            filter.AddFilter(fh, FilterOperator.Equals, false);
            if (model.AreaId > 0)
                filter.AddFilter(Field.PlanDetailArea, FilterOperator.Equals, model.AreaId);
            taskPlanDetail = RepoProductionPlanDetail.FindAllAsync(filter);

            filter = new FilterQuery(FilterLogic.And);
            if (model.AreaId > 0)
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, model.AreaId);

            if (model.RangeData == RangeData.DAILY)
            {
                filter.AddFilter(Field.Month, FilterOperator.Equals, model.Month);
                filter.AddFilter(Field.Year, FilterOperator.Equals, model.Year);
            }
            else if (model.RangeData == RangeData.MONTHLY)
            {
                filter.AddFilter(Field.Year, FilterOperator.Equals, model.Year);
            }

            taskViewProgresses = RepoViewProgressEnergyCumulative.FindAllAsync(sorts, filter);

            await Task.WhenAll(taskViewProgresses, taskPlanDetail);

            viewProgressYears = taskViewProgresses.Result;
            plandetail = taskPlanDetail.Result;

            if (viewProgressYears.Any())
            {
                viewProgress = viewProgressYears.FirstOrDefault();
                model.Year = viewProgress.Periode.Value.Year;
                model.LastDateProduction = viewProgress.Periode.Value;
            }
            else
            {
                model.Year = now.Year;
                model.LastDateProduction = now;
            }


            filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, new DateTime(model.LastDateProduction.Value.Year, model.LastDateProduction.Value.Month, 1));
            filter.AddFilter(Field.Periode, FilterOperator.LessThanOrEquals, model.LastDateProduction);
            viewProgressMonths = RepoViewProgressEnergyCumulative.FindAll(filter);

            if (!string.IsNullOrEmpty(model.SOType))
            {
                viewProgressYears = viewProgressYears.Where(n => n.SOType == model.SOType);
                viewProgressMonths = viewProgressMonths.Where(n => n.SOType == model.SOType);
            }

            filter = new FilterQuery(Field.Periode, FilterOperator.Greater, model.LastDateProduction.Value);
            filter.AddFilter(Field.Periode, FilterOperator.LessThanOrEquals, new DateTime(model.LastDateProduction.Value.Year, model.LastDateProduction.Value.Month, DateTime.DaysInMonth(model.LastDateProduction.Value.Year, model.LastDateProduction.Value.Month)));
            viewProgressMonthsProg = RepoViewProgressEnergyCumulative.FindAll(filter);

            filter = new FilterQuery(Field.Periode, FilterOperator.Greater, model.LastDateProduction.Value);
            filter.AddFilter(Field.Periode, FilterOperator.LessThanOrEquals, new DateTime(model.LastDateProduction.Value.Year, 12, 31));
            viewProgressYearsProg = RepoViewProgressEnergyCumulative.FindAll(filter);

            int viewProgressMonthsProgCount = viewProgressMonthsProg?.Count(n => n.EnergyPrice > 0) ?? 1;
            int viewProgressYearsProgCount = viewProgressYearsProg?.Count(n => n.EnergyPrice > 0) ?? 1;

            if (viewProgressYears.Any() || viewProgressMonths.Any())
            {
                model.HasData = true;

                int daysInMonth = DateTime.DaysInMonth(model.LastDateProduction.Value.Year, model.LastDateProduction.Value.Month);
                int daysInYear = DateTime.Now.Year % 4 == 0 ? 366 : 365;
                int dayOfYear = model.LastDateProduction.Value.DayOfYear;

                if (model.ProductionUtilization == ProductionUtilization.ELECTRIC.ToString())
                {
                    plan = plandetail.Where(n => n.ProductionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString() && n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month).Sum(n => n.Value);
                    planMonth = plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString()).Sum(n => n.Value);

                    planMonthProg = plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString()).SelectMany(x => x.ProductionPlanDailies)?.Where(x => x.Day > model.LastDateProduction.Value.Day)?.Sum(x => x.Value) ?? (planMonth / daysInMonth * (daysInMonth - model.LastDateProduction.Value.Day));
                    planYearProg = planMonthProg + plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month > model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString()).Sum(x => x.Value);

                    //boepd properties                    
                    model.PlanElectric = viewProgressYears.Sum(n => n.EnergyPlan.Value);
                    model.PlanElectricBOEPD = viewProgressYears.Sum(n => n.EnergyPlan.Value) * 1.971;
                    model.ActualElectric = viewProgressYears.Sum(n => n.EnergyNett.Value);
                    model.ActualElectricBOEPD = model.ActualElectric * 1.971;

                    model.PlanMonthElectric = planMonth / 1000;
                    model.ActualMonthElectric = viewProgressMonths.Sum(n => n.EnergyNett.Value) / 1000;
                    model.PrognosaMonthElectric = model.ActualMonthElectric + (planMonthProg / 1000);

                    model.PlanYearElectric = plan / 1000;
                    model.ActualYearElectric = viewProgressYears.Sum(n => n.EnergyNett.Value) / 1000;
                    model.PrognosaYearElectric = model.ActualYearElectric + (planYearProg / 1000);

                    model.PlanMonthElectricFinance = (viewProgressMonths.Sum(n => n.EnergyPrice.Value) * 1000 * planMonth) / viewProgressMonths.Count(n => n.EnergyPrice > 0);
                    model.ActualMonthElectricFinance = viewProgressMonths.Sum(n => (n.EnergyNett.Value) * n.EnergyPrice.Value) * 1000;
                    model.PrognosaMonthElectricFinance = model.ActualMonthElectricFinance + ((viewProgressMonthsProg?.Sum(n => n.EnergyPrice.Value) * 1000 * planMonthProg) / (viewProgressMonthsProgCount != 0 ? viewProgressMonthsProgCount : 1) ?? 0);

                    model.PlanYearElectricFinance = (viewProgressYears.Sum(n => n.EnergyPrice.Value) * 1000 * plan) / viewProgressYears.Count(n => n.EnergyPrice > 0);
                    model.ActualYearElectricFinance = viewProgressYears.Sum(n => n.TotalEnergyActual);
                    model.PrognosaYearElectricFinance = model.ActualYearElectricFinance + ((viewProgressYearsProg?.Sum(n => n.EnergyPrice.Value) * 1000 * planYearProg) / (viewProgressYearsProgCount != 0 ? viewProgressYearsProgCount : 1) ?? 0);
                }
                else
                {
                    plan = 0; planMonth = 0;
                    plan = plandetail.Where(n => n.ProductionPlan.UtilType == ProductionUtilization.STEAM.ToString() && n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month).Sum(n => n.Value);
                    planMonth = plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.STEAM.ToString()).Sum(n => n.Value);

                    planMonthProg = plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.STEAM.ToString()).SelectMany(x => x.ProductionPlanDailies)?.Where(x => x.Day > model.LastDateProduction.Value.Day)?.Sum(x => x.Value) ?? (planMonth / daysInMonth * (daysInMonth - model.LastDateProduction.Value.Day));
                    planYearProg = planMonthProg + plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month > model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.STEAM.ToString()).Sum(x => x.Value);

                    //boepd properties                    
                    model.PlanElectric = plan;
                    model.PlanElectricBOEPD = viewProgressYears.Sum(n => n.SteamPlan.Value) * 1.971;
                    model.ActualElectric = viewProgressYears.Sum(n => n.SteamUtilization.Value);
                    model.ActualElectricBOEPD = model.ActualElectric * 1.971;

                    model.PlanMonthElectric = planMonth;
                    model.ActualMonthElectric = viewProgressMonths.Sum(n => n.SteamUtilization.Value);
                    model.PrognosaMonthElectric = model.ActualMonthElectric + (planMonthProg / 1000);

                    model.PlanYearElectric = plan;
                    model.ActualYearElectric = viewProgressYears.Sum(n => n.SteamUtilization.Value);
                    model.PrognosaYearElectric = model.ActualYearElectric + (planYearProg / 1000);


                    model.PlanMonthElectricFinance = viewProgressMonths.Sum(n => n.SteamPrice.Value) * planMonth / viewProgressMonths.Count(n => n.SteamPrice > 0);
                    model.ActualMonthElectricFinance = viewProgressMonths.Sum(n => n.TotalSteamActual);
                    model.PrognosaMonthElectricFinance = model.ActualMonthElectricFinance + ((viewProgressMonthsProg?.Sum(n => n.EnergyPrice.Value) * 1000 * planMonthProg) / (viewProgressMonthsProgCount != 0 ? viewProgressMonthsProgCount : 1) ?? 0);

                    model.PlanYearElectricFinance = viewProgressYears.Sum(n => n.SteamPrice.Value) * plan / viewProgressYears.Count(n => n.SteamPrice > 0);
                    model.ActualYearElectricFinance = viewProgressYears.Sum(n => n.TotalSteamActual);
                    model.PrognosaYearElectricFinance = model.ActualYearElectricFinance + ((viewProgressYearsProg?.Sum(n => n.EnergyPrice.Value) * 1000 * planYearProg) / (viewProgressYearsProgCount != 0 ? viewProgressYearsProgCount : 1) ?? 0);
                }

            }
            else
            {
                model.ListYear = new List<int> { DateTime.Now.Year };
                model.HasData = false;
            }
        }


        private async void PrepareDashboardChart(DashboardOperationPresentationStub model)
        {
            //kamus
            Task<List<Area>> taskAreas;
            Task<List<ProductionPlan>> taskPlans;
            Task<List<TaskReport>> taskProgresses;

            Task<List<Business.Entities.PowerPlant>> taskPowerPlant;

            IEnumerable<Area> areas;
            IEnumerable<ProductionPlan> plans;
            IEnumerable<ProductionPlanDetail> planDetails;
            IEnumerable<TaskReport> progresses;

            IEnumerable<Business.Entities.PowerPlant> powerPlants;

            FilterQuery filter;
            FieldHelper fh = new FieldHelper(Table.PowerPlant, Field.SOType);

            DateTime? startPeriode = null, endPeriode = null;
            FilterQuery filterPowerPlant;
            filterPowerPlant = new FilterQuery(Field.SOType, FilterOperator.Equals, SOType.ZPGE.ToString());

            FilterQuery filterArea = new FilterQuery();
            int areaId = model.AreaId;
            if (areaId != 0)
            {
                filterArea.AddFilter(Field.Id, FilterOperator.Equals, areaId);
            }

            int year = model.YearChart > 0 ? model.YearChart : DateTime.Now.Year;
            int month = model.MonthChart > 0 ? model.MonthChart : DateTime.Now.Month;
            string util = !string.IsNullOrEmpty(model.ProductionUtilizationChart) ? model.ProductionUtilizationChart : ProductionUtilization.ELECTRIC.ToString();



            if (model.RangeData == RangeData.DAILY)
            {
                filter = new FilterQuery();
                filter.AddFilter(Field.Year, FilterOperator.Equals, model.YearChart);
                filter.AddFilter(Field.UtilType, FilterOperator.Equals, util);
                filter.AddFilter(fh, FilterOperator.Equals, SOType.ZPGE);
                if (areaId > 0)
                {
                    fh = new FieldHelper(Table.PowerPlant, Field.AreaId);
                    filter.AddFilter(fh, FilterOperator.Equals, areaId);
                }
                taskPlans = RepoProductionPlan.FindAllAsync(filter);


                startPeriode = new DateTime(model.YearChart, model.MonthChart, 1);
                endPeriode = startPeriode.Value.AddMonths(1);

                filter = new FilterQuery(FilterLogic.And);
                filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, startPeriode);
                filter.AddFilter(Field.Periode, FilterOperator.LessThan, endPeriode);
                filter.AddFilter(Field.Status, FilterOperator.Equals, DataStatus.APPROVED);
                if (areaId > 0)
                    filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
                taskProgresses = RepoTaskReport.FindAllAsync(filter);
            }
            else if (model.RangeData == RangeData.MONTHLY)
            {
                filter = new FilterQuery();
                filter.AddFilter(Field.Year, FilterOperator.Equals, model.YearChart);
                filter.AddFilter(Field.UtilType, FilterOperator.Equals, util);
                filter.AddFilter(fh, FilterOperator.Equals, SOType.ZPGE);
                if (areaId > 0)
                {
                    fh = new FieldHelper(Table.PowerPlant, Field.AreaId);
                    filter.AddFilter(fh, FilterOperator.Equals, areaId);
                }
                taskPlans = RepoProductionPlan.FindAllAsync(filter);


                startPeriode = new DateTime(model.YearChart, 1, 1);
                endPeriode = startPeriode.Value.AddYears(1);

                filter = new FilterQuery(FilterLogic.And);
                filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, startPeriode);
                filter.AddFilter(Field.Periode, FilterOperator.LessThan, endPeriode);
                filter.AddFilter(Field.Status, FilterOperator.Equals, DataStatus.APPROVED);
                if (areaId > 0)
                    filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
                taskProgresses = RepoTaskReport.FindAllAsync(filter);
            }
            else
            {
                startPeriode = null;
                filter = new FilterQuery();
                filter.AddFilter(Field.UtilType, FilterOperator.Equals, util);
                filter.AddFilter(fh, FilterOperator.Equals, SOType.ZPGE);
                if (areaId > 0)
                {
                    fh = new FieldHelper(Table.PowerPlant, Field.AreaId);
                    filter.AddFilter(fh, FilterOperator.Equals, areaId);
                }
                taskPlans = RepoProductionPlan.FindAllAsync(filter);


                filter = new FilterQuery();
                if (areaId > 0)
                    filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
                filter.AddFilter(Field.Status, FilterOperator.Equals, DataStatus.APPROVED);
                taskProgresses = RepoTaskReport.FindAllAsync(filter);
            }

            taskAreas = RepoArea.FindAllAsync(filterArea);
            taskPowerPlant = RepoPowerPlant.FindAllAsync(filterPowerPlant);

            await Task.WhenAll(taskPlans, taskProgresses, taskAreas, taskPowerPlant);

            plans = taskPlans.Result;
            planDetails = plans.SelectMany(n => n.ProductionPlanDetails);
            progresses = taskProgresses.Result;
            powerPlants = taskPowerPlant.Result;
            areas = taskAreas.Result.Where(a => powerPlants.Any(b => b.AreaId == a.Id));

            if (planDetails.Any() && progresses.Any())
            {
                model.ChartHasData = true;
                model.SetChartPlan(plans, planDetails, progresses, startPeriode);
                model.SetChartActual(areas, plans, planDetails, progresses);
            }
            else
            {
                model.ListYear = new List<int> { year };
                model.ChartHasData = false;
            }
        }
        private async Task<DashboardExecutiveModelStub> InitDashboardExecutive(DashboardExecutiveModelStub model)
        {
            if (model != null)
            {
                int? areaId = model.AreaId;
                int? month = null;
                DateTime? periode = null;
                DateTime? endPeriode = null;
                Task<List<TaskReport>> taskReports;
                Task<List<ProductionPlan>> taskPlans;
                Task<List<ViewPpi>> taskEnergyPrices;
                Task<List<SteamPowerPlantPrice>> taskSteamPrices;
                Task<List<ViewCapacityFactor>> taskCapacityFactors;

                List<TaskReport> tasks = new List<TaskReport>();
                List<ProductionPlan> plans = new List<ProductionPlan>();
                List<ViewPpi> energyPrices = new List<ViewPpi>();
                List<SteamPowerPlantPrice> steamPrices = new List<SteamPowerPlantPrice>();
                List<ViewCapacityFactor> capacityFactors = new List<ViewCapacityFactor>();
                FilterQuery filter = new FilterQuery();
                FieldHelper fh = new FieldHelper(Table.PowerPlant, Field.SOType);


                if (model.RangeData == RangeData.DAILY.ToString())
                {
                    periode = new DateTime(model.EndYear, model.Month, 1);
                    endPeriode = periode.Value.AddMonths(1);

                    filter = new FilterQuery();
                    filter.AddFilter(Field.Year, FilterOperator.Equals, model.EndYear);
                    filter.AddFilter(fh, FilterOperator.Equals, SOType.ZPGE);
                    if (areaId > 0)
                    {
                        fh = new FieldHelper(Table.PowerPlant, Field.AreaId);
                        filter.AddFilter(fh, FilterOperator.Equals, areaId);
                    }
                    taskPlans = RepoProductionPlan.FindAllAsync(filter);


                    filter = new FilterQuery();
                    filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, periode);
                    filter.AddFilter(Field.Periode, FilterOperator.LessThan, endPeriode);
                    filter.AddFilter(Field.Status, FilterOperator.Equals, DataStatus.APPROVED);
                    if (areaId > 0)
                        filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
                    taskReports = RepoTaskReport.FindAllAsync(filter);

                    month = model.Month;
                }
                else if (model.RangeData == RangeData.MONTHLY.ToString())
                {
                    periode = new DateTime(model.EndYear, 1, 1);
                    endPeriode = periode.Value.AddYears(1);

                    filter = new FilterQuery();
                    filter.AddFilter(Field.Year, FilterOperator.Equals, model.EndYear);
                    filter.AddFilter(fh, FilterOperator.Equals, SOType.ZPGE);

                    if (areaId > 0)
                    {
                        fh = new FieldHelper(Table.PowerPlant, Field.AreaId);
                        filter.AddFilter(fh, FilterOperator.Equals, areaId);
                    }
                    taskPlans = RepoProductionPlan.FindAllAsync(filter);

                    filter = new FilterQuery();
                    filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, periode);
                    filter.AddFilter(Field.Periode, FilterOperator.LessThan, endPeriode);
                    filter.AddFilter(Field.Status, FilterOperator.Equals, DataStatus.APPROVED);
                    if (areaId > 0)
                        filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);

                    taskReports = RepoTaskReport.FindAllAsync(filter);

                    periode = null;
                }
                else
                {
                    filter = new FilterQuery();
                    filter.AddFilter(fh, FilterOperator.Equals, SOType.ZPGE);
                    if (areaId > 0)
                    {
                        fh = new FieldHelper(Table.PowerPlant, Field.AreaId);
                        filter.AddFilter(fh, FilterOperator.Equals, areaId);
                    }
                    taskPlans = RepoProductionPlan.FindAllAsync(filter);

                    filter = new FilterQuery();
                    if (areaId > 0)
                        filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
                    filter.AddFilter(Field.Status, FilterOperator.Equals, DataStatus.APPROVED);
                    taskReports = RepoTaskReport.FindAllAsync(filter);

                    periode = null;
                }

                filter = new FilterQuery();
                filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZPGE.ToString());
                taskCapacityFactors = RepoViewCapacityFactor.FindAllAsync(filter);


                filter = new FilterQuery();
                filter.AddFilter(Field.Year, FilterOperator.Equals, (model.EndYear));
                taskEnergyPrices = RepoViewPpi.FindAllAsync(filter);

                filter = new FilterQuery();
                filter.AddFilter(Field.Year, FilterOperator.GreaterThanOrEquals, model.EndYear);
                taskSteamPrices = RepoSteamPowerPlantPrice.FindAllAsync(filter);

                await Task.WhenAll(taskReports, taskPlans, taskCapacityFactors, taskEnergyPrices, taskSteamPrices);

                tasks = taskReports.Result;
                plans = taskPlans.Result;
                capacityFactors = taskCapacityFactors.Result;
                energyPrices = taskEnergyPrices.Result;
                steamPrices = taskSteamPrices.Result;

                model = new DashboardExecutiveModelStub(tasks, plans, energyPrices, steamPrices, capacityFactors, periode)
                {
                    AreaId = areaId
                };

            }
            return model;
        }
        #endregion

    }
}
