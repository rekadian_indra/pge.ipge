﻿using System;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class BaseAuthController : Controller
    {
        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            var url = Request.QueryString["ReturnUrl"];
            var identity = filterContext.HttpContext.ApplicationInstance.User.Identity;
            if (identity.IsAuthenticated && filterContext.ActionDescriptor.ActionName.ToLower() != "logoff")
            {
                if (url == null)
                    Response.Redirect("~/");
                else
                    Response.Redirect("~" + url);
            }
            base.OnAuthorization(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Result is RedirectToRouteResult)
            {
                try
                {
                    // put the ModelState into TempData
                    TempData.Add("_MODELSTATE", ModelState);
                }
                catch (Exception)
                {
                    TempData.Clear();
                    // swallow exception
                }
            }

            else if (filterContext.Result is ViewResult && TempData.ContainsKey("_MODELSTATE"))
            {
                // merge modelstate from TempData
                var modelState = TempData["_MODELSTATE"] as ModelStateDictionary;
                foreach (var item in modelState)
                {
                    if (!ModelState.ContainsKey(item.Key))
                        ModelState.Add(item);
                }
            }

            base.OnActionExecuted(filterContext);
        }
    }
}
