﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using System.Web;
using Business.Infrastructure;
using Business.Entities;
using Business.Abstract;
using System.Linq;
using System;
using WebUI.Models.Home;
using Newtonsoft.Json;
using Business.Entities.Views;

namespace WebUI.Controllers
{
    public class HomeControllerOld : BaseController
    {
        public HomeControllerOld(ILogRepository repoLog,
            ISalesOrderRepository repoSalesOrder,
            IAreaRepository repoArea,
            ITaskReportRepository repoTaskReport,
            IProductionPlanRepository repoProductionPlan,
            IInvoiceRepository repoInvoice,
            IPpiRepository repoPPIPrice,
            ISteamPowerPlantPriceRepository repoSteamPrice,
            IViewProgressEnergyRepository repoViewProgressEnergy,
            IViewProgressEnergyCumulativeRepository repoViewProgressEnergyCumulative,
            IWorkflowRepository repoWorkflow,
            IPowerPlantRepository repoPowerPlant,
            IPltpRepository repoPltp,
            IProductionPlanDetailRepository repoPlanDetail)
        {
            RepoLog = repoLog;
            RepoSalesOrder = repoSalesOrder;
            RepoArea = repoArea;
            RepoTaskReport = repoTaskReport;
            RepoProductionPlan = repoProductionPlan;
            RepoInvoice = repoInvoice;
            RepoPPI = repoPPIPrice;
            RepoSteamPowerPlantPrice = repoSteamPrice;
            RepoViewProgressEnergy = repoViewProgressEnergy;
            RepoViewProgressEnergyCumulative = repoViewProgressEnergyCumulative;
            RepoWorkflow = repoWorkflow;
            RepoPowerPlant = repoPowerPlant;
            RepoPltp = repoPltp;
            RepoProductionPlanDetail = repoPlanDetail;
        }

        //if using ModelBinder to get user login use SGUser parameter
        //if using Principal dont get user login SGUser parameter
        //public ActionResult Index(SGUser user)
        //{
        //}
        [Authorize]
        [MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span>", Key = "Dashboard", ParentKey = "Home")]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [HttpPost]
        public async Task<string> GetTopDashboardModel(string soType, string pu, int areaId, int year)
        {
            DashboardOperationPresentationStub model = new DashboardOperationPresentationStub();

            model.SOType = soType;
            model.ProductionUtilization = pu;
            model.AreaId = areaId;
            model.Year = year;

            PrepareDashboard(model);

            if (model.LastDateProduction != null)
            {
                model.LastDateProductionString = model.LastDateProduction.Value.ToString("dd MMM yyyy");
            }
            else
            {
                model.LastDateProductionString = "-";
            }

            await Task.Delay(0);
            return JsonConvert.SerializeObject(model);
        }

        [HttpPost]
        public async Task<string> GetBottomDashboardModel(string puChart, RangeData rangeData, int monthChart, int yearChart)
        {
            DashboardOperationPresentationStub model = new DashboardOperationPresentationStub();

            model.ProductionUtilizationChart = puChart;
            model.RangeData = rangeData;
            model.MonthChart = monthChart;
            model.YearChart = yearChart;

            PrepareDashboardChart(model);

            await Task.Delay(0);
            return JsonConvert.SerializeObject(model);
        }

        [AllowAnonymous]
        [MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span>", Key = "Mobile", ParentKey = "Home")]
        public async Task<ActionResult> Mobile()
        {
            await Task.Delay(0);
            return View("Mobile");
        }

        [Authorize]
        //[MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span> Executive", Key = "DashboardExecutive", ParentKey = "Home")]
        public async Task<ActionResult> Executive()
        {
            await Task.Delay(0);
            return View("Executive_");
        }

        [HttpPost]
        public async Task<string> GetTopDashboardExecutiveModel(int areaId)
        {
            DashboardExecutivePresentationStub model = new DashboardExecutivePresentationStub() { AreaId = areaId };

            //model.AreaId = areaId;

            model = await InitDashboardExecutive(model);

            if (model.LastDateProduction != null)
            {
                model.LastDateProductionStringDate = model.LastDateProduction.Value.ToString("dd MMM yyyy");
            }
            else
            {
                model.LastDateProductionStringDate = "-";
            }

            return JsonConvert.SerializeObject(model);
        }

        //[Authorize]
        //[MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span>", Key = "Dashboard", ParentKey = "Home")]
        //public override async Task<ActionResult> Index()
        //{
        //    //kamus
        //    DashboardOperationPresentationStub model = new DashboardOperationPresentationStub();

        //    model.IsFirstTimeLoad = true;
        //    model.ProductionUtilization = string.IsNullOrEmpty(model.ProductionUtilization) ? ProductionUtilization.ELECTRIC.ToString() : model.ProductionUtilization;
        //    model.SOType = string.IsNullOrEmpty(model.SOType) ? SOType.ZPGE.ToString() : model.SOType;
        //    model.ProductionUtilizationChart = string.IsNullOrEmpty(model.ProductionUtilizationChart) ? ProductionUtilization.ELECTRIC.ToString() : model.ProductionUtilizationChart;
        //    model.RangeData = string.IsNullOrEmpty(model.RangeData.ToString()) ? RangeData.MONTHLY : model.RangeData;

        //    PrepareDashboard(model);
        //    PrepareDashboardChart(model);

        //    await Task.Delay(0);
        //    return View(model);
        //}

        //[AllowAnonymous]
        //[MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span>", Key = "Mobile", ParentKey = "Home")]
        //public async Task<ActionResult> Mobile()
        //{
        //    //kamus
        //    DashboardOperationPresentationStub model = new DashboardOperationPresentationStub();

        //    model.IsFirstTimeLoad = true;
        //    model.ProductionUtilization = string.IsNullOrEmpty(model.ProductionUtilization) ? ProductionUtilization.ELECTRIC.ToString() : model.ProductionUtilization;
        //    model.SOType = string.IsNullOrEmpty(model.SOType) ? SOType.ZPGE.ToString() : model.SOType;
        //    model.ProductionUtilizationChart = string.IsNullOrEmpty(model.ProductionUtilizationChart) ? ProductionUtilization.ELECTRIC.ToString() : model.ProductionUtilizationChart;
        //    model.RangeData = string.IsNullOrEmpty(model.RangeData.ToString()) ? RangeData.MONTHLY : model.RangeData;

        //    PrepareDashboard(model);
        //    PrepareDashboardChart(model);

        //    await Task.Delay(0);
        //    return View("Mobile", model);
        //}

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> Mobile(DashboardOperationPresentationStub model)
        {
            //kamus
            //if (model.AreaId == 0)
            //    return RedirectToAction("Index");

            model.IsFirstTimeLoad = false;

            PrepareDashboard(model);
            PrepareDashboardChart(model);
            await Task.Delay(0);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Index(DashboardOperationPresentationStub model)
        {
            //kamus
            //if (model.AreaId == 0)
            //    return RedirectToAction("Index");

            model.IsFirstTimeLoad = false;

            PrepareDashboard(model);
            PrepareDashboardChart(model);
            await Task.Delay(0);
            return View(model);
        }

        [AllowAnonymous]
        //[MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span> Financial", Key = "DashboardFinancial", ParentKey = "Home")]
        public async Task<ActionResult> Financial()
        {
            await Task.Delay(0);

            return View();
        }

        //[Authorize]
        ////[MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span> Executive", Key = "DashboardExecutive", ParentKey = "Home")]
        //public async Task<ActionResult> Executive()
        //{
        //    DashboardExecutivePresentationStub model = new DashboardExecutivePresentationStub();

        //    model = await InitDashboardExecutive(model);

        //    return View(model);
        //}

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Executive(DashboardExecutivePresentationStub model)
        {
            model = await InitDashboardExecutive(model);

            return View(model);
        }

        [MvcSiteMapNode(Title = "Data Input", Key = "DataInput", ParentKey = "Dashboard")]
        public async Task<ActionResult> InputData()
        {
            await Task.Delay(0);
            List<SelectListItem> statusOption = DisplayFormat.EnumToSelectList<DataStatus>();
            return View("InputData");
        }

        [MvcSiteMapNode(Title = "Manual Input", Key = "ManualInput", ParentKey = "DataInput")]
        public async Task<ActionResult> FormInputData()
        {
            HomeFormStub model = new HomeFormStub();

            await Task.Delay(0);
            return View("FormInputData");
        }

        #region Binding 
        [HttpPost]
        public async Task<string> BindingFinance(int? year = null)
        {
            //lib
            Task<List<Area>> TaskAreas = null;
            Task<List<Invoice>> TaskInvoices = null;
            DashboardFinancePresentationStub model;
            List<Area> Areas = null;
            List<Invoice> Invoices = null;
            FilterQuery filter = new FilterQuery();

            //algorithm
            if (year != null && year > 0)
            {
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.GreaterThanOrEquals, new DateTime(year.Value, 01, 01).ToString());
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.LessThanOrEquals, new DateTime(year.Value, 12, 31).ToString());
            }
            else
            {
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.GreaterThanOrEquals, new DateTime(DateTime.Now.Year, 01, 01).ToString());
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.LessThanOrEquals, DateTime.Now.ToString());
            }
            TaskInvoices = RepoInvoice.FindAllAsync(filter);
            TaskAreas = RepoArea.FindAllAsync();


            //get and count data from db in background thread
            await Task.WhenAll(TaskAreas, TaskInvoices);

            //get callback from task
            Areas = TaskAreas.Result;
            Invoices = TaskInvoices.Result;

            //map db data to model
            model = new DashboardFinancePresentationStub(Invoices, Areas);

            return JsonConvert.SerializeObject(new { data = model });
        }

        protected virtual async Task<List<SelectListItem>> YearByInvoiceAsync()
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Invoice> invoices = await RepoInvoice.FindAllAsync();

            //algorithm
            if (invoices.Any())
            {
                options = invoices.Where(m => m.InvoiceAcceptDate != null && m.Workflow.Name == Common.Enums.Workflow.FINISH.ToString())?
                    .OrderByDescending(x => x.InvoiceAcceptDate)
                    .Select(x => new SelectListItem { Text = x.InvoiceAcceptDate.Value.Year.ToString(), Value = x.InvoiceAcceptDate.Value.Year.ToString() })
                    .Distinct().ToList();
            }
            return options.ToList();
        }

        public async Task<string> BindingPowerPlant()
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Business.Entities.PowerPlant> powerPlants = await RepoPowerPlant.FindAllAsync();

            //algorithm
            powerPlants = powerPlants.OrderBy(x => x.Id).ToList();
            foreach (Business.Entities.PowerPlant s in powerPlants)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = s.Name,
                    Value = s.Id.ToString()
                };

                options.Add(o);
            }

            return JsonConvert.SerializeObject(options.ToList());
        }

        public async Task<ActionResult> BindingDocumentFinancialDashboard(int year, int powerPlantId)
        {
            // kamus
            Business.Entities.PowerPlant powerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(powerPlantId);
            List<Task<Business.Entities.Invoice>> invoiceTasks = new List<Task<Invoice>>();
            List<Business.Entities.SalesOrder> salesOrders;
            List<Business.Entities.Invoice> invoices;
            List<Business.Entities.Workflow> salesOrderWorkflows;
            List<Business.Entities.Workflow> invoiceWorkflows;

            var salesOrderDocumentTable = new object();
            var invoiceDocumentTable = new object();

            // algo
            FinanceDocumentStub model = new FinanceDocumentStub(powerPlant);

            salesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(new DateTime(year, 1, 1), powerPlantId, RangeData.YEARLY);
            if (powerPlant.SOType == SOType.ZPGE.ToString())
            {
                salesOrderWorkflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.ZPGE);
                salesOrderDocumentTable = new FinanceSalesOrderDocumentZPGEStub(salesOrders, salesOrderWorkflows);
            }
            else
            {
                salesOrderWorkflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.ZTS);
                salesOrderDocumentTable = new FinanceSalesOrderDocumentZTSStub(salesOrders, salesOrderWorkflows);
            }
            model.SalesOrderWorkflows = salesOrderWorkflows;

            // invoices
            foreach (Business.Entities.SalesOrder salesOrder in salesOrders)
            {
                invoiceTasks.Add(RepoInvoice.FindBySalesOrderIdAsync(salesOrder.Id));
            }
            Task.WaitAll(invoiceTasks.ToArray());
            invoices = invoiceTasks.Select(x => x.Result).ToList();
            if (powerPlant.Material == Material.ELECTRIC_POWER.ToString())
            {
                invoiceWorkflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.INVEL);
                invoiceDocumentTable = new FinanceInvoiceDocumentElectricStub(invoices, invoiceWorkflows);

                // cek yg satu invoice group
                foreach (FinanceInvoiceDocumentElectricStub.Row row in ((FinanceInvoiceDocumentElectricStub)invoiceDocumentTable).Rows)
                {
                    List<Business.Entities.PowerPlant> sameGroupPowerPlants = await RepoPowerPlant.FindAllByInvoiceGroup(powerPlant.InvoiceGroup);
                    foreach (Business.Entities.PowerPlant sameGroupPowerPlant in sameGroupPowerPlants)
                    {
                        Business.Entities.Invoice curInvoice = invoices?.First(x => x.Id == row.InvoiceId);
                        if (curInvoice != null)
                        {
                            List<Business.Entities.SalesOrder> curSalesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(curInvoice.SalesOrder.Periode, sameGroupPowerPlant.Id);
                            if (curSalesOrders != null && curSalesOrders.Count > 0)
                            {
                                Business.Entities.SalesOrder curSalesOrder = curSalesOrders.FirstOrDefault(x => x.Workflow.Approver == null);
                                if (curSalesOrder != null)
                                {
                                    if (curSalesOrder.Invoices.Count > 0)
                                    {
                                        continue;
                                    }
                                }
                            }
                        }
                        row.IsCoverLetter = false;
                        break;
                    }
                }
            }
            else
            {
                invoiceWorkflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.INVSM);
                invoiceDocumentTable = new FinanceInvoiceDocumentSteamStub(invoices, invoiceWorkflows);

                // cek yg satu invoice group
                foreach (FinanceInvoiceDocumentSteamStub.Row row in ((FinanceInvoiceDocumentSteamStub)invoiceDocumentTable).Rows)
                {
                    List<Business.Entities.PowerPlant> sameGroupPowerPlants = await RepoPowerPlant.FindAllByInvoiceGroup(powerPlant.InvoiceGroup);
                    foreach (Business.Entities.PowerPlant sameGroupPowerPlant in sameGroupPowerPlants)
                    {
                        Business.Entities.Invoice curInvoice = invoices?.First(x => x.Id == row.InvoiceId);
                        if (curInvoice != null)
                        {
                            List<Business.Entities.SalesOrder> curSalesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(curInvoice.SalesOrder.Periode, sameGroupPowerPlant.Id);
                            if (curSalesOrders != null && curSalesOrders.Count > 0)
                            {
                                Business.Entities.SalesOrder curSalesOrder = curSalesOrders.FirstOrDefault(x => x.Workflow.Approver == null);
                                if (curSalesOrder != null)
                                {
                                    if (curSalesOrder.Invoices.Count > 0)
                                    {
                                        continue;
                                    }
                                }
                            }
                        }
                        row.IsCoverLetter = false;
                        break;
                    }
                }
            }
            model.InvoiceWorkflows = invoiceWorkflows;

            return Json(new { soType = model.SOType, material = model.Material, salesOrderDocumentTable = salesOrderDocumentTable, invoiceDocumentTable = invoiceDocumentTable });
        }

        #endregion

        #region Helper

        private double CalculateProductionPlan(List<ProductionPlan> models, int? currentMonth = null, bool isCumulative = true)
        {
            //kamus
            double result = 0;

            foreach (ProductionPlan pp in models)
            {
                if (currentMonth != null)
                {
                    if (isCumulative)
                        result += pp.ProductionPlanDetails.Where(x => x.Month <= currentMonth.Value).Sum(x => x.Value);
                    else
                        result += pp.ProductionPlanDetails.Where(x => x.Month == currentMonth.Value).Sum(x => x.Value);
                }
                else
                    result += pp.ProductionPlanDetails.Sum(x => x.Value);
            }

            return result;
        }

        private double CalculatePricePlan(List<ProductionPlan> models, List<PPI> ppis, int? currentMonth = null, bool isCumulative = true)
        {
            //kamus
            double result = 0;
            double price = 0;
            IEnumerable<ProductionPlanDetail> plans;
            IEnumerable<PPI> prices;

            foreach (ProductionPlan pp in models)
            {
                prices = ppis.Where(n => n.PowerPlantId == pp.PowerPlantId);
                if (currentMonth != null)
                {
                    if (isCumulative)
                        plans = pp.ProductionPlanDetails.Where(n => n.Month <= currentMonth);
                    else
                        plans = pp.ProductionPlanDetails.Where(n => n.Month == currentMonth);
                }
                else
                    plans = pp.ProductionPlanDetails;

                foreach (ProductionPlanDetail pd in plans)
                {
                    price = 0;
                    price = prices.Any() ? prices.FirstOrDefault().NextPrice : 0;
                    result += (pd.Value * price * 1000);
                }
            }

            return result;
        }

        private double CalculatePricePlanSteam(List<ProductionPlan> models, List<SteamPowerPlantPrice> prices, int? currentMonth = null, bool isCumulative = true)
        {
            //kamus
            double result = 0;
            double price = 0;
            IEnumerable<ProductionPlanDetail> plans;
            IEnumerable<SteamPowerPlantPrice> steamPrices;

            foreach (ProductionPlan pp in models)
            {
                steamPrices = prices.Where(n => n.PowerPlantId == pp.PowerPlantId);
                if (currentMonth != null)
                {
                    if (isCumulative)
                        plans = pp.ProductionPlanDetails.Where(n => n.Month <= currentMonth);
                    else
                        plans = pp.ProductionPlanDetails.Where(n => n.Month == currentMonth);
                }
                else
                    plans = pp.ProductionPlanDetails;

                foreach (ProductionPlanDetail pd in plans)
                {
                    price = 0;
                    price = steamPrices.Any() ? steamPrices.FirstOrDefault().Price : 0;
                    result += (pd.Value * price);
                }
            }

            return result;
        }

        private double CalculateProductionActual(List<TaskReport> models, int? currentMonth = null)
        {
            //kamus
            double result = 0;

            foreach (TaskReport pp in models)
            {
                if (currentMonth != null)
                {
                    if (pp.Periode.Value.Month == currentMonth)
                        result += pp.Pltps.Sum(x => x.EnergyNett.Value);
                }
                else
                    result += pp.Pltps.Sum(x => x.EnergyNett.Value);
            }

            return result;
        }

        private double CalculateProductionActualSteam(List<TaskReport> models, int? currentMonth = null)
        {
            //kamus
            double result = 0;

            foreach (TaskReport pp in models)
            {
                if (currentMonth != null)
                {
                    if (pp.Periode.Value.Month == currentMonth)
                        result += pp.Pltps.Sum(x => x.SteamUtilization.Value);
                }
                else
                    result += pp.Pltps.Sum(x => x.SteamUtilization.Value);
            }

            return result;
        }

        private double CalculatePriceActual(List<TaskReport> models, List<PPI> ppis, int? currentMonth = null)
        {
            //kamus
            double result = 0;
            double price = 0;
            IEnumerable<Pltp> pltps;

            foreach (TaskReport pp in models)
            {
                if (currentMonth != null)
                    pltps = pp.Pltps.Where(n => n.TaskReport.Periode.Value.Month == currentMonth);
                else
                    pltps = pp.Pltps;

                foreach (Pltp pd in pltps)
                {
                    var temp = ppis.Where(n => n.PowerPlantId == pd.PowerPlantId);
                    price = 0;
                    price = temp.Any() ? temp.FirstOrDefault().NextPrice : 0;
                    result += (pd.EnergyNett.Value * price);
                }
            }

            return result;
        }

        private double CalculatePriceActualSteam(List<TaskReport> models, List<SteamPowerPlantPrice> prices, int? currentMonth = null)
        {
            //kamus
            double result = 0;
            double price = 0;
            IEnumerable<Pltp> pltps;

            foreach (TaskReport pp in models)
            {
                if (currentMonth != null)
                    pltps = pp.Pltps.Where(n => n.TaskReport.Periode.Value.Month == currentMonth);
                else
                    pltps = pp.Pltps;

                foreach (Pltp pd in pltps)
                {
                    var temp = prices.Where(n => n.PowerPlantId == pd.PowerPlantId);
                    price = 0;
                    price = temp.Any() ? temp.FirstOrDefault().Price : 0;
                    result += (pd.SteamUtilization.Value * price);
                }
            }

            return result;
        }

        private async void PrepareDashboard(DashboardOperationPresentationStub model)
        {
            FilterQuery filter = new FilterQuery(FilterLogic.And);
            FilterQuery filterMonth = new FilterQuery(FilterLogic.And);
            List<SortQuery> sorts = new List<SortQuery> { new SortQuery(Field.Periode, SortOrder.Descending) };
            DateTime now = DateTime.Now;
			double plan = 0, planMonth = 0, planMonthProg = 0, planYearProg = 0;

            Task<List<ViewProgressEnergy>> taskViewProgresses;
            Task<List<ProductionPlanDetail>> taskPlanDetail;

            IEnumerable<ViewProgressEnergy> viewProgressMonths, viewProgressMonthsProg;
            IEnumerable<ViewProgressEnergy> viewProgressYears, viewProgressYearsProg;
            IEnumerable<ProductionPlanDetail> plandetail;
            ViewProgressEnergy viewProgress;
            FieldHelper fh = new FieldHelper(Table.ProductionPlan, Field.IsDeleted);

            filter = new FilterQuery(FilterLogic.And);
            filter.AddFilter(fh, FilterOperator.Equals, false);
            if (model.AreaId > 0)
                filter.AddFilter(Field.PlanDetailArea, FilterOperator.Equals, model.AreaId);
            taskPlanDetail = RepoProductionPlanDetail.FindAllAsync(filter);

            filter = new FilterQuery(FilterLogic.And);
            if (model.AreaId > 0)
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, model.AreaId);

            filter.AddFilter(Field.Year, FilterOperator.Equals, model.Year);

            taskViewProgresses = RepoViewProgressEnergyCumulative.FindAllAsync(sorts, filter);



            await Task.WhenAll(taskViewProgresses, taskPlanDetail);

            viewProgressYears = taskViewProgresses.Result;
            plandetail = taskPlanDetail.Result;

            if (viewProgressYears.Any())
            {
                viewProgress = viewProgressYears.FirstOrDefault();
                model.Year = viewProgress.Periode.Value.Year;
                model.LastDateProduction = viewProgress.Periode.Value;
            }
            else
            {
                model.Year = now.Year;
                model.LastDateProduction = now;
            }


            filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, new DateTime(model.LastDateProduction.Value.Year, model.LastDateProduction.Value.Month, 1));
            filter.AddFilter(Field.Periode, FilterOperator.LessThanOrEquals, model.LastDateProduction);
            viewProgressMonths = RepoViewProgressEnergyCumulative.FindAll(filter);

            if (!string.IsNullOrEmpty(model.SOType))
            {
                viewProgressYears = viewProgressYears.Where(n => n.SOType == model.SOType);
                viewProgressMonths = viewProgressMonths.Where(n => n.SOType == model.SOType);
			}

			filter = new FilterQuery(Field.Periode, FilterOperator.Greater, model.LastDateProduction.Value);
			filter.AddFilter(Field.Periode, FilterOperator.LessThanOrEquals, new DateTime(model.LastDateProduction.Value.Year, model.LastDateProduction.Value.Month, DateTime.DaysInMonth(model.LastDateProduction.Value.Year, model.LastDateProduction.Value.Month)));
			viewProgressMonthsProg = RepoViewProgressEnergyCumulative.FindAll(filter);

			filter = new FilterQuery(Field.Periode, FilterOperator.Greater, model.LastDateProduction.Value);
			filter.AddFilter(Field.Periode, FilterOperator.LessThanOrEquals, new DateTime(model.LastDateProduction.Value.Year, 12, 31));
			viewProgressYearsProg = RepoViewProgressEnergyCumulative.FindAll(filter);

			int viewProgressMonthsProgCount = viewProgressMonthsProg?.Count(n => n.EnergyPrice > 0) ?? 1;
			int viewProgressYearsProgCount = viewProgressYearsProg?.Count(n => n.EnergyPrice > 0) ?? 1;

			if (viewProgressYears.Any() || viewProgressMonths.Any())
            {
                model.HasData = true;

				int daysInMonth = DateTime.DaysInMonth(model.LastDateProduction.Value.Year, model.LastDateProduction.Value.Month);
				int daysInYear = DateTime.Now.Year % 4 == 0 ? 366 : 365;
				int dayOfYear = model.LastDateProduction.Value.DayOfYear;

				if (model.ProductionUtilization == ProductionUtilization.ELECTRIC.ToString())
                {
                    plan = plandetail.Where(n => n.ProductionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString() && n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month).Sum(n => n.Value);
                    planMonth = plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString()).Sum(n => n.Value);

					planMonthProg = plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString()).SelectMany(x => x.ProductionPlanDailies)?.Where(x => x.Day > model.LastDateProduction.Value.Day)?.Sum(x => x.Value) ?? (planMonth / daysInMonth * (daysInMonth - model.LastDateProduction.Value.Day));
					planYearProg = planMonthProg + plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month > model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString()).Sum(x => x.Value);

					//boepd properties                    
					model.PlanElectric = plan;
                    model.PlanElectricBOEPD = model.PlanElectric * 1.971;
                    model.ActualElectric = viewProgressYears.Sum(n => n.EnergyNett.Value);
                    model.ActualElectricBOEPD = model.ActualElectric * 1.971;

                    model.PlanMonthElectric = planMonth / 1000;
                    model.ActualMonthElectric = viewProgressMonths.Sum(n => n.EnergyNett.Value) / 1000;
					model.PrognosaMonthElectric = model.ActualMonthElectric + (planMonthProg / 1000);
					
					model.PlanYearElectric = plan / 1000;
                    model.ActualYearElectric = viewProgressYears.Sum(n => n.EnergyNett.Value) / 1000;
					model.PrognosaYearElectric = model.ActualYearElectric + (planYearProg / 1000);
					
					model.PlanMonthElectricFinance = (viewProgressMonths.Sum(n => n.EnergyPrice.Value) * 1000 * planMonth) / viewProgressMonths.Count(n => n.EnergyPrice > 0);
                    model.ActualMonthElectricFinance = viewProgressMonths.Sum(n => (n.EnergyNett.Value) * n.EnergyPrice.Value) * 1000;
					model.PrognosaMonthElectricFinance = model.ActualMonthElectricFinance + ((viewProgressMonthsProg?.Sum(n => n.EnergyPrice.Value) * 1000 * planMonthProg) / (viewProgressMonthsProgCount != 0 ? viewProgressMonthsProgCount : 1) ?? 0);

					model.PlanYearElectricFinance = (viewProgressYears.Sum(n => n.EnergyPrice.Value) * 1000 * plan) / viewProgressYears.Count(n => n.EnergyPrice > 0);
                    model.ActualYearElectricFinance = viewProgressYears.Sum(n => n.TotalEnergyActual);
					model.PrognosaYearElectricFinance = model.ActualYearElectricFinance + ((viewProgressYearsProg?.Sum(n => n.EnergyPrice.Value) * 1000 * planYearProg) / (viewProgressYearsProgCount != 0 ? viewProgressYearsProgCount : 1) ?? 0);
				}
				else
                {
                    plan = 0; planMonth = 0;
                    plan = plandetail.Where(n => n.ProductionPlan.UtilType == ProductionUtilization.STEAM.ToString() && n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month).Sum(n => n.Value);
                    planMonth = plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.STEAM.ToString()).Sum(n => n.Value);

					planMonthProg = plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.STEAM.ToString()).SelectMany(x => x.ProductionPlanDailies)?.Where(x => x.Day > model.LastDateProduction.Value.Day)?.Sum(x => x.Value) ?? (planMonth / daysInMonth * (daysInMonth - model.LastDateProduction.Value.Day));
					planYearProg = planMonthProg + plandetail.Where(n => n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month > model.LastDateProduction.Value.Month && n.ProductionPlan.UtilType == ProductionUtilization.STEAM.ToString()).Sum(x => x.Value);

					//boepd properties                    
					model.PlanElectric = plan;
                    model.PlanElectricBOEPD = model.PlanElectric * 1.971;
                    model.ActualElectric = viewProgressYears.Sum(n => n.SteamUtilization.Value);
                    model.ActualElectricBOEPD = model.ActualElectric * 1.971;

                    model.PlanMonthElectric = planMonth;
                    model.ActualMonthElectric = viewProgressMonths.Sum(n => n.SteamUtilization.Value);
					model.PrognosaMonthElectric = model.ActualMonthElectric + (planMonthProg / 1000);

					model.PlanYearElectric = plan;
                    model.ActualYearElectric = viewProgressYears.Sum(n => n.SteamUtilization.Value);
					model.PrognosaYearElectric = model.ActualYearElectric + (planYearProg / 1000);


					model.PlanMonthElectricFinance = viewProgressMonths.Sum(n => n.SteamPrice.Value) * planMonth / viewProgressMonths.Count(n => n.SteamPrice > 0);
                    model.ActualMonthElectricFinance = viewProgressMonths.Sum(n => n.TotalSteamActual);
					model.PrognosaMonthElectricFinance = model.ActualMonthElectricFinance + ((viewProgressMonthsProg?.Sum(n => n.EnergyPrice.Value) * 1000 * planMonthProg) / (viewProgressMonthsProgCount != 0 ? viewProgressMonthsProgCount : 1) ?? 0);

					model.PlanYearElectricFinance = viewProgressYears.Sum(n => n.SteamPrice.Value) * plan / viewProgressYears.Count(n => n.SteamPrice > 0);
                    model.ActualYearElectricFinance = viewProgressYears.Sum(n => n.TotalSteamActual);
					model.PrognosaYearElectricFinance = model.ActualYearElectricFinance + ((viewProgressYearsProg?.Sum(n => n.EnergyPrice.Value) * 1000 * planYearProg) / (viewProgressYearsProgCount != 0 ? viewProgressYearsProgCount : 1) ?? 0);
				}

			}
            else
            {
                model.ListYear = new List<int> { DateTime.Now.Year };
                model.HasData = false;
            }
        }

        //private void PrepareDashboardChart(DashboardOperationPresentationStub model)
        //{
        //    //kamus
        //    Task<List<ProductionPlan>> taskProductionPlans;
        //    List<ProductionPlan> ppEd;
        //    Task<List<Area>> taskAreas;
        //    Task<List<TaskReport>> taskTaskReports;
        //    List<ProductionPlan> productionPlans;
        //    TaskReport taskReport;
        //    Task<TaskReport> taskTaskReport;
        //    int year = DateTime.Now.Year;
        //    FilterQuery filter;

        //    //algo
        //    //taskReport = RepoTaskReport.GetLastData(null, year);

        //    if (model.RangeData == RangeData.DAILY)
        //    {
        //        taskTaskReport = RepoTaskReport.GetLastDataAsync(null, model.MonthChart, model.YearChart);
        //    }
        //    else if (model.RangeData == RangeData.MONTHLY)
        //    {
        //        taskTaskReport = RepoTaskReport.GetLastDataAsync(null, null, model.YearChart);
        //    }
        //    else
        //    {
        //        taskTaskReport = RepoTaskReport.GetLastDataAsync(null, null, null);
        //    }
        //    Task.WhenAll(taskTaskReport);

        //    taskReport = taskTaskReport.Result;

        //    if (taskReport != null)
        //    {
        //        model.ChartHasData = true;
        //        model.ProductionUtilizationChart = model.ProductionUtilizationChart == null ? ProductionUtilization.ELECTRIC.ToString() : model.ProductionUtilizationChart;

        //        //get plan data electric
        //        //taskProductionPlans = RepoProductionPlan.GetFromPowerPlantIdAsync(null, year, ProductionUtilization.ELECTRIC);
        //        if (!string.IsNullOrEmpty(model.ProductionUtilizationChart))
        //        {
        //            //taskProductionPlans = RepoProductionPlan.GetFromPowerPlantIdAsync(null, year, (ProductionUtilization)Enum.Parse(typeof(ProductionUtilization), model.ProductionUtilization));
        //            taskProductionPlans = RepoProductionPlan.GetFromPowerPlantIdAsync(null, null, (ProductionUtilization)Enum.Parse(typeof(ProductionUtilization), model.ProductionUtilizationChart));
        //        }
        //        else
        //        {
        //            taskProductionPlans = RepoProductionPlan.GetFromPowerPlantIdAsync(null, null, ProductionUtilization.ELECTRIC);
        //            //taskProductionPlans = RepoProductionPlan.GetFromPowerPlantIdAsync(null, year);
        //        }

        //        taskAreas = RepoArea.FindAllAsync();

        //        filter = new FilterQuery(Field.Status, FilterOperator.Equals, DataStatus.APPROVED.ToString());
        //        taskTaskReports = RepoTaskReport.FindAllAsync(filter);

        //        Task.WhenAll(taskProductionPlans, taskAreas, taskTaskReports);

        //        productionPlans = taskProductionPlans.Result;
        //        filter = new FilterQuery(Field.UtilType, FilterOperator.Equals, ProductionUtilization.DAYSEFFECTIVE.ToString());
        //        ppEd = RepoProductionPlan.FindAllAsync(filter).Result;
        //        if (productionPlans.Any())
        //        {
        //            //set grafik monthly and yearly
        //            model.SetPlanYearlyMonthly(productionPlans, ppEd);
        //            model.SetActualYearlyMonthly(taskTaskReports.Result, taskAreas.Result, model.MonthChart, model.YearChart);
        //        }
        //        else
        //        {
        //            model.ListYear = new List<int> { DateTime.Now.Year };
        //            model.ChartHasData = false;
        //        }
        //    }
        //    else
        //    {
        //        model.ListYear = new List<int> { DateTime.Now.Year };
        //        model.ChartHasData = false;
        //    }
        //}

        private async void PrepareDashboardChart(DashboardOperationPresentationStub model)
        {
            //kamus
            Task<List<Area>> taskAreas;
            Task<List<ProductionPlan>> taskPlans;
            Task<List<ProductionPlanDetail>> taskPlanDetails;
            Task<List<ViewProgressEnergy>> taskProgresses;

            IEnumerable<Area> areas;
            IEnumerable<ProductionPlan> plans;
            IEnumerable<ProductionPlanDetail> planDetails;
            IEnumerable<ViewProgressEnergy> progresses;

            FilterQuery filter;
            FilterQuery filterPlan;
            FilterQuery filterPlanDetail;
            FieldHelper fh;

            int year = model.YearChart > 0 ? model.YearChart : DateTime.Now.Year;
            int month = model.MonthChart > 0 ? model.MonthChart : DateTime.Now.Month;
            string util = !string.IsNullOrEmpty(model.ProductionUtilizationChart) ? model.ProductionUtilizationChart : ProductionUtilization.ELECTRIC.ToString();


            filter = new FilterQuery(FilterLogic.And);

            filterPlan = new FilterQuery(FilterLogic.And);
            filterPlan.AddFilter(Field.UtilType, FilterOperator.Equals, util);


            filterPlanDetail = new FilterQuery(FilterLogic.And);
            fh = new FieldHelper(Table.ProductionPlan, Field.IsDeleted);
            filterPlanDetail.AddFilter(fh, FilterOperator.Equals, false);

            fh = new FieldHelper(Table.ProductionPlan, Field.UtilType);
            filterPlanDetail.AddFilter(fh, FilterOperator.Equals, util);


            if (model.RangeData != RangeData.YEARLY)
            {
                filter.AddFilter(Field.Year, FilterOperator.Equals, year);

                filterPlan.AddFilter(Field.Year, FilterOperator.Equals, year);

                fh = new FieldHelper(Table.ProductionPlan, Field.Year);
                filterPlanDetail.AddFilter(fh, FilterOperator.Equals, year);

                if (model.RangeData == RangeData.DAILY)
                {
                    filter.AddFilter(Field.Month, FilterOperator.Equals, month);

                    filterPlanDetail.AddFilter(Field.Month, FilterOperator.Equals, month);
                }
            }

            taskPlans = RepoProductionPlan.FindAllAsync(filterPlan);
            taskPlanDetails = RepoProductionPlanDetail.FindAllAsync(filterPlanDetail);
            taskProgresses = RepoViewProgressEnergyCumulative.FindAllAsync(filter);
            taskAreas = RepoArea.FindAllAsync();

            await Task.WhenAll(taskPlans, taskPlanDetails, taskProgresses, taskAreas);

            plans = model.RangeData == RangeData.DAILY ? taskPlanDetails.Result.Where(n => n.ProductionPlan.IsDeleted == false).Select(n => n.ProductionPlan) : taskPlans.Result;
            planDetails = taskPlanDetails.Result;
            progresses = taskProgresses.Result;
            areas = taskAreas.Result;

            if (planDetails.Any() && progresses.Any())
            {
                model.ChartHasData = true;
                model.SetChartPlan(plans, planDetails, progresses);
                model.SetChartActual(areas, plans, planDetails, progresses);
            }
            else
            {
                model.ListYear = new List<int> { year };
                model.ChartHasData = false;
            }
        }

        private async Task<DashboardExecutivePresentationStub> InitDashboardExecutive(DashboardExecutivePresentationStub model)
        {
            if (model != null)
            {
                int? areaId = model.AreaId;
                double plan = 0, planMonth = 0;
                Task<List<ViewProgressEnergy>> taskEnergies;
                Task<List<ViewProgressEnergy>> taskEnergiesCumulative;
                Task<List<ViewProgressEnergy>> taskProgresses;
                Task<List<ProductionPlanDetail>> taskPlanDetail;
                Task<List<Pltp>> taskPltps;


                IEnumerable<ProductionPlanDetail> planDetail;
                List<ViewProgressEnergy> energies = new List<ViewProgressEnergy>();
                List<ViewProgressEnergy> energiesCumulative = new List<ViewProgressEnergy>();
                List<ViewProgressEnergy> progresses = new List<ViewProgressEnergy>();
                List<Pltp> pltps;

                FilterQuery filter = new FilterQuery(FilterLogic.And);
                FieldHelper fh = new FieldHelper(Table.ProductionPlan, Field.IsDeleted);
                DateTime? lastDate;

                filter.AddFilter(fh, FilterOperator.Equals, false);
                if (model.AreaId.HasValue && model.AreaId > 0)
                    filter.AddFilter(Field.PlanDetailArea, FilterOperator.Equals, model.AreaId);

                taskPlanDetail = RepoProductionPlanDetail.FindAllAsync(filter);

                filter = new FilterQuery(FilterLogic.And);
                fh = new FieldHelper(Table.TaskReport, Field.Status);

                if (model.AreaId.HasValue && model.AreaId > 0)
                    filter.AddFilter(Field.AreaId, FilterOperator.Equals, model.AreaId);
                taskEnergies = RepoViewProgressEnergy.FindAllAsync(filter);
                taskProgresses = RepoViewProgressEnergyCumulative.FindAllAsync(filter);

                filter = new FilterQuery(FilterLogic.And);
                filter.AddFilter(fh, FilterOperator.Equals, DataStatus.APPROVED);
                filter.AddFilter(Field.TaskReportId, FilterOperator.IsNotNull);

                if (model.AreaId.HasValue && model.AreaId > 0)
                {
                    fh = new FieldHelper(Table.TaskReport, Field.AreaId);
                    filter.AddFilter(fh, FilterOperator.Equals, model.AreaId);
                }
                taskPltps = RepoPltp.FindAllAsync(filter);

                await Task.WhenAll(taskPltps, taskProgresses, taskPlanDetail);

                pltps = taskPltps.Result;
                progresses = taskProgresses.Result;
                planDetail = taskPlanDetail.Result;

                if (progresses.Any())
                {
                    lastDate = progresses.Where(n => n.Periode.HasValue).Max(n => n.Periode.Value);
                    energies = progresses.Where(n => n.Periode.Value.Month == lastDate.Value.Month && n.Periode.Value.Year == lastDate.Value.Year).ToList();
                }
                model = new DashboardExecutivePresentationStub(energies, pltps, progresses, planDetail);
                model.AreaId = areaId;

                if (model.LastDateProduction != null)
                {
                    filter = new FilterQuery(FilterLogic.And);
                    filter.AddFilter(Field.Year, FilterOperator.Equals, model.LastDateProduction.Value.Year);
                    if (model.AreaId.HasValue && model.AreaId > 0)
                        filter.AddFilter(Field.AreaId, FilterOperator.Equals, model.AreaId);
                    taskEnergiesCumulative = RepoViewProgressEnergyCumulative.FindAllAsync(filter);

                    await Task.WhenAll(taskEnergiesCumulative);

                    energiesCumulative = taskEnergiesCumulative.Result;
                }

                //energy
                model.EnergyCumulative = new Utilization();

                //actual            
                model.EnergyCumulative.ActualProgress = new Progress();
                model.EnergyCumulative.ActualProgress.Nett = energiesCumulative.Where(n => n.EnergyNett.HasValue).Sum(n => n.EnergyNett.Value);
                model.EnergyCumulative.ActualProgress.Price = energiesCumulative.Sum(n => n.TotalEnergyActual);

                //plan            
                plan = model.LastDateProduction.HasValue ? planDetail.Where(n => n.ProductionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString() && n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month).Sum(n => n.Value) : 0;
                model.EnergyCumulative.PlanProgress = new Progress();
                model.EnergyCumulative.PlanProgress.Nett = plan;
                model.EnergyCumulative.PlanProgress.Price = (energiesCumulative.Sum(n => n.EnergyPrice.Value) * 1000 * plan) / energiesCumulative.Count(n => n.EnergyPrice > 0);
                //model.EnergyCumulative.PlanProgress.Price = (energiesCumulative.Sum(n => n.EnergyPrice.Value) * 1000 * plan);


                //steam            
                model.SteamCumulative = new Utilization();
                plan = 0;

                //actual
                model.SteamCumulative.ActualProgress = new Progress();
                model.SteamCumulative.ActualProgress.Nett = energiesCumulative.Where(n => n.SteamUtilization.HasValue).Sum(n => n.SteamUtilization.Value);
                model.SteamCumulative.ActualProgress.Price = energiesCumulative.Sum(n => n.TotalSteamActual);

                //plan
                plan = model.LastDateProduction.HasValue ? planDetail.Where(n => n.ProductionPlan.UtilType == ProductionUtilization.STEAM.ToString() && n.ProductionPlan.Year == model.LastDateProduction.Value.Year && n.Month <= model.LastDateProduction.Value.Month).Sum(n => n.Value) : 0;
                model.SteamCumulative.PlanProgress = new Progress();
                model.SteamCumulative.PlanProgress.Nett = plan;
                model.SteamCumulative.PlanProgress.Price = energiesCumulative.Sum(n => n.SteamPrice.Value) * plan / energiesCumulative.Count(n => n.SteamPrice > 0);

            }
            return model;
        }
        #endregion  

    }
}
