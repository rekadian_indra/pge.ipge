﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using LogAction.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    //[LogActionFilter]
    public abstract class BaseController : BaseController<object>
    {
    }

    //[LogActionFilter]
    public abstract class BaseController<TFormStub> : Controller
        where TFormStub : class
    {
        public ILogRepository RepoLog { get; set; }
        public ICompanyRepository RepoCompany { get; set; }
        public IAreaRepository RepoArea { get; set; }
        public IPowerPlantRepository RepoPowerPlant { get; set; }
        public IPltpRepository RepoPltp { get; set; }
        public ITaskReportRepository RepoTaskReport { get; set; }
        public IDkpRepository RepoDkp { get; set; }
        public IClusterRepository RepoCluster { get; set; }
        public IWellRepository RepoWell { get; set; }
        public IMstSeparatorRepository RepoMasterSeparator { get; set; }
        public IInterfacePointRepository RepoInterfacePoint { get; set; }
        public IScrubberRepository RepoScrubber { get; set; }
        public IProductionRepository RepoProduction { get; set; }
        public IReInjectionRepository RepoReinjection { get; set; }
        public IMonitoringRepository RepoMonitoring { get; set; }
        public ISeparatorRepository RepoSeparator { get; set; }
        public IInterfaceScrubberRepository RepoInterfaceScrubber { get; set; }
        public IActualPlanningRepository RepoActualPlanning { get; set; }
        public IExplorationRepository RepoExploration { get; set; }
        public IExplorationDetailRepository RepoExplorationDetail { get; set; }
        public IViewExplorationRepository RepoViewExploration { get; set; }
        public ISoldToPartyRepository RepoSoldToParty { get; set; }
        public ISalesOrderRepository RepoSalesOrder { get; set; }
        public IBastFileRepository RepoBastFile { get; set; }
        public IBastFileSignedRepository RepoBastFileSigned { get; set; }
        public IScreenshotSapRepository RepoScreenshotSap { get; set; }
        public IPpiRepository RepoPPI { get; set; }
        public ISteamPowerPlantPriceRepository RepoSteamPowerPlantPrice { get; set; }
        public IProductionPlanRepository RepoProductionPlan { get; set; }
        public IProductionPlanDetailRepository RepoProductionPlanDetail { get; set; }
        public IProductionPlanDailyRepository RepoProductionPlanDaily { get; set; }
        public ISalesOrderApprovalRepository RepoSalesOrderApproval { get; set; }
        public IPpiApprovalRepository RepoPpiApproval { get; set; }
        public ISteamPowerPlantPriceApprovalRepository RepoSteamPowerPlantPriceApproval { get; set; }
        public IInvoiceRepository RepoInvoice { get; set; }
        public IInvoiceFileRepository RepoInvoiceFile { get; set; }
        public IAdditionalInvoiceFileRepository RepoAdditionalInvoiceFile { get; set; }
        public IEFakturFileRepository RepoEFakturFile { get; set; }
        public IInvoiceApprovalRepository RepoInvoiceApproval { get; set; }

        public IModuleRepository RepoModule { get; set; }
        public IUserRepository RepoUser { get; set; }
        public IViewUserAreaRepository RepoUserArea { get; set; }
        public IMembershipRepository RepoMembership { get; set; }
        public IMembershipUserRepository RepoMembershipUser { get; set; }
        public IRoleRepository RepoRole { get; set; }

        public IWorkflowRepository RepoWorkflow { get; set; }
        public IViewProgressEnergyRepository RepoViewProgressEnergy { get; set; }
        public IViewProgressEnergyCumulativeRepository RepoViewProgressEnergyCumulative { get; set; }
        public IViewSalesOrderApprovalRepository RepoViewSalesOrderApproval { get; set; }
        public IAutoNumberRepository RepoAutoNumber { get; set; }
		public IReliabilityPlanRepository RepoReliabilityPlan { get; set; }

        public IWellHistoryRepository RepoWellHistory { get; set; }
        public IViewWellRepository RepoViewWell { get; set; }

		public IViewPltpRepository RepoViewPltp { get; set; }
		public IViewPpiRepository RepoViewPpi { get; set; }
		public IViewProductionPlanDetailRepository RepoViewProductionPlanDetail { get; set; }
		public IViewDkpRepository RepoViewDkp { get; set; }
		public IViewReliabilityPlanRepository RepoViewReliabilityPlan { get; set; }
		public IViewReliabilityPlanYearlyRepository RepoViewReliabilityPlanYearly { get; set; }
		public IViewReliabilityActualRepository RepoViewReliabilityActual { get; set; }
        public IViewInvoicingSalesOrderRepository RepoViewInvoicingSalesOrder { get; set; }
        public IViewInvoicingKpiRepository RepoViewInvoicingKpi { get; set; }
        public IViewCapacityFactorRepository RepoViewCapacityFactor { get; set; }
        public IViewInvoicingRepository RepoViewInvoicing { get; set; }
        public IViewSalesOrderFinanceRepository RepoViewSalesOrderFinance { get; set; }
        public IViewInvoiceRepository RepoViewInvoice { get; set; }
        protected string AppAddress
        {
            get
            {
                string result = Request.ApplicationPath == "/" ?
                    string.Format("{0}://{1}/", Request.Url.Scheme, Request.Url.Authority) :
                    string.Format("{0}://{1}{2}/", Request.Url.Scheme, Request.Url.Authority, Request.ApplicationPath);
                return result;
            }
        }

        protected virtual new Principal User
        {
            get
            {
                return HttpContext.User as Principal;
            }
        }

        public BaseController()
        {
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            ViewBag.AppAddress = AppAddress;
        }

        #region Main action

        public virtual async Task<ActionResult> Index()
        {
            await Task.Delay(0);
            return View();
        }

        public virtual async Task<ActionResult> Create()
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Create(TFormStub model)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        public virtual async Task<ActionResult> Edit(params object[] id)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Edit(TFormStub model)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Delete(params object[] id)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        /**
         * Action to put dataSource options into session when in query event
        */
        public async Task<ActionResult> PutOption(DataSourceOptionsStub model)
        {
            if (model == null)
                throw new ArgumentNullException();

            model.Area = RouteData.DataTokens[RouteValue.Area]?.ToString();

            if (string.IsNullOrEmpty(model.Controller))
                model.Controller = RouteData.Values[RouteValue.Controller].ToString();

            HttpContext.Session[AppSession.DataSourceOption] = JsonConvert.SerializeObject(model);

            await Task.Delay(0);

            return Json(true);
        }
        #endregion

        #region Binding

        public virtual async Task<string> Binding(params object[] args)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        public async Task<string> BindingOption(OptionType? optionType, params object[] args)
        {
            //lib
            List<SelectListItem> options;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            switch (optionType)
            {
                case OptionType.COMPANY:
                    options = await CompanyOptionAsync(param);

                    break;
                case OptionType.AREA:
                    options = await AreaOptionAsync(param);
                    break;
                case OptionType.CLUSTER:
                    options = await ClusterOptionAsync(param, args);
                    break;
                case OptionType.WELL:
                    options = await WellOptionAsync(param, args);
                    break;
                case OptionType.AREAALL:
                    options = await AreaAllOptionAsync(param);
                    break;
                case OptionType.AREA_USER:
                    options = await AreaUserOptionAsync(param);
                    break;
                case OptionType.AREA_ZPGE:
                    options = await AreaZPGEOptionAsync(param);
                    break;
                case OptionType.POWERPLANT:
                    options = await PowerPlanOptionAsync(param, args);

                    break;
                case OptionType.INTERRUPTION_STATUS:
                    options = await InterruptionStatusOptionAsync(param);

                    break;
                case OptionType.DRAWWELL_STATUS:
                    options = await DrawWellStatusOptionAsync(param);

                    break;
                case OptionType.DRAWWELL:
                    options = await DrawWellOptionAsync(param);

                    break;
                case OptionType.DRAWWELL_CATEGORY:
                    options = await DrawWellCategoryOptionAsync(param, args);

                    break;
                case OptionType.TIME_SPAN:
                    options = await TimeSpanOptionAsync(param);

                    break;
                case OptionType.INTERFACE_POINT:
                    options = await InterfacePointOptionAsync(param, args);

                    break;
                case OptionType.SCRUBBER:
                    options = await ScrubberOptionAsync(param, args);

                    break;
                case OptionType.SEPARATOR:
                    options = await SeparatorOptionAsync(param, args);

                    break;
                case OptionType.ENERGY_UNIT:
                    options = await EnergyUnitOptionAsync(param);

                    break;
                case OptionType.PRESSURE_UNIT:
                    options = await PressureUnitOptionAsync(param);

                    break;
                case OptionType.TEMPERATURE_UNIT:
                    options = await TemperatureUnitOptionAsync(param);

                    break;
                case OptionType.FLOW_UNIT:
                    options = await FlowUnitOptionAsync(param);

                    break;
                case OptionType.MUFFLER_UNIT:
                    options = await MufflerUnitOptionAsync(param);

                    break;
                case OptionType.OTHER_UNIT:
                    options = await OtherUnitOptionAsync(param);

                    break;
                case OptionType.INTERRUPTION_LOCATION:
                    options = await IntteruptionLocationOptionAsync(param);

                    break;
                case OptionType.WELL_HISTORY_TARGET:
                    options = await WellHistoryTargetOptionAsync(param);

                    break;
                case OptionType.SHIP_TO_PARTY:
                    options = await ShipToPartyOptionAsync(param);

                    break;
                case OptionType.SO_TYPE:
                    if (args != null && args.Any())
                    {
                        param.Filter = new FilterQuery(FilterLogic.And);
                        param.Filter.AddFilter(Field.Text, FilterOperator.Equals, args[0]);
                    }
                    options = await SoTypeOptionAsync(param);

                    break;
                case OptionType.MATERIAL:
                    options = await MaterialOptionAsync(param);

                    break;
                case OptionType.SOLD_TO_PARTY:
                    options = await SoldToPartyOptionAsync(param);

                    break;
                case OptionType.YEAR_BY_TASK:
                    options = await YearByTaskOptionAsync(param);

                    break;
                case OptionType.RANGE_DATA:
                    options = await RangeDataOptionAsync(param);

                    break;
                case OptionType.RANGE_DATA_MOBILE:
                    options = await RangeDataMobileOptionAsync(param);

                    break;
                case OptionType.USERINFO:
                    options = await LocalUsersOptionAsync(param);
                    break;
                case OptionType.USER:
                    options = await UsersOptionAsync(param);
                    break;
                case OptionType.USERFULLNAME:
                    options = await UsersOptionNameAsync(param);
                    break;
                case OptionType.BUSINESS_OWNER:
                    options = await BusinessOwnerOptionAsync(param);
                    break;
                case OptionType.YEAR_BY_INVOICE:
                    options = await YearByInvoiceAsync(param);
                    break;
                case OptionType.PRODUCT_UTILIZATION:
                    options = await ProductUtilizationOptionAsync(param);
                    break;
                case OptionType.DIAGRAM:
                    options = await DiagramOptionAsync(param);
                    break;
                case OptionType.MONTH:
                    options = await MonthOptionAsync(param);
                    break;
                case OptionType.YEAR:
                    options = await YearOptionAsync(param);
                    break;
                case OptionType.AREA_REPORT:
                    options = await AreaAllReportOptionAsync(param);
                    break;
                case OptionType.REPORT_MODULE:
                    options = await ReportModuleOptionAsync(param);
                    break;
                case OptionType.POWERPLANT_BY_AREA:
                    options = await PowerPlantByAreaOptionAsync(param);
                    break;
                case OptionType.WELL_BY_AREA:
                    options = await WellByAreaOptionAsync(param);
                    break;
                case OptionType.INTERFACE_POINT_BY_AREA:
                    options = await InterfaceByAreaOptionAsync(param);
                    break;
                case OptionType.SCRUBBER_BY_AREA:
                    options = await ScrubberByAreaOptionAsync(param);
                    break;
                case OptionType.SEPARATOR_BY_AREA:
                    options = await SeparatorByAreaOptionAsync(param);
                    break;
                case OptionType.VIEW_OPTIONS:
                    options = await ViewOptionsAsync(param);
                    break;
                default:
                    return null;
            }


            return JsonConvert.SerializeObject(options);
        }

        #endregion

        #region Http404 handling

        protected override void HandleUnknownAction(string actionName)
        {
            // If controller is ErrorController dont 'nest' exceptions
            if (GetType() != typeof(ErrorController))
                InvokeHttp404(HttpContext);
        }

        public ActionResult InvokeHttp404(HttpContextBase httpContext)
        {
            IController errorController = DependencyResolver.Current.GetService<ErrorController>();
            RouteData errorRoute = new RouteData();

            errorRoute.Values.Add("controller", "Error");
            errorRoute.Values.Add("action", "Http404");
            errorRoute.Values.Add("url", httpContext.Request.Url.OriginalString);
            errorController.Execute(new RequestContext(httpContext, errorRoute));

            return new EmptyResult();
        }

        #endregion

        #region Helper
        protected virtual async Task<List<SelectListItem>> PowerPlanOptionAsync(QueryRequestParameter param, params object[] args)
        {
            List<Business.Entities.PowerPlant> powerPlants;
            List<SelectListItem> options = new List<SelectListItem>();
            FilterQuery filter = new FilterQuery();

            //algorithm
            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                              int.Parse(args.ElementAt(0).ToString()) : (int?)null;

                string periode = args.ElementAt(1)?.ToString();
                string selectedPowerPlanId = args.ElementAt(2)?.ToString();
                string tab = args.ElementAt(3)?.ToString();

                if (areaId != null)
                {
                    List<Pltp> pltps;
                    List<Dkp> dkps;
                    List<string> powerPlanIds = null;

                    if (!string.IsNullOrEmpty(periode))
                    {
                        filter = new FilterQuery(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);
                        filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.Equals, periode);
                    }

                    if (tab == TargetTab.TAB_PLTP.ToDescription())
                    {
                        pltps = await RepoPltp.FindAllAsync(filter);
                        powerPlanIds = pltps.Select(x => x.PowerPlantId.Value.ToString()).Distinct().ToList();
                    }
                    else if (tab == TargetTab.TAB_DKP.ToDescription())
                    {
                        dkps = await RepoDkp.FindAllAsync(filter);
                        powerPlanIds = dkps.Select(x => x.PowerPlantId.Value.ToString()).Distinct().ToList();
                    }

                    //trigger on edit state
                    if (!string.IsNullOrEmpty(selectedPowerPlanId) && powerPlanIds != null && powerPlanIds.Any())
                    {
                        if (powerPlanIds.Contains(selectedPowerPlanId))
                            powerPlanIds.Remove(selectedPowerPlanId);
                    }

                    param.InstanceFilter();
                    param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
                    powerPlants = await RepoPowerPlant.FindAllAsync(param.Sorts, param.Filter);

                    foreach (var m in powerPlants)
                    {
                        if (!string.IsNullOrEmpty(tab))
                        {
                            if (!powerPlanIds.Contains(m.Id.ToString()))
                            {
                                SelectListItem o = new SelectListItem
                                {
                                    Text = m.Name,
                                    Value = m.Id.ToString()
                                };

                                options.Add(o);
                            }
                        }
                        else
                        {
                            SelectListItem o = new SelectListItem
                            {
                                Text = m.Name,
                                Value = m.Id.ToString()
                            };

                            options.Add(o);
                        }
                    }
                }
            }

            return options;
        }
        protected virtual async Task<List<SelectListItem>> ClusterOptionAsync(QueryRequestParameter param, params object[] args)
        {
            List<Cluster> dbObject;
            List<SelectListItem> options = new List<SelectListItem>();
            FilterQuery filter = new FilterQuery();
            //algorithm
            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                              int.Parse(args.ElementAt(0).ToString()) : (int?)null;

                if (areaId != null)
                {

                    param.InstanceFilter();
                    param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);

                    if (param.Filter != null)
                    {
                        if (param.Filter.Filters.Any())
                        {
                            if (param.Filter.Filters.Exists(f => f.Field == Field.ClusterId))
                            {
                                param.InstanceFilter();
                                FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.ClusterId);
                                param.Filter.Filters.Remove(filterFmn);
                            }
                            if (param.Filter.Filters.Exists(f => f.Field == Field.WellId))
                            {
                                param.InstanceFilter();
                                FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.WellId);
                                param.Filter.Filters.Remove(filterFmn);
                            }
                        }
                    }

                    dbObject = await RepoCluster.FindAllAsync(param.Sorts, param.Filter);

                    foreach (var m in dbObject)
                    {
                        SelectListItem o = new SelectListItem
                        {
                            Text = m.Name,
                            Value = m.Id.ToString()
                        };

                        options.Add(o);
                    }
                }
            }
            else
            {
                dbObject = await RepoCluster.FindAllAsync();

                foreach (var m in dbObject)
                {
                    SelectListItem o = new SelectListItem
                    {
                        Text = m.Name,
                        Value = m.Id.ToString()
                    };

                    options.Add(o);
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> WellOptionAsync(QueryRequestParameter param, params object[] args)
        {
            List<Well> dbObject;
            List<SelectListItem> options = new List<SelectListItem>();
            FilterQuery filter = new FilterQuery();

            //algorithm
            if (args != null && args.Any())
            {
                int? ClusterId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                              int.Parse(args.ElementAt(0).ToString()) : (int?)null;

                if (ClusterId != null)
                {

                    param.InstanceFilter();
                    param.Filter.AddFilter(Field.ClusterId, FilterOperator.Equals, ClusterId);

                    if (param.Filter != null)
                    {
                        if (param.Filter.Filters.Any())
                        {
                            if (param.Filter.Filters.Exists(f => f.Field == Field.AreaId))
                            {
                                param.InstanceFilter();
                                FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.AreaId);
                                param.Filter.Filters.Remove(filterFmn);
                            }
                            if (param.Filter.Filters.Exists(f => f.Field == Field.WellId))
                            {
                                param.InstanceFilter();
                                FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.WellId);
                                param.Filter.Filters.Remove(filterFmn);
                            }
                        }
                    }

                    dbObject = await RepoWell.FindAllAsync(param.Sorts, param.Filter);

                    foreach (var m in dbObject)
                    {
                        SelectListItem o = new SelectListItem
                        {
                            Text = m.Name,
                            Value = m.Id.ToString()
                        };

                        options.Add(o);
                    }
                }
            }

            return options;
        }
        protected virtual async Task<List<SelectListItem>> DrawWellOptionAsync(QueryRequestParameter param)
        {
            List<ViewWell> dbObjects = null;
            List<SelectListItem> models = new List<SelectListItem>();

            dbObjects = await RepoViewWell.FindViewWellByParamAsync(param.Sorts, param.Filter);

            if (dbObjects != null && dbObjects.Any())
            {
                foreach (ViewWell viewWell in dbObjects)
                {
                    SelectListItem m = new SelectListItem
                    {
                        Value = viewWell.WellId.ToString(),
                        Text = viewWell.WellName
                    };
                    models.Add(m);
                }
            }

            return models;
        }

        protected virtual async Task<List<SelectListItem>> DrawWellCategoryOptionAsync(QueryRequestParameter param, params object[] args)
        {
            IEnumerable<ViewWell> viewWell;
            List<SelectListItem> options = new List<SelectListItem>();
            FilterQuery filter = new FilterQuery();

            //algorithm
            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                int.Parse(args.ElementAt(0).ToString()) : (int?)null;
             
                string wellStatus = "";
                if (args.Length >= 5)
                {
                    wellStatus = args.ElementAt(4).ToString();
                }

                if (areaId != null)
                {
                    DateTime date = DateTime.Now;
                    filter = new FilterQuery(FilterLogic.And);

                    if (args.ElementAt(0) != null)
                        filter.AddFilter(Field.AreaId, FilterOperator.Equals, args.ElementAt(0).ToString());
                    if (args.Length >= 2 && args.ElementAt(1) != null)
                        date = DateTime.Parse(args.ElementAt(1).ToString());
                    if (args.Length >= 3 && args.ElementAt(2) != null && !string.IsNullOrEmpty(args.ElementAt(2).ToString()))
                        filter.AddFilter(Field.WellId, FilterOperator.Equals, args.ElementAt(2).ToString());

                    if (!string.IsNullOrEmpty(wellStatus))
                        filter.AddFilter(Field.Status, FilterOperator.Equals, wellStatus);

                    viewWell = await RepoViewWell.FindAllAsync(filter);
                    viewWell = viewWell.Where(n => n.StartDate.ToLocalDateTime() <= date && n.EndDate >= date);
                    foreach (var m in viewWell)
                    {
                        SelectListItem o = new SelectListItem
                        {
                            Text = m.WellName,
                            Value = m.WellId.ToString()
                        };
                        options.Add(o);
                    }
                }
            }
            return options;
        }

        protected virtual async Task<List<SelectListItem>> InterfacePointOptionAsync(QueryRequestParameter param, params object[] args)
        {
            List<InterfacePoint> dbObjects;
            FilterQuery filter = new FilterQuery();
            List<SelectListItem> options = new List<SelectListItem>();

            //algorithm
            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                              int.Parse(args.ElementAt(0).ToString()) : (int?)null;

                string periode = args.ElementAt(1).ToString();
                string selectedInterfaceId = args.ElementAt(2).ToString();

                if (areaId != null)
                {
                    List<InterfaceScrubber> interfaceScrubbers;
                    List<string> interfaceIds = null;

                    if (!string.IsNullOrEmpty(periode))
                    {
                        filter = new FilterQuery(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);
                        filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.Equals, periode);
                    }

                    interfaceScrubbers = await RepoInterfaceScrubber.FindAllAsync(filter);

                    interfaceIds = interfaceScrubbers.Select(x => x.InterfaceId.ToString()).Distinct().ToList();

                    //trigger on edit state
                    if (!string.IsNullOrEmpty(selectedInterfaceId) && interfaceIds != null && interfaceIds.Any())
                    {
                        if (interfaceIds.Contains(selectedInterfaceId))
                            interfaceIds.Remove(selectedInterfaceId);
                    }

                    param.InstanceFilter();
                    param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
                    dbObjects = await RepoInterfacePoint.FindAllAsync(param.Sorts, param.Filter);

                    foreach (var p in dbObjects)
                    {
                        if (!string.IsNullOrEmpty(periode))
                        {
                            if (!interfaceIds.Contains(p.Id.ToString()))
                            {
                                SelectListItem o = new SelectListItem
                                {
                                    Text = p.Name,
                                    Value = p.Id.ToString()
                                };

                                options.Add(o);
                            }
                        }
                        else
                        {
                            SelectListItem o = new SelectListItem
                            {
                                Text = p.Name,
                                Value = p.Id.ToString()
                            };

                            options.Add(o);
                        }
                    }
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> ScrubberOptionAsync(QueryRequestParameter param, params object[] args)
        {
            List<Scrubber> dbObjects;
            FilterQuery filter = new FilterQuery();
            List<SelectListItem> options = new List<SelectListItem>();

            //algorithm
            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                              int.Parse(args.ElementAt(0).ToString()) : (int?)null;

                string periode = args.ElementAt(1).ToString();
                string selectedInterfaceId = args.ElementAt(2).ToString();

                if (areaId != null)
                {
                    List<InterfaceScrubber> interfaceScrubbers;
                    List<string> scrubberIds = null;

                    if (!string.IsNullOrEmpty(periode))
                    {
                        filter = new FilterQuery(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);
                        filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.Equals, periode);
                    }

                    interfaceScrubbers = await RepoInterfaceScrubber.FindAllAsync(filter);

                    scrubberIds = interfaceScrubbers.Select(x => x.ScrubberId.ToString()).Distinct().ToList();

                    //trigger on edit state
                    if (!string.IsNullOrEmpty(selectedInterfaceId) && scrubberIds != null && scrubberIds.Any())
                    {
                        if (scrubberIds.Contains(selectedInterfaceId))
                            scrubberIds.Remove(selectedInterfaceId);
                    }

                    param.InstanceFilter();
                    param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
                    dbObjects = await RepoScrubber.FindAllAsync(param.Sorts, param.Filter);

                    foreach (var p in dbObjects)
                    {
                        if (!string.IsNullOrEmpty(periode))
                        {
                            if (!scrubberIds.Contains(p.Id.ToString()))
                            {
                                SelectListItem o = new SelectListItem
                                {
                                    Text = p.Name,
                                    Value = p.Id.ToString()
                                };

                                options.Add(o);
                            }
                        }
                        else
                        {
                            SelectListItem o = new SelectListItem
                            {
                                Text = p.Name,
                                Value = p.Id.ToString()
                            };

                            options.Add(o);
                        }
                    }
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> SeparatorOptionAsync(QueryRequestParameter param, params object[] args)
        {
            List<MstSeparator> dbObjects;
            FilterQuery filter = new FilterQuery();
            List<SelectListItem> options = new List<SelectListItem>();

            //algorithm
            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                              int.Parse(args.ElementAt(0).ToString()) : (int?)null;

                string periode = args.ElementAt(1).ToString();
                string selectedSeparatorId = args.ElementAt(2).ToString();

                if (areaId != null)
                {
                    List<Separator> separators;
                    List<string> separatorIds = null;

                    if (!string.IsNullOrEmpty(periode))
                    {
                        filter = new FilterQuery(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);
                        filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.Equals, periode);
                    }

                    separators = await RepoSeparator.FindAllAsync(filter);

                    separatorIds = separators.Select(x => x.SeparatorId.ToString()).Distinct().ToList();

                    //trigger on edit state
                    if (!string.IsNullOrEmpty(selectedSeparatorId) && separatorIds != null && separatorIds.Any())
                    {
                        if (separatorIds.Contains(selectedSeparatorId))
                            separatorIds.Remove(selectedSeparatorId);
                    }

                    param.InstanceFilter();
                    param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);

                    dbObjects = await RepoMasterSeparator.FindAllAsync(param.Sorts, param.Filter);

                    foreach (var p in dbObjects)
                    {
                        if (!string.IsNullOrEmpty(periode))
                        {
                            if (!separatorIds.Contains(p.Id.ToString()))
                            {
                                SelectListItem o = new SelectListItem
                                {
                                    Text = p.Name,
                                    Value = p.Id.ToString()
                                };

                                options.Add(o);
                            }
                        }
                        else
                        {
                            SelectListItem o = new SelectListItem
                            {
                                Text = p.Name,
                                Value = p.Id.ToString()
                            };

                            options.Add(o);
                        }
                    }
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> InterruptionStatusOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<InterruptionStatus>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> DrawWellStatusOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<DrawWellStatus>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> TimeSpanOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<string> timeSpans = TimeSpanHelper.GenerateTimeSpan();

            foreach (var t in timeSpans)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = t,
                    Value = t
                };

                options.Add(o);
            }

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> EnergyUnitOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<EnergyUnit>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> DiagramOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<Diagram>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        //protected virtual async Task<List<SelectListItem>> MstPowerPlantOptionAsync(QueryRequestParameter param, params object[] args)
        //{
        //    List<Business.Entities.PowerPlant> powerPlants;
        //    List<SelectListItem> options = new List<SelectListItem>();

        //    if (args != null && args.Any())
        //    {
        //        int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
        //            int.Parse(args.ElementAt(0).ToString()) : (int?)null;

        //        param.InstanceFilter();
        //        param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
        //        powerPlants = await RepoPowerPlant.FindAllAsync(param.Sorts, param.Filter);

        //        foreach (var m in powerPlants)
        //        {
        //            SelectListItem o = new SelectListItem
        //            {
        //                Text = m.Name,
        //                Value = m.Id.ToString()
        //            };

        //            options.Add(o);
        //        }
        //    }

        //    return options;
        //}

        protected virtual async Task<List<SelectListItem>> PressureUnitOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<PressureUnit>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> TemperatureUnitOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<TemperatueUnit>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> WellHistoryTargetOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<WellHistoryTarget>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> MufflerUnitOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<RockMufflerUnit>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> FlowUnitOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<FlowUnit>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> OtherUnitOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<OtherUnit>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> IntteruptionLocationOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<InterruptionLocation>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> SoTypeOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<SOType>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> MaterialOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<Material>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }


        protected virtual async Task<List<SelectListItem>> RangeDataOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<RangeData>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> RangeDataMobileOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<RangeData>();
            options = options.Where(n => n.Value != RangeData.DAILY.ToString());

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> BusinessOwnerOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<BussinessOwner>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> SoldToPartyOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<SoldToParty> soldToParties = await RepoSoldToParty.FindAllAsync();

            //algorithm
            foreach (SoldToParty s in soldToParties)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = s.Description,
                    Value = s.Id.ToString()
                };

                options.Add(o);
            }

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> YearByTaskOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<TaskReport> reports = await RepoTaskReport.FindAllAsync();

            //algorithm
            if (reports.Any())
            {
                List<int> periods = reports.OrderByDescending(x => x.Periode).Select(x => x.Periode.ToDateTime().Year).Distinct().ToList();

                foreach (int s in periods)
                {
                    SelectListItem o = new SelectListItem
                    {
                        Text = s.ToString(),
                        Value = s.ToString()
                    };

                    options.Add(o);
                }
            }
            else
            {
                SelectListItem o = new SelectListItem
                {
                    Text = DateTime.Now.Year.ToString(),
                    Value = DateTime.Now.Year.ToString()
                };

                options.Add(o);

            }

            return options.ToList();
        }

        //protected virtual async Task<List<SelectListItem>> PowerPlanPeriodeOptionAsync(QueryRequestParameter param, params object[] args)
        //{
        //    List<SelectListItem> options = new List<SelectListItem>();

        //    //algorithm
        //    if (args != null && args.Any())
        //    {
        //        int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
        //                      int.Parse(args.ElementAt(0).ToString()) : (int?)null;
        //        string periode = args.ElementAt(1).ToString();

        //        if (areaId != null && !string.IsNullOrEmpty(periode))
        //        {
        //            List<Business.Entities.PowerPlant> dbObjects;
        //            param.InstanceFilter();
        //            param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
        //            param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.Equals, periode);

        //            dbObjects = await RepoPowerPlant.FindAllAsync(param.Sorts, param.Filter);

        //            foreach (var p in dbObjects)
        //            {
        //                SelectListItem o = new SelectListItem
        //                {
        //                    Text = p.Name,
        //                    Value = p.Id.ToString()
        //                };

        //                options.Add(o);
        //            }

        //        }
        //    }

        //    return options;
        //}

        protected virtual async Task<List<SelectListItem>> CompanyOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            //List<Company> dbObjects = await RepoCompany.FindAllAsync(param.Sorts, param.Filter);

            ////algorithm
            //foreach (Company c in dbObjects)
            //{
            //    SelectListItem o = new SelectListItem
            //    {
            //        Text = c.Name,
            //        Value = c.Id.ToString()
            //    };

            //    options.Add(o);
            //}

            await Task.Delay(0);
            return options;
        }

        protected virtual async Task<List<SelectListItem>> AreaOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Business.Entities.PowerPlant> plants = new List<Business.Entities.PowerPlant>();
            FilterQuery filter = new FilterQuery(FilterLogic.And);
            IEnumerable<int> areaZts;
            Area area;

            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);

            if (User.AreaId.HasValue && User.AreaId.Value > 0)
            {
                area = await RepoArea.FindByPrimaryKeyAsync(User.AreaId.Value);
                if (area.PowerPlants.Any(n => n.SOType == SOType.ZTS.ToString()))
                {
                    filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZTS);
                    plants = RepoPowerPlant.FindAll(filter);
                    areaZts = plants.Any() ? plants.Select(n => n.AreaId.Value).Distinct() : null;

                    filter = new FilterQuery(FilterLogic.Or);
                    foreach (int id in areaZts)
                    {
                        filter.AddFilter(Field.Id, FilterOperator.Equals, id);
                    }

                    param.Filter.AddFilter(filter);
                }
                else
                {
                    param.Filter.AddFilter(Field.Id, FilterOperator.Equals, User.AreaId);
                }
            }

            List<Area> dbObjects = await RepoArea.FindAllAsync(param.Sorts, param.Filter);

            //algorithm
            //SelectListItem a = new SelectListItem
            //{
            //    Text = "Semua Area",
            //    Value = "0"
            //};

            //options.Add(a);

            foreach (Area c in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = c.Name,
                    Value = c.Id.ToString()
                };

                options.Add(o);
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> AreaAllOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Area> dbObjects = await RepoArea.FindAllAsync(param.Sorts, param.Filter);

            //algorithm
            //SelectListItem a = new SelectListItem
            //{
            //    Text = "Semua Area",
            //    Value = "0"
            //};

            //options.Add(a);

            foreach (Area c in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = c.Name,
                    Value = c.Id.ToString()
                };

                options.Add(o);
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> AreaAllReportOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Area> dbObjects = await RepoArea.FindAllAsync();

            options.Add(new SelectListItem
            {
                Text = "Semua",
                Value = "0"
            });

            foreach (Area c in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = c.Name,
                    Value = c.Id.ToString()
                };

                options.Add(o);
            }

            if (param.Sorts != null && param.Sorts.Any())
            {
                SortQuery sort = param.Sorts.FirstOrDefault();

                if (sort.Field.Value.Equals(Field.Value))
                {
                    SortOrder direction = sort.Direction.Value;

                    switch (direction)
                    {
                        default:
                        case SortOrder.Ascending:
                            options = options.OrderBy(m => m.Value.ToInteger()).ToList();

                            break;
                        case SortOrder.Descending:
                            options = options.OrderByDescending(m => m.Value.ToInteger()).ToList();

                            break;

                    }
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> AreaUserOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Area> dbObjects = await RepoArea.FindAllAsync(param.Sorts, param.Filter);
            int? ztsId = null;
            if (dbObjects.Any(n => n.PowerPlants.Any(m => m.SOType == SOType.ZTS.ToString())))
                ztsId = dbObjects.FirstOrDefault(n => n.PowerPlants.Any(m => m.SOType == SOType.ZTS.ToString())).Id;

            foreach (Area c in dbObjects)
            {
                SelectListItem o = new SelectListItem();
                if (c.PowerPlants.Any(n => n.SOType == SOType.ZTS.ToString()))
                {
                    if (!options.Any() && !options.Any(n => n.Text == SOType.ZTS.ToString()))
                    {
                        o = new SelectListItem
                        {
                            Text = SOType.ZTS.ToString(),
                            Value = ztsId.Value.ToString()
                        };
                    }
                }
                else
                {
                    o = new SelectListItem
                    {
                        Text = c.Name,
                        Value = c.Id.ToString()
                    };
                }
                if (o.Text != null)
                    options.Add(o);
            }
            options = options.OrderBy(n => n.Text).ToList();
            return options;
        }
        protected virtual async Task<List<SelectListItem>> AreaZPGEOptionAsync(QueryRequestParameter param)
        {
            //lib
            //List<SelectListItem> options = new List<SelectListItem>();
            //FilterQuery filter = new FilterQuery(FilterLogic.And);
            //filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZTS);
            //IEnumerable<Business.Entities.PowerPlant> plants = RepoPowerPlant.FindAll(filter);
            //List<int> idZts = new List<int>();

            //if (plants.Any())
            //    idZts = plants.Select(n => n.AreaId.Value).Distinct().ToList();

            //if (param.Filter == null)
            //    param.Filter = new FilterQuery(FilterLogic.And);

            //param.Filter.Filters = new List<FilterQuery>();
            //foreach (int id in idZts)
            //{
            //    param.Filter.Filters.Add(new FilterQuery(Field.Id, FilterOperator.NotEquals, id));
            //}

            //List<Area> dbObjects = await RepoArea.FindAllAsync(param.Sorts, param.Filter);

            //foreach (Area c in dbObjects)
            //{
            //    SelectListItem o = new SelectListItem
            //    {
            //        Text = c.Name,
            //        Value = c.Id.ToString()
            //    };

            //    options.Add(o);
            //}

            //return options;

            FilterQuery filterQuery = new FilterQuery(Field.SOType,FilterOperator.Equals,SOType.ZPGE.ToString());
            List<SelectListItem> options = new List<SelectListItem>();
            List<Business.Entities.PowerPlant> powerPlants = new List<Business.Entities.PowerPlant>();
            List<Area> areas = new List<Area>();

            powerPlants = await RepoPowerPlant.FindAllAsync(filterQuery);
            areas = await RepoArea.FindAllAsync(param.Sorts,param.Filter);

            if (areas != null && areas.Any() && powerPlants != null && powerPlants.Any())
            {
                areas = areas.FindAll(a => powerPlants.Any(b => b.AreaId == a.Id));

                foreach (Area area in areas)
                {
                    options.Add(new SelectListItem
                    {
                        Text = area.Name,
                        Value = area.Id.ToString()
                    });
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> ShipToPartyOptionAsync(QueryRequestParameter param, params object[] args)
        {
            //kamus
            List<Business.Entities.PowerPlant> powerPlants;
            List<SelectListItem> options = new List<SelectListItem>();

            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);

            if (User.AreaId.HasValue && User.AreaId.Value > 0)
                param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, User.AreaId);

            //algorithm
            powerPlants = await RepoPowerPlant.FindAllAsync(param.Sorts, param.Filter);

            foreach (Business.Entities.PowerPlant p in powerPlants)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = $"{p.Code} - {p.Name}",
                    Value = p.Id.ToString()
                };

                options.Add(o);
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> LocalUsersOptionAsync(QueryRequestParameter param)
        {


            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Membership> memberships = await RepoMembership.FindAllAsync();

            //algorithm
            //memberships = memberships?.Where(x => x.SignatureFilePath != null && x.EmployeeNo != null && x.FullName != null).ToList();
            memberships = memberships?.Where(x => x.EmployeeNo != null && x.FullName != null).ToList();
            foreach (Membership membership in memberships)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = membership.EmployeeNo + " - " + membership.FullName,
                    Value = membership.EmployeeNo
                };

                options.Add(o);
            }

            if (param.Filter != null)
            {
                if (param.Filter.Filters != null)
                {
                    if (param.Filter.Filters.Exists(m => m.Field == Field.UserName))
                    {
                        string item = param.Filter.Filters.Find(m => m.Field == Field.UserName).Value.ToString().ToUpper();
                        options = options.FindAll(f => f.Text.ToUpper().Contains(item));
                    }                    
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> UsersOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            FilterQuery filter = param.Filter != null && param.Filter.Filters.Any(n => n.Field == Field.UserName) ? param.Filter.Filters.FirstOrDefault(n => n.Field == Field.UserName) : null;
            string key = filter != null ? filter.Value.ToString() : string.Empty;
            IEnumerable<ActiveDirectoryUtil> listUsers = string.IsNullOrEmpty(key) ? new List<ActiveDirectoryUtil>() : ActiveDirectoryUtil.GetUsers(key);
            List<User> dbObjects = await RepoUser.FindAllAsync();

            listUsers = listUsers.Where(m => !dbObjects.Any(n => n.UserName.ToLower() == m.UserName.ToLower()));

            //algorithm
            foreach (ActiveDirectoryUtil u in listUsers)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = u.UserName,
                    Value = u.Email
                };

                options.Add(o);
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> UsersOptionNameAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            FilterQuery filter = param.Filter != null && param.Filter.Filters.Any(n => n.Field == Field.UserName) ? param.Filter.Filters.FirstOrDefault(n => n.Field == Field.UserName) : null;
            string key = filter != null ? filter.Value.ToString() : string.Empty;
            IEnumerable<ActiveDirectoryUtil> listUsers = string.IsNullOrEmpty(key) ? new List<ActiveDirectoryUtil>() : ActiveDirectoryUtil.GetUsers(key);
            List<User> dbObjects = await RepoUser.FindAllAsync();

            listUsers = listUsers.Where(m => !dbObjects.Any(n => n.UserName.ToLower() == m.UserName.ToLower()));

            //algorithm
            foreach (ActiveDirectoryUtil u in listUsers)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = u.UserName,
                    Value = u.FullName
                };

                options.Add(o);
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> YearByInvoiceAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = options = new List<SelectListItem>();
            List<Invoice> invoices = await RepoInvoice.FindAllAsync();
            List<int> listYear = new List<int>();

            //algorithm
            invoices = invoices.FindAll(i => i.InvoiceAcceptDate != null && i.Workflow.Code == Common.Enums.Workflow.FINISH.ToString());

            if (invoices.Any())
            {
                foreach (Invoice invoice in invoices)
                {
                    listYear.Add(invoice.InvoiceAcceptDate.Value.Year);
                }
                listYear = listYear.Distinct().ToList();
                listYear = listYear.OrderByDescending(y => y).ToList();

                foreach (int year in listYear)
                {
                    SelectListItem o = new SelectListItem
                    {
                        Value = year.ToString(),
                        Text = year.ToString()
                    };
                    options.Add(o);
                }

                //options = invoices.Where(m => m.InvoiceAcceptDate != null && m.Workflow.Code == Common.Enums.Workflow.FINISH.ToString())?
                //    .OrderByDescending(x => x.InvoiceAcceptDate)
                //    .Select(x => new SelectListItem { Text = x.InvoiceAcceptDate.Value.Year.ToString(), Value = x.InvoiceAcceptDate.Value.Year.ToString() })
                //    .Distinct().ToList();
            }
            if (!options.Any())
            {
                options = new List<SelectListItem>();
                options.Add(new SelectListItem { Text = DateTime.Now.Year.ToString(), Value = DateTime.Now.Year.ToString() });
            }
            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> ProductUtilizationOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<ProductionUtilization>();
            options = options.Where(n => n.Value != ProductionUtilization.DAYSEFFECTIVE.ToString());

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> MonthOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<Month>();

            foreach (SelectListItem o in options)
            {
                o.Value = Common.Enums.EnumExtension.ToEnum<Month>(o.Text).GetHashCode().ToString();
            }

            if (param.Sorts != null && param.Sorts.Any())
            {
                SortQuery sort = param.Sorts.FirstOrDefault();

                if (sort.Field.Value.Equals(Field.Value))
                {
                    SortOrder direction = sort.Direction.Value;

                    switch (direction)
                    {
                        default:
                        case SortOrder.Ascending:
                            options = options.OrderBy(m => m.Value.ToInteger());

                            break;
                        case SortOrder.Descending:
                            options = options.OrderByDescending(m => m.Value.ToInteger());

                            break;

                    }
                }
            }

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> YearOptionAsync(QueryRequestParameter param)
        {
            //lib
            int startYear = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SystemYearStart"]);
            List<SelectListItem> options = new List<SelectListItem>();

            //algorithm
            for (int i = DateTime.Now.Year; i >= startYear; i--)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                };

                options.Add(o);
            }

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> ReportModuleOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<ReportModule>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> PowerPlantByAreaOptionAsync(QueryRequestParameter param)
        {
            List<Business.Entities.PowerPlant> dbObjects;
            List<SelectListItem> options = new List<SelectListItem>();

            //algorithm
            dbObjects = await RepoPowerPlant.FindAllAsync(param.Sorts, param.Filter);

            if (dbObjects != null && dbObjects.Any())
            {
                foreach (Business.Entities.PowerPlant powerPlant in dbObjects)
                {
                    options.Add(new SelectListItem
                    {
                        Value = powerPlant.Id.ToString(),
                        Text = powerPlant.Name
                    });
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> WellByAreaOptionAsync(QueryRequestParameter param)
        {
            List<Business.Entities.Well> dbObjects;
            List<SelectListItem> options = new List<SelectListItem>();

            //algorithm
            dbObjects = await RepoWell.FindAllAsync(param.Sorts, param.Filter);

            if (dbObjects != null && dbObjects.Any())
            {
                foreach (Business.Entities.Well well in dbObjects)
                {
                    options.Add(new SelectListItem
                    {
                        Value = well.Id.ToString(),
                        Text = well.Description
                    });
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> InterfaceByAreaOptionAsync(QueryRequestParameter param)
        {
            List<Business.Entities.InterfacePoint> dbObjects;
            List<SelectListItem> options = new List<SelectListItem>();

            //algorithm
            dbObjects = await RepoInterfacePoint.FindAllAsync(param.Sorts, param.Filter);

            if (dbObjects != null && dbObjects.Any())
            {
                foreach (Business.Entities.InterfacePoint interfacePoint in dbObjects)
                {
                    options.Add(new SelectListItem
                    {
                        Value = interfacePoint.Id.ToString(),
                        Text = interfacePoint.Name
                    });
                }
            }

            return options;
        }
        protected virtual async Task<List<SelectListItem>> ScrubberByAreaOptionAsync(QueryRequestParameter param)
        {
            List<Business.Entities.Scrubber> dbObjects;
            List<SelectListItem> options = new List<SelectListItem>();

            //algorithm
            dbObjects = await RepoScrubber.FindAllAsync(param.Sorts, param.Filter);

            if (dbObjects != null && dbObjects.Any())
            {
                foreach (Business.Entities.Scrubber scrubber in dbObjects)
                {
                    options.Add(new SelectListItem
                    {
                        Value = scrubber.Id.ToString(),
                        Text = scrubber.Name
                    });
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> SeparatorByAreaOptionAsync(QueryRequestParameter param)
        {
            List<Business.Entities.MstSeparator> dbObjects;
            List<SelectListItem> options = new List<SelectListItem>();

            //algorithm
            dbObjects = await RepoMasterSeparator.FindAllAsync(param.Sorts, param.Filter);

            if (dbObjects != null && dbObjects.Any())
            {
                foreach (Business.Entities.MstSeparator separator in dbObjects)
                {
                    options.Add(new SelectListItem
                    {
                        Value = separator.Id.ToString(),
                        Text = separator.Name
                    });
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> ViewOptionsAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<ViewOptions>();

            //algorithm
            //DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }
        #endregion
    }
}
