﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using System.Web;
using Business.Infrastructure;
using Business.Entities;
using Business.Abstract;
using System.Linq;
using System;
using WebUI.Models.Home;
using Newtonsoft.Json;
using Business.Entities.Views;

namespace WebUI.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController(ILogRepository repoLog,
            IAreaRepository repoArea,
            ITaskReportRepository repoTaskReport,
            IProductionPlanRepository repoProductionPlan,
            IInvoiceRepository repoInvoice,
            IPpiRepository repoPPIPrice,
            ISteamPowerPlantPriceRepository repoSteamPrice,
            IViewProgressEnergyRepository repoViewProgressEnergy,
            IViewProgressEnergyCumulativeRepository repoViewProgressEnergyCumulative,
            IPltpRepository repoPltp)
        {
            RepoLog = repoLog;
            RepoArea = repoArea;
            RepoTaskReport = repoTaskReport;
            RepoProductionPlan = repoProductionPlan;
            RepoInvoice = repoInvoice;
            RepoPPI = repoPPIPrice;
            RepoSteamPowerPlantPrice = repoSteamPrice;
            RepoViewProgressEnergy = repoViewProgressEnergy;
            RepoViewProgressEnergyCumulative = repoViewProgressEnergyCumulative;
            RepoPltp = repoPltp;
        }

        //if using ModelBinder to get user login use SGUser parameter
        //if using Principal dont get user login SGUser parameter
        //public ActionResult Index(SGUser user)
        //{
        //}

        [MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span>", Key = "Dashboard", ParentKey = "Home")]
        public override async Task<ActionResult> Index()
        {
            //kamus
            DashboardOperationPresentationStub model = new DashboardOperationPresentationStub();

            PrepareDashboard(model);
            PrepareDashboardChart(model);
            await Task.Delay(0);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(DashboardOperationPresentationStub model)
        {
            //kamus
            if (model.AreaId == 0)
                return RedirectToAction("Index");

            PrepareDashboard(model);
            PrepareDashboardChart(model);

            await Task.Delay(0);
            return View(model);
        }

        //[MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span> Financial", Key = "DashboardFinancial", ParentKey = "Home")]
        public async Task<ActionResult> Financial()
        {
            await Task.Delay(0);

            return View();
        }

        //[MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span> Executive", Key = "DashboardExecutive", ParentKey = "Home")]
        public async Task<ActionResult> Executive()
        {

            DashboardExecutivePresentationStub model = new DashboardExecutivePresentationStub();
            model = await InitDashboardExecutive(model);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Executive(DashboardExecutivePresentationStub model)
        {
            model = await InitDashboardExecutive(model);

            return View(model);
        }

        [MvcSiteMapNode(Title = "Data Input", Key = "DataInput", ParentKey = "Dashboard")]
        public async Task<ActionResult> InputData()
        {
            await Task.Delay(0);
            List<SelectListItem> statusOption = DisplayFormat.EnumToSelectList<DataStatus>();
            return View("InputData");
        }

        [MvcSiteMapNode(Title = "Manual Input", Key = "ManualInput", ParentKey = "DataInput")]
        public async Task<ActionResult> FormInputData()
        {
            HomeFormStub model = new HomeFormStub();

            await Task.Delay(0);
            return View("FormInputData");
        }

        #region Binding 
        [HttpPost]
        public async Task<string> BindingFinance(int? year = null)
        {
            //lib
            Task<List<Area>> TaskAreas = null;
            Task<List<Invoice>> TaskInvoices = null;
            DashboardFinancePresentationStub model;
            List<Area> Areas = null;
            List<Invoice> Invoices = null;
            FilterQuery filter = new FilterQuery();

            //algorithm
            if (year != null && year > 0)
            {
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.GreaterThanOrEquals, new DateTime(year.Value, 01, 01).ToString());
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.LessThanOrEquals, new DateTime(year.Value, 12, 31).ToString());
            }
            else
            {
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.GreaterThanOrEquals, new DateTime(DateTime.Now.Year, 01, 01).ToString());
                filter.AddFilter(Field.InvoiceAcceptDate, FilterOperator.LessThanOrEquals, DateTime.Now.ToString());
            }
            TaskInvoices = RepoInvoice.FindAllAsync(filter);
            TaskAreas = RepoArea.FindAllAsync();


            //get and count data from db in background thread
            await Task.WhenAll(TaskAreas, TaskInvoices);

            //get callback from task
            Areas = TaskAreas.Result;
            Invoices = TaskInvoices.Result;

            //map db data to model
            model = new DashboardFinancePresentationStub(Invoices, Areas);

            return JsonConvert.SerializeObject(new { data = model });
        }

        protected virtual async Task<List<SelectListItem>> YearByInvoiceAsync()
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Invoice> invoices = await RepoInvoice.FindAllAsync();

            //algorithm
            if (invoices.Any())
            {
                options = invoices.Where(m => m.InvoiceAcceptDate != null && m.Workflow.Name == Common.Enums.Workflow.FINISH.ToString())?
                    .OrderByDescending(x => x.InvoiceAcceptDate)
                    .Select(x => new SelectListItem { Text = x.InvoiceAcceptDate.Value.Year.ToString(), Value = x.InvoiceAcceptDate.Value.Year.ToString() })
                    .Distinct().ToList();
            }
            return options.ToList();
        }

        #endregion

        #region Helper

        private double CalculateProductionPlan(List<ProductionPlan> models, int? currentMonth = null, bool isCumulative = true)
        {
            //kamus
            double result = 0;

            foreach (ProductionPlan pp in models)
            {
                if (currentMonth != null)
                {
                    if (isCumulative)
                        result += pp.ProductionPlanDetails.Where(x => x.Month <= currentMonth.Value).Sum(x => x.Value);
                    else
                        result += pp.ProductionPlanDetails.Where(x => x.Month == currentMonth.Value).Sum(x => x.Value);
                }
                else
                    result += pp.ProductionPlanDetails.Sum(x => x.Value);
            }

            return result;
        }

        private double CalculateProductionActual(List<TaskReport> models, int? currentMonth = null)
        {
            //kamus
            double result = 0;

            foreach (TaskReport pp in models)
            {
                if (currentMonth != null)
                {
                    if (pp.Periode.Value.Month == currentMonth)
                        result += pp.Pltps.Sum(x => x.EnergyNett.Value);
                }
                else
                    result += pp.Pltps.Sum(x => x.EnergyNett.Value);
            }

            return result;
        }

        private void PrepareDashboard(DashboardOperationPresentationStub model)
        {
            Task<List<ProductionPlan>> taskProductionPlans;
            List<ProductionPlan> productionPlans;
            List<TaskReport> taskReports;
            TaskReport taskReport;

            //get last year data
            //check area and year
            if (model.AreaId != 0)
                taskReport = RepoTaskReport.GetLastData(model.AreaId, model.Year);
            else
                taskReport = RepoTaskReport.GetLastData(null, model.Year);

            if (taskReport != null)
            {
                model.HasData = true;
                model.Year = taskReport.Periode.Value.Year;
                model.LastDateProduction = taskReport.Periode.Value;

                //get plan data electric
                taskProductionPlans = RepoProductionPlan.GetFromPowerPlantIdAsync(null, model.Year, ProductionUtilization.ELECTRIC);

                if (model.AreaId != 0)
                {
                    taskReports = RepoTaskReport.GetFromPeriode(model.LastDateProduction, true).Where(x => x.Status == DataStatus.APPROVED.ToString() && x.AreaId == model.AreaId).ToList();
                    productionPlans = taskProductionPlans.Result.Where(x => x.PowerPlant.AreaId == model.AreaId).ToList();
                }
                else
                {
                    taskReports = RepoTaskReport.GetFromPeriode(model.LastDateProduction, true).Where(x => x.Status == DataStatus.APPROVED.ToString()).ToList();
                    productionPlans = taskProductionPlans.Result;
                }

                if (productionPlans.Any())
                {
                    model.PlanElectric = CalculateProductionPlan(productionPlans, model.LastDateProduction.Month);
                    model.PlanElectricBOEPD = model.PlanElectric * 1.971;

                    //need to convert all to kwh
                    model.ActualElectric = CalculateProductionActual(taskReports) / 1000; // convert kwh to mwh
                    model.ActualElectricBOEPD = model.ActualElectric * 1.971;

                    //calculate MTD
                    model.PlanMonthElectric = CalculateProductionPlan(productionPlans, model.LastDateProduction.Month, false) / 1000;
                    //need to convert all to kwh
                    model.ActualMonthElectric = CalculateProductionActual(taskReports, model.LastDateProduction.Month) / 1000000; // convert kwh to gwh

                    //calculate YTD
                    model.PlanYearElectric = CalculateProductionPlan(productionPlans) / 1000; //gwh
                    //need to convert all to kwh
                    model.ActualYearElectric = CalculateProductionActual(taskReports) / 1000000; // convert kwh to gwh

                }
                else
                {
                    model.HasData = false;
                }
            }
            else
            {
                model.HasData = false;
            }
        }

        private void PrepareDashboardChart(DashboardOperationPresentationStub model)
        {
            //kamus
            Task<List<ProductionPlan>> taskProductionPlans;
            Task<List<Area>> taskAreas;
            Task<List<TaskReport>> taskTaskReports;
            List<ProductionPlan> productionPlans;
            TaskReport taskReport;
            int year = DateTime.Now.Year;
            FilterQuery filter;

            //algo
            taskReport = RepoTaskReport.GetLastData(null, year);
            if (taskReport != null)
            {
                model.ChartHasData = true;

                //get plan data electric
                taskProductionPlans = RepoProductionPlan.GetFromPowerPlantIdAsync(null, year, ProductionUtilization.ELECTRIC);
                taskAreas = RepoArea.FindAllAsync();

                filter = new FilterQuery(Field.Status, FilterOperator.Equals, DataStatus.APPROVED.ToString());
                taskTaskReports = RepoTaskReport.FindAllAsync(filter);

                Task.WhenAll(taskProductionPlans, taskAreas, taskTaskReports);

                productionPlans = taskProductionPlans.Result;
                if (productionPlans.Any())
                {
                    //set grafik monthly and yearly
                    model.SetPlanYearlyMonthly(productionPlans);
                    model.SetActualYearlyMonthly(taskTaskReports.Result, taskAreas.Result);
                }
                else
                {
                    model.ChartHasData = false;
                }
            }
            else
            {
                model.ChartHasData = false;
            }
        }

        private async Task<DashboardExecutivePresentationStub> InitDashboardExecutive(DashboardExecutivePresentationStub model)
        {
            int? areaId = model.AreaId;
            Task<List<ViewProgressEnergy>> taskEnergies;
            Task<List<ViewProgressEnergy>> taskSteams;
            Task<List<ViewProgressEnergy>> taskEnergiesCumulative;
            Task<List<ViewProgressEnergy>> taskSteamsCumulative;
            Task<List<Pltp>> taskPltps;

            List<ViewProgressEnergy> energies;
            List<ViewProgressEnergy> steams;
            List<ViewProgressEnergy> energiesCumulative = new List<ViewProgressEnergy>();
            List<ViewProgressEnergy> steamsCumulative = new List<ViewProgressEnergy>();
            List<Pltp> pltps;

            FieldHelper fh = new FieldHelper(Table.TaskReport, Field.Status);
            FilterQuery filter = new FilterQuery(FilterLogic.And);

            filter.AddFilter(Field.Material, FilterOperator.Equals, Material.ELECTRIC_POWER);
            if (model.AreaId.HasValue && model.AreaId > 0)
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, model.AreaId);
            taskEnergies = RepoViewProgressEnergy.FindAllAsync(filter);

            filter = new FilterQuery(FilterLogic.And);
            filter.AddFilter(Field.Material, FilterOperator.Equals, Material.GEOTHERMAL_STEAM);
            if (model.AreaId.HasValue && model.AreaId > 0)
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, model.AreaId);
            taskSteams = RepoViewProgressEnergy.FindAllAsync(filter);

            filter = new FilterQuery(FilterLogic.And);
            filter.AddFilter(fh, FilterOperator.Equals, DataStatus.APPROVED);
            filter.AddFilter(Field.TaskReportId, FilterOperator.IsNotNull);

            if (model.AreaId.HasValue && model.AreaId > 0)
            {
                fh = new FieldHelper(Table.TaskReport, Field.AreaId);
                filter.AddFilter(fh, FilterOperator.Equals, model.AreaId);
            }
            taskPltps = RepoPltp.FindAllAsync(filter);

            await Task.WhenAll(taskEnergies, taskSteams, taskPltps);

            energies = taskEnergies.Result;
            steams = taskSteams.Result;
            pltps = taskPltps.Result;

            model = new DashboardExecutivePresentationStub(energies, steams, pltps);
            model.AreaId = areaId;

            if (model.LastDateProduction != null)
            {
                filter = new FilterQuery(FilterLogic.And);
                filter.AddFilter(Field.Year, FilterOperator.Equals, model.LastDateProduction.Value.Year);
                filter.AddFilter(Field.Material, FilterOperator.Equals, Material.ELECTRIC_POWER);
                if (model.AreaId.HasValue && model.AreaId > 0)
                    filter.AddFilter(Field.AreaId, FilterOperator.Equals, model.AreaId);
                taskEnergiesCumulative = RepoViewProgressEnergyCumulative.FindAllAsync(filter);

                filter = new FilterQuery(FilterLogic.And);
                filter.AddFilter(Field.Year, FilterOperator.Equals, model.LastDateProduction.Value.Year);
                filter.AddFilter(Field.Material, FilterOperator.Equals, Material.GEOTHERMAL_STEAM);
                if (model.AreaId.HasValue && model.AreaId > 0)
                    filter.AddFilter(Field.AreaId, FilterOperator.Equals, model.AreaId);
                taskSteamsCumulative = RepoViewProgressEnergyCumulative.FindAllAsync(filter);

                await Task.WhenAll(taskEnergiesCumulative, taskSteamsCumulative);

                energiesCumulative = taskEnergiesCumulative.Result;
                steamsCumulative = taskSteamsCumulative.Result;
            }

            //energy
            model.EnergyCumulative = new Utilization();

            //actual            
            model.EnergyCumulative.ActualProgress = new Progress();
            model.EnergyCumulative.ActualProgress.Nett = energiesCumulative.Sum(n => n.EnergyNett);
            model.EnergyCumulative.ActualProgress.Price = energiesCumulative.Sum(n => n.TotalEnergyActual);

            //plan            
            model.EnergyCumulative.PlanProgress = new Progress();
            model.EnergyCumulative.PlanProgress.Nett = energiesCumulative.Sum(n => n.AverageEnergyPlan);
            model.EnergyCumulative.PlanProgress.Price = energiesCumulative.Sum(n => n.TotalEnergyPlan);


            //steam            
            model.SteamCumulative = new Utilization();

            //actual
            model.SteamCumulative.ActualProgress = new Progress();
            model.SteamCumulative.ActualProgress.Nett = steamsCumulative.Sum(n => n.SteamUtilization);
            model.SteamCumulative.ActualProgress.Price = steamsCumulative.Sum(n => n.TotalSteamActual);

            //plan
            model.SteamCumulative.PlanProgress = new Progress();
            model.SteamCumulative.PlanProgress.Nett = steamsCumulative.Sum(n => n.AverageSteamPlan);
            model.SteamCumulative.PlanProgress.Price = steamsCumulative.Sum(n => n.TotalSteamPlan);

            return model;
        }
        #endregion  

    }
}
