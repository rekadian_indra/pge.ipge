﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;

namespace WebUI.Controllers
{
    [Authorize]
    public class InvoiceMController : BaseController
    {
        public InvoiceMController(ILogRepository repoLog)
        {
            RepoLog = repoLog;
        }


        [MvcSiteMapNode(Title = "Invoice", Key = "InvoiceM", ParentKey ="Dashboard")]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = "Create Invoice", Key = "CreateInvoiceM", ParentKey = "InvoiceM")]
        public async Task<ActionResult> CreateInvoice()
        {
            await Task.Delay(0);
            return View("Form");
        }

        [MvcSiteMapNode(Title = "Create Invoice", Key = "CreateInvoiceZTSM", ParentKey = "InvoiceM")]
        public async Task<ActionResult> CreateInvoiceZTS()
        {
            await Task.Delay(0);
            return View("FormZTS");
        }

        [MvcSiteMapNode(Title = "Upload Invoice", Key = "UploadInvoiceM", ParentKey = "InvoiceM")]
        public async Task<ActionResult> UploadInvoice()
        {
            await Task.Delay(0);
            return View("UploadInvoice");
        }

    }
}
