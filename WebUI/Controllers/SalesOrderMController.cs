﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;

namespace WebUI.Controllers
{
    [Authorize]
    public class SalesOrderMController : BaseController
    {
        public SalesOrderMController(ILogRepository repoLog)
        {
            RepoLog = repoLog;
        }


        //[MvcSiteMapNode(Title = "Sales Order", Key = "SalesOrder", ParentKey ="Dashboard")]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        //[MvcSiteMapNode(Title = "Create SO", Key = "CreateSO", ParentKey = "SalesOrder")]
        public async Task<ActionResult> CreateSO()
        {
            await Task.Delay(0);
            return View("Form");
        }

        //[MvcSiteMapNode(Title = "Create SO", Key = "CreateSOZTS", ParentKey = "SalesOrder")]
        public async Task<ActionResult> CreateSOZTS()
        {
            await Task.Delay(0);
            return View("FormZTS");
        }

        //[MvcSiteMapNode(Title = "Area Ops. Mgr. Verification", Key = "ApprovalOpsMgr", ParentKey = "SalesOrder")]
        public async Task<ActionResult> Approval()
        {
            await Task.Delay(0);
            return View("Approval");
        }

    }
}
