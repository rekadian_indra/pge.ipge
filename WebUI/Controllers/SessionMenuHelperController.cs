﻿using LogAction.Abstract;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    [AllowAnonymous]
    public class SessionMenuHelperController : BaseController
    {
        public SessionMenuHelperController(ILogRepository repoLog)
        {
            RepoLog = repoLog;
        }

        // GET: SessionMenuHelper
        public void Cookies(string option)
        {
            HttpCookie myCookie = new HttpCookie("targetOption", option);
            Response.Cookies.Add(myCookie);
            var absUri = Request.UrlReferrer.AbsoluteUri;

        }
    }
}