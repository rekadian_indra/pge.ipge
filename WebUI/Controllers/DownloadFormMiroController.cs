﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.SalesOrder.Models;
using WebUI.Infrastructure;
using WebUI.Models;
using WebUI.Models.FormMiro;

namespace WebUI.Controllers
{
    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin })]
    public class DownloadFormMiroController : BaseController<AutoNumberFormStub>
    {
        private const string KEY = "ExcelFile";
        private const string FILE_PATH = "~/Template/";
        private const string FILE_NAME = "Template_AR_Form_MIRO.xlsx";

        public DownloadFormMiroController(
            IViewSalesOrderApprovalRepository repoViewSalesOrderApproval,
            ISalesOrderRepository repoSalesOrder,
            IAreaRepository repoArea,
            IAutoNumberRepository repoAutoNumber,
            ITaskReportRepository repoTaskReport,
            IPltpRepository repoPltp)
        {
            RepoViewSalesOrderApproval = repoViewSalesOrderApproval;
            RepoSalesOrder = repoSalesOrder;
            RepoArea = repoArea;
            RepoAutoNumber = repoAutoNumber;
            RepoTaskReport = repoTaskReport;
            RepoPltp = repoPltp;
        }

        [MvcSiteMapNode(Title = TitleSite.FormMiro, Key = KeySite.IndexFormMiro, ParentKey = KeySite.Dashboard)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        public async Task<ActionResult> CreateFormMiro(int year, int month, string soType)
        {
            FilterQuery filter;
            ResponseModel response = new ResponseModel(true);

            DateTime startDate = new DateTime(year, month, 1);
            DateTime endDate = startDate.AddMonths(1).AddDays(-1);

            filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
            filter.AddFilter(Field.Periode, FilterOperator.LessThanOrEquals, endDate);
            filter.AddFilter(Field.SOType, FilterOperator.Equals, soType);
            List<SalesOrder> salesOrders = await RepoSalesOrder.FindAllAsync(filter);

            if (salesOrders.Any())
            {
                if (string.IsNullOrEmpty(salesOrders.FirstOrDefault().FormMiroNo))
                {
                    //check autonumber
                    bool hasAutonumber = CheckAutoNumber();

                    if (hasAutonumber)
                    {
                        //generate autonumber
                        string autoNumber = GenerateAutoNumber();

                        foreach(SalesOrder m in salesOrders)
                        {
                            m.FormMiroNo = autoNumber;
                            RepoSalesOrder.Save(m);
                        }
                    } else
                    {
                        response.SetFail("Creating form miro failed, no autonumber data found.");
                    }
                }

            }

            return Json(response);
        }

        public async Task<ActionResult> DownloadZpge(int year, int month, int areaId)
        {
            FileStream fileStream;
            FilterQuery filter;
            string filePath = FILE_PATH;
            string fileName = FILE_NAME;

            DownloadParameterStub downloadParam = null;
            XSSFCellStyle style;
            XSSFCellStyle fomatStyle;
            XSSFFont font;
            XSSFSheet sheet = null;
            XSSFRow row = null;
            XSSFCell cell = null;
            XSSFWorkbook workbook;

            ResponseModel response = new ResponseModel(true);

            DateTime startDate = new DateTime(year, month, 1);
            DateTime endDate = startDate.AddMonths(1).AddDays(-1);
            DateTime beforeEndDate = endDate.AddDays(-1);

            //remove warning opening excel
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            //algo
            filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
            filter.AddFilter(Field.Periode, FilterOperator.LessThanOrEquals, endDate);
            filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZPGE.ToString());

            FieldHelper fh = new FieldHelper(Table.PowerPlant, Field.AreaId);
            filter.AddFilter(fh, FilterOperator.Equals, areaId);

            List<SortQuery> sorts = new List<SortQuery>();
            SortQuery sort = new SortQuery(Field.PowerPlantId, SortOrder.Ascending);
            sorts.Add(sort);

            List<SalesOrder> tempSalesOrders = await RepoSalesOrder.FindAllAsync(sorts, filter);

            List<SalesOrder> salesOrders = new List<SalesOrder>();

            foreach (SalesOrder s in tempSalesOrders)
            {
                if (string.IsNullOrEmpty(s.FormMiroNo))
                {
                    string autoNumber = GenerateAutoNumber();
                    s.FormMiroNo = autoNumber;
                    RepoSalesOrder.Save(s);
                }

                salesOrders.Add(s);
            }

            //cari task report
            foreach (SalesOrder s in tempSalesOrders)
            {
                filter = new FilterQuery(Field.Periode, FilterOperator.Equals, beforeEndDate);
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, s.PowerPlant.AreaId);
                filter.AddFilter(Field.Status, FilterOperator.Equals, DataStatus.APPROVED.ToString());

                TaskReport taskReport = RepoTaskReport.Find(filter);
                SalesOrder salesOrder;
                Pltp pltp;

                if (taskReport != null)
                {
                    //cari pltp
                    filter = new FilterQuery(Field.TaskReportId, FilterOperator.Equals, taskReport.Id);
                    filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, s.PowerPlantId);
                    pltp = RepoPltp.Find(filter);

                    if (pltp != null)
                    {
                        salesOrder = new SalesOrder
                        {
                            Periode = beforeEndDate,
                            SOType = s.SOType,
                            PowerPlantId = s.PowerPlantId,
                            Quantity = pltp.EnergyNett,
                            NetPrice = s.NetPrice,
                            NetValue = pltp.EnergyNett * s.NetPrice,
                            FormMiroNo = s.FormMiroNo,
                            PowerPlant = s.PowerPlant
                        };

                        salesOrders.Add(salesOrder);
                    }
                    else
                    {
                        salesOrder = new SalesOrder
                        {
                            Periode = beforeEndDate,
                            SOType = s.SOType,
                            PowerPlantId = s.PowerPlantId,
                            Quantity = 0,
                            NetPrice = 0,
                            NetValue = 0,
                            FormMiroNo = s.FormMiroNo,
                            PowerPlant = s.PowerPlant
                        };
                        salesOrders.Add(salesOrder);
                    }
                }
                else
                {
                    salesOrder = new SalesOrder
                    {
                        Periode = beforeEndDate,
                        SOType = s.SOType,
                        PowerPlantId = s.PowerPlantId,
                        Quantity = 0,
                        NetPrice = 0,
                        NetValue = 0,
                        FormMiroNo = s.FormMiroNo,
                        PowerPlant = s.PowerPlant
                    };
                    salesOrders.Add(salesOrder);
                }
            }

            salesOrders = salesOrders.OrderBy(m => m.PowerPlantId).ToList();

            if (salesOrders.Any())
            {
                if (System.IO.File.Exists(Server.MapPath($"{filePath}{fileName}")))
                {
                    //read file and set to workbook
                    fileStream = new FileStream(Server.MapPath($"{filePath}{fileName}"), FileMode.Open, FileAccess.Read);
                    workbook = new XSSFWorkbook(fileStream);
                    font = (XSSFFont)workbook.CreateFont();
                    font.Color = IndexedColors.White.Index;
                    font.IsBold = true;

                    var format = workbook.CreateDataFormat();

                    style = (XSSFCellStyle)workbook.CreateCellStyle();
                    style.DataFormat = format.GetFormat("#,###,###,###0.00");
                    style.SetFont(font);
                    style.FillForegroundColor = IndexedColors.Black.Index;
                    style.FillPattern = FillPattern.SolidForeground;

                    fomatStyle = (XSSFCellStyle)workbook.CreateCellStyle();
                    fomatStyle.DataFormat = format.GetFormat("#,###,###,###0.00");

                    fileStream.Close();
                    sheet = (XSSFSheet)workbook.GetSheetAt(0);

                    //Form Miro No
                    row = (XSSFRow)sheet.GetRow(1);
                    if (row == null)
                    {
                        row = (XSSFRow)sheet.CreateRow(1);
                    }
                    cell = (XSSFCell)row.GetCell(0);
                    cell.SetCellValue($"No: {salesOrders.FirstOrDefault().FormMiroNo}");

                    //SOType
                    row = (XSSFRow)sheet.GetRow(6);
                    if (row == null)
                    {
                        row = (XSSFRow)sheet.CreateRow(6);
                    }
                    cell = (XSSFCell)row.GetCell(1);
                    cell.SetCellValue(salesOrders.FirstOrDefault().SOType);

                    row = (XSSFRow)sheet.GetRow(15);
                    if (row == null)
                    {
                        row = (XSSFRow)sheet.CreateRow(15);
                    }

                    //SoldToParty.Code
                    cell = (XSSFCell)row.GetCell(1);
                    if (cell == null)
                    {
                        cell = (XSSFCell)row.CreateCell(1);
                    }
                    cell.SetCellValue(salesOrders.FirstOrDefault().PowerPlant.SoldToParty.Code);

                    //SoldToPart.Name
                    cell = (XSSFCell)row.GetCell(2);
                    cell.SetCellValue(salesOrders.FirstOrDefault().PowerPlant.SoldToParty.Name);

                    //Periode
                    row = (XSSFRow)sheet.GetRow(11);
                    cell = (XSSFCell)row.GetCell(4);
                    if (cell == null)
                    {
                        cell = (XSSFCell)row.CreateCell(4);
                    }
                    cell.SetCellValue(salesOrders.FirstOrDefault().Periode.Value.ToString("MMM yyyy"));

                    //print detail
                    int idx = 19;
                    for (int i = 0; i < salesOrders.Count; i++)
                    {
                        string material = ((Material)Enum.Parse(typeof(Material), salesOrders[i].PowerPlant.Material)).ToDescription();

                        row = (XSSFRow)sheet.GetRow(idx);

                        //A
                        if (row == null)
                        {
                            row = (XSSFRow)sheet.CreateRow(idx);
                        }
                        cell = (XSSFCell)row.GetCell(0);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(0);
                        }
                        cell.SetCellValue(salesOrders[i].PowerPlant.Code);

                        //B
                        cell = (XSSFCell)row.GetCell(1);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(1);
                        }
                        cell.SetCellValue(salesOrders[i].PowerPlant.Name);

                        //C
                        cell = (XSSFCell)row.GetCell(2);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(2);
                        }
                        cell.SetCellValue(material.Split('-')[0]);

                        //D
                        cell = (XSSFCell)row.GetCell(3);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(3);
                        }
                        cell.SetCellValue(material.Split('-')[1]);

                        //E ***
                        cell = (XSSFCell)row.GetCell(4);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(4);
                        }

                        //cari additional
                        if (salesOrders[i].Periode.Value == beforeEndDate)
                        {
                            cell.SetCellValue($"{salesOrders[i].PowerPlant.Name} (ADDITIONAL)");
                        }
                        else
                        {
                            cell.SetCellValue(salesOrders[i].PowerPlant.Name);
                        }

                        //F
                        cell = (XSSFCell)row.GetCell(5);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(5);
                        }
                        cell.SetCellValue(salesOrders[i].Quantity.Value);
                        cell.CellStyle = fomatStyle;

                        //G
                        cell = (XSSFCell)row.GetCell(6);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(6);
                        }
                        cell.SetCellValue(salesOrders[i].NetPrice.Value);
                        cell.CellStyle = fomatStyle;

                        //H
                        cell = (XSSFCell)row.GetCell(7);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(7);
                        }
                        cell.SetCellValue(salesOrders[i].NetPrice.Value * 1000);
                        cell.CellStyle = fomatStyle;

                        //I
                        cell = (XSSFCell)row.GetCell(8);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(8);
                        }
                        cell.SetCellValue(salesOrders[i].NetValue.Value);
                        cell.CellStyle = fomatStyle;

                        //J
                        cell = (XSSFCell)row.GetCell(9);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(9);
                        }
                        cell.SetCellValue(salesOrders[i].NoSo);

                        //L
                        cell = (XSSFCell)row.GetCell(11);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(11);
                        }
                        cell.SetCellValue(salesOrders[i].NoPr);

                        //P
                        cell = (XSSFCell)row.GetCell(15);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(15);
                        }
                        cell.SetCellValue(salesOrders[i].NoPo);

                        //Q
                        //R
                        DateTime periode = salesOrders[i].Periode.Value;

                        if (salesOrders[i].Periode.Value < beforeEndDate)
                        {
                            periode = endDate;
                        }

                        cell = (XSSFCell)row.GetCell(17);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(17);
                        }
                        cell.SetCellValue(periode.ToString("dddd, MMM dd, yyyy"));

                        //S
                        cell = (XSSFCell)row.GetCell(18);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(18);
                        }
                        cell.SetCellValue(periode.ToString("dddd, MMM dd, yyyy"));
                        //T
                        //U

                        idx++;
                    }

                    //total
                    //F
                    double sumQuantity = salesOrders.Sum(m => m.Quantity.Value);
                    double sumNetValue = salesOrders.Sum(m => m.NetValue.Value);

                    row = (XSSFRow)sheet.GetRow(idx);
                    if (row == null)
                    {
                        row = (XSSFRow)sheet.CreateRow(idx);
                    }
                    cell = (XSSFCell)row.GetCell(5);
                    if (cell == null)
                    {
                        cell = (XSSFCell)row.CreateCell(5);
                    }
                    cell.SetCellValue(sumQuantity);
                    cell.CellStyle = style;

                    //I
                    cell = (XSSFCell)row.GetCell(8);
                    if (cell == null)
                    {
                        cell = (XSSFCell)row.CreateCell(8);
                    }
                    cell.SetCellValue(sumNetValue);
                    cell.CellStyle = style;

                    //
                    idx += 6;
                    row = (XSSFRow)sheet.GetRow(idx);

                    //T
                    row = (XSSFRow)sheet.GetRow(idx);

                    if (row == null)
                    {
                        row = (XSSFRow)sheet.CreateRow(idx);
                    }
                    cell = (XSSFCell)row.GetCell(19);
                    if (cell == null)
                    {
                        cell = (XSSFCell)row.CreateCell(19);
                    }
                    cell.SetCellValue($"Jakarta, {DateTime.Now.Date.ToString("dd MMMM yyyy")}");

                    //set filename to file
                    fileName = DisplayFormat.GetUniqueFileName("FormMIRO", ".xlsx");

                    //write workbook to stream
                    MemoryStream memoryStream = new MemoryStream();
                    workbook.Write(memoryStream);
                    downloadParam = new DownloadParameterStub
                    {
                        MemoryStream = memoryStream,
                        ContentType = MimeMapping.GetMimeMapping(fileName),
                        FileDownloadName = fileName
                    };

                    TempData[KEY] = downloadParam;
                    return File(memoryStream.ToArray(), MimeMapping.GetMimeMapping(fileName), fileName);
                }
            }
            else
            {
                response.SetFail("No data to download.");
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> DownloadZts(int year, int month)
        {
            //lib
            FilterQuery filter;
            FileStream fileStream;

            string filePath = FILE_PATH;
            string fileName = FILE_NAME;

            DownloadParameterStub downloadParam = null;
            XSSFCellStyle style;
            XSSFCellStyle formatStyle;
            XSSFFont font;
            XSSFSheet sheet = null;
            XSSFRow row = null;
            XSSFCell cell = null;
            XSSFWorkbook workbook;

            ResponseModel response = new ResponseModel(true);
            DateTime startDate = new DateTime(year, month, 1);
            DateTime endDate = startDate.AddMonths(1).AddDays(-1);

            //remove warning opening excel
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
            filter.AddFilter(Field.Periode, FilterOperator.LessThanOrEquals, endDate);
            filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZTS.ToString());

            List<SortQuery> sorts = new List<SortQuery>();
            SortQuery sort = new SortQuery(Field.PowerPlantId, SortOrder.Ascending);
            sorts.Add(sort);

            List<SalesOrder> salesOrders = await RepoSalesOrder.FindAllAsync(sorts, filter);

            if (salesOrders.Any())
            {
                if (System.IO.File.Exists(Server.MapPath($"{filePath}{fileName}")))
                {
                    //read file and set to workbook
                    fileStream = new FileStream(Server.MapPath($"{filePath}{fileName}"), FileMode.Open, FileAccess.Read);
                    workbook = new XSSFWorkbook(fileStream);
                    font = (XSSFFont)workbook.CreateFont();
                    font.Color = IndexedColors.White.Index;
                    font.IsBold = true;

                    var format = workbook.CreateDataFormat();

                    style = (XSSFCellStyle)workbook.CreateCellStyle();
                    style.DataFormat = format.GetFormat("#,###,###,###0.00");
                    style.SetFont(font);
                    style.FillForegroundColor = IndexedColors.Black.Index;
                    style.FillPattern = FillPattern.SolidForeground;

                    formatStyle = (XSSFCellStyle)workbook.CreateCellStyle();
                    formatStyle.DataFormat = format.GetFormat("#,###,###,###0.00");

                    fileStream.Close();
                    sheet = (XSSFSheet)workbook.GetSheetAt(0);

                    //cek no form miro dari sales order
                    if (!string.IsNullOrEmpty(salesOrders.FirstOrDefault().FormMiroNo))
                    {
                        //Form Miro No
                        row = (XSSFRow)sheet.GetRow(1);
                        if (row == null)
                        {
                            row = (XSSFRow)sheet.CreateRow(1);
                        }
                        cell = (XSSFCell)row.GetCell(0);
                        cell.SetCellValue($"No: {salesOrders.FirstOrDefault().FormMiroNo}");

                        //SOType
                        row = (XSSFRow)sheet.GetRow(6);
                        if (row == null)
                        {
                            row = (XSSFRow)sheet.CreateRow(6);
                        }
                        cell = (XSSFCell)row.GetCell(1);
                        cell.SetCellValue(salesOrders.FirstOrDefault().SOType);

                        row = (XSSFRow)sheet.GetRow(15);
                        if (row == null)
                        {
                            row = (XSSFRow)sheet.CreateRow(15);
                        }

                        //SoldToParty.Code
                        cell = (XSSFCell)row.GetCell(1);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(1);
                        }
                        cell.SetCellValue(salesOrders.FirstOrDefault().PowerPlant.SoldToParty.Code);

                        //SoldToPart.Name
                        cell = (XSSFCell)row.GetCell(2);
                        cell.SetCellValue(salesOrders.FirstOrDefault().PowerPlant.SoldToParty.Name);

                        //Periode
                        row = (XSSFRow)sheet.GetRow(11);
                        cell = (XSSFCell)row.GetCell(4);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(4);
                        }
                        cell.SetCellValue(salesOrders.FirstOrDefault().Periode.Value.ToString("MMM yyyy"));

                        //print detail
                        int idx = 19;
                        for (int i = 0; i < salesOrders.Count; i++)
                        {
                            string material = ((Material)Enum.Parse(typeof(Material), salesOrders[i].PowerPlant.Material)).ToDescription();

                            row = (XSSFRow)sheet.GetRow(idx);

                            //A
                            if (row == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(idx);
                            }
                            cell = (XSSFCell)row.GetCell(0);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(0);
                            }
                            cell.SetCellValue(salesOrders[i].PowerPlant.Code);

                            //B
                            cell = (XSSFCell)row.GetCell(1);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(1);
                            }
                            cell.SetCellValue(salesOrders[i].PowerPlant.Name);

                            //C
                            cell = (XSSFCell)row.GetCell(2);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(2);
                            }
                            cell.SetCellValue(material.Split('-')[0]);

                            //D
                            cell = (XSSFCell)row.GetCell(3);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(3);
                            }
                            cell.SetCellValue(material.Split('-')[1]);

                            //E ***
                            cell = (XSSFCell)row.GetCell(4);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(4);
                            }

                            //cari additional
                            if (salesOrders[i].Periode.Value == endDate.AddDays(-1))
                            {
                                cell.SetCellValue($"{salesOrders[i].PowerPlant.Name} (ADDITIONAL)");
                            }
                            else
                            {
                                cell.SetCellValue(salesOrders[i].PowerPlant.Name);
                            }

                            //F
                            cell = (XSSFCell)row.GetCell(5);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(5);
                            }
                            cell.SetCellValue(salesOrders[i].Quantity.Value);
                            cell.CellStyle = formatStyle;

                            //G
                            cell = (XSSFCell)row.GetCell(6);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(6);
                            }
                            cell.SetCellValue(salesOrders[i].NetPrice.Value);
                            cell.CellStyle = formatStyle;

                            //H
                            cell = (XSSFCell)row.GetCell(7);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(7);
                            }
                            cell.SetCellValue(salesOrders[i].NetPrice.Value * 1000);
                            cell.CellStyle = formatStyle;

                            //I
                            cell = (XSSFCell)row.GetCell(8);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(8);
                            }
                            cell.SetCellValue(salesOrders[i].NetValue.Value);
                            cell.CellStyle = formatStyle;

                            //J
                            cell = (XSSFCell)row.GetCell(9);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(9);
                            }
                            cell.SetCellValue(salesOrders[i].NoSo);

                            //L
                            cell = (XSSFCell)row.GetCell(11);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(11);
                            }
                            cell.SetCellValue(salesOrders[i].NoPr);

                            //P
                            cell = (XSSFCell)row.GetCell(15);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(15);
                            }
                            cell.SetCellValue(salesOrders[i].NoPo);

                            //Q
                            //R
                            cell = (XSSFCell)row.GetCell(17);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(17);
                            }
                            cell.SetCellValue(salesOrders[i].Periode.Value.ToString("dddd, MMM dd, yyyy"));

                            //S
                            cell = (XSSFCell)row.GetCell(18);
                            if (cell == null)
                            {
                                cell = (XSSFCell)row.CreateCell(18);
                            }
                            cell.SetCellValue(salesOrders[i].Periode.Value.ToString("dddd, MMM dd, yyyy"));
                            //T
                            //U

                            idx++;
                        }
                        //total
                        //F
                        row = (XSSFRow)sheet.GetRow(idx);
                        if (row == null)
                        {
                            row = (XSSFRow)sheet.CreateRow(idx);
                        }
                        cell = (XSSFCell)row.GetCell(5);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(5);
                        }
                        cell.SetCellValue(salesOrders.Sum(m => m.Quantity.Value));
                        cell.CellStyle = style;

                        //I
                        cell = (XSSFCell)row.GetCell(8);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(8);
                        }
                        cell.SetCellValue(salesOrders.Sum(m => m.NetValue.Value));
                        cell.CellStyle = style;

                        //
                        idx += 6;
                        row = (XSSFRow)sheet.GetRow(idx);

                        //T
                        row = (XSSFRow)sheet.GetRow(idx);

                        if (row == null)
                        {
                            row = (XSSFRow)sheet.CreateRow(idx);
                        }
                        cell = (XSSFCell)row.GetCell(19);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(19);
                        }
                        cell.SetCellValue($"Jakarta, {DateTime.Now.Date.ToString("dd MMMM yyyy")}");

                        //set filename to file
                        fileName = DisplayFormat.GetUniqueFileName("FormMIRO", ".xlsx");

                        //write workbook to stream
                        MemoryStream memoryStream = new MemoryStream();
                        workbook.Write(memoryStream);
                        downloadParam = new DownloadParameterStub
                        {
                            MemoryStream = memoryStream,
                            ContentType = MimeMapping.GetMimeMapping(fileName),
                            FileDownloadName = fileName
                        };

                        return File(memoryStream.ToArray(), MimeMapping.GetMimeMapping(fileName), fileName);
                        //TempData[KEY] = downloadParam;
                    }
                    else
                    {
                        response.SetFail("Please create form miro before.");
                    }
                }
            }
            else
            {
                response.SetFail("No data to download.");
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult TestDownload(params object[] id)
        //{
        //    ResponseModel response = new ResponseModel(true);
        //    List<string> descriptions = new List<string>()
        //    {
        //        "SEG DARAJAT UNIT I",
        //        "SEG DARAJAT UNIT I",
        //        "SEG DARAJAT UNIT II",
        //        "SEG DARAJAT UNIT III",
        //        "SEG DARAJAT UNIT I-III",
        //        "SEG SALAK PRATAMA UNIT IV-VI",
        //        "SEG SALAK PRATAMA UNIT IV-VI"
        //    };

        //    List<TestClass> objs = new List<TestClass>();

        //    var res = descriptions.GroupBy(s => s);
        //    foreach(var h in res)
        //    {
        //        TestClass temp = new TestClass
        //        {
        //            Key = h.Key,
        //            Count = h.Count()
        //        };

        //        objs.Add(temp);
        //    }

        //    return Json(response);
        //}
        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<FormMiroPresentationStub> models;
            List<ViewSalesOrderApproval> dbObjects;
            List<ViewSalesOrderApproval> dbObjectss = new List<ViewSalesOrderApproval>();
            Task<List<ViewSalesOrderApproval>> taskDbObjects = null;
            Task<int> countTask;
            QueryRequestParameter param = QueryRequestParameter.Current;
            List<Business.Entities.PowerPlant> plants;
            FilterQuery filter = new FilterQuery(Field.SOType, FilterOperator.Equals, SOType.ZTS);

            if (param.Filter == null)
            {
                param.InstanceFilter();
            }

            if (User.AreaId.HasValue && User.AreaId > 0)
            {
                param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, User.AreaId);

                filter.AddFilter(Field.AreaId, FilterOperator.Equals, User.AreaId);
                plants = RepoPowerPlant.FindAll(filter);

                if (plants.Any())
                {
                    param.Filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZTS);
                }
            }

            taskDbObjects = RepoViewSalesOrderApproval.FindAllAsync(param.Filter);
            countTask = RepoViewSalesOrderApproval.CountAsync(param.Filter);

            await Task.WhenAll(taskDbObjects, countTask);

            //dbObjects = taskDbObjects.Result.Where(m => m.WorkflowCode == Common.Enums.Workflow.AREA_OPS_MANAGER_VERIFICATION.ToString() ||
            //            m.WorkflowCode == Common.Enums.Workflow.PARTNERSHIP_MANAGER_VERIFICATION.ToString() ||
            //            m.WorkflowCode == Common.Enums.Workflow.PRINT_COVER_LETTER.ToString()).ToList();
            dbObjects = taskDbObjects.Result;

            //dbObjects = dbObjects.Select(m => new ViewSalesOrderApproval
            //{
            //    Year = m.Year,
            //    Month = m.Month,
            //    WorkflowCode = m.WorkflowCode,
            //    WorkflowName = m.WorkflowName,
            //    CreatedBy = m.CreatedBy,
            //    SOType = m.SOType,
            //    WorkflowType = m.WorkflowType
            //}).GroupBy(n => new { n.Year, n.Month, n.WorkflowCode, n.WorkflowName, n.CreatedBy, n.SOType, n.WorkflowType })
            //.Select(n => n.First()).ToList();

            count = countTask.Result;

            models = ListMapper.MapList<ViewSalesOrderApproval, FormMiroPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });

        }

        public async Task<string> BindingDataPopupZTS(int year, int month)
        {
            //lib
            FilterQuery filter;
            List<SalesOrder> dbObjects;
            List<SalesOrderPresentationStub> models;
            Task<List<SalesOrder>> taskDbObjects = null;

            DateTime startDate = new DateTime(year, month, 1);
            DateTime endDate = startDate.AddMonths(1).AddDays(-1);

            filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
            filter.AddFilter(Field.Periode, FilterOperator.LessThanOrEquals, endDate);
            filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZTS.ToString());

            taskDbObjects = RepoSalesOrder.FindAllAsync(filter);
            await Task.WhenAll(taskDbObjects);

            dbObjects = taskDbObjects.Result;
            models = ListMapper.MapList<SalesOrder, SalesOrderPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(models);
        }
        #endregion

        #region "Helper"
        public ActionResult Download()
        {
            DownloadParameterStub param = TempData[KEY] as DownloadParameterStub;

            if (param is DownloadParameterStub)
            {
                return File(param.MemoryStream.ToArray(), param.ContentType, param.FileDownloadName);
            }

            return new EmptyResult();
        }

        private string GenerateAutoNumber()
        {
            string result = null;
            bool hasAutoNumber = CheckAutoNumber();

            if (hasAutoNumber)
            {
                FilterQuery filter = new FilterQuery(Field.TransactionCode, FilterOperator.Equals, "01");
                filter.AddFilter(Field.Year, FilterOperator.Equals, DateTime.Now.Year);
                AutoNumber autoNumber = RepoAutoNumber.Find(filter);

                AutoNumberFormStub model = new AutoNumberFormStub(autoNumber);
                model.IncrementNumber = model.IncrementNumber + 1;
                model.MapDbObject(autoNumber);
                RepoAutoNumber.Save(autoNumber);

                int currMonth = DateTime.Now.Month;
                string sCurrMonth;

                if (currMonth < 10)
                {
                    sCurrMonth = $"0{currMonth}";
                } else
                {
                    sCurrMonth = $"{currMonth}";
                }

                if (autoNumber.IncrementNumber < 10)
                {
                    result = $"0{autoNumber.IncrementNumber}/{autoNumber.Prefix}/{sCurrMonth}/{DateTime.Now.Year}";
                }
                else
                {
                    result = $"{autoNumber.IncrementNumber}/{autoNumber.Prefix}/{sCurrMonth}/{DateTime.Now.Year}";
                }

            }

            return result;
        }

        private bool CheckAutoNumber()
        {
            FilterQuery filter = new FilterQuery(Field.TransactionCode, FilterOperator.Equals, "01");
            filter.AddFilter(Field.Year, FilterOperator.Equals, DateTime.Now.Year);
            AutoNumber autoNumber = RepoAutoNumber.Find(filter);

            if (autoNumber == null) {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    public class Periode
    {
        public int Year { get; set; }
        public int Month { get; set; }
    }
}