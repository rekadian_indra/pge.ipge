﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class FileManagementController : Controller
    {
		List<FileManagementResponseModel> fileManagementResponseModels = new List<FileManagementResponseModel>();
		/**
         * save file using kendo file uploader
         * menangani kalau nama file sama
         * The Name of the Upload component is "files"
         * currently not support multiple uploads
         * 
         * @return filepath ex "~/Uploads/rekadia.jpg"
         */
		public string Save(IEnumerable<HttpPostedFileBase> files, Guid ids)
        {
            //lib
            string relativePath = "";
            string friendlyFilename = "";
            string fileName = "";

			//algorithm
			if (files != null)
            {
                foreach (var file in files)
                {
                    string serverPath;
                    Guid identifier = ids;
                    fileName = Path.GetFileNameWithoutExtension(file.FileName);

                    friendlyFilename = $"{DisplayFormat.URLFriendly(fileName)}{Path.GetExtension(file.FileName)}";
                    fileName = $"{identifier} {friendlyFilename}";
                    relativePath = $"{DirectoryHelper.Upload}{fileName}";

                    //save file
                    serverPath = Path.Combine(Server.MapPath(DirectoryHelper.Upload), fileName);
                    file.SaveAs(serverPath);

					fileManagementResponseModels.Add(new FileManagementResponseModel
					{
						FilePath = relativePath,
						FileName = friendlyFilename,
						FileNameSaved = fileName,
						AbsolutePath = VirtualPathUtility.ToAbsolute(relativePath),
						UId = identifier.ToString()
					});
				}
            }

            return JsonConvert.SerializeObject(fileManagementResponseModels);
        }

        /**
         * remove file using kendo file uploader
         */
        public ActionResult Remove(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"

            if (fileNames != null)
            {
                foreach (string fullName in fileNames)
                {
                    string fileName = Path.GetFileName(fullName);
                    string physicalPath = Path.Combine(Server.MapPath(DirectoryHelper.Upload), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        // The files are not actually removed in this demo
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

		public static string FriendlyName(string filePath)
		{
			string result = "";
			char divider = ' ';

			int indexOfDivider = filePath.IndexOf(divider);
			if ((indexOfDivider >= 0) && (indexOfDivider < filePath.Length - 1))
			{
				result = filePath.Substring(indexOfDivider + 1);
				result = result.Replace('-', ' ');
				result = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(result);
				string extension = result.Substring(result.LastIndexOf('.'));
				result = result.Replace(extension, extension.ToLower());
			}

			return result;
		}

		public static string AbsolutePath(string filePath)
		{
			string result = "";

			if (filePath.Length > 2)
			{
				if (filePath.Substring(0, 2) == "~/")
				{
					result = "/" + filePath.Substring(2);
				}
			}

			return result;
		}
	}
}
