﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    //[AllowCrossSite]
    //[OutputCache(NoStore = true, Duration = 0, Location = System.Web.UI.OutputCacheLocation.None)]
    [AuthorizeUser(ModuleName = UserModule.Whitelist)]
    public class WhitelistController : BaseController<WhitelistFormStub>
    {
        private IWhitelistRepository RepoWhitelist { get; set; }
        private ICrewRepository RepoCrew { get; set; }

        public WhitelistController(
            IWhitelistRepository repoWhitelist,
            IViewWhitelistRepository repoViewWhitelist,
            ICrewRepository repoCrew,
            IViewCrewRepository repoViewCrew,
            ICompanyRepository repoCompany,
            IAirportRepository repoAirport
        )
        {
            RepoWhitelist = repoWhitelist;
            RepoViewWhitelist = repoViewWhitelist;
            RepoCrew = repoCrew;
            RepoViewCrew = repoViewCrew;
            RepoCompany = repoCompany;
            RepoAirport = repoAirport;
        }

        [MvcSiteMapNode(Title = TitleSite.Whitelist, ParentKey = KeySite.Dashboard, Key = KeySite.IndexWhitelist)]
        public override async Task<ActionResult> Index()
        {
            List<SelectListItem> statusOption = DisplayFormat.EnumToSelectList<CrewStatus>();
            ViewBag.StatusOption = JsonConvert.SerializeObject(statusOption);

            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexWhitelist, Key = KeySite.CreateWhitelist)]
        public override async Task<ActionResult> Create()
        {
            //lib
            WhitelistFormStub model = new WhitelistFormStub();

            //algorithm
            model.StartDate = DateTime.Now.Date;
            model.EndDate = DateTime.Now.AddDays(2).Date;

            await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(WhitelistFormStub model)
        {
            if (ModelState.IsValid)
            {
                Whitelist dbObject = new Whitelist();
                Crew crew = await RepoCrew.FindByPrimaryKeyAsync(model.CrewId);

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                RepoWhitelist.Save(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject("AppGlobalMessage", "CreateSuccess").ToString();
                this.SetMessage($"Whitelist {crew.CrewName}", template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexWhitelist, Key = KeySite.EditWhitelist)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            //lib
            int primaryKey = int.Parse(id.ElementAt(0).ToString());
            Whitelist dbObject = await RepoWhitelist.FindByPrimaryKeyAsync(primaryKey);
            WhitelistFormStub model = new WhitelistFormStub(dbObject);

            //algorithm
            ViewBag.Breadcrumb = dbObject.Crew.CrewName;
            ViewBag.SelectedCrewId = model.CrewId;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(WhitelistFormStub model)
        {
            Crew crew = await RepoCrew.FindByPrimaryKeyAsync(model.CrewId);

            if (ModelState.IsValid)
            {
                Whitelist dbObject = await RepoWhitelist.FindByPrimaryKeyAsync(model.WhitelistId);

                //set activity log
                model.LogActivity = new LogActivityModel<Whitelist>(dbObject);

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                RepoWhitelist.Save(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                this.SetMessage($"Whitelist {crew.CrewName}", template);

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Name = crew.CrewName;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            Whitelist dbObject = await RepoWhitelist.FindByPrimaryKeyAsync(id);
            ResponseModel response = new ResponseModel(true);

            if (!RepoWhitelist.Delete(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        #region binding

        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<ViewWhitelist> dbObjects;
            List<WhitelistPresentationStub> models;
            Task<List<ViewWhitelist>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (args != null && args.Any())
            {
                bool isOnDuty = Boolean.Parse(args.ElementAt(0).ToString());

                if (isOnDuty)
                {
                    DateTime today = DateTime.Now.Date;

                    param.InstanceFilter();

                    param.Filter.AddFilter(Field.StartDate, FilterOperator.LessThanOrEquals, today);
                    param.Filter.AddFilter(Field.EndDate, FilterOperator.GreaterThanOrEquals, today);
                }
            }

            dbObjectsTask = RepoViewWhitelist.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewWhitelist.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewWhitelist, WhitelistPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        #endregion
    }
}
