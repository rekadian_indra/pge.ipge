﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;

namespace WebUI.Controllers
{
    //[Authorize]
    public class ReportMController : BaseController
    {
        public ReportMController(ILogRepository repoLog)
        {
            RepoLog = repoLog;
        }

        //if using ModelBinder to get user login use SGUser parameter
        //if using Principal dont get user login SGUser parameter
        //public ActionResult Index(SGUser user)
        //{
        //}

        [MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span>", Key = "Report", ParentKey ="Home")]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = "Reliability", Key = "Reliability", ParentKey = "Dashboard")]
        public ActionResult Reliability()
        {
            return View();
        }

        [MvcSiteMapNode(Title = "Daily Monthly", Key = "DailyMonthly", ParentKey = "Dashboard")]
        public ActionResult DailyMonthly()
        {
            return View();
        }

        public ActionResult Monthly()
        {
            return PartialView("_Monthly");
        }

        public ActionResult Daily()
        {
            return PartialView("_Daily");
        }

        [MvcSiteMapNode(Title = "Management", Key = "ManagementReport", ParentKey = "Dashboard")]
        public ActionResult ManagementReport()
        {
            return View();
        }

        [MvcSiteMapNode(Title = "Sales", Key = "Sales", ParentKey = "Dashboard")]
        public ActionResult Sales()
        {
            return View();
        }

        [MvcSiteMapNode(Title = "Geoscience", Key = "Geoscience", ParentKey = "Dashboard")]
        public ActionResult Geoscience()
        {
            return View();
        }

        [MvcSiteMapNode(Title = "Management", Key = "Management", ParentKey = "Dashboard")]
        public ActionResult Management()
        {
            return View();
        }
    }
}
