﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using NPOI;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;
using WebUI.Models.EEData;

namespace WebUI.Controllers
{
    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.RESERVOIR })]
    public class DataController : BaseController<ExplorationFormStub>
    {
        private const string KEY = "ExcelFile";
        public DataController
        (
            IAreaRepository repoArea,
            IClusterRepository repoCluster,
            IWellRepository repoWell,
            IExplorationRepository repoExploration,
            IExplorationDetailRepository repoExplorationDetail,
            IPowerPlantRepository repoPowerPlant
        )
        {
            RepoArea = repoArea;
            RepoCluster = repoCluster;
            RepoWell = repoWell;
            RepoExploration = repoExploration;
            RepoExplorationDetail = repoExplorationDetail;
            RepoPowerPlant = repoPowerPlant;
        }

        [MvcSiteMapNode(Title = TitleSite.EEData, Key = KeySite.IndexEEData, ParentKey = KeySite.Dashboard)]
        public override async Task<ActionResult> Index()
        {
            ExplorationPresentationStub model;
            HttpCookie areaIdCookie = Request.Cookies["AreaId"];
            HttpCookie clusterIdCookie = Request.Cookies["ClusterId"];
            HttpCookie wellIdCookie = Request.Cookies["WellId"];
            if (areaIdCookie != null)
            {
                model = new ExplorationPresentationStub
                {
                    AreaId = Convert.ToInt32(areaIdCookie.Value),
                    ClusterId = Convert.ToInt32(clusterIdCookie.Value),
                    WellId = Convert.ToInt32(wellIdCookie.Value)
                };
            }
            else
            {
                model = new ExplorationPresentationStub
                {
                    AreaId = 0,
                    ClusterId = 0,
                    WellId = 0
                };
            }

            await Task.Delay(0);
            return View(model);
        }

        [MvcSiteMapNode(Title = TitleSite.Add, ParentKey = KeySite.IndexEEData, Key = KeySite.AddEEData)]
        public async Task<ActionResult> Add(int areaId, int clusterId, int wellId)
        {
            Area dbObject = await RepoArea.FindByPrimaryKeyAsync(areaId);
            Well well = await RepoWell.FindByPrimaryKeyAsync(wellId);
            ExplorationFormStub model;
            if (dbObject != null)
            {
                model = new ExplorationFormStub
                {
                    AreaId = dbObject.Id,
                    AreaDescription = dbObject.Description,
                    ClusterId = well.Cluster.Id,
                    ClusterDescription = well.Cluster.Name,
                    WellId = well.Id,
                    WellDescription = well.Name,
                    ExplorationDate = DateTime.Now.Date
                };
            }
            else
            {
                model = new ExplorationFormStub();
            }

            return View("Form", model);
        }

        [HttpPost]
        public async Task<ActionResult> Add(ExplorationFormStub model)
        {
            if (ModelState.IsValid)
            {
                Exploration dbObject = await RepoExploration.FindAsync(new FilterQuery(Field.ExplorationDate, FilterOperator.Equals, model.ExplorationDate));
                if (dbObject == null)
                {
                    dbObject = new Exploration();
                    model.MapDbObject(dbObject);

                    ExplorationDetail explorationDetail = new ExplorationDetail
                    {
                        Mku = model.Mku,
                        Mkt = model.Mkt,
                        Mdpl = model.Mdpl,
                        Sds = model.Sds,
                        Tds = model.Tds,
                        CreatedBy = model.LogActivity.CreatedBy,
                        CreatedDateTime = DateTime.Now,
                        ModifiedBy = model.LogActivity.ModifiedBy,
                        ModifiedDateTime = DateTime.Now
                    };
                    dbObject.ExplorationDetails.Add(explorationDetail);
                    dbObject.CreatedDateTime = model.LogActivity.CreatedDateTimeUtc;
                    dbObject.ModifiedDateTime = model.LogActivity.ModifiedDateTimeUtc;

                    //save dbObject
                    await RepoExploration.SaveAsync(dbObject);

                    //message
                    string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                    this.SetMessage(model.AreaDescription, template);
                }
                else
                {
                    FilterQuery filterQuery = new FilterQuery();
                    filterQuery.AddFilter(Field.Mku, FilterOperator.Equals, model.Mku);
                    filterQuery.AddFilter(Field.Mkt, FilterOperator.Equals, model.Mkt);
                    filterQuery.AddFilter(Field.Mdpl, FilterOperator.Equals, model.Mdpl);
                    ExplorationDetail explorationDetail = await RepoExplorationDetail.FindAsync(filterQuery);
                    if (explorationDetail == null)
                    {
                        explorationDetail = new ExplorationDetail
                        {
                            Mku = model.Mku,
                            Mkt = model.Mkt,
                            Mdpl = model.Mdpl,
                            Sds = model.Sds,
                            Tds = model.Tds,
                            CreatedBy = User.UserName,
                            CreatedDateTime = DateTime.Now,
                            ModifiedBy = User.UserName,
                            ModifiedDateTime = DateTime.Now
                        };

                        dbObject.ExplorationDetails.Add(explorationDetail);

                        //save dbObject
                        await RepoExploration.SaveAsync(dbObject);

                        //message
                        string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                        this.SetMessage(model.AreaDescription, template);
                    }
                    else
                    {
                        //message
                        this.SetMessage(model.AreaDescription, "Data sudah ada, harap lakukan aksi edit untuk merubah data.");
                    }
                }

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexEEData, Key = KeySite.EditEEData)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            //lib
            ExplorationFormStub model;
            int primaryKey = Convert.ToInt32(id[0]);
            ExplorationDetail dbObject = await RepoExplorationDetail.FindByPrimaryKeyAsync(primaryKey);
            Exploration exploration = dbObject.Exploration;

            model = new ExplorationFormStub(exploration);
            model.ExplorationDetailId = dbObject.Id;
            model.AreaDescription = exploration.Area.Description;
            if (exploration.Well != null)
            {
                model.ClusterDescription = exploration.Cluster.Name;
                model.WellDescription = exploration.Well.Name;
            }

            model.Mku = dbObject.Mku;
            model.Mkt = dbObject.Mkt;
            model.Mdpl = dbObject.Mdpl;
            model.Sds = dbObject.Sds;
            model.Tds = dbObject.Tds;

            ViewBag.Breadcrumb = model.AreaDescription + " " + model.ExplorationDate.ToString(DisplayFormat.ShortDateFormat);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(ExplorationFormStub model)
        {
            if (ModelState.IsValid)
            {
                ExplorationDetail dbObject = await RepoExplorationDetail.FindByPrimaryKeyAsync(model.ExplorationDetailId);

                //map dbObject
                dbObject.Mku = model.Mku;
                dbObject.Mkt = model.Mkt;
                dbObject.Mdpl = model.Mdpl;
                dbObject.Sds = model.Sds;
                dbObject.Tds = model.Tds;

                //save dbObject
                await RepoExplorationDetail.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                this.SetMessage(model.AreaDescription, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.AreaDescription;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            int primaryKey = Convert.ToInt32(id[0]);
            ExplorationDetail dbObject = await RepoExplorationDetail.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!(await RepoExplorationDetail.DeleteAsync(dbObject)))
            {
                response.SetFail(GlobalMessageField.DeleteFailed);
            }

            return Json(response);
        }

        public async Task<ActionResult> DownloadFilter(int areaId, int wellId)
        {
            Area dbObject = await RepoArea.FindByPrimaryKeyAsync(areaId);
            Well dbObjectWell = await RepoWell.FindByPrimaryKeyAsync(wellId);
            DownloadFilterStub model = new DownloadFilterStub();
            if (dbObject != null)
            {
                model.AreaId = dbObject.Id;
                model.AreaDescription = dbObject.Description;
            }
            if (dbObjectWell != null)
            {
                model.ClusterId = dbObjectWell.Cluster.Id;
                model.ClusterDescription = dbObjectWell.Cluster.Name;
                model.WellId = dbObjectWell.Id;
                model.WellDescription = dbObjectWell.Name;
            }
            return PartialView("DownloadFilterForm", model);
        }

        public async Task<ActionResult> GetTemplate(DownloadFilterStub model)
        {
            if (model == null)
                throw new ArgumentNullException();

            ResponseModel response = new ResponseModel(true);

            //lib
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
            ISheet sheet, sheet2;
            IRow row = null;
            ICell cell = null;
            IWorkbook workbook = new XSSFWorkbook();
            XSSFWorkbook xssfWorkbook = workbook as XSSFWorkbook;
            POIXMLProperties xmlProps = xssfWorkbook.GetProperties();
            CoreProperties coreProps = xmlProps.CoreProperties;
            IDataFormat dataFormat = workbook.CreateDataFormat();
            ICellStyle titleStyle = workbook.CreateCellStyle();
            ICellStyle yellowHeaderStyle = workbook.CreateCellStyle();
            ICellStyle whiteHeaderStyle = workbook.CreateCellStyle();
            ICellStyle dateHeaderStyle = workbook.CreateCellStyle();
            ICellStyle borderStyle = workbook.CreateCellStyle();
            IFont fontBold = workbook.CreateFont();
            IFont fontTitle = workbook.CreateFont();
            string fileDownloadName = null;
            string contentType;
            MemoryStream memoryStream = new MemoryStream();
            DownloadParameterStub downloadParam = new DownloadParameterStub();

            //algorithm
            fontBold.Boldweight = (short)FontBoldWeight.Bold;
            fontTitle.Boldweight = (short)FontBoldWeight.Bold;
            fontTitle.FontHeight = 14.0;

            //set title style
            titleStyle.SetFont(fontTitle);
            titleStyle.Alignment = HorizontalAlignment.Center;
            titleStyle.BorderLeft = BorderStyle.Medium;
            titleStyle.BorderTop = BorderStyle.Medium;
            titleStyle.BorderBottom = BorderStyle.Medium;

            //set yellow header style
            yellowHeaderStyle.SetFont(fontBold);
            yellowHeaderStyle.Alignment = HorizontalAlignment.Center;
            yellowHeaderStyle.FillForegroundColor = IndexedColors.Yellow.Index;
            yellowHeaderStyle.FillPattern = FillPattern.SolidForeground;

            //set date header style
            dateHeaderStyle.DataFormat = dataFormat.GetFormat("dd-MMM-yy");
            dateHeaderStyle.SetFont(fontBold);
            dateHeaderStyle.Alignment = HorizontalAlignment.Center;

            //set white header style
            whiteHeaderStyle.SetFont(fontBold);
            whiteHeaderStyle.Alignment = HorizontalAlignment.Center;

            //change file author
            coreProps.Creator = "I PGE";

            //create sheet explorations
            sheet = workbook.CreateSheet("Exploration");

            //set area id
            sheet2 = workbook.CreateSheet("Area");
            sheet2.CreateRow(0).CreateCell(0).SetCellValue(model.AreaId);
            sheet2.CreateRow(1).CreateCell(0).SetCellValue(model.WellId);
            sheet2.CreateRow(2).CreateCell(0).SetCellValue(model.ClusterId);
            workbook.SetSheetHidden(1, 1);

            //set title
            int titleMergeRegion = ((model.EndDate.Subtract(model.StartDate).Days + 1) * 2) + 3;
            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, titleMergeRegion - 1));
            row = sheet.CreateRow(0);
            cell = row.CreateCell(0);
            cell.SetCellValue("HASIL PENGUKURAN DAN TEKANAN SUMUR AREA " + model.AreaDescription.ToUpper());
            cell.CellStyle = titleStyle;

            //set border title
            borderStyle.BorderTop = BorderStyle.Medium;
            borderStyle.BorderBottom = BorderStyle.Medium;
            for (int a = 1; a < titleMergeRegion; a++)
            {
                if (a == (titleMergeRegion - 1))
                {
                    borderStyle.BorderRight = BorderStyle.Medium;
                }
                cell = row.CreateCell(a);
                cell.CellStyle = borderStyle;
            }

            //set header kedalaman & header date
            row = sheet.CreateRow(2);

            sheet.AddMergedRegion(new CellRangeAddress(2, 2, 0, 2));
            cell = row.CreateCell(0);
            cell.SetCellValue("KEDALAMAN");
            cell.CellStyle = yellowHeaderStyle;
            cell = row.CreateCell(1);
            cell.CellStyle = yellowHeaderStyle;
            cell = row.CreateCell(2);
            cell.CellStyle = yellowHeaderStyle;

            //set border kedalaman
            cell = row.GetCell(0);
            cell.CellStyle.BorderTop = BorderStyle.Medium;
            cell.CellStyle.BorderLeft = BorderStyle.Medium;
            cell.CellStyle.BorderBottom = BorderStyle.Medium;
            cell = row.GetCell(1);
            cell.CellStyle.BorderTop = BorderStyle.Medium;
            cell.CellStyle.BorderBottom = BorderStyle.Medium;
            cell = row.GetCell(2);
            cell.CellStyle.BorderTop = BorderStyle.Medium;
            cell.CellStyle.BorderRight = BorderStyle.Medium;
            cell.CellStyle.BorderBottom = BorderStyle.Medium;

            DateTime dateHeaderVal = model.StartDate;
            for (int a = 3; a < titleMergeRegion; a++)
            {
                sheet.AddMergedRegion(new CellRangeAddress(2, 2, a, a + 1));
                cell = row.CreateCell(a);
                cell.SetCellValue(dateHeaderVal);
                cell.CellStyle = dateHeaderStyle;

                //set border header date
                cell = row.GetCell(a);
                cell.CellStyle.BorderTop = BorderStyle.Medium;
                cell.CellStyle.BorderLeft = BorderStyle.Medium;
                cell.CellStyle.BorderBottom = BorderStyle.Medium;
                cell = row.CreateCell(a + 1);
                cell.CellStyle = dateHeaderStyle;
                cell = row.GetCell(a + 1);
                cell.CellStyle.BorderTop = BorderStyle.Medium;
                cell.CellStyle.BorderRight = BorderStyle.Medium;
                cell.CellStyle.BorderBottom = BorderStyle.Medium;

                dateHeaderVal = dateHeaderVal.AddDays(1);
                a++;
            }

            row = sheet.CreateRow(3);
            //set header mKU
            sheet.AddMergedRegion(new CellRangeAddress(3, 4, 0, 0));
            cell = row.CreateCell(0);
            cell.SetCellValue("mKU");
            cell.CellStyle = yellowHeaderStyle;

            //set border header mKU
            cell = row.GetCell(0);
            cell.CellStyle.BorderTop = BorderStyle.Medium;
            cell.CellStyle.BorderLeft = BorderStyle.Medium;
            cell.CellStyle.BorderRight = BorderStyle.Medium;

            // set header mKT
            sheet.AddMergedRegion(new CellRangeAddress(3, 4, 1, 1));
            cell = row.CreateCell(1);
            cell.SetCellValue("mKT");
            cell.CellStyle = yellowHeaderStyle;

            //set border header mKT
            cell = row.GetCell(1);
            cell.CellStyle.BorderTop = BorderStyle.Medium;
            cell.CellStyle.BorderLeft = BorderStyle.Medium;
            cell.CellStyle.BorderRight = BorderStyle.Medium;

            // set header mdpl
            sheet.AddMergedRegion(new CellRangeAddress(3, 4, 2, 2));
            cell = row.CreateCell(2);
            cell.SetCellValue("mdpl");
            cell.CellStyle = yellowHeaderStyle;

            //set border header mdpl
            cell = row.GetCell(2);
            cell.CellStyle.BorderTop = BorderStyle.Medium;
            cell.CellStyle.BorderLeft = BorderStyle.Medium;
            cell.CellStyle.BorderRight = BorderStyle.Medium;

            //set SDS TDS
            int numVal = 1;
            for (int a = 3; a < titleMergeRegion; a++)
            {
                //SDS
                cell = row.CreateCell(a);
                cell.SetCellValue("SDS/" + numVal.ToString("000"));
                cell.CellStyle = whiteHeaderStyle;

                //set border header SDS
                cell = row.GetCell(a);
                cell.CellStyle.BorderTop = BorderStyle.Medium;
                cell.CellStyle.BorderLeft = BorderStyle.Medium;
                cell.CellStyle.BorderRight = BorderStyle.Medium;

                a++;

                //TDS
                cell = row.CreateCell(a);
                cell.SetCellValue("TDS/" + numVal.ToString("000"));
                cell.CellStyle = whiteHeaderStyle;

                //set border header TDS
                cell = row.GetCell(a);
                cell.CellStyle.BorderTop = BorderStyle.Medium;
                cell.CellStyle.BorderLeft = BorderStyle.Medium;
                cell.CellStyle.BorderRight = BorderStyle.Medium;

                numVal++;
            }

            //set C & KSC
            row = sheet.CreateRow(4);


            //set border header
            cell = row.CreateCell(0);
            cell.CellStyle = yellowHeaderStyle;
            cell.CellStyle.BorderBottom = BorderStyle.Medium;
            cell.CellStyle.BorderLeft = BorderStyle.Medium;
            cell.CellStyle.BorderRight = BorderStyle.Medium;
            cell = row.CreateCell(1);
            cell.CellStyle = yellowHeaderStyle;
            cell.CellStyle.BorderBottom = BorderStyle.Medium;
            cell.CellStyle.BorderLeft = BorderStyle.Medium;
            cell.CellStyle.BorderRight = BorderStyle.Medium;
            cell = row.CreateCell(2);
            cell.CellStyle = yellowHeaderStyle;
            cell.CellStyle.BorderBottom = BorderStyle.Medium;
            cell.CellStyle.BorderLeft = BorderStyle.Medium;
            cell.CellStyle.BorderRight = BorderStyle.Medium;

            for (int a = 3; a < titleMergeRegion; a++)
            {
                //C
                cell = row.CreateCell(a);
                cell.SetCellValue("( °C )");
                cell.CellStyle = whiteHeaderStyle;

                //set border header C
                cell = row.GetCell(a);
                cell.CellStyle.BorderBottom = BorderStyle.Medium;
                cell.CellStyle.BorderLeft = BorderStyle.Medium;
                cell.CellStyle.BorderRight = BorderStyle.Medium;

                a++;

                //KSC
                cell = row.CreateCell(a);
                cell.SetCellValue("( KSC )");
                cell.CellStyle = whiteHeaderStyle;

                //set border header KSC
                cell = row.GetCell(a);
                cell.CellStyle.BorderBottom = BorderStyle.Medium;
                cell.CellStyle.BorderLeft = BorderStyle.Medium;
                cell.CellStyle.BorderRight = BorderStyle.Medium;
            }

            sheet.CreateFreezePane(3, 5);

            //set file name
            fileDownloadName = "Exploration Template.xlsx";
            //get content type
            contentType = MimeMapping.GetMimeMapping(fileDownloadName);

            //write to memory stream
            workbook.Write(memoryStream);


            //set to download parameter
            downloadParam.MemoryStream = memoryStream;
            downloadParam.ContentType = contentType;
            downloadParam.FileDownloadName = fileDownloadName;

            TempData[KEY] = downloadParam;

            await Task.Delay(0);

            return Json(response);
        }

        public ActionResult Download()
        {
            if (TempData[KEY] != null)
            {
                DownloadParameterStub param = TempData[KEY] as DownloadParameterStub;

                return File(param.MemoryStream.ToArray(), param.ContentType, param.FileDownloadName);
            }

            return new EmptyResult();
        }

        public async Task<ActionResult> Upload(HttpPostedFileBase[] files)
        {
            ResponseModel response = new ResponseModel(true);

            try
            {
                IWorkbook workbook = new XSSFWorkbook(files[0].InputStream);
                ISheet sheet;
                IRow row = null;
                ICell cell = null;
                List<DateTime> explorationDates = new List<DateTime>();
                List<Exploration> dbObjects = await RepoExploration.FindAllAsync();
                List<Exploration> explorations = new List<Exploration>();
                Exploration exploration = null;
                ExplorationDetail explorationDetail = null;
                ExplorationFormStub model = null;
                List<string> err = new List<string>();                

                sheet = workbook.GetSheetAt(1);
                row = sheet.GetRow(0);
                cell = row.GetCell(0);
                int areaId = Convert.ToInt32(cell.NumericCellValue);
                row = sheet.GetRow(1);
                cell = row.GetCell(0);
                int wellId = Convert.ToInt32(cell.NumericCellValue);
                row = sheet.GetRow(2);
                cell = row.GetCell(0);
                int clusterId = Convert.ToInt32(cell.NumericCellValue);                        
                response.Message = "Upload Excel Success";
                int cellIndex = 3;

                sheet = workbook.GetSheetAt(0);
                row = sheet.GetRow(2);
                cell = row.GetCell(cellIndex);
                while (cell != null)
                {
                    explorationDates.Add(cell.DateCellValue);
                    cellIndex++; cellIndex++;
                    cell = row.GetCell(cellIndex);
                }

                int lastCell = row.LastCellNum;
                int indexDate = 0;
                for (int a = 3; a < lastCell; a++)
                {
                    exploration = dbObjects.FirstOrDefault(e => e.AreaId == areaId && e.ExplorationDate == explorationDates.ElementAt(indexDate));
                    if (exploration == null)
                    {
                        exploration = new Exploration();
                        for (int b = 5; b <= sheet.LastRowNum; b++)
                        {
                            row = sheet.GetRow(b);

                            cell = row.GetCell(0);
                            if (cell == null)
                                err.Add("Nilai pada baris ke-" + (b + 1) + " kolom ke-1 harus diisi");

                            cell = row.GetCell(1);
                            if (cell == null)
                                err.Add("Nilai pada baris ke-" + (b + 1) + " kolom ke-2 harus diisi");

                            cell = row.GetCell(2);
                            if (cell == null)
                                err.Add("Nilai pada baris ke-" + (b + 1) + " kolom ke-3 harus diisi");

                            cell = row.GetCell(3);
                            if (cell == null)
                                err.Add("Nilai pada baris ke-" + (b + 1) + " kolom ke-4 harus diisi");

                            cell = row.GetCell(4);
                            if (cell == null)
                                err.Add("Nilai pada baris ke-" + (b + 1) + " kolom ke-5 harus diisi");

                            if (!err.Any())
                            {
                                explorationDetail = new ExplorationDetail
                                {
                                    Mku = row.GetCell(0).NumericCellValue,
                                    Mkt = row.GetCell(1).NumericCellValue,
                                    Mdpl = row.GetCell(2).NumericCellValue,
                                    Sds = row.GetCell(a).NumericCellValue,
                                    Tds = row.GetCell(a + 1).NumericCellValue,
                                    CreatedBy = User.UserName,
                                    CreatedDateTime = DateTime.Now,
                                    ModifiedBy = User.UserName,
                                    ModifiedDateTime = DateTime.Now
                                };
                                model = new ExplorationFormStub
                                {
                                    AreaId = areaId,
                                    ClusterId = clusterId,
                                    WellId = wellId,
                                    ExplorationDate = explorationDates.ElementAt(indexDate)
                                };
                                model.MapDbObject(exploration);
                                exploration.CreatedBy = User.UserName;
                                exploration.CreatedDateTime = DateTime.Now;
                                exploration.ModifiedBy = User.UserName;
                                exploration.ModifiedDateTime = DateTime.Now;
                                exploration.ExplorationDetails.Add(explorationDetail);

                                explorations.Add(exploration);
                            }
                            else
                            {
                                response.SetFail(string.Join("\n",err));
                            }
                        }
                    }
                    else
                    {                                                
                        for (int b = 5; b <= sheet.LastRowNum; b++)
                        {
                            row = sheet.GetRow(b);
                            cell = row.GetCell(0);
                            if (cell == null)
                                err.Add("Nilai pada baris ke-" + (b + 1) + " kolom ke-1 harus diisi");

                            cell = row.GetCell(1);
                            if (cell == null)
                                err.Add("Nilai pada baris ke-" + (b + 1) + " kolom ke-2 harus diisi");

                            cell = row.GetCell(2);
                            if (cell == null)
                                err.Add("Nilai pada baris ke-" + (b + 1) + " kolom ke-3 harus diisi");

                            cell = row.GetCell(3);
                            if (cell == null)
                                err.Add("Nilai pada baris ke-" + (b + 1) + " kolom ke-4 harus diisi");

                            cell = row.GetCell(4);
                            if (cell == null)
                                err.Add("Nilai pada baris ke-" + (b + 1) + " kolom ke-5 harus diisi");

                            if (!err.Any())
                            {
                                explorationDetail = exploration.ExplorationDetails.FirstOrDefault(e => e.Mku == row.GetCell(0).NumericCellValue && e.Mkt == row.GetCell(1).NumericCellValue && e.Mdpl == row.GetCell(2).NumericCellValue);
                                if (explorationDetail == null)
                                {
                                    explorationDetail = new ExplorationDetail
                                    {
                                        Mku = row.GetCell(0).NumericCellValue,
                                        Mkt = row.GetCell(1).NumericCellValue,
                                        Mdpl = row.GetCell(2).NumericCellValue,
                                        Sds = row.GetCell(a).NumericCellValue,
                                        Tds = row.GetCell(a + 1).NumericCellValue,
                                        CreatedBy = User.UserName,
                                        CreatedDateTime = DateTime.Now,
                                        ModifiedBy = User.UserName,
                                        ModifiedDateTime = DateTime.Now
                                    };

                                    exploration.ExplorationDetails.Add(explorationDetail);

                                    explorations.Add(exploration);
                                }
                                else
                                {
                                    explorationDetail.Mku = row.GetCell(0).NumericCellValue;
                                    explorationDetail.Mkt = row.GetCell(1).NumericCellValue;
                                    explorationDetail.Mdpl = row.GetCell(2).NumericCellValue;
                                    explorationDetail.Sds = row.GetCell(a).NumericCellValue;
                                    explorationDetail.Tds = row.GetCell(a + 1).NumericCellValue;

                                    explorations.Add(exploration);
                                }
                            }
                            else
                            {
                                response.SetFail(string.Join("\n", err));
                            }
                        }
                    }
                    a++;
                    indexDate++;
                }

                if (explorations.Any())
                {
                    await RepoExploration.SaveAllAsync(explorations);
                }
            }
            catch (Exception)
            {
                response.SetFail("Terjadi Kesalahan! Periksa kembali template dan data yang di upload!");
            }

            await Task.Delay(0);

            return Json(response);
        }

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            int count;
            List<ExplorationPresentationStub> models;
            List<Exploration> dbObjects;
            Task<List<Exploration>> taskDbObjects = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (param.Filter != null)
            {
                if (param.Filter.Filters.Any())
                {
                    if (param.Filter.Filters.Exists(f => f.Field == Field.ClusterId))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.ClusterId);
                        if (filterFmn.Value.ToString() == "0" || filterFmn.Value.ToString() == "")
                        {
                            param.Filter.Filters.Remove(filterFmn);
                        }
                    }

                    if (param.Filter.Filters.Exists(f => f.Field == Field.AreaId))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.AreaId);
                        if (filterFmn.Value.ToString() == "0" || filterFmn.Value.ToString() == "")
                        {
                            param.Filter.Filters.Remove(filterFmn);
                        }
                    }

                    if (param.Filter.Filters.Exists(f => f.Field == Field.WellId))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.WellId);
                        if (filterFmn.Value.ToString() == "0" || filterFmn.Value.ToString() == "")
                        {
                            param.Filter.Filters.Remove(filterFmn);
                        }
                    }
                }
            }

            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);

            if (User.AreaId.HasValue && User.AreaId > 0)
                param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, User.AreaId);

            taskDbObjects = RepoExploration.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoExploration.CountAsync();

            await Task.WhenAll(taskDbObjects, countTask);

            dbObjects = taskDbObjects.Result;
            count = dbObjects.Count;

            models = ListMapper.MapList<Exploration, ExplorationPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingDetail()
        {
            //kamus
            int count;
            List<ExplorationDetail> dbObjects;
            List<ExplorationDetailPresentationStub> models;
            Task<List<ExplorationDetail>> taskDbObjects = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            taskDbObjects = RepoExplorationDetail.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoExplorationDetail.CountAsync();

            await Task.WhenAll(taskDbObjects, countTask);

            dbObjects = taskDbObjects.Result;
            count = dbObjects.Count;

            models = ListMapper.MapList<ExplorationDetail, ExplorationDetailPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }



        #endregion
    }
}