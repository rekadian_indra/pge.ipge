﻿using Common.Enums;
using LogAction.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Linq;

namespace WebUI.Areas.Invoice.Controllers
{
    public class InvoiceServiceController : BaseController
    {
        #region Constructor

        public InvoiceServiceController()
        {
        }

        public InvoiceServiceController
        (
            ILogRepository repoLog,
            IWorkflowRepository repoWorkflow,
            IInvoiceRepository invoiceRepository,
            IInvoiceApprovalRepository invoiceApprovalRepository,
            ISalesOrderRepository repoSalesOrder,
            IMembershipRepository repoMembership
        )
        {
            RepoLog = repoLog;
            RepoWorkflow = repoWorkflow;
            RepoInvoice = invoiceRepository;
            RepoInvoiceApproval = invoiceApprovalRepository;
            RepoSalesOrder = repoSalesOrder;
            RepoMembership = repoMembership;
        }

        #endregion

        #region Approval

        public async Task<ActionResult> Approve(Guid userId, int id, int invApprovalId)
        {
            ResponseModel response = new ResponseModel(true);
            response.Message = "Invoice Approved";

            Membership membership = await RepoMembership.FindAsync(new FilterQuery(Field.UserId, FilterOperator.Equals, userId));
            Business.Entities.Invoice invoice = await RepoInvoice.FindByPrimaryKeyAsync(id);
            Business.Entities.Workflow workflow;

            // cek membership dan sales order ada
            if (membership != null && invoice != null)
            {
                if (invoice.SalesOrder.PowerPlantId != null)
                {
                    if (invoice.SalesOrder.PowerPlant.AreaId != null && membership.AreaId != null)
                    {
                        if (invoice.SalesOrder.PowerPlant.AreaId.Value == membership.AreaId)
                        {
                            int lastStep = invoice.InvoiceApprovals.Select(a => a.Id).Max();
                            if (lastStep == invApprovalId)
                            {
                                // approve / revise
                                
                                workflow = await RepoWorkflow.FindAsync(new FilterQuery(Field.PreviousStep, FilterOperator.Equals, invoice.WorkflowId));
                                if(workflow != null)
                                {
                                    invoice.WorkflowId = workflow.Id;
                                    invoice.ModifiedBy = membership.User.UserName;
                                    invoice.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();
                                    
                                    if (await RepoInvoice.SaveAsync(invoice))
                                    {
                                        InvoiceApproval invoiceApprovalDb = new InvoiceApproval
                                        {
                                            InvoiceId = id,
                                            WorkflowId = workflow.Id,
                                            Notes = "",
                                            CreatedBy = membership.User.UserName,
                                            CreatedDateTimeUtc = DateTime.Now.ToUtcDateTime()
                                        };
                                        if (await RepoInvoiceApproval.SaveAsync(invoiceApprovalDb))
                                        {
                                            await InvoiceEmailSender(invoice, invoiceApprovalDb.Id);
                                        }
                                        else
                                        {
                                            response.SetFail("Invoice Workflow is not saved");
                                        }
                                    }
                                    else
                                    {
                                        response.SetFail("This Invoice Approval is not success");
                                    }
                                }
                                else
                                {
                                    response.SetFail("This Invoice can't go to Next Workflow");
                                }
                            }
                            else
                            {
                                // jika approval tidak sama
                                response.SetFail("This Invoice has been approved / revised by another user.");
                            }
                        }
                        else
                        {
                            // jika area so powerplant tidak sama area user
                            response.SetFail("User Area not equals to Invoice Area.");
                        }
                    }
                    else if (invoice.SalesOrder.PowerPlant.AreaId == null)
                    {
                        // jika power plant tidak punya area
                        response.SetFail("This Invoice doesn't have area.");
                    }
                    else if (membership.AreaId == null)
                    {
                        // jika user tidak punya area
                        response.SetFail("This User doesn't have area.");
                    }
                }
                else
                {
                    // jika power plant tidak ada
                    response.SetFail("This Invoice doesn't have Power Plant of Sales Order.");
                }
            }
            else if (membership == null)
            {
                // jika membership tidak ada
                response.SetFail("User is not available.");
            }
            else if (invoice == null)
            {
                // jika invoice tidak ada
                response.SetFail("This Invoice is not available.");
            }

            return View("Index", response);
        }

        public async Task<ActionResult> Revise(Guid userId, int id, int invApprovalId)
        {
            ResponseModel response = new ResponseModel(true);
            response.Message = "Invoice Revise";

            Membership membership = await RepoMembership.FindAsync(new FilterQuery(Field.UserId, FilterOperator.Equals, userId));
            Business.Entities.Invoice invoice = await RepoInvoice.FindByPrimaryKeyAsync(id);
            Business.Entities.Workflow workflow;

            // cek membership dan sales order ada
            if (membership != null && invoice != null)
            {
                if (invoice.SalesOrder.PowerPlantId != null)
                {
                    if (invoice.SalesOrder.PowerPlant.AreaId != null && membership.AreaId != null)
                    {
                        if (invoice.SalesOrder.PowerPlant.AreaId.Value == membership.AreaId)
                        {
                            int lastStep = invoice.InvoiceApprovals.Select(a => a.Id).Max();
                            if (lastStep == invApprovalId)
                            {
                                // approve / revise

                                if (invoice.Workflow.PreviousStep != null)
                                {
                                    workflow = await RepoWorkflow.FindAsync(new FilterQuery(Field.Id, FilterOperator.Equals, invoice.Workflow.PreviousStep));
                                    if (workflow != null)
                                    {
                                        invoice.WorkflowId = workflow.Id;
                                        invoice.ModifiedBy = membership.User.UserName;
                                        invoice.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();

                                        if (await RepoInvoice.SaveAsync(invoice))
                                        {
                                            InvoiceApproval invoiceReviseDb = new InvoiceApproval
                                            {
                                                InvoiceId = id,
                                                WorkflowId = workflow.Id,
                                                Notes = "",
                                                CreatedBy = membership.User.UserName,
                                                CreatedDateTimeUtc = DateTime.Now.ToUtcDateTime()
                                            };
                                            if (await RepoInvoiceApproval.SaveAsync(invoiceReviseDb))
                                            {
                                                await InvoiceEmailSender(invoice, invoiceReviseDb.Id);
                                            }
                                            else
                                            {
                                                response.SetFail("Invoice Workflow is not saved");
                                            }
                                        }
                                        else
                                        {
                                            response.SetFail("This Invoice Revise is not success");
                                        }
                                    }
                                    else
                                    {
                                        response.SetFail("This Invoice doesn't have Workflow");
                                    }
                                }
                                else
                                {
                                    response.SetFail("This Invoice can't go back to Previos Workflow");
                                }
                            }
                            else
                            {
                                // jika approval tidak sama
                                response.SetFail("This Invoice has been approved / revised by another user.");
                            }
                        }
                        else
                        {
                            // jika area so powerplant tidak sama area user
                            response.SetFail("User Area not equals to Invoice Area.");
                        }
                    }
                    else if (invoice.SalesOrder.PowerPlant.AreaId == null)
                    {
                        // jika power plant tidak punya area
                        response.SetFail("This Invoice doesn't have area.");
                    }
                    else if (membership.AreaId == null)
                    {
                        // jika user tidak punya area
                        response.SetFail("This User doesn't have area.");
                    }
                }
                else
                {
                    // jika power plant tidak ada
                    response.SetFail("This Invoice doesn't have Power Plant of Sales Order.");
                }
            }
            else if (membership == null)
            {
                // jika membership tidak ada
                response.SetFail("User is not available.");
            }
            else if (invoice == null)
            {
                // jika invoice tidak ada
                response.SetFail("This Invoice is not available.");
            }

            return View("Index", response);
        }

        #endregion

        public virtual async Task<bool> InvoiceEmailSender(Business.Entities.Invoice dbObject, int invoiceApprovalId, string message = null)
        {
            List<Membership> memberships = null;
            Business.Entities.SalesOrder salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(dbObject.SalesOrderId);

            if (salesOrder != null)
            {
                if (salesOrder.PowerPlant.AreaId != null)
                {
                    FilterQuery filterQuery = new FilterQuery();
                    filterQuery.AddFilter(Field.AreaId, FilterOperator.Equals, salesOrder.PowerPlant.AreaId.Value);
                    memberships = await RepoMembership.FindAllAsync(filterQuery);
                }
            }

            UserRole? userRole;

            switch (dbObject.WorkflowId)
            {
                case 1019:
                case 1020:
                case 1026:
                case 1027:
                    userRole = UserRole.FINANCE_CONTROLLER;
                    break;
                case 1021:
                case 1022:
                case 1028:
                    userRole = UserRole.FINANCE_TREASURY;
                    break;
                default:
                    userRole = null;
                    break;
            }

            if (userRole != null)
            {
                if (memberships.Any())
                {
                    memberships = memberships.FindAll(n => n.User.Roles.Any(m => m.RoleName == userRole.Value.ToString()));
                    if (memberships.Any())
                    {
                        string subject = "Notifikasi Invoice IPGE";
                        string body = "";

                        foreach (Membership m in memberships)
                        {
                            string approveUrl = Url.Action("Approve", "InvoiceServices", new { userId = m.UserId, id = dbObject.Id, invApprovalId = invoiceApprovalId, area = "Invoice" }, Request.Url.Scheme);
                            string reviseUrl = Url.Action("Revise", "InvoiceServices", new { userId = m.UserId, id = dbObject.Id, invApprovalId = invoiceApprovalId, area = "Invoice" }, Request.Url.Scheme);

                            body = @"<p><strong>Yth. Bpk / Ibu</strong></p>
                            <p>Dengan ini kami sampaikan bahwa Invoice berikut ini, menunggu persetujuan / revisi dari Bpk/Ibu.<br /><br /></p>
                            <p><strong>Detail Invoice :</strong></p>
                            <table style=""width: 347px;"">
                            <tbody>
                            <tr>
                            <td style=""width: 129px;"">Invoice Number</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.InvoiceNo != null ? dbObject.InvoiceNo : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">SO Type</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.SOType != null ? salesOrder.SOType : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Periode</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.Periode != null ? salesOrder.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat) : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Ship To party</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.PowerPlant.Name != null ? salesOrder.PowerPlant.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Sold To Party</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.PowerPlant.SoldToPartyId != null ? salesOrder.PowerPlant.SoldToParty.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;\"">Material</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.PowerPlant.Material != null ? ((Material)Enum.Parse(typeof(Material), salesOrder.PowerPlant.Material)).ToDescription() : "") + @"</td>
                            </tr>
							<tr>
                            <td style=""width: 129px;"">Quantity</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.Quantity != null ? salesOrder.Quantity.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Net Price</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.NetPrice != null ? salesOrder.NetPrice.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Net Value</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.NetValue != null ? salesOrder.NetValue.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Notes</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.Notes != null ? salesOrder.Notes : "") + @"</td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <p>Untuk verifikasi silahkan klik link di bawah ini</p>
                            <table><tr><td style =""background-color: #4ecdc4;border-color: #45b7af;border-radius: 5px;text-align: center;padding: 10px;"">
                            <a style='display: block; color: #ffffff;font-size: 12px;text-decoration: none;text-transform: uppercase;a:hover{cursor:pointer;}' href=""" + approveUrl + @""">Setujui</a></td>
                            <td style=""background-color: #bf3131;border-color: #45b7af;border-radius: 5px;text-align: center;padding: 10px;"">
                            <a style='display: block; color: #ffffff;font-size: 12px;text-decoration: none;text-transform: uppercase;a:hover{cursor:pointer;}' href=""" + reviseUrl + @""">Revisi</a></td></tr></table>
                            <p>&nbsp;</p>
                            <p>Atas perhatiannya kami ucapkan terima kasih.</p>";

                            string email = m.Email;
                            bool isSuccess = EmailHelper.SendEmail(m.Email, subject, body);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }
    }
}
