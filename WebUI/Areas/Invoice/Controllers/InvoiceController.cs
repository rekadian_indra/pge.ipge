﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Web;
using System.Linq;
using WebUI.Extension;
using Resources;
using WebUI.Areas.Invoice.Models;
using WebUI.Areas.SalesOrder.Models;
using System.IO;
using NPOI.XSSF.UserModel;
using System.Configuration;
using GemBox.Spreadsheet;
using WebUI.Models.AdditionalInvoiceFiles;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.Invoice.Controllers
{
    [CheckSessionTimeOut]
    [Authorize]
    public class InvoiceController : BaseController<EditInvoiceStub>
    {
        const string TEMPDATA_SALESORDERID = "salesOrderId";
        const string TEMPDATA_POWERPLANTID = "powerPlantId";
        const string TEMPDATA_PERIODE = "periode";
        const string COVER_LETTER_FILE_TEMPLATE = "TEMPLATE_SURAT_PENGANTAR.xlsx";
        const string DEFAULT_ADDITIONAL_TEXT = "<p>Pembayaran harap dilakukan melalui:</p>" +
            "<p><strong>Bank MANDIRI KCP. JKT KP PERTAMINA</strong></p>" +
            "<p><strong>Rekening PT. Pertamina Gethermal Energy</strong></p>" +
            "<p><strong>QQ Penerimaan USD</strong></p>" +
            "<p><strong>Nomor Rekening: 119-00-0468535-8</strong></p>" +
            "<p><strong>SWIFT CODE: BMRIIDJAAXXX</strong></p>";

        #region Constructor

        public InvoiceController()
        {
        }

        public InvoiceController
        (
            ILogRepository repoLog,
            IWorkflowRepository repoWorkflow,
            IInvoiceRepository repoInvoice,
            IInvoiceFileRepository repoInvoiceFile,
            IAdditionalInvoiceFileRepository repoAdditionalInvoiceFile,
            IEFakturFileRepository repoEFakturFile,
            IInvoiceApprovalRepository repoInvoiceApproval,
            ISalesOrderRepository repoSalesOrder,
            IPowerPlantRepository repoPowerPlant,
            IBastFileSignedRepository repoBastFileSigned,
            IUserRepository repoUser,
            IAreaRepository repoArea,
			IMembershipRepository repoMembership
        )
        {
            RepoLog = repoLog;
            RepoWorkflow = repoWorkflow;
            RepoInvoice = repoInvoice;
            RepoAdditionalInvoiceFile = repoAdditionalInvoiceFile;
            RepoInvoiceFile = repoInvoiceFile;
            RepoEFakturFile = repoEFakturFile;
            RepoInvoiceApproval = repoInvoiceApproval;
            RepoSalesOrder = repoSalesOrder;
            RepoPowerPlant = repoPowerPlant;
            RepoBastFileSigned = repoBastFileSigned;
            RepoUser = repoUser;
            RepoArea = repoArea;
			RepoMembership = repoMembership;
        }

        #endregion

        #region View

        [MvcSiteMapNode(Title = TitleSite.Invoice, Key = KeySite.IndexInvoice, ParentKey = KeySite.Dashboard)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        public ActionResult BackToIndex(int powerPlantId, DateTime periode)
        {
            return RedirectToAction(ActionSite.Index);
        }

        public async Task<ActionResult> Check(DateTime periode, int powerPlantId)
        {
            List<Business.Entities.SalesOrder> salesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(periode, powerPlantId);

            if (salesOrders.Any())
            {
                Business.Entities.SalesOrder lastSalesOrder = salesOrders.First();
                if (lastSalesOrder.Workflow.Approver == null) // kalo SO udah di approve sampe finish
                {
                    Business.Entities.Invoice invoice = await RepoInvoice.FindBySalesOrderIdAsync(lastSalesOrder.Id);

                    if (invoice != null)
                    {
                        return RedirectToAction(ActionSite.Edit, new
                        {
                            id = invoice.Id
                        });
                    }
                    else
                    {
                        TempData.Add(TEMPDATA_SALESORDERID, lastSalesOrder.Id);
                        return RedirectToAction(ActionSite.Create);
                    }
                }
                else
                {
                    Business.Entities.PowerPlant powerPlant = RepoPowerPlant.FindByPrimaryKey(powerPlantId);

                    this.SetMessage($"Sales Order untuk {powerPlant.Name} pada periode {periode.ToString(DisplayFormat.MonthYearDateFormat)} belum selesai diproses");

                    return RedirectToAction(ActionSite.Choose, new
                    {
                        periode = periode,
                        powerPlantId = powerPlantId
                    });
                }
            }
            else
            {
                Business.Entities.PowerPlant powerPlant = RepoPowerPlant.FindByPrimaryKey(powerPlantId);

                this.SetMessage($"Belum terdapat Sales Order untuk {powerPlant.Name} pada periode {periode.ToString(DisplayFormat.MonthYearDateFormat)}");

                return RedirectToAction(ActionSite.Choose, new
                {
                    periode = periode,
                    powerPlantId = powerPlantId
                });
            }
        }

        #endregion

        #region Create

        [MvcSiteMapNode(Title = TitleSite.Create, Key = KeySite.ChooseInvoice, ParentKey = KeySite.IndexInvoice)]
        public async Task<ActionResult> Choose(DateTime? periode = null, int? powerPlantId = null)
        {
            //kamus
            EditInvoiceStub model = new EditInvoiceStub();
            ChooseInvoiceStub chooseStub = new ChooseInvoiceStub();            
            Area area;

            //algo
            if (User != null && User.AreaId.HasValue && User.AreaId.Value > 0)
            {
                model.IsAllArea = false;
                area = RepoArea.FindByPrimaryKey(User.AreaId.Value);
                if (area != null && area.PowerPlants.Any())
                    chooseStub.SOType = area.PowerPlants.FirstOrDefault().SOType;
                else
                    chooseStub.SOType = SOType.ZPGE.ToString();
            }
            else
            {
                model.IsAllArea = true;
            }

            model.ChooseInvoice = GenerateChooseForm(periode, powerPlantId);

            if (model.ChooseInvoice.SOType == null)
                model.ChooseInvoice = chooseStub;

            await Task.Delay(0);

            return View("Form", model);
        }

        [MvcSiteMapNode(Title = TitleSite.Create, Key = KeySite.CreateInvoice, ParentKey = KeySite.IndexInvoice)]
        public override async Task<ActionResult> Create()
        {
            //kamus
            EditInvoiceStub model = new EditInvoiceStub();

            //algo
            if (TempData.ContainsKey(TEMPDATA_SALESORDERID))
            {
                int salesOrderId = (int)TempData[TEMPDATA_SALESORDERID];

                PrepareFormCreate(model, salesOrderId);

                model.CurrentAdditionalText = HttpUtility.HtmlDecode(DEFAULT_ADDITIONAL_TEXT);

                return View("Form", model);
            }

            await Task.Delay(0);

            return RedirectToAction(ActionSite.Choose);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(EditInvoiceStub model)
        {
            //kamus
            Business.Entities.Workflow workflow;
            Business.Entities.Invoice invoice;
            ResponseModel response = new ResponseModel(true);

            //algoritma
            if (model.IsDraft)
            {
                workflow = RepoWorkflow.FindByPrimaryKey(model.WorkflowId);
            }
            else
            {
                workflow = await RepoWorkflow.FindNextAsync(model.WorkflowId);
            }

            if (workflow != null)
            {
                invoice = await RepoInvoice.FindBySalesOrderIdAsync(model.SalesOrderId); // siapatau udah bikin dari surat pengantar
                if (invoice == null)
                {
                    invoice = new Business.Entities.Invoice();
                }
                else
                {
                    model.Id = invoice.Id;
                }
                if (invoice.InvoiceSequence == null)
                {
                    int lastFormSequence = await RepoInvoice.GetLastInvoiceSequence();
                    model.InvoiceSequence = (lastFormSequence + 1).ToString("000");
                }

                model.MapDbObject(invoice);
                invoice.WorkflowId = workflow.Id;


                await RepoInvoice.SaveAsync(invoice);

                // masukin ke history approval
                InvoiceApprovalFormStub invoiceApproval = new InvoiceApprovalFormStub()
                {
                    InvoiceId = invoice.Id,
                    WorkflowId = invoice.WorkflowId,
                    Notes = ""
                };
                InvoiceApproval invoiceApprovalDb = new InvoiceApproval();
                invoiceApproval.MapDbObject(invoiceApprovalDb);
                RepoInvoiceApproval.Save(invoiceApprovalDb);

                //send email
                await InvoiceEmailSender(invoice, invoiceApprovalDb.Id);

                return RedirectToAction("Index");
            }
            PrepareFormCreate(model, model.SalesOrderId);

            return View("Form", model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateEFakturFile(int id, string filePath)
        {
            //kamus
            string message;
            ResponseModel response = new ResponseModel(true);
            EFakturFileFormStub eFakturFile;
            EFakturFile eFakturFileDb = new EFakturFile();

            //algoritma
            eFakturFile = new EFakturFileFormStub()
            {
                FilePath = filePath,
                InvoiceId = id
            };
            eFakturFile.MapDbObject(eFakturFileDb);

            if (!(await RepoEFakturFile.SaveAsync(eFakturFileDb)))
            {
                message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> CreateInvoiceFile(int id, string filePath)
        {
            //kamus
            string message;
            ResponseModel response = new ResponseModel(true);
            InvoiceFileFormStub invoiceFile;
            InvoiceFile invoiceFileDb = new InvoiceFile();

            //algoritma
            invoiceFile = new InvoiceFileFormStub()
            {
                FilePath = filePath,
                InvoiceId = id
            };
            invoiceFile.MapDbObject(invoiceFileDb);

            if (!(await RepoInvoiceFile.SaveAsync(invoiceFileDb)))
            {
                message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> CreateAdditionalInvoiceFile(int id, string filePath)
        {
            //kamus
            string message;
            ResponseModel response = new ResponseModel(true);
            AdditionalInvoiceFileFormStub model;
            AdditionalInvoiceFile dbObject = new AdditionalInvoiceFile();

            //algoritma
            model = new AdditionalInvoiceFileFormStub()
            {
                FilePath = filePath,
                InvoiceId = id
            };
            model.MapDbObject(dbObject);

            if (!(await RepoAdditionalInvoiceFile.SaveAsync(dbObject)))
            {
                message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region Edit

        [MvcSiteMapNode(Title = TitleSite.Edit, Key = KeySite.EditInvoice, ParentKey = KeySite.IndexInvoice)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            //kamus

            //algo
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.Invoice dbObject = await RepoInvoice.FindByPrimaryKeyAsync(primaryKey);

            EditInvoiceStub model = new EditInvoiceStub(dbObject);

            // render
            PrepareFormEdit(model, primaryKey);
            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(EditInvoiceStub model)
        {
            //kamus
            FilterQuery filter;
            Business.Entities.Workflow workflow;
            Business.Entities.Invoice invoice = new Business.Entities.Invoice();
            ResponseModel response = new ResponseModel(true);

            //algoritma
            invoice = await RepoInvoice.FindByPrimaryKeyAsync(model.Id);
            model.CalculateWorkflowType(invoice.SalesOrder);

            if (model.IsDraft)
            {
                workflow = RepoWorkflow.FindByPrimaryKey(model.WorkflowId);
            }
            else
            {
                workflow = await RepoWorkflow.FindNextAsync(model.WorkflowId);
            }

            if (model.EFakturFiles == null)
            {
                filter = new FilterQuery(Field.InvoiceId, FilterOperator.Equals, model.Id);
                List<EFakturFile> eFakturFiles = await RepoEFakturFile.FindAllAsync(filter);

                if (eFakturFiles.Count > 0)
                {
                    // soalnya file2 yg lama nya ga perlu di add ulang
                    model.EFakturFiles = new List<EFakturFileFormStub>();
                }
            }

            if (model.InvoiceFiles == null)
            {
                filter = new FilterQuery(Field.InvoiceId, FilterOperator.Equals, model.Id);
                List<InvoiceFile> invoiceFiles = await RepoInvoiceFile.FindAllAsync(filter);

                if (invoiceFiles.Count > 0)
                {
                    // soalnya file2 yg lama nya ga perlu di add ulang
                    model.InvoiceFiles = new List<InvoiceFileFormStub>();
                }
            }

            if ((model.InvoiceFiles != null) && (model.EFakturFiles != null) && (workflow != null))
            {
                model.MapDbObject(invoice);
                invoice.WorkflowId = workflow.Id;

                await RepoInvoice.SaveAsync(invoice);

                //foreach (InvoiceFileFormStub invoiceFile in model.InvoiceFiles)
                //{
                //	InvoiceFile invoiceFileDb = new InvoiceFile();
                //	invoiceFile.InvoiceId = invoice.Id;
                //	invoiceFile.MapDbObject(invoiceFileDb);

                //	await RepoInvoiceFile.SaveAsync(invoiceFileDb);
                //}

                //foreach (EFakturFileFormStub eFakturFile in model.EFakturFiles)
                //{
                //	EFakturFile eFakturFileDb = new EFakturFile();
                //	eFakturFile.InvoiceId = invoice.Id;
                //	eFakturFile.MapDbObject(eFakturFileDb);

                //	await RepoEFakturFile.SaveAsync(eFakturFileDb);
                //}

                // masukin ke history approval
                InvoiceApprovalFormStub invoiceApproval = new InvoiceApprovalFormStub()
                {
                    InvoiceId = invoice.Id,
                    WorkflowId = invoice.WorkflowId,
                    Notes = ""
                };
                InvoiceApproval invoiceApprovalDb = new InvoiceApproval();
                invoiceApproval.MapDbObject(invoiceApprovalDb);
                RepoInvoiceApproval.Save(invoiceApprovalDb);

                //send email
                await InvoiceEmailSender(invoice, invoiceApprovalDb.Id);

                return RedirectToAction("Index");
            }

            PrepareFormEdit(model, model.Id);
            return await Edit(model.Id);
        }

        #endregion

        #region Delete

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            // kamus
            FilterQuery filter;
            string message;

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.Invoice dbObject = await RepoInvoice.FindByPrimaryKeyAsync(primaryKey);

            // hapus data Invoice File nya dulu
            filter = new FilterQuery(Field.InvoiceId, FilterOperator.Equals, dbObject.Id);
            List<InvoiceFile> invoiceFiles = await RepoInvoiceFile.FindAllAsync(filter);
            foreach (InvoiceFile invoiceFile in invoiceFiles)
            {
                string filePath = Server.MapPath(invoiceFile.FilePath);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }
            await RepoInvoiceFile.DeleteAllAsync(invoiceFiles);

            // hapus data EFaktur File nya dulu
            filter = new FilterQuery(Field.InvoiceId, FilterOperator.Equals, dbObject.Id);
            List<EFakturFile> EFakturFiles = await RepoEFakturFile.FindAllAsync(filter);
            foreach (EFakturFile EFakturFile in EFakturFiles)
            {
                string filePath = Server.MapPath(EFakturFile.FilePath);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }
            await RepoEFakturFile.DeleteAllAsync(EFakturFiles);

            // hapus data Approval
            filter = new FilterQuery(Field.InvoiceId, FilterOperator.Equals, dbObject.Id);
            List<InvoiceApproval> approvals = await RepoInvoiceApproval.FindAllAsync(filter);
            if ((approvals != null) && approvals.Any()) await RepoInvoiceApproval.DeleteAllAsync(approvals);

            // baru hapus sales order nya
            if (!(await RepoInvoice.DeleteAsync(dbObject)))
            {
                message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteInvoiceFile(params object[] id)
        {
            // kamus

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.InvoiceFile dbObject = await RepoInvoiceFile.FindByPrimaryKeyAsync(primaryKey);

            List<InvoiceFile> deleteFiles = RepoInvoiceFile.GetFromFilePath(dbObject.FilePath);

            await RepoInvoiceFile.DeleteAllAsync(deleteFiles);

            foreach (InvoiceFile deleteFile in deleteFiles)
            {
                string filePath = Server.MapPath(deleteFile.FilePath);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteAdditionalInvoiceFile(params object[] id)
        {
            // kamus

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            AdditionalInvoiceFile dbObject = await RepoAdditionalInvoiceFile.FindByPrimaryKeyAsync(primaryKey);

            List<AdditionalInvoiceFile> deleteFiles = RepoAdditionalInvoiceFile.GetFromFilePath(dbObject.FilePath);

            await RepoAdditionalInvoiceFile.DeleteAllAsync(deleteFiles);

            foreach (AdditionalInvoiceFile deleteFile in deleteFiles)
            {
                string filePath = Server.MapPath(deleteFile.FilePath);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteEFakturFile(params object[] id)
        {
            // kamus

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.EFakturFile dbObject = await RepoEFakturFile.FindByPrimaryKeyAsync(primaryKey);

            List<EFakturFile> deleteFiles = RepoEFakturFile.GetFromFilePath(dbObject.FilePath);

            await RepoEFakturFile.DeleteAllAsync(deleteFiles);

            foreach (EFakturFile deleteFile in deleteFiles)
            {
                string filePath = Server.MapPath(deleteFile.FilePath);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json(response);
        }

        #endregion

        #region Approval

        [HttpPost]
        public async Task<ActionResult> Approve(params object[] id)
        {
            // kamus
            FilterQuery filter;
            string message;
            Business.Entities.Workflow workflow;

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.Invoice dbObject = await RepoInvoice.FindByPrimaryKeyAsync(primaryKey);

            // cari workflow lanjutannya
            filter = new FilterQuery(Field.PreviousStep, FilterOperator.Equals, dbObject.WorkflowId);
            workflow = await RepoWorkflow.FindAsync(filter);
            dbObject.WorkflowId = workflow.Id;

            // baru update data sales order nya
            if (!(await RepoInvoice.SaveAsync(dbObject)))
            {
                message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
                response.SetFail(message);
            }
            else
            {
                // masukin ke history approval
                InvoiceApprovalFormStub invoiceApproval = new InvoiceApprovalFormStub()
                {
                    InvoiceId = primaryKey,
                    WorkflowId = workflow.Id,
                    Notes = ""
                };
                InvoiceApproval invoiceApprovalDb = new InvoiceApproval();
                invoiceApproval.MapDbObject(invoiceApprovalDb);
                RepoInvoiceApproval.Save(invoiceApprovalDb);

                //send email
                await InvoiceEmailSender(dbObject, invoiceApprovalDb.Id);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> Revise(int id, string message = "")
        {
            // kamus
            FilterQuery filter;
            //string message;
            Business.Entities.Workflow workflow;
            bool isSuccess = false;

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = id; // int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.Invoice dbObject = await RepoInvoice.FindByPrimaryKeyAsync(primaryKey);

            // cari workflow sebelumnya
            if (dbObject.Workflow != null)
            {
                if (dbObject.Workflow.PreviousStep != null)
                {
                    filter = new FilterQuery(Field.Id, FilterOperator.Equals, dbObject.Workflow.PreviousStep);
                    workflow = await RepoWorkflow.FindAsync(filter);
                    dbObject.WorkflowId = workflow.Id;

                    // update data sales order nya
                    isSuccess = await RepoInvoice.SaveAsync(dbObject);

                    // masukin ke history approval
                    InvoiceApprovalFormStub invoiceApproval = new InvoiceApprovalFormStub()
                    {
                        InvoiceId = id,
                        WorkflowId = workflow.Id,
                        Notes = message
                    };
                    InvoiceApproval invoiceApprovalDb = new InvoiceApproval();
                    invoiceApproval.MapDbObject(invoiceApprovalDb);
                    RepoInvoiceApproval.Save(invoiceApprovalDb);

                    //send email
                    await InvoiceEmailSender(dbObject, invoiceApprovalDb.Id, message);
                }
            }

            return RedirectToAction(ActionSite.Index);
        }

        #endregion

        #region Binding

        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<Business.Entities.Invoice> dbObjects;
            List<InvoicePresentationStub> models;
            Task<List<Business.Entities.Invoice>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;
            FieldHelper fh = new FieldHelper(Table.Workflow, Field.Code);

            if (param.Filter == null)
                param.InstanceFilter();

            if (User.AreaId.HasValue && User.AreaId > 0)
                param.Filter.AddFilter(Field.InvoiceArea, FilterOperator.Equals, User.AreaId);
            
            if(args != null)
                param.Filter.AddFilter(fh, FilterOperator.Equals, WorkflowStatus.FINISH);
            else
                param.Filter.AddFilter(fh, FilterOperator.NotEquals, WorkflowStatus.FINISH);

            //algorithm
            dbObjectsTask = RepoInvoice.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoInvoice.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Business.Entities.Invoice, InvoicePresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingApproval(params object[] args)
        {
            return await Binding(args);
        }

        public async Task<string> BindingPowerPlant(params object[] args)
        {
            QueryRequestParameter param = QueryRequestParameter.Current;

            List<Business.Entities.PowerPlant> dbObjects = new List<Business.Entities.PowerPlant>();
            List<SelectListItem> result = new List<SelectListItem>();
            List<Business.Entities.PowerPlant> plants = new List<Business.Entities.PowerPlant>();            


            string soType = args.ElementAt(0)?.ToString();
            if (Enum.TryParse(soType, out SOType curSOType))
            {
                dbObjects = await RepoPowerPlant.FindAllBySOType(curSOType);
                if (User.AreaId.HasValue && User.AreaId > 0)
                {
                    if(soType == SOType.ZTS.ToString())
                        dbObjects = dbObjects.Where(n => n.SOType == soType).ToList();
                    else
                        dbObjects = dbObjects.Where(n => n.AreaId == User.AreaId).ToList();
                }

                dbObjects.ForEach(
                    dbObject => result.Add(
                        new SelectListItem()
                        {
                            Text = dbObject.ShipToPartyName,
                            Value = dbObject.Id.ToString()
                        }
                    )
                );
            }

            await Task.Delay(0);

            return JsonConvert.SerializeObject(result);
        }

        public async Task<string> BindingSalesOrders(params object[] args)
        {
            //lib
            int count;
            List<Business.Entities.SalesOrder> dbObjects;
            List<SalesOrderPresentationStub> models;
            Task<List<Business.Entities.SalesOrder>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoSalesOrder.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoSalesOrder.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Business.Entities.SalesOrder, SalesOrderPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingBastFileSigneds(params object[] args)
        {
            //lib
            int count;
            List<Business.Entities.BastFileSigned> dbObjects;
            List<BastFileSignedPresentationStub> models;
            Task<List<Business.Entities.BastFileSigned>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoBastFileSigned.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoBastFileSigned.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Business.Entities.BastFileSigned, BastFileSignedPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }


        public async Task<string> BindingEFakturFiles(params object[] args)
        {
            //lib
            int count;
            List<Business.Entities.EFakturFile> dbObjects;
            List<EFakturFilePresentationStub> models;
            Task<List<Business.Entities.EFakturFile>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoEFakturFile.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoEFakturFile.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Business.Entities.EFakturFile, EFakturFilePresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }


        public async Task<string> BindingInvoiceFiles(params object[] args)
        {
            //lib
            int count;
            List<Business.Entities.InvoiceFile> dbObjects;
            List<InvoiceFilePresentationStub> models;
            Task<List<Business.Entities.InvoiceFile>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoInvoiceFile.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoInvoiceFile.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Business.Entities.InvoiceFile, InvoiceFilePresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingAdditionalInvoiceFiles(params object[] args)
        {
            //lib
            int count;
            List<AdditionalInvoiceFile> dbObjects;
            List<AdditionalInvoiceFilePresentationStub> models;
            Task<List<AdditionalInvoiceFile>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoAdditionalInvoiceFile.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoAdditionalInvoiceFile.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<AdditionalInvoiceFile, AdditionalInvoiceFilePresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        #endregion

        #region Download

        [HttpGet]
        public async Task<ActionResult> DownloadTemplate(int id)
        {
            byte[] excel = { 0 };

            string filename = $".xlsx";

            await Task.Delay(0);

            return File(excel, "application/x-msexcel", filename);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> DownloadCoverLetter(EditInvoiceStub model)
        {
            ResponseModel response = new ResponseModel(true);
            Business.Entities.Invoice invoice;
            Business.Entities.SalesOrder salesOrder;
            string errorMessage = "";
            List<Business.Entities.SalesOrder> sameGroupSalesOrders = new List<Business.Entities.SalesOrder>();
            List<Business.Entities.PowerPlant> sameGroupPowerPlants = new List<Business.Entities.PowerPlant>();

            if (string.IsNullOrEmpty(model.SignatureName))
            {
                errorMessage += "Penanda tangan harus dipilih terlebih dahulu.";
            }
            else
            {
                if (model.Id != 0) // kalau udah create invoice sebelumnya
                {
                    invoice = await RepoInvoice.FindByPrimaryKeyAsync(model.Id);
                }
                else // kalau baru masuk page create invoice, tapi udah pernah klik print surat pengantar
                {
                    invoice = await RepoInvoice.FindBySalesOrderIdAsync(model.SalesOrderId);
                }

                // kalau bener2 baru masuk page create invoice (blm save)
                if (invoice == null)
                {
                    invoice = new Business.Entities.Invoice();
                    model.MapDbObject(invoice);

                    await RepoInvoice.SaveAsync(invoice);
                    Business.Entities.Invoice invoiceDb = await RepoInvoice.FindByPrimaryKeyAsync(invoice.Id);
                    salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(model.SalesOrderId);
                }
                else
                {
                    salesOrder = invoice.SalesOrder;
                }

                // update invoice nya sesuai form surat pengantar
                if (invoice.InvoiceSequence == null)
                {
                    int lastFormSequence = await RepoInvoice.GetLastInvoiceSequence();
                    invoice.InvoiceSequence = (lastFormSequence + 1).ToString("000");
                }
                invoice.InvoiceNo = model.InvoiceNo;
                invoice.SignatureName = model.SignatureName;
                invoice.SignaturePosition = model.SignaturePosition;

                if (!string.IsNullOrEmpty(model.AdditionalText))
                {
                    string strDecode = HttpUtility.HtmlDecode(model.AdditionalText);
                    strDecode = strDecode.Replace("<em>", "<i>")
                        .Replace("</em>", "</i>").Replace("<span style=", "<u>")
                        .Replace("</span>", "</u>").Replace(@"""", "")
                        .Replace("text-decoration:underline;>", "");

                    invoice.AdditionalText = strDecode;
                }
                else
                {
                    invoice.AdditionalText = HttpUtility.HtmlDecode(DEFAULT_ADDITIONAL_TEXT);
                }

                await RepoInvoice.SaveAsync(invoice);

                // ambil sales order yg bersangkutan
                sameGroupPowerPlants = await RepoPowerPlant.FindAllByInvoiceGroup(salesOrder.PowerPlant.InvoiceGroup);
                foreach (Business.Entities.PowerPlant sameGroupPowerPlant in sameGroupPowerPlants)
                {
                    List<Business.Entities.SalesOrder> curSalesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(salesOrder.Periode, sameGroupPowerPlant.Id);
                    if (curSalesOrders != null && curSalesOrders.Count > 0)
                    {
                        Business.Entities.SalesOrder curSalesOrder = curSalesOrders.FirstOrDefault(x => x.Workflow.Approver == null);
                        if (curSalesOrder != null)
                        {
                            if (curSalesOrder.Invoices.Count > 0)
                            {
                                // update SignatureName & SignaturePosition
                                foreach (Business.Entities.Invoice curInvoice in curSalesOrder.Invoices)
                                {
                                    Business.Entities.Invoice curInvoiceDb = RepoInvoice.FindByPrimaryKey(curInvoice.Id);
                                    curInvoiceDb.SignatureName = model.SignatureName;
                                    curInvoiceDb.SignaturePosition = model.SignaturePosition;

                                    if (!string.IsNullOrEmpty(model.AdditionalText))
                                    {
                                        string strDecode = HttpUtility.HtmlDecode(model.AdditionalText);
                                        strDecode = strDecode.Replace("<em>", "<i>")
                                            .Replace("</em>", "</i>").Replace("<span style=", "<u>")
                                            .Replace("</span>", "</u>").Replace(@"""", "")
                                            .Replace("text-decoration:underline;>", "");

                                        curInvoiceDb.AdditionalText = strDecode;
                                    }
                                    else
                                    {
                                        curInvoiceDb.AdditionalText = HttpUtility.HtmlDecode(DEFAULT_ADDITIONAL_TEXT);
                                    }

                                    await RepoInvoice.SaveAsync(curInvoiceDb);
                                }

                                sameGroupSalesOrders.Add(curSalesOrder);
                            }
                            else // blm create invoice
                            {
                                errorMessage += $"Belum terdapat invoice untuk sales order {sameGroupPowerPlant.Name} periode {salesOrder.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}<br/>";
                            }
                        }
                        else // sales order blm di approve
                        {
                            errorMessage += $"Sales order {sameGroupPowerPlant.Name} periode {salesOrder.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)} belum selesai diproses<br/>";
                        }
                    }
                    else // blm ada sales order
                    {
                        errorMessage += $"Belum terdapat sales order {sameGroupPowerPlant.Name} periode {salesOrder.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}<br/>";
                    }
                }

                //if (errorMessage != "") // kalo ada error
                //{
                //    response.SetFail(errorMessage);
                //}
            }

            if (errorMessage != "") // kalo ada error
            {
                response.SetFail(errorMessage);
            }

            return Json(response);
        }

        public async Task<ActionResult> GenerateCoverLetter(int Id, int SalesOrderId)
        {
            byte[] excel = { 0 };
            List<Business.Entities.SalesOrder> sameGroupSalesOrders = new List<Business.Entities.SalesOrder>();
            List<Business.Entities.PowerPlant> sameGroupPowerPlants = new List<Business.Entities.PowerPlant>();

            // update invoice nya sesuai form surat pengantar
            Business.Entities.Invoice invoice;
            if (Id != 0) // kalau udah create invoice sebelumnya
            {
                invoice = await RepoInvoice.FindByPrimaryKeyAsync(Id);
            }
            else // kalau baru masuk page create invoice, tapi udah pernah klik print surat pengantar
            {
                invoice = await RepoInvoice.FindBySalesOrderIdAsync(SalesOrderId);
            }
            sameGroupPowerPlants = await RepoPowerPlant.FindAllByInvoiceGroup(invoice.SalesOrder.PowerPlant.InvoiceGroup);
            foreach (Business.Entities.PowerPlant sameGroupPowerPlant in sameGroupPowerPlants)
            {
                List<Business.Entities.SalesOrder> curSalesOrders = RepoSalesOrder.GetFromPeriodePowerPlant(invoice.SalesOrder.Periode, sameGroupPowerPlant.Id);
                if (curSalesOrders != null)
                {
                    Business.Entities.SalesOrder curSalesOrder = curSalesOrders.FirstOrDefault(x => x.Workflow.Approver == null);
                    if (curSalesOrder != null)
                    {
                        if (curSalesOrder.Invoices.Count > 0)
                        {
                            sameGroupSalesOrders.Add(curSalesOrder);
                        }
                    }
                }
            }

            // generate excelnya
            //string filePath = Path.Combine(Server.MapPath(DirectoryHelper.Resources), COVER_LETTER_FILE_TEMPLATE);
            string filePath = Path.Combine(Server.MapPath(DirectoryHelper.Template), COVER_LETTER_FILE_TEMPLATE);
            XSSFWorkbook workbook = new XSSFWorkbook(System.IO.File.Open(filePath, FileMode.Open));

			FilterQuery filter = new FilterQuery(Field.EmployeeNo, FilterOperator.Equals, invoice.SignatureName);
			Membership signatureMember = await RepoMembership.FindAsync(filter);

            InvoiceExcelStub invoiceExcel = new InvoiceExcelStub(workbook);
            excel = invoiceExcel.GenerateTemplate(invoice, signatureMember, sameGroupSalesOrders);

            //LoadFrom FileStream and Convert to pdf
            filePath = "~/Temp/";
            string fileNameExcel = "SuratPengantarTemp.xlsx";
            FileStream out1 = new FileStream(Server.MapPath($"{filePath}{fileNameExcel}"), FileMode.Create);
            workbook.Write(out1);
            out1.Close();

            string license = ConfigurationManager.AppSettings["GemboxLicense"];
            SpreadsheetInfo.SetLicense(license);
            var pdfFile = ExcelFile.Load(Server.MapPath($"{filePath}{fileNameExcel}"));

            MemoryStream memoryStream = new MemoryStream();
            //foreach (ExcelWorksheet worksheet in pdfFile.Worksheets)
            //{
            //	pdfFile.Worksheets.ActiveWorksheet = worksheet;
            //}
            PdfSaveOptions saveOptions = new PdfSaveOptions();
            saveOptions.SelectionType = SelectionType.EntireFile;
            pdfFile.Save(memoryStream, saveOptions);

            await Task.Delay(0);

            string fileNamePdf = DisplayFormat.GetUniqueFileName($"Surat Pengantar {invoice.SalesOrder.PowerPlant.Name}",".pdf");
            string contentType = MimeMapping.GetMimeMapping(fileNamePdf);
            return File(memoryStream.ToArray(), contentType, fileNamePdf);
        }

        #endregion

        #region Helper

        private ChooseInvoiceStub GenerateChooseForm(DateTime? periode, int? powerPlantId)
        {
            Business.Entities.PowerPlant powerPlant = (powerPlantId != null) ? RepoPowerPlant.FindByPrimaryKey(powerPlantId) : null;

            return new ChooseInvoiceStub()
            {
                Periode = periode,
                PowerPlantId = powerPlantId,
                SOType = powerPlant?.SOType
            };
        }

        private async void PrepareFormCreate(EditInvoiceStub model, int salesOrderId)
        {
            List<Business.Entities.Workflow> workflows;

            Business.Entities.SalesOrder salesOrder = RepoSalesOrder.FindByPrimaryKey(salesOrderId);

            // siapin form chooser nya
            model.ChooseInvoice = GenerateChooseForm(salesOrder.Periode, salesOrder.PowerPlantId);

            // siapin workflow panel nya
            model.CalculateWorkflowType(salesOrder);
            workflows = await RepoWorkflow.FindByTypeAsync(model.WorkflowType);
            model.WorkflowPanel = new WorkflowPanelStub(workflows);
            model.WorkflowId = workflows.First().Id;
            // parno bisi ga ada workflow aktif nya wkwkwkwk
            //int? workFlowId = model.WorkflowPanel.GetWorkflowId();
            //if (!workFlowId.HasValue)
            //{
            //	Business.Entities.Workflow firstWorkflow = await RepoWorkflow.FindFirstAsync(model.WorkflowType);
            //	workFlowId = firstWorkflow.Id;
            //}
            //model.WorkflowId = workFlowId.Value;

            // set sales order nya
            model.SalesOrderId = salesOrderId;
            model.SalesOrder = new SalesOrder.Models.SalesOrderPresentationStub(salesOrder);
            model.SalesOrder.PowerPlant = new PowerPlantPresentationStub(salesOrder.PowerPlant);
        }

        private async void PrepareFormEdit(EditInvoiceStub model, int invoiceId)
        {
            List<Business.Entities.Workflow> workflows;

            Business.Entities.Invoice invoice = RepoInvoice.FindByPrimaryKey(invoiceId);

            // siapin form chooser nya
            model.ChooseInvoice = GenerateChooseForm(invoice.SalesOrder.Periode, invoice.SalesOrder.PowerPlantId);

            // siapin workflow panel nya
            model.CalculateWorkflowType(invoice.SalesOrder);
            workflows = await RepoWorkflow.FindByTypeAsync(model.WorkflowType);
            model.WorkflowPanel = new WorkflowPanelStub(workflows, invoice.WorkflowId);

            // set sales order nya
            model.SalesOrder = new SalesOrder.Models.SalesOrderPresentationStub(invoice.SalesOrder);
            model.SalesOrder.PowerPlant = new PowerPlantPresentationStub(invoice.SalesOrder.PowerPlant);

            // ambil last invoice approval nya, bisi ada notes revisi
            InvoiceApproval invoiceApproval = await RepoInvoiceApproval.GetLastApproval(invoiceId);
            model.LastInvoiceApproval = invoiceApproval;
        }

        public virtual async Task<bool> InvoiceEmailSender(Business.Entities.Invoice dbObject, int invoiceApprovalId, string message = null)
        {
            List<Membership> memberships = null;
            Business.Entities.SalesOrder salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(dbObject.SalesOrderId);

            if (salesOrder != null)
            {
                if (salesOrder.PowerPlant.AreaId != null)
                {
                    FilterQuery filterQuery = new FilterQuery();
                    filterQuery.AddFilter(Field.AreaId, FilterOperator.Equals, salesOrder.PowerPlant.AreaId.Value);
                    memberships = await RepoMembership.FindAllAsync(filterQuery);
                }
            }

            UserRole? userRole;

            switch (dbObject.WorkflowId)
            {
                case 1019:
                case 1020:
                case 1026:
                case 1027:
                    userRole = UserRole.FINANCE_CONTROLLER;
                    break;
                case 1021:
                case 1022:
                case 1028:
                    userRole = UserRole.FINANCE_TREASURY;
                    break;
                default:
                    userRole = null;
                    break;
            }

            if (userRole != null)
            {
                if (memberships.Any())
                {
                    memberships = memberships.FindAll(n => n.User.Roles.Any(m => m.RoleName == userRole.Value.ToString()));
                    if (memberships.Any())
                    {
                        string subject = "Notifikasi Invoice IPGE";
                        string body = "";

                        foreach (Membership m in memberships)
                        {
                            string approveUrl = Url.Action("Approve", "InvoiceServices", new { userId = m.UserId, id = dbObject.Id, invApprovalId = invoiceApprovalId, area = "Invoice" }, Request.Url.Scheme);
                            string reviseUrl = Url.Action("Revise", "InvoiceServices", new { userId = m.UserId, id = dbObject.Id, invApprovalId = invoiceApprovalId, area = "Invoice" }, Request.Url.Scheme);

                            body = @"<p><strong>Yth. Bpk / Ibu</strong></p>
                            <p>Dengan ini kami sampaikan bahwa Invoice berikut ini, menunggu persetujuan / revisi dari Bpk/Ibu.<br /><br /></p>
                            <p><strong>Detail Invoice :</strong></p>
                            <table style=""width: 347px;"">
                            <tbody>
                            <tr>
                            <td style=""width: 129px;"">Invoice Number</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.InvoiceNo != null ? dbObject.InvoiceNo : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">SO Type</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.SOType != null ? salesOrder.SOType : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Periode</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.Periode != null ? salesOrder.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat) : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Ship To party</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.PowerPlant.Name != null ? salesOrder.PowerPlant.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Sold To Party</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.PowerPlant.SoldToPartyId != null ? salesOrder.PowerPlant.SoldToParty.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;\"">Material</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.PowerPlant.Material != null ? ((Material)Enum.Parse(typeof(Material), salesOrder.PowerPlant.Material)).ToDescription() : "") + @"</td>
                            </tr>
							<tr>
                            <td style=""width: 129px;"">Quantity</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.Quantity != null ? salesOrder.Quantity.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Net Price</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.NetPrice != null ? salesOrder.NetPrice.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Net Value</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.NetValue != null ? salesOrder.NetValue.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Notes</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (salesOrder.Notes != null ? salesOrder.Notes : "") + @"</td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <p>Untuk verifikasi silahkan klik link di bawah ini</p>
                            <table><tr><td style =""background-color: #4ecdc4;border-color: #45b7af;border-radius: 5px;text-align: center;padding: 10px;"">
                            <a style='display: block; color: #ffffff;font-size: 12px;text-decoration: none;text-transform: uppercase;a:hover{cursor:pointer;}' href=""" + approveUrl + @""">Setujui</a></td>
                            <td style=""background-color: #bf3131;border-color: #45b7af;border-radius: 5px;text-align: center;padding: 10px;"">
                            <a style='display: block; color: #ffffff;font-size: 12px;text-decoration: none;text-transform: uppercase;a:hover{cursor:pointer;}' href=""" + reviseUrl + @""">Revisi</a></td></tr></table>
                            <p>&nbsp;</p>
                            <p>Atas perhatiannya kami ucapkan terima kasih.</p>";

                            string email = m.Email;
                            bool isSuccess = EmailHelper.SendEmail(m.Email, subject, body);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        //public virtual async Task<bool> InvoiceEmailSender(Business.Entities.Invoice dbObject, bool isApprove, string message = null)
        //{
        //    List<Membership> memberships = null;
        //    Business.Entities.SalesOrder salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(dbObject.SalesOrderId);

        //    if (salesOrder != null)
        //    {
        //        if (salesOrder.PowerPlant.AreaId != null)
        //        {
        //            FilterQuery filterQuery = new FilterQuery();
        //            filterQuery.AddFilter(Field.AreaId, FilterOperator.Equals, salesOrder.PowerPlant.AreaId.Value);
        //            memberships = await RepoMembership.FindAllAsync(filterQuery);
        //        }
        //    }

        //    UserRole? userRole;

        //    switch (dbObject.WorkflowId)
        //    {
        //        case 1019:
        //        case 1020:
        //        case 1026:
        //        case 1027:
        //            userRole = UserRole.FINANCE_CONTROLLER;
        //            break;
        //        case 1021:
        //        case 1022:
        //        case 1028:
        //            userRole = UserRole.FINANCE_TREASURY;
        //            break;
        //        default:
        //            userRole = null;
        //            break;
        //    }

        //    if (userRole != null)
        //    {
        //        if (memberships.Any())
        //        {
        //            memberships = memberships.FindAll(n => n.User.Roles.Any(m => m.RoleName == userRole.Value.ToString()));
        //            if (memberships.Any())
        //            {
        //                string subject = "Notifikasi Invoice IPGE";
        //                string body = "";

        //                if (isApprove)
        //                {
        //                    body = @"<p><strong>Yth. Bpk / Ibu</strong></p>
        //                    <p>Dengan ini kami sampaikan bahwa Invoice berikut ini, menunggu persetujuan dari Bpk/Ibu.<br /><br /></p>
        //                    <p><strong>Detail Invoice :</strong></p>
        //                    <table style=""width: 347px;"">
        //                    <tbody>
        //                    <tr>
        //                    <td style=""width: 129px;"">Invoice Number</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (dbObject.InvoiceNo != null ? dbObject.InvoiceNo : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;"">SO Type</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (salesOrder.SOType != null ? salesOrder.SOType : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;"">Periode</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (salesOrder.Periode != null ? salesOrder.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat) : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;"">Ship To party</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (salesOrder.PowerPlant.Name != null ? salesOrder.PowerPlant.Name : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;"">Sold To Party</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (salesOrder.PowerPlant.SoldToPartyId != null ? salesOrder.PowerPlant.SoldToParty.Name : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;\"">Material</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (salesOrder.PowerPlant.Material != null ? ((Material)Enum.Parse(typeof(Material), salesOrder.PowerPlant.Material)).ToDescription() : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;"">Notes</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (salesOrder.Notes != null ? salesOrder.Notes : "") + @"</td>
        //                    </tr>
        //                    </tbody>
        //                    </table>
        //                    <p>&nbsp;</p><p>&nbsp;</p>
        //                    <p>Untuk verifikasi silahkan klik link di bawah ini</p>
        //                    <p><a title=""Invoice"" href=""" + (AppAddress + "Invoice/Invoice/Edit?id=" + dbObject.Id) + @""">Invoice</a></p>
        //                    <p>&nbsp;</p>
        //                    <p>Atas perhatiannya kami ucapkan terima kasih.</p>";
        //                }
        //                else
        //                {
        //                    body = @"<p><strong>Yth. Bpk / Ibu</strong></p>
        //                    <p>Dengan ini kami sampaikan bahwa Invoice berikut ini, menunggu Revisi dari Bpk/Ibu.<br /><br /></p>
        //                    <p><strong>Detail Invoice :</strong></p>
        //                    <table style=""width: 347px;"">
        //                    <tbody>
        //                    <tr>
        //                    <td style=""width: 129px;"">Invoice Number</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (dbObject.InvoiceNo != null ? dbObject.InvoiceNo : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;"">SO Type</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (salesOrder.SOType != null ? salesOrder.SOType : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;"">Periode</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (salesOrder.Periode != null ? salesOrder.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat) : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;"">Ship To party</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (salesOrder.PowerPlant.Name != null ? salesOrder.PowerPlant.Name : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;"">Sold To Party</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (salesOrder.PowerPlant.SoldToPartyId != null ? salesOrder.PowerPlant.SoldToParty.Name : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;\"">Material</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (salesOrder.PowerPlant.Material != null ? ((Material)Enum.Parse(typeof(Material), salesOrder.PowerPlant.Material)).ToDescription() : "") + @"</td>
        //                    </tr>
        //                    <tr>
        //                    <td style=""width: 129px;"">Notes</td>
        //                    <td style=""width: 10px;"">:</td>
        //                    <td style=""width: 269px;"">" + (message != null ? message : "") + @"</td>
        //                    </tr>
        //                    </tbody>
        //                    </table>
        //                    <p>&nbsp;</p><p>&nbsp;</p>
        //                    <p>Untuk verifikasi silahkan klik link di bawah ini</p>
        //                    <p><a title=""Invoice"" href=""" + (AppAddress + "Invoice/Invoice/Edit?id=" + dbObject.Id) + @""">Invoice</a></p>
        //                    <p>&nbsp;</p>
        //                    <p>Atas perhatiannya kami ucapkan terima kasih.</p>";
        //                }

        //                foreach (Membership m in memberships)
        //                {

        //                    string email = m.Email;
        //                    bool isSuccess = EmailHelper.SendEmail(m.Email, subject, body);
        //                }
        //                return true;
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //        else
        //        {
        //            return false;
        //        }

        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        #endregion
    }
}
