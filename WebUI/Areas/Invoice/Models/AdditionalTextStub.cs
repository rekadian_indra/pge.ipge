﻿namespace WebUI.Areas.Invoice.Models
{
    public class AdditionalTextStub
    {
        public string Text { get; set; }
        public bool IsBold { get; set; }
        public bool IsItalic { get; set; }
        public bool IsUnderline { get; set; }
    }
}