﻿using Business.Infrastructure;
using NPOI.XSSF.UserModel;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using WebUI.Infrastructure;
using WebUI.Models;
using NPOI.SS.UserModel;
using System.Collections.Generic;
using Common.Enums;

namespace WebUI.Areas.SalesOrder.Models
{
	public class CoverLetterExcelStub : InvoicePresentationStub
	{
		#region Fields

		const string INVOICE_NO_SUFFIX = "/INV/PGE310/2017-S4";

		public string NomorSurat;
		public string AtasPemakaianUnit;
		public string AlamatKepada;
		public string SignatureFullName;
		public string SignatureEmployeeNo;
		public string SignatureFilePath;
		public string SignatureDate;
		public int JumlahLampiran;

		public List<InvoiceReceipt> InvoiceReceipts;

		#endregion

		#region Classes

		public class InvoiceReceipt : SalesOrderPresentationStub
		{
			public string InvoiceNo;
			public string AtasPemakaianUnit;
			public string Terbilang;
			public string RekTambahanKMJ123;
			public string TextTambahanUBL34;
            public string AdditionalText;

			public InvoiceReceipt() : base()
			{
				InvoiceNo = "";
				AtasPemakaianUnit = "";
				Terbilang = "";
				RekTambahanKMJ123 = "";
				TextTambahanUBL34 = "";
                AdditionalText = "";
			}

			public InvoiceReceipt(Business.Entities.SalesOrder salesOrder) : base(salesOrder)
			{
                string[] text;
				InvoiceNo = salesOrder.Invoices.FirstOrDefault().InvoiceNo;

				AtasPemakaianUnit = (salesOrder.PowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString()) ? "Listrik" : "Uap";
				AtasPemakaianUnit += $" {salesOrder.PowerPlant.ShipToPartyName}";

				Terbilang = DisplayFormat.Terbilang(salesOrder.NetValue.Value, 5);
				RekTambahanKMJ123 = "";
				TextTambahanUBL34 = "";
                AdditionalText = salesOrder.Invoices.Where(n => !string.IsNullOrEmpty(n.AdditionalText)).Any() ? salesOrder.Invoices.FirstOrDefault(n => !string.IsNullOrEmpty(n.AdditionalText)).AdditionalText : 
                                    "";
                text = AdditionalText.Split(new string[] { "<br/>" }, StringSplitOptions.RemoveEmptyEntries);
                AdditionalText = string.Join("\r\n", text);
			}

            public InvoiceReceipt(Business.Entities.SalesOrder salesOrder, bool hasTransmition = true) : base(salesOrder)
            {
                string[] text;
                InvoiceNo = $"{salesOrder.Invoices.FirstOrDefault().InvoiceNo}-1";

                AtasPemakaianUnit = (salesOrder.PowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString()) ? "Listrik (PE)" : "Uap (PE)";
                AtasPemakaianUnit += $" {salesOrder.PowerPlant.ShipToPartyName}";

                double netValue = salesOrder.Quantity.Value * (salesOrder.PowerPlant.TransmitionPrice ?? 0);
                double transmitionPrice = salesOrder.PowerPlant.TransmitionPrice ?? 0;
                NetPrice = DisplayFormat.NumberFormatFiveDecimal(transmitionPrice);
                NetValue = DisplayFormat.NumberFormatFiveDecimal(netValue);
                Terbilang = DisplayFormat.Terbilang(netValue, 5);
                RekTambahanKMJ123 = "";
                TextTambahanUBL34 = "";
                AdditionalText = salesOrder.Invoices.Where(n => !string.IsNullOrEmpty(n.AdditionalText)).Any() ? salesOrder.Invoices.FirstOrDefault(n => !string.IsNullOrEmpty(n.AdditionalText)).AdditionalText :
                                    "";
                text = AdditionalText.Split(new string[] { "<br/>" }, StringSplitOptions.RemoveEmptyEntries);
                AdditionalText = string.Join("\r\n", text);
            }
        }

		#endregion

		#region Constructor

		public CoverLetterExcelStub()
		{
			NomorSurat = "";
			AlamatKepada = "";
			AtasPemakaianUnit = "";
			InvoiceReceipts = new List<InvoiceReceipt>();
			JumlahLampiran = 0;
		}

		public CoverLetterExcelStub(Business.Entities.Invoice invoice, Business.Entities.Membership signatureMember, List<Business.Entities.SalesOrder> salesOrders) : base(invoice)
		{
			NomorSurat = invoice.InvoiceSequence + INVOICE_NO_SUFFIX;
			AlamatKepada = invoice.SalesOrder.PowerPlant.SoldToParty.Name;
            CultureInfo cultureinfo = new CultureInfo("id-ID");
            SignatureDate = DateTime.Now.ToString(DisplayFormat.FullDateFormat, cultureinfo);
			SignatureFullName = signatureMember.FullName;
			SignatureEmployeeNo = signatureMember.EmployeeNo;
			SignatureFilePath = signatureMember.SignatureFilePath;

			AtasPemakaianUnit = "";
			InvoiceReceipts = new List<InvoiceReceipt>();
			foreach (Business.Entities.SalesOrder salesOrder in salesOrders)
			{
				AtasPemakaianUnit += (AtasPemakaianUnit != "") ? ", " : "";
				AtasPemakaianUnit += (salesOrder.PowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString()) ? "Listrik" : "Uap";
				AtasPemakaianUnit += $" {salesOrder.PowerPlant.ShipToPartyName}";

				InvoiceReceipts.Add(new InvoiceReceipt(salesOrder));

                //jika harga transmisi lebih besar dari nol, maka tambahkan no invoicenya ke list
                double transmitionPrice = salesOrder.PowerPlant.TransmitionPrice ?? 0;
                if (transmitionPrice > 0)
                    InvoiceReceipts.Add(new InvoiceReceipt(salesOrder, true));
            }
			JumlahLampiran = InvoiceReceipts.Count * 2;
		}

		#endregion
	}
}