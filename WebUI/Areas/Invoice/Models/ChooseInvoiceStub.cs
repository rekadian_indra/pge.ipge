﻿using WebUI.Models;
using Business.Entities;
using Common.Enums;
using System.Collections.Generic;
using System.ComponentModel;
using System;

namespace WebUI.Areas.Invoice.Models
{
    public class ChooseInvoiceStub
    {
		[DisplayName("Periode")]
		public DateTime? Periode { get; set; }

		[DisplayName("SO Type")]
		public string SOType { get; set; }

		[DisplayName("Power Plant")]
		public int? PowerPlantId { get; set; }

		public ChooseInvoiceStub()
        {

		}
	}
}