﻿using WebUI.Models;
using Business.Entities;
using Common.Enums;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebUI.Areas.Invoice.Models
{
    public class EditInvoiceStub : InvoiceFormStub
    {
        const string DEFAULT_ADDITIONAL_TEXT = "<p>Pembayaran harap dilakukan melalui:</p>" +
            "<p><strong>Bank MANDIRI KCP. JKT KP PERTAMINA</strong></p>" +
            "<p><strong>Rekening PT. Pertamina Gethermal Energy</strong></p>" +
            "<p><strong>QQ Penerimaan USD</strong></p>" +
            "<p><strong>Nomor Rekening: 119-00-0468535-8</strong></p>" +
            "<p><strong>SWIFT CODE: BMRIIDJAAXXX</strong></p>";

        public WorkflowPanelStub WorkflowPanel { get; set; }
		public ChooseInvoiceStub ChooseInvoice { get; set; }

		public bool IsDraft { get; set; }

		public WorkflowType WorkflowType { get; set; }
		public InvoiceApproval LastInvoiceApproval { get; set; }
        public bool IsAllArea { get; set; }
        public string AdditionalText { get; set; }

        [AllowHtml]
        public string CurrentAdditionalText { get; set; }

        public double TransmitionPrice { get; set; }

		public EditInvoiceStub() : base()
        {

		}

		public EditInvoiceStub(Business.Entities.Invoice dbObject) : base(dbObject)
		{
			CalculateWorkflowType(dbObject.SalesOrder);

            if (string.IsNullOrEmpty(dbObject.AdditionalText))
            {
                CurrentAdditionalText = DEFAULT_ADDITIONAL_TEXT;
            }
            else
            {
                CurrentAdditionalText = dbObject.AdditionalText;
            }

            TransmitionPrice = dbObject.SalesOrder.PowerPlant.TransmitionPrice ?? 0;
            if (TargetSendBackDate == null)
            {
                if (dbObject.AcceptedDateBy != null)
                {
                    TargetSendBackDate = dbObject.AcceptedDateBy.Value.AddDays(30);
                }
            }
        }

		public WorkflowType CalculateWorkflowType(Business.Entities.SalesOrder salesOrder)
		{
			WorkflowType = (salesOrder.PowerPlant.Material == Material.ELECTRIC_POWER.ToString()) ? WorkflowType.INVEL : WorkflowType.INVSM;
			return WorkflowType;
		}
	}
}