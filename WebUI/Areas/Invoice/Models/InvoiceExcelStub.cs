﻿using Business.Infrastructure;
using NPOI.XSSF.UserModel;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using WebUI.Infrastructure;
using WebUI.Models;
using NPOI.SS.UserModel;
using System.Collections.Generic;
using Business.Entities;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using WebUI.Areas.Invoice.Models;
using Common.Enums;
using NPOI.HSSF.UserModel;

namespace WebUI.Areas.SalesOrder.Models
{
	public class InvoiceExcelStub : BaseExcelStub
	{
        const string TARGET_NODE = "//p";
        const string DEFAULT_ADDITIONAL_TEXT = "<p>Pembayaran harap dilakukan melalui:</p>" +
            "<p><strong>Bank MANDIRI KCP. JKT KP PERTAMINA</strong></p>" +
            "<p><strong>Rekening PT. Pertamina Gethermal Energy</strong></p>" +
            "<p><strong>QQ Penerimaan USD</strong></p>" +
            "<p><strong>Nomor Rekening: 119-00-0468535-8</strong></p>" +
            "<p><strong>SWIFT CODE: BMRIIDJAAXXX</strong></p>";

        #region Fields

        #endregion

        #region Constructor

        public InvoiceExcelStub() : base()
		{

		}

		public InvoiceExcelStub(XSSFWorkbook workbook) : base(workbook)
		{

		}

		#endregion

		public byte[] GenerateTemplate(Business.Entities.Invoice invoice, Membership signatureMember, List<Business.Entities.SalesOrder> salesOrders)
		{
			//culture
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US"); //supaya file tidak corrupt

			ProcessInvoice(invoice, signatureMember, salesOrders);

			//write to byte[]
			MemoryStream ms = new MemoryStream();

			Workbook.Write(ms);

			return ms.ToArray();
		}

		public void ProcessInvoice(Business.Entities.Invoice invoice, Membership signatureMember, List<Business.Entities.SalesOrder> salesOrders)
		{
            // proses data
            CoverLetterExcelStub model = new CoverLetterExcelStub(invoice, signatureMember, salesOrders);

            // set sheet Surat Pengantar
            int rowIdx = 0;
            XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(0);
            XSSFRow row;
            XSSFCell cell;
            XSSFFont font;
            XSSFFont font1;
            XSSFFont font2;
            XSSFCellStyle style;
            XSSFCellStyle style1;
            XSSFCellStyle style2;
            XSSFCellStyle style3;
            XSSFCellStyle style4;
            XSSFCellStyle style5;

            font1 = (XSSFFont)Workbook.CreateFont();
            font1.FontName = HSSFFont.FONT_ARIAL;
            font1.FontHeightInPoints = 10;

            font2 = (XSSFFont)Workbook.CreateFont();
            font2.IsBold = true;
            font2.FontName = HSSFFont.FONT_ARIAL;
            font2.FontHeightInPoints = 12;

            style1 = (XSSFCellStyle)Workbook.CreateCellStyle();
            style1.SetFont(font1);
            style1.WrapText = true;
            style1.BorderLeft = BorderStyle.Thin;
            style1.BorderRight = BorderStyle.Thin;
            style1.BorderBottom = BorderStyle.Thin;

            style2 = (XSSFCellStyle)Workbook.CreateCellStyle();
            style2.SetFont(font2);
            style2.VerticalAlignment = VerticalAlignment.Center;
            style2.WrapText = true;
            style2.BorderLeft = BorderStyle.Thin;
            style2.BorderRight = BorderStyle.Thin;
            style2.BorderBottom = BorderStyle.Thin;

            style3 = (XSSFCellStyle)Workbook.CreateCellStyle();
            style3.SetFont(font2);
            style3.VerticalAlignment = VerticalAlignment.Center;
            style3.WrapText = true;

            style4 = (XSSFCellStyle)Workbook.CreateCellStyle();
            style4.SetFont(font1);
            style4.WrapText = true;
            style4.BorderLeft = BorderStyle.Thin;
            style4.BorderRight = BorderStyle.Thin;

            style5 = (XSSFCellStyle)Workbook.CreateCellStyle();
            style5.SetFont(font1);

            // masuk2in data
            SetCellValue(sheet, "B", 1 + 5, model.SignatureDate);
            SetCellValue(sheet, "B", 2 + 5, model.NomorSurat);
            SetCellValue(sheet, "A", 5 + 5, $"Perihal : Tagihan Pemakaian {model.AtasPemakaianUnit}");
            SetCellValue(sheet, "A", 6 + 5, $"Lampiran : {model.JumlahLampiran} ({DisplayFormat.Terbilang(model.JumlahLampiran, 0)}) berkas");
            SetCellValue(sheet, "A", 10 + 5, model.AlamatKepada);
            SetCellValue(sheet, "A", 13 + 5, $"Terlampir kami sampaikan Tagihan Pemakaian {model.AtasPemakaianUnit} Periode {model.Period} sebagai berikut:");
          

            //tulis kata-kata dan nomor invoice di surat pengantar
            int targetRowIndex = 14 + 5;
            for (int i = 0; i < model.InvoiceReceipts.Count; i++)
            {
                row = (XSSFRow)sheet.GetRow(targetRowIndex);
                if (row == null)
                {
                    row = (XSSFRow)sheet.CreateRow(targetRowIndex);
                }
                cell = (XSSFCell)row.GetCell(1);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(1);
                }
                cell.SetCellValue($"{model.InvoiceReceipts[i].InvoiceNo} {model.InvoiceReceipts[i].AtasPemakaianUnit}");

                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(targetRowIndex, targetRowIndex, 1, 9));

                targetRowIndex++;
            }

            //SetCellValue(sheet, "A", (targetRowIndex + 1), $"Pembayaran dilakukan secara penuh (tidak dipotong biaya bank)");
            //sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress((targetRowIndex + 1), (targetRowIndex + 1), 1, 9));

            //SetCellValue(sheet, "A", (targetRowIndex + 3), $"Demikian kami sampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih.");
            //sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress((targetRowIndex + 3), (targetRowIndex + 3), 1, 9));

            SetCellValue(sheet, "A", (targetRowIndex + 6), "PT PERTAMINA GEOTHERMAL ENERGY");
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress((targetRowIndex + 6), (targetRowIndex + 6), 0, 9));

            SetCellValue(sheet, "A", (targetRowIndex + 8), model.SignaturePosition ?? "");
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress((targetRowIndex + 8), (targetRowIndex + 8), 0, 9));

            row = (XSSFRow)sheet.GetRow(targetRowIndex + 13);
            if (row == null)
            {
                row = (XSSFRow)sheet.CreateRow(targetRowIndex + 13);
            }
            cell = (XSSFCell)row.GetCell(0);
            if (cell == null)
            {
                cell = (XSSFCell)row.CreateCell(0);
            }
            cell.SetCellValue(model.SignatureFullName);
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress((targetRowIndex + 13), (targetRowIndex + 13), 0, 9));

            // iterasi
            for (int i = 0; i < model.InvoiceReceipts.Count; i++)
			{
				if (i > 0)
				{
					// copy sheet invoice & kuitansi sebutuhnya
					Workbook.CloneSheet(1);
					Workbook.CloneSheet(2);
				}

                CoverLetterExcelStub.InvoiceReceipt curInvoiceReceipt = model.InvoiceReceipts.ElementAt(i);               

                // process invoice
                sheet = (XSSFSheet)Workbook.GetSheetAt(1 + (i * 2));
				SetCellValue(sheet, "D", 9, curInvoiceReceipt.InvoiceNo);
				SetCellValue(sheet, "B", 9, model.AlamatKepada);
				SetCellValue(sheet, "A", 16, model.SignatureDate);
				SetCellValue(sheet, "B", 16, $"Tagihan atas pemakaian {curInvoiceReceipt.AtasPemakaianUnit}");
				SetCellValue(sheet, "B", 18, $"Periode {model.Period}");
				SetCellValue(sheet, "B", 21, curInvoiceReceipt.Terbilang);

                string s = "";
                List<AdditionalTextStub> additionalTexts = new List<AdditionalTextStub>();

                if (!string.IsNullOrEmpty(curInvoiceReceipt.AdditionalText))
                {
                    s = curInvoiceReceipt.AdditionalText.Replace("<p>", "!").Replace("</p>", "").Replace("<br />","");
                }
                else
                {
                    s = DEFAULT_ADDITIONAL_TEXT.Replace("<p>", "!").Replace("</p>", "").Replace("<br />", "");
                }

                //split each text from s
                string[] splits = s.Split('!');

                if (splits.Any() && splits != null)
                {
                    for (int j = 0; j < splits.Count(); j++)
                    {
                        string text = "";
                        bool isBold = false;
                        bool isItalic = false;
                        bool isUnderline = false;

                        if (j == 0)
                        {
                            if (splits[j] == "") { continue; }
                        }

                        if (splits[j] == "<strong></strong>") { splits[j] = ""; }

                        text = splits[j];
                        text = text.Replace("<i>", "");
                        text = text.Replace("</i>", "");
                        text = text.Replace("<u>", "");
                        text = text.Replace("</u>", "");
                        text = text.Replace("<strong>", "");
                        text = text.Replace("</strong>", "");

                        if (splits[j].Contains("<strong>")) { isBold = true; }
                        if (splits[j].Contains("<i>")) { isItalic = true; }
                        if (splits[j].Contains("<u>")) { isUnderline = true; }

                        AdditionalTextStub temp = new AdditionalTextStub()
                        {
                            Text = text,
                            IsBold = isBold,
                            IsItalic = isItalic,
                            IsUnderline = isUnderline
                        };
                        additionalTexts.Add(temp);
                    }
                }

                //print Additional Text
                if (additionalTexts.Any())
                {
                    rowIdx = 23;
                    for (int h = 0; h < additionalTexts.Count; h++)
                    {
                        font = (XSSFFont)Workbook.CreateFont();
                        font.FontName = HSSFFont.FONT_ARIAL;
                        font.FontHeightInPoints = 8;

                        style = (XSSFCellStyle)Workbook.CreateCellStyle();
                        style.WrapText = true;
                        style.VerticalAlignment = VerticalAlignment.Center;
                        style.BorderLeft = BorderStyle.Thin;
                        style.BorderRight = BorderStyle.Thin;

                        row = (XSSFRow)sheet.GetRow(rowIdx);
                        if (row == null)
                        {
                            row = (XSSFRow)sheet.CreateRow(rowIdx);
                        }
                        cell = (XSSFCell)row.GetCell(1);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(1);
                        }

                        font.IsBold = additionalTexts[h].IsBold;
                        font.IsItalic = additionalTexts[h].IsItalic;
                        if (additionalTexts[h].IsUnderline == true)
                        {
                            font.Underline = FontUnderlineType.Single;
                        }
                        else
                        {
                            font.Underline = FontUnderlineType.None;
                        }

                        cell.SetCellValue(additionalTexts[h].Text);
                        style.SetFont(font);
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.GetCell(0);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(0);
                        }
                        cell.SetCellValue("");
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.GetCell(2);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(2);
                        }
                        cell.SetCellValue("");
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.GetCell(3);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(3);
                        }
                        cell.SetCellValue("");
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.GetCell(4);
                        if (cell == null)
                        {
                            cell = (XSSFCell)row.CreateCell(4);
                        }
                        cell.SetCellValue("");
                        cell.CellStyle = style;

                        rowIdx++;
                    }
                    
                }

                //ROW KOSONG KE-1
                row = (XSSFRow)sheet.GetRow(rowIdx);
                if (row == null)
                {
                    row = (XSSFRow)sheet.CreateRow(rowIdx);
                }
                cell = (XSSFCell)row.GetCell(0);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(0);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(1);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(1);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(2);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(2);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(3);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(3);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(4);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(4);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                //Rekening Tambahan
                row = (XSSFRow)sheet.GetRow(rowIdx + 1);
                if (row == null)
                {
                    row = (XSSFRow)sheet.CreateRow(rowIdx);
                }
                cell = (XSSFCell)row.GetCell(0);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(0);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(1);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(1);
                }
                cell.SetCellValue(curInvoiceReceipt.RekTambahanKMJ123);
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(2);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(2);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(3);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(3);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(4);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(4);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                //ROW KOSONG KE-2
                row = (XSSFRow)sheet.GetRow(rowIdx + 2);
                if (row == null)
                {
                    row = (XSSFRow)sheet.CreateRow(rowIdx);
                }

                cell = (XSSFCell)row.GetCell(0);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(0);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(1);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(1);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(2);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(2);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(3);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(3);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(4);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(4);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                //Keterangan pelunasan
                row = (XSSFRow)sheet.GetRow(rowIdx + 3);
                if (row == null)
                {
                    row = (XSSFRow)sheet.CreateRow(rowIdx);
                }
                row.HeightInPoints = 32;

                cell = (XSSFCell)row.GetCell(0);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(0);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(1);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(1);
                }
                cell.SetCellValue("Tagihan ini agar dilunasi paling lambat 30 hari kalender setelah tagihan diterima");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(2);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(2);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(3);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(3);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(4);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(4);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                //Text tambahan UBL34
                row = (XSSFRow)sheet.GetRow(rowIdx + 4);
                if (row == null)
                {
                    row = (XSSFRow)sheet.CreateRow(rowIdx);
                }
                cell = (XSSFCell)row.GetCell(0);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(0);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(1);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(1);
                }
                cell.SetCellValue(curInvoiceReceipt.TextTambahanUBL34);
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(2);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(2);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(3);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(3);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                cell = (XSSFCell)row.GetCell(4);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(4);
                }
                cell.SetCellValue("");
                //cell.CellStyle = style1;

                //NET VALUE
                row = (XSSFRow)sheet.GetRow(rowIdx + 5);
                if (row == null)
                {
                    row = (XSSFRow)sheet.CreateRow(rowIdx);
                }
                row.HeightInPoints = 25;
                cell = (XSSFCell)row.GetCell(3);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(3);
                }
                cell.SetCellValue("Total");

                cell = (XSSFCell)row.GetCell(4);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(4);
                }
                cell.SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), "{0:N}", curInvoiceReceipt.NetValueSO));

                row = (XSSFRow)sheet.GetRow(rowIdx + 4);
                for (int c = 0; c < row.Cells.Count; c++)
                {
                    row.Cells[c].CellStyle = style1;
                }

                row = (XSSFRow)sheet.GetRow(rowIdx + 5);
                cell = (XSSFCell)row.GetCell(3);
                cell.CellStyle = style3;
                cell = (XSSFCell)row.GetCell(4);
                cell.CellStyle = style2;

                row = (XSSFRow)sheet.GetRow(rowIdx);
                for (int c = 0; c < row.Cells.Count; c++)
                {
                    row.Cells[c].CellStyle = style4;
                }

                row = (XSSFRow)sheet.GetRow(rowIdx + 1);
                for (int c = 0; c < row.Cells.Count; c++)
                {
                    row.Cells[c].CellStyle = style4;
                }

                row = (XSSFRow)sheet.GetRow(rowIdx + 2);
                for (int c = 0; c < row.Cells.Count; c++)
                {
                    row.Cells[c].CellStyle = style4;
                }

                row = (XSSFRow)sheet.GetRow(rowIdx + 3);
                for (int c = 0; c < row.Cells.Count; c++)
                {
                    row.Cells[c].CellStyle = style4;
                }

                SetCellValue(sheet, "C", 18, String.Format(CultureInfo.CreateSpecificCulture("id-id"), "{0:N}", curInvoiceReceipt.QuantitySO));
				SetCellValue(sheet, "D", 18, String.Format(CultureInfo.CreateSpecificCulture("id-id"), "{0:N5}", curInvoiceReceipt.NetPriceSO));
				SetCellValue(sheet, "E", 18, String.Format(CultureInfo.CreateSpecificCulture("id-id"), "{0:N}", curInvoiceReceipt.NetValueSO));

                row = (XSSFRow)sheet.GetRow(rowIdx + 7);
                if (row == null) { row = row = (XSSFRow)sheet.CreateRow(rowIdx + 7); }
                cell = (XSSFCell)row.GetCell(0);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(0);
                }
                cell.SetCellValue("Jakarta,");
                cell.CellStyle = style5;

                cell = (XSSFCell)row.GetCell(1);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(1);
                }
                cell.SetCellValue(model.SignatureDate);
                cell.CellStyle = style5;

                row = (XSSFRow)sheet.GetRow(rowIdx + 8);
                if (row == null) { row = row = (XSSFRow)sheet.CreateRow(rowIdx + 8); }
                cell = (XSSFCell)row.GetCell(0);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(0);
                }
                cell.SetCellValue("PT PERTAMINA GEOTHERMAL ENERGY");
                cell.CellStyle = style5;

                row = (XSSFRow)sheet.GetRow(rowIdx + 9);
                if (row == null) { row = row = (XSSFRow)sheet.CreateRow(rowIdx + 9); }
                cell = (XSSFCell)row.GetCell(0);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(0);
                }
                cell.SetCellValue(model.SignaturePosition);
                cell.CellStyle = style5;

                row = (XSSFRow)sheet.GetRow(rowIdx + 10);
                if (row == null) { row = row = (XSSFRow)sheet.CreateRow(rowIdx + 10); }
                cell = (XSSFCell)row.GetCell(3);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(3);
                }
                cell.SetCellValue("Menara Cakrawala Lt.8");
                cell.CellStyle = style5;

                row = (XSSFRow)sheet.GetRow(rowIdx + 11);
                if (row == null) { row = row = (XSSFRow)sheet.CreateRow(rowIdx + 11); }
                cell = (XSSFCell)row.GetCell(3);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(3);
                }
                cell.SetCellValue("Jl. MH Thamrin No.9");
                cell.CellStyle = style5;

                row = (XSSFRow)sheet.GetRow(rowIdx + 12);
                if (row == null) { row = row = (XSSFRow)sheet.CreateRow(rowIdx + 12); }
                cell = (XSSFCell)row.GetCell(3);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(3);
                }
                cell.SetCellValue("Jakarta 10430 Indonesia");
                cell.CellStyle = style5;

                row = (XSSFRow)sheet.GetRow(rowIdx + 13);
                if (row == null) { row = row = (XSSFRow)sheet.CreateRow(rowIdx + 13); }
                cell = (XSSFCell)row.GetCell(3);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(3);
                }
                cell.SetCellValue("T : +62 21 39833222");
                cell.CellStyle = style5;

                row = (XSSFRow)sheet.GetRow(rowIdx + 14);
                if (row == null) { row = row = (XSSFRow)sheet.CreateRow(rowIdx + 14); }
                cell = (XSSFCell)row.GetCell(3);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(3);
                }
                cell.SetCellValue("F : +62 21 39833230");
                cell.CellStyle = style5;

                row = (XSSFRow)sheet.GetRow(rowIdx + 14);
                if (row == null) { row = row = (XSSFRow)sheet.CreateRow(rowIdx + 14); }
                cell = (XSSFCell)row.GetCell(0);
                if (cell == null)
                {
                    cell = (XSSFCell)row.CreateCell(0);
                }
                cell.SetCellValue(model.SignatureFullName);
                cell.CellStyle = style5;

                if (model.SignatureFilePath != null) SetCellImage(sheet, "A", rowIdx + 11, "A", rowIdx + 14, model.SignatureFilePath);

                // process kuitansi
                sheet = (XSSFSheet)Workbook.GetSheetAt(2 + (i * 2));
				SetCellValue(sheet, "E", 8, curInvoiceReceipt.Terbilang);
				SetCellValue(sheet, "E", 12, $"Tagihan Pemakaian {curInvoiceReceipt.AtasPemakaianUnit} No. {curInvoiceReceipt.InvoiceNo} Periode {model.Period}");
				SetCellValue(sheet, "C", 16, String.Format(CultureInfo.CreateSpecificCulture("id-id"), "{0:N}", curInvoiceReceipt.NetValueSO));
				SetCellValue(sheet, "G", 18, model.SignatureDate);
				SetCellValue(sheet, "F", 20, model.SignaturePosition);
				SetCellValue(sheet, "F", 28, model.SignatureFullName);
				//if (model.SignatureFilePath != null) SetCellImage(sheet, "F", 22, "I", 26, model.SignatureFilePath);
			}
        }
		
		
	}
}