﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Controllers;
using Business.Abstract;
using MvcSiteMapProvider;
using WebUI.Infrastructure;
using System.Threading.Tasks;
using Common.Enums;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebUI.Areas.DataMaster.Models.Clusters;
using WebUI.Extension;
using WebUI.Models;
using Business.Infrastructure;
using Newtonsoft.Json;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.DataMaster.Controllers
{
    [CheckSessionTimeOut]
    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.OPERATION_SUPERVISOR, UserRole.OPERATION_OPERATOR })]
    public class ClusterController : BaseController<ClusterFormStub>
    {
        public ClusterController
       (
           IAreaRepository repoArea,
           IClusterRepository repoCluster,
           IPowerPlantRepository repoPowerPlant
       )
        {
            RepoArea = repoArea;
            RepoCluster = repoCluster;
            RepoPowerPlant = repoPowerPlant;
        }

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.Cluster, Area = AreaSite.DataMaster, ParentKey = KeySite.Dashboard, Key = KeySite.IndexCluster)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexCluster, Key = KeySite.CreateCluster)]
        [RequiredParameter(QueryStringValue.Area, IncludePost = false, Mode = MatchMode.Any)]
        public async Task<ActionResult> Create(int? areaId)
        {
            ClusterFormStub model = new ClusterFormStub()
            {
                AreaId = areaId
            };

            await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(ClusterFormStub model)
        {
            if (ModelState.IsValid)
            {
               Cluster dbObject = new Cluster();

                model.MapDbObject(dbObject);
                await RepoCluster.SaveAsync(dbObject);

                ////send email
                //string emailTo = "rahayusanti05@gmail.com";
                //string subject = "Test Email Sender";
                //string body = "Ini adalah test untuk send email";
                //bool resultEmail = EmailHelper.SendEmail(emailTo, subject,body);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Name, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexCluster, Key = KeySite.EditCluster)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
           Cluster dbObject = await RepoCluster.FindByPrimaryKeyAsync(primaryKey);
            ClusterFormStub model = new ClusterFormStub(dbObject);

            ViewBag.Breadcrumb = model.Code;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(ClusterFormStub model)
        {
            if (ModelState.IsValid)
            {
                Cluster dbObject = await RepoCluster.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoCluster.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Name, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.Code;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
           Cluster dbObject = await RepoCluster.FindByPrimaryKeyAsync(primaryKey);

            if (!(await RepoCluster.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<Cluster> dbObjects;
            List<ClusterPresentationStub> models;
            Task<List<Cluster>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;            

            if (param.Filter != null)
            {
                if (param.Filter.Filters.Any())
                {
                   
                    if (param.Filter.Filters.Exists(f => f.Field == Field.AreaName))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.AreaName);
                        param.Filter.AddFilter(new FilterQuery(new FieldHelper(Table.Area, Field.Name), FilterOperator.Contains, filterFmn.Value));
                        param.Filter.Filters.Remove(filterFmn);
                    }
                    if (param.Filter.Filters.Exists(f => f.Field == Field.ClusterId))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.ClusterId);
                        param.Filter.Filters.Remove(filterFmn);
                    }
                    if (param.Filter.Filters.Exists(f => f.Field == Field.WellId))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.WellId);
                        param.Filter.Filters.Remove(filterFmn);
                    }
                    if (param.Filter.Filters.Exists(f => f.Field == Field.ExplorationDate))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.ExplorationDate);
                        param.Filter.Filters.Remove(filterFmn);
                    }
                }
            }

            if (param.Sorts != null)
            {
                if (param.Sorts.Exists(s => s.Field == Field.AreaName))
                {
                    SortQuery sortFmn = param.Sorts.FirstOrDefault(s => s.Field == Field.AreaName);
                    param.Sorts.Add(new SortQuery(new FieldHelper(Table.Area, Field.Name), sortFmn.Direction));
                    param.Sorts.Remove(sortFmn);
                }
            }

            if (param.Filter == null)
                param.InstanceFilter();

            if (User.AreaId.HasValue && User.AreaId > 0)
                param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, User.AreaId);

            //algorithm
            dbObjectsTask = RepoCluster.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoCluster.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Cluster, ClusterPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion

        #region "Helper"
    
        #endregion
    }
}