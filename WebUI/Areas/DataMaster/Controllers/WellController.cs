﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.DataMaster.Models.Wells;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Infrastructure.Attribute;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Controllers
{
    [CheckSessionTimeOut]
    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin , UserRole.OPERATION_SUPERVISOR, UserRole.OPERATION_OPERATOR})]
    public class WellController : BaseController<WellFormStub>
    {
        public WellController
        (
            IAreaRepository repoArea,
            IClusterRepository repoCluster,
            IWellRepository repoWell,
            IPowerPlantRepository repoPowerPlant
        )
        {
            RepoArea = repoArea;
            RepoCluster = repoCluster;
            RepoWell = repoWell;
            RepoPowerPlant = repoPowerPlant;
        }

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.Well, Area = AreaSite.DataMaster, ParentKey = KeySite.Dashboard, Key = KeySite.IndexWell)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexWell, Key = KeySite.CreateWell)]
        [RequiredParameter(QueryStringValue.Area, IncludePost = false, Mode = MatchMode.Any)]
        public async Task<ActionResult> Create(int? areaId, int? clusterId)
        {
            WellFormStub model = new WellFormStub()
            {
                AreaId = areaId,
                ClusterId = clusterId
            };

            await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(WellFormStub model)
        {
            if (ModelState.IsValid)
            {
                Well dbObject = new Well();

                model.MapDbObject(dbObject);
                await RepoWell.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Name, template);

                return RedirectToAction(ActionSite.History, ControllerSite.WellHistory, new { wellId = dbObject.Id });
                //return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexWell, Key = KeySite.EditWell)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Well dbObject = await RepoWell.FindByPrimaryKeyAsync(primaryKey);
            WellFormStub model = new WellFormStub(dbObject);

            ViewBag.Breadcrumb = model.Code;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(WellFormStub model)
        {
            if (ModelState.IsValid)
            {
                Well dbObject = await RepoWell.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoWell.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Name, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.Code;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Well dbObject = await RepoWell.FindByPrimaryKeyAsync(primaryKey);

            if (!(await RepoWell.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<Well> dbObjects;
            List<WellPresentationStub> models;
            Task<List<Well>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;
            FilterQuery filterStatus = null;
            SortQuery sortStatus = null;

            if (param.Filter != null)
            {
                if (param.Filter.Filters.Any())
                {

                    if (param.Filter.Filters.Exists(f => f.Field == Field.WellId))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.WellId);
                        param.Filter.Filters.Remove(filterFmn);
                    }
                    if (param.Filter.Filters.Exists(f => f.Field == Field.ClusterId))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.ClusterId);
                        if (filterFmn.Value.ToString() == "0" || filterFmn.Value.ToString() == "")
                        {
                            param.Filter.Filters.Remove(filterFmn);
                        }
                    }

                    if (param.Filter.Filters.Exists(f => f.Field == Field.AreaId))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.AreaId);
                        if (filterFmn.Value.ToString() == "0" || filterFmn.Value.ToString() == "")
                        {
                            param.Filter.Filters.Remove(filterFmn);
                        }
                    }

                    if (param.Filter.Filters.Exists(f => f.Field == Field.AreaName))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.AreaName);
                        param.Filter.AddFilter(new FilterQuery(new FieldHelper(Table.Area, Field.Name), FilterOperator.Contains, filterFmn.Value));
                        param.Filter.Filters.Remove(filterFmn);
                    }

                    if (param.Filter.Filters.Exists(f => f.Field == Field.ClusterName))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.ClusterName);
                        param.Filter.AddFilter(new FilterQuery(new FieldHelper(Table.Cluster, Field.Name), FilterOperator.Contains, filterFmn.Value));
                        param.Filter.Filters.Remove(filterFmn);
                    }

                    filterStatus = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.Status);
                    if (filterStatus != null)
                    {
                        param.Filter.Filters.Remove(filterStatus);
                    }

                }
            }

            if (param.Sorts != null)
            {
                if (param.Sorts.Exists(s => s.Field == Field.AreaName))
                {
                    SortQuery sortFmn = param.Sorts.FirstOrDefault(s => s.Field == Field.AreaName);
                    param.Sorts.Add(new SortQuery(new FieldHelper(Table.Area, Field.Name), sortFmn.Direction));
                    param.Sorts.Remove(sortFmn);
                }
                if (param.Sorts.Exists(s => s.Field == Field.ClusterName))
                {
                    SortQuery sortFmn = param.Sorts.FirstOrDefault(s => s.Field == Field.ClusterName);
                    param.Sorts.Add(new SortQuery(new FieldHelper(Table.Cluster, Field.Name), sortFmn.Direction));
                    param.Sorts.Remove(sortFmn);
                }
                if (param.Sorts.Exists(s => s.Field == Field.Status))
                {
                    SortQuery sortFmn = param.Sorts.FirstOrDefault(s => s.Field == Field.Status);
                    param.Sorts.Add(new SortQuery(new FieldHelper(Table.WellHistory, Field.Status), sortFmn.Direction));
                    param.Sorts.Remove(sortFmn);
                }
                sortStatus = param.Sorts.FirstOrDefault(s => s.Field == Field.Status);
                if (sortStatus != null)
                {
                    param.Sorts.Remove(sortStatus);
                }
            }

            if (param.Filter == null)
                param.InstanceFilter();

            if (User.AreaId.HasValue && User.AreaId > 0)
                param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, User.AreaId);

            //algorithm
            dbObjectsTask = RepoWell.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoWell.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Well, WellPresentationStub>(dbObjects);

            if (filterStatus != null)
            {
                models = models.FindAll(m => m.Status.ToUpper().Contains(filterStatus.Value.ToString().ToUpper()));
                count = models.Count();
                models = models.Skip(param.Skip).Take(param.Take).ToList();
            }
            if (sortStatus != null)
            {
                if (sortStatus.Direction.Value.ToString().ToUpper() == "DESCENDING")
                {
                    models = models.OrderByDescending(m => m.Status).ToList();
                }
                else
                {
                    models = models.OrderBy(m => m.Status).ToList();
                }
            }
            
            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion

        #region "Helper"

        #endregion
    }
}