﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Web;
using System.Linq;
using WebUI.Extension;
using Resources;
using WebUI.Areas.DataMaster.Models;
using MvcSiteMapProvider.Web.Mvc.Filters;
using NPOI.XSSF.UserModel;
using System.IO;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.DataMaster.Controllers
{
    [CheckSessionTimeOut]
    [Authorize]
    public class MaintainPriceElectricController : BaseController<MaintainPriceElectricFormStub>
    {
        public MaintainPriceElectricController
        (
            ILogRepository repoLog,
            IPpiRepository repoPPI,
            ISteamPowerPlantPriceRepository repoSteamPowerPlantPrice,
            IPowerPlantRepository repoPowerPlant,
            IWorkflowRepository repoWorkflow,
            ISalesOrderRepository repoSalesOrder,
            IPpiApprovalRepository repoPpiApproval,
            IMembershipRepository repoMembership,
            IUserRepository repoUser,
			IAreaRepository repoArea
		)
        {
            RepoLog = repoLog;
            RepoPPI = repoPPI;
			RepoSteamPowerPlantPrice = repoSteamPowerPlantPrice;
			RepoPowerPlant = repoPowerPlant;
			RepoWorkflow = repoWorkflow;
			RepoSalesOrder = repoSalesOrder;
			RepoPpiApproval = repoPpiApproval;
            RepoMembership = repoMembership;
			RepoUser = repoUser;
            RepoArea = repoArea;
		}

        #region View CRUD
		[MvcSiteMapNode(Title = TitleSite.MaintainHargaListrik, Area = AreaSite.DataMaster, ParentKey = KeySite.Dashboard, Key = KeySite.IndexMaintainHargaListrik)]
		public override async Task<ActionResult> Index()
		{
			ViewBag.powerPlantId = TempData["powerPlantId"] ?? 0;
			return await base.Index();
		}

		public ActionResult BackToIndex(int powerPlantId)
		{
			TempData.Add("powerPlantId", powerPlantId);

			return RedirectToAction(ActionSite.Index);
		}

		public ActionResult Check(int powerPlantId, int year, byte quarterNum)
		{
			List<PPI> ppis = RepoPPI.GetFromPowerPlantId(powerPlantId, year, quarterNum);

			if (ppis.Any())
			{
				PPI lastPpi = ppis.First();
				return RedirectToAction(ActionSite.Edit, new
				{
					id = lastPpi.Id
				});
			}
			else
			{
				return RedirectToAction(ActionSite.Create, new
				{
					powerPlantId = powerPlantId,
					year = year,
					quarterNum = quarterNum
				});
			}
		}

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexMaintainHargaListrik, Key = KeySite.CreateMaintainHargaListrik)]
        [RequiredParameter(QueryStringValue.PowerPlant, IncludePost = false)]
        public async Task<ActionResult> Create(int powerPlantId, int? year = null, byte? quarterNum = null)
        {
			List<Business.Entities.Workflow> workflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.PRICE);
			Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync(powerPlantId);

			Task.WaitAll(powerPlantTask);
			Business.Entities.PowerPlant powerPlant = powerPlantTask.Result;

			MaintainPriceElectricFormStub model = new MaintainPriceElectricFormStub(workflows, powerPlant.BasePricePPI);
			if (powerPlant.SOType == SOType.ZTS.ToString()) // kalau ZTS, lgsg 1 step sebelum beres
			{
				int beforeLastWorkflowId = workflows.First(x => x.Approver == null)?.PreviousStep ?? 0;
				model.SetWorkflowId(beforeLastWorkflowId);
				model.IsRevisable = false;
			}

			List<PPI> ppis = RepoPPI.GetFromPowerPlantId(powerPlantId);
			if (ppis.Any() && (year == null)) // kalau blm masukin year nya, defaultnya yg next nya aja
			{
				PPI lastPpi = ppis.Where(x => x.Year == ppis.Max(y => y.Year)).OrderBy(x => x.QuarterNum).Last();
				model.NextPpi(lastPpi);
			}
			else
			{
				model.PowerPlantId = powerPlantId;
				model.Year = year ?? DateTime.Now.Year;
				model.QuarterNum = quarterNum ?? Math.Ceiling((decimal)DateTime.Now.Month / 3).ToByte();
			}
			model.CalculateLabels();

			model.PowerPlant = new PowerPlantPresentationStub(powerPlant);

			await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(MaintainPriceElectricFormStub model)
        {
            if (ModelState.IsValid)
			{
				Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync(model.PowerPlantId);

				Business.Entities.Workflow curWorkflow;
				Business.Entities.PowerPlant powerPlant;
				List<Business.Entities.Workflow> workflows;
				Business.Entities.PPI dbObject = new Business.Entities.PPI();
				
				Task<List<Business.Entities.Workflow>> workflowsTask = RepoWorkflow.FindByTypeAsync(WorkflowType.PRICE);
				Task.WaitAll(powerPlantTask, workflowsTask);
				powerPlant = powerPlantTask.Result;
				workflows = workflowsTask.Result;

				model.MapDbObject(dbObject, powerPlant.SOType == SOType.ZTS.ToString());

				if (powerPlant.SOType == SOType.ZPGE.ToString())
				{
					if (model.IsDraft) curWorkflow = workflows.First(x => x.PreviousStep == null);
					else curWorkflow = workflows.First(x => x.PreviousStep == model.WorkflowId);
				}
				else // kalau ZTS langsung approved
				{
					curWorkflow = workflows.First(x => x.Approver == null);
				}
				dbObject.WorkflowId = curWorkflow.Id;

				await RepoPPI.SaveAsync(dbObject);

				// masukin ke history approval
				PpiApprovalFormStub ppiApproval = new PpiApprovalFormStub()
				{
					PpiId = dbObject.Id,
					WorkflowId = dbObject.WorkflowId,
					Notes = ""
				};
				PpiApproval ppiApprovalDb = new PpiApproval();
				ppiApproval.MapDbObject(ppiApprovalDb);
				RepoPpiApproval.Save(ppiApprovalDb);

                //send email
                await PPIEmailSender(dbObject, true);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage($"PPI {powerPlant.Name} Triwulan " + dbObject.QuarterNum + " Tahun " + dbObject.Year, template);
				TempData.Add("powerPlantId", dbObject.PowerPlantId);

				return RedirectToAction(ActionSite.Index);
            }
            else
            {
				return await Create(model.PowerPlantId, model.Year, model.QuarterNum);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexMaintainHargaListrik, Key = KeySite.EditMaintainHargaListrik)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
			FilterQuery filter;

            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Task<Business.Entities.PPI> dbObjectTask = RepoPPI.FindByPrimaryKeyAsync(primaryKey);
			Task<List<Business.Entities.Workflow>> workflowsTask = RepoWorkflow.FindByTypeAsync(WorkflowType.PRICE);

			Task.WaitAll(dbObjectTask, workflowsTask);
			Business.Entities.PPI dbObject = dbObjectTask.Result;
			List<Business.Entities.Workflow> workflows = workflowsTask.Result;

			MaintainPriceElectricFormStub model = new MaintainPriceElectricFormStub(dbObject, workflows);
			
			filter = new FilterQuery(Field.PowerPlantId, FilterOperator.Equals, model.PowerPlantId);
			List<Business.Entities.SalesOrder> salesOrders = await RepoSalesOrder.FindAllAsync(filter);
			model.CalculateLabels();
			model.CalculateValidAction(salesOrders, model.Workflows);
			
			PpiApproval ppiApproval = await RepoPpiApproval.GetLastApproval(primaryKey);
			model.LastPpiApproval = ppiApproval;

			ViewBag.Breadcrumb = "Triwulan " + dbObject.QuarterNum + " Tahun " + dbObject.Year;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(MaintainPriceElectricFormStub model)
        {
            if (ModelState.IsValid)
            {
				Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync(model.PowerPlantId);

				Business.Entities.Workflow curWorkflow;
				List<Business.Entities.Workflow> workflows;
				Business.Entities.PPI dbObject = await RepoPPI.FindByPrimaryKeyAsync(model.Id);
				Task.WaitAll(powerPlantTask);
				Business.Entities.PowerPlant powerPlant = powerPlantTask.Result;

				workflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.PRICE);
				if (model.IsDraft) curWorkflow = workflows.First(x => x.PreviousStep == null);
				else curWorkflow = workflows.FirstOrDefault(x => x.PreviousStep == model.WorkflowId) ?? workflows.First(x => x.Id == model.WorkflowId);

				model.MapDbObject(dbObject, powerPlant.SOType == SOType.ZTS.ToString());

				dbObject.WorkflowId = curWorkflow.Id;

				await RepoPPI.SaveAsync(dbObject);

				// masukin ke history approval
				PpiApprovalFormStub ppiApproval = new PpiApprovalFormStub()
				{
					PpiId = dbObject.Id,
					WorkflowId = dbObject.WorkflowId,
					Notes = ""
				};
				PpiApproval ppiApprovalDb = new PpiApproval();
				ppiApproval.MapDbObject(ppiApprovalDb);
				RepoPpiApproval.Save(ppiApprovalDb);

                //send email
                await PPIEmailSender(dbObject, true);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage($"PPI {powerPlant.Name} Triwulan " + dbObject.QuarterNum + " Tahun " + dbObject.Year, template);
				TempData.Add("powerPlantId", dbObject.PowerPlantId);

				return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = "Triwulan " + model.QuarterNum + " Tahun " + model.Year;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.PPI dbObject = await RepoPPI.FindByPrimaryKeyAsync(primaryKey);

            if (!(await RepoPPI.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
		#endregion

		#region Approval

		[HttpPost]
		public async Task<ActionResult> Approve(params object[] id)
		{
			// kamus
			FilterQuery filter;
			string message;
			Business.Entities.Workflow workflow;

			// algo
			ResponseModel response = new ResponseModel(true);
			int primaryKey = int.Parse(id.FirstOrDefault().ToString());
			Business.Entities.PPI dbObject = await RepoPPI.FindByPrimaryKeyAsync(primaryKey);

			// cari workflow lanjutannya
			filter = new FilterQuery(Field.PreviousStep, FilterOperator.Equals, dbObject.WorkflowId);
			workflow = await RepoWorkflow.FindAsync(filter);
			dbObject.WorkflowId = workflow.Id;

			// baru update data sales order nya
			if (!(await RepoPPI.SaveAsync(dbObject)))
			{
				message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
				response.SetFail(message);
			}
			else
			{
				// masukin ke history approval
				PpiApprovalFormStub ppiApproval = new PpiApprovalFormStub()
				{
					PpiId = dbObject.Id,
					WorkflowId = dbObject.WorkflowId,
					Notes = ""
				};
				PpiApproval ppiApprovalDb = new PpiApproval();
				ppiApproval.MapDbObject(ppiApprovalDb);
				RepoPpiApproval.Save(ppiApprovalDb);

                //send email
                await PPIEmailSender(dbObject, true);
            }

			return Json(response);
		}

		[HttpPost]
		public async Task<ActionResult> Revise(int id, string message = "")
		{
			// kamus
			FilterQuery filter;
			//string message;
			Business.Entities.Workflow workflow;
			bool isSuccess = false;

			// algo
			ResponseModel response = new ResponseModel(true);
			int primaryKey = id; // int.Parse(id.FirstOrDefault().ToString());
			Business.Entities.PPI dbObject = await RepoPPI.FindByPrimaryKeyAsync(primaryKey);

			// cari workflow sebelumnya
			if (dbObject.Workflow != null)
			{
				if (dbObject.Workflow.PreviousStep != null)
				{
					filter = new FilterQuery(Field.Id, FilterOperator.Equals, dbObject.Workflow.PreviousStep);
					workflow = await RepoWorkflow.FindAsync(filter);
					dbObject.WorkflowId = workflow.Id;

					// update data sales order nya
					isSuccess = await RepoPPI.SaveAsync(dbObject);

					// masukin ke history approval
					PpiApprovalFormStub ppiApproval = new PpiApprovalFormStub()
					{
						PpiId = dbObject.Id,
						WorkflowId = dbObject.WorkflowId,
						Notes = message
					};
					PpiApproval ppiApprovalDb = new PpiApproval();
					ppiApproval.MapDbObject(ppiApprovalDb);
					RepoPpiApproval.Save(ppiApprovalDb);

                    //send email
                    await PPIEmailSender(dbObject, false, message);
                }
			}

			TempData.Add("powerPlantId", dbObject.PowerPlantId);

			//if (!isSuccess)
			//{
			//	message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
			//	response.SetFail(message);
			//}

			return RedirectToAction(ActionSite.Index);

			//return Json(response);
		}
		#endregion

		#region Binding
		public async Task<string> BindingPowerPlant()
		{
			//lib
			List<SelectListItem> options = new List<SelectListItem>();                      
            Area area;
            IEnumerable<Business.Entities.PowerPlant> powerPlants = await RepoPowerPlant.FindAllByMaterial(Material.ELECTRIC_POWER);

            if (User.AreaId.HasValue && User.AreaId > 0)
            {
                area = RepoArea.FindByPrimaryKey(User.AreaId.Value);
                if (area.PowerPlants.Any(n => n.SOType == SOType.ZTS.ToString()))
                    powerPlants = powerPlants.Where(n => n.SOType == SOType.ZTS.ToString());
                else
                    powerPlants = powerPlants.Where(n => n.AreaId == User.AreaId);
            }

            //algorithm
            foreach (Business.Entities.PowerPlant s in powerPlants)
			{
				SelectListItem o = new SelectListItem
				{
					Text = s.Name,
					Value = s.Id.ToString()
				};

				options.Add(o);
			}

			return JsonConvert.SerializeObject(options.ToList());
		}

		public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<Business.Entities.PPI> dbObjects;
            List<MaintainPriceElectricPresentationStub> models = new List<MaintainPriceElectricPresentationStub>();
            Task<List<Business.Entities.PPI>> dbObjectsTask = null;
            Task<int> countTask = null;
			FilterQuery filter;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoPPI.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoPPI.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

			//map db data to model
			//foreach (PPI dbObject in dbObjects)
			//{
			//	models.Add(new MaintainPriceElectricPresentationStub(dbObject));
			//}
			models = ListMapper.MapList<Business.Entities.PPI, MaintainPriceElectricPresentationStub>(dbObjects);
			//foreach (PPI dbObject in dbObjects)
			//{
			//	models.Add(new MaintainPriceElectricPresentationStub(dbObject));
			//}

			if (models.Any())
			{
				filter = new FilterQuery(Field.PowerPlantId, FilterOperator.Equals, models.First().PowerPlantId);
				List<Business.Entities.SalesOrder> salesOrders = await RepoSalesOrder.FindAllAsync(filter);

				List<Business.Entities.Workflow> workflowsDb = await RepoWorkflow.FindByTypeAsync(WorkflowType.PRICE);
				List<WorkflowPresentationStub> workflows = ListMapper.MapList<Business.Entities.Workflow, WorkflowPresentationStub>(workflowsDb);
				foreach (MaintainPriceElectricPresentationStub model in models)
				{
					model.CalculateValidAction(salesOrders, workflows);
					model.StrWorkflow = workflows.First(x => x.Id == model.WorkflowId).Name;
				}
			}
			
            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
		#endregion

		#region Download

		[HttpGet]
		public async Task<ActionResult> Download(int id)
		{
			// kamus
			FilterQuery filter;
			User user;

			byte[] excel = { 0 };

			Business.Entities.PPI ppi = await RepoPPI.FindByPrimaryKeyAsync(id);

			DateTime nextQuarterYear = DateHelper.QuarterYearToDateTime(ppi.QuarterNum, ppi.Year).AddMonths(3);
			string filename = $"Maintain Harga {ppi.PowerPlant.Name} TW {DateHelper.MonthToQuarter(nextQuarterYear.Month)} {nextQuarterYear.Year}.xlsx";

			string filePath = Path.Combine(Server.MapPath(DirectoryHelper.Resources), "MAINTAIN_HARGA.xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(System.IO.File.Open(filePath, FileMode.Open));

			if (ppi.FormSequence == null)
			{
				int lastFormSequence = await RepoPPI.GetLastFormSequence();
				lastFormSequence += await RepoSteamPowerPlantPrice.GetLastFormSequence();
				ppi.FormSequence = (lastFormSequence + 1).ToString("000");
				await RepoPPI.SaveAsync(ppi);
			}

			PpiApproval lastPpiApprovalRequester = ppi.PpiApprovals.LastOrDefault(x => x.Workflow.Code == Common.Enums.Workflow.MANAGER_OPS_OE_VERIFICATION.ToString());
			PpiApproval lastPpiApprovalLegal = ppi.PpiApprovals.LastOrDefault(x => x.Workflow.Code == Common.Enums.Workflow.FINISH.ToString());

			filter = new FilterQuery(Field.UserName, FilterOperator.Equals, lastPpiApprovalRequester.CreatedBy);
			user = await RepoUser.FindAsync(filter);
			filter = new FilterQuery(Field.UserId, FilterOperator.Equals, user.UserId);
			Membership requesterMembership = await RepoMembership.FindAsync(filter);

			filter = new FilterQuery(Field.UserName, FilterOperator.Equals, lastPpiApprovalLegal.CreatedBy);
			user = await RepoUser.FindAsync(filter);
			filter = new FilterQuery(Field.UserId, FilterOperator.Equals, user.UserId);
			Membership legalMembership = await RepoMembership.FindAsync(filter);
			
			MaintainPriceElectricExcelStub maintainPriceElectricExcelStub = new MaintainPriceElectricExcelStub(workbook, ppi, requesterMembership, legalMembership);

			excel = maintainPriceElectricExcelStub.GenerateTemplate();

			return File(excel, "application/x-msexcel", filename);
		}

        #endregion

        #region Helper

        public virtual async Task<bool> PPIEmailSender(PPI dbObject, bool isApprove, string message = null)
        {
            List<Membership> memberships = null;
            Business.Entities.PowerPlant powerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(dbObject.PowerPlantId);

            if (powerPlant.AreaId != null)
            {
                FilterQuery filterQuery = new FilterQuery();
                filterQuery.AddFilter(Field.AreaId, FilterOperator.Equals, powerPlant.AreaId.Value);
                memberships = await RepoMembership.FindAllAsync(filterQuery);
            }

            UserRole? userRole;

            switch (dbObject.WorkflowId)
            {
                case 14:
                    userRole = UserRole.ASMEN;
                    break;
                case 15:
                    userRole = UserRole.MANAGER_AREA;
                    break;
                case 17:
                    userRole = UserRole.OE_VERIFICATION;
                    break;
                case 19:
                    userRole = UserRole.MANAGER_OE;
                    break;
                default:
                    userRole = null;
                    break;
            }

            if (userRole != null)
            {
                if (memberships.Any())
                {
                    memberships = memberships.FindAll(n => n.User.Roles.Any(m => m.RoleName == userRole.Value.ToString()));
                    if (memberships.Any())
                    {
                        string subject = "Notifikasi Maintain Harga Listrik";
                        string body = "";

                        if (isApprove)
                        {
                            body = @"<p><strong>Yth. Bpk / Ibu</strong></p>
                            <p>Dengan ini kami sampaikan bahwa Maintain Harga Listrik berikut ini, menunggu persetujuan dari Bpk/Ibu.<br /><br /></p>
                            <p><strong>Maintain Harga Listrik :</strong></p>
                            <table style=""width: 347px;"">
                            <tbody>
                            <tr>
                            <td style=""width: 129px;"">Power Plant</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (powerPlant.Name != null ? powerPlant.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td colspan=""3"">Perhitungan</td>                            
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Tahun</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.Year + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Triwulan ke</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.QuarterNum + @"</td>
                            </tr>
                            <tr>
                            <td colspan=""3"">Untuk Digunakan pada periode</td>                            
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Tahun</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.Year + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Triwulan ke</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.QuarterNum + @"</td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <p>Untuk verifikasi silahkan klik link di bawah ini</p>
                            <p><a title=""Maintain Listrik"" href=""" + (AppAddress + "DataMaster/MaintainPriceElectric/Edit?id=" + dbObject.Id) + @""">Maintain Listrik</a></p>
                            <p>&nbsp;</p>
                            <p>Atas perhatiannya kami ucapkan terima kasih.</p>";
                        }
                        else
                        {
                            body = @"<p><strong>Yth. Bpk / Ibu</strong></p>
                            <p>Dengan ini kami sampaikan bahwa Maintain Listrik berikut ini, menunggu revisi dari Bpk/Ibu.<br /><br /></p>
                            <p><strong>Maintain Listrik :</strong></p>
                            <table style=""width: 347px;"">
                            <tbody>
                            <tr>
                            <td style=""width: 129px;"">Power Plant</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (powerPlant.Name != null ? powerPlant.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Tahun</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.Year + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Triwulan ke</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.QuarterNum + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Notes</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (message != null ? message : "") + @"</td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <p>Untuk verifikasi silahkan klik link di bawah ini</p>
                            <p><a title=""Maintain Listrik"" href=""" + (AppAddress + "DataMaster/MaintainPriceElectric/Edit?id=" + dbObject.Id) + @""">Maintain Listrik</a></p>
                            <p>&nbsp;</p>
                            <p>Atas perhatiannya kami ucapkan terima kasih.</p>";
                        }

                        foreach (Membership m in memberships)
                        {

                            string email = m.Email;
                            bool isSuccess = EmailHelper.SendEmail(m.Email, subject, body);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
