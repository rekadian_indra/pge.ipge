﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Web;
using System.Linq;
using WebUI.Extension;
using Resources;
using WebUI.Areas.DataMaster.Models;
using MvcSiteMapProvider.Web.Mvc.Filters;
using NPOI.XSSF.UserModel;
using System.IO;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.DataMaster.Controllers
{
    [CheckSessionTimeOut]
    [Authorize]
    public class MaintainPriceSteamController : BaseController<MaintainPriceSteamFormStub>
    {
        public MaintainPriceSteamController
		(
            ILogRepository repoLog,
			ISteamPowerPlantPriceRepository repoSteamPowerPlantPrice,
			IPpiRepository repoPPI,
			IPowerPlantRepository repoPowerPlant,
			IWorkflowRepository repoWorkflow,
			ISalesOrderRepository repoSalesOrder,
			ISteamPowerPlantPriceApprovalRepository repoSteamPowerPlantPriceApproval,
            IMembershipRepository repoMembership,
			IUserRepository repoUser,
			IAreaRepository repoArea
		)
        {
            RepoLog = repoLog;
            RepoSteamPowerPlantPrice = repoSteamPowerPlantPrice;
			RepoPPI = repoPPI;
			RepoPowerPlant = repoPowerPlant;
			RepoWorkflow = repoWorkflow;
			RepoSalesOrder = repoSalesOrder;
			RepoSteamPowerPlantPriceApproval = repoSteamPowerPlantPriceApproval;
            RepoMembership = repoMembership;
			RepoUser = repoUser;
			RepoArea = repoArea;
		}

		#region View CRUD
		[MvcSiteMapNode(Title = TitleSite.MaintainHargaUap, Area = AreaSite.DataMaster, ParentKey = KeySite.Dashboard, Key = KeySite.IndexMaintainHargaUap)]
		public override async Task<ActionResult> Index()
		{
			ViewBag.powerPlantId = TempData["powerPlantId"] ?? 0;
			return await base.Index();
		}

		public ActionResult BackToIndex(int powerPlantId)
		{
			TempData.Add("powerPlantId", powerPlantId);

			return RedirectToAction(ActionSite.Index);
		}

		public ActionResult Check(int powerPlantId, DateTime startDate, DateTime endDate)
		{
			List<SteamPowerPlantPrice> steamPowerPlantPrices = RepoSteamPowerPlantPrice.GetFromPowerPlantId(powerPlantId, startDate, endDate);

			if (steamPowerPlantPrices.Any())
			{
                SteamPowerPlantPrice lastSteamPowerPlantPrice = steamPowerPlantPrices.First();
                return RedirectToAction(ActionSite.Edit, new
                {
                    id = lastSteamPowerPlantPrice.Id
                });
                
            }
            else
            {
                return RedirectToAction(ActionSite.Create, new
                {
                       powerPlantId = powerPlantId,
                       startDate = startDate,
                       endDate =endDate
                });
            }
			
		}

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexMaintainHargaUap, Key = KeySite.CreateMaintainHargaUap)]
        [RequiredParameter(QueryStringValue.PowerPlant, IncludePost = false)]
        public async Task<ActionResult> Create(int powerPlantId, DateTime? startDate, DateTime? endDate)
        {
			List<Business.Entities.Workflow> workflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.PRICE);
			Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync(powerPlantId);
			Task.WaitAll(powerPlantTask);
			Business.Entities.PowerPlant powerPlant = powerPlantTask.Result;

			MaintainPriceSteamFormStub model = new MaintainPriceSteamFormStub(workflows);
			if (powerPlant.SOType == SOType.ZTS.ToString()) // kalau ZTS, lgsg 1 step sebelum beres
			{
				int beforeLastWorkflowId = workflows.First(x => x.Approver == null)?.PreviousStep ?? 0;
				model.SetWorkflowId(beforeLastWorkflowId);
				model.IsRevisable = false;
			}

			//List<SteamPowerPlantPrice> steamPowerPlantPrices = RepoSteamPowerPlantPrice.GetFromPowerPlantId(powerPlantId);
			//if (steamPowerPlantPrices.Any())
			//{
			//	if (year == null) // kalau blm masukin year nya, defaultnya yg next nya aja
			//	{
			//		SteamPowerPlantPrice lastSteamPowerPlantPrice = steamPowerPlantPrices.OrderBy(x => x.Year).Last();
			//		model.NextSteamPowerPlantPrice(lastSteamPowerPlantPrice);
			//	}
			//}
			//else
			//{
			//	model.PowerPlantId = powerPlantId;
			//	model.Year = year ?? DateTime.Now.Year;
			//}

			model.PowerPlant = new PowerPlantPresentationStub(powerPlant);

            //SteamPowerPlantPrice prevPrice = RepoSteamPowerPlantPrice.GetPrevPrice(powerPlant.Id, new DateTime(model.Year, 1, 1));

            //model.PrevPrice = prevPrice?.Price ?? powerPlant.BasePrice;
            //model.PrevPriceAdd = prevPrice?.PriceAdd ?? powerPlant.BasePrice;
            model.PowerPlantId = powerPlantId;
            if (startDate != null)
            {
                model.EndDate = endDate.ToDateTime();
            }
            if (endDate != null)
            {
                model.StartDate = startDate.ToDateTime();
            }           
           
            model.PrevPrice = powerPlant.BasePrice;
            model.PrevPriceAdd = powerPlant.BasePrice;

            await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(MaintainPriceSteamFormStub model)
		{
			Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync(model.PowerPlantId);
			if (ModelState.IsValid)
			{
				Business.Entities.Workflow curWorkflow;
				Business.Entities.PowerPlant powerPlant;
				List<Business.Entities.Workflow> workflows;
				Business.Entities.SteamPowerPlantPrice dbObject = new Business.Entities.SteamPowerPlantPrice();

				Task<List<Business.Entities.Workflow>> workflowsTask = RepoWorkflow.FindByTypeAsync(WorkflowType.PRICE);
				Task.WaitAll(powerPlantTask, workflowsTask);
				powerPlant = powerPlantTask.Result;
				workflows = workflowsTask.Result;

				model.MapDbObject(dbObject, powerPlant.SOType == SOType.ZTS.ToString());

				if (powerPlant.SOType == SOType.ZPGE.ToString())
				{
					if (model.IsDraft) curWorkflow = workflows.First(x => x.PreviousStep == null);
					else curWorkflow = workflows.First(x => x.PreviousStep == model.WorkflowId);
				}
				else // kalau ZTS langsung approved
				{
					curWorkflow = workflows.First(x => x.Approver == null);
				}

				dbObject.WorkflowId = curWorkflow.Id;

				await RepoSteamPowerPlantPrice.SaveAsync(dbObject);

				// masukin ke history approval
				SteamPowerPlantPriceApprovalFormStub steamPowerPlantPriceApproval = new SteamPowerPlantPriceApprovalFormStub()
				{
					SteamPowerPlantPriceId = dbObject.Id,
					WorkflowId = dbObject.WorkflowId,
					Notes = ""
				};
				SteamPowerPlantPriceApproval steamPowerPlantPriceApprovalDb = new SteamPowerPlantPriceApproval();
				steamPowerPlantPriceApproval.MapDbObject(steamPowerPlantPriceApprovalDb);
				RepoSteamPowerPlantPriceApproval.Save(steamPowerPlantPriceApprovalDb);

                await MPSEmailSender(dbObject, true);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage($"Harga untuk {powerPlant.Name} Periode " + dbObject.StartDate + " s.d " + dbObject.EndDate, template);
				TempData.Add("powerPlantId", dbObject.PowerPlantId);

				return RedirectToAction(ActionSite.Index);
            }
            else
            {
				return await Create(model.PowerPlantId, model.StartDate, model.EndDate);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexMaintainHargaUap, Key = KeySite.EditMaintainHargaUap)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
			FilterQuery filter;
			
			int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Task<Business.Entities.SteamPowerPlantPrice> dbObjectTask = RepoSteamPowerPlantPrice.FindByPrimaryKeyAsync(primaryKey);
			Task<List<Business.Entities.Workflow>> workflowsTask = RepoWorkflow.FindByTypeAsync(WorkflowType.PRICE);

			Task.WaitAll(dbObjectTask, workflowsTask);
			Business.Entities.SteamPowerPlantPrice dbObject = dbObjectTask.Result;
			List<Business.Entities.Workflow> workflows = workflowsTask.Result;

			MaintainPriceSteamFormStub model = new MaintainPriceSteamFormStub(dbObject, workflows);

			Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync(model.PowerPlantId);

			filter = new FilterQuery(Field.PowerPlantId, FilterOperator.Equals, model.PowerPlantId);
			List<Business.Entities.SalesOrder> salesOrders = await RepoSalesOrder.FindAllAsync(filter);
			model.CalculateValidAction(salesOrders, model.Workflows);
			
			Business.Entities.PowerPlant powerPlant = powerPlantTask.Result;
			model.PowerPlant = new PowerPlantPresentationStub(powerPlant);

			//SteamPowerPlantPrice prevPrice = RepoSteamPowerPlantPrice.GetPrevPrice(powerPlant.Id, new DateTime(model.Year, 1, 1));

			model.PrevPrice = powerPlant.BasePrice;
			model.PrevPriceAdd =  powerPlant.BasePrice;

			SteamPowerPlantPriceApproval steamPowerPlantPriceApproval = await RepoSteamPowerPlantPriceApproval.GetLastApproval(primaryKey);
			model.LastSteamPowerPlantPriceApproval = steamPowerPlantPriceApproval;

			ViewBag.Breadcrumb = "Periode " + dbObject.StartDate + " s.d " + dbObject.EndDate;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(MaintainPriceSteamFormStub model)
        {
            if (ModelState.IsValid)
			{
				Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync(model.PowerPlantId);

				Business.Entities.Workflow curWorkflow;
				List<Business.Entities.Workflow> workflows;
				Business.Entities.SteamPowerPlantPrice dbObject = await RepoSteamPowerPlantPrice.FindByPrimaryKeyAsync(model.Id);
				Task.WaitAll(powerPlantTask);
				Business.Entities.PowerPlant powerPlant = powerPlantTask.Result;

				model.MapDbObject(dbObject, powerPlant.SOType == SOType.ZTS.ToString());

				workflows = await RepoWorkflow.FindByTypeAsync(WorkflowType.PRICE);
				if (model.IsDraft) curWorkflow = workflows.First(x => x.PreviousStep == null);
				else curWorkflow = workflows.FirstOrDefault(x => x.PreviousStep == model.WorkflowId) ?? workflows.First(x => x.Id == model.WorkflowId);

				dbObject.WorkflowId = curWorkflow.Id;

				await RepoSteamPowerPlantPrice.SaveAsync(dbObject);

				// masukin ke history approval
				SteamPowerPlantPriceApprovalFormStub steamPowerPlantPriceApproval = new SteamPowerPlantPriceApprovalFormStub()
				{
					SteamPowerPlantPriceId = dbObject.Id,
					WorkflowId = dbObject.WorkflowId,
					Notes = ""
				};
				SteamPowerPlantPriceApproval steamPowerPlantPriceApprovalDb = new SteamPowerPlantPriceApproval();
				steamPowerPlantPriceApproval.MapDbObject(steamPowerPlantPriceApprovalDb);
				RepoSteamPowerPlantPriceApproval.Save(steamPowerPlantPriceApprovalDb);

                await MPSEmailSender(dbObject, true);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage($"{powerPlant.Name} Periode " + dbObject.StartDate + " s.d " + dbObject.EndDate, template);
				TempData.Add("powerPlantId", dbObject.PowerPlantId);

				return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = "Periode " + model.StartDate + " s.d " + model.EndDate;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.SteamPowerPlantPrice dbObject = await RepoSteamPowerPlantPrice.FindByPrimaryKeyAsync(primaryKey);

            if (!(await RepoSteamPowerPlantPrice.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
		#endregion

		#region Approval

		[HttpPost]
		public async Task<ActionResult> Approve(params object[] id)
		{
			// kamus
			FilterQuery filter;
			string message;
			Business.Entities.Workflow workflow;

			// algo
			ResponseModel response = new ResponseModel(true);
			int primaryKey = int.Parse(id.FirstOrDefault().ToString());
			Business.Entities.SteamPowerPlantPrice dbObject = await RepoSteamPowerPlantPrice.FindByPrimaryKeyAsync(primaryKey);

			// cari workflow lanjutannya
			filter = new FilterQuery(Field.PreviousStep, FilterOperator.Equals, dbObject.WorkflowId);
			workflow = await RepoWorkflow.FindAsync(filter);
			dbObject.WorkflowId = workflow.Id;

			// baru update data sales order nya
			if (!(await RepoSteamPowerPlantPrice.SaveAsync(dbObject)))
			{
				message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
				response.SetFail(message);
			}
			else
			{
				// masukin ke history approval
				SteamPowerPlantPriceApprovalFormStub steamPowerPlantPriceApproval = new SteamPowerPlantPriceApprovalFormStub()
				{
					SteamPowerPlantPriceId = dbObject.Id,
					WorkflowId = dbObject.WorkflowId,
					Notes = ""
				};
				SteamPowerPlantPriceApproval steamPowerPlantPriceApprovalDb = new SteamPowerPlantPriceApproval();
				steamPowerPlantPriceApproval.MapDbObject(steamPowerPlantPriceApprovalDb);
				RepoSteamPowerPlantPriceApproval.Save(steamPowerPlantPriceApprovalDb);

                await MPSEmailSender(dbObject, true);
            }

			return Json(response);
		}

		[HttpPost]
		public async Task<ActionResult> Revise(int id, string message = "")
		{
			// kamus
			FilterQuery filter;
			Business.Entities.Workflow workflow;
			bool isSuccess = false;

			// algo
			ResponseModel response = new ResponseModel(true);
			int primaryKey = id; // int.Parse(id.FirstOrDefault().ToString());
			Business.Entities.SteamPowerPlantPrice dbObject = await RepoSteamPowerPlantPrice.FindByPrimaryKeyAsync(primaryKey);

			// cari workflow sebelumnya
			if (dbObject.Workflow != null)
			{
				if (dbObject.Workflow.PreviousStep != null)
				{
					filter = new FilterQuery(Field.Id, FilterOperator.Equals, dbObject.Workflow.PreviousStep);
					workflow = await RepoWorkflow.FindAsync(filter);
					dbObject.WorkflowId = workflow.Id;

					// update data sales order nya
					isSuccess = await RepoSteamPowerPlantPrice.SaveAsync(dbObject);
					
					// masukin ke history approval
					SteamPowerPlantPriceApprovalFormStub steamPowerPlantPriceApproval = new SteamPowerPlantPriceApprovalFormStub()
					{
						SteamPowerPlantPriceId = dbObject.Id,
						WorkflowId = dbObject.WorkflowId,
						Notes = message
					};
					SteamPowerPlantPriceApproval steamPowerPlantPriceApprovalDb = new SteamPowerPlantPriceApproval();
					steamPowerPlantPriceApproval.MapDbObject(steamPowerPlantPriceApprovalDb);
					RepoSteamPowerPlantPriceApproval.Save(steamPowerPlantPriceApprovalDb);

                    await MPSEmailSender(dbObject, false, message);
                }
			}

			TempData.Add("powerPlantId", dbObject.PowerPlantId);

			//if (!isSuccess)
			//{
			//	message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
			//	response.SetFail(message);
			//}

			return RedirectToAction(ActionSite.Index);

			// return Json(response);
		}
		#endregion

		#region Binding
		public async Task<string> BindingPowerPlant()
		{
			//lib
			List<SelectListItem> options = new List<SelectListItem>();
			IEnumerable<Business.Entities.PowerPlant> powerPlants = await RepoPowerPlant.FindAllByMaterial(Material.GEOTHERMAL_STEAM);
            Area area;            

            if (User.AreaId.HasValue && User.AreaId > 0)
            {
                area = RepoArea.FindByPrimaryKey(User.AreaId.Value);
                if (area.PowerPlants.Any(n => n.SOType == SOType.ZTS.ToString()))
                    powerPlants = powerPlants.Where(n => n.SOType == SOType.ZTS.ToString());
                else
                    powerPlants = powerPlants.Where(n => n.AreaId == User.AreaId);
            }

            //algorithm
            foreach (Business.Entities.PowerPlant s in powerPlants)
			{
				SelectListItem o = new SelectListItem
				{
					Text = s.Name,
					Value = s.Id.ToString()
				};

				options.Add(o);
			}

			return JsonConvert.SerializeObject(options.ToList());
		}

		public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<Business.Entities.SteamPowerPlantPrice> dbObjects;
            List<MaintainPriceSteamPresentationStub> models = new List<MaintainPriceSteamPresentationStub>();
            Task<List<Business.Entities.SteamPowerPlantPrice>> dbObjectsTask = null;
            Task<int> countTask = null;
			FilterQuery filter;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoSteamPowerPlantPrice.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoSteamPowerPlantPrice.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
			models = ListMapper.MapList<Business.Entities.SteamPowerPlantPrice, MaintainPriceSteamPresentationStub>(dbObjects);
			//foreach (SteamPowerPlantPrice dbObject in dbObjects)
			//{
			//	models.Add(new MaintainPriceSteamPresentationStub(dbObject));
			//}
			
			if (models.Any())
			{
				filter = new FilterQuery(Field.PowerPlantId, FilterOperator.Equals, models.First().PowerPlantId);
				List<Business.Entities.SalesOrder> salesOrders = await RepoSalesOrder.FindAllAsync(filter);

				List<Business.Entities.Workflow> workflowsDb = await RepoWorkflow.FindByTypeAsync(WorkflowType.PRICE);
				List<WorkflowPresentationStub> workflows = ListMapper.MapList<Business.Entities.Workflow, WorkflowPresentationStub>(workflowsDb);
				foreach (MaintainPriceSteamPresentationStub model in models)
				{
					model.CalculateValidAction(salesOrders, workflows);
					model.StrWorkflow = workflows.First(x => x.Id == model.WorkflowId).Name;
				}
			}


            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
		#endregion

		#region Download

		[HttpGet]
		public async Task<ActionResult> Download(int id)
		{
			// kamus
			FilterQuery filter;
			User user;

			byte[] excel = { 0 };

			Business.Entities.SteamPowerPlantPrice steamPowerPlantPrice = await RepoSteamPowerPlantPrice.FindByPrimaryKeyAsync(id);
			
			string filename = $"Maintain Harga {steamPowerPlantPrice.PowerPlant.Name} Tahun {steamPowerPlantPrice.Year}.xlsx";

			string filePath = Path.Combine(Server.MapPath(DirectoryHelper.Resources), "MAINTAIN_HARGA.xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(System.IO.File.Open(filePath, FileMode.Open));

			if (steamPowerPlantPrice.FormSequence == null)
			{
				int lastFormSequence = await RepoSteamPowerPlantPrice.GetLastFormSequence();
				lastFormSequence += await RepoPPI.GetLastFormSequence();
				steamPowerPlantPrice.FormSequence = (lastFormSequence + 1).ToString("000");
				await RepoSteamPowerPlantPrice.SaveAsync(steamPowerPlantPrice);
			}

			SteamPowerPlantPriceApproval lastSteamPowerPlantPriceApprovalRequester = steamPowerPlantPrice.SteamPowerPlantPriceApprovals.LastOrDefault(x => x.Workflow.Code == Common.Enums.Workflow.MANAGER_OPS_OE_VERIFICATION.ToString());
			SteamPowerPlantPriceApproval lastSteamPowerPlantPriceApprovalLegal = steamPowerPlantPrice.SteamPowerPlantPriceApprovals.LastOrDefault(x => x.Workflow.Code == Common.Enums.Workflow.FINISH.ToString());

			filter = new FilterQuery(Field.UserName, FilterOperator.Equals, lastSteamPowerPlantPriceApprovalRequester.CreatedBy);
			user = await RepoUser.FindAsync(filter);
			filter = new FilterQuery(Field.UserId, FilterOperator.Equals, user.UserId);
			Membership requesterMembership = await RepoMembership.FindAsync(filter);

			filter = new FilterQuery(Field.UserName, FilterOperator.Equals, lastSteamPowerPlantPriceApprovalLegal.CreatedBy);
			user = await RepoUser.FindAsync(filter);
			filter = new FilterQuery(Field.UserId, FilterOperator.Equals, user.UserId);
			Membership legalMembership = await RepoMembership.FindAsync(filter);

			MaintainPriceSteamExcelStub maintainPriceSteamExcelStub = new MaintainPriceSteamExcelStub(workbook, steamPowerPlantPrice, requesterMembership, legalMembership);

			excel = maintainPriceSteamExcelStub.GenerateTemplate();

			return File(excel, "application/x-msexcel", filename);
		}

        #endregion

        #region Helper
        public virtual async Task<bool> MPSEmailSender(SteamPowerPlantPrice dbObject, bool isApprove, string message = null)
        {
            List<Membership> memberships = null;
            Business.Entities.PowerPlant powerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(dbObject.PowerPlantId);

            if (powerPlant.AreaId != null)
            {
                FilterQuery filterQuery = new FilterQuery();
                filterQuery.AddFilter(Field.AreaId, FilterOperator.Equals, powerPlant.AreaId.Value);
                memberships = await RepoMembership.FindAllAsync(filterQuery);
            }

            UserRole? userRole;

            switch (dbObject.WorkflowId)
            {
                case 14:
                    userRole = UserRole.ASMEN;
                    break;
                case 15:
                    userRole = UserRole.MANAGER_AREA;
                    break;
                case 17:
                    userRole = UserRole.OE_VERIFICATION;
                    break;
                case 19:
                    userRole = UserRole.MANAGER_OE;
                    break;
                default:
                    userRole = null;
                    break;
            }

            if (userRole != null)
            {
                if (memberships.Any())
                {
                    memberships = memberships.FindAll(n => n.User.Roles.Any(m => m.RoleName == userRole.Value.ToString()));
                    if (memberships.Any())
                    {
                        string subject = "Notifikasi Maintain Uap";
                        string body = "";

                        if (isApprove)
                        {
                            body = @"<p><strong>Yth. Bpk / Ibu</strong></p>
                            <p>Dengan ini kami sampaikan bahwa Maintain Uap berikut ini, menunggu persetujuan dari Bpk/Ibu.<br /><br /></p>
                            <p><strong>Maintain Uap :</strong></p>
                            <table style=""width: 347px;"">
                            <tbody>
                            <tr>
                            <td style=""width: 129px;"">Power Plant</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (powerPlant.Name != null ? powerPlant.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Tahun</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.Year + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Persentase Kenaikan</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.IncreasePercentage + @"%</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Harga</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.Price + @"</td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <p>Untuk verifikasi silahkan klik link di bawah ini</p>
                            <p><a title=""Maintain Uap"" href=""" + (AppAddress + "DataMaster/MaintainPriceSteam/Edit?id=" + dbObject.Id) + @""">Maintain Uap</a></p>
                            <p>&nbsp;</p>
                            <p>Atas perhatiannya kami ucapkan terima kasih.</p>";
                        }
                        else
                        {
                            body = @"<p><strong>Yth. Bpk / Ibu</strong></p>
                            <p>Dengan ini kami sampaikan bahwa Maintain Uap berikut ini, menunggu revisi dari Bpk/Ibu.<br /><br /></p>
                            <p><strong>Maintain Uap :</strong></p>
                            <table style=""width: 347px;"">
                            <tbody>
                            <tr>
                            <td style=""width: 129px;"">Power Plant</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (powerPlant.Name != null ? powerPlant.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Tahun</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.Year + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Persentase Kenaikan</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.IncreasePercentage + @"%</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Harga</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + dbObject.Price + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Notes</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (message != null ? message : "") + @"</td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <p>Untuk verifikasi silahkan klik link di bawah ini</p>
                            <p><a title=""Maintain Uap"" href=""" + (AppAddress + "DataMaster/MaintainPriceSteam/Edit?id=" + dbObject.Id) + @""">Maintain Uap</a></p>
                            <p>&nbsp;</p>
                            <p>Atas perhatiannya kami ucapkan terima kasih.</p>";
                        }

                        foreach (Membership m in memberships)
                        {

                            string email = m.Email;
                            bool isSuccess = EmailHelper.SendEmail(m.Email, subject, body);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
