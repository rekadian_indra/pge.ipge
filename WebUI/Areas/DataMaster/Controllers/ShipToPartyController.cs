﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Web;
using System.Linq;
using WebUI.Extension;
using Resources;
using WebUI.Areas.DataMaster.Models.ShipToParties;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.DataMaster.Controllers
{
    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin })]
    [CheckSessionTimeOut]
    public class ShipToPartyController : BaseController<ShipToPartyFormStub>
    {
        public ShipToPartyController
        (
            ILogRepository repoLog,
            IPowerPlantRepository repoPowerPlant,
            ISoldToPartyRepository repoSoldToParty,
            IAreaRepository repoArea
        )
        {
            RepoLog = repoLog;
            RepoPowerPlant = repoPowerPlant;
            RepoSoldToParty = repoSoldToParty;
            RepoArea = repoArea;
        }

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.ShipToParty, Area = AreaSite.DataMaster, ParentKey = KeySite.Dashboard, Key = KeySite.IndexShipToParty)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexShipToParty, Key = KeySite.CreateShipToParty)]
        [RequiredParameter(QueryStringValue.Area, IncludePost = false, Mode = MatchMode.Any)]
        public async Task<ActionResult> Create(int? areaId)
        {
            ShipToPartyFormStub model = new ShipToPartyFormStub()
            {
                AreaId = areaId,
                URC = 0,
                AgreementDate = DateTime.Now.Date
            };

            await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(ShipToPartyFormStub model)
        {
            if (ModelState.IsValid)
            {
                Business.Entities.PowerPlant dbObject = new Business.Entities.PowerPlant();

                model.MapDbObject(dbObject);
                await RepoPowerPlant.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Code, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexShipToParty, Key = KeySite.EditShipToParty)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.PowerPlant dbObject = await RepoPowerPlant.FindByPrimaryKeyAsync(primaryKey);
            ShipToPartyFormStub model = new ShipToPartyFormStub(dbObject);

            ViewBag.Breadcrumb = model.Code;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(ShipToPartyFormStub model)
        {
            if (ModelState.IsValid)
            {
                Business.Entities.PowerPlant dbObject = await RepoPowerPlant.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoPowerPlant.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Name, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.Code;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.PowerPlant dbObject = await RepoPowerPlant.FindByPrimaryKeyAsync(primaryKey);

            if (!(await RepoPowerPlant.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<Business.Entities.PowerPlant> dbObjects;
            List<ShipToPartyPresentationStub> models;
            Task<List<Business.Entities.PowerPlant>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;
            FilterQuery filter = new FilterQuery(FilterLogic.And);
            IEnumerable<Business.Entities.PowerPlant> plants;
            IEnumerable<int> areaZts;
            Area area;

            //algorithm
            if (param.Filter == null)
                param.InstanceFilter();

            if (User.AreaId.HasValue && User.AreaId > 0)
            {
                area = RepoArea.FindByPrimaryKey(User.AreaId.Value);
                if (area.PowerPlants.Any(n => n.SOType == SOType.ZTS.ToString()))
                {
                    filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZTS);
                    plants = RepoPowerPlant.FindAll(filter);

                    filter = new FilterQuery(FilterLogic.Or);
                    if (plants.Any())
                    {
                        areaZts = plants.Select(n => n.AreaId.Value).Distinct();
                        foreach(int id in areaZts)
                        {
                            filter.AddFilter(Field.AreaId, FilterOperator.Equals, id);
                        }
                        param.Filter.AddFilter(filter);
                    }
                }
                else
                    param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, User.AreaId);
            }

            if (param.Filter != null)
            {
                if (param.Filter.Filters.Any())
                {

                    if (param.Filter.Filters.Exists(f => f.Field == Field.AreaName))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.AreaName);
                        param.Filter.AddFilter(new FilterQuery(new FieldHelper(Table.Area, Field.Name), FilterOperator.Contains, filterFmn.Value));
                        param.Filter.Filters.Remove(filterFmn);
                    }
                }
            }

            if (param.Sorts != null)
            {
                if (param.Sorts.Exists(s => s.Field == Field.AreaName))
                {
                    SortQuery sortFmn = param.Sorts.FirstOrDefault(s => s.Field == Field.AreaName);
                    param.Sorts.Add(new SortQuery(new FieldHelper(Table.Area, Field.Name), sortFmn.Direction));
                    param.Sorts.Remove(sortFmn);
                }
            }

            dbObjectsTask = RepoPowerPlant.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoPowerPlant.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Business.Entities.PowerPlant, ShipToPartyPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion

        #region "Helper"
        public async Task<JsonResult> CheckSOType(int powerPlantId)
		{
			Business.Entities.PowerPlant powerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(powerPlantId);

			return Json(new { Success = true, Result = powerPlant.SOType.ToString() });
		}

		public async Task<JsonResult> PowerPlantCodeExists(string Code, string CurrentCode)
        {
            //lib

            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(Code))
            {
                exist = true;
            }
            else if (Code.ToLower() == CurrentCode.ToLower())
            {
                exist = false;
            }
            else
            {
                Business.Entities.PowerPlant dbObject;
                FilterQuery filter = new FilterQuery(
                    Field.Code,
                    FilterOperator.Equals,
                    Code
                );

                dbObject = await RepoPowerPlant.FindAsync(filter);

                exist = dbObject != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> PowerPlantNameExists(string Name, string CurrentName)
        {
            //lib

            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(Name))
            {
                exist = true;
            }
            else if (Name.ToLower() == CurrentName.ToLower())
            {
                exist = false;
            }
            else
            {
                Business.Entities.PowerPlant dbObject;
                FilterQuery filter = new FilterQuery(
                    Field.Name,
                    FilterOperator.Equals,
                    Name
                );

                dbObject = await RepoPowerPlant.FindAsync(filter);

                exist = dbObject != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
