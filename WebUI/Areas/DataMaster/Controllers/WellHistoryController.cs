﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.DataMaster.Models.Wells;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Infrastructure.Attribute;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Controllers
{
    [CheckSessionTimeOut]
    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.OPERATION_SUPERVISOR, UserRole.OPERATION_OPERATOR })]
    public class WellHistoryController : BaseController<WellHistoryFormStub>
    {
        public WellHistoryController
        (
            IWellRepository repoWell,
            IWellHistoryRepository repoWellHistory,
            IViewWellRepository repoVIewWell
        )
        {
            RepoWell = repoWell;
            RepoWellHistory = repoWellHistory;
            RepoViewWell = repoVIewWell;
        }

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.WellHistory, Area = AreaSite.DataMaster, ParentKey = KeySite.IndexWell, Key = KeySite.IndexWellHistory)]
        public ActionResult History(int wellId, int? historyId)
        {
            Well well = RepoWell.FindByPrimaryKey(wellId);
            WellHistoryFormStub model = new WellHistoryFormStub();
            if (historyId.HasValue)
            {
                WellHistory history = RepoWellHistory.FindByPrimaryKey(historyId.Value);
                model = new WellHistoryFormStub(history);
            }
            else
            {
                model = new WellHistoryFormStub(well);
            }
          

            return View("History", model);
        }


        [HttpPost]
        public async Task<ActionResult> History(WellHistoryFormStub model)
        {
            if (ModelState.IsValid)
            {
                WellHistory dbObject = new WellHistory();
                WellHistory oldData = new WellHistory();
                IEnumerable<WellHistory> checkData;
                FilterQuery filter = new FilterQuery(FilterLogic.And);
                string template;

                filter.AddFilter(Field.WellId, FilterOperator.Equals, model.WellId);
                //filter.AddFilter(Field.Status, FilterOperator.Equals, model.Status);

                checkData = RepoWellHistory.FindAll(filter);
                checkData = checkData.Where(n => n.HistoryDateTimeUtc == model.HistoryDateTimeUtc.ToUtcDateTime());

               

                if (model.Id == 0)
                {
                    if (checkData != null && checkData.Any())
                    {
                        template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.Unique).ToString();
                        this.SetMessage(model.HistoryDateTimeUtc.ToString("d MMM yyyy (HH:mm)"), template);
                    }
                    else
                    {
                        model.MapDbObject(dbObject);
                        await RepoWellHistory.SaveAsync(dbObject);

                        template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                        this.SetMessage(model.WellName, template);
                    }
                }
                else
                {
                    oldData = RepoWellHistory.FindByPrimaryKey(model.Id);
                    DateTime oldTime = oldData.HistoryDateTimeUtc.ToLocalDateTime();
                    if (model.HistoryDateTimeUtc == oldTime)
                    {
                        if (model.Status != oldData.Status)
                        {
                            model.MapDbObject(oldData);
                            await RepoWellHistory.SaveAsync(oldData);
                        }

                        template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                        this.SetMessage(model.WellName, template);
                    }
                    else
                    {
                        //await RepoWellHistory.SaveAsync(oldData);
                        if (checkData != null && checkData.Any())
                        {
                            template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.Unique).ToString();
                            this.SetMessage(model.HistoryDateTimeUtc.ToString("d MMM yyyy (HH:mm)"), template);
                        }
                        else
                        {                            
                            model.MapDbObject(oldData);
                            await RepoWellHistory.SaveAsync(oldData);

                            template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                            this.SetMessage(model.WellName, template);
                        }
                    }
                }

                return RedirectToAction(ActionSite.History, new { wellId = model.WellId });
            }
            else
            {
                return View("History", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexWellHistory, Key = KeySite.EditWellHistory)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            WellHistory dbObject = await RepoWellHistory.FindByPrimaryKeyAsync(primaryKey);
            WellHistoryFormStub model = new WellHistoryFormStub(dbObject);

            ViewBag.Breadcrumb = model.WellName;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(WellHistoryFormStub model)
        {
            if (ModelState.IsValid)
            {
                WellHistory dbObject = await RepoWellHistory.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoWellHistory.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.WellName, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.WellName;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            WellHistory dbObject = await RepoWellHistory.FindByPrimaryKeyAsync(primaryKey);

            if (!(await RepoWellHistory.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<ViewWell> dbObjects;
            List<WellHistoryPresentationStub> models;
            Task<List<ViewWell>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (param.Filter == null)
                param.InstanceFilter();

            if (User.AreaId.HasValue && User.AreaId > 0)
                param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, User.AreaId);

            //algorithm
            dbObjectsTask = RepoViewWell.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewWell.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewWell, WellHistoryPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion

        #region "Helper"

        #endregion
    }
}