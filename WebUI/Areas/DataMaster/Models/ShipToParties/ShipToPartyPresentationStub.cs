﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.ShipToParties
{
    public class ShipToPartyPresentationStub : BasePresentationStub<Business.Entities.PowerPlant, ShipToPartyPresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int? AreaId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ShipToPartyName { get; set; }
        public string Address { get; set; }
        public string SalesGroup { get; set; }
        public string ShippingPoint { get; set; }
        public int? SoldToPartyId { get; set; }
        public string Material { get; set; }
        public int? URC { get; set; }
        public string SOType { get; set; }
        public string Description { get; set; }
        public string AgreementName { get; set; }
        public DateTime? AgreementDate { get; set; }
        public string MeterName { get; set; }
        public string MeterType { get; set; }
        public double BasePrice { get; set; }
        public double? BasePricePPI { get; set; }
        public double? TransmitionPrice { get; set; }
        public bool HasRelation { get; set; }
        public string AreaName { get; set; }
        #endregion

        #region "Contructor"
        public ShipToPartyPresentationStub() : base()
        {

        }

        public ShipToPartyPresentationStub(Business.Entities.PowerPlant dbObject) : base(dbObject)
        {
            SOType = ((SOType)Enum.Parse(typeof(SOType), dbObject.SOType)).ToDescription();
            Material = ((Material)Enum.Parse(typeof(Material), dbObject.Material)).ToDescription();
            AreaName = dbObject.Area.Name;
            AgreementDate = dbObject.AgreementDate != null ? dbObject.AgreementDate : DateTime.Now.Date;

            if (dbObject.Pltps.Count > 0 || dbObject.Dkps.Count > 0)
            {
                HasRelation = true;
            }
        }

        #endregion

        #region "Helper"
        protected override void Init()
        {
            //throw new NotImplementedException();
        }
        #endregion

    }
}