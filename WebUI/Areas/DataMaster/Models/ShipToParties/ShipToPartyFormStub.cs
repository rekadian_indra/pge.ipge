﻿using Business.Entities;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.ShipToParties
{
    public class ShipToPartyFormStub : BaseFormStub<PowerPlant, ShipToPartyFormStub>
    {
        #region "Properties"
        public int Id { get; set; }

        [DisplayName("Area")]
        public int? AreaId { get; set; }

        [DisplayName("Code")]
        [StringLength(10, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.PowerPlantCodeExists, ControllerSite.ShipToParty, AdditionalFields = "CurrentCode", ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Code { get; set; }

        [DisplayName("Ship To Party")]
        [StringLength(250, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        //[Remote(ActionSite.PowerPlantNameExists, ControllerSite.ShipToParty, AdditionalFields = "CurrentName", ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string ShipToPartyName { get; set; }

        [DisplayName("Name")]
        [StringLength(250, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        //[Remote(ActionSite.PowerPlantNameExists, ControllerSite.ShipToParty, AdditionalFields = "CurrentName", ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Name { get; set; }

        [DisplayName("Address")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Address { get; set; }

        [DisplayName("Sales Group")]
        [StringLength(50, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string SalesGroup { get; set; }

        [DisplayName("Shipping Point")]
        [StringLength(50, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string ShippingPoint { get; set; }

        [DisplayName("Sold to Party")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? SoldToPartyId { get; set; }

        [DisplayName("Material")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Material { get; set; }

        [DisplayName("URC")]
        public int? URC { get; set; }

        [DisplayName("So Type")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string SOType { get; set; }

        [StringLength(500, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Description { get; set; }

        [DisplayName("Agreement Name")]
        public string AgreementName { get; set; }

        [DisplayName("Agreement Date")]
        public DateTime? AgreementDate { get; set; }

        [DisplayName("Meter Name")]
        public string MeterName { get; set; }

        [DisplayName("Meter Type")]
        public string MeterType { get; set; }

        [DisplayName("Base Price (USD)")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double BasePrice { get; set; }

        [DisplayName("Base Price PPI (YB)")]
        public double? BasePricePPI { get; set; }

        [DisplayName("Transmition Price")]
        public double? TransmitionPrice { get; set; }

        public string CurrentCode { get; set; }

        [DisplayName("Invoice Group")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int InvoiceGroup { get; set; }
        //public string CurrentName { get; set; }
        #endregion

        #region "Constructor"
        public ShipToPartyFormStub() : base()
        {

        }

        public ShipToPartyFormStub(PowerPlant dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            CurrentCode = dbObject.Code;
            AgreementDate = dbObject.AgreementDate != null ? dbObject.AgreementDate : DateTime.Now.Date;
        }
        #endregion

        #region "Helper"
        public override void MapDbObject(PowerPlant dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here
        }

        protected override void Init()
        {

        }
        #endregion
    }
}