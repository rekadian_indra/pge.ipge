﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.InterfacePoint
{
    public class InterfacePointPresentationStub : BasePresentationStub<Business.Entities.InterfacePoint, InterfacePointPresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public Nullable<int> AreaId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        #endregion

        #region "Contructor"
        public InterfacePointPresentationStub() : base()
        {

        }

        public InterfacePointPresentationStub(Business.Entities.InterfacePoint dbObject) : base(dbObject)
        {
        }

        #endregion

        #region "Helper"
        protected override void Init()
        {
            //throw new NotImplementedException();
        }
        #endregion

    }
}