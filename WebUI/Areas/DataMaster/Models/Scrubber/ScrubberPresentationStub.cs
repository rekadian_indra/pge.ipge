﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Scrubber
{
    public class ScrubberPresentationStub : BasePresentationStub<Business.Entities.Scrubber, ScrubberPresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public Nullable<int> AreaId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        #endregion

        #region "Contructor"
        public ScrubberPresentationStub() : base()
        {

        }

        public ScrubberPresentationStub(Business.Entities.Scrubber dbObject) : base(dbObject)
        {

        }

        #endregion

        #region "Helper"
        protected override void Init()
        {
            //throw new NotImplementedException();
        }
        #endregion

    }
}