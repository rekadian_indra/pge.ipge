﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models
{
    public class MaintainPriceElectricFormStub : BaseFormStub<PPI, MaintainPriceElectricFormStub>
    {
		#region "Properties"
		public List<WorkflowPresentationStub> Workflows;

        public int Id { get; set; }

		[DisplayName("Power Plant")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public int PowerPlantId { get; set; }

		[DisplayName("Tahun")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public int Year { get; set; }

		[DisplayName("Triwulan ke")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public byte QuarterNum { get; set; }

		[DisplayName("PPI")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public double ValueMonth1 { get; set; }

		[DisplayName("PPI")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public double ValueMonth2 { get; set; }

		[DisplayName("PPI")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public double ValueMonth3 { get; set; }

		[DisplayName("Indeks Inflasi")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public double InflationIndex { get; set; }

		[DisplayName("Harga Triwulan Berikutnya")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public double NextPrice { get; set; }

		[DisplayName("PPI")]
		public double? ValueMonth1Add { get; set; }

		[DisplayName("PPI")]
		public double? ValueMonth2Add { get; set; }

		[DisplayName("PPI")]
		public double? ValueMonth3Add { get; set; }

		[DisplayName("Indeks Inflasi")]
		public double? InflationIndexAdd { get; set; }

		[DisplayName("Harga Triwulan Berikutnya")]
		public double? NextPriceAdd { get; set; }

		public int WorkflowId { get; set; }

		public PpiApproval LastPpiApproval { get; set; }

		public string StrMonth1 { get; set; }
		public string StrMonth2 { get; set; }
		public string StrMonth3 { get; set; }
		public string StrNextPrice { get; set; }

		public string StrNextMonths { get; set; }

		public double BasePPI { get; set; }

		public PowerPlantPresentationStub PowerPlant { get; set; }

		public bool IsDraft { get; set; }
		public bool IsEditable { get; set; }
		public bool IsApprovable { get; set; }
		public bool IsRevisable { get; set; }
		#endregion

		#region "Constructor"
		public MaintainPriceElectricFormStub() : base()
		{
			Year = DateTime.Now.Year;
			QuarterNum = 1;
			ValueMonth1 = 0;
			ValueMonth2 = 0;
			ValueMonth3 = 0;

			ValueMonth1Add = 0;
			ValueMonth2Add = 0;
			ValueMonth3Add = 0;

			IsEditable = true;
			IsApprovable = true;
			IsRevisable = false;

			BasePPI = double.Parse(System.Configuration.ConfigurationManager.AppSettings["BasePPI"]);
			InflationIndex = 1;
			InflationIndexAdd = 1;

			CalculateStrNextMonths();
		}

		public MaintainPriceElectricFormStub(List<Workflow> workflows, double? basePPI) : base()
        {
			Year = DateTime.Now.Year;
			QuarterNum = 1;
			ValueMonth1 = 0;
			ValueMonth2 = 0;
			ValueMonth3 = 0;

			ValueMonth1Add = 0;
			ValueMonth2Add = 0;
			ValueMonth3Add = 0;

			IsEditable = true;
			IsApprovable = true;
			IsRevisable = false;

			SetWorkflow(workflows);
			SetWorkflowId(workflows.First(x => x.PreviousStep == null).Id);

            //BasePPI = double.Parse(System.Configuration.ConfigurationManager.AppSettings["BasePPI"]);
            BasePPI = basePPI ?? double.Parse(System.Configuration.ConfigurationManager.AppSettings["BasePPI"]);
            InflationIndex = 1;
			InflationIndexAdd = null;

			CalculateStrNextMonths();
		}

        public MaintainPriceElectricFormStub(PPI dbObject, List<Workflow> workflows) : base(dbObject)
        {
			//TODO: Manual mapping object here

			PowerPlant = new PowerPlantPresentationStub(dbObject.PowerPlant);
			SetWorkflow(workflows);
			SetWorkflowId(dbObject.WorkflowId);

            //BasePPI = double.Parse(System.Configuration.ConfigurationManager.AppSettings["BasePPI"]);
            BasePPI = dbObject.PowerPlant.BasePricePPI ?? double.Parse(System.Configuration.ConfigurationManager.AppSettings["BasePPI"]);

            ///case beda rumus untuk kamojang 4
            if (dbObject.PowerPlantId == 1021) {
                InflationIndex = 0.46 + (0.54 * (ValueMonth1 + ValueMonth2 + ValueMonth3) / 3 / BasePPI);
                InflationIndexAdd = 0.46 + (0.54 * (ValueMonth1Add + ValueMonth2Add + ValueMonth3Add) / 3 / BasePPI);
            }
            else
            {
                InflationIndex = 0.6 + (0.4 * (ValueMonth1 + ValueMonth2 + ValueMonth3) / 3 / BasePPI);                
                InflationIndexAdd = 0.6 + (0.4 * (ValueMonth1Add + ValueMonth2Add + ValueMonth3Add) / 3 / BasePPI);
                
            }

            InflationIndex = Math.Round(InflationIndex, 5);
            InflationIndexAdd = InflationIndexAdd.HasValue ? Math.Round(InflationIndexAdd.Value, 5) : 0;
            NextPrice = Math.Round(NextPrice, 8);
            NextPriceAdd = NextPriceAdd.HasValue ? Math.Round(NextPriceAdd.Value, 8) : 0;
        }
        #endregion

        #region "Helper"
        public void MapDbObject(PPI dbObject, bool isAdditional=false)
        {
            base.MapDbObject(dbObject);

			//TODO: Manual mapping object here
			//dbObject.InflationIndex = 0.6 + (0.4 * (ValueMonth1 + ValueMonth2 + ValueMonth3) / 3 / BasePPI);
			//dbObject.NextPrice = PowerPlant.BasePrice * dbObject.InflationIndex;
			if (isAdditional)
			{
				//dbObject.InflationIndexAdd = 0.6 + (0.4 * (ValueMonth1Add + ValueMonth2Add + ValueMonth3Add) / 3 / BasePPI);
				//dbObject.NextPriceAdd = PowerPlant.BasePrice * dbObject.InflationIndexAdd;
			}
			else
			{
				dbObject.InflationIndexAdd = null;
				dbObject.NextPriceAdd = null;
			}
		}

		public void NextPpi(PPI dbObject)
		{
			PowerPlantId = dbObject.PowerPlantId;
			Year = (dbObject.QuarterNum < 4) ? dbObject.Year : dbObject.Year + 1;
			QuarterNum = ((dbObject.QuarterNum % 4) + 1).ToByte();
			
			CalculateStrNextMonths();
		}

		public void CalculateLabels()
		{
			
			StrMonth1 = (new DateTime(Year, DateHelper.QuarterToMonthStart(QuarterNum), 1)).ToString("MMMM");
			StrMonth2 = (new DateTime(Year, DateHelper.QuarterToMonthStart(QuarterNum) + 1, 1)).ToString("MMMM");
			StrMonth3 = (new DateTime(Year, DateHelper.QuarterToMonthStart(QuarterNum) + 2, 1)).ToString("MMMM");
			
			int nextQuarter = DateHelper.NextQuarter(QuarterNum);
			int nextQuarterYear = (nextQuarter == 1) ? Year + 1 : Year;
			StrNextPrice = $"Harga Triwulan {Enum.GetName(typeof(Common.Enums.QuarterYear), nextQuarter)} Tahun {nextQuarterYear}";

			CalculateStrNextMonths();
		}

		public void SetWorkflow(List<Workflow> workflows)
		{
			Workflows = ListMapper.MapList<Workflow, WorkflowPresentationStub>(workflows);
		}

		public void SetWorkflowId(int workflowId)
		{
			WorkflowId = workflowId;
			foreach (WorkflowPresentationStub workflow in Workflows)
			{
				if (workflow.Id == workflowId)
				{
					workflow.Active = true;
				}
				else
				{
					workflow.Active = false;
				}
			}
		}

		public void CalculateValidAction(List<Business.Entities.SalesOrder> salesOrders, List<WorkflowPresentationStub> workflows)
		{
			int curNextPeriodYear = (QuarterNum < 4) ? Year : Year + 1;
			byte curNextPeriodQuarterNum = ((QuarterNum % 4) + 1).ToByte();
			bool isSalesOrderExist = salesOrders.Where(x => x.Periode.HasValue).Where(x =>
				(x.Periode.Value.Year == curNextPeriodYear) &&
				(x.Periode.Value.Month >= (curNextPeriodQuarterNum * 3) - 2) &&
				(x.Periode.Value.Month <= (curNextPeriodQuarterNum * 3))
			).Any();

			if (isSalesOrderExist)
			{
				IsEditable = false;
				IsApprovable = false;
				IsRevisable = false;
			}
			else
			{
                
                    if (User.IsInRole(Common.Enums.UserRole.Superadmin.ToString()))                    
                        IsEditable = true;                    
                    else
                        IsEditable = workflows.Where(x => (x.Id == WorkflowId) && (x.PreviousStep == null)).Any();

                    IsApprovable = workflows.Where(x => x.PreviousStep == WorkflowId).Any();
                    IsRevisable = !IsEditable && IsApprovable;
			}
		}

		protected override void Init()
        {

        }

		private void CalculateStrNextMonths()
		{
			StrNextMonths = "";
			for (int i = 0; i < 3; i++)
			{
				int curMonth = (((QuarterNum * 3) + i) % 12) + 1;
				StrNextMonths += (StrNextMonths != "" ? ", " : "") + (new DateTime(DateTime.Now.Year, curMonth, 1)).ToString("MMMM");
			}
		}

		#endregion
	}
}