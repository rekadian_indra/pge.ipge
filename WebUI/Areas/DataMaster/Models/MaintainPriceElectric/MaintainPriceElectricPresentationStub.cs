﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models
{
    public class MaintainPriceElectricPresentationStub : BasePresentationStub<Business.Entities.PPI, MaintainPriceElectricPresentationStub>
    {
		#region "Properties"
		public List<WorkflowPresentationStub> Workflows;

		public int Id { get; set; }
		[System.ComponentModel.DisplayName("Power Plant")]
		public int PowerPlantId { get; set; }
		public int Year { get; set; }
		public byte QuarterNum { get; set; }
		public double ValueMonth1 { get; set; }
		public double ValueMonth2 { get; set; }
		public double ValueMonth3 { get; set; }
		public double InflationIndex { get; set; }
		public double NextPrice { get; set; }
		public double? ValueMonth1Add { get; set; }
		public double? ValueMonth2Add { get; set; }
		public double? ValueMonth3Add { get; set; }
		public double? InflationIndexAdd { get; set; }
		public double? NextPriceAdd { get; set; }
		public int WorkflowId { get; set; }

		public string StrQuarterNum { get; set; }
		public string StrMonth1 { get; set; }
		public string StrMonth2 { get; set; }
		public string StrMonth3 { get; set; }
		public string StrInflationIndex { get; set; }
		public string StrNextQuarterYear { get; set; }
		public string StrNextPrice { get; set; }
		public string StrWorkflow { get; set; }

		public string SOType { get; set; }

		public double BasePPI { get; set; }

		public bool IsEditable { get; set; }
		public bool IsApprovable { get; set; }
		public bool IsRevisable { get; set; }
		#endregion

		#region "Contructor"
		public MaintainPriceElectricPresentationStub() : base()
        {

        }

        public MaintainPriceElectricPresentationStub(Business.Entities.PPI dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            //BasePPI = double.Parse(System.Configuration.ConfigurationManager.AppSettings["BasePPI"]);
            BasePPI = dbObject.PowerPlant.BasePricePPI ?? double.Parse(System.Configuration.ConfigurationManager.AppSettings["BasePPI"]);
;

            //InflationIndex = 0.6 + (0.4 * (dbObject.ValueMonth1 + dbObject.ValueMonth2 + dbObject.ValueMonth3) / 3 / BasePPI);
            //InflationIndexAdd = dbObject.InflationIndexAdd;
            //NextPrice = dbObject.PowerPlant.BasePrice * InflationIndex;
            //NextPriceAdd = dbObject.NextPriceAdd;

            int nextQuarter = DateHelper.NextQuarter(QuarterNum);
			int nextQuarterYear = (nextQuarter == 1) ? Year + 1 : Year;

			StrInflationIndex = DisplayFormat.NumberFormatFiveDecimal(InflationIndex);
			StrInflationIndex += (InflationIndexAdd > 0) ? " (Add. " + DisplayFormat.NumberFormatFiveDecimal(InflationIndexAdd) + ")" : "";
			StrNextQuarterYear = $"Triwulan {Enum.GetName(typeof(QuarterYear), nextQuarter)} Tahun {nextQuarterYear}";
			StrNextPrice = DisplayFormat.NumberFormatFiveDecimal(NextPrice);
			StrNextPrice += (NextPriceAdd > 0) ? " (Add. " + DisplayFormat.NumberFormatFiveDecimal(NextPriceAdd) + ")" : "";

			StrMonth1 = (new DateTime(Year, 1 + ((QuarterNum - 1) * 3), 1)).ToString("MMMM");
			StrMonth2 = (new DateTime(Year, 2 + ((QuarterNum - 1) * 3), 1)).ToString("MMMM");
			StrMonth3 = (new DateTime(Year, 3 + ((QuarterNum - 1) * 3), 1)).ToString("MMMM");

			StrQuarterNum = Enum.GetName(typeof(QuarterYear), QuarterNum);

			SOType = dbObject.PowerPlant.SOType;
		}

		#endregion

		#region "Helper"
		protected override void Init()
        {
            //throw new NotImplementedException();
        }

		public void CalculateValidAction(List<Business.Entities.SalesOrder> salesOrders, List<WorkflowPresentationStub> workflows)
		{
			int curNextPeriodYear = (QuarterNum < 4) ? Year : Year + 1;
			byte curNextPeriodQuarterNum = ((QuarterNum % 4) + 1).ToByte();
			bool isSalesOrderExist = salesOrders.Where(x => x.Periode.HasValue).Where(x =>
				(x.Periode.Value.Year == curNextPeriodYear) &&
				(x.Periode.Value.Month >= (curNextPeriodQuarterNum * 3) - 2) &&
				(x.Periode.Value.Month <= (curNextPeriodQuarterNum * 3))
			).Any();

			if (isSalesOrderExist)
			{
				IsEditable = false;
				IsApprovable = false;
				IsRevisable = false;
			}
			else
			{
				IsEditable = workflows.Where(x => (x.Id == WorkflowId) && (x.PreviousStep == null)).Any();
				IsApprovable = workflows.Where(x => x.PreviousStep == WorkflowId).Any();
				IsRevisable = !IsEditable && IsApprovable;
			}
		}
		#endregion

	}
}