﻿using Business.Entities;
using Business.Entities.Views;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Wells
{
    public class WellHistoryPresentationStub : BasePresentationStub<ViewWell, WellHistoryPresentationStub>
    {
        #region Properties

        public int Id { get; set; }
        public int AreaId { get; set; }
        public int WellId { get; set; }
        public string WellName { get; set; }
        public string ClusterName { get; set; }
        public string Status { get; set; }
        public string StatusDesc { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        #endregion

        #region Constructor

        public WellHistoryPresentationStub() : base()
        {
        }

        public WellHistoryPresentationStub(ViewWell dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            StartDate = dbObject.StartDate.ToLocalTime();
            EndDate = dbObject.EndDate.ToLocalTime();
        }

        #endregion

        #region Helper

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }

        #endregion
    }
}