﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Wells
{
    public class WellHistoryFormStub : BaseFormStub<WellHistory, WellHistoryFormStub>
    {
        #region "Properties"
        public int Id { get; set; }

        [DisplayName("Well")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int WellId { get; set; }

        [DisplayName("Status")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Status { get; set; }

        [DisplayName("Tanggal")]        
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public DateTime HistoryDateTimeUtc { get; set; }

        public string WellName { get; set; }

        public string ClusterName { get; set; }

        #endregion

        #region Constructor

        public WellHistoryFormStub() : base()
        {
        }

        public WellHistoryFormStub(Well dbObject)
        {
            //TODO: Manual mapping object
            WellId = dbObject.Id;
            WellName = dbObject.Name;
            HistoryDateTimeUtc = DateTime.Now;
        }

        public WellHistoryFormStub(WellHistory dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object
            if(dbObject.Well != null)
                WellName = dbObject.Well.Name;
            HistoryDateTimeUtc = HistoryDateTimeUtc.ToLocalDateTime();
        }

        #endregion

        #region Helper

        public override void MapDbObject(WellHistory dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here   
            dbObject.HistoryDateTimeUtc = HistoryDateTimeUtc.ToUtcDateTime();
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }

        #endregion
    }
}