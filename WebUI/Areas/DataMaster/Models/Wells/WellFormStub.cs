﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Wells
{
    public class WellFormStub : BaseFormStub<Well, WellFormStub>
    {
        #region "Properties"
        public int Id { get; set; }

        [DisplayName("Area ")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public Nullable<int> AreaId { get; set; }

        [DisplayName("Cluster ")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public Nullable<int> ClusterId { get; set; }

        [DisplayName("Name")]
        [StringLength(250, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Name { get; set; }

        [DisplayName("Code")]
        [StringLength(10, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Code { get; set; }

        [DisplayName("Description")]
        [StringLength(500, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Description { get; set; }
        #endregion

        #region Constructor

        public WellFormStub() : base()
        {
        }

        public WellFormStub(Well dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object
           
        }

        #endregion

        #region Helper

        public override void MapDbObject(Well dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here           
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }

        #endregion
    }
}