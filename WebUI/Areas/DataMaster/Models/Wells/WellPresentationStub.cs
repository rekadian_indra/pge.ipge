﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Wells
{
    public class WellPresentationStub : BasePresentationStub<Well, WellPresentationStub>
    {
        #region Properties

        public int Id { get; set; }
        public Nullable<int> AreaId { get; set; }
        public string AreaName { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Nullable<int> ClusterId { get; set; }
        public string ClusterName { get; set; }
        public string Status { get; set; }
        public bool HasRelation { get; set; }
        #endregion

        #region Constructor

        public WellPresentationStub() : base()
        {
        }

        public WellPresentationStub(Well dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            if (dbObject.Area != null)
            {
                AreaName = dbObject.Area.Name;
            }
            if (dbObject.Cluster != null)
            {
                ClusterName = dbObject.Cluster.Name;
            }
            if (dbObject.WellHistories != null)
            {
                List<WellHistory> wellHistory = new List<WellHistory>();
                wellHistory = dbObject.WellHistories.OrderByDescending(p => p.HistoryDateTimeUtc).ToList();
                if (wellHistory.Count > 0)
                {
                    Status = wellHistory.FirstOrDefault().Status;
                }
                else
                {
                    Status = "-";
                }
               
            }

            if (dbObject.Monitorings.Count > 0 || dbObject.Productions.Count > 0 || dbObject.ReInjections.Count > 0)
            {
                HasRelation = true;
            }
            else
            {
                HasRelation = false;
            }
        }

        #endregion

        #region Helper

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }

        #endregion
    }
}