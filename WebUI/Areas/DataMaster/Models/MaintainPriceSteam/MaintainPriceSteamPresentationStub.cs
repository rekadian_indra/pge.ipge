﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models
{
    public class MaintainPriceSteamPresentationStub : BasePresentationStub<Business.Entities.SteamPowerPlantPrice, MaintainPriceSteamPresentationStub>
    {
		#region "Properties"
		public List<WorkflowPresentationStub> Workflows;

		public int Id { get; set; }
		[System.ComponentModel.DisplayName("Power Plant")]
		public int PowerPlantId { get; set; }
		public int Year { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public double IncreasePercentage { get; set; }
		public double Price { get; set; }
		public double? IncreasePercentageAdd { get; set; }
		public double? PriceAdd { get; set; }
		public int WorkflowId { get; set; }

		public string StrPrice { get; set; }
		public string StrPriceAdd { get; set; }
		public string StrWorkflow { get; set; }

		public string SOType { get; set; }

		public bool IsEditable { get; set; }
		public bool IsApprovable { get; set; }
		public bool IsRevisable { get; set; }
		#endregion

		#region "Contructor"
		public MaintainPriceSteamPresentationStub() : base()
        {

        }

        public MaintainPriceSteamPresentationStub(Business.Entities.SteamPowerPlantPrice dbObject) : base(dbObject)
        {
			//TODO: Manual mapping object here
			
			StrPrice = DisplayFormat.NumberFormatFiveDecimal(Price);
			StrPrice += (PriceAdd > 0) ? " (Add. " + DisplayFormat.NumberFormatFiveDecimal(PriceAdd) + ")" : "";

			SOType = dbObject.PowerPlant.SOType;
		}

		#endregion

		#region "Helper"
		protected override void Init()
        {
            //throw new NotImplementedException();
        }

		public void CalculateValidAction(List<Business.Entities.SalesOrder> salesOrders, List<WorkflowPresentationStub> workflows)
		{
			bool isSalesOrderExist = salesOrders.Where(x => x.Periode.HasValue).Where(x =>
				x.Periode.Value.Year == Year
			).Any();

			if (isSalesOrderExist)
			{
				IsEditable = false;
				IsApprovable = false;
				IsRevisable = false;
			}
			else
			{
				IsApprovable = workflows.Where(x => x.PreviousStep == WorkflowId).Any();
				IsEditable = workflows.Where(x => (x.Id == WorkflowId) && (x.PreviousStep == null)).Any() || !IsApprovable;
				IsRevisable = !IsEditable && IsApprovable;
			}
		}
		#endregion

	}
}