﻿using Business.Infrastructure;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models
{
	public class MaintainPriceSteamExcelStub : BaseExcelStub
	{
		const string FORM_NO_SUFFIX = "/Perubahan Harga/PGE2022/2018";

		#region Fields

		public string NomorForm;

		public string Area;
		public string ConditionType;
		public string Salesorg;
		public string ShipTo;
		public string Material;
		public string SoldToParty;
		public string SalesGroup;
		public string ValidityStart;
		public string ValidityEnd;
		public string Amount;
		public string Currency;
		public string Per;
		public string UoM;
		public string Nama;

		public string SignaturePlace;
		public string SignatureDate;
		public string RequesterName;
		public string RequesterPosition;
		public string RequesterNo;
		public string RequesterSignature;
		public string LegalName;
		public string LegalPosition;
		public string LegalNo;
		public string LegalSignature;


		#endregion

		#region Constructor

		public MaintainPriceSteamExcelStub() : base()
		{
			NomorForm = "";

			Area = "";
			ConditionType = "";
			Salesorg = "";
			ShipTo = "";
			Material = "";
			SoldToParty = "";
			SalesGroup = "";
			ValidityStart = "";
			ValidityEnd = "";
			Amount = "";
			Currency = "";
			Per = "";
			UoM = "";
			Nama = "";

			SignaturePlace = "";
			SignatureDate = "";
			RequesterName = "";
			RequesterPosition = "";
			RequesterNo = "";
			RequesterSignature = "";
			LegalName = "";
			LegalPosition = "";
			LegalNo = "";
			LegalSignature = "";
		}

		public MaintainPriceSteamExcelStub(XSSFWorkbook workbook, Business.Entities.SteamPowerPlantPrice steamPowerPlantPrice) : base(workbook)
		{
			if (!Enum.TryParse(steamPowerPlantPrice.PowerPlant.Material, out Common.Enums.Material curMaterial))
			{
				curMaterial = Common.Enums.Material.GEOTHERMAL_STEAM; // assign asal aja
			}
			DateTime priceValidDateStart = new DateTime(steamPowerPlantPrice.Year, 1, 1);
			DateTime priceValidDateEnd = new DateTime(steamPowerPlantPrice.Year, 12, 31);

			NomorForm = steamPowerPlantPrice.FormSequence + FORM_NO_SUFFIX;

			Area = steamPowerPlantPrice.PowerPlant.Area.Name;
			ConditionType = "";
			Salesorg = "";
			ShipTo = steamPowerPlantPrice.PowerPlant.Code;
			Material = Common.Enums.EnumExtension.ToDescription(curMaterial).Before(" - ");
			SoldToParty = steamPowerPlantPrice.PowerPlant.SoldToParty.Code;
			SalesGroup = steamPowerPlantPrice.PowerPlant.SalesGroup;
			ValidityStart = priceValidDateStart.ToString("yyyyMMdd");
			ValidityEnd = priceValidDateEnd.ToString("yyyyMMdd");
			Amount = DisplayFormat.NumberFormatFiveDecimal(steamPowerPlantPrice.Price * 1000);
			Currency = "USDN";
			Per = "1000";
			UoM = "kWh";
			Nama = steamPowerPlantPrice.PowerPlant.Name;

			SignaturePlace = "Jakarta";
			SignatureDate = DateTime.Now.ToString(DisplayFormat.FullDateFormat);
			RequesterName = "Ibu Yusniar Ully A";
			RequesterPosition = "Engineer Production";
			RequesterNo = "19020176";
			RequesterSignature = "";
			LegalName = "Bpk Uus Rahmat Kurniawan";
			LegalPosition = "Manager Production";
			LegalNo = "737881";
			LegalSignature = "";
		}

		public MaintainPriceSteamExcelStub(XSSFWorkbook workbook, Business.Entities.SteamPowerPlantPrice steamPowerPlantPrice, Business.Entities.Membership requesterMember, Business.Entities.Membership legalMember) : this(workbook, steamPowerPlantPrice)
		{
			RequesterName = requesterMember.FullName;
			RequesterPosition = requesterMember.Position;
			RequesterNo = requesterMember.EmployeeNo;
			RequesterSignature = requesterMember.SignatureFilePath;
			LegalName = legalMember.FullName;
			LegalPosition = legalMember.Position;
			LegalNo = legalMember.EmployeeNo;
			LegalSignature = legalMember.SignatureFilePath;
		}

		#endregion

		#region Template Generator

		public byte[] GenerateTemplate()
		{
			// set sheet
			XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(0);

			// masuk2in data
			SetCellValue(sheet, "A", 2, NomorForm);
			SetCellValue(sheet, "A", 5, Area);
			SetCellValue(sheet, "A", 7, ConditionType);
			SetCellValue(sheet, "B", 7, Salesorg);
			SetCellValue(sheet, "C", 7, ShipTo);
			SetCellValue(sheet, "D", 7, Material);
			SetCellValue(sheet, "E", 7, SoldToParty);
			SetCellValue(sheet, "F", 7, SalesGroup);
			SetCellValue(sheet, "G", 7, ValidityStart);
			SetCellValue(sheet, "H", 7, ValidityEnd);
			SetCellValue(sheet, "I", 7, Amount);
			SetCellValue(sheet, "J", 7, Currency);
			SetCellValue(sheet, "K", 7, Per);
			SetCellValue(sheet, "L", 7, UoM);
			SetCellValue(sheet, "M", 7, Nama);
			SetCellValue(sheet, "A", 9, $"{SignaturePlace}, {SignatureDate}");
			SetCellValue(sheet, "A", 15, RequesterName);
			SetCellValue(sheet, "A", 16, $"Jabatan: {RequesterPosition}");
			SetCellValue(sheet, "A", 17, $"No Pekerja: {RequesterNo}");
			if (RequesterSignature != null) SetCellImage(sheet, "A", 11, "B", 14, RequesterSignature);
			SetCellValue(sheet, "D", 15, LegalName);
			SetCellValue(sheet, "D", 16, $"Jabatan: {LegalPosition}");
			SetCellValue(sheet, "D", 17, $"No Pekerja: {LegalNo}");
			if (LegalSignature != null) SetCellImage(sheet, "D", 11, "E", 14, LegalSignature);

			return WorkbookToArray();
		}

		#endregion
	}
}