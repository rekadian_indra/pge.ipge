﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models
{
    public class MaintainPriceSteamFormStub : BaseFormStub<SteamPowerPlantPrice, MaintainPriceSteamFormStub>
    {
		#region "Properties"
		public List<WorkflowPresentationStub> Workflows;

        public int Id { get; set; }

		[DisplayName("Power Plant")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public int PowerPlantId { get; set; }

		[DisplayName("Tahun")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public int Year { get; set; }

        [DisplayName("Start Date")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public System.DateTime StartDate { get; set; }

        [DisplayName("End Date")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public System.DateTime EndDate { get; set; }

        [DisplayName("Harga Tahun Sebelumnya")]
		public double PrevPrice { get; set; }

		[DisplayName("Persentase Kenaikan")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public double IncreasePercentage { get; set; }

		[DisplayName("Harga")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public double Price { get; set; }

		[DisplayName("Harga Tahun Sebelumnya")]
		public double PrevPriceAdd { get; set; }

		[DisplayName("Persentase Kenaikan")]
		public double? IncreasePercentageAdd { get; set; }

		[DisplayName("Harga")]
		public double? PriceAdd { get; set; }

		public int WorkflowId { get; set; }

		public SteamPowerPlantPriceApproval LastSteamPowerPlantPriceApproval { get; set; }

		public PowerPlantPresentationStub PowerPlant { get; set; }

		public bool IsDraft { get; set; }
		public bool IsEditable { get; set; }
		public bool IsApprovable { get; set; }
		public bool IsRevisable { get; set; }
		#endregion

		#region "Constructor"
		public MaintainPriceSteamFormStub() : base()
		{
			Year = DateTime.Now.Year;
            StartDate = DateTime.Now; ;
            EndDate = DateTime.Now.AddYears(1);

            IncreasePercentageAdd = 0;
			PriceAdd = 0;

			IsEditable = true;
			IsApprovable = true;
			IsRevisable = false;
		}

		public MaintainPriceSteamFormStub(List<Workflow> workflows) : base()
        {
            Year = DateTime.Now.Year;
            StartDate = DateTime.Now;
            EndDate = DateTime.Now.AddYears(1);

            IncreasePercentageAdd = 0;
			PriceAdd = 0;

			IsEditable = true;
			IsApprovable = true;
			IsRevisable = false;

			SetWorkflow(workflows);
			SetWorkflowId(workflows.First(x => x.PreviousStep == null).Id);
		}

        public MaintainPriceSteamFormStub(SteamPowerPlantPrice dbObject, List<Workflow> workflows) : base(dbObject)
        {
			//TODO: Manual mapping object here

			PowerPlant = new PowerPlantPresentationStub(dbObject.PowerPlant);
			SetWorkflow(workflows);
			SetWorkflowId(dbObject.WorkflowId);
		}
		#endregion

		#region "Helper"
		public void MapDbObject(SteamPowerPlantPrice dbObject, bool isAdditional = false)
        {
            base.MapDbObject(dbObject);

			if (!isAdditional)
			{
				IncreasePercentageAdd = null;
				PriceAdd = null;
			}

			//TODO: Manual mapping object here
		}

		public void NextSteamPowerPlantPrice(SteamPowerPlantPrice dbObject)
		{
			PowerPlantId = dbObject.PowerPlantId;
			Year = dbObject.Year + 1;
		}

		public void SetWorkflow(List<Workflow> workflows)
		{
			Workflows = ListMapper.MapList<Workflow, WorkflowPresentationStub>(workflows);
		}

		public void SetWorkflowId(int workflowId)
		{
			WorkflowId = workflowId;
			foreach (WorkflowPresentationStub workflow in Workflows)
			{
				if (workflow.Id == workflowId)
				{
					workflow.Active = true;
				}
				else
				{
					workflow.Active = false;
				}
			}
		}

		public void CalculateValidAction(List<Business.Entities.SalesOrder> salesOrders, List<WorkflowPresentationStub> workflows)
		{
			bool isSalesOrderExist = salesOrders.Where(x => x.Periode.HasValue).Where(x =>
				x.Periode.Value.Year == Year
			).Any();

			if (isSalesOrderExist)
			{
				IsEditable = false;
				IsApprovable = false;
				IsRevisable = false;
			}
			else
			{
				IsApprovable = workflows.Where(x => x.PreviousStep == WorkflowId).Any();
				IsEditable = workflows.Where(x => (x.Id == WorkflowId) && (x.PreviousStep == null)).Any() || !IsApprovable;
				IsRevisable = !IsEditable && IsApprovable;
			}
		}

		protected override void Init()
        {

        }
        #endregion
    }
}