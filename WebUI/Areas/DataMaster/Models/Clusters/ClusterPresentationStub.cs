﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Clusters
{
    public class ClusterPresentationStub : BasePresentationStub<Cluster, ClusterPresentationStub>
    {
        #region Properties

        public int Id { get; set; }
        public Nullable<int> AreaId { get; set; }
        public string AreaName { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool HasRelation { get; set; }
        #endregion

        #region Constructor

        public ClusterPresentationStub() : base()
        {
        }

        public ClusterPresentationStub(Cluster dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            if (dbObject.Area != null)
            {
                AreaName = dbObject.Area.Name;
            }
            if (dbObject.Wells.Count > 0 )
            {
                HasRelation = true;
            }
            else
            {
                HasRelation = false;
            }
        }

        #endregion

        #region Helper

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }

        #endregion
    }
}