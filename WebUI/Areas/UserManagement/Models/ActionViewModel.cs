﻿using System.ComponentModel.DataAnnotations;

namespace WebUI.Areas.UserManagement.Models
{
    public class ActionViewModel
    {
        [Required(ErrorMessage="Action Name is required.")]
        public string ActionName { get; set; }
        public string Description { get; set; }
    }
}
