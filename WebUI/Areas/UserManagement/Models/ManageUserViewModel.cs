﻿using System;
using Business.Entities;

namespace WebUI.Areas.UserManagement.Models
{
    public class ManageUserViewModel
    {
        public Guid guid { get; set; }
        public string userName { get; set; }

        public ManageUserViewModel() { }
        public ManageUserViewModel(User user)
        {
            guid = user.UserId;
            userName = user.UserName;
        }
    }
}