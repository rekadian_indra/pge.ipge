﻿using System.Web.Mvc;

namespace WebUI.Areas.UserManagement.Models
{
    public class ManageRolesViewModel
    {
        public SelectList Roles { get; set; }
        public string[] RoleList { get; set; }
    }
}
