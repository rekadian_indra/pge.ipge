using Business.Entities;
using Business.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Areas.UserManagement.Models
{
    public class RoleFormStub
    {
        public Guid ApplicationId { get; set; }

        public Guid RoleId { get; set; }

        [Display(Name = "Role Name")]
        [RegularExpression(@"([\sa-zA-Z-._0-9]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.RoleNameExists, ControllerSite.Role, AreaSite.UserManagement, AdditionalFields = "CurrentRoleName", ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string RoleName { get; set; }

        public string Description { get; set; }

        public string CurrentRoleName { get; set; }

        public RoleFormStub()
        {
        }

        public RoleFormStub(Role dbObject)
        {
            ObjectMapper.MapObject<Role, RoleFormStub>(dbObject, this);
            CurrentRoleName = dbObject.RoleName;
        }
        public virtual void MapDbObject(Role dbObject)
        {
            ObjectMapper.MapObject<RoleFormStub, Role>(this, dbObject);
        }
    }
}
