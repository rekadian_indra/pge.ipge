using System;
using System.Collections.Generic;

namespace WebUI.Areas.UserManagement.Models
{
    public class AssignModuleStub
    {
        public Guid RoleId { get; set; }
        public List<ModuleFormStub> Modules { get; set; }

        public AssignModuleStub()
        {
            Modules = new List<ModuleFormStub>();
        }
    }
}