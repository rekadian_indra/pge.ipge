﻿using Business.Entities;
using Common.Enums;
using System;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Models
{
    public class AreaPresentationStub : BasePresentationStub<Area, AreaPresentationStub>
    {
        public int Id { get; set; }        
        public string Name { get; set; }
        public string Code { get; set; }
        public string Plant { get; set; }

        public AreaPresentationStub() : base()
        {
        }

        public AreaPresentationStub(Area dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}