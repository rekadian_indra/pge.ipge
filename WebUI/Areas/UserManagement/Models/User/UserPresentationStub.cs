﻿using Business.Entities;
using Business.Entities.Views;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using WebUI.Models;

namespace WebUI.Models
{
    public class UserPresentationStub : BasePresentationStub<ViewUser, UserPresentationStub>
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public Nullable<int> AreaId { get; set; }
        public string Name { get; set; }
        public string Roles { get; set; }
        public UserPresentationStub() : base()
        {
        }

        public UserPresentationStub(ViewUser dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here          
        }

        public UserPresentationStub(ViewUser dbObject, Area area) : base(dbObject)
        {
            //TODO: Manual mapping object here           
            if (area != null && area.PowerPlants.Any(n => n.SOType == SOType.ZTS.ToString()))
                Name = SOType.ZTS.ToString();
        }

        public static List<UserPresentationStub> MapList(List<ViewUser> dbObjects, List<Area> areas, List<Role> roles)
        {
            List<UserPresentationStub> result = new List<UserPresentationStub>();
            foreach (ViewUser user in dbObjects)
            {
                UserPresentationStub model = new UserPresentationStub();
                var userRoles = roles.Where(n => n.Users.Any(m => m.UserId == user.UserId)).Select(n => n.Description);
                Area area = new Area();
                area = user.AreaId.HasValue && areas.Any(n => n.Id == user.AreaId.Value) ? areas.FirstOrDefault(n => n.Id == user.AreaId.Value) : null;
                model = new UserPresentationStub(user, area);
                model.Roles = userRoles != null && userRoles.Any() ? string.Join(", ", userRoles) : "-";
                result.Add(model);

            }
            return result;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}