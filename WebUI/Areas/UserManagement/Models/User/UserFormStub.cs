using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebUI.Infrastructure;

namespace WebUI.Areas.UserManagement.Models
{
    public class UserFormStub
    {
        public Guid UserId { get; set; }

        [Display(Name = "User ID")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string UserName { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessageResourceName = GlobalErrorField.EmailType, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessageResourceName = GlobalErrorField.MinStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError), MinimumLength = 3)]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Password { get; set; }

        [Display(Name = "Konfirmasi Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessageResourceName = GlobalErrorField.Compare, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string ConfirmPassword { get; set; }

        public string SecretQuestion { get; set; }

        public string SecretAnswer { get; set; }

        public bool Approve { get; set; }

        public bool RequireSecretQuestionAndAnswer { get; set; }

        public string CurrentUserName { get; set; }

        public string CurrentEmail { get; set; }

        [Display(Name = "Area")]
        public Nullable<int> AreaId { get; set; }

        public List<Role> Roles { get; set; }

		[Display(Name = "Nama Lengkap")]
		public string FullName { get; set; }

		[Display(Name = "Jabatan")]
		public string Position { get; set; }

		[Display(Name = "No Pekerja")]
		public string EmployeeNo { get; set; }

		[Display(Name = "Tanda Tangan")]
		public string SignatureFilePath { get; set; }
		public string SignatureFileName { get; set; }

		public UserFormStub()
        {
        }

        public UserFormStub(User dbObject)
        {
            Membership membership = dbObject.Membership;

            if (membership != null)
            {
                UserProfile profile = UserProfile.GetUserProfile(dbObject.UserName);

                UserId = dbObject.UserId;
                UserName = dbObject.UserName;
                Email = membership.Email;
                CurrentUserName = dbObject.UserName;
                CurrentEmail = membership.Email;
                SecretQuestion = membership.PasswordQuestion;
                SecretAnswer = membership.PasswordAnswer;
                Approve = membership.IsApproved;
                AreaId = membership.AreaId;
                Roles = dbObject.Roles.ToList();
                Password = membership.Password;
                ConfirmPassword = membership.Password;
				FullName = membership.FullName;
				Position = membership.Position;
                EmployeeNo = membership.EmployeeNo;
                SignatureFilePath = membership.SignatureFilePath;

				SignatureFileName = (SignatureFilePath != null) ? WebUI.Controllers.FileManagementController.FriendlyName(SignatureFilePath) : "";
			}
        }
    }
}
