﻿using System;
using Business.Entities;
using Business.Infrastructure;

namespace WebUI.Areas.UserManagement.Models
{
    public class ModulePresentationStub
    {

        public Guid ModuleId { get; set; }
        public string ModuleName { get; set; }
        public Guid? ParentModuleId { get; set; }

        public ModulePresentationStub()
        {
        }

        public ModulePresentationStub(Module dbObject)
        {
            //TO DO: Mapping db object to model
            ObjectMapper.MapObject<Module, ModulePresentationStub>(dbObject, this);
        }
    }
}