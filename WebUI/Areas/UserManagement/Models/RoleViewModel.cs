﻿using System.ComponentModel.DataAnnotations;

namespace WebUI.Areas.UserManagement.Models
{
    public class RoleViewModel
    {
        [Required(ErrorMessage="RoleName is required.")]
        public string RoleName { get; set; }
        public string Description { get; set; }
    }
}
