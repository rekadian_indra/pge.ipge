﻿using System.Web.Mvc;

namespace WebUI.Areas.UserManagement.Models
{
    public class ManageModulesViewModel
    {
        public SelectList Modules { get; set; }
        public string[] ModuleList { get; set; }
    }
}
