﻿namespace WebUI.Areas.UserManagement.Models
{
    public class DashboardModel
    {
        public int TotalUser { get; set; }
        public int TotalRole { get; set; }
    }
}
