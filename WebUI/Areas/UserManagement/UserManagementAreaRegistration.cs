using System.Web.Mvc;

namespace WebUI.Areas.UserManagement
{
    public class UserManagementAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "UserManagement";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {

            context.MapRoute("SearchMembership", "UserManagement/Membership/index/{searchterm}/{filterby}",
                new { controller = "Membership", action = "Index", searchterm = UrlParameter.Optional, filterby = "all" }
            );

            context.MapRoute("Membership", "UserManagement/Membership/{action}",
                new { controller = "Membership" }
            );

            context.MapRoute(
                "UserManagement_default",
                "UserManagement/{controller}/{action}/{id}",
                new { controller = "Dashboard", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
