using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using Newtonsoft.Json;
using SecurityGuard.Interfaces;
using SecurityGuard.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using WebUI.Areas.UserManagement.Models;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Infrastructure.Attribute;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Controllers
{
    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin })]
    [CheckSessionTimeOut]
    public partial class UserController : BaseController<UserFormStub>
    {
        private IMembershipService MembershipService { get; set; }
        private IRoleService RoleService { get; set; }        

        private string DefaultPassKey { get; set; }        

        public UserController(
            IUserRepository repoUser, 
            IRoleRepository repoRole, 
            IAreaRepository repoArea,
            IMembershipRepository repoMembership,
            IViewUserAreaRepository repoUserArea)
        {
            RepoRole = repoRole;
            RepoUser = repoUser;
            RepoArea = repoArea;
            MembershipService = new MembershipService(System.Web.Security.Membership.Provider);
            RepoMembership = repoMembership;
            RepoUserArea = repoUserArea;
            RoleService = new RoleService(Roles.Provider);
        }

        [MvcSiteMapNode(Title = TitleSite.User, Area = AreaSite.UserManagement, ParentKey = KeySite.DashboardUserManagement, Key = KeySite.IndexUser)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [HttpPost]
        public async Task<ActionResult> Index(UserFormStub model)
        {
            await Task.Delay(0);

            if (ModelState.IsValid || !ModelState.IsValidField(Field.Password.ToString()))
            {
                string template;
                MembershipUser membership = MembershipService.CreateUser(model.UserName, "123456", model.Email);
                if (membership != null)
                {
                    template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                    this.SetMessage(model.UserName, template);

					ActiveDirectoryUtil user = ActiveDirectoryUtil.GetUsers(model.UserName).FirstOrDefault();
					FilterQuery filter = new FilterQuery(Field.Email, FilterOperator.Equals, user.Email);
					Business.Entities.Membership membershipUser = RepoMembership.Find(filter);

					membershipUser.FullName = user.FullName;

					RepoMembership.Save(membershipUser);
				}

                return RedirectToAction("Index");
            }
            else
                return View(model);
        }
        public override async Task<ActionResult> Create()
        {
            UserFormStub model = new UserFormStub
            {
                RequireSecretQuestionAndAnswer = MembershipService.RequiresQuestionAndAnswer
            };

            await Task.Delay(0);

            return PartialView("_Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(UserFormStub model)
        {
            ResponseModel response = new ResponseModel(true);

            model.Password = model.ConfirmPassword = "123456";

            MembershipUser membership;

            model.Approve = true;
            membership = MembershipService.CreateUser(model.UserName, model.Password, model.Email);
            if (membership != null)
            {
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                string message = string.Format(template, membership.UserName);

                response.Message = message;

				ActiveDirectoryUtil user = ActiveDirectoryUtil.GetUsers(model.UserName).FirstOrDefault();
				FilterQuery filter = new FilterQuery(Field.Email, FilterOperator.Equals, model.Email);
				Business.Entities.Membership membershipUser = await RepoMembership.FindAsync(filter);

				membershipUser.FullName = user.FullName;

				RepoMembership.Save(membershipUser);
			}

            await Task.Delay(0);

            return Json(response);
        }

        public override async Task<ActionResult> Edit(params object[] id)
        {
            if (id == null)
                throw new ArgumentNullException();

            FilterQuery filter = new FilterQuery(Field.UserId, FilterOperator.Equals, id.ElementAt(0));
            User dbObject = await RepoUser.FindAsync(filter);
            UserFormStub model = new UserFormStub(dbObject);

            ViewBag.Breadcrumb = model.UserName;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(UserFormStub model)
        {
            ResponseModel response = new ResponseModel(true);
            string template; string message;

            if (ModelState.IsValid)
            {
                MembershipUser membership = MembershipService.GetUser(model.UserName);
                if (membership != null)
                {
                    WebUI.Models.UserProfile profile = WebUI.Models.UserProfile.GetUserProfile(membership.UserName);
                    template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                    message = string.Format(template, model.UserName);

                    //update user
                    membership.Email = model.Email;
                    MembershipService.UpdateUser(membership);

                    FilterQuery filter = new FilterQuery(Field.UserId, FilterOperator.Equals, model.UserId);
                    Business.Entities.Membership membershipUser = await RepoMembership.FindAsync(filter);
                    if (model.AreaId.HasValue)
                        membershipUser.AreaId = model.AreaId;
                    else
                        membershipUser.AreaId = 0;

                    membershipUser.FullName = model.FullName;
                    membershipUser.Position = model.Position;
                    membershipUser.EmployeeNo = model.EmployeeNo;

                    RepoMembership.Save(membershipUser);

                   
                }
                else
                {
                    template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditFailed).ToString();
                    message = string.Format(template, model.UserName);
                }

                //set message
                response.Message = message;
                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.UserName;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            if (id == null)
                throw new ArgumentNullException();

            string userName = id.ElementAt(0).ToString();
            ResponseModel response = new ResponseModel(true);

            if (!MembershipService.DeleteUser(userName))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            await Task.Delay(0);

            return Json(response);
        }

        public async Task<ActionResult> AssignRoles(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException();

            FilterQuery filter = new FilterQuery(Field.UserName, FilterOperator.Equals, Server.UrlDecode(id));
            User dbObject = await RepoUser.FindAsync(filter);
            UserFormStub model = new UserFormStub(dbObject);

            return PartialView("_AssignRoles", model);
        }

        [HttpPost]
        public async Task<ActionResult> AssignRoles(AssignRoleStub model)
        {
            ResponseModel response = new ResponseModel(true);
            User user = await RepoUser.FindByPrimaryKeyAsync(model.UserId);
            List<Role> roles = new List<Role>();

            foreach (RoleFormStub m in model.Roles)
            {
                Role role = new Role();

                m.MapDbObject(role);
                roles.Add(role);
            }

            await RepoUser.AssignRoleAsync(user, roles);

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> RevokeRoles(AssignRoleStub model)
        {
            ResponseModel response = new ResponseModel(true);
            User user = await RepoUser.FindByPrimaryKeyAsync(model.UserId);
            List<Role> roles = new List<Role>();

            foreach (RoleFormStub m in model.Roles)
            {
                Role role = new Role();

                m.MapDbObject(role);
                roles.Add(role);
            }

            await RepoUser.RevokeRoleAsync(user, roles);

            return Json(response);
        }

        public async Task<JsonResult> UserNameExists(string UserName, string CurrentUserName)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(UserName))
            {
                exist = true;
            }
            else if (UserName.ToLower() == CurrentUserName.ToLower())
            {
                exist = false;
            }
            else
            {
                FilterQuery filter = new FilterQuery(
                    Field.UserName,
                    FilterOperator.Equals,
                    UserName
                );
                User user = await RepoUser.FindAsync(filter);

                exist = user != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> EmailExists(string Email, string CurrentEmail)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(Email))
            {
                exist = true;
            }
            else if (Email.ToLower() == CurrentEmail.ToLower())
            {
                exist = false;
            }
            else
            {
                FilterQuery filter = new FilterQuery(
                    new FieldHelper(Table.Membership, Field.Email),
                    FilterOperator.Equals,
                    Email
                );
                User user = await RepoUser.FindAsync(filter);

                exist = user != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public async Task<ActionResult> CreateSignatureFile(Guid userId, string userName, string filePath)
		{
			//kamus
			string template, message;
			ResponseModel response = new ResponseModel(true);

			Business.Entities.Membership membership = await RepoMembership.FindByPrimaryKeyAsync(userId);

			//update user
			membership.SignatureFilePath = filePath;
			if (await RepoMembership.SaveAsync(membership))
			{
				template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
			}
			else
			{
				template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditFailed).ToString();
				response = new ResponseModel(false);
			}
			message = string.Format(template, userName);
			response.Message = message;

			return Json(response);
		}

		[HttpPost]
		public async Task<ActionResult> GetUserInfo(string employeeNo)
		{
            //lib
            Business.Entities.Membership membership = new Business.Entities.Membership();

            //algo
            if (!string.IsNullOrEmpty(employeeNo))
            {
                FilterQuery filter = new FilterQuery(Field.EmployeeNo, FilterOperator.Equals, employeeNo);
                membership = await RepoMembership.FindAsync(filter);
            }

			return Json(new { Success = true, data = new { FullName = membership.FullName, SignatureFilePath = membership.SignatureFilePath, EmployeeNo = membership.EmployeeNo, Position = membership.Position } });
		}

		#region Binding

		public async Task<JsonResult> BindingUser(params object[] args)
        {
            //lib
            int count;
            List<ViewUser> dbObjects;
            List<Area> areas;
            List<Role> roles;
            List<UserPresentationStub> models;
            Task<List<ViewUser>> dbObjectsTask = null;
            Task<List<Area>> areasTask;
            Task<List<Role>> rolesTask;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoUserArea.FindAllAsync();
            countTask = RepoUserArea.CountAsync();
            areasTask = RepoArea.FindAllAsync();
            rolesTask = RepoRole.FindAllAsync();

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask, areasTask, rolesTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;
            areas = areasTask.Result;
            roles = rolesTask.Result;

            //map db data to model            
            models = UserPresentationStub.MapList(dbObjects, areas, roles);
            //return JsonConvert.SerializeObject(new { total = count, data = models });

            return Json(models);
        }

        public async Task<string> BindingRoles()
        {
            //lib
            List<Role> dbObjects;
            List<RolePresentationStub> models;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjects = await RepoRole.FindAllAsync(param.Sorts, param.Filter);
            models = ListMapper.MapList<Role, RolePresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { data = models });
        }

        public async Task<string> BindingUsers()
        {
            QueryRequestParameter param = QueryRequestParameter.Current;
            List<ActiveDirectoryUtil> models = new List<ActiveDirectoryUtil>();
            string key;

            if (param != null && param.Filter != null && param.Filter.Filters.Any())
            {
                var filter = param.Filter.Filters.FirstOrDefault(n => n.Field == Field.UserName);
                key = filter != null ? filter.Value.ToString() : string.Empty;
                models = ActiveDirectoryUtil.GetUsers(key);
            }

            await Task.Delay(0);

            return JsonConvert.SerializeObject(new { data = models });
        }

        public async Task<string> BindingArea(params object[] args)
        {
            //lib
            List<Area> dbObjects;
            List<AreaPresentationStub> models = new List<AreaPresentationStub>();

            //algorithm
            //get callback from task
            dbObjects = await RepoArea.FindAllAsync();

            if (dbObjects != null)
            {
                models = ListMapper.MapList<Area, AreaPresentationStub>(dbObjects);
            }
            return JsonConvert.SerializeObject(models);
        }

        #endregion
    }
}
