using Business.Abstract;
using Common.Enums;
using SecurityGuard.Interfaces;
using SecurityGuard.Services;
using WebUI.Areas.UserManagement.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using WebUI.Controllers;
using WebUI.Infrastructure;
using System.Linq;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.UserManagement.Controllers
{
    [AuthorizeUser(ModuleName = UserModule.UserManagement)]
    [CheckSessionTimeOut]
    public partial class RoleController : BaseController<ManageRolesViewModel>
    {
        private IRoleService RoleService { get; set; }
        private IModuleRepository RepoModule { get; set; }
        private IActionRepository RepoAction { get; set; }
        private IRoleRepository RepoRole { get; set; }
        private IModulesInRoleRepository RepoModulesInRole { get; set; }

        public RoleController(
            IModuleRepository repoParam, 
            IActionRepository repoActionParam, 
            IRoleRepository repoRole, 
            IModulesInRoleRepository repoModulesInRole
        )
        {
            RoleService = new RoleService(Roles.Provider);
            
            RepoModule = repoParam;
            RepoAction = repoActionParam;
            RepoRole = repoRole;
            RepoModulesInRole = repoModulesInRole;
        }

        public virtual ActionResult Index()
        {
            ManageRolesViewModel model = new ManageRolesViewModel();
            model.Roles = new SelectList(RoleService.GetAllRoles());
            model.RoleList = RoleService.GetAllRoles();

            return View(model);
        }

        #region Create Roles Methods

        [HttpGet]
        public virtual ActionResult CreateRole()
        {
            return View(new RoleViewModel());
        }

        [HttpPost]
        public virtual ActionResult CreateRole(string roleName)
        {
            JsonResponse response = new JsonResponse();

            if (string.IsNullOrEmpty(roleName))
            {
                response.Success = false;
                response.Message = "You must enter a role name.";
                response.CssClass = "red";

                return Json(response);
            }

            try
            {
                RoleService.CreateRole(roleName);

                if (Request.IsAjaxRequest())
                {
                    response.Success = true;
                    response.Message = "Role created successfully!";
                    response.CssClass = "green";

                    return Json(response);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (Request.IsAjaxRequest())
                {
                    response.Success = false;
                    response.Message = ex.Message;
                    response.CssClass = "red";

                    return Json(response);
                }

                ModelState.AddModelError("", ex.Message);
            }

            return RedirectToAction("Index");
        }       

        #endregion

        #region Delete Roles Methods

        /// <summary>
        /// This is an Ajax method.
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult DeleteRole(string roleName)
        {
            JsonResponse response = new JsonResponse();

            if (string.IsNullOrEmpty(roleName))
            {
                response.Success = false;
                response.Message = "You must select a Role Name to delete.";
                response.CssClass = "red";

                return Json(response);
            }

            RoleService.DeleteRole(roleName);

            response.Success = true;
            response.Message = roleName + " was deleted successfully!";
            response.CssClass = "green";

            return Json(response);
        }

        [HttpPost]
        public ActionResult DeleteRoles(string roles, bool throwOnPopulatedRole)
        {
            JsonResponse response = new JsonResponse();
            response.Messages = new List<ResponseItem>();

            if (string.IsNullOrEmpty(roles))
            {
                response.Success = false;
                response.Message = "You must select at least one role.";
                return Json(response);
            }

            string[] roleNames = roles.Split(',');
            StringBuilder sb = new StringBuilder();

            ResponseItem item = null;

            foreach (var role in roleNames)
            {
                if (!string.IsNullOrEmpty(role))
                {
                    try
                    {
                        RoleService.DeleteRole(role, throwOnPopulatedRole);

                        item = new ResponseItem();
                        item.Success = true;
                        item.Message = "Deleted this role successfully - " + role;
                        item.CssClass = "green";
                        response.Messages.Add(item);

                        //sb.AppendLine("Deleted this role successfully - " + role + "<br />");
                    }
                    catch (System.Configuration.Provider.ProviderException ex)
                    {
                        //sb.AppendLine(role + " - " + ex.Message + "<br />");

                        item = new ResponseItem();
                        item.Success = false;
                        item.Message = ex.Message;
                        item.CssClass = "yellow";
                        response.Messages.Add(item);
                    }
                }
            }

            response.Success = true;
            response.Message = sb.ToString();

            return Json(response);
        }

        #endregion

        #region Get Users In Role methods

        /// <summary>
        /// This is an Ajax method that populates the 
        /// Roles drop down list.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAllRoles()
        {
            var list = RoleService.GetAllRoles();

            List<SelectObject> selectList = new List<SelectObject>();

            foreach (var item in list)
            {
                selectList.Add(new SelectObject() { caption = item, value = item });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetUsersInRole(string roleName)
        {
            var list = RoleService.GetUsersInRole(roleName);

            return Json(list, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region add Modules to Role

        /// <summary>
        /// Return two lists:
        ///   1)  a list of Actions not in module
        ///   2)  a list of Actions in module
        /// </summary>
        /// <param name="moduleName"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        //[MvcSiteMapNode(Title = "Add Action to Modules", ParentKey = "ManageModules", Key = "AddActiontoModules")]
        //[SiteMapTitle("Breadcrumb")]
        public virtual ActionResult AddModule(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                return RedirectToAction("Index");
            }

            GrantModulesToRoleViewModel model = new GrantModulesToRoleViewModel();
            model.RoleName = roleName;
            model.GUID = RepoRole.FindByName(roleName).RoleId.ToString();
            string actionId = RepoAction.Find().ActionId.ToString();

            List<Business.Entities.Module> availableModules = RepoModule.Find();
            List<Business.Entities.Module> usedModules = RepoRole.FindByName(roleName).ModulesInRoles.Select(m => m.Module).ToList();

            //used action
            foreach (Business.Entities.Module a in usedModules)
            {
                availableModules.RemoveAll(x => x.ModuleName == a.ModuleName);
            }

            List<ModuleStub> availableModuleStub = new ModuleStub().MapList(availableModules, actionId);
            List<ModuleStub> usedModuleStub = new ModuleStub().MapList(usedModules, actionId);
            model.AvailableModules = new SelectList(availableModuleStub, "Id", "ModuleName");
            model.GrantedModules = new SelectList(usedModuleStub, "Id", "ModuleName");

            return View(model);
        }

        public virtual ActionResult GrantModulesToRole(string RoleId, string Modules)
        {
            JsonResponse response = new JsonResponse();
            response.Messages = new List<ResponseItem>();

            string[] moduleIds = Modules.Split(',');
            StringBuilder sb = new StringBuilder();

            ResponseItem item = null;

            try
            {
                RepoRole.AddModuleAndAction(moduleIds, RoleId);

                item = new ResponseItem();
                item.Success = true;
                response.Message = "modules was added successfully!";
                response.CssClass = "green";
                response.Messages.Add(item);
            }
            catch (Exception ex)
            {
                item = new ResponseItem();
                item.Success = false;
                response.Success = false;
                response.Message = ex.Message;
                response.CssClass = "red";
                response.Messages.Add(item);
            }
            return Json(response);
        }

        public virtual ActionResult ViewRoleModule()
        {
            ViewBag.modules = RepoModule.Find();
            ViewBag.actions = RepoAction.Find();
            return PartialView();
        }

        public virtual ActionResult RevokeModulesForRole(string RoleId, string Modules)
        {
            //JsonResponse response = new JsonResponse();
            //response.Messages = new List<ResponseItem>();

            //string[] moduleIds = Modules.Split(',');
            //StringBuilder sb = new StringBuilder();

            //ResponseItem item = null;

            //foreach (string s in moduleIds)
            //{
            //    if (!string.IsNullOrWhiteSpace(s))
            //    {
            //        try
            //        {
            //            //remove action in repo
            //            repoModulesInRole.DeleteByModule(new Guid(s));                        

            //            ////remove action in ModulesInRole
            //            //modulesInRoleRepo.RemoveAction(new Guid(moduleId), new Guid(s));

            //            ////remove all empty actions in ModulesInRole
            //            //modulesInRoleRepo.DeleteByModule(new Guid(moduleId));

            //            item = new ResponseItem();
            //            item.Success = true;
            //            response.Message = moduleRepo.FindByPk(new Guid(s)).ModuleName + " was removed successfully!";
            //            response.CssClass = "green";
            //            response.Messages.Add(item);
            //        }
            //        catch (Exception ex)
            //        {
            //            item = new ResponseItem();
            //            item.Success = false;
            //            response.Success = false;
            //            response.Message = ex.Message;
            //            response.CssClass = "red";
            //            response.Messages.Add(item);
            //        }
            //    }
            //}

            //return Json(response);
            JsonResponse response = new JsonResponse();
            response.Messages = new List<ResponseItem>();

            string[] moduleIds = Modules.Split(',');
            StringBuilder sb = new StringBuilder();

            ResponseItem item = null;

            try
            {
                RepoRole.AddModuleAndAction(moduleIds, RoleId);

                item = new ResponseItem();
                item.Success = true;
                response.Message = "modules was added successfully!";
                response.CssClass = "green";
                response.Messages.Add(item);
            }
            catch (Exception ex)
            {
                item = new ResponseItem();
                item.Success = false;
                if (moduleIds == null)
                {
                    item = new ResponseItem();
                    item.Success = true;
                    response.Message = "modules was added successfully!";
                    response.CssClass = "green";
                    response.Messages.Add(item);
                }
                else
                {
                    response.Success = false;
                    response.Message = ex.Message;
                    response.CssClass = "red";
                    response.Messages.Add(item);
                }

            }
            return Json(response);
        }
        #endregion
    }
}
