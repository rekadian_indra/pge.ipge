﻿namespace WebUI.Areas.Service.Models
{
    public class ServiceObjectStub<T>
    {
        public T Data { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}