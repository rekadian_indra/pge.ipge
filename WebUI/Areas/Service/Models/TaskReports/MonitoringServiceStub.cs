﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Areas.Service.Models.TaskReports
{
    public class MonitoringServiceStub
    {
        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? WellId { get; set; }
        public string WellName { get; set; }
        public double? Tks { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public string TksUnit { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }
        #endregion

        #region "Contructor"
        public MonitoringServiceStub()
        {

        }

        public MonitoringServiceStub(Monitoring dbObject)
        {
            ObjectMapper.MapObject<Monitoring, MonitoringServiceStub>(dbObject, this);
            WellName = dbObject.Well?.Name;
        }
        #endregion

        #region "Helper"
        public void MapDbObject(Monitoring dbObject)
        {
            dbObject.WellId = WellId;
            dbObject.Tks = Tks;
            dbObject.Status = Status;
            dbObject.Notes = Notes;
            dbObject.TksUnit = TksUnit;

            if (dbObject.Id > 0)
            {
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
            }
            else
            {
                dbObject.CreatedBy = CreatedBy;
                dbObject.CreatedDateTimeUtc = DateTime.Now;
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
                dbObject.IsDeleted = false;
            }
        }
        #endregion
    }
}