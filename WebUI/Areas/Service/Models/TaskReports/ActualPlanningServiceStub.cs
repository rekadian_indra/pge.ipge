﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Areas.Service.Models.TaskReports
{
    public class ActualPlanningServiceStub
    {
        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? AreaId { get; set; }
        public string AreaName { get; set; }
        public string Planning { get; set; }
        public string Actual { get; set; }
        public string ActualCase { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }
        #endregion

        #region "Contructor"
        public ActualPlanningServiceStub()
        {

        }

        public ActualPlanningServiceStub(ActualPlanning dbObject)
        {
            ObjectMapper.MapObject<ActualPlanning, ActualPlanningServiceStub>(dbObject, this);
            AreaName = dbObject.Area?.Name;
        }
        #endregion

        #region "Helper"
        public void MapDbObject(ActualPlanning dbObject)
        {
            dbObject.AreaId = AreaId;
            dbObject.Planning = Planning;
            dbObject.Actual = Actual;
            dbObject.ActualCase = ActualCase;

            if (this.Id > 0)
            {
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
            }
            else
            {
                dbObject.CreatedBy = CreatedBy;
                dbObject.CreatedDateTimeUtc = DateTime.Now;
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
                dbObject.IsDeleted = false;
            }
        }
        #endregion
    }
}