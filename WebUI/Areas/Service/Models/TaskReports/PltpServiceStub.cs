﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;

namespace WebUI.Areas.Service.Models.TaskReports
{
    public class PltpServiceStub
    {
        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? PowerPlantId { get; set; }
        public string PowerPlantName { get; set; }
        public double? EnergyGross { get; set; }
        public double? EnergyNett { get; set; }
        public double? SteamUtilization { get; set; }
        public double? WasteRockMufller { get; set; }
        public double? Ssc { get; set; }
        public double? GppsShutdown { get; set; }
        public double? ImportEnergy { get; set; }
        public double? SgssShutdown { get; set; }
        public string Notes { get; set; }
        public string EnergyGrossUnit { get; set; }
        public string EnergyNettUnit { get; set; }
        public string SteamUtilizationUnit { get; set; }
        public string WasteRockMufllerUnit { get; set; }
        public string SscUnit { get; set; }
        public string GppsShutdownUnit { get; set; }
        public string ImportEnergyUnit { get; set; }
        public string SgssShutdownUnit { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }
        #endregion

        #region "Contructor"
        public PltpServiceStub()
        {
        }

        public PltpServiceStub(Pltp dbObject)
        {
            ObjectMapper.MapObject<Pltp, PltpServiceStub>(dbObject, this);
            PowerPlantName = dbObject.PowerPlant?.Name;
        }
        #endregion

        #region "Helper"
        public void MapDbObject(Pltp dbObject)
        {
            dbObject.PowerPlantId = PowerPlantId;
            dbObject.EnergyGross = EnergyGross;
            dbObject.EnergyNett = EnergyNett;
            dbObject.SteamUtilization = SteamUtilization;
            dbObject.WasteRockMufller = WasteRockMufller;
            dbObject.Ssc = Ssc;
            dbObject.GppsShutdown = GppsShutdown;
            dbObject.ImportEnergy = ImportEnergy;
            dbObject.SgssShutdown = SgssShutdown;
            dbObject.Notes = Notes;
            dbObject.EnergyGrossUnit = EnergyUnit.KW_HR.ToString();
            dbObject.EnergyNettUnit = EnergyUnit.KW_HR.ToString();
            dbObject.SteamUtilizationUnit = SteamUtilizationUnit;
            dbObject.WasteRockMufllerUnit = WasteRockMufllerUnit;
            dbObject.SscUnit = SscUnit;
            dbObject.GppsShutdownUnit = GppsShutdownUnit;
            dbObject.ImportEnergyUnit = EnergyUnit.KW_HR.ToString();
            dbObject.SgssShutdownUnit = SgssShutdownUnit;

            if (dbObject.Id > 0)
            {
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
            }
            else
            {
                dbObject.CreatedBy = CreatedBy;
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.CreatedDateTimeUtc = DateTime.Now;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
                dbObject.IsDeleted = false;
            }
        }
        #endregion
    }
}