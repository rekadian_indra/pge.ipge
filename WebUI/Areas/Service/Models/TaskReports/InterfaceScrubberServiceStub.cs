﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Areas.Service.Models.TaskReports
{
    public class InterfaceScrubberServiceStub
    {
        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? InterfaceId { get; set; }
        public string InterfaceName { get; set; }
        public int? ScrubberId { get; set; }
        public string ScrubberName { get; set; }
        public double? InterfacePressure { get; set; }
        public double? InterfaceTemperature { get; set; }
        public double? ScrubberPressure { get; set; }
        public double? ScrubberTemperature { get; set; }
        public string InterfacePressureUnit { get; set; }
        public string InterfaceTemperatureUnit { get; set; }
        public string ScrubberPressureUnit { get; set; }
        public string ScrubberTemperatureUnit { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }
        #endregion

        #region "Contructor"
        public InterfaceScrubberServiceStub()
        {

        }

        public InterfaceScrubberServiceStub(InterfaceScrubber dbObject)
        {
            ObjectMapper.MapObject<InterfaceScrubber, InterfaceScrubberServiceStub>(dbObject, this);
            InterfaceName = dbObject.InterfacePoint?.Name;
            ScrubberName = dbObject.Scrubber?.Name;
        }
        #endregion

        #region "Helper"
        public void MapDbObject(InterfaceScrubber dbObject)
        {
            dbObject.InterfaceId = InterfaceId;
            dbObject.ScrubberId = ScrubberId;
            dbObject.InterfacePressure = InterfacePressure;
            dbObject.InterfaceTemperature = InterfaceTemperature;
            dbObject.ScrubberPressure = ScrubberPressure;
            dbObject.ScrubberTemperature = ScrubberTemperature;
            dbObject.InterfacePressureUnit = InterfacePressureUnit;
            dbObject.InterfaceTemperatureUnit = InterfaceTemperatureUnit;
            dbObject.ScrubberPressureUnit = ScrubberPressureUnit;
            dbObject.ScrubberTemperatureUnit = ScrubberTemperatureUnit;

            if (this.Id > 0)
            {
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
            }
            else
            {
                dbObject.CreatedBy = CreatedBy;
                dbObject.CreatedDateTimeUtc = DateTime.Now;
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
                dbObject.IsDeleted = false;
            }
        }
        #endregion
    }
}