﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebUI.Areas.Service.Models.TaskReports
{
    public class TaskReportRequestStub
    {
        #region "Properties"
        public int Id { get; set; }
        public int? AreaId { get; set; }
        public string AreaName { get; set; }
        public DateTime? Periode { get; set; }
        public string SourceInput { get; set; }
        public string Status { get; set; }
        public string ApprovedBy { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }

        public List<PltpServiceStub> Pltps { get; set; }
        public List<DkpServiceStub> Dkps { get; set; }
        public List<ProductionServiceStub> Productions { get; set; }
        public List<ReinjectionServiceStub> Reinjections { get; set; }
        public List<MonitoringServiceStub> Monitorings { get; set; }
        public List<SeparatorServiceStub> Separators { get; set; }
        public List<InterfaceScrubberServiceStub> InterfaceScrubbers { get; set; }
        public List<ActualPlanningServiceStub> ActualPlannings { get; set; }
        #endregion

        public TaskReportRequestStub(TaskReport dbObject)
        {
            ObjectMapper.MapObject<TaskReport, TaskReportRequestStub>(dbObject, this);

            AreaName = dbObject.Area.Name;

            if (dbObject.Pltps.Any())
            {
                Pltps = ListMapper.MapList<Pltp, PltpServiceStub>(dbObject.Pltps.Where(x => x.IsDeleted == false));
            }

            if (dbObject.Dkps.Any())
            {
                Dkps = ListMapper.MapList<Dkp, DkpServiceStub>(dbObject.Dkps.Where(x => x.IsDeleted == false));
            }

            if (dbObject.Productions.Any())
            {
                Productions = ListMapper.MapList<Production, ProductionServiceStub>(dbObject.Productions.Where(x => x.IsDeleted == false));
            }

            if (dbObject.ReInjections.Any())
            {
                Reinjections = ListMapper.MapList<ReInjection, ReinjectionServiceStub>(dbObject.ReInjections.Where(x => x.IsDeleted == false));
            }

            if (dbObject.Monitorings.Any())
            {
                Monitorings = ListMapper.MapList<Monitoring, MonitoringServiceStub>(dbObject.Monitorings.Where(x => x.IsDeleted == false));
            }

            if (dbObject.Separators.Any())
            {
                Separators = ListMapper.MapList<Separator, SeparatorServiceStub>(dbObject.Separators.Where(x => x.IsDeleted == false));
            }

            if (dbObject.InterfaceScrubbers.Any())
            {
                InterfaceScrubbers = ListMapper.MapList<InterfaceScrubber, InterfaceScrubberServiceStub>(dbObject.InterfaceScrubbers.Where(x => x.IsDeleted == false));
            }

            if (dbObject.ActualPlannings.Any())
            {
                ActualPlannings = ListMapper.MapList<ActualPlanning, ActualPlanningServiceStub>(dbObject.ActualPlannings.Where(x => x.IsDeleted == false));
            }
        }
    }
}