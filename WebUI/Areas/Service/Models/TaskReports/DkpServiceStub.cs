﻿using Business.Entities;
using Business.Infrastructure;
using System;

namespace WebUI.Areas.Service.Models.TaskReports
{
    public class DkpServiceStub
    {
        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? PowerPlantId { get; set; }
        public string PowerPlantName { get; set; }
        public string Status { get; set; }
        public TimeSpan? StartHour { get; set; }
        public TimeSpan? EndHour { get; set; }
        public double? Derating { get; set; }
        public string Location { get; set; }
        public string Cause { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }
        #endregion

        #region "Contructor"
        public DkpServiceStub()
        {

        }

        public DkpServiceStub(Dkp dbObject)
        {
            ObjectMapper.MapObject<Dkp, DkpServiceStub>(dbObject, this);
            PowerPlantName = dbObject.PowerPlant?.Name;
        }
        #endregion

        #region "Helper"
        public void MapDbObject(Dkp dbObject)
        {
            dbObject.PowerPlantId = PowerPlantId;
            dbObject.Status = Status;
            dbObject.StartHour = StartHour;
            dbObject.EndHour = EndHour;
            dbObject.Derating = Derating;
            dbObject.Location = Location;
            dbObject.Cause = Cause;
            dbObject.Notes = Notes;

            if (dbObject.Id > 0)
            {
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
            }
            else
            {
                dbObject.CreatedBy = CreatedBy;
                dbObject.CreatedDateTimeUtc = DateTime.Now;
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
                dbObject.IsDeleted = false;
            }
        }
        #endregion
    }
}