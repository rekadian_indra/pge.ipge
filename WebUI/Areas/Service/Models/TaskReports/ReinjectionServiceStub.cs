﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Areas.Service.Models.TaskReports
{
    public class ReinjectionServiceStub
    {
        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? WellId { get; set; }
        public string WellName { get; set; }
        public double? Tks { get; set; }
        public double? PressureLine { get; set; }
        public double? Temperature { get; set; }
        public double? Ph { get; set; }
        public double? FlowRate { get; set; }
        public double? FlowTotal { get; set; }
        public string Fluida { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public string TksUnit { get; set; }
        public string PressureLineUnit { get; set; }
        public string TemperatureUnit { get; set; }
        public string FlowRateUnit { get; set; }
        public string FlowTotalUnit { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }
        #endregion

        #region "Contructor"
        public ReinjectionServiceStub()
        {

        }

        public ReinjectionServiceStub(ReInjection dbObject)
        {
            ObjectMapper.MapObject<ReInjection, ReinjectionServiceStub>(dbObject, this);
            WellName = dbObject.Well?.Name;
        }
        #endregion

        #region "Helper"
        public void MapDbObject(ReInjection dbObject)
        {
            dbObject.WellId = WellId;
            dbObject.Tks = Tks;
            dbObject.PressureLine = PressureLine;
            dbObject.Temperature = Temperature;
            dbObject.Ph = Ph;
            dbObject.FlowRate = FlowRate;
            dbObject.FlowTotal = FlowTotal;
            dbObject.Fluida = Fluida;
            dbObject.Status = Status;
            dbObject.Notes = Notes;
            dbObject.TksUnit = TksUnit;
            dbObject.PressureLineUnit = PressureLineUnit;
            dbObject.TemperatureUnit = TemperatureUnit;
            dbObject.FlowRateUnit = FlowRateUnit;
            dbObject.FlowTotalUnit = FlowTotalUnit;

            if (dbObject.Id > 0)
            {
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
            }
            else
            {
                dbObject.CreatedBy = CreatedBy;
                dbObject.CreatedDateTimeUtc = DateTime.Now;
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
                dbObject.IsDeleted = false;
            }
        }
        #endregion
    }
}