﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Areas.Service.Models.TaskReports
{
    public class SeparatorServiceStub
    {
        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? SeparatorId { get; set; }
        public string SeparatorName { get; set; }
        public double? Pressure { get; set; }
        public double? Temperature { get; set; }
        public double? LevelBrine { get; set; }
        public double? ThrottleValveBrine { get; set; }
        public double? ThrottleValveSteam { get; set; }
        public double? SteamFlowRate { get; set; }
        public double? BrineFlowRate { get; set; }
        public string PressureUnit { get; set; }
        public string TemperatureUnit { get; set; }
        public string LevelBrineUnit { get; set; }
        public string ThrottleValveBrineUnit { get; set; }
        public string ThrottleValveSteamUnit { get; set; }
        public string SteamFlowRateUnit { get; set; }
        public string BrineFlowRateUnit { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }
        #endregion

        #region "Contructor"
        public SeparatorServiceStub()
        {

        }

        public SeparatorServiceStub(Separator dbObject)
        {
            ObjectMapper.MapObject<Separator, SeparatorServiceStub>(dbObject, this);
            SeparatorName = dbObject.MstSeparator?.Name;
        }
        #endregion

        #region "Helper"
        public void MapDbObject(Separator dbObject)
        {
            dbObject.SeparatorId = SeparatorId;
            dbObject.Pressure = Pressure;
            dbObject.Temperature = Temperature;
            dbObject.LevelBrine = LevelBrine;
            dbObject.ThrottleValveBrine = ThrottleValveBrine;
            dbObject.ThrottleValveSteam = ThrottleValveSteam;
            dbObject.SteamFlowRate = SteamFlowRate;
            dbObject.BrineFlowRate = BrineFlowRate;
            dbObject.PressureUnit = PressureUnit;
            dbObject.TemperatureUnit = TemperatureUnit;
            dbObject.LevelBrineUnit = LevelBrineUnit;
            dbObject.ThrottleValveBrineUnit = ThrottleValveBrineUnit;
            dbObject.ThrottleValveSteamUnit = ThrottleValveSteamUnit;
            dbObject.SteamFlowRateUnit = SteamFlowRateUnit;
            dbObject.BrineFlowRateUnit = BrineFlowRateUnit;

            if (this.Id > 0)
            {
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
            }
            else
            {
                dbObject.CreatedBy = CreatedBy;
                dbObject.CreatedDateTimeUtc = DateTime.Now;
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
                dbObject.IsDeleted = false;
            }
        }
        #endregion
    }
}