﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using WebUI.Areas.Services.Models;
using WebUI.Models;

namespace WebUI.Areas.Service.Models.TaskReports
{
    public class TaskReportServiceStub/* : BaseFormStub<TaskReport, TaskReportServiceStub>*/
    {
        #region "Properties"
        public int Id { get; set; }
        public int? AreaId { get; set; }
        public DateTime? Periode { get; set; }
        public string SourceInput { get; set; }
        public string Status { get; set; }
        public string ApprovedBy { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }

        public List<PltpServiceStub> Pltps { get; set; }
        public List<DkpServiceStub> Dkps { get; set; }
        public List<ProductionServiceStub> Productions { get; set; }
        public List<ReinjectionServiceStub> Reinjections { get; set; }
        public List<MonitoringServiceStub> Monitorings { get; set; }
        public List<SeparatorServiceStub> Separators { get; set; }
        public List<InterfaceScrubberServiceStub> InterfaceScrubbers { get; set; }
        public List<ActualPlanningServiceStub> ActualPlannings { get; set; }

        //USER
        public UserServiceStub User { get; set; }     
        #endregion

        #region "Constructor"
        public TaskReportServiceStub()
        {
            Pltps = new List<PltpServiceStub>();
            Dkps = new List<DkpServiceStub>();
            Productions = new List<ProductionServiceStub>();
            Reinjections = new List<ReinjectionServiceStub>();
            Monitorings = new List<MonitoringServiceStub>();
            Separators = new List<SeparatorServiceStub>();
            InterfaceScrubbers = new List<InterfaceScrubberServiceStub>();
            ActualPlannings = new List<ActualPlanningServiceStub>();
        }
        public TaskReportServiceStub(TaskReport dbObject)
        {
            ObjectMapper.MapObject<TaskReport, TaskReportServiceStub>(dbObject, this);

            if (dbObject.Pltps.Any())
            {
                Pltps = ListMapper.MapList<Pltp, PltpServiceStub>(dbObject.Pltps);
            }

            if (dbObject.Dkps.Any())
            {
                Dkps = ListMapper.MapList<Dkp, DkpServiceStub>(dbObject.Dkps);
            }

            if (dbObject.Productions.Any())
            {
                Productions = ListMapper.MapList<Production, ProductionServiceStub>(dbObject.Productions);
            }

            if (dbObject.ReInjections.Any())
            {
                Reinjections = ListMapper.MapList<ReInjection, ReinjectionServiceStub>(dbObject.ReInjections);
            }

            if (dbObject.Monitorings.Any())
            {
                Monitorings = ListMapper.MapList<Monitoring, MonitoringServiceStub>(dbObject.Monitorings);
            }

            if (dbObject.Separators.Any())
            {
                Separators = ListMapper.MapList<Separator, SeparatorServiceStub>(dbObject.Separators);
            }

            if (dbObject.InterfaceScrubbers.Any())
            {
                InterfaceScrubbers = ListMapper.MapList<InterfaceScrubber, InterfaceScrubberServiceStub>(dbObject.InterfaceScrubbers);
            }

            if (dbObject.ActualPlannings.Any())
            {
                ActualPlannings = ListMapper.MapList<ActualPlanning, ActualPlanningServiceStub>(dbObject.ActualPlannings);
            }
        }

        #endregion

        #region "Helper"
        public void MapDbObject(TaskReport dbObject)
        {
            if (dbObject.Id == 0)
            {
                dbObject.AreaId = AreaId;
                dbObject.Periode = Periode;
                dbObject.SourceInput = InputSource.MOBILE.ToString();
                dbObject.Status = DataStatus.WAITING.ToString();
                dbObject.ApprovedBy = null;
                dbObject.CreatedBy = CreatedBy;
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.CreatedDateTimeUtc = DateTime.Now;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
                dbObject.IsDeleted = false;
            }
            else
            {
                dbObject.Status = Status;
                dbObject.ApprovedBy = ApprovedBy;
                dbObject.ModifiedBy = ModifiedBy;
                dbObject.ModifiedDateTimeUtc = DateTime.Now;
            }

            if (Pltps.Any())
            {
                foreach (PltpServiceStub m in Pltps)
                {
                    Pltp pltp = dbObject.Pltps.FirstOrDefault(x => x.PowerPlantId == m.PowerPlantId && x.IsDeleted == false);

                    if (pltp != null)
                    {
                        m.MapDbObject(pltp);
                    }
                    else
                    {
                        pltp = new Pltp();
                        m.MapDbObject(pltp);

                        dbObject.Pltps.Add(pltp);
                    }
                }
            }

            if (Dkps.Any())
            {
                foreach (DkpServiceStub m in Dkps)
                {
                    Dkp dkp = dbObject.Dkps.FirstOrDefault(x => x.PowerPlantId == m.PowerPlantId && x.IsDeleted == false);

                    if (dkp != null)
                    {
                        m.MapDbObject(dkp);
                    }
                    else
                    {
                        dkp = new Dkp();
                        m.MapDbObject(dkp);

                        dbObject.Dkps.Add(dkp);
                    }
                }
            }

            if (Productions.Any())
            {
                foreach (ProductionServiceStub m in Productions)
                {
                    Production production = dbObject.Productions.FirstOrDefault(x => x.WellId == m.WellId && x.IsDeleted == false);

                    if (production != null)
                    {
                        m.MapDbObject(production);
                    }
                    else
                    {
                        production = new Production();
                        m.MapDbObject(production);

                        dbObject.Productions.Add(production);
                    }
                }
            }

            if (Reinjections.Any())
            {
                foreach (ReinjectionServiceStub m in Reinjections)
                {
                    ReInjection reInjection = dbObject.ReInjections.FirstOrDefault(x => x.WellId == m.WellId && x.IsDeleted == false);

                    if (reInjection != null)
                    {
                        m.MapDbObject(reInjection);
                    }
                    else
                    {
                        reInjection = new ReInjection();
                        m.MapDbObject(reInjection);

                        dbObject.ReInjections.Add(reInjection);
                    }
                }
            }

            if (Monitorings.Any())
            {
                foreach (MonitoringServiceStub m in Monitorings)
                {
                    Monitoring monitoring = dbObject.Monitorings.FirstOrDefault(x => x.WellId == m.WellId && x.IsDeleted == false);

                    if (monitoring != null)
                    {
                        m.MapDbObject(monitoring);
                    }
                    else
                    {
                        monitoring = new Monitoring();
                        m.MapDbObject(monitoring);

                        dbObject.Monitorings.Add(monitoring);
                    }
                }
            }

            if (Separators.Any())
            {
                foreach (SeparatorServiceStub m in Separators)
                {
                    Separator separator = dbObject.Separators.FirstOrDefault(x => x.SeparatorId == m.SeparatorId && x.IsDeleted == false);

                    if (separator != null)
                    {
                        m.MapDbObject(separator);
                    }
                    else
                    {
                        separator = new Separator();
                        m.MapDbObject(separator);

                        dbObject.Separators.Add(separator);
                    }
                }
            }

            if (InterfaceScrubbers.Any())
            {
                foreach (InterfaceScrubberServiceStub m in InterfaceScrubbers)
                {
                    InterfaceScrubber interfaceScrubber = dbObject.InterfaceScrubbers.FirstOrDefault(x => x.InterfaceId == m.InterfaceId && x.IsDeleted == false);

                    if (interfaceScrubber != null)
                    {
                        m.MapDbObject(interfaceScrubber);
                    }
                    else
                    {
                        interfaceScrubber = new InterfaceScrubber();
                        m.MapDbObject(interfaceScrubber);

                        dbObject.InterfaceScrubbers.Add(interfaceScrubber);
                    }
                }
            }

            if (ActualPlannings.Any())
            {
                foreach (ActualPlanningServiceStub m in ActualPlannings)
                {
                    ActualPlanning actualPlanning = dbObject.ActualPlannings.FirstOrDefault(x => x.Planning == m.Planning && x.IsDeleted == false);

                    if (actualPlanning != null)
                    {
                        m.MapDbObject(actualPlanning);
                    }
                    else
                    {
                        actualPlanning = new ActualPlanning();
                        m.MapDbObject(actualPlanning);

                        dbObject.ActualPlannings.Add(actualPlanning);
                    }
                }
            }
        }
        #endregion
    }
}