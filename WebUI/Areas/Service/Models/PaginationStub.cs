﻿using Business.Infrastructure;
using Common.Enums;
using System.Collections.Generic;
using System.Linq;

namespace WebUI.Areas.Service.Models
{
    public class PaginationStub
    {
        public int Skip { get; set; }
        public int Take { get; set; }
        public List<SortingHelper> Sorts { get; set; }
        public List<FilterHelper> Filters { get; set; }
    }

    public class SortingHelper
    {
        public Field? Field { get; set; }
        public FieldHelper RelationField { get; set; }
        public SortOrder? Direction { get; set; }

        public static List<SortQuery> MapList(IEnumerable<SortingHelper> sortings)
        {
            var result = new List<SortQuery>();

            foreach (var m in sortings)
            {
                result.Add(new SortQuery(m.Field, m.Direction));
            }

            return result;
        }
    }

    public class FilterHelper
    {
        public FilterOperator? Operator { get; set; }
        public Field? Field { get; set; }
        public FieldHelper RelationField { get; set; }
        public object Value { get; set; }
        public List<FilterQuery> Filters { get; set; }
        public FilterLogic? Logic { get; set; }


        public static List<FilterQuery> MapList(IEnumerable<FilterHelper> filters)
        {
            var result = new List<FilterQuery>();

            foreach (var m in filters)
            {
                FilterQuery f = new FilterQuery
                {
                    Field = m.Field,
                    Operator = m.Operator,
                    Value = m.Value,
                    Logic = m.Logic
                };

                if (m.Filters != null && m.Filters.Any())
                {
                    f.Filters = new List<FilterQuery>();
                    foreach(var n in m.Filters)
                    {
                        FilterQuery t = new FilterQuery
                        {
                            Field = n.Field,
                            Operator = n.Operator,
                            Value = n.Value
                        };

                        f.Filters.Add(t);
                    }
                }

                result.Add(f);
            }

            return result;
        }
    }
}