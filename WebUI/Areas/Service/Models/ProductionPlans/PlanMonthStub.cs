﻿using System.Collections.Generic;
using System.Linq;
using Business.Entities;

namespace WebUI.Areas.Service.Models.ProductionPlans
{
    public class PlanMonthStub
    {
        public int ProductionPlanId { get; set; }
        public double Jan { get; set; }
        public double Feb { get; set; }
        public double Mar { get; set; }
        public double Apr { get; set; }
        public double Mei { get; set; }
        public double Jun { get; set; }
        public double Jul { get; set; }
        public double Agt { get; set; }
        public double Sep { get; set; }
        public double Okt { get; set; }
        public double Nov { get; set; }
        public double Des { get; set; }

        public PlanMonthStub()
        {
        }

        public PlanMonthStub(List<ProductionPlanDetail> dbObjects)
        {
            //TODO: Manual mapping object here
            ProductionPlanId = dbObjects.FirstOrDefault().ProductionPlanId;

            foreach (ProductionPlanDetail dbObject in dbObjects)
            {
                switch (dbObject.Month)
                {
                    case 1:
                        Jan = dbObject.Value;
                        break;
                    case 2:
                        Feb = dbObject.Value;
                        break;
                    case 3:
                        Mar = dbObject.Value;
                        break;
                    case 4:
                        Apr = dbObject.Value;
                        break;
                    case 5:
                        Mei = dbObject.Value;
                        break;
                    case 6:
                        Jun = dbObject.Value;
                        break;
                    case 7:
                        Jul = dbObject.Value;
                        break;
                    case 8:
                        Agt = dbObject.Value;
                        break;
                    case 9:
                        Sep = dbObject.Value;
                        break;
                    case 10:
                        Okt = dbObject.Value;
                        break;
                    case 11:
                        Nov = dbObject.Value;
                        break;
                    case 12:
                        Des = dbObject.Value;
                        break;
                }
            }
        }
    }
}