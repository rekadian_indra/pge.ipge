﻿using Business.Entities;
using System.Collections.Generic;

namespace WebUI.Areas.Service.Models.ProductionPlans
{
    public class ProductionPlanDetailServiceStub
    {
        public int Year { get; set; }
        public PlanMonthStub SteamData { get; set; }
        public PlanMonthStub ElectricData { get; set; }

        public ProductionPlanDetailServiceStub()
        {
        }

        public ProductionPlanDetailServiceStub(int year, List<ProductionPlanDetail> steamData, List<ProductionPlanDetail> electricData)
        {
            Year = year;
            SteamData = new PlanMonthStub(steamData);
            ElectricData = new PlanMonthStub(electricData);
        }
    }
}