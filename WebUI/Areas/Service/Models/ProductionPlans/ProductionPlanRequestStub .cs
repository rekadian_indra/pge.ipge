﻿using System;
using System.Collections.Generic;
using Business.Entities;
using Business.Infrastructure;

namespace WebUI.Areas.Service.Models.ProductionPlans
{
    public class ProductionPlanRequestStub
    {
        public Guid UserId { get; set; }
        public int? AreaId { get; set; }
        public string AreaName { get; set; }
        public int PowerPlanId { get; set; }
        public string PowerPlanName { get; set; }
        public List<ProductionPlanDetailServiceStub> YearPlan { get; set; }

        public ProductionPlanRequestStub()
        {
        }
    }
}