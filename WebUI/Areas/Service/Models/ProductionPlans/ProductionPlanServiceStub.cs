﻿using System.Collections.Generic;
using Business.Entities;
using Business.Infrastructure;

namespace WebUI.Areas.Service.Models.ProductionPlans
{
    public class ProductionPlanServiceStub
    {
        public int? AreaId { get; set; }
        public string AreaName { get; set; }
        public int PowerPlanId { get; set; }
        public string PowerPlanName { get; set; }
        public List<ProductionPlanDetailServiceStub> YearPlan { get; set; }

        public ProductionPlanServiceStub()
        {
        }

        public ProductionPlanServiceStub(ProductionPlan dbObject, List<ProductionPlanDetailServiceStub> yearPlan)
        {
            //TODO: Manual mapping object here
            AreaId = dbObject.PowerPlant.AreaId;
            if (AreaId != null)
            {
                AreaName = dbObject.PowerPlant.Area.Name;
            }
            PowerPlanId = dbObject.PowerPlantId;
            PowerPlanName = dbObject.PowerPlant.Name;

            YearPlan = yearPlan;
        }
    }
}