﻿using Business.Infrastructure;
using Common.Enums;
using System.Collections.Generic;

namespace WebUI.Areas.Service.Models.ProductionPlans
{
    public class ProductionPlanParamStub
    {
        public int? skip { get; set; }
        public int? take { get; set; }
        public Field? sortField { get; set; }
        public SortOrder? sortDirection { get; set; }
        public int? areaId { get; set; }
        public int? powerPlanId { get; set; }
        public int? year { get; set; }

        public ProductionPlanParamStub()
        {
        }
    }
}