﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using WebUI.Infrastructure;

namespace WebUI.Areas.Services.Models
{
    public class UserServiceStub
    {
        public string UserName { get; set; }
        public List<string> Roles { get; set; }
        public List<string> Modules { get; set; }
        public int? AreaId { get; set; }
        public string AreaName { get; set; }

        public UserServiceStub()
        {
        }

       
    }


}