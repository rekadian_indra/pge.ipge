﻿using System.Collections.Generic;

namespace WebUI.Areas.Service.Models
{
    public class ServiceListStub<T>
    {
        public int Total { get; set; }
        public List<T> Data { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}