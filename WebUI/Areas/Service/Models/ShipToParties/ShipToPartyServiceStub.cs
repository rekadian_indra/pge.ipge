﻿using System;
using System.Collections.Generic;
using WebUI.Infrastructure;

namespace WebUI.Areas.Service.Models.ShipToParties
{
    public class ShipToPartyServiceStub
    {
        public int? AreaId { get; set; }
        public string AreaName { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDateTimeUtc { get; set; }
        public List<WebUI.Areas.DataMaster.Models.ShipToParties.ShipToPartyPresentationStub> PowerPlants { get; set; }

        public List<WebUI.Areas.DataMaster.Models.Separator.SeparatorPresentationStub> Separators { get; set; }

        public List<WebUI.Areas.DataMaster.Models.InterfacePoint.InterfacePointPresentationStub> InterfacePoints { get; set; }

        public List<WebUI.Areas.DataMaster.Models.Scrubber.ScrubberPresentationStub> Scrubbers { get; set; }

        public List<WebUI.Areas.DataMaster.Models.Wells.WellPresentationStub> Wells { get; set; }
    }
}