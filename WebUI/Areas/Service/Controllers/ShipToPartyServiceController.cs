﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Business.Entities;
using Business.Abstract;
using WebUI.Areas.Service.Models.ShipToParties;
using System.Threading.Tasks;
using System.Net;
using Business.Infrastructure;
using WebUI.Areas.Service.Models;
using Common.Enums;

namespace WebUI.Areas.Service.Controllers
{
    public class ShipToPartyServiceController : ApiController
    {
        private IPowerPlantRepository RepoPowerPlant { get; set; }
        private IAreaRepository RepoArea { get; set; }
        private IViewWellRepository RepoViewWell { get; set; }

        public ShipToPartyServiceController(IPowerPlantRepository repoPowerPlant, IAreaRepository repoArea, IViewWellRepository repoViewWell)
        {
            RepoPowerPlant = repoPowerPlant;
            RepoArea = repoArea;
            RepoViewWell = repoViewWell;
        }

        [HttpGet]
        public async Task<ServiceListStub<ShipToPartyServiceStub>> GetListShipToParty()
        {
            List<Area> areas;
            List<ShipToPartyServiceStub> models = new List<ShipToPartyServiceStub>();
            FilterQuery filter = new FilterQuery();
            var result = new ServiceListStub<ShipToPartyServiceStub>();

            filter.AddFilter(Field.IsDeleted, FilterOperator.Equals, "False");
            areas = await RepoArea.FindAllAsync(filter);

            if (areas.Any())
            {
                areas = areas.OrderBy(m => m.Id).ToList();
                
                //map db data to model
                foreach (Area item in areas)
                {
                    ShipToPartyServiceStub shipToPartyServiceStub = new ShipToPartyServiceStub();
                    shipToPartyServiceStub.AreaId = item.Id;
                    shipToPartyServiceStub.AreaName = item.Name;
                    shipToPartyServiceStub.CreatedBy = item.CreatedBy;
                    shipToPartyServiceStub.CreatedDateTimeUtc = item.CreatedDateTimeUtc;
                    shipToPartyServiceStub.ModifiedBy = item.ModifiedBy;
                    shipToPartyServiceStub.ModifiedDateTimeUtc = item.ModifiedDateTimeUtc;
                    shipToPartyServiceStub.PowerPlants = ListMapper.MapList<Business.Entities.PowerPlant, DataMaster.Models.ShipToParties.ShipToPartyPresentationStub>(item.PowerPlants.Where(x => x.IsDeleted == false));
                    shipToPartyServiceStub.Separators = ListMapper.MapList<Business.Entities.MstSeparator, DataMaster.Models.Separator.SeparatorPresentationStub>(item.MstSeparators.Where(x => x.IsDeleted == false));
                    shipToPartyServiceStub.InterfacePoints = ListMapper.MapList<Business.Entities.InterfacePoint, DataMaster.Models.InterfacePoint.InterfacePointPresentationStub>(item.InterfacePoints.Where(x => x.IsDeleted == false));
                    shipToPartyServiceStub.Scrubbers = ListMapper.MapList<Business.Entities.Scrubber, DataMaster.Models.Scrubber.ScrubberPresentationStub>(item.Scrubbers.Where(x => x.IsDeleted == false));
                    shipToPartyServiceStub.Wells = ListMapper.MapList<Business.Entities.Well, DataMaster.Models.Wells.WellPresentationStub>(item.Wells.Where(x => x.IsDeleted == false));
                    models.Add(shipToPartyServiceStub);
                }

                result.Status = HttpStatusCode.OK.GetHashCode();
                result.Message = "Data found";
                result.Total = areas.Count();
                result.Data = models;
            }
            else
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "Data not found";
            }
            
            return result;
        }

    }
}
