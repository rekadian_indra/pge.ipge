﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using SecurityGuard.Interfaces;
using SecurityGuard.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using WebUI.Areas.Service.Models;
using WebUI.Areas.Services.Models;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.Service.Controllers
{
    public class UserServiceController : ApiController
    {
        private IUserRepository RepoUser { get; set; }
        private IRoleRepository RepoRole { get; set; }
        private IMembershipRepository RepoMembership { get; set; }
        private IAreaRepository RepoArea { get; set; }
        private IAuthenticationService AuthenticationService { get; set; }
        private IMembershipService MembershipService { get; set; }

        public UserServiceController(
            IUserRepository repoUser, 
            IRoleRepository repoRole, 
            IMembershipRepository repoMembership,
            IAreaRepository repoArea
            )
        {
            RepoUser = repoUser;
            RepoRole = repoRole;
            RepoMembership = repoMembership;
            RepoArea = repoArea;
            MembershipService = new MembershipService(System.Web.Security.Membership.Provider);
            AuthenticationService = new AuthenticationService(MembershipService, new FormsAuthenticationService());
        }

        [HttpPost]
        public ServiceObjectStub<UserServiceStub> Login([FromBody] LogOnModel model)
        {
            Area area = null;
            bool isAuthenticated = false;
            ServiceObjectStub<UserServiceStub> result = null;
            ActiveDirectoryUtil AdModel = new ActiveDirectoryUtil();

            if (ModelState.IsValid)
            {
                if (model.UserName.ToLower() == UserAccount.SuperAdmin.ToDescription().ToLower())
                {
                    //username: superadmin | password: 123456
                    isAuthenticated = AuthenticationService.LogOn(model.UserName, model.Password, model.RememberMe);
                }
                else
                {
                    string deviceId = model.DeviceId;                   
                    AdModel.Login(model.UserName, model.Password, model.RememberMe);
                    isAuthenticated = AdModel.IsAuthenticated;
                }

                if (isAuthenticated)
                {
                    FilterQuery filter = new FilterQuery(Field.UserName, FilterOperator.Equals, model.UserName);
                    User user = RepoUser.Find(filter);
                    List<Role> roles = new List<Role>(user.Roles);

                    filter = new FilterQuery(Field.UserId, FilterOperator.Equals, user.UserId);
                    Membership membership = RepoMembership.Find(filter);

                    if (membership.AreaId != null)
                    {
                        filter = new FilterQuery(Field.Id, FilterOperator.Equals, membership.AreaId);
                        area = RepoArea.Find(filter);
                    }

                    UserServiceStub data = new UserServiceStub()
                    {
                        UserName = model.UserName,
                        Roles = roles.Select(n => n.RoleName).ToList(),
                        Modules = roles.SelectMany(n => n.ModulesInRoles.Select(m => m.Module.ModuleName)).Distinct().ToList(),
                        AreaId = area?.Id,
                        AreaName = area?.Name
                    };

                    result = ResponseBuilder(HttpStatusCode.OK, "Success", data);
                }
                else
                {
                    result = ResponseBuilder(HttpStatusCode.NotFound, "User Name atau Password salah.");
                }
            }
            else
            {
                result = ResponseBuilder(HttpStatusCode.NotFound, "UserName / Password / DeviceId is required.");
            }

            return result;
        }

        #region "Helper"

        private ServiceObjectStub<UserServiceStub> ResponseBuilder
        (
            HttpStatusCode status,
            string message,
            UserServiceStub data = null
        )
        {
            return new ServiceObjectStub<UserServiceStub>
            {
                Status = status.GetHashCode(),
                Message = message,
                Data = data
            };
        }

        #endregion
    }
}
