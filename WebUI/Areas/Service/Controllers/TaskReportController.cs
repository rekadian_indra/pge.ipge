﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebUI.Areas.Service.Models;
using WebUI.Areas.Service.Models.TaskReports;
using WebUI.Areas.Services.Models;
using WebUI.Infrastructure;

namespace WebUI.Areas.Service.Controllers
{
    public class TaskReportController : ApiController
    {
        private ITaskReportRepository RepoTaskReport { get; set; }
        IPltpRepository RepoPltp { get; set; }
        IDkpRepository RepoDkp { get; set; }
        IProductionRepository RepoProduction { get; set; }
        IReInjectionRepository RepoReinjection { get; set; }
        IMonitoringRepository RepoMonitoring { get; set; }
        ISeparatorRepository RepoSeparator { get; set; }
        IInterfaceScrubberRepository RepoInterfaceScrubber { get; set; }
        IActualPlanningRepository RepoActualPlanning { get; set; }
        IViewWellRepository RepoViewWell { get; set; }

        public TaskReportController(
            ITaskReportRepository repoTaskReport,
            IPowerPlantRepository repoPowerPlant,
            IPltpRepository repoPltp,
            IDkpRepository repoDkp,
            IWellRepository repoWell,
            IMstSeparatorRepository repoMasterSeparator,
            IInterfacePointRepository repoInterfacePoint,
            IScrubberRepository repoScrubber,
            IProductionRepository repoProduction,
            IReInjectionRepository repoReinjection,
            IMonitoringRepository repoMonitoring,
            ISeparatorRepository repoSeparator,
            IInterfaceScrubberRepository repoInterfaceScrubber,
            IActualPlanningRepository repoActualPlanning,
            IViewWellRepository repoVIewWell
        )
        {
            RepoTaskReport = repoTaskReport;
            RepoPltp = repoPltp;
            RepoDkp = repoDkp;
            RepoProduction = repoProduction;
            RepoReinjection = repoReinjection;
            RepoMonitoring = repoMonitoring;
            RepoSeparator = repoSeparator;
            RepoInterfaceScrubber = repoInterfaceScrubber;
            RepoActualPlanning = repoActualPlanning;
            RepoViewWell = repoVIewWell;
        }

        #region "Action: Create & Edit"

        [HttpPost]
        public async Task<ServiceObjectStub<object>> Create([FromBody] TaskReportServiceStub param)
        {
            ServiceObjectStub<object> result = new ServiceObjectStub<object>();
            FilterQuery filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, param.AreaId);
            filter.AddFilter(Field.Periode, FilterOperator.Equals, param.Periode);
            filter.AddFilter(Field.Status, FilterOperator.Equals, DataStatus.WAITING.ToString());

            TaskReport dbObject = await RepoTaskReport.FindAsync(filter);

            if (dbObject == null)
            {
                dbObject = new TaskReport();
            }

            param.MapDbObject(dbObject);

            await RepoTaskReport.SaveAsync(dbObject);

            result.Status = HttpStatusCode.OK.GetHashCode();
            result.Message = "Task report success created.";

            return result;
        }
        #endregion

        #region "Action: Delete"

        [HttpPost]
        public async Task<ServiceObjectStub<object>> DeleteTaskReport([FromUri] int id)
        {
            //lib
            FilterQuery filter;
            Task<List<Pltp>> taskPltps = null;
            Task<List<Dkp>> taskDkps = null;
            Task<List<Production>> taskProductions = null;
            Task<List<ReInjection>> taskReinjections = null;
            Task<List<Monitoring>> taskMonitorings = null;
            Task<List<Separator>> taskSeparators = null;
            Task<List<InterfaceScrubber>> taskInterfaceScrubbers = null;
            Task<List<ActualPlanning>> taskActualPlans = null;

            ServiceObjectStub<object> result = new ServiceObjectStub<object>();
            TaskReport dbObject = await RepoTaskReport.FindByPrimaryKeyAsync(id);

            if (dbObject.Status != DataStatus.APPROVED.ToString())
            {
                if (!(await RepoTaskReport.DeleteAsync(dbObject)))
                {
                    result.Status = HttpStatusCode.NotFound.GetHashCode();
                    result.Message = "Task report delete failed.";
                }
                else
                {
                    filter = new FilterQuery(Field.TaskReportId, FilterOperator.Equals, id);

                    taskPltps = RepoPltp.FindAllAsync(filter);
                    taskDkps = RepoDkp.FindAllAsync(filter);
                    taskProductions = RepoProduction.FindAllAsync(filter);
                    taskReinjections = RepoReinjection.FindAllAsync(filter);
                    taskMonitorings = RepoMonitoring.FindAllAsync(filter);
                    taskSeparators = RepoSeparator.FindAllAsync(filter);
                    taskInterfaceScrubbers = RepoInterfaceScrubber.FindAllAsync(filter);
                    taskActualPlans = RepoActualPlanning.FindAllAsync(filter);

                    await Task.WhenAll(taskPltps, taskDkps, taskProductions, taskReinjections, taskMonitorings,
                                       taskSeparators, taskInterfaceScrubbers, taskActualPlans);

                    if (taskPltps.Result.Any())
                    {
                        await RepoPltp.DeleteAllAsync(taskPltps.Result);
                    }
                    if (taskDkps.Result.Any())
                    {
                        await RepoDkp.DeleteAllAsync(taskDkps.Result);
                    }
                    if (taskProductions.Result.Any())
                    {
                        await RepoProduction.DeleteAllAsync(taskProductions.Result);
                    }
                    if (taskReinjections.Result.Any())
                    {
                        await RepoReinjection.DeleteAllAsync(taskReinjections.Result);
                    }
                    if (taskMonitorings.Result.Any())
                    {
                        await RepoMonitoring.DeleteAllAsync(taskMonitorings.Result);
                    }
                    if (taskSeparators.Result.Any())
                    {
                        await RepoSeparator.DeleteAllAsync(taskSeparators.Result);
                    }
                    if (taskInterfaceScrubbers.Result.Any())
                    {
                        await RepoInterfaceScrubber.DeleteAllAsync(taskInterfaceScrubbers.Result);
                    }
                    if (taskActualPlans.Result.Any())
                    {
                        await RepoActualPlanning.DeleteAllAsync(taskActualPlans.Result);
                    }

                    result.Status = HttpStatusCode.OK.GetHashCode();
                    result.Message = "Task report success deleted.";
                }
            }
            else
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "Task report has been approved. Delete failed.";
            }

            return result;
        }

        [HttpPost]
        public async Task<ServiceObjectStub<object>> DeletePltp([FromUri] int id)
        {
            ServiceObjectStub<object> result = new ServiceObjectStub<object>
            {
                Status = HttpStatusCode.OK.GetHashCode(),
                Message = "PLTP success deleted."
            };

            Pltp dbObject = await RepoPltp.FindByPrimaryKeyAsync(id);

            if (!(await RepoPltp.DeleteAsync(dbObject)))
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "PLTP delete failed.";
            }

            return result;
        }

        [HttpPost]
        public async Task<ServiceObjectStub<object>> DeleteDkp([FromUri] int id)
        {
            ServiceObjectStub<object> result = new ServiceObjectStub<object>
            {
                Status = HttpStatusCode.OK.GetHashCode(),
                Message = "DKP success deleted."
            };

            Dkp dbObject = await RepoDkp.FindByPrimaryKeyAsync(id);

            if (!(await RepoDkp.DeleteAsync(dbObject)))
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "DKP delete failed.";
            }

            return result;
        }

        [HttpPost]
        public async Task<ServiceObjectStub<object>> DeleteProduction([FromUri] int id)
        {
            ServiceObjectStub<object> result = new ServiceObjectStub<object>
            {
                Status = HttpStatusCode.OK.GetHashCode(),
                Message = "Production success deleted."
            };

            Production dbObject = await RepoProduction.FindByPrimaryKeyAsync(id);

            if (!(await RepoProduction.DeleteAsync(dbObject)))
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "Production delete failed.";
            }

            return result;
        }

        [HttpPost]
        public async Task<ServiceObjectStub<object>> DeleteMonitoring([FromUri] int id)
        {
            ServiceObjectStub<object> result = new ServiceObjectStub<object>
            {
                Status = HttpStatusCode.OK.GetHashCode(),
                Message = "Monitoring success deleted."
            };

            Monitoring dbObject = await RepoMonitoring.FindByPrimaryKeyAsync(id);

            if (!(await RepoMonitoring.DeleteAsync(dbObject)))
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "Monitoring delete failed.";
            }

            return result;
        }

        [HttpPost]
        public async Task<ServiceObjectStub<object>> DeleteReinjection([FromUri] int id)
        {
            ServiceObjectStub<object> result = new ServiceObjectStub<object>
            {
                Status = HttpStatusCode.OK.GetHashCode(),
                Message = "Reinjection success deleted."
            };

            ReInjection dbObject = await RepoReinjection.FindByPrimaryKeyAsync(id);

            if (!(await RepoReinjection.DeleteAsync(dbObject)))
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "Reinjection delete failed.";
            }

            return result;
        }

        [HttpPost]
        public async Task<ServiceObjectStub<object>> DeleteSeparator([FromUri] int id)
        {
            ServiceObjectStub<object> result = new ServiceObjectStub<object>
            {
                Status = HttpStatusCode.OK.GetHashCode(),
                Message = "Separator success deleted."
            };

            Separator dbObject = await RepoSeparator.FindByPrimaryKeyAsync(id);

            if (!(await RepoSeparator.DeleteAsync(dbObject)))
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "Separator delete failed.";
            }

            return result;
        }

        [HttpPost]
        public async Task<ServiceObjectStub<object>> DeleteInterfaceScrubber([FromUri] int id)
        {
            ServiceObjectStub<object> result = new ServiceObjectStub<object>
            {
                Status = HttpStatusCode.OK.GetHashCode(),
                Message = "Interface scrubber success deleted."
            };

            InterfaceScrubber dbObject = await RepoInterfaceScrubber.FindByPrimaryKeyAsync(id);

            if (!(await RepoInterfaceScrubber.DeleteAsync(dbObject)))
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "Interface scrubber delete failed.";
            }

            return result;
        }

        [HttpPost]
        public async Task<ServiceObjectStub<object>> DeleteActualPlanning([FromUri] int id)
        {
            ServiceObjectStub<object> result = new ServiceObjectStub<object>
            {
                Status = HttpStatusCode.OK.GetHashCode(),
                Message = "Actual planning success deleted."
            };

            ActualPlanning dbObject = await RepoActualPlanning.FindByPrimaryKeyAsync(id);

            if (!(await RepoActualPlanning.DeleteAsync(dbObject)))
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "Actual planning delete failed.";
            }

            return result;
        }

        [HttpPost]
        public async Task<ServiceObjectStub<object>> ApproveTaskReport([FromBody] TaskReportServiceStub param)
        {
            DataStatus status = (DataStatus)Enum.Parse(typeof(DataStatus), param.Status);
            UserServiceStub user = param.User;
            ServiceObjectStub<object> result = new ServiceObjectStub<object>
            {
                Status = HttpStatusCode.OK.GetHashCode(),
                Message = string.Format("{0} {1} {2}", "Task Report ", status.ToDescription(), " success")
            };
            FilterQuery filter = new FilterQuery(FilterLogic.And);
            filter.AddFilter(Field.Id, FilterOperator.Equals, param.Id);

            if (user != null && user.AreaId.HasValue && user.AreaId.Value > 0)
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, user.AreaId);

            TaskReport dbObject = await RepoTaskReport.FindAsync(filter);

            if (dbObject != null && user != null && user.Roles.Any() 
                && user.Roles.Any(n => n == UserRole.Superadmin.ToString() || n == UserRole.OPERATION_SUPERVISOR.ToString()))
            {
                dbObject.Status = param.Status;
                dbObject.ApprovedBy = user.UserName;
                dbObject.ModifiedBy = user.UserName;
                dbObject.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();

                if (!(await RepoTaskReport.SaveAsync(dbObject)))
                {
                    result.Status = HttpStatusCode.NotFound.GetHashCode();
                    result.Message = string.Format("{0} {1} {2}", "Task Report ", status.ToDescription(), " failed.");
                }
                else
                {
                    param = new TaskReportServiceStub(dbObject);                    
                }
            }
            else
            {
                result.Status = HttpStatusCode.Forbidden.GetHashCode();
                result.Message = string.Format("{0} {1}", "Sorry you don't have the authorization to ", status.ToDescription());
            }

            return result;
        }
        #endregion

        #region "Action: Get"

        [HttpPost]
        public async Task<ServiceListStub<TaskReportRequestStub>> GetTaskReport([FromBody] PaginationStub paging)
        {
            int count;
            int? skip = null, take = null;
            FilterQuery filter = new FilterQuery();
            List<SortQuery> sorts = new List<SortQuery>();
            List<TaskReport> dbObjects;
            Task<List<TaskReport>> taskDbObjects = null;
            Task<int> countTask = null;
            ServiceListStub<TaskReportRequestStub> result = new ServiceListStub<TaskReportRequestStub>();

            if (paging != null)
            {
                if (paging.Take != 0)
                {
                    skip = paging.Skip;
                    take = paging.Take;
                }

                if (paging.Sorts != null && paging.Sorts.Any())
                    sorts.AddRange(SortingHelper.MapList(paging.Sorts));

                if (paging.Filters != null && paging.Filters.Any())
                {
                    filter.Filters = new List<FilterQuery>();
                    filter.Filters.AddRange(FilterHelper.MapList(paging.Filters));
                }
            }


            taskDbObjects = RepoTaskReport.FindAllAsync(skip, take, sorts, filter);
            countTask = RepoTaskReport.CountAsync(filter);

            await Task.WhenAll(taskDbObjects, countTask);

            dbObjects = taskDbObjects.Result;
            count = countTask.Result;

            if (count > 0)
            {
                result.Status = HttpStatusCode.OK.GetHashCode();
                result.Message = "Data found";
                result.Total = count;
                result.Data = ListMapper.MapList<TaskReport, TaskReportRequestStub>(dbObjects);
            }
            else
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "Data not found";
            }

            return result;
        }
        #endregion
    }
}
