﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using WebUI.Areas.Service.Models;
using WebUI.Areas.Service.Models.ProductionPlans;
using PowerPlant = Business.Entities.PowerPlant;

namespace WebUI.Areas.Service.Controllers
{
    public class ProductionPlanController : ApiController
    {
        private IProductionPlanRepository RepoProductionPlan { get; set; }

        private IProductionPlanDetailRepository RepoProductionPlanDetail { get; set; }

        private IPowerPlantRepository RepoPowerPlan { get; set; }
        private IUserRepository RepoUser { get; set; }

        public ProductionPlanController(
            IProductionPlanRepository repoProductionPlan,
            IProductionPlanDetailRepository repoProductionPlanDetail,
            IPowerPlantRepository repoPowerPlan,
            IUserRepository repoUser
        )
        {
            RepoProductionPlan = repoProductionPlan;
            RepoProductionPlanDetail = repoProductionPlanDetail;
            RepoPowerPlan = repoPowerPlan;
            RepoUser = repoUser;
        }

        [HttpPost]
        public async Task<ServiceObjectStub<object>> Delete([FromBody] ProductionPlanParamStub param)
        {
            ServiceObjectStub<object> result = new ServiceObjectStub<object>();
            result.Status = HttpStatusCode.OK.GetHashCode();

            List<ProductionPlan> productionPlans = null;
            List<ProductionPlanDetail> productionPlanDetails = new List<ProductionPlanDetail>();
            FilterQuery filterQuery = new FilterQuery();

            if (param != null)
            {
                if(param.areaId != null && param.powerPlanId != null && param.year != null &&
                    param.areaId != 0 && param.powerPlanId != 0 && param.year != 0)
                {
                    filterQuery.AddFilter(new FieldHelper("PowerPlant", "AreaId"), FilterOperator.Equals, param.areaId);
                    filterQuery.AddFilter(Field.PowerPlantId, FilterOperator.Equals, param.powerPlanId);
                    filterQuery.AddFilter(Field.Year, FilterOperator.Equals, param.year);

                    productionPlans = await RepoProductionPlan.FindAllAsync(filterQuery);

                    if(productionPlans.Any())
                    {
                        ProductionPlan s = productionPlans.FirstOrDefault(p => p.UtilType == ProductionUtilization.STEAM.ToString());
                        ProductionPlan e = productionPlans.FirstOrDefault(p => p.UtilType == ProductionUtilization.ELECTRIC.ToString());

                        if(s != null && e != null)
                        {
                            productionPlanDetails.Concat(s.ProductionPlanDetails.ToList());
                            productionPlanDetails.Concat(e.ProductionPlanDetails.ToList());

                            if(productionPlanDetails.Any())
                            {
                                //delete production detail
                                try
                                {
                                    RepoProductionPlanDetail.DeleteAll(productionPlanDetails);
                                    result.Status = HttpStatusCode.OK.GetHashCode();
                                }
                                catch (Exception)
                                {
                                    result.Status = HttpStatusCode.NotFound.GetHashCode();
                                    result.Message = "Gagal menghapus data Production Plan Detail!";
                                }
                            }
                        }
                        if(result.Status == HttpStatusCode.OK.GetHashCode())
                        {
                            try
                            {
                                RepoProductionPlan.DeleteAll(productionPlans);
                                result.Status = HttpStatusCode.OK.GetHashCode();
                            }
                            catch (Exception)
                            {
                                result.Status = HttpStatusCode.NotFound.GetHashCode();
                                result.Message = "Gagal menghapus data Production Plan!";
                            }
                        }
                    }
                    else
                    {
                        result.Status = HttpStatusCode.NotFound.GetHashCode();
                        result.Message = "Data not found!";
                    }
                }
                else
                {
                    result.Status = HttpStatusCode.NotFound.GetHashCode();
                    result.Message = "Parameter null!";
                }
            }
            else
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "Parameter null!";
            }

            return result;
        }

        [HttpPost]
        public async Task<ServiceObjectStub<object>> Put([FromBody] ProductionPlanRequestStub request)
        {
            ServiceObjectStub<object> result = new ServiceObjectStub<object>();
            result.Status = HttpStatusCode.OK.GetHashCode();

            Business.Entities.User user = await RepoUser.FindAsync(new FilterQuery(Field.UserId,FilterOperator.Equals,request.UserId));

            List<ProductionPlan> productionPlans = await RepoProductionPlan.FindAllAsync();
            List<ProductionPlanDetail> productionPlanDetails = await RepoProductionPlanDetail.FindAllAsync();

            FilterQuery filterQuery = new FilterQuery();
            filterQuery.AddFilter(Field.AreaId,FilterOperator.Equals,request.AreaId);
            filterQuery.AddFilter(Field.Id, FilterOperator.Equals, request.PowerPlanId);
            PowerPlant powerPlan = await RepoPowerPlan.FindAsync(filterQuery);

            List<ProductionPlan> savedProductionPlans = new List<ProductionPlan>();
            List<ProductionPlanDetail> savedProductionPlanDetails = new List<ProductionPlanDetail>();

            if (user != null)
            {
                if (powerPlan != null)
                {
                    foreach (ProductionPlanDetailServiceStub y in request.YearPlan)
                    {
                        List<ProductionPlan> productionPlansByYear = productionPlans.FindAll(p => p.Year == y.Year);

                        if(productionPlansByYear.Any() && y.SteamData.ProductionPlanId != 0 && y.ElectricData.ProductionPlanId != 0)
                        {
                            //EDIT

                            ProductionPlan s = productionPlans.Find(pl => pl.Year == y.Year && pl.Id == y.SteamData.ProductionPlanId);
                            ProductionPlan e = productionPlans.Find(pl => pl.Year == y.Year && pl.Id == y.ElectricData.ProductionPlanId);
                            if (s != null && e != null)
                            {
                                List<ProductionPlanDetail> steamData = productionPlanDetails.FindAll(i => i.ProductionPlanId == s.Id);
                                List<ProductionPlanDetail> electricData = productionPlanDetails.FindAll(i => i.ProductionPlanId == e.Id);

                                if (steamData.Count == 12 && electricData.Count == 12)
                                {
                                    for (int i = 1; i <= 12; i++)
                                    {
                                        ProductionPlanDetail productionPlanDetailSteam = steamData.FirstOrDefault(m => m.Month == i);
                                        ProductionPlanDetail productionPlanDetailElectric = electricData.FirstOrDefault(m => m.Month == i);

                                        List<ProductionPlanDetail> updatedSteamData = new List<ProductionPlanDetail>();
                                        List<ProductionPlanDetail> updatedElectricData = new List<ProductionPlanDetail>();

                                        if (productionPlanDetailSteam != null && productionPlanDetailElectric != null)
                                        {
                                            switch (i)
                                            {
                                                case 1:
                                                    productionPlanDetailSteam.Value = y.SteamData.Jan;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Jan;
                                                    break;
                                                case 2:
                                                    productionPlanDetailSteam.Value = y.SteamData.Feb;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Feb;
                                                    break;
                                                case 3:
                                                    productionPlanDetailSteam.Value = y.SteamData.Mar;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Mar;
                                                    break;
                                                case 4:
                                                    productionPlanDetailSteam.Value = y.SteamData.Apr;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Apr;
                                                    break;
                                                case 5:
                                                    productionPlanDetailSteam.Value = y.SteamData.Mei;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Mei;
                                                    break;
                                                case 6:
                                                    productionPlanDetailSteam.Value = y.SteamData.Jun;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Jun;
                                                    break;
                                                case 7:
                                                    productionPlanDetailSteam.Value = y.SteamData.Jul;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Jul;
                                                    break;
                                                case 8:
                                                    productionPlanDetailSteam.Value = y.SteamData.Agt;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Agt;
                                                    break;
                                                case 9:
                                                    productionPlanDetailSteam.Value = y.SteamData.Sep;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Sep;
                                                    break;
                                                case 10:
                                                    productionPlanDetailSteam.Value = y.SteamData.Okt;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Okt;
                                                    break;
                                                case 11:
                                                    productionPlanDetailSteam.Value = y.SteamData.Nov;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Nov;
                                                    break;
                                                case 12:
                                                    productionPlanDetailSteam.Value = y.SteamData.Des;
                                                    productionPlanDetailElectric.Value = y.ElectricData.Des;
                                                    break;
                                            }
                                            savedProductionPlanDetails.Add(productionPlanDetailSteam);
                                            savedProductionPlanDetails.Add(productionPlanDetailElectric);
                                        }
                                        else
                                        {
                                            result.Status = HttpStatusCode.NotFound.GetHashCode();
                                            result.Message = "Data ProductionPlanDetail tidak lengkap!";
                                        }
                                    }
                                }
                                else
                                {
                                    result.Status = HttpStatusCode.NotFound.GetHashCode();
                                    result.Message = "Data ProductionPlanDetail tidak lengkap!";
                                }
                            }
                            else
                            {
                                result.Status = HttpStatusCode.NotFound.GetHashCode();
                                result.Message = "ProductionPlanId tidak valid!";
                            }
                        }
                        else if (!productionPlansByYear.Any() && y.SteamData.ProductionPlanId == 0 && y.ElectricData.ProductionPlanId == 0)
                        {
                            //CREATE

                            ProductionPlan productionPlanSteam = new ProductionPlan();
                            productionPlanSteam.PowerPlantId = powerPlan.Id;
                            productionPlanSteam.UtilType = ProductionUtilization.STEAM.ToString();
                            productionPlanSteam.Year = y.Year;
                            productionPlanSteam.CreatedBy = user.UserName;
                            productionPlanSteam.CreatedDateTimeUtc = DateTime.Now.ToUtcDateTime();
                            productionPlanSteam.ModifiedBy = user.UserName;
                            productionPlanSteam.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();
                            productionPlanSteam.IsDeleted = false;

                            ProductionPlan productionPlanElectric = new ProductionPlan();
                            productionPlanElectric.PowerPlantId = powerPlan.Id;
                            productionPlanElectric.UtilType = ProductionUtilization.ELECTRIC.ToString();
                            productionPlanElectric.Year = y.Year;
                            productionPlanElectric.CreatedBy = user.UserName;
                            productionPlanElectric.CreatedDateTimeUtc = DateTime.Now.ToUtcDateTime();
                            productionPlanElectric.ModifiedBy = user.UserName;
                            productionPlanElectric.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();
                            productionPlanElectric.IsDeleted = false;

                            for (int i = 1; i <= 12; i++)
                            {
                                ProductionPlanDetail productionPlanDetailSteam = new ProductionPlanDetail();
                                productionPlanDetailSteam.Month = i;

                                ProductionPlanDetail productionPlanDetailElectric = new ProductionPlanDetail();
                                productionPlanDetailElectric.Month = i;

                                switch (i)
                                {
                                    case 1:
                                        productionPlanDetailSteam.Value = y.SteamData.Jan;
                                        productionPlanDetailElectric.Value = y.ElectricData.Jan;
                                        break;
                                    case 2:
                                        productionPlanDetailSteam.Value = y.SteamData.Feb;
                                        productionPlanDetailElectric.Value = y.ElectricData.Feb;
                                        break;
                                    case 3:
                                        productionPlanDetailSteam.Value = y.SteamData.Mar;
                                        productionPlanDetailElectric.Value = y.ElectricData.Mar;
                                        break;
                                    case 4:
                                        productionPlanDetailSteam.Value = y.SteamData.Apr;
                                        productionPlanDetailElectric.Value = y.ElectricData.Apr;
                                        break;
                                    case 5:
                                        productionPlanDetailSteam.Value = y.SteamData.Mei;
                                        productionPlanDetailElectric.Value = y.ElectricData.Mei;
                                        break;
                                    case 6:
                                        productionPlanDetailSteam.Value = y.SteamData.Jun;
                                        productionPlanDetailElectric.Value = y.ElectricData.Jun;
                                        break;
                                    case 7:
                                        productionPlanDetailSteam.Value = y.SteamData.Jul;
                                        productionPlanDetailElectric.Value = y.ElectricData.Jul;
                                        break;
                                    case 8:
                                        productionPlanDetailSteam.Value = y.SteamData.Agt;
                                        productionPlanDetailElectric.Value = y.ElectricData.Agt;
                                        break;
                                    case 9:
                                        productionPlanDetailSteam.Value = y.SteamData.Sep;
                                        productionPlanDetailElectric.Value = y.ElectricData.Sep;
                                        break;
                                    case 10:
                                        productionPlanDetailSteam.Value = y.SteamData.Okt;
                                        productionPlanDetailElectric.Value = y.ElectricData.Okt;
                                        break;
                                    case 11:
                                        productionPlanDetailSteam.Value = y.SteamData.Nov;
                                        productionPlanDetailElectric.Value = y.ElectricData.Nov;
                                        break;
                                    case 12:
                                        productionPlanDetailSteam.Value = y.SteamData.Des;
                                        productionPlanDetailElectric.Value = y.ElectricData.Des;
                                        break;
                                }
                                productionPlanSteam.ProductionPlanDetails.Add(productionPlanDetailSteam);
                                productionPlanElectric.ProductionPlanDetails.Add(productionPlanDetailElectric);
                            }
                            savedProductionPlans.Add(productionPlanSteam);
                            savedProductionPlans.Add(productionPlanElectric);
                        }
                        else
                        {
                            result.Status = HttpStatusCode.NotFound.GetHashCode();
                            result.Message = "ProductionPlanId tidak valid!";
                        }
                    }
                    //save
                    if(result.Status == HttpStatusCode.OK.GetHashCode())
                    {
                        if (savedProductionPlanDetails.Any())
                        {
                            try
                            {
                                RepoProductionPlanDetail.SaveAll(savedProductionPlanDetails);
                                result.Status = HttpStatusCode.OK.GetHashCode();
                            }
                            catch(Exception e)
                            {
                                result.Status = HttpStatusCode.NotFound.GetHashCode();
                                result.Message = "Error update data!";
                            }
                        }

                        if (savedProductionPlans.Any() && result.Status == HttpStatusCode.OK.GetHashCode())
                        {
                            try
                            {
                                RepoProductionPlan.SaveAll(savedProductionPlans);
                                result.Status = HttpStatusCode.OK.GetHashCode();
                            }
                            catch (Exception e)
                            {
                                result.Status = HttpStatusCode.NotFound.GetHashCode();
                                result.Message = "Error create data!";
                            }
                        }
                    }
                }
                else
                {
                    result.Status = HttpStatusCode.NotFound.GetHashCode();
                    result.Message = request.PowerPlanName + " tidak berada dalam Area " + request.AreaName;
                }
            }
            else
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "User tidak valid!";
            }
            
            return result;
        }

        [HttpPost]
        public async Task<ServiceListStub<ProductionPlanServiceStub>> GetProductionPlan([FromBody] ProductionPlanParamStub param)
        {
            ServiceListStub<ProductionPlanServiceStub> result = new ServiceListStub<ProductionPlanServiceStub>();

            FilterQuery filterQuery = null;
            List<SortQuery> sortQueries = null;
            List<ProductionPlan> productionPlans = null;
            List<ProductionPlanServiceStub> model = new List<ProductionPlanServiceStub>();
            //Task<List<ProductionPlan>> productionPlansTask = null;
            //Task<int> countTask = null;
            int count;

            if (param != null)
            {
                filterQuery = new FilterQuery();
                sortQueries = new List<SortQuery>();

                if(param.areaId != null)
                {
                    if(param.areaId.Value != 0)
                    {
                        filterQuery.AddFilter(new FieldHelper("PowerPlant", "AreaId"), FilterOperator.Equals, param.areaId);
                    }
                }
                if(param.powerPlanId != null)
                {
                    if(param.powerPlanId.Value != 0)
                    {
                        filterQuery.AddFilter(Field.PowerPlantId, FilterOperator.Equals, param.powerPlanId);
                    }
                }
                if(param.year != null)
                {
                    if(param.year.Value != 0)
                    {
                        filterQuery.AddFilter(Field.Year, FilterOperator.Equals, param.year);
                    }
                }

                if(param.sortField != null && param.sortDirection != null)
                {
                    if((param.sortField == Field.AreaId || param.sortField == Field.PowerPlantId || param.sortField == Field.Year) &&
                        (param.sortDirection == SortOrder.Ascending || param.sortDirection == SortOrder.Descending))
                    {
                        sortQueries.Add(new SortQuery(param.sortField, param.sortDirection));
                    }
                }
            }

            if(param != null)
            {
                //productionPlansTask = RepoProductionPlan.FindAllAsync(param.skip,param.take,sortQueries,filterQuery);
                productionPlans = await RepoProductionPlan.FindAllAsync(sortQueries, filterQuery);
            }
            else
            {
                //productionPlansTask = RepoProductionPlan.FindAllAsync();
                productionPlans = await RepoProductionPlan.FindAllAsync();

            }
            //countTask = RepoProductionPlan.CountAsync(filterQuery);

            //await Task.WhenAll(productionPlansTask, countTask);

            //count = countTask.Result;
            //productionPlans = productionPlansTask.Result;

            if (productionPlans.Any())
            {
                List<int?> areaId = productionPlans.Select(y => y.PowerPlant.AreaId).Distinct().ToList();
                List<int> powerPlanId = productionPlans.Select(y => y.PowerPlantId).Distinct().ToList();
                List<int> year = productionPlans.Select(y => y.Year).Distinct().ToList();

                if (areaId.Any())
                {
                    foreach(int? aId in areaId)
                    {
                        List<ProductionPlan> productionPlansByArea = productionPlans.FindAll(p => p.PowerPlant.AreaId == aId);

                        if (powerPlanId.Any())
                        {
                            foreach(int pPId in powerPlanId)
                            {
                                List<ProductionPlan> productionPlansByPowerPlanId = productionPlansByArea.FindAll(p => p.PowerPlantId == pPId);

                                List<ProductionPlanDetailServiceStub> productionPlanDetailServiceStubByYear = new List<ProductionPlanDetailServiceStub>();

                                if (year.Any())
                                {
                                    foreach (int y in year)
                                    {
                                        List<ProductionPlan> productionPlansByYear = productionPlansByPowerPlanId.FindAll(p => p.Year == y);
                                        if (productionPlansByYear.Any())
                                        {
                                            ProductionPlan productionPlanSteam = productionPlansByYear.FirstOrDefault(p => p.UtilType == ProductionUtilization.STEAM.ToString());
                                            ProductionPlan productionPlanElectric = productionPlansByYear.FirstOrDefault(p => p.UtilType == ProductionUtilization.ELECTRIC.ToString());
                                            if(productionPlanSteam != null && productionPlanElectric != null)
                                            {
                                                List<ProductionPlanDetail> productionPlanDetailsSteam = productionPlanSteam.ProductionPlanDetails.ToList();
                                                List<ProductionPlanDetail> productionPlanDetailsElectric = productionPlanElectric.ProductionPlanDetails.ToList();
                                                ProductionPlanDetailServiceStub productionPlanDetailServiceStub = new ProductionPlanDetailServiceStub(y, productionPlanDetailsSteam, productionPlanDetailsElectric);
                                                productionPlanDetailServiceStubByYear.Add(productionPlanDetailServiceStub);
                                            }
                                        }
                                    }
                                }
                                ProductionPlanServiceStub productionPlanServiceStub = new ProductionPlanServiceStub(productionPlansByPowerPlanId.FirstOrDefault(), productionPlanDetailServiceStubByYear);
                                model.Add(productionPlanServiceStub);
                            }
                        }
                    }
                }
            }

            if (model.Any())
            {
                count = model.Count;
                if(param != null)
                {
                    if(param.skip != null)
                    {
                        model = model.Skip(param.skip.Value).ToList();
                    }
                    if (param.take != null)
                    {
                        model = model.Take(param.take.Value).ToList();
                    }
                }

                result.Status = HttpStatusCode.OK.GetHashCode();
                result.Message = "Data found";
                //result.Total = model.Count;
                result.Total = count;
                result.Data = model;
            }
            else
            {
                result.Status = HttpStatusCode.NotFound.GetHashCode();
                result.Message = "Data not found";
            }

            return result;
        }

    }
}