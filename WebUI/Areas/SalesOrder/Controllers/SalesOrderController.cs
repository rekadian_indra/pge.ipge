﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Web;
using System.Linq;
using WebUI.Extension;
using Resources;
using WebUI.Areas.SalesOrder.Models;
using System.IO;
using NPOI.XSSF.UserModel;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.SalesOrder.Controllers
{
    [Authorize]
    [CheckSessionTimeOut]
    public class SalesOrderController : BaseController<TaskReportFormStub>
    {
        #region Constructor

        public SalesOrderController()
        {
        }

        public SalesOrderController
        (
            ILogRepository repoLog,
            IWorkflowRepository repoWorkflow,
            IPowerPlantRepository repoPowerPlant,
            ITaskReportRepository repoTaskReport,
            ISalesOrderRepository repoSalesOrder,
            IBastFileRepository repoBastFile,
            IBastFileSignedRepository repoBastFileSigned,
            ISteamPowerPlantPriceRepository repoSteamPowerPlantPrice,
            IPpiRepository repoPPI,
            ISalesOrderApprovalRepository repoSalesOrderApproval,
            IMembershipRepository repoMembership,
            IScreenshotSapRepository repoScreenshotSap
        )
        {
            RepoLog = repoLog;
            RepoWorkflow = repoWorkflow;
            RepoPowerPlant = repoPowerPlant;
            RepoTaskReport = repoTaskReport;
            RepoSalesOrder = repoSalesOrder;
            RepoBastFile = repoBastFile;
            RepoBastFileSigned = repoBastFileSigned;
            RepoScreenshotSap = repoScreenshotSap;
            RepoSteamPowerPlantPrice = repoSteamPowerPlantPrice;
            RepoPPI = repoPPI;
            RepoSalesOrderApproval = repoSalesOrderApproval;
            RepoMembership = repoMembership;
        }

        #endregion

        #region View

        [MvcSiteMapNode(Title = TitleSite.SalesOrder, Key = KeySite.IndexSalesOrder, ParentKey = KeySite.Dashboard)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [HttpPost]
        public async Task<ActionResult> LoadCreateZPGE(SalesOrderFormStub model)
        {
            if (ModelState.IsValid)
            {
                return await CreateSOZPGE(model);
            }

            //kamus
            FilterQuery filter;
            //List<Business.Entities.TaskReport> reports;
            List<Business.Entities.SalesOrder> existingSalesOrders;
            List<Business.Entities.TaskReport> reportsFilter;
            List<Business.Entities.Pltp> pltps;
            //List<Business.Entities.Pltp> temp;
            List<Business.Entities.TaskReport> reportNotApprove;
            List<string> errors = new List<string>();
            string error;
            string body, subject;

            List<SortQuery> sorts = new List<SortQuery>();
            SortQuery sort;
            List<Business.Entities.Workflow> workflows;
            IEnumerable<Membership> memberships;

            //algo
            filter = new FilterQuery(FilterLogic.And);
            if (model.PowerPlant != null)
                filter.AddFilter(Field.AreaId, FilterOperator.Equals, model.PowerPlant.AreaId);

            memberships = RepoMembership.FindAll(filter);

            filter = new FilterQuery(FilterLogic.And);
            existingSalesOrders = RepoSalesOrder.GetFromPeriodeSOTypePowerPlant(model.Periode, SOType.ZPGE.ToString(), model.ShipToPartyId.Value);

            reportsFilter = RepoTaskReport.GetFromPowerPlantIdPeriode(model.ShipToPartyId, model.Periode);
            pltps = reportsFilter.SelectMany(x => x.Pltps.Where(y => y.PowerPlantId == model.ShipToPartyId)).ToList();

            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZPGE);
            sort = new SortQuery(Field.Id, SortOrder.Ascending);
            sorts.Add(sort);
            workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

            model.SetWorkflow(workflows);
            model.SOType = SOType.ZPGE.ToString();


            if (existingSalesOrders.Any())
            {
                Business.Entities.PowerPlant curPowerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(model.ShipToPartyId);
                model.SetPowerPlant(curPowerPlant);

                error = $"Sales Order untuk {curPowerPlant.Name} periode {model.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)} sudah ada";
                errors.Add(error);
                model.SetValidationMessage(errors);
            }
            else
            {
                if (!reportsFilter.Any())
                {
                    error = $"Belum terdapat data operation untuk periode {model.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
                    errors.Add(error);
                    model.SetValidationMessage(errors);
                }
                else
                {
                    reportNotApprove = reportsFilter.Where(x => x.Status == DataStatus.WAITING.ToString()).ToList();
                    if (reportNotApprove.Any())
                    {
                        foreach (TaskReport t in reportNotApprove)
                        {
                            error = $"Data pada periode {t.Periode.Value.ToString(DisplayFormat.ShortDateFormat)} belum disetujui";
                            errors.Add(error);
                        }
                        model.SetValidationMessage(errors);
                    }
                    else
                    {

                        Business.Entities.PowerPlant curPowerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(model.ShipToPartyId);
                        double netPrice = 0;
                        if (curPowerPlant.Material == Material.ELECTRIC_POWER.ToString())
                        {
                            PPI curPPI = RepoPPI.GetPrice(model.ShipToPartyId.Value, model.Periode.Value);
                            if (curPPI == null) // blm ada harga yg masuk
                            {
                                error = $"Belum terdapat data harga untuk periode {model.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
                                errors.Add(error);
                                model.SetValidationMessage(errors);
                            }
                            else if (curPPI.Workflow.Approver != null) // belum finish, ga bisa diambil
                            {
                                error = $"Data harga untuk periode {model.Periode.Value.ToString(DisplayFormat.ShortDateFormat)} belum disetujui";
                                errors.Add(error);
                                model.SetValidationMessage(errors);
                            }
                            else
                            {
                                netPrice = curPPI.NextPrice;
                            }
                        }
                        else // if (curPowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString())
                        {
                            //SteamPowerPlantPrice curSteamPowerPlantPrice = RepoSteamPowerPlantPrice.GetPrice(model.ShipToPartyId.Value, model.Periode.Value);
                            List<SortQuery> sortings = new List<SortQuery> { new SortQuery(Field.Id, SortOrder.Descending) };

                            filter = new FilterQuery(FilterLogic.And);
                            filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, model.ShipToPartyId);
                            filter.AddFilter(Field.StartDate, FilterOperator.LessThanOrEquals, model.Periode);
                            filter.AddFilter(Field.EndDate, FilterOperator.GreaterThanOrEquals, model.Periode);

                            SteamPowerPlantPrice curSteamPowerPlantPrice = RepoSteamPowerPlantPrice.Find(sortings, filter);

                            if (curSteamPowerPlantPrice == null) // blm ada harga yg masuk
                            {
                                error = $"Belum terdapat data harga untuk periode {model.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
                                errors.Add(error);
                                model.SetValidationMessage(errors);
                            }
                            else if (curSteamPowerPlantPrice.Workflow.Approver != null) // belum finish, ga bisa diambil
                            {
                                error = $"Data harga untuk periode {model.Periode.Value.ToString(DisplayFormat.ShortDateFormat)} belum disetujui";
                                errors.Add(error);
                                model.SetValidationMessage(errors);
                            }
                            else
                            {
                                netPrice = curSteamPowerPlantPrice.Price;
                            }
                        }

                        if (netPrice != 0) // kalau dapet harganya, baru proses
                        {
                            //if (model.ProcessPltps(pltps))
                            if (model.ProcessPltps(pltps, netPrice))
                            {
                                model.Pltps = pltps.OrderBy(pltp => pltp.TaskReport.Periode).ToList();

                                //model.SetPowerPlant(pltps.First().PowerPlant);
                                model.IsValid = true;
                                //model.Quantity = pltps.Sum(x => x.EnergyNett).ToFloat();
                                model.NetPrice = netPrice;
                                model.SetWorkflowId(model.Workflows.FirstOrDefault(x => x.PreviousStep == null).Id);
                                model.IsEditable = true;
                                model.IsRevisable = false;
                                model.IsApprovable = true;
                                model.NetValue = model.Quantity * model.NetPrice;
                                model.Currency = "USDN";
                            }
                            else
                            {
                                error = $"Belum terdapat data operation untuk periode {model.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
                                errors.Add(error);
                                model.SetValidationMessage(errors);
                            }
                        }
                    }
                }
            }           

            return View("FormZPGE", model);
        }

        [HttpPost]
        public async Task<ActionResult> LoadCreateZTS(FormZtsStub model)
        {
            if (ModelState.IsValid)
            {
                return await CreateSOZTS(model);
            }

            //kamus
            FilterQuery filter;
            List<Business.Entities.SalesOrder> existingSalesOrders;
            List<Business.Entities.TaskReport> reportsFilter;
            List<Business.Entities.TaskReport> reportNotApprove;
            List<string> errors = new List<string>();
            string error;

            List<SortQuery> sorts = new List<SortQuery>();
            SortQuery sort;
            List<Business.Entities.Workflow> workflows;

            //algo
            filter = new FilterQuery(Field.SOType, FilterOperator.Equals, SOType.ZTS);
            List<Business.Entities.PowerPlant> powerPlants = await RepoPowerPlant.FindAllAsync(filter);

            existingSalesOrders = RepoSalesOrder.GetFromPeriodeSOType(model.Periode, SOType.ZTS.ToString());
            reportsFilter = RepoTaskReport.GetFromPeriode(model.Periode);

            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZTS);
            sort = new SortQuery(Field.Id, SortOrder.Ascending);
            sorts.Add(sort);
            workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

            model.SetWorkflow(workflows);
            model.SOType = SOType.ZTS.ToString();

            if (existingSalesOrders.Any())
            {
                error = $"Sales Order untuk periode {model.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)} sudah ada";
                errors.Add(error);
                model.SetValidationMessage(errors);
            }
            else
            {
                //if (!reportsFilter.Any())
                //{
                //	error = $"Belum terdapat data operation untuk periode {model.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
                //	errors.Add(error);
                //	model.SetValidationMessage(errors);
                //}
                //else
                //{	
                //	reportNotApprove = reportsFilter.Where(x => x.Status == DataStatus.WAITING.ToString()).ToList();
                //	if (reportNotApprove.Any())
                //	{
                //		foreach (TaskReport t in reportNotApprove)
                //		{
                //			error = $"Data {t.Area.Name} pada periode {t.Periode.Value.ToString(DisplayFormat.ShortDateFormat)} belum disetujui";
                //			errors.Add(error);
                //		}
                //		model.SetValidationMessage(errors);
                //	}
                //	else
                //	{
                List<PPI> ppis = new List<PPI>();
                List<SteamPowerPlantPrice> steamPowerPlantPrices = new List<SteamPowerPlantPrice>();
                //foreach (TaskReport taskReport in reportsFilter)
                //{
                //	foreach (Business.Entities.Pltp pltp in taskReport.Pltps)
                //	{
                //		if (pltp.PowerPlantId.HasValue)
                //		{
                //			if (pltp.PowerPlant.SOType == SOType.ZTS.ToString())
                //			{
                foreach (Business.Entities.PowerPlant powerPlant in powerPlants)
                {
                    if (powerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString())
                    {
                        PPI curPPI = RepoPPI.GetPrice(powerPlant.Id, model.Periode.Value);
                        if (curPPI != null)
                            if (!ppis.Where(x => x.Id == curPPI.Id).Any())
                            {
                                ppis.Add(curPPI);
                            }
                    }
                    else // if (curPowerPlant.Material == Common.Enums.Material.GEOTHERMAL_STEAM.ToString())
                    {
                        //SteamPowerPlantPrice cursteamPowerPlantPrice = RepoSteamPowerPlantPrice.GetPrice(powerPlant.Id, model.Periode.Value);
                        List<SortQuery> sortings = new List<SortQuery> { new SortQuery(Field.Id, SortOrder.Descending) };

                        filter = new FilterQuery(FilterLogic.And);
                        filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, powerPlant.Id);
                        filter.AddFilter(Field.StartDate, FilterOperator.LessThanOrEquals, model.Periode);
                        filter.AddFilter(Field.EndDate, FilterOperator.GreaterThanOrEquals, model.Periode);

                        SteamPowerPlantPrice cursteamPowerPlantPrice = RepoSteamPowerPlantPrice.Find(sortings, filter);

                        if (cursteamPowerPlantPrice != null)
                            if (!steamPowerPlantPrices.Where(x => x.Id == cursteamPowerPlantPrice.Id).Any())
                            {
                                steamPowerPlantPrices.Add(cursteamPowerPlantPrice);
                            }
                    }
                }
                //			}
                //		}
                //	}
                //}

                model.IsEditable = true;
                model.IsApprovable = true;
                model.IsRevisable = false;
                if (model.PowerPlants == null)
                {
                    //model.ProcessTaskReports(reportsFilter, ppis, steamPowerPlantPrices);
                    //if (model.PowerPlants.Count == 0)
                    //{
                    //	error = $"Belum terdapat data operation untuk periode {model.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
                    //	errors.Add(error);
                    //	model.SetValidationMessage(errors);
                    //}
                    model.ProcessPowerPlants(powerPlants, ppis, steamPowerPlantPrices);
                }
                else
                {
                    model.SetActiveWorkflow();
                }

                if (model.ValidationMessages?.Count() > 0)
                {
                    model.PowerPlants = null;
                }
                //	}
                //}
            }

            return View("FormZTS", model);
        }

        #endregion

        #region Create

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.ASMEN })]
        [MvcSiteMapNode(Title = TitleSite.CreateSalesOrderZPGE, Key = KeySite.CreateSalesOrder, ParentKey = KeySite.IndexSalesOrder)]
        public async Task<ActionResult> CreateSOZPGE()
        {
            //kamus
            FilterQuery filter;
            List<SortQuery> sorts = new List<SortQuery>();
            SortQuery sort;
            List<Business.Entities.Workflow> workflows;
            SalesOrderFormStub model;

            //algo
            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZPGE);
            sort = new SortQuery(Field.Id, SortOrder.Ascending);
            sorts.Add(sort);
            workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

            model = new SalesOrderFormStub(workflows);
            model.SOType = SOType.ZPGE.ToString();

            return View("FormZPGE", model);
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.ASMEN })]
        [MvcSiteMapNode(Title = TitleSite.CreateSalesOrderZTS, Key = KeySite.CreateSalesOrderZTS, ParentKey = KeySite.IndexSalesOrder)]
        public async Task<ActionResult> CreateSOZTS()
        {
            //kamus
            FilterQuery filter;
            List<SortQuery> sorts = new List<SortQuery>();
            SortQuery sort;
            List<Business.Entities.Workflow> workflows;
            FormZtsStub model;

            //algo
            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZTS);
            sort = new SortQuery(Field.Id, SortOrder.Ascending);
            sorts.Add(sort);
            workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

            model = new FormZtsStub(workflows);
            model.SOType = SOType.ZTS.ToString();

            return View("FormZTS", model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateSOZPGE(SalesOrderFormStub model)
        {
            //kamus
            FilterQuery filter;
            List<Business.Entities.Workflow> workflows;
            Business.Entities.SalesOrder salesOrder = new Business.Entities.SalesOrder();
            ResponseModel response = new ResponseModel(true);

            //algoritma
            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, model.SOType);


            if (model.WorkflowId.HasValue)
            {
                filter.AddFilter(Field.PreviousStep, FilterOperator.Equals, model.WorkflowId);
                workflows = await RepoWorkflow.FindAllAsync(filter);
            }
            else
                workflows = await RepoWorkflow.FindAllAsync(filter);

            if (ModelState.IsValid)
            {
                if (workflows != null)
                {
                    model.MapDbObject(salesOrder);
                    salesOrder.WorkflowId = workflows.ElementAt(0).Id;

                    await RepoSalesOrder.SaveAsync(salesOrder);

                    //List<BastFile> bastFiles = new List<BastFile>();
                    foreach (BastFileFormStub bastFileStub in model.BastFiles)
                    {
                        BastFile bastFileDb = new BastFile();
                        bastFileStub.SalesOrderId = salesOrder.Id;
                        bastFileStub.MapDbObject(bastFileDb);
                        //bastFiles.Add(bastFileDb);
                        await RepoBastFile.SaveAsync(bastFileDb);
                    }

                    // masukin ke history approval
                    SalesOrderApprovalFormStub salesOrderApproval = new SalesOrderApprovalFormStub()
                    {
                        SalesOrderId = salesOrder.Id,
                        WorkflowId = salesOrder.WorkflowId.Value,
                        Notes = ""
                    };
                    SalesOrderApproval salesOrderApprovalDb = new SalesOrderApproval();
                    salesOrderApproval.MapDbObject(salesOrderApprovalDb);
                    RepoSalesOrderApproval.Save(salesOrderApprovalDb);

                    //send email
                    await SOEmailSender(salesOrder, salesOrderApprovalDb.Id);

                    return RedirectToAction("Index");
                }
            }
            return View("FormZPGE", model);
            //return await CreateSOZPGE(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateSOZTS(FormZtsStub model)
        {
            //kamus
            FilterQuery filter;
            List<Business.Entities.Workflow> workflows;
            ResponseModel response = new ResponseModel(true);
            Business.Entities.SalesOrder salesOrderDb = null;

            //algoritma
            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, model.SOType);
            workflows = await RepoWorkflow.FindAllAsync(filter);

            if (ModelState.IsValid)
            {
                if (workflows != null)
                {
                    foreach (PowerPlantFormStub powerPlant in model.PowerPlants)
                    {
                        foreach (SalesOrderFormStub salesOrder in powerPlant.SalesOrders)
                        {
                            if (salesOrder.IsValid)
                            {
                                IEnumerable<Business.Entities.Workflow> curWorkflows;
                                if (model.IsDraft) curWorkflows = workflows.Where(x => x.PreviousStep == null);
                                else curWorkflows = workflows.Where(x => x.PreviousStep == salesOrder.WorkflowId);

                                if (curWorkflows.Any())
                                {
                                    salesOrderDb = new Business.Entities.SalesOrder();
                                    salesOrder.WorkflowId = curWorkflows.ElementAt(0).Id;
                                    salesOrder.MapDbObject(salesOrderDb);

                                    await RepoSalesOrder.SaveAsync(salesOrderDb);

                                    //List<BastFile> bastFiles = new List<BastFile>();
                                    foreach (BastFileFormStub bastFileStub in salesOrder.BastFiles)
                                    {
                                        if (bastFileStub.FilePath != null)
                                        {
                                            BastFile bastFileDb = new BastFile();
                                            bastFileStub.SalesOrderId = salesOrderDb.Id;
                                            bastFileStub.MapDbObject(bastFileDb);
                                            //bastFiles.Add(bastFileDb);
                                            await RepoBastFile.SaveAsync(bastFileDb);
                                        }
                                    }

                                    //foreach (ScreenshotSapFormStub screenshotSapStub in salesOrder.ScreenshotSaps)
                                    //{
                                    //    if (screenshotSapStub.FilePath != null)
                                    //    {
                                    //        ScreenshotSap screenshotSapDb = new ScreenshotSap();
                                    //        screenshotSapStub.SalesOrderId = salesOrder.Id;
                                    //        screenshotSapStub.MapDbObject(screenshotSapDb);
                                    //        await RepoScreenshotSap.SaveAsync(screenshotSapDb);
                                    //    }                                        
                                    //}

                                    // masukin ke history approval
                                    SalesOrderApprovalFormStub salesOrderApproval = new SalesOrderApprovalFormStub()
                                    {
                                        SalesOrderId = salesOrderDb.Id,
                                        WorkflowId = salesOrderDb.WorkflowId.Value,
                                        Notes = ""
                                    };
                                    SalesOrderApproval salesOrderApprovalDb = new SalesOrderApproval();
                                    salesOrderApproval.MapDbObject(salesOrderApprovalDb);
                                    RepoSalesOrderApproval.Save(salesOrderApprovalDb);

                                    //send email
                                    await SOEmailSender(salesOrderDb, salesOrderApprovalDb.Id);
                                }
                            }
                        }
                    }

                    return RedirectToAction("Index");
                }
            }
            return View("FormZTS", model);
            //return await CreateSOZPGE(model);
        }


        [HttpPost]
        public async Task<ActionResult> CreateBastFileSigned(int id, string filePath)
        {
            //kamus
            string message;
            ResponseModel response = new ResponseModel(true);
            BastFileSignedFormStub bastFileSigned;
            BastFileSigned bastFileSignedDb = new BastFileSigned();

            //algoritma
            bastFileSigned = new BastFileSignedFormStub()
            {
                FilePath = filePath,
                SalesOrderId = id
            };
            bastFileSigned.MapDbObject(bastFileSignedDb);

            if (!(await RepoBastFileSigned.SaveAsync(bastFileSignedDb)))
            {
                message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
            //return await CreateSOZPGE(model);
        }


        #endregion

        #region Edit

        [MvcSiteMapNode(Title = TitleSite.EditSalesOrderZPGE, Key = KeySite.EditSalesOrderZPGE, ParentKey = KeySite.IndexSalesOrder)]
        public async Task<ActionResult> EditSOZPGE(params object[] id)
        {
            //kamus
            FilterQuery filter;
            SortQuery sort;
            List<SortQuery> sorts = new List<SortQuery>();
            List<Business.Entities.Workflow> workflows;
            List<Business.Entities.TaskReport> reportsFilter;
            List<Business.Entities.Pltp> pltps;

            //algo
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZPGE);
            sort = new SortQuery(Field.Id, SortOrder.Ascending);
            sorts.Add(sort);
            workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

            SalesOrderFormStub model = new SalesOrderFormStub(dbObject, workflows);

            //ambil powerplant dan hitung quantity
            reportsFilter = RepoTaskReport.GetFromPowerPlantIdPeriode(model.ShipToPartyId, model.Periode);
            pltps = reportsFilter.SelectMany(x => x.Pltps.Where(y => y.PowerPlantId == model.ShipToPartyId)).ToList();
            if (model.ProcessPltps(pltps))
            {
                SalesOrderApproval salesOrderApproval = await RepoSalesOrderApproval.GetLastApproval(primaryKey);
                model.LastSalesOrderApproval = salesOrderApproval;

                model.Pltps = pltps.OrderBy(pltp => pltp.TaskReport.Periode).ToList();

                model.IsValid = true;
                return View("FormZPGE", model);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> EditSOZPGE(SalesOrderFormStub model)
        {
            //kamus
            FilterQuery filter;
            List<Business.Entities.Workflow> workflows;
            Business.Entities.SalesOrder salesOrder = new Business.Entities.SalesOrder();
            ResponseModel response = new ResponseModel(true);

            //algoritma
            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, model.SOType);
            filter.AddFilter(Field.PreviousStep, FilterOperator.Equals, model.WorkflowId);
            workflows = await RepoWorkflow.FindAllAsync(filter);

            if (model.BastFiles == null)
            {
                filter = new FilterQuery(Field.SalesOrderId, FilterOperator.Equals, model.Id);
                List<BastFile> bastFiles = await RepoBastFile.FindAllAsync(filter);

                if (bastFiles.Count > 0)
                {
                    // soalnya file2 yg lama nya ga perlu di add ulang
                    model.BastFiles = new List<BastFileFormStub>();
                }
            }

            if ((ModelState.IsValid) || ((model.BastFiles != null) && (model.BastFileSigneds != null)))
            {
                if (workflows != null)
                {
                    salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(model.Id);
                    model.MapDbObject(salesOrder);
                    salesOrder.WorkflowId = workflows.ElementAt(0).Id;

                    await RepoSalesOrder.SaveAsync(salesOrder);

                    //List<BastFile> bastFiles = new List<BastFile>();
                    foreach (BastFileFormStub bastFileStub in model.BastFiles)
                    {
                        BastFile bastFileDb = new BastFile();
                        bastFileStub.SalesOrderId = salesOrder.Id;
                        bastFileStub.MapDbObject(bastFileDb);
                        //bastFiles.Add(bastFileDb);
                        await RepoBastFile.SaveAsync(bastFileDb);
                    }

                    // masukin ke history approval
                    SalesOrderApprovalFormStub salesOrderApproval = new SalesOrderApprovalFormStub()
                    {
                        SalesOrderId = salesOrder.Id,
                        WorkflowId = salesOrder.WorkflowId.Value,
                        Notes = ""
                    };
                    SalesOrderApproval salesOrderApprovalDb = new SalesOrderApproval();
                    salesOrderApproval.MapDbObject(salesOrderApprovalDb);
                    RepoSalesOrderApproval.Save(salesOrderApprovalDb);

                    //send email
                    await SOEmailSender(salesOrder, salesOrderApprovalDb.Id);

                    return RedirectToAction("Index");
                }
            }
            //return View("FormZPGE", model);
            return await EditSOZPGE(model.Id);
        }

        [MvcSiteMapNode(Title = TitleSite.EditSalesOrderZTS, Key = KeySite.EditSalesOrderZTS, ParentKey = KeySite.IndexSalesOrder)]
        public async Task<ActionResult> EditSOZTS(params object[] id)
        {
            //kamus
            FilterQuery filter;
            SortQuery sort;
            List<SortQuery> sorts = new List<SortQuery>();
            List<Business.Entities.Workflow> workflows;

            //algo
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

            List<Business.Entities.SalesOrder> salesOrders = RepoSalesOrder.GetFromPeriodeSOType(dbObject.Periode, SOType.ZTS.ToString());

            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZTS);
            sort = new SortQuery(Field.Id, SortOrder.Ascending);
            sorts.Add(sort);
            workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

            FormZtsStub model = new FormZtsStub(workflows);
            SalesOrderApproval salesOrderApproval = await RepoSalesOrderApproval.GetLastApproval(primaryKey);
            model.LastSalesOrderApproval = salesOrderApproval;

            List<Business.Entities.PowerPlant> powerPlants = salesOrders.Select(x => x.PowerPlant).ToList();
            List<PPI> ppis = new List<PPI>();
            List<SteamPowerPlantPrice> steamPowerPlantPrices = new List<SteamPowerPlantPrice>();
            foreach (Business.Entities.PowerPlant powerPlant in powerPlants)
            {
                if (powerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString())
                {
                    PPI curPPI = RepoPPI.GetPrice(powerPlant.Id, dbObject.Periode.Value);
                    if (curPPI != null)
                        if (!ppis.Where(x => x.Id == curPPI.Id).Any())
                        {
                            ppis.Add(curPPI);
                        }
                }
                else // if (curPowerPlant.Material == Common.Enums.Material.GEOTHERMAL_STEAM.ToString())
                {
                    //SteamPowerPlantPrice cursteamPowerPlantPrice = RepoSteamPowerPlantPrice.GetPrice(powerPlant.Id, dbObject.Periode.Value);
                    List<SortQuery> sortings = new List<SortQuery> { new SortQuery(Field.Id, SortOrder.Descending) };

                    filter = new FilterQuery(FilterLogic.And);
                    filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, powerPlant.Id);
                    filter.AddFilter(Field.StartDate, FilterOperator.LessThanOrEquals, dbObject.Periode);
                    filter.AddFilter(Field.EndDate, FilterOperator.GreaterThanOrEquals, dbObject.Periode);

                    SteamPowerPlantPrice cursteamPowerPlantPrice = RepoSteamPowerPlantPrice.Find(sortings, filter);

                    if (cursteamPowerPlantPrice != null)
                        if (!steamPowerPlantPrices.Where(x => x.Id == cursteamPowerPlantPrice.Id).Any())
                        {
                            steamPowerPlantPrices.Add(cursteamPowerPlantPrice);
                        }
                }
            }

            model.ProcessSalesOrders(salesOrders, ppis, steamPowerPlantPrices);

            return View("FormZTS", model);
        }

        [HttpPost]
        public async Task<ActionResult> EditSOZTS(FormZtsStub model)
        {
            //kamus
            FilterQuery filter;
            List<Business.Entities.Workflow> workflows;
            ResponseModel response = new ResponseModel(true);

            //algoritma
            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, model.SOType);
            workflows = await RepoWorkflow.FindAllAsync(filter);

            foreach (PowerPlantFormStub powerPlant in model.PowerPlants)
            {
                foreach (SalesOrderFormStub salesOrder in powerPlant.SalesOrders)
                {
                    if (salesOrder.IsValid)
                    {
                        if (salesOrder.WorkflowId == 9 && salesOrder.ScreenshotSaps == null)
                        {
                            return await EditSOZTS(model.PowerPlants.ElementAt(0).SalesOrders.ElementAt(0).Id);
                        } else
                        {
                            IEnumerable<Business.Entities.Workflow> curWorkflows;
                            if (model.IsDraft) curWorkflows = workflows.Where(x => x.PreviousStep == null);
                            else curWorkflows = workflows.Where(x => x.PreviousStep == salesOrder.WorkflowId);

                            if (curWorkflows.Any())
                            {
                                if (salesOrder.BastFiles == null)
                                {
                                    filter = new FilterQuery(Field.SalesOrderId, FilterOperator.Equals, salesOrder.Id);
                                    List<BastFile> bastFiles = await RepoBastFile.FindAllAsync(filter);

                                    if (bastFiles.Count > 0)
                                    {
                                        // soalnya file2 yg lama nya ga perlu di add ulang
                                        salesOrder.BastFiles = new List<BastFileFormStub>();
                                    }
                                }

                                if (salesOrder.ScreenshotSaps == null)
                                {
                                    filter = new FilterQuery(Field.SalesOrderId, FilterOperator.Equals, salesOrder.Id);
                                    List<ScreenshotSap> screenshotSaps = await RepoScreenshotSap.FindAllAsync(filter);

                                    if (screenshotSaps.Count > 0)
                                    {
                                        // soalnya file2 yg lama nya ga perlu di add ulang
                                        salesOrder.ScreenshotSaps = new List<ScreenshotSapFormStub>();
                                    }
                                }

                                if ((ModelState.IsValid) || (salesOrder.BastFiles != null) || (salesOrder.ScreenshotSaps != null))
                                {
                                    if (workflows != null)
                                    {
                                        Business.Entities.SalesOrder salesOrderDb = new Business.Entities.SalesOrder();
                                        if (salesOrder.Id != 0)
                                        {
                                            salesOrderDb = await RepoSalesOrder.FindByPrimaryKeyAsync(salesOrder.Id);
                                        }
                                        salesOrder.MapDbObject(salesOrderDb);
                                        salesOrderDb.WorkflowId = curWorkflows.ElementAt(0).Id;
                                        await RepoSalesOrder.SaveAsync(salesOrderDb);

                                        foreach (BastFileFormStub bastFileStub in salesOrder.BastFiles)
                                        {
                                            BastFile bastFileDb = new BastFile();
                                            bastFileStub.SalesOrderId = salesOrderDb.Id;
                                            bastFileStub.MapDbObject(bastFileDb);
                                            //bastFiles.Add(bastFileDb);
                                            await RepoBastFile.SaveAsync(bastFileDb);
                                        }

                                        if (salesOrder.WorkflowId == 9)
                                        {                                            
                                            foreach (ScreenshotSapFormStub screenshotSapStub in salesOrder.ScreenshotSaps)
                                            {
                                                ScreenshotSap screenshotSapDb = new ScreenshotSap();
                                                screenshotSapStub.SalesOrderId = salesOrderDb.Id;
                                                screenshotSapStub.MapDbObject(screenshotSapDb);
                                                await RepoScreenshotSap.SaveAsync(screenshotSapDb);
                                            }
                                            
                                        }

                                        // masukin ke history approval
                                        SalesOrderApprovalFormStub salesOrderApproval = new SalesOrderApprovalFormStub()
                                        {
                                            SalesOrderId = salesOrderDb.Id,
                                            WorkflowId = salesOrderDb.WorkflowId.Value,
                                            Notes = ""
                                        };
                                        SalesOrderApproval salesOrderApprovalDb = new SalesOrderApproval();
                                        salesOrderApproval.MapDbObject(salesOrderApprovalDb);
                                        RepoSalesOrderApproval.Save(salesOrderApprovalDb);

                                        //send email
                                        await SOEmailSender(salesOrderDb, salesOrderApprovalDb.Id);
                                    }
                                }
                                else
                                {
                                    return await EditSOZTS(model.PowerPlants.ElementAt(0).SalesOrders.ElementAt(0).Id);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (salesOrder.Id != 0)
                        {
                            Business.Entities.SalesOrder salesOrderDb = await RepoSalesOrder.FindByPrimaryKeyAsync(salesOrder.Id);
                            await RepoSalesOrder.DeleteAsync(salesOrderDb);
                        }
                    }
                }
            }

            return RedirectToAction("Index");

            //return View("FormZTS", model);
        }

        #endregion

        #region Delete

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            // kamus
            FilterQuery filter;
            string message;
            List<Business.Entities.SalesOrder> soZTS = new List<Business.Entities.SalesOrder>();

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);


            if (dbObject.SOType == SOType.ZTS.ToString())
            {
                filter = new FilterQuery(FilterLogic.And);
                filter.AddFilter(Field.Periode, FilterOperator.Equals, dbObject.Periode);
                filter.AddFilter(Field.SOType, FilterOperator.Equals, dbObject.SOType);
                filter.AddFilter(Field.WorkflowId, FilterOperator.Equals, dbObject.WorkflowId);
                soZTS = await RepoSalesOrder.FindAllAsync(filter);

                // hapus data BAST File nya dulu
                filter = new FilterQuery(FilterLogic.Or);
                foreach (Business.Entities.SalesOrder so in soZTS)
                {
                    filter.AddFilter(Field.SalesOrderId, FilterOperator.Equals, dbObject.Id);
                }

                List<BastFile> bastFiles = await RepoBastFile.FindAllAsync(filter);
                foreach (BastFile bastFile in bastFiles)
                {
                    string filePath = Server.MapPath(bastFile.FilePath);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                await RepoBastFile.DeleteAllAsync(bastFiles);

                List<ScreenshotSap> screenshotSaps = await RepoScreenshotSap.FindAllAsync(filter);
                foreach (ScreenshotSap screenshotSap in screenshotSaps)
                {
                    string filePath = Server.MapPath(screenshotSap.FilePath);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                await RepoScreenshotSap.DeleteAllAsync(screenshotSaps);

                // baru hapus sales order nya            
                try
                {
                    await RepoSalesOrder.DeleteAllAsync(soZTS);
                }
                catch (Exception ex)
                {
                    message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                    response.SetFail(message);
                }
            }
            else
            {
                // hapus data BAST File nya dulu
                filter = new FilterQuery(Field.SalesOrderId, FilterOperator.Equals, dbObject.Id);
                List<BastFile> bastFiles = await RepoBastFile.FindAllAsync(filter);
                foreach (BastFile bastFile in bastFiles)
                {
                    string filePath = Server.MapPath(bastFile.FilePath);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                await RepoBastFile.DeleteAllAsync(bastFiles);

                // baru hapus sales order nya            
                if (!(await RepoSalesOrder.DeleteAsync(dbObject)))
                {
                    message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                    response.SetFail(message);
                }
            }



            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteZTS(params object[] id)
        {
            // kamus
            FilterQuery filter;
            //string message;

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

            List<Business.Entities.SalesOrder> deleteDbObjects = RepoSalesOrder.GetFromPeriodeSOType(dbObject.Periode, SOType.ZTS.ToString());

            foreach (Business.Entities.SalesOrder deleteDbObject in deleteDbObjects)
            {
                // hapus data BAST File nya dulu
                filter = new FilterQuery(Field.SalesOrderId, FilterOperator.Equals, deleteDbObject.Id);
                List<BastFile> bastFiles = await RepoBastFile.FindAllAsync(filter);
                foreach (BastFile bastFile in bastFiles)
                {
                    string filePath = Server.MapPath(bastFile.FilePath);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                await RepoBastFile.DeleteAllAsync(bastFiles);

                List<ScreenshotSap> screenshotSaps = await RepoScreenshotSap.FindAllAsync(filter);
                foreach (ScreenshotSap screenshotSap in screenshotSaps)
                {
                    string filePath = Server.MapPath(screenshotSap.FilePath);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                await RepoScreenshotSap.DeleteAllAsync(screenshotSaps);

                // baru hapus sales order nya
                //if (!(await RepoSalesOrder.DeleteAsync(dbObject)))
                //{
                //	message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                //	response.SetFail(message);
                //}
            }
            await RepoSalesOrder.DeleteAllAsync(deleteDbObjects);

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteBastFile(params object[] id)
        {
            // kamus
            //string message;

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.BastFile dbObject = await RepoBastFile.FindByPrimaryKeyAsync(primaryKey);

            List<BastFile> deleteFiles = RepoBastFile.GetFromFilePath(dbObject.FilePath);
            // baru hapus sales order nya
            //if (!(await RepoBastFile.DeleteAsync(dbObject)))
            await RepoBastFile.DeleteAllAsync(deleteFiles);
            //	{
            //	message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
            //	response.SetFail(message);
            //  }
            // else
            // {
            // hapus file nya
            foreach (BastFile deleteFile in deleteFiles)
            {
                string filePath = Server.MapPath(deleteFile.FilePath);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }
            //FileManagementController fmc = new FileManagementController();
            //string[] fileNames = new string[1];
            //fileNames[0] = filePath;
            //fmc.Remove(fileNames);
            // }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteScreenshotSap(params object[] id)
        {
            // kamus
            //string message;

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            ScreenshotSap dbObject = await RepoScreenshotSap.FindByPrimaryKeyAsync(primaryKey);

            List<ScreenshotSap> deleteFiles = RepoScreenshotSap.GetFromFilePath(dbObject.FilePath);
            // baru hapus sales order nya
            
            await RepoScreenshotSap.DeleteAllAsync(deleteFiles);
            
            foreach (ScreenshotSap deleteFile in deleteFiles)
            {
                string filePath = Server.MapPath(deleteFile.FilePath);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }           

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteBastFileSigned(params object[] id)
        {
            // kamus

            // algo
            ResponseModel response = new ResponseModel(true);            
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.BastFileSigned dbObject = await RepoBastFileSigned.FindByPrimaryKeyAsync(primaryKey);

            List<BastFileSigned> deleteFiles = RepoBastFileSigned.GetFromFilePath(dbObject.FilePath);

            await RepoBastFileSigned.DeleteAllAsync(deleteFiles);

            foreach (BastFileSigned deleteFile in deleteFiles)
            {
                string filePath = Server.MapPath(deleteFile.FilePath);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json(response);
        }
        #endregion

        #region history
        [MvcSiteMapNode(Title = TitleSite.HistorySO, Key = KeySite.HistorySalesOrder, ParentKey = KeySite.IndexSalesOrder)]
        public async Task<ActionResult> HistorySO(int id)
        {
            SalesOrderPresentationStub model = new SalesOrderPresentationStub();
            model.Id = id;

            await Task.Delay(0);

            return View(model);
        }
        #endregion

        #region Approval

        [HttpPost]
        public async Task<ActionResult> ApproveSalesOrder(params object[] id)
        {
            // kamus
            FilterQuery filter;
            string message;
            Business.Entities.Workflow workflow;
            List<Business.Entities.Workflow> workflows;

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

            // cari workflow lanjutannya
            filter = new FilterQuery(Field.PreviousStep, FilterOperator.Equals, dbObject.WorkflowId);
            workflow = await RepoWorkflow.FindAsync(filter);
            dbObject.WorkflowId = workflow.Id;


            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZPGE);
            List<SortQuery> sorts = new List<SortQuery>();
            sorts.Add(new SortQuery(Field.Id, SortOrder.Ascending));
            workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

            // baru update data sales order nya
            if (!(await RepoSalesOrder.SaveAsync(dbObject)))
            {
                message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
                response.SetFail(message);
            }
            else
            {
                // masukin ke history approval
                SalesOrderApprovalFormStub salesOrderApproval = new SalesOrderApprovalFormStub()
                {
                    SalesOrderId = primaryKey,
                    WorkflowId = workflow.Id,
                    Notes = ""
                };
                SalesOrderApproval salesOrderApprovalDb = new SalesOrderApproval();
                salesOrderApproval.MapDbObject(salesOrderApprovalDb);
                RepoSalesOrderApproval.Save(salesOrderApprovalDb);

                //send email
                await SOEmailSender(dbObject, salesOrderApprovalDb.Id);
            }

            return Json(response);
        }
        public async Task<ActionResult> ApproveSalesOrderZTS(params object[] id)
        {
            // kamus
            FilterQuery filter;
            //string message;
            Business.Entities.Workflow workflow;
            SalesOrderApprovalFormStub salesOrderApproval;
            SalesOrderApproval salesOrderApprovalDbObject;
            List<SalesOrderApproval> salesOrderApprovalDbObjects = new List<SalesOrderApproval>();

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

            List<Business.Entities.SalesOrder> approvedDbObjects = RepoSalesOrder.GetFromPeriodeSOType(dbObject.Periode, SOType.ZTS.ToString());

            // cari workflow lanjutannya
            filter = new FilterQuery(Field.PreviousStep, FilterOperator.Equals, dbObject.WorkflowId);
            workflow = await RepoWorkflow.FindAsync(filter);

            // baru update data sales order nya
            foreach (Business.Entities.SalesOrder approvedDbObject in approvedDbObjects)
            {
                approvedDbObject.WorkflowId = workflow.Id;

                // masukin ke history approval
                salesOrderApproval = new SalesOrderApprovalFormStub()
                {
                    SalesOrderId = primaryKey,
                    WorkflowId = workflow.Id,
                    Notes = ""
                };
                salesOrderApprovalDbObject = new SalesOrderApproval();
                salesOrderApproval.MapDbObject(salesOrderApprovalDbObject);
                salesOrderApprovalDbObjects.Add(salesOrderApprovalDbObject);

                //send email
                await SOEmailSender(approvedDbObject, salesOrderApprovalDbObject.Id);
            }
            Task taskSalesOrder = RepoSalesOrder.SaveAllAsync(approvedDbObjects);
            Task taskSalesOrderApproval = RepoSalesOrderApproval.SaveAllAsync(salesOrderApprovalDbObjects);

            Task.WaitAll(taskSalesOrder, taskSalesOrderApproval);

            //{
            //	message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
            //	response.SetFail(message);
            //}

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> ReviseSalesOrder(int id, string message = "")
        {
            // kamus
            FilterQuery filter;
            //string message;
            Business.Entities.Workflow workflow;
            bool isSuccess = false;

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = id; // int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

            // cari workflow sebelumnya
            if (dbObject.Workflow != null)
            {
                if (dbObject.Workflow.PreviousStep != null)
                {
                    filter = new FilterQuery(Field.Id, FilterOperator.Equals, dbObject.Workflow.PreviousStep);
                    workflow = await RepoWorkflow.FindAsync(filter);
                    dbObject.WorkflowId = workflow.Id;

                    // update data sales order nya
                    isSuccess = await RepoSalesOrder.SaveAsync(dbObject);

                    // masukin ke history approval
                    SalesOrderApprovalFormStub salesOrderApproval = new SalesOrderApprovalFormStub()
                    {
                        SalesOrderId = id,
                        WorkflowId = workflow.Id,
                        Notes = message
                    };
                    SalesOrderApproval salesOrderApprovalDb = new SalesOrderApproval();
                    salesOrderApproval.MapDbObject(salesOrderApprovalDb);
                    RepoSalesOrderApproval.Save(salesOrderApprovalDb);

                    //send email
                    await SOEmailSender(dbObject, salesOrderApprovalDb.Id, message);
                }
            }

            return RedirectToAction(ActionSite.Index);

            //if (!isSuccess)
            //{
            //	message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
            //	response.SetFail(message);
            //}

            //return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> ReviseSalesOrderZTS(int id, string message = "")
        {
            // kamus
            FilterQuery filter;
            //string message;
            Business.Entities.Workflow workflow;
            SalesOrderApprovalFormStub salesOrderApproval;
            SalesOrderApproval salesOrderApprovalDbObject;
            List<SalesOrderApproval> salesOrderApprovalDbObjects = new List<SalesOrderApproval>();
            //bool isSuccess = false;

            // algo
            ResponseModel response = new ResponseModel(true);
            int primaryKey = id; // int.Parse(id.FirstOrDefault().ToString());
            Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

            List<Business.Entities.SalesOrder> revisedDbObjects = RepoSalesOrder.GetFromPeriodeSOType(dbObject.Periode, SOType.ZTS.ToString());

            // cari workflow sebelumnya
            if (dbObject.Workflow != null)
            {
                if (dbObject.Workflow.PreviousStep != null)
                {
                    filter = new FilterQuery(Field.Id, FilterOperator.Equals, dbObject.Workflow.PreviousStep);
                    workflow = await RepoWorkflow.FindAsync(filter);
                    foreach (Business.Entities.SalesOrder revisedDbObject in revisedDbObjects)
                    {
                        revisedDbObject.WorkflowId = workflow.Id;

                        // masukin ke history approval
                        salesOrderApproval = new SalesOrderApprovalFormStub()
                        {
                            SalesOrderId = id,
                            WorkflowId = workflow.Id,
                            Notes = message
                        };
                        salesOrderApprovalDbObject = new SalesOrderApproval();
                        salesOrderApproval.MapDbObject(salesOrderApprovalDbObject);
                        salesOrderApprovalDbObjects.Add(salesOrderApprovalDbObject);

                        //send email
                        await SOEmailSender(dbObject, salesOrderApprovalDbObject.Id, message);
                    }

                    // update data sales order nya
                    Task taskSalesOrder = RepoSalesOrder.SaveAllAsync(revisedDbObjects);
                    Task taskSalesOrderApproval = RepoSalesOrderApproval.SaveAllAsync(salesOrderApprovalDbObjects);

                    Task.WaitAll(taskSalesOrder, taskSalesOrderApproval);
                }
            }

            //if (!isSuccess)
            //{
            //	message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
            //	response.SetFail(message);
            //}

            return RedirectToAction(ActionSite.Index);

            //return Json(response);
        }

        #endregion

        #region Binding
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            List<Business.Entities.SalesOrder> dbObjectsZPGE;
            int countZPGE;
            List<Business.Entities.SalesOrder> dbObjectsZTS;
            int countZTS;
            List<SalesOrderPresentationStub> tempZTSs;
            List<SalesOrderPresentationStub> tempZPGEs;
            List<SalesOrderPresentationStub> models = new List<SalesOrderPresentationStub>();
            FilterQuery filter;
            SortQuery sort;
            List<SortQuery> sorts = new List<SortQuery>();
            List<Business.Entities.Workflow> workflows;

            Task<List<Business.Entities.SalesOrder>> dbObjectsTaskZPGE = null;
            Task<int> countTaskZPGE = null;
            Task<List<Business.Entities.SalesOrder>> dbObjectsTaskZTS = null;
            Task<List<Business.Entities.SalesOrder>> countTaskZTSObject = null;

            QueryRequestParameter param = QueryRequestParameter.Current;
            FieldHelper fh = new FieldHelper(Table.PowerPlant, Field.AreaId);

            //algorithm
            filter = new FilterQuery();
            if (param.Filter != null) filter = param.Filter;

            filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZPGE);

            if (User.AreaId.HasValue && User.AreaId > 0)
                filter.AddFilter(fh, FilterOperator.Equals, User.AreaId);

            filter.AddFilter(Field.IsDeleted, FilterOperator.Equals, false);


            if (args == null)
            {
                fh = new FieldHelper(Table.Workflow, Field.Code);
                filter.AddFilter(fh, FilterOperator.NotEquals, WorkflowStatus.FINISH);
            }
            else
            {
                fh = new FieldHelper(Table.Workflow, Field.Code);
                filter.AddFilter(fh, FilterOperator.Equals, WorkflowStatus.FINISH);
            }



            dbObjectsTaskZPGE = RepoSalesOrder.FindAllAsync(param.Skip, param.Take, param.Sorts, filter);
            countTaskZPGE = RepoSalesOrder.CountAsync(filter);

            filter = new FilterQuery();
            if (param.Filter != null) filter = param.Filter;
            filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZTS);

            if (User.AreaId.HasValue && User.AreaId > 0)
            {
                fh = new FieldHelper(Table.PowerPlant, Field.AreaId);
                filter.AddFilter(fh, FilterOperator.Equals, User.AreaId);
            }

            filter.AddFilter(Field.IsDeleted, FilterOperator.Equals, false);

            if (args == null)
            {
                fh = new FieldHelper(Table.Workflow, Field.Code);
                filter.AddFilter(fh, FilterOperator.NotEquals, WorkflowStatus.FINISH);
            }
            else
            {
                fh = new FieldHelper(Table.Workflow, Field.Code);
                filter.AddFilter(fh, FilterOperator.Equals, WorkflowStatus.FINISH);
            }


            dbObjectsTaskZTS = RepoSalesOrder.FindAllAsync(param.Sorts, filter);
            countTaskZTSObject = RepoSalesOrder.FindAllAsync(filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTaskZPGE, countTaskZPGE, dbObjectsTaskZTS, countTaskZTSObject);

            //get callback from task
            dbObjectsZPGE = dbObjectsTaskZPGE.Result;
            countZPGE = countTaskZPGE.Result;
            dbObjectsZTS = dbObjectsTaskZTS.Result;
            countZTS = countTaskZTSObject.Result.GroupBy(x => x.Periode).Select(x => x.FirstOrDefault()).Count();

            //map data ZTS ke model
            tempZTSs = ListMapper.MapList<Business.Entities.SalesOrder, SalesOrderPresentationStub>(dbObjectsZTS);
            tempZTSs = tempZTSs.OrderBy(x => x.Periode).ToList();
            foreach (SalesOrderPresentationStub tempZTS in tempZTSs)
            {
                SalesOrderPresentationStub existedModel = models.FirstOrDefault(x => (x.Periode == tempZTS.Periode));

                if (existedModel != null)
                {
                    string[] addedUnits = existedModel.Unit.Split(',');
                    if (!addedUnits.Contains(tempZTS.Unit) && !addedUnits.Contains(" " + tempZTS.Unit))
                    {
                        existedModel.Unit += ", " + tempZTS.Unit;
                    }

                    continue;
                }
                models.Add(tempZTS);
            }

            models = models.Skip(param.Skip).Take(param.Take).ToList();

            //map data ZPGE ke model
            tempZPGEs = ListMapper.MapList<Business.Entities.SalesOrder, SalesOrderPresentationStub>(dbObjectsZPGE);
            models.AddRange(tempZPGEs);

            int count = countZPGE + countZTS;

            // cek approvable / ga nya
            workflows = await RepoWorkflow.FindAllAsync();
            foreach (SalesOrderPresentationStub model in models)
            {
                model.IsApprovable = (model.WorkflowId != null) ? workflows.Where(x => x.PreviousStep == model.WorkflowId).Any() : true;
            }

            models = models.OrderByDescending(x => x.Id).ToList();

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingApproval(params object[] args)
        {
            //lib
            int count;
            List<Business.Entities.SalesOrder> dbObjects;
            List<SalesOrderPresentationStub> models;
            Task<List<Business.Entities.SalesOrder>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;
            FieldHelper fh = new FieldHelper(Table.PowerPlant, Field.AreaId);

            if (param.Filter == null)
                param.InstanceFilter();

            if (User.AreaId.HasValue && User.AreaId > 0)
                param.Filter.AddFilter(fh, FilterOperator.Equals, User.AreaId);

            fh = new FieldHelper(Table.Workflow, Field.Code);
            param.Filter.AddFilter(fh, FilterOperator.Equals, WorkflowStatus.FINISH);

            //algorithm
            dbObjectsTask = RepoSalesOrder.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoSalesOrder.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Business.Entities.SalesOrder, SalesOrderPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingBastFiles(params object[] args)
        {
            //lib
            int count;
            List<Business.Entities.BastFile> dbObjects;
            List<BastFilePresentationStub> models;
            Task<List<Business.Entities.BastFile>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoBastFile.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoBastFile.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Business.Entities.BastFile, BastFilePresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingScreenshotSaps(params object[] args)
        {
            //lib
            int count;
            List<ScreenshotSap> dbObjects;
            List<ScreenshotSapPresentationStub> models;
            Task<List<ScreenshotSap>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoScreenshotSap.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoScreenshotSap.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ScreenshotSap, ScreenshotSapPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingBastFileSigneds(params object[] args)
        {
            //lib
            int count;
            List<Business.Entities.BastFileSigned> dbObjects;
            List<BastFileSignedPresentationStub> models;
            Task<List<Business.Entities.BastFileSigned>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoBastFileSigned.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoBastFileSigned.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Business.Entities.BastFileSigned, BastFileSignedPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingHistorySO(params object[] args)
        {
            //kamus
            int count = 0;
            List<SalesOrderApproval> dbObjects = new List<SalesOrderApproval>();
            List<SalesOrderApprovalPresentationStub> models =  new List<SalesOrderApprovalPresentationStub>();
            Task<List<SalesOrderApproval>> taskDbObjects = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            List<Business.Entities.PowerPlant> plants = new List<Business.Entities.PowerPlant>();
            FilterQuery filter = new FilterQuery(FilterLogic.And);

            if (args != null && args.Any())
            {
                string id = args.ElementAt(0).ToString();

                if (param.Filter == null)
                    param.InstanceFilter();

                param.Filter.AddFilter(Field.SalesOrderId, FilterOperator.Equals, id);


                taskDbObjects = RepoSalesOrderApproval.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
                countTask = RepoSalesOrderApproval.CountAsync(param.Filter);

                await Task.WhenAll(taskDbObjects, countTask);

                dbObjects = taskDbObjects.Result;
                count = countTask.Result;

                models = ListMapper.MapList<SalesOrderApproval, SalesOrderApprovalPresentationStub>(dbObjects);

            }


            return JsonConvert.SerializeObject(new { total = count, data = models });
        }


        /*
		public async Task<string> BindingTotalProduksi(params object[] args)
		{
			
			//lib
			int count;
			List<Business.Entities.BastFile> dbObjects;
			List<BastFilePresentationStub> models;
			Task<List<Business.Entities.BastFile>> dbObjectsTask = null;
			Task<int> countTask = null;
			QueryRequestParameter param = QueryRequestParameter.Current;

			//algorithm
			dbObjectsTask = RepoBastFile.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
			countTask = RepoBastFile.CountAsync(param.Filter);

			//get and count data from db in background thread
			await Task.WhenAll(dbObjectsTask, countTask);

			//get callback from task
			dbObjects = dbObjectsTask.Result;
			count = countTask.Result;

			//map db data to model
			models = ListMapper.MapList<Business.Entities.BastFile, BastFilePresentationStub>(dbObjects);

			return JsonConvert.SerializeObject(new { total = count, data = models });



			List<Business.Entities.TaskReport> reportsFilter;
			List<Business.Entities.Pltp> pltps;
			List<string> errors = new List<string>();
			string error;

			//algo
			reportsFilter = RepoTaskReport.GetFromPowerPlantIdPeriode(model.ShipToPartyId, model.Periode);
			pltps = reportsFilter.SelectMany(x => x.Pltps.Where(y => y.PowerPlantId == model.ShipToPartyId)).ToList();
			
		}
		*/
        #endregion

        #region Download

        [HttpGet]
        public async Task<ActionResult> DownloadBastTemplate(int id) // dari salesOrderId, pasti ZPGE
        {
            byte[] excel;

            Business.Entities.SalesOrder salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(id);

            string periodeStr = salesOrder.Periode.Value.ToString(DateTimeFormat.LocalDateFormat);
            string filename = $"BAST {periodeStr} - {salesOrder.PowerPlant.Name}.xlsx";

            string filePath = Path.Combine(Server.MapPath(DirectoryHelper.Resources), salesOrder.PowerPlant.TemplateBastFileName);
            XSSFWorkbook workbook = new XSSFWorkbook(System.IO.File.Open(filePath, FileMode.Open));

            SalesOrderExcelStub salesOrderExcelStub = new SalesOrderExcelStub(workbook);
            if (salesOrder.PowerPlant.Material == Material.ELECTRIC_POWER.ToString())
            {
                List<Business.Entities.SalesOrder> allSalesOrders = RepoSalesOrder.GetFromPowerPlant(salesOrder.PowerPlantId.Value);
                excel = salesOrderExcelStub.GenerateTemplateElectric(salesOrder, allSalesOrders);
            }
            else // if (salesOrder.PowerPlant.Material == Material.GEOTHERMAL_STEAM.ToString())
            {
                List<TaskReport> allTaskReports = RepoTaskReport.GetFromPowerPlantIdPeriode(salesOrder.PowerPlantId.Value);
                excel = salesOrderExcelStub.GenerateTemplateSteam(salesOrder, allTaskReports);
            }

            return File(excel, "application/x-msexcel", filename);
        }

        [HttpGet]
        public async Task<ActionResult> DownloadSap(int id) // dari salesOrderId
        {
            byte[] excel;

            Business.Entities.SalesOrder salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(id);
            string periodeStr = salesOrder.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat);
            string filename = $"SAP {periodeStr} - {salesOrder.PowerPlant.Name}.xlsx";

            string filePath = Path.Combine(Server.MapPath(DirectoryHelper.Resources), $"SAP_SO_{salesOrder.PowerPlant.SOType}.xlsx");
            XSSFWorkbook workbook = new XSSFWorkbook(System.IO.File.Open(filePath, FileMode.Open));
            DateTime currentPeriode = DateTime.Now;

            SalesOrderExcelStub salesOrderExcelStub = new SalesOrderExcelStub(workbook);
            if (salesOrder.PowerPlant.SOType == SOType.ZPGE.ToString())
            {                
                List<TaskReport> allTaskReports = RepoTaskReport.GetFromPowerPlantIdPeriode(salesOrder.PowerPlantId.Value, salesOrder.Periode);
                excel = salesOrderExcelStub.GenerateSapZPGE(salesOrder, allTaskReports);
            }
            else // if (salesOrder.PowerPlant.SOType == SOType.ZTS.ToString())
            {
                List<Business.Entities.SalesOrder> monthSalesOrders = RepoSalesOrder.GetFromPeriode(salesOrder.Periode);
                excel = salesOrderExcelStub.GenerateSapZTS(monthSalesOrders);
            }

            return File(excel, "application/x-msexcel", filename);
        }

        public virtual async Task<bool> SOEmailSender(Business.Entities.SalesOrder dbObject, int salesOrderApprovalId, string message = null)
        {
            List<Membership> memberships = null;
            Business.Entities.PowerPlant powerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(dbObject.PowerPlantId);

            string approveAction = dbObject.SOType == "ZPGE" ? "ApproveSalesOrder" : "ApproveSalesOrderZTS";
            string reviseAction = dbObject.SOType == "ZPGE" ? "ReviseSalesOrder" : "ReviseSalesOrderZTS";

            if (powerPlant != null)
            {
                if (powerPlant.AreaId != null)
                {
                    FilterQuery filterQuery = new FilterQuery();
                    filterQuery.AddFilter(Field.AreaId, FilterOperator.Equals, powerPlant.AreaId.Value);
                    memberships = await RepoMembership.FindAllAsync(filterQuery);
                }
            }

            UserRole? userRole;

            switch (dbObject.WorkflowId)
            {
                case 3:
                case 11:
                    userRole = UserRole.ASMEN;
                    break;
                case 4:
                    userRole = UserRole.MANAGER_AREA;
                    break;
                case 5:
                    userRole = UserRole.FINANCE_AREA;
                    break;
                case 7:
                case 9:
                    userRole = UserRole.ASMEN_PARTNERSHIP;
                    break;
                case 8:
                    userRole = UserRole.PARTNERSHIP;
                    break;
                default:
                    userRole = null;
                    break;
            }

            if (userRole != null)
            {
                if (memberships.Any())
                {
                    memberships = memberships.FindAll(n => n.User.Roles.Any(m => m.RoleName == userRole.Value.ToString()));
                    if (memberships.Any())
                    {
                        string subject = "Notifikasi Sales Order IPGE";
                        string body = "";

                        foreach (Membership m in memberships)
                        {
                            string approveUrl = Url.Action(approveAction, "SOServices", new { userId = m.UserId, id = dbObject.Id, soApprovalId = salesOrderApprovalId, area = "SalesOrder" }, Request.Url.Scheme);
                            string reviseUrl = Url.Action(reviseAction, "SOServices", new { userId = m.UserId, id = dbObject.Id, soApprovalId = salesOrderApprovalId, area = "SalesOrder" }, Request.Url.Scheme);
                            body = @"<p><strong>Yth. Bpk / Ibu</strong></p>
                            <p>Dengan ini kami sampaikan bahwa Sales Order berikut ini, menunggu persetujuan/revisi dari Bpk/Ibu.<br /><br /></p>
                            <p><strong>Detail Sales Order :</strong></p>
                            <table style=""width: 347px;"">
                            <tbody>
                            <tr>
                            <td style=""width: 129px;"">SO Type</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.SOType != null ? dbObject.SOType : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Periode</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.Periode != null ? dbObject.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat) : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Ship To party</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (powerPlant.Name != null ? powerPlant.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Sold To Party</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (powerPlant.SoldToPartyId != null ? powerPlant.SoldToParty.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;\"">Material</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (powerPlant.Material != null ? ((Material)Enum.Parse(typeof(Material), powerPlant.Material)).ToDescription() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Quantity</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.Quantity != null ? dbObject.Quantity.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Net Price</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.NetPrice != null ? dbObject.NetPrice.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Net Value</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.NetValue != null ? dbObject.NetValue.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Notes</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.Notes != null ? dbObject.Notes : "") + @"</td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <p>Untuk verifikasi silahkan klik link di bawah ini</p>
                            <table><tr><td style =""background-color: #4ecdc4;border-color: #45b7af;border-radius: 5px;text-align: center;padding: 10px;"">
                            <a style='display: block; color: #ffffff;font-size: 12px;text-decoration: none;text-transform: uppercase;a:hover{cursor:pointer;}' href=""" + approveUrl + @""">Setujui</a></td>
                            <td style=""background-color: #bf3131;border-color: #45b7af;border-radius: 5px;text-align: center;padding: 10px;"">
                            <a style='display: block; color: #ffffff;font-size: 12px;text-decoration: none;text-transform: uppercase;a:hover{cursor:pointer;}' href=""" + reviseUrl + @""">Revisi</a></td></tr></table>
                            <p>&nbsp;</p>
                            <p>Atas perhatiannya kami ucapkan terima kasih.</p>";
                            string email = m.Email;
                            bool isSuccess = EmailHelper.SendEmail(email, subject, body);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
