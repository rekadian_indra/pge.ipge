﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Web;
using System.Linq;
using WebUI.Extension;
using Resources;
using WebUI.Areas.SalesOrder.Models;

namespace WebUI.Areas.SalesOrder.ControllersOld
{
    [Authorize]
    public class SalesOrderController : BaseController<TaskReportFormStub>
    {
		#region Constructor

		public SalesOrderController()
        {
        }

        public SalesOrderController
        (
            ILogRepository repoLog,
            IWorkflowRepository repoWorkflow,
            IPowerPlantRepository repoPowerPlant,
            ITaskReportRepository repoTaskReport,
			ISalesOrderRepository repoSalesOrder,
			IBastFileRepository repoBastFile


		)
        {
            RepoLog = repoLog;
            RepoWorkflow = repoWorkflow;
            RepoPowerPlant = repoPowerPlant;
            RepoTaskReport = repoTaskReport;
            RepoSalesOrder = repoSalesOrder;
            RepoBastFile = repoBastFile;
		}

		#endregion

		#region View

		[MvcSiteMapNode(Title = TitleSite.SalesOrder, Key = KeySite.IndexSalesOrder, ParentKey = KeySite.Dashboard)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
		}

		[HttpPost]
		public async Task<ActionResult> LoadCreateZPGE(SalesOrderFormStub model)
		{
			if (ModelState.IsValid)
			{
				return await CreateSOZPGE(model);
			}

			//kamus
			FilterQuery filter;
			//List<Business.Entities.TaskReport> reports;
			List<Business.Entities.TaskReport> reportsFilter;
			List<Business.Entities.Pltp> pltps;
			//List<Business.Entities.Pltp> temp;
			List<Business.Entities.TaskReport> reportNotApprove;
			List<string> errors = new List<string>();
			string error;

			List<SortQuery> sorts = new List<SortQuery>();
			SortQuery sort;
			List<Business.Entities.Workflow> workflows;

			//algo
			reportsFilter = RepoTaskReport.GetFromPowerPlantIdPeriode(model.ShipToPartyId, model.Periode);
			pltps = reportsFilter.SelectMany(x => x.Pltps.Where(y => y.PowerPlantId == model.ShipToPartyId)).ToList();

			filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZPGE);
			sort = new SortQuery(Field.Id, SortOrder.Ascending);
			sorts.Add(sort);
			workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

			model.SetWorkflow(workflows);
			model.SOType = SOType.ZPGE.ToString();

			if (!reportsFilter.Any())
			{
				error = $"Belum terdapat data operation untuk periode {model.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
				errors.Add(error);
				model.SetValidationMessage(errors);
			}
			else
			{
				reportNotApprove = reportsFilter.Where(x => x.Status == DataStatus.WAITING.ToString()).ToList();
				if (reportNotApprove.Any())
				{
					foreach (TaskReport t in reportNotApprove)
					{
						error = $"Data pada periode {t.Periode.Value.ToString(DisplayFormat.ShortDateFormat)} belum disetujui";
						errors.Add(error);
					}
					model.SetValidationMessage(errors);
				}
				else
				{
					if (model.ProcessPltps(pltps))
					{
						//model.SetPowerPlant(pltps.First().PowerPlant);
						model.IsValid = true;
						//model.Quantity = pltps.Sum(x => x.EnergyNett).ToFloat();
						model.NetPrice = 62.4;
						model.WorkflowId = model.Workflows.FirstOrDefault(x => x.PreviousStep == null).Id;
						model.IsRevisable = true;
						model.NetValue = model.Quantity * model.NetPrice;
						model.Currency = "USDN";
					}
					else
					{
						error = $"Belum terdapat data operation untuk periode {model.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
						errors.Add(error);
						model.SetValidationMessage(errors);
					}
				}
			}

			return View("FormZPGE", model);
		}

		[HttpPost]
		public async Task<ActionResult> LoadCreateZTS(FormZtsStub model)
		{
			if (ModelState.IsValid)
			{
				return await CreateSOZTS(model);
			}

			//kamus
			IEnumerable<SalesOrderFormStub> salesOrders;
			FilterQuery filter;
			List<Business.Entities.TaskReport> reportsFilter;
			List<Business.Entities.TaskReport> reportNotApprove;
			List<string> errors = new List<string>();
			string error;

			List<SortQuery> sorts = new List<SortQuery>();
			SortQuery sort;
			List<Business.Entities.Workflow> workflows;

			//algo
			reportsFilter = RepoTaskReport.GetFromPeriode(model.Periode);

			filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZTS);
			sort = new SortQuery(Field.Id, SortOrder.Ascending);
			sorts.Add(sort);
			workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

			model.SetWorkflow(workflows);
			model.SOType = SOType.ZTS.ToString();

			if (!reportsFilter.Any())
			{
				error = $"Belum terdapat data operation untuk periode {model.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
				errors.Add(error);
				model.SetValidationMessage(errors);
			}
			else
			{
				reportNotApprove = reportsFilter.Where(x => x.Status == DataStatus.WAITING.ToString()).ToList();
				if (reportNotApprove.Any())
				{
					foreach (TaskReport t in reportNotApprove)
					{
						error = $"Data pada periode {t.Periode.Value.ToString(DisplayFormat.ShortDateFormat)} belum disetujui";
						errors.Add(error);
					}
					model.SetValidationMessage(errors);
				}
				else
				{
					model.IsRevisable = true;
					model.SalesOrders = new List<SalesOrderFormStub>();
					foreach (TaskReport reportFilter in reportsFilter)
					{
						foreach (Business.Entities.Pltp pltp in reportFilter.Pltps)
						{
							if (pltp.PowerPlantId.HasValue)
							{
								int curPowerPlantId = pltp.PowerPlantId.Value;
								salesOrders = model.SalesOrders.Where(x => x.ShipToPartyId == curPowerPlantId);

								SalesOrderFormStub curSalesOrder;

								// kalau blm ada salesorder yg pake power plant tsb
								if (!salesOrders.Any())
								{
									curSalesOrder = new SalesOrderFormStub();

									curSalesOrder.SetPowerPlant(pltp.PowerPlant);
									curSalesOrder.IsValid = true;
									curSalesOrder.Quantity = pltp.EnergyNett.ToFloat();
									curSalesOrder.NetPrice = 62.4;
									curSalesOrder.WorkflowId = model.Workflows.FirstOrDefault(x => x.PreviousStep == null).Id;
									curSalesOrder.IsRevisable = true;
									curSalesOrder.Currency = "USDN";
									curSalesOrder.NetValue = curSalesOrder.Quantity * curSalesOrder.NetPrice;

									model.SalesOrders.Add(curSalesOrder);
								}
								else
								{
									curSalesOrder = salesOrders.FirstOrDefault();
									curSalesOrder.Quantity += pltp.EnergyNett.ToFloat();
									curSalesOrder.NetValue = curSalesOrder.Quantity * curSalesOrder.NetPrice;
								}
							}
						}
					}
				}
			}

			return View("FormZTS", model);
		}
		
		#endregion

		#region Create

		[MvcSiteMapNode(Title = TitleSite.CreateSalesOrderZPGE, Key = KeySite.CreateSalesOrder, ParentKey = KeySite.IndexSalesOrder)]
        public async Task<ActionResult> CreateSOZPGE()
        {
            //kamus
            FilterQuery filter;
            List<SortQuery> sorts = new List<SortQuery>();
            SortQuery sort;
            List<Business.Entities.Workflow> workflows;
            SalesOrderFormStub model;
            
            //algo
            filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZPGE);
            sort = new SortQuery(Field.Id, SortOrder.Ascending);
            sorts.Add(sort);
            workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

            model = new SalesOrderFormStub(workflows);
			model.SOType = SOType.ZPGE.ToString();

			return View("FormZPGE", model);
		}

		[MvcSiteMapNode(Title = TitleSite.CreateSalesOrderZTS, Key = KeySite.CreateSalesOrderZTS, ParentKey = KeySite.IndexSalesOrder)]
		public async Task<ActionResult> CreateSOZTS()
		{
			//kamus
			FilterQuery filter;
			List<SortQuery> sorts = new List<SortQuery>();
			SortQuery sort;
			List<Business.Entities.Workflow> workflows;
			FormZtsStub model;

			//algo
			filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZTS);
			sort = new SortQuery(Field.Id, SortOrder.Ascending);
			sorts.Add(sort);
			workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

			model = new FormZtsStub(workflows);
			model.SOType = SOType.ZTS.ToString();

			return View("FormZTS", model);
		}

		[HttpPost]
		public async Task<ActionResult> CreateSOZPGE(SalesOrderFormStub model)
		{
			//kamus
			FilterQuery filter;
			List<Business.Entities.Workflow> workflows;
			Business.Entities.SalesOrder salesOrder = new Business.Entities.SalesOrder();
			ResponseModel response = new ResponseModel(true);

			//algoritma
			filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, model.SOType);
			filter.AddFilter(Field.PreviousStep, FilterOperator.Equals, model.WorkflowId);
			workflows = await RepoWorkflow.FindAllAsync(filter);

			if (ModelState.IsValid)
			{
				if (workflows != null)
				{
					model.MapDbObject(salesOrder);
					salesOrder.WorkflowId = workflows.ElementAt(0).Id;

					await RepoSalesOrder.SaveAsync(salesOrder);

					//List<BastFile> bastFiles = new List<BastFile>();
					foreach (BastFileFormStub bastFileStub in model.BastFiles)
					{
						BastFile bastFileDb = new BastFile();
						bastFileStub.SalesOrderId = salesOrder.Id;
						bastFileStub.MapDbObject(bastFileDb);
						//bastFiles.Add(bastFileDb);
						await RepoBastFile.SaveAsync(bastFileDb);
					}

					return RedirectToAction("Index");
				}
			}
			return View("FormZPGE", model);
			//return await CreateSOZPGE(model);
		}

		[HttpPost]
		public async Task<ActionResult> CreateSOZTS(FormZtsStub model)
		{
			//kamus
			FilterQuery filter;
			List<Business.Entities.Workflow> workflows;
			ResponseModel response = new ResponseModel(true);

			//algoritma
			filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, model.SOType);
			workflows = await RepoWorkflow.FindAllAsync(filter);

			if (ModelState.IsValid)
			{
				if (workflows != null)
				{
					foreach (SalesOrderFormStub salesOrder in model.SalesOrders)
					{
						IEnumerable<Business.Entities.Workflow> curWorkflows = workflows.Where(x => x.PreviousStep == salesOrder.WorkflowId);
						if (curWorkflows.Any())
						{
							Business.Entities.SalesOrder salesOrderDb = new Business.Entities.SalesOrder();
							salesOrder.WorkflowId = curWorkflows.ElementAt(0).Id;
							salesOrder.MapDbObject(salesOrderDb);

							await RepoSalesOrder.SaveAsync(salesOrderDb);

							//List<BastFile> bastFiles = new List<BastFile>();
							foreach (BastFileFormStub bastFileStub in salesOrder.BastFiles)
							{
								BastFile bastFileDb = new BastFile();
								bastFileStub.SalesOrderId = salesOrderDb.Id;
								bastFileStub.MapDbObject(bastFileDb);
								//bastFiles.Add(bastFileDb);
								await RepoBastFile.SaveAsync(bastFileDb);
							}
						}
					}

					return RedirectToAction("Index");
				}
			}
			return View("FormZTS", model);
			//return await CreateSOZPGE(model);
		}

		/*
		[HttpPost]
		public async Task<ActionResult> CreateSOZTS(SalesOrderFormStub model)
		{
			//kamus
			FilterQuery filter;
			List<Business.Entities.Workflow> workflows;
			Business.Entities.SalesOrder salesOrder = new Business.Entities.SalesOrder();
			ResponseModel response = new ResponseModel(true);

			//algoritma
			filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, model.SOType);
			filter.AddFilter(Field.PreviousStep, FilterOperator.Equals, model.WorkflowId);
			workflows = await RepoWorkflow.FindAllAsync(filter);

			if (ModelState.IsValid)
			{
				if (workflows != null)
				{
					model.MapDbObject(salesOrder);
					salesOrder.WorkflowId = workflows.ElementAt(0).Id;

					await RepoSalesOrder.SaveAsync(salesOrder);

					//List<BastFile> bastFiles = new List<BastFile>();
					foreach (BastFileFormStub bastFileStub in model.BastFiles)
					{
						BastFile bastFileDb = new BastFile();
						bastFileStub.SalesOrderId = salesOrder.Id;
						bastFileStub.MapDbObject(bastFileDb);
						//bastFiles.Add(bastFileDb);
						await RepoBastFile.SaveAsync(bastFileDb);
					}

					return RedirectToAction("Index");
				}
			}
			return View("FormZPGE", model);
			//return await CreateSOZPGE(model);
		}
		*/

		#endregion

		#region Edit

		[MvcSiteMapNode(Title = TitleSite.EditSalesOrderZPGE, Key = KeySite.EditSalesOrderZPGE, ParentKey = KeySite.IndexSalesOrder)]
		public async Task<ActionResult> EditSOZPGE(params object[] id)
		{
			//kamus
			FilterQuery filter;
			SortQuery sort;
			List<SortQuery> sorts = new List<SortQuery>();
			List<Business.Entities.Workflow> workflows;
			List<Business.Entities.TaskReport> reportsFilter;
			List<Business.Entities.Pltp> pltps;

			//algo
			int primaryKey = int.Parse(id.FirstOrDefault().ToString());
			Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

			filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZPGE);
			sort = new SortQuery(Field.Id, SortOrder.Ascending);
			sorts.Add(sort);
			workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

			SalesOrderFormStub model = new SalesOrderFormStub(dbObject, workflows);

			//ambil powerplant dan hitung quantity
			reportsFilter = RepoTaskReport.GetFromPowerPlantIdPeriode(model.ShipToPartyId, model.Periode);
			pltps = reportsFilter.SelectMany(x => x.Pltps.Where(y => y.PowerPlantId == model.ShipToPartyId)).ToList();
			if (model.ProcessPltps(pltps))
			{
				model.IsValid = true;
				return View("FormZPGE", model);
			}

			return RedirectToAction("Index");
		}

		[HttpPost]
		public async Task<ActionResult> EditSOZPGE(SalesOrderFormStub model)
		{
			//kamus
			FilterQuery filter;
			List<Business.Entities.Workflow> workflows;
			Business.Entities.SalesOrder salesOrder = new Business.Entities.SalesOrder();
			ResponseModel response = new ResponseModel(true);

			//algoritma
			filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, model.SOType);
			filter.AddFilter(Field.PreviousStep, FilterOperator.Equals, model.WorkflowId);
			workflows = await RepoWorkflow.FindAllAsync(filter);

			if (model.BastFiles == null)
			{
				filter = new FilterQuery(Field.SalesOrderId, FilterOperator.Equals, model.Id);
				List<BastFile> bastFiles = await RepoBastFile.FindAllAsync(filter);

				if (bastFiles.Count > 0)
				{
					// soalnya file2 yg lama nya ga perlu di add ulang
					model.BastFiles = new List<BastFileFormStub>();
				}
			}

			if ((ModelState.IsValid) || (model.BastFiles != null))
			{
				if (workflows != null)
				{
					salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(model.Id);
					model.MapDbObject(salesOrder);
					salesOrder.WorkflowId = workflows.ElementAt(0).Id;

					await RepoSalesOrder.SaveAsync(salesOrder);

					//List<BastFile> bastFiles = new List<BastFile>();
					foreach (BastFileFormStub bastFileStub in model.BastFiles)
					{
						BastFile bastFileDb = new BastFile();
						bastFileStub.SalesOrderId = salesOrder.Id;
						bastFileStub.MapDbObject(bastFileDb);
						//bastFiles.Add(bastFileDb);
						await RepoBastFile.SaveAsync(bastFileDb);
					}

					return RedirectToAction("Index");
				}
			}
			//return View("FormZPGE", model);
			return await EditSOZPGE(model.Id);
		}

		[MvcSiteMapNode(Title = TitleSite.EditSalesOrderZTS, Key = KeySite.EditSalesOrderZTS, ParentKey = KeySite.IndexSalesOrder)]
		public async Task<ActionResult> EditSOZTS(params object[] id)
		{
			//kamus
			FilterQuery filter;
			SortQuery sort;
			List<SortQuery> sorts = new List<SortQuery>();
			List<Business.Entities.Workflow> workflows;
			List<Business.Entities.TaskReport> reportsFilter;
			List<Business.Entities.Pltp> pltps;

			//algo
			int primaryKey = int.Parse(id.FirstOrDefault().ToString());
			Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

			filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, SOType.ZTS);
			sort = new SortQuery(Field.Id, SortOrder.Ascending);
			sorts.Add(sort);
			workflows = await RepoWorkflow.FindAllAsync(sorts, filter);

			SalesOrderFormStub model = new SalesOrderFormStub(dbObject, workflows);

			//ambil powerplant dan hitung quantity
			reportsFilter = RepoTaskReport.GetFromPowerPlantIdPeriode(model.ShipToPartyId, model.Periode);
			pltps = reportsFilter.SelectMany(x => x.Pltps.Where(y => y.PowerPlantId == model.ShipToPartyId)).ToList();
			if (model.ProcessPltps(pltps))
			{
				model.IsValid = true;
				return View("FormZPGE", model);
			}

			return RedirectToAction("Index");
		}

		[HttpPost]
		public async Task<ActionResult> EditSOZTS(SalesOrderFormStub model)
		{
			//kamus
			FilterQuery filter;
			List<Business.Entities.Workflow> workflows;
			Business.Entities.SalesOrder salesOrder = new Business.Entities.SalesOrder();
			ResponseModel response = new ResponseModel(true);

			//algoritma
			filter = new FilterQuery(Field.WorkflowType, FilterOperator.Equals, model.SOType);
			filter.AddFilter(Field.PreviousStep, FilterOperator.Equals, model.WorkflowId);
			workflows = await RepoWorkflow.FindAllAsync(filter);

			if (model.BastFiles == null)
			{
				filter = new FilterQuery(Field.SalesOrderId, FilterOperator.Equals, model.Id);
				List<BastFile> bastFiles = await RepoBastFile.FindAllAsync(filter);

				if (bastFiles.Count > 0)
				{
					// soalnya file2 yg lama nya ga perlu di add ulang
					model.BastFiles = new List<BastFileFormStub>();
				}
			}

			if ((ModelState.IsValid) || (model.BastFiles != null))
			{
				if (workflows != null)
				{
					salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(model.Id);
					model.MapDbObject(salesOrder);
					salesOrder.WorkflowId = workflows.ElementAt(0).Id;

					await RepoSalesOrder.SaveAsync(salesOrder);

					//List<BastFile> bastFiles = new List<BastFile>();
					foreach (BastFileFormStub bastFileStub in model.BastFiles)
					{
						BastFile bastFileDb = new BastFile();
						bastFileStub.SalesOrderId = salesOrder.Id;
						bastFileStub.MapDbObject(bastFileDb);
						//bastFiles.Add(bastFileDb);
						await RepoBastFile.SaveAsync(bastFileDb);
					}

					return RedirectToAction("Index");
				}
			}
			//return View("FormZTS", model);
			return await EditSOZTS(model.Id);
		}

		#endregion

		#region Delete

		[HttpPost]
		public override async Task<ActionResult> Delete(params object[] id)
		{
			// kamus
			FilterQuery filter;
			string message;

			// algo
			ResponseModel response = new ResponseModel(true);
			int primaryKey = int.Parse(id.FirstOrDefault().ToString());
			Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

			// hapus data BAST File nya dulu
			filter = new FilterQuery(Field.SalesOrderId, FilterOperator.Equals, dbObject.Id);
			List<BastFile> bastFiles = await RepoBastFile.FindAllAsync(filter);
			foreach (BastFile bastFile in bastFiles)
			{
				string filePath = Server.MapPath(bastFile.FilePath);
				if (System.IO.File.Exists(filePath))
				{
					System.IO.File.Delete(filePath);
				}
			}
			await RepoBastFile.DeleteAllAsync(bastFiles);

			// baru hapus sales order nya
			if (!(await RepoSalesOrder.DeleteAsync(dbObject)))
			{
				message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
				response.SetFail(message);
			}

			return Json(response);
		}

		[HttpPost]
		public async Task<ActionResult> DeleteBastFile(params object[] id)
		{
			// kamus
			string message;

			// algo
			ResponseModel response = new ResponseModel(true);
			int primaryKey = int.Parse(id.FirstOrDefault().ToString());
			Business.Entities.BastFile dbObject = await RepoBastFile.FindByPrimaryKeyAsync(primaryKey);
			
			// baru hapus sales order nya
			if (!(await RepoBastFile.DeleteAsync(dbObject)))
			{
				message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
				response.SetFail(message);
			}
			else
			{
				// hapus file nya
				string filePath = Server.MapPath(dbObject.FilePath);
				if (System.IO.File.Exists(filePath))
				{
					System.IO.File.Delete(filePath);
				}
				//FileManagementController fmc = new FileManagementController();
				//string[] fileNames = new string[1];
				//fileNames[0] = filePath;
				//fmc.Remove(fileNames);
			}

			return Json(response);
		}

#endregion

		#region Approval

		[HttpPost]
		public async Task<ActionResult> ApproveSalesOrder(params object[] id)
		{
			// kamus
			FilterQuery filter;
			string message;
			Business.Entities.Workflow workflow;

			// algo
			ResponseModel response = new ResponseModel(true);
			int primaryKey = int.Parse(id.FirstOrDefault().ToString());
			Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

			// cari workflow lanjutannya
			filter = new FilterQuery(Field.PreviousStep, FilterOperator.Equals, dbObject.WorkflowId);
			workflow = await RepoWorkflow.FindAsync(filter);
			dbObject.WorkflowId = workflow.Id;

			// baru update data sales order nya
			if (!(await RepoSalesOrder.SaveAsync(dbObject)))
			{
				message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
				response.SetFail(message);
			}

			return Json(response);
		}

		[HttpPost]
		public async Task<ActionResult> ReviseSalesOrder(params object[] id)
		{
			// kamus
			FilterQuery filter;
			string message;
			Business.Entities.Workflow workflow;
			bool isSuccess = false;

			// algo
			ResponseModel response = new ResponseModel(true);
			int primaryKey = int.Parse(id.FirstOrDefault().ToString());
			Business.Entities.SalesOrder dbObject = await RepoSalesOrder.FindByPrimaryKeyAsync(primaryKey);

			// cari workflow sebelumnya
			if (dbObject.Workflow != null)
			{
				if (dbObject.Workflow.PreviousStep != null)
				{
					filter = new FilterQuery(Field.Id, FilterOperator.Equals, dbObject.Workflow.PreviousStep);
					workflow = await RepoWorkflow.FindAsync(filter);
					dbObject.WorkflowId = workflow.Id;

					// update data sales order nya
					isSuccess = await RepoSalesOrder.SaveAsync(dbObject);
				}
			}

			if (!isSuccess)
			{
				message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedFailed).ToString();
				response.SetFail(message);
			}

			return Json(response);
		}

		#endregion
		
		#region Binding
		public override async Task<string> Binding(params object[] args)
		{
			//lib
			int count;
			List<Business.Entities.SalesOrder> dbObjects;
			List<SalesOrderPresentationStub> models;
			Task<List<Business.Entities.SalesOrder>> dbObjectsTask = null;
			Task<int> countTask = null;
			QueryRequestParameter param = QueryRequestParameter.Current;

			//algorithm
			dbObjectsTask = RepoSalesOrder.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
			countTask = RepoSalesOrder.CountAsync(param.Filter);

			//get and count data from db in background thread
			await Task.WhenAll(dbObjectsTask, countTask);

			//get callback from task
			dbObjects = dbObjectsTask.Result;
			count = countTask.Result;

			//map db data to model
			models = ListMapper.MapList<Business.Entities.SalesOrder, SalesOrderPresentationStub>(dbObjects);

			return JsonConvert.SerializeObject(new { total = count, data = models });
		}

		public async Task<string> BindingApproval(params object[] args)
		{
			//lib
			int count;
			List<Business.Entities.SalesOrder> dbObjects;
			List<SalesOrderPresentationStub> models;
			Task<List<Business.Entities.SalesOrder>> dbObjectsTask = null;
			Task<int> countTask = null;
			QueryRequestParameter param = QueryRequestParameter.Current;

			//algorithm
			dbObjectsTask = RepoSalesOrder.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
			countTask = RepoSalesOrder.CountAsync(param.Filter);

			//get and count data from db in background thread
			await Task.WhenAll(dbObjectsTask, countTask);

			//get callback from task
			dbObjects = dbObjectsTask.Result;
			count = countTask.Result;

			//map db data to model
			models = ListMapper.MapList<Business.Entities.SalesOrder, SalesOrderPresentationStub>(dbObjects);

			return JsonConvert.SerializeObject(new { total = count, data = models });
		}

		public async Task<string> BindingBastFiles(params object[] args)
		{
			//lib
			int count;
			List<Business.Entities.BastFile> dbObjects;
			List<BastFilePresentationStub> models;
			Task<List<Business.Entities.BastFile>> dbObjectsTask = null;
			Task<int> countTask = null;
			QueryRequestParameter param = QueryRequestParameter.Current;

			//algorithm
			dbObjectsTask = RepoBastFile.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
			countTask = RepoBastFile.CountAsync(param.Filter);

			//get and count data from db in background thread
			await Task.WhenAll(dbObjectsTask, countTask);

			//get callback from task
			dbObjects = dbObjectsTask.Result;
			count = countTask.Result;

			//map db data to model
			models = ListMapper.MapList<Business.Entities.BastFile, BastFilePresentationStub>(dbObjects);

			return JsonConvert.SerializeObject(new { total = count, data = models });
		}

		/*
		public async Task<string> BindingTotalProduksi(params object[] args)
		{
			
			//lib
			int count;
			List<Business.Entities.BastFile> dbObjects;
			List<BastFilePresentationStub> models;
			Task<List<Business.Entities.BastFile>> dbObjectsTask = null;
			Task<int> countTask = null;
			QueryRequestParameter param = QueryRequestParameter.Current;

			//algorithm
			dbObjectsTask = RepoBastFile.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
			countTask = RepoBastFile.CountAsync(param.Filter);

			//get and count data from db in background thread
			await Task.WhenAll(dbObjectsTask, countTask);

			//get callback from task
			dbObjects = dbObjectsTask.Result;
			count = countTask.Result;

			//map db data to model
			models = ListMapper.MapList<Business.Entities.BastFile, BastFilePresentationStub>(dbObjects);

			return JsonConvert.SerializeObject(new { total = count, data = models });



			List<Business.Entities.TaskReport> reportsFilter;
			List<Business.Entities.Pltp> pltps;
			List<string> errors = new List<string>();
			string error;

			//algo
			reportsFilter = RepoTaskReport.GetFromPowerPlantIdPeriode(model.ShipToPartyId, model.Periode);
			pltps = reportsFilter.SelectMany(x => x.Pltps.Where(y => y.PowerPlantId == model.ShipToPartyId)).ToList();
			
		}
		*/
		#endregion
	}
}
