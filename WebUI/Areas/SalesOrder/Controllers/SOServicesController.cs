﻿using Common.Enums;
using LogAction.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Linq;

namespace WebUI.Areas.SalesOrder.Controllers
{
    public class SOServicesController : BaseController
    {
        #region Constructor

        public SOServicesController()
        {
        }

        public SOServicesController
        (
            ILogRepository repoLog,
            IWorkflowRepository repoWorkflow,
            IPowerPlantRepository repoPowerPlant,
            ISalesOrderRepository repoSalesOrder,
            ISalesOrderApprovalRepository repoSalesOrderApproval,
            IMembershipRepository repoMembership
        )
        {
            RepoLog = repoLog;
            RepoWorkflow = repoWorkflow;
            RepoPowerPlant = repoPowerPlant;
            RepoSalesOrder = repoSalesOrder;
            RepoSalesOrderApproval = repoSalesOrderApproval;
            RepoMembership = repoMembership;
        }

        #endregion

        #region Approval

        public async Task<ActionResult> ApproveSalesOrder(Guid userId, int id, int soApprovalId)
        {
            ResponseModel response = new ResponseModel(true);
            response.Message = "Sales Order Approved";

            Membership membership = await RepoMembership.FindAsync(new FilterQuery(Field.UserId, FilterOperator.Equals, userId));
            Business.Entities.SalesOrder salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(id);
            Business.Entities.Workflow workflow;

            // cek membership dan sales order ada
            if (membership != null && salesOrder != null)
            {
                if (salesOrder.PowerPlantId != null)
                {
                    if (salesOrder.PowerPlant.AreaId != null && membership.AreaId != null)
                    {
                        if (salesOrder.PowerPlant.AreaId.Value == membership.AreaId)
                        {
                            int lastStep = salesOrder.SalesOrderApprovals.Select(a => a.Id).Max();
                            if (lastStep == soApprovalId)
                            {
                                // approve / revise

                                if(salesOrder.WorkflowId != null)
                                {
                                    workflow = await RepoWorkflow.FindAsync(new FilterQuery(Field.PreviousStep, FilterOperator.Equals, salesOrder.WorkflowId));
                                    if(workflow != null)
                                    {
                                        salesOrder.WorkflowId = workflow.Id;
                                        salesOrder.ModifiedBy = membership.User.UserName;
                                        salesOrder.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();
                                        if (await RepoSalesOrder.SaveAsync(salesOrder))
                                        {
                                            SalesOrderApproval salesOrderApprovalDb = new SalesOrderApproval
                                            {
                                                SalesOrderId = id,
                                                WorkflowId = workflow.Id,
                                                Notes = "",
                                                CreatedBy = membership.User.UserName,
                                                CreatedDateTimeUtc = DateTime.Now.ToUtcDateTime()
                                            };
                                            if (await RepoSalesOrderApproval.SaveAsync(salesOrderApprovalDb))
                                            {
                                                await SOEmailSender(salesOrder, salesOrderApprovalDb.Id);
                                            }
                                            else
                                            {
                                                // jika error saat menyimpan approval sales order
                                                response.SetFail("Sales Order Workflow is not saved");
                                            }
                                        }
                                        else
                                        {
                                            // jika error saat menyimpan sales order
                                            response.SetFail("This Sales Order Approval is not success");
                                        }
                                    }
                                    else
                                    {
                                        // jika sales order tidak bisa maju workflow nya
                                        response.SetFail("This Sales Order can't go to Next Workflow");
                                    }
                                }
                                else
                                {
                                    // jika sales order tidak punya workflow
                                    response.SetFail("This Sales Order doesn't have workflow");
                                }
                            }
                            else
                            {
                                // jika approval tidak sama
                                response.SetFail("This Sales Order has been approved / revised by another user.");
                            }
                        }
                        else
                        {
                            // jika area so powerplant tidak sama area user
                            response.SetFail("User Area not equals to Sales Order Power Plant Area.");
                        }
                    }
                    else if (salesOrder.PowerPlant.AreaId == null)
                    {
                        // jika power plant tidak punya area
                        response.SetFail("This Sales Order doesn't have area.");
                    }
                    else if (membership.AreaId == null)
                    {
                        // jika user tidak punya area
                        response.SetFail("This User doesn't have area.");
                    }
                }
                else
                {
                    // jika power plant tidak ada
                    response.SetFail("This Sales Order doesn't have Power Plant.");
                }
            }
            else if (membership == null)
            {
                // jika membership tidak ada
                response.SetFail("User is not available.");
            }
            else if (salesOrder == null)
            {
                // jika sales order tidak ada
                response.SetFail("This Sales Order is not available.");
            }

            return View("Index", response);
        }

        public async Task<ActionResult> ApproveSalesOrderZTS(Guid userId, int id, int soApprovalId)
        {
            ResponseModel response = new ResponseModel(true);
            response.Message = "Sales Order Approved";

            Membership membership = await RepoMembership.FindAsync(new FilterQuery(Field.UserId, FilterOperator.Equals, userId));
            Business.Entities.SalesOrder salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(id);
            Business.Entities.Workflow workflow;

            // cek membership dan sales order ada
            if (membership != null && salesOrder != null)
            {
                if (salesOrder.PowerPlantId != null)
                {
                    if (salesOrder.PowerPlant.AreaId != null && membership.AreaId != null)
                    {
                        if (salesOrder.PowerPlant.AreaId.Value == membership.AreaId)
                        {
                            int lastStep = salesOrder.SalesOrderApprovals.Select(a => a.Id).Max();
                            if (lastStep == soApprovalId)
                            {
                                // approve / revise

                                List<Business.Entities.SalesOrder> approvedDbObjects = RepoSalesOrder.GetFromPeriodeSOType(salesOrder.Periode, SOType.ZTS.ToString());
                                
                                workflow = await RepoWorkflow.FindAsync(new FilterQuery(Field.PreviousStep, FilterOperator.Equals, salesOrder.WorkflowId));

                                if(workflow != null)
                                {
                                    foreach (Business.Entities.SalesOrder approvedDbObject in approvedDbObjects)
                                    {
                                        approvedDbObject.WorkflowId = workflow.Id;
                                        approvedDbObject.ModifiedBy = membership.User.UserName;
                                        approvedDbObject.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();

                                        SalesOrderApproval salesOrderApprovalDbObject = new SalesOrderApproval
                                        {
                                            SalesOrderId = id,
                                            WorkflowId = workflow.Id,
                                            Notes = "",
                                            CreatedBy = membership.User.UserName,
                                            CreatedDateTimeUtc = DateTime.Now.ToUtcDateTime()
                                        };

                                        if(await RepoSalesOrder.SaveAsync(approvedDbObject))
                                        {
                                            if(await RepoSalesOrderApproval.SaveAsync(salesOrderApprovalDbObject))
                                            {
                                                await SOEmailSender(approvedDbObject, salesOrderApprovalDbObject.Id);
                                            }
                                            else
                                            {
                                                // jika sales order tidak bisa maju workflow nya
                                                response.SetFail("This Sales Order can't go to Next Workflow");
                                            }
                                        }
                                        else
                                        {
                                            // jika error saat menyimpan sales order
                                            response.SetFail("This Sales Order Approval is not success");
                                        }
                                    }
                                }
                                else
                                {
                                    // jika sales order tidak punya workflow
                                    response.SetFail("This Sales Order doesn't have workflow");
                                }
                            }
                            else
                            {
                                // jika approval tidak sama
                                response.SetFail("This Sales Order has been approved / revised by another user.");
                            }
                        }
                        else
                        {
                            // jika area so powerplant tidak sama area user
                            response.SetFail("User Area not equals to Sales Order Power Plant Area.");
                        }
                    }
                    else if (salesOrder.PowerPlant.AreaId == null)
                    {
                        // jika power plant tidak punya area
                        response.SetFail("This Sales Order doesn't have area.");
                    }
                    else if (membership.AreaId == null)
                    {
                        // jika user tidak punya area
                        response.SetFail("This User doesn't have area.");
                    }
                }
                else
                {
                    // jika power plant tidak ada
                    response.SetFail("This Sales Order doesn't have Power Plant.");
                }
            }
            else if (membership == null)
            {
                // jika membership tidak ada
                response.SetFail("User is not available.");
            }
            else if (salesOrder == null)
            {
                // jika sales order tidak ada
                response.SetFail("This Sales Order is not available.");
            }

            return View("Index", response);
        }

        public async Task<ActionResult> ReviseSalesOrder(Guid userId, int id, int soApprovalId)
        {
            ResponseModel response = new ResponseModel(true);
            response.Message = "Sales Order Revised";

            Membership membership = await RepoMembership.FindAsync(new FilterQuery(Field.UserId, FilterOperator.Equals, userId));
            Business.Entities.SalesOrder salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(id);
            Business.Entities.Workflow workflow;

            // cek membership dan sales order ada
            if (membership != null && salesOrder != null)
            {
                if (salesOrder.PowerPlantId != null)
                {
                    if (salesOrder.PowerPlant.AreaId != null && membership.AreaId != null)
                    {
                        if (salesOrder.PowerPlant.AreaId.Value == membership.AreaId)
                        {
                            int lastStep = salesOrder.SalesOrderApprovals.Select(a => a.Id).Max();
                            if (lastStep == soApprovalId)
                            {
                                // approve / revise

                                // cari workflow sebelumnya
                                if (salesOrder.WorkflowId != null)
                                {
                                    if (salesOrder.Workflow.PreviousStep != null)
                                    {
                                        workflow = await RepoWorkflow.FindAsync(new FilterQuery(Field.Id, FilterOperator.Equals, salesOrder.Workflow.PreviousStep));
                                        if(workflow != null)
                                        {
                                            salesOrder.WorkflowId = workflow.Id;
                                            salesOrder.ModifiedBy = membership.User.UserName;
                                            salesOrder.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();

                                            // update data sales order nya
                                            if (await RepoSalesOrder.SaveAsync(salesOrder))
                                            {
                                                // masukin ke history approval
                                                SalesOrderApproval salesOrderApprovalDb = new SalesOrderApproval
                                                {
                                                    SalesOrderId = id,
                                                    WorkflowId = workflow.Id,
                                                    Notes = "",
                                                    CreatedBy = membership.User.UserName,
                                                    CreatedDateTimeUtc = DateTime.Now.ToUtcDateTime()
                                                };
                                                if (await RepoSalesOrderApproval.SaveAsync(salesOrderApprovalDb))
                                                {
                                                    await SOEmailSender(salesOrder, salesOrderApprovalDb.Id);
                                                }
                                                else
                                                {
                                                    // jika error saat menyimpan approval sales order
                                                    response.SetFail("Sales Order Workflow is not saved");
                                                }
                                            }
                                            else
                                            {
                                                // jika error saat menyimpan sales order
                                                response.SetFail("This Sales Order Revise is not success");
                                            }
                                        }
                                        else
                                        {
                                            // jika sales order tidak punya workflow
                                            response.SetFail("This Sales Order doesn't have workflow");
                                        }
                                    }
                                    else
                                    {
                                        // jika sales order tidak bisa kembali workflow nya
                                        response.SetFail("This Sales Order can't go back to Previos Workflow");
                                    }
                                }
                                else
                                {
                                    // jika sales order tidak punya workflow
                                    response.SetFail("This Sales Order doesn't have workflow");
                                }
                            }
                            else
                            {
                                // jika approval tidak sama
                                response.SetFail("This Sales Order has been approved / revised by another user.");
                            }
                        }
                        else
                        {
                            // jika area so powerplant tidak sama area user
                            response.SetFail("User Area not equals to Sales Order Power Plant Area.");
                        }
                    }
                    else if (salesOrder.PowerPlant.AreaId == null)
                    {
                        // jika power plant tidak punya area
                        response.SetFail("This Sales Order doesn't have area.");
                    }
                    else if (membership.AreaId == null)
                    {
                        // jika user tidak punya area
                        response.SetFail("This User doesn't have area.");
                    }
                }
                else
                {
                    // jika power plant tidak ada
                    response.SetFail("This Sales Order doesn't have Power Plant.");
                }
            }
            else if (membership == null)
            {
                // jika membership tidak ada
                response.SetFail("User is not available.");
            }
            else if (salesOrder == null)
            {
                // jika sales order tidak ada
                response.SetFail("This Sales Order is not available.");
            }

            return View("Index",response);
        }

        public async Task<ActionResult> ReviseSalesOrderZTS(Guid userId, int id, int soApprovalId)
        {
            ResponseModel response = new ResponseModel(true);
            response.Message = "Sales Order Revised";

            Membership membership = await RepoMembership.FindAsync(new FilterQuery(Field.UserId, FilterOperator.Equals, userId));
            Business.Entities.SalesOrder salesOrder = await RepoSalesOrder.FindByPrimaryKeyAsync(id);
            Business.Entities.Workflow workflow;

            // cek membership dan sales order ada
            if (membership != null && salesOrder != null)
            {
                if (salesOrder.PowerPlantId != null)
                {
                    if (salesOrder.PowerPlant.AreaId != null && membership.AreaId != null)
                    {
                        if (salesOrder.PowerPlant.AreaId.Value == membership.AreaId)
                        {
                            int lastStep = salesOrder.SalesOrderApprovals.Select(a => a.Id).Max();
                            if (lastStep == soApprovalId)
                            {
                                // approve / revise

                                List<Business.Entities.SalesOrder> revisedDbObjects = RepoSalesOrder.GetFromPeriodeSOType(salesOrder.Periode, SOType.ZTS.ToString());

                                // cari workflow sebelumnya
                                if (salesOrder.Workflow != null)
                                {
                                    if (salesOrder.Workflow.PreviousStep != null)
                                    {
                                        workflow = await RepoWorkflow.FindAsync(new FilterQuery(Field.Id, FilterOperator.Equals, salesOrder.Workflow.PreviousStep));

                                        if(workflow != null)
                                        {
                                            foreach (Business.Entities.SalesOrder revisedDbObject in revisedDbObjects)
                                            {
                                                revisedDbObject.WorkflowId = workflow.Id;
                                                revisedDbObject.ModifiedBy = membership.User.UserName;
                                                revisedDbObject.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();

                                                SalesOrderApproval salesOrderReviseDbObject = new SalesOrderApproval
                                                {
                                                    SalesOrderId = id,
                                                    WorkflowId = workflow.Id,
                                                    Notes = "",
                                                    CreatedBy = membership.User.UserName,
                                                    CreatedDateTimeUtc = DateTime.Now.ToUtcDateTime()
                                                };

                                                if (await RepoSalesOrder.SaveAsync(revisedDbObject))
                                                {
                                                    if (await RepoSalesOrderApproval.SaveAsync(salesOrderReviseDbObject))
                                                    {
                                                        await SOEmailSender(revisedDbObject, salesOrderReviseDbObject.Id);
                                                    }
                                                    else
                                                    {
                                                        // jika sales order tidak bisa maju workflow nya
                                                        response.SetFail("This Sales Order can't go to Previous Workflow");
                                                    }
                                                }
                                                else
                                                {
                                                    // jika error saat menyimpan sales order
                                                    response.SetFail("This Sales Order Revise is not success");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            // jika sales order tidak punya workflow
                                            response.SetFail("This Sales Order doesn't have workflow");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // jika approval tidak sama
                                response.SetFail("This Sales Order has been approved / revised by another user.");
                            }
                        }
                        else
                        {
                            // jika area so powerplant tidak sama area user
                            response.SetFail("User Area not equals to Sales Order Power Plant Area.");
                        }
                    }
                    else if (salesOrder.PowerPlant.AreaId == null)
                    {
                        // jika power plant tidak punya area
                        response.SetFail("This Sales Order doesn't have area.");
                    }
                    else if (membership.AreaId == null)
                    {
                        // jika user tidak punya area
                        response.SetFail("This User doesn't have area.");
                    }
                }
                else
                {
                    // jika power plant tidak ada
                    response.SetFail("This Sales Order doesn't have Power Plant.");
                }
            }
            else if (membership == null)
            {
                // jika membership tidak ada
                response.SetFail("User is not available.");
            }
            else if (salesOrder == null)
            {
                // jika sales order tidak ada
                response.SetFail("This Sales Order is not available.");
            }

            return View("Index", response);
        }

        #endregion

        public virtual async Task<bool> SOEmailSender(Business.Entities.SalesOrder dbObject, int salesOrderApprovalId, string message = null)
        {
            List<Membership> memberships = null;
            Business.Entities.PowerPlant powerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(dbObject.PowerPlantId);

            string approveAction = dbObject.SOType == "ZPGE" ? "ApproveSalesOrder" : "ApproveSalesOrderZTS";
            string reviseAction = dbObject.SOType == "ZPGE" ? "ReviseSalesOrder" : "ReviseSalesOrderZTS";

            if (powerPlant != null)
            {
                if (powerPlant.AreaId != null)
                {
                    FilterQuery filterQuery = new FilterQuery();
                    filterQuery.AddFilter(Field.AreaId, FilterOperator.Equals, powerPlant.AreaId.Value);
                    memberships = await RepoMembership.FindAllAsync(filterQuery);
                }
            }

            UserRole? userRole;

            switch (dbObject.WorkflowId)
            {
                case 3:
                case 11:
                    userRole = UserRole.ASMEN;
                    break;
                case 4:
                    userRole = UserRole.MANAGER_AREA;
                    break;
                case 5:
                    userRole = UserRole.FINANCE_AREA;
                    break;
                case 7:
                case 9:
                    userRole = UserRole.ASMEN_PARTNERSHIP;
                    break;
                case 8:
                    userRole = UserRole.PARTNERSHIP;
                    break;
                default:
                    userRole = null;
                    break;
            }

            if (userRole != null)
            {
                if (memberships.Any())
                {
                    memberships = memberships.FindAll(n => n.User.Roles.Any(m => m.RoleName == userRole.Value.ToString()));
                    if (memberships.Any())
                    {
                        string subject = "Notifikasi Sales Order IPGE";
                        string body = "";

                        foreach (Membership m in memberships)
                        {
                            string approveUrl = Url.Action(approveAction, "SOServices", new { userId = m.UserId, id = dbObject.Id, soApprovalId = salesOrderApprovalId, area = "SalesOrder" }, Request.Url.Scheme);
                            string reviseUrl = Url.Action(reviseAction, "SOServices", new { userId = m.UserId, id = dbObject.Id, soApprovalId = salesOrderApprovalId, area = "SalesOrder" }, Request.Url.Scheme);
                            body = @"<p><strong>Yth. Bpk / Ibu</strong></p>
                            <p>Dengan ini kami sampaikan bahwa Sales Order berikut ini, menunggu persetujuan/revisi dari Bpk/Ibu.<br /><br /></p>
                            <p><strong>Detail Sales Order :</strong></p>
                            <table style=""width: 347px;"">
                            <tbody>
                            <tr>
                            <td style=""width: 129px;"">SO Type</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.SOType != null ? dbObject.SOType : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Periode</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.Periode != null ? dbObject.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat) : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Ship To party</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (powerPlant.Name != null ? powerPlant.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Sold To Party</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (powerPlant.SoldToPartyId != null ? powerPlant.SoldToParty.Name : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;\"">Material</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (powerPlant.Material != null ? ((Material)Enum.Parse(typeof(Material), powerPlant.Material)).ToDescription() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Quantity</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.Quantity != null ? dbObject.Quantity.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Net Price</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.NetPrice != null ? dbObject.NetPrice.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <td style=""width: 129px;"">Net Value</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.NetValue != null ? dbObject.NetValue.Value.ToString() : "") + @"</td>
                            </tr>
                            <tr>
                            <tr>
                            <td style=""width: 129px;"">Notes</td>
                            <td style=""width: 10px;"">:</td>
                            <td style=""width: 269px;"">" + (dbObject.Notes != null ? dbObject.Notes : "") + @"</td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <p>Untuk verifikasi silahkan klik link di bawah ini</p>
                            <table><tr><td style =""background-color: #4ecdc4;border-color: #45b7af;border-radius: 5px;text-align: center;padding: 10px;"">
                            <a style='display: block; color: #ffffff;font-size: 12px;text-decoration: none;text-transform: uppercase;a:hover{cursor:pointer;}' href=""" + approveUrl + @""">Setujui</a></td>
                            <td style=""background-color: #bf3131;border-color: #45b7af;border-radius: 5px;text-align: center;padding: 10px;"">
                            <a style='display: block; color: #ffffff;font-size: 12px;text-decoration: none;text-transform: uppercase;a:hover{cursor:pointer;}' href=""" + reviseUrl + @""">Revisi</a></td></tr></table>
                            <p>&nbsp;</p>
                            <p>Atas perhatiannya kami ucapkan terima kasih.</p>";
                            string email = m.Email;
                            bool isSuccess = EmailHelper.SendEmail(email, subject, body);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }
    }
}
