﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.SalesOrder.Models
{
    public class ScreenshotSapFormStub : BaseFormStub<ScreenshotSap, ScreenshotSapFormStub>
    {
		public int Id { get; set; }
		public int SalesOrderId { get; set; }
		public string FilePath { get; set; }
		public string FriendlyName { get; set; }
		public string AbsolutePath { get; set; }
		public string CreatedBy { get; set; }
		public System.DateTime CreatedDateTimeUtc { get; set; }
		public string ModifiedBy { get; set; }
		public System.DateTime ModifiedDateTimeUtc { get; set; }

		//validasi data
		public bool IsValid { get; set; }

		public List<string> ValidationMessages { get; set; }

		public ScreenshotSapFormStub() : base()
        {

		}

		public ScreenshotSapFormStub(Business.Entities.ScreenshotSap dbObject) : base(dbObject)
		{
			FriendlyName = WebUI.Controllers.FileManagementController.FriendlyName(FilePath);
			AbsolutePath = new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext).Content(FilePath);
		}
		//public ScreenshotSapFormStub(int salesOrderId) : base()
		//{
		//	SalesOrderId = salesOrderId;
		//}

		public override void MapDbObject(Business.Entities.ScreenshotSap dbObject)
		{
			base.MapDbObject(dbObject);
		}

		protected override void Init()
		{
			//TODO: Set the init object here to be called in all constructor
			IsValid = false;
		}

		public void SetValidationMessage(List<string> messages)
		{
			IsValid = false;
			ValidationMessages = messages;
		}
	}
}