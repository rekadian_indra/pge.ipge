﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.SalesOrder.Models
{
    public class SalesOrderApprovalPresentationStub : BasePresentationStub<Business.Entities.SalesOrderApproval, SalesOrderApprovalPresentationStub>
    {
		#region "Properties"
		public int Id { get; set; }
		public string Name { get; set; }
		public string Code { get; set; }				
		#endregion

		#region "Contructor"
		public SalesOrderApprovalPresentationStub() : base()
        {

        }

        public SalesOrderApprovalPresentationStub(Business.Entities.SalesOrderApproval dbObject) : base(dbObject)
        {
            if (dbObject.Workflow != null)
            {
                Name = dbObject.Workflow.Name;
                Code = dbObject.Workflow.Code;
            }           
		}

        #endregion

        #region "Helper"
        protected override void Init()
        {
            //throw new NotImplementedException();
        }
        #endregion

    }
}