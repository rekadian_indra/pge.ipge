﻿using Business.Infrastructure;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.SalesOrder.Models
{
	public class ListrikBASTExcelStub : BaseBASTExcelStub
	{
		#region Fields

		// kamojang, karaha, lahendong, ulubelu
		public string NomorBastCompany;
		public string NomorBastSoldToParty;
		public string Unit;
		public int URC;
		public int PeriodeProduksi;
		public double PgeTidakOperasi;
		public double HargaDasar;
		public double IndeksInflasi;
		public string AgreementName;
		public string AgreementDate;

		// kamojang, karaha, lahendong
		//public string Tanggal;

		// kamojang, karaha, ulubelu
		public double PgeEkspor;
		public double PgeImpor;
		public string SellerCompanyName;
		public string BuyerCompanyName;

		// kamojang, karaha
		public double KewajibanPlnAmbil;
		public double EksporTenagaListrik;
		public double EnergiEksporNettoHutang;
		public double KewajibanPlnAmbilPerTahun;
		public double JumlahDitagih;
		public string JumlahDitagihTerbilang;

		// kamojang, ulubelu
		public double KumulatifEksporNettoTahunIni;
		
		// ulubelu
		public double DispatchCreditHour;
		public double EnergiMinimumBulanan;
		public double TOP;
		public double JumlahYangDitagih;
		public string JumlahYangDitagihTerbilang;

		// lahendong
		public double MeterUtamaOut;
		public double MeterUtamaIn;
		public double MeterPembandingOut;
		public double MeterPembandingIn;
		public double MeterTransaksiOut;
		public double MeterTransaksiIn;
		public double HargaDasarPE;
		public double JumlahYangDitagihPGE;
		public string JumlahYangDitagihPGETerbilang;

		// karaha
		public double EnergiEksporNettoTerhutang;
		public double KelebihanEksporNettoHutang;
		public double KumulatifEksporNettoSdBulanIni;
		public double KelebihanEnergiThdTOP;
		public double JumlahYangDitagihPRT;
		public double JumlahYangDitagihTotal;
		public string JumlahYangDitagihTotalTerbilang;
		public double KomponenPE;
		public double JumlahYangDitagihPE;
		public string JumlahYangDitagihPETerbilang;
		public double JumlahTotalTagihan;
		public string JumlahTotalTagihanTerbilang;

		#endregion

		#region Constructor

		public ListrikBASTExcelStub() : base()
		{

		}

		public ListrikBASTExcelStub(Business.Entities.SalesOrder salesOrder, List<Business.Entities.SalesOrder> yearSalesOrders = null) : base(salesOrder)
		{
			if (yearSalesOrders != null)
			{
				CalculateYearlyCumulativeExport(yearSalesOrders);
			}

			var USD_STR = "Dolar AS";

			// kamojang, karaha, lahendong, ulubelu
			NomorBastCompany = salesOrder.PowerPlant.Area.Company.Name + " No. : " + salesOrder.Id;
			NomorBastSoldToParty = salesOrder.PowerPlant.SoldToParty.Name + " No. : ";
			Unit = salesOrder.PowerPlant.ShipToPartyName;//.Substring(salesOrder.PowerPlant.Name.LastIndexOf(' ') + 1);
			URC = salesOrder.PowerPlant.URC ?? 0;
			PeriodeProduksi = DateTime.DaysInMonth(salesOrder.Periode.Value.Year, salesOrder.Periode.Value.Month) * 24;
			PgeTidakOperasi = salesOrder.PowerPlant.Pltps?.Sum(x => x.GppsShutdown ?? 0) ?? 0; // ga yakin
			HargaDasar = salesOrder.PowerPlant.BasePrice;
			IndeksInflasi = salesOrder.PowerPlant.PPIs.First(ppi =>
				(ppi.Year == salesOrder.Periode.Value.AddMonths(-3).Year) &&
				(ppi.QuarterNum == DateHelper.MonthToQuarter(salesOrder.Periode.Value.AddMonths(-3).Month))
			).InflationIndex;

			// kamojang, karaha, lahendong
			AgreementName = salesOrder.PowerPlant.AgreementName;
			AgreementDate = salesOrder.PowerPlant.AgreementDate?.ToString(DisplayFormat.MonthYearDateFormat);

			// kamojang, karaha, ulubelu
			PgeEkspor = salesOrder.PowerPlant.Pltps?.Sum(x => x.EnergyNett ?? 0) ?? 0;
			PgeImpor = salesOrder.PowerPlant.Pltps?.Sum(x => x.ImportEnergy ?? 0) ?? 0;
			SellerCompanyName = salesOrder.PowerPlant.Area.Company.Name;
			BuyerCompanyName = salesOrder.PowerPlant.SoldToParty.Name;

			// kamojang, karaha
			KewajibanPlnAmbil = 0.9 * URC * PeriodeProduksi;
			EksporTenagaListrik = PgeEkspor;
			EnergiEksporNettoHutang = PgeEkspor - PgeImpor;
			KewajibanPlnAmbilPerTahun = 0.9 * URC * 24 * ((DateTime.Now.Year % 4 == 0) ? 366 : 365);
			JumlahDitagih = EnergiEksporNettoHutang * HargaDasar * IndeksInflasi; // atau PgeEkspor * HargaDasar * IndeksInflasi
			JumlahDitagihTerbilang = "Terbilang: " + DisplayFormat.Terbilang(JumlahDitagih) + " " + USD_STR;

			// kamojang, ulubelu
			//KumulatifEksporNettoTahunIni = 0; // calculating function called separately

			// ulubelu
			DispatchCreditHour = 0; // dummy
			EnergiMinimumBulanan = 0.9 * URC * PeriodeProduksi; // = KewajibanPlnAmbil
			TOP = 0.9 * URC * PeriodeProduksi; // = KewajibanPlnAmbil
			JumlahYangDitagih = PgeEkspor * HargaDasar * IndeksInflasi; // {PgeEkspor} atau {PgeEkspor - PgeImpor} ya?
			JumlahYangDitagihTerbilang = "Terbilang: " + DisplayFormat.Terbilang(JumlahYangDitagih) + " " + USD_STR;

			// lahendong
			MeterUtamaOut = PgeEkspor;
			MeterUtamaIn = PgeImpor;
			MeterPembandingOut = 0; // dummy
			MeterPembandingIn = 0; // dummy
			MeterTransaksiOut = MeterUtamaOut;
			MeterTransaksiIn = MeterUtamaIn;
			HargaDasarPE = 0.0006; // fixed cenah
			JumlahYangDitagihPGE = MeterUtamaOut * HargaDasarPE;
			JumlahYangDitagihPGETerbilang = "Terbilang: " + DisplayFormat.Terbilang(JumlahYangDitagihPGE) + " " + USD_STR;

			// karaha
			KumulatifEksporNettoSdBulanIni = Math.Min(KumulatifEksporNettoSdBulanIni, KewajibanPlnAmbilPerTahun); // calculating function called separately
			KelebihanEnergiThdTOP = Math.Max(KumulatifEksporNettoSdBulanIni - KewajibanPlnAmbil, 0);
			KelebihanEksporNettoHutang = KelebihanEnergiThdTOP;
			EnergiEksporNettoTerhutang = EnergiEksporNettoHutang - KelebihanEksporNettoHutang;
			KomponenPE = 0.00350; // kemungkinan besar fixed juga
			JumlahYangDitagihPRT = salesOrder.PowerPlant.PPIs.Where(ppi =>
				(ppi.Year == salesOrder.Periode.Value.AddMonths(-3).Year) &&
				(ppi.QuarterNum <= DateHelper.MonthToQuarter(salesOrder.Periode.Value.AddMonths(-3).Month))
			).Average(ppi => ppi.NextPrice) * KomponenPE * KelebihanEnergiThdTOP; // kemungkinan gini
			JumlahYangDitagihTotal = JumlahDitagih + JumlahYangDitagihPRT;
			JumlahYangDitagihTotalTerbilang = "Terbilang: " + DisplayFormat.Terbilang(JumlahYangDitagihTotal) + " " + USD_STR;
			JumlahYangDitagihPE = PgeEkspor * KomponenPE; // bener, ga usah dikali indeks inflasi lagi
			JumlahYangDitagihPETerbilang = "Terbilang: " + DisplayFormat.Terbilang(JumlahYangDitagihPE) + " " + USD_STR;
			JumlahTotalTagihan = JumlahYangDitagihTotal + JumlahYangDitagihPE;
			JumlahTotalTagihanTerbilang = "Terbilang: " + DisplayFormat.Terbilang(JumlahTotalTagihan) + " " + USD_STR;
		}

		#endregion

		#region Helper

		public void CalculateYearlyCumulativeExport(List<Business.Entities.SalesOrder> yearSalesOrders)
		{
			KumulatifEksporNettoTahunIni = KumulatifEksporNettoSdBulanIni = yearSalesOrders.SelectMany(x => x.PowerPlant.Pltps).Sum(x => x.EnergyNett ?? 0);
		}

		#endregion
	}
}