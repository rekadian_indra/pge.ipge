﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.SalesOrder.Models
{
    public class PowerPlantFormStub : BaseFormStub<Business.Entities.PowerPlant, PowerPlantFormStub>
    {
		public int Id { get; set; }

        [DisplayName("Nama")]
        public string Name { get; set; }

		[DisplayName("Material")]
		public string Material { get; set; }

		[DisplayName("Material")]
		public string StrMaterial { get; set; }

		[DisplayName("SO Type")]
		public string SOType { get; set; }

		public string AgreementName { get; set; }
		public Nullable<System.DateTime> AgreementDate { get; set; }

		[DisplayName("Contain Additional SO Detail")]
		public bool IsAdditionalSODetail { get; set; }

		public List<SalesOrderFormStub> SalesOrders { get; set; }

		public PowerPlantFormStub() : base()
        {

		}
		public PowerPlantFormStub(Business.Entities.PowerPlant dbObject) : base(dbObject)
		{
			IsAdditionalSODetail = false;
			SalesOrders = new List<SalesOrderFormStub>();
			StrMaterial = ((Material)Enum.Parse(typeof(Material), dbObject.Material)).ToDescription();

		}

		public PowerPlantFormStub(List<Business.Entities.SalesOrder> salesOrders)
		{
			SalesOrders = ListMapper.MapList<Business.Entities.SalesOrder, SalesOrderFormStub>(salesOrders);
		}

		public PowerPlantFormStub(Business.Entities.PowerPlant dbObject, List<Business.Entities.SalesOrder> salesOrders) : base(dbObject)
        {
			SalesOrders = ListMapper.MapList<Business.Entities.SalesOrder, SalesOrderFormStub>(salesOrders);
			StrMaterial = ((Material)Enum.Parse(typeof(Material), dbObject.Material)).ToDescription();
		}

        public override void MapDbObject(Business.Entities.PowerPlant dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor      
        }
	}
}