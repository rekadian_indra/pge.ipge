﻿using Business.Infrastructure;
using NPOI.XSSF.UserModel;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using WebUI.Infrastructure;
using WebUI.Models;
using NPOI.SS.UserModel;
using System.Collections.Generic;

namespace WebUI.Areas.SalesOrder.Models
{
	public class SalesOrderExcelStub : BaseExcelStub
	{
		#region Constructor

		public SalesOrderExcelStub() : base()
		{

		}

		public SalesOrderExcelStub(XSSFWorkbook workbook) : base(workbook)
		{

		}

		#endregion

		#region BAST Generator

		public byte[] GenerateTemplateElectric(Business.Entities.SalesOrder salesOrder, List<Business.Entities.SalesOrder> allSalesOrders)
		{
			List<Business.Entities.SalesOrder> yearSalesOrders = allSalesOrders.Where(s => s.Periode?.Year == salesOrder.Periode?.Year).ToList();
			switch (salesOrder.PowerPlant.TemplateBastFileName)
			{
				case "BA_KAMOJANG_LISTRIK.xlsx": ProcessKamojangListrik(salesOrder, yearSalesOrders); break;
				case "BA_KARAHA_LISTRIK.xlsx": ProcessKarahaListrik(salesOrder, yearSalesOrders); break;
				case "BA_LAHENDONG_LISTRIK.xlsx": ProcessLahendongListrik(salesOrder); break;
				case "BA_ULUBELU_LISTRIK.xlsx": ProcessUlubeluListrik(salesOrder, yearSalesOrders); break;
				default: break;
			}

			return WorkbookToArray();
		}

		public byte[] GenerateTemplateSteam(Business.Entities.SalesOrder salesOrder, List<Business.Entities.TaskReport> allTaskReports)
		{
			switch (salesOrder.PowerPlant.TemplateBastFileName)
			{
				case "BA_KAMOJANG_UAP.xlsx"		: ProcessKamojangUap(salesOrder, allTaskReports); break;
				case "BA_LAHENDONG_UAP.xlsx"	: ProcessLahendongUap(salesOrder, allTaskReports); break;
				case "BA_ULUBELU_UAP.xlsx"		: ProcessUlubeluUap(salesOrder, allTaskReports); break;
				default: break;
			}

			return WorkbookToArray();
		}

		public void ProcessKamojangUap(Business.Entities.SalesOrder salesOrder, List<Business.Entities.TaskReport> allTaskReports)
		{
			// proses data
			UapBASTExcelStub model = new UapBASTExcelStub(salesOrder, allTaskReports);

			// set sheet
			Workbook.SetSheetName(0, "BA " + model.Periode);
			XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(0);

			// masuk2in data
			SetCellValue(sheet, "A", 6, model.Title);
			SetCellValue(sheet, "E", 9, model.NamaMeter);
			SetCellValue(sheet, "E", 10, model.TipeMeter);
			SetCellValue(sheet, "E", 12, model.PeriodeMulai);
			SetCellValue(sheet, "E", 13, model.PeriodeSampai);
			SetCellValue(sheet, "E", 15, model.KwhDariPembangkit, DataFormatDecimalTwo);
			SetCellValue(sheet, "E", 16, model.KwhKePembangkit, DataFormatDecimalTwo);
			SetCellValue(sheet, "E", 17, model.KvarhDariPembangkit, DataFormatDecimalTwo);
			SetCellValue(sheet, "E", 18, model.KvarhKePembangkit, DataFormatDecimalTwo);
			SetCellValue(sheet, "A", 21, model.Catatan);
			SetCellValue(sheet, "F", 29, model.SignaturePlaceDate);
			SetCellValue(sheet, "A", 31, model.CompanyName);
			SetCellValue(sheet, "F", 31, model.SoldToPartyName);
			SetCellValue(sheet, "C", 39, model.SellerName);
			SetCellValue(sheet, "C", 40, model.SellerPosition);
			SetCellValue(sheet, "H", 39, model.BuyerName);
			SetCellValue(sheet, "H", 40, model.BuyerPosition);

			ProcessRekapitulasiBulananUap(model);
		}

		public void ProcessKamojangListrik(Business.Entities.SalesOrder salesOrder, List<Business.Entities.SalesOrder> yearSalesOrders)
		{
			// proses data
			ListrikBASTExcelStub model = new ListrikBASTExcelStub(salesOrder, yearSalesOrders);

			// set sheet
			Workbook.SetSheetName(0, "BA " + model.Periode);
			XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(0);

			// masuk2in data
			//SetCellValue(sheet, "B", 3, model.CompanyName);
			//SetCellValue(sheet, "B", 5, model.SoldToPartyName);
			SetCellValue(sheet, "B", 6, model.AgreementName);
			SetCellValue(sheet, "B", 7, model.AgreementDate);
			SetCellValue(sheet, "B", 9, model.NomorBastCompany);
			SetCellValue(sheet, "B", 10, model.NomorBastSoldToParty);
			SetCellValue(sheet, "B", 12, model.Periode);
			SetCellValue(sheet, "B", 19, model.Unit);
			SetCellValue(sheet, "C", 19, model.URC, DataFormatNumeric);
			SetCellValue(sheet, "D", 19, model.PeriodeProduksi, DataFormatNumeric);
			SetCellValue(sheet, "E", 19, model.PgeTidakOperasi, DataFormatNumeric);
			SetCellValue(sheet, "F", 19, model.PgeEkspor, DataFormatDecimalTwo);
			SetCellValue(sheet, "G", 19, model.KewajibanPlnAmbil, DataFormatDecimalTwo);
			SetCellValue(sheet, "H", 19, model.EksporTenagaListrik, DataFormatDecimalTwo);
			SetCellValue(sheet, "I", 19, model.PgeImpor, DataFormatDecimalTwo);
			SetCellValue(sheet, "J", 19, model.EnergiEksporNettoHutang, DataFormatDecimalTwo);

			SetCellValue(sheet, "B", 26, model.Unit);
			SetCellValue(sheet, "C", 26, model.URC, DataFormatNumeric);
			SetCellValue(sheet, "D", 26, model.KewajibanPlnAmbilPerTahun, DataFormatDecimalTwo);
			SetCellValue(sheet, "E", 26, model.EnergiEksporNettoHutang, DataFormatDecimalTwo);
			SetCellValue(sheet, "G", 26, model.KumulatifEksporNettoTahunIni, DataFormatDecimalTwo);
			SetCellValue(sheet, "H", 26, model.HargaDasar, DataFormatDecimalFive);
			SetCellValue(sheet, "I", 26, model.IndeksInflasi, DataFormatDecimalFive);
			SetCellValue(sheet, "J", 26, model.JumlahDitagih, DataFormatDecimalTwo);
			SetCellValue(sheet, "B", 27, model.JumlahDitagihTerbilang);

			SetCellValue(sheet, "B", 33, model.SellerCompanyName);
			SetCellValue(sheet, "B", 38, model.SellerName);
			SetCellValue(sheet, "B", 39, model.SellerPosition);
			SetCellValue(sheet, "D", 41, model.SignatureDate);

			SetCellValue(sheet, "I", 33, model.BuyerCompanyName);
			SetCellValue(sheet, "I", 38, model.BuyerName);
			SetCellValue(sheet, "I", 39, model.BuyerPosition);
			SetCellValue(sheet, "K", 41, model.SignatureDate);
		}

		public void ProcessKarahaListrik(Business.Entities.SalesOrder salesOrder, List<Business.Entities.SalesOrder> yearSalesOrders)
		{
			// proses data
			ListrikBASTExcelStub model = new ListrikBASTExcelStub(salesOrder, yearSalesOrders);

			// set sheet
			Workbook.SetSheetName(0, "BA " + model.Periode);
			XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(0);

			// masuk2in data
			//SetCellValue(sheet, "B", 2, "ANTARA " + model.CompanyName + " DAN " + model.SoldToPartyName);
			//SetCellValue(sheet, "B", 3, "PERJANJIAN JUAL BELI TENAGA LISTRIK PLTP KARAHA TANGGAL " + model.Tanggal);

			SetCellValue(sheet, "B", 3, model.AgreementName + " " + model.AgreementDate);
			SetCellValue(sheet, "B", 5, model.NomorBastCompany);
			SetCellValue(sheet, "B", 7, model.NomorBastSoldToParty);
			SetCellValue(sheet, "B", 9, model.Periode);
			SetCellValue(sheet, "B", 15, model.Unit);
			SetCellValue(sheet, "C", 15, model.URC);
			SetCellValue(sheet, "D", 15, model.PeriodeProduksi, DataFormatNumeric);
			SetCellValue(sheet, "E", 15, model.PgeTidakOperasi, DataFormatNumeric);
			SetCellValue(sheet, "F", 15, model.PgeEkspor, DataFormatDecimalTwo);
			SetCellValue(sheet, "G", 15, model.KewajibanPlnAmbil, DataFormatDecimalTwo);
			SetCellValue(sheet, "H", 15, model.EksporTenagaListrik, DataFormatDecimalTwo);
			SetCellValue(sheet, "I", 15, model.PgeImpor, DataFormatDecimalTwo);
			SetCellValue(sheet, "J", 15, model.EnergiEksporNettoHutang, DataFormatDecimalTwo);

			SetCellValue(sheet, "B", 22, model.Unit);
			SetCellValue(sheet, "C", 22, model.URC, DataFormatNumeric);
			SetCellValue(sheet, "D", 22, model.KewajibanPlnAmbilPerTahun, DataFormatDecimalTwo);
			SetCellValue(sheet, "E", 22, model.EnergiEksporNettoTerhutang, DataFormatDecimalTwo);
			SetCellValue(sheet, "F", 22, model.KelebihanEksporNettoHutang, DataFormatDecimalTwo);
			SetCellValue(sheet, "G", 22, model.KumulatifEksporNettoSdBulanIni, DataFormatDecimalTwo);
			SetCellValue(sheet, "G", 23, model.KelebihanEnergiThdTOP, DataFormatDecimalTwo);
			SetCellValue(sheet, "H", 22, model.HargaDasar, DataFormatDecimalFive);
			SetCellValue(sheet, "I", 22, model.IndeksInflasi, DataFormatDecimalFive);
			SetCellValue(sheet, "K", 22, model.JumlahDitagih, DataFormatDecimalTwo);
			SetCellValue(sheet, "K", 23, model.JumlahYangDitagihPRT, DataFormatDecimalTwo);
			SetCellValue(sheet, "K", 24, model.JumlahYangDitagihTotal, DataFormatDecimalTwo);
			SetCellValue(sheet, "B", 25, model.JumlahYangDitagihTotalTerbilang);

			SetCellValue(sheet, "F", 30, model.Unit);
			SetCellValue(sheet, "G", 30, model.URC, DataFormatNumeric);
			SetCellValue(sheet, "H", 30, model.PgeEkspor, DataFormatDecimalTwo);
			SetCellValue(sheet, "I", 30, model.KomponenPE, DataFormatDecimalFive);
			SetCellValue(sheet, "J", 30, model.JumlahYangDitagihPE, DataFormatDecimalTwo);
			SetCellValue(sheet, "B", 31, model.JumlahYangDitagihPETerbilang);

			SetCellValue(sheet, "J", 33, model.JumlahTotalTagihan, DataFormatDecimalTwo);
			SetCellValue(sheet, "B", 34, model.JumlahTotalTagihanTerbilang);

			SetCellValue(sheet, "B", 39, model.SellerCompanyName);
			SetCellValue(sheet, "B", 45, model.SellerName);
			SetCellValue(sheet, "B", 46, model.SellerPosition);
			SetCellValue(sheet, "D", 47, model.SignatureDate);

			SetCellValue(sheet, "I", 39, model.BuyerCompanyName);
			SetCellValue(sheet, "I", 45, model.BuyerName);
			SetCellValue(sheet, "I", 46, model.BuyerPosition);
			SetCellValue(sheet, "K", 47, model.SignatureDate);
		}

		public void ProcessLahendongListrik(Business.Entities.SalesOrder salesOrder)
		{
			// proses data
			ListrikBASTExcelStub model = new ListrikBASTExcelStub(salesOrder);

			// set sheet
			Workbook.SetSheetName(0, "BA " + model.Periode);
			XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(0);

			// masuk2in data
			//SetCellValue(sheet, "A", 3, model.CompanyName);
			//SetCellValue(sheet, "A", 5, model.SoldToPartyName);
			SetCellValue(sheet, "A", 6, model.AgreementName);
			SetCellValue(sheet, "A", 7, model.AgreementDate);
			SetCellValue(sheet, "A", 9, model.NomorBastCompany);
			SetCellValue(sheet, "A", 10, model.NomorBastSoldToParty);
			SetCellValue(sheet, "A", 12, model.Periode);
			SetCellValue(sheet, "B", 16, model.Unit);
			SetCellValue(sheet, "F", 16, model.URC, DataFormatNumeric);
			SetCellValue(sheet, "G", 16, model.PeriodeProduksi, DataFormatNumeric);
			SetCellValue(sheet, "H", 16, model.PgeTidakOperasi, DataFormatNumeric);
			SetCellValue(sheet, "I", 16, model.MeterUtamaOut, DataFormatDecimalTwo);
			SetCellValue(sheet, "I", 17, model.MeterUtamaIn, DataFormatDecimalTwo);
			SetCellValue(sheet, "K", 16, model.MeterPembandingOut, DataFormatDecimalTwo);
			SetCellValue(sheet, "K", 17, model.MeterPembandingIn, DataFormatDecimalTwo);
			SetCellValue(sheet, "M", 16, model.MeterTransaksiOut, DataFormatDecimalTwo);
			SetCellValue(sheet, "M", 17, model.MeterTransaksiIn, DataFormatDecimalTwo);
			SetCellValue(sheet, "N", 16, model.HargaDasar, DataFormatDecimalFive);
			SetCellValue(sheet, "O", 16, model.IndeksInflasi, DataFormatDecimalFive);
			SetCellValue(sheet, "P", 16, model.JumlahYangDitagihPGE, DataFormatDecimalTwo);
			SetCellValue(sheet, "A", 20, model.JumlahYangDitagihPGETerbilang);

			SetCellValue(sheet, "B", 24, model.Unit);
			SetCellValue(sheet, "F", 24, model.URC, DataFormatNumeric);
			SetCellValue(sheet, "G", 24, model.PeriodeProduksi, DataFormatNumeric);
			SetCellValue(sheet, "H", 24, model.PgeTidakOperasi, DataFormatNumeric);
			SetCellValue(sheet, "I", 24, model.MeterUtamaOut, DataFormatDecimalTwo);
			SetCellValue(sheet, "I", 25, model.MeterUtamaIn, DataFormatDecimalTwo);
			SetCellValue(sheet, "K", 24, model.MeterPembandingOut, DataFormatDecimalTwo);
			SetCellValue(sheet, "K", 25, model.MeterPembandingIn, DataFormatDecimalTwo);
			SetCellValue(sheet, "M", 24, model.MeterTransaksiOut, DataFormatDecimalTwo);
			SetCellValue(sheet, "M", 25, model.MeterTransaksiIn, DataFormatDecimalTwo);
			SetCellValue(sheet, "N", 24, model.HargaDasarPE, DataFormatDecimalTwo);
			SetCellValue(sheet, "O", 24, model.IndeksInflasi, DataFormatDecimalFive);
			SetCellValue(sheet, "P", 24, model.JumlahYangDitagihPGE, DataFormatDecimalTwo);
			SetCellValue(sheet, "A", 26, model.JumlahYangDitagihPGETerbilang);

			SetCellValue(sheet, "A", 33, model.SellerCompanyName);
			SetCellValue(sheet, "A", 40, model.SellerName);
			SetCellValue(sheet, "A", 41, model.SellerPosition);
			SetCellValue(sheet, "J", 31, model.SignaturePlaceDate);

			SetCellValue(sheet, "J", 33, model.BuyerCompanyName);
			SetCellValue(sheet, "J", 40, model.BuyerName);
			SetCellValue(sheet, "J", 41, model.BuyerPosition);
			SetCellValue(sheet, "J", 42, "");
		}

		public void ProcessLahendongUap(Business.Entities.SalesOrder salesOrder, List<Business.Entities.TaskReport> allTaskReports)
		{
			// proses data
			UapBASTExcelStub model = new UapBASTExcelStub(salesOrder, allTaskReports);

			// set sheet
			Workbook.SetSheetName(0, "BA " + model.Periode);
			XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(0);

			// masuk2in data
			SetCellValue(sheet, "A", 6, model.Title);
			SetCellValue(sheet, "E", 9, model.NamaMeter);
			SetCellValue(sheet, "E", 10, model.TipeMeter);
			SetCellValue(sheet, "E", 12, model.PeriodeMulai);
			SetCellValue(sheet, "E", 13, model.PeriodeSampai);
			SetCellValue(sheet, "E", 15, model.KwhDariPembangkit, DataFormatDecimalTwo);
			SetCellValue(sheet, "E", 16, model.KwhKePembangkit, DataFormatDecimalTwo);
			SetCellValue(sheet, "E", 17, model.KvarhDariPembangkit);
			SetCellValue(sheet, "E", 18, model.KvarhKePembangkit);
			SetCellValue(sheet, "A", 21, model.Catatan);
			SetCellValue(sheet, "F", 29, model.SignaturePlaceDate);
			SetCellValue(sheet, "A", 31, model.CompanyName);
			SetCellValue(sheet, "F", 31, model.SoldToPartyName);
			SetCellValue(sheet, "C", 39, model.SellerName);
			SetCellValue(sheet, "C", 40, model.SellerPosition);
			SetCellValue(sheet, "H", 39, model.BuyerName);
			SetCellValue(sheet, "H", 40, model.BuyerPosition);

			ProcessRekapitulasiBulananUap(model);
		}

		public void ProcessUlubeluListrik(Business.Entities.SalesOrder salesOrder, List<Business.Entities.SalesOrder> yearSalesOrders)
		{
			// proses data
			ListrikBASTExcelStub model = new ListrikBASTExcelStub(salesOrder, yearSalesOrders);

			// set sheet
			Workbook.SetSheetName(0, "BA " + model.Periode);
			XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(0);

			// masuk2in data
			//SetCellValue(sheet, "B", 3, model.CompanyName);
			//SetCellValue(sheet, "B", 5, model.SoldToPartyName);
			SetCellValue(sheet, "B", 6, model.AgreementName);
			
			SetCellValue(sheet, "B", 8, model.NomorBastCompany);
			SetCellValue(sheet, "B", 9, model.NomorBastSoldToParty);
			SetCellValue(sheet, "B", 11, model.Periode);
			SetCellValue(sheet, "B", 18, model.Unit);
			SetCellValue(sheet, "C", 18, model.URC, DataFormatNumeric);
			SetCellValue(sheet, "D", 18, model.PeriodeProduksi, DataFormatNumeric);
			SetCellValue(sheet, "E", 18, model.PgeTidakOperasi, DataFormatNumeric);
			SetCellValue(sheet, "F", 18, model.DispatchCreditHour, DataFormatDecimalTwo);
			SetCellValue(sheet, "G", 18, model.EnergiMinimumBulanan, DataFormatDecimalTwo);
			SetCellValue(sheet, "H", 18, model.PgeImpor, DataFormatDecimalTwo);
			SetCellValue(sheet, "I", 18, model.PgeEkspor, DataFormatDecimalTwo);

			SetCellValue(sheet, "B", 25, model.Unit);
			SetCellValue(sheet, "C", 25, model.TOP, DataFormatDecimalTwo);
			SetCellValue(sheet, "D", 25, model.KumulatifEksporNettoTahunIni, DataFormatDecimalTwo);
			SetCellValue(sheet, "E", 25, model.EnergiEksporNettoHutang, DataFormatDecimalTwo);
			SetCellValue(sheet, "F", 25, model.HargaDasar, DataFormatDecimalFive);
			SetCellValue(sheet, "G", 25, model.IndeksInflasi, DataFormatDecimalFive);
			SetCellValue(sheet, "H", 25, model.JumlahDitagih, DataFormatDecimalTwo);
			SetCellValue(sheet, "B", 26, model.JumlahDitagihTerbilang);

			SetCellValue(sheet, "B", 32, model.SellerCompanyName);
			SetCellValue(sheet, "B", 42, model.SellerName);
			SetCellValue(sheet, "B", 43, model.SellerPosition);
			SetCellValue(sheet, "B", 45, "Tanggal: " + model.SignatureDate);

			SetCellValue(sheet, "H", 32, model.BuyerCompanyName);
			SetCellValue(sheet, "H", 42, model.BuyerName);
			SetCellValue(sheet, "H", 43, model.BuyerPosition);
			SetCellValue(sheet, "H", 45, "Tanggal: " + model.SignatureDate);
		}

		public void ProcessUlubeluUap(Business.Entities.SalesOrder salesOrder, List<Business.Entities.TaskReport> allTaskReports)
		{
			// proses data
			UapBASTExcelStub model = new UapBASTExcelStub(salesOrder, allTaskReports);

			// set sheet
			Workbook.SetSheetName(0, "BA " + model.Periode);
			XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(0);

			// masuk2in data
			SetCellValue(sheet, "A", 6, model.Title);
			SetCellValue(sheet, "D", 9, model.NamaMeter);
			SetCellValue(sheet, "D", 10, model.TipeMeter);
			SetCellValue(sheet, "D", 12, model.PeriodeMulai);
			SetCellValue(sheet, "D", 13, model.PeriodeSampai);
			SetCellValue(sheet, "D", 15, model.KwhDariPembangkit, DataFormatDecimalTwo);
			SetCellValue(sheet, "D", 16, model.KwhKePembangkit, DataFormatDecimalTwo);
			SetCellValue(sheet, "D", 17, model.KvarhDariPembangkit, DataFormatDecimalTwo);
			SetCellValue(sheet, "D", 18, model.KvarhKePembangkit, DataFormatDecimalTwo);
			SetCellValue(sheet, "A", 21, model.Catatan);
			SetCellValue(sheet, "E", 29, model.SignaturePlaceDate);
			SetCellValue(sheet, "A", 31, model.CompanyName);
			SetCellValue(sheet, "E", 31, model.SoldToPartyName);
			SetCellValue(sheet, "B", 39, model.SellerName);
			SetCellValue(sheet, "B", 40, model.SellerPosition);
			SetCellValue(sheet, "F", 39, model.BuyerName);
			SetCellValue(sheet, "F", 40, model.BuyerPosition);

			ProcessRekapitulasiBulananUap(model);
		}

		public void ProcessRekapitulasiBulananUap(UapBASTExcelStub model)
		{
			// set sheet 2
			XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(1);

			// masuk2in data
			SetCellValue(sheet, "A", 6, model.Title);
			SetCellValue(sheet, "C", 9, ": " + model.UnitNumber);
			SetCellValue(sheet, "C", 10, ": " + model.Periode);

			for (int i = 0; i < model.Harians.Length; i++)
			{
				if (model.Harians[i].Tanggal.HasValue) SetCellValue(sheet, "A", 14 + i, model.Harians[i].Tanggal.Value);
				else SetCellValue(sheet, "A", 14 + i, "");

				SetCellValue(sheet, "B", 14 + i, model.Harians[i].Waktu);

				if (model.Harians[i].JPH.HasValue) SetCellValue(sheet, "C", 14 + i, model.Harians[i].JPH.Value);
				else SetCellValue(sheet, "C", 14 + i, "");

				if (model.Harians[i].KumulatifPembangkitan.HasValue) SetCellValue(sheet, "D", 14 + i, model.Harians[i].KumulatifPembangkitan.Value);
				else SetCellValue(sheet, "D", 14 + i, "");

				if (model.Harians[i].PembangkitanPerHari.HasValue) SetCellValue(sheet, "E", 14 + i, model.Harians[i].PembangkitanPerHari.Value);
				else SetCellValue(sheet, "E", 14 + i, "");

				if (model.Harians[i].UapTersalurkan.HasValue) SetCellValue(sheet, "F", 14 + i, model.Harians[i].UapTersalurkan.Value);
				else SetCellValue(sheet, "F", 14 + i, "");
				
				if (model.Harians[i].PotensiTOP.HasValue) SetCellValue(sheet, "G", 14 + i, model.Harians[i].PotensiTOP.Value);
				else SetCellValue(sheet, "G", 14 + i, "");

				if (model.Harians[i].PotensiDop.HasValue) SetCellValue(sheet, "H", 14 + i, model.Harians[i].PotensiDop.Value);
				else SetCellValue(sheet, "H", 14 + i, "");

				SetCellValue(sheet, "I", 14 + i, model.Harians[i].Keterangan);

				SetCellValue(sheet, "J", 14 + i, model.Harians[i].Paraf1);

				SetCellValue(sheet, "K", 14 + i, model.Harians[i].Paraf2);
			}

			SetCellValue(sheet, "C", 45, model.SumJPH);
			SetCellValue(sheet, "E", 45, model.SumPembangkitanPerHari);
			SetCellValue(sheet, "F", 45, model.SumUapTersalurkan);
		}

		#endregion

		#region SAP Generator

		public byte[] GenerateSapZPGE(Business.Entities.SalesOrder salesOrder, List<Business.Entities.TaskReport> taskReports)
		{
			ProcessSapZPGE(salesOrder, taskReports);
			return WorkbookToArray();
		}

		public byte[] GenerateSapZTS(List<Business.Entities.SalesOrder> monthSalesOrders)
		{
			ProcessSapZTS(monthSalesOrders);
			return WorkbookToArray();
		}

		public void ProcessSapZPGE(Business.Entities.SalesOrder salesOrder, List<Business.Entities.TaskReport> taskReports)
		{
			// proses data
			SAPExcelStub model = new SAPExcelStub(salesOrder, taskReports);

			// set sheet
			XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(0);

			// masuk2in data
			for (int i = 0; i < model.Harians.Count(); i++)
			{
				SAPExcelStub.Harian curHarian = model.Harians[i];
				SetCellValue(sheet, "A", 2 + i, curHarian.SalesOrganization);
				SetCellValue(sheet, "B", 2 + i, curHarian.DistributionChanel);
				SetCellValue(sheet, "C", 2 + i, curHarian.Division);
				SetCellValue(sheet, "D", 2 + i, curHarian.PricingDate);
				SetCellValue(sheet, "E", 2 + i, curHarian.OrdetType);
				SetCellValue(sheet, "F", 2 + i, curHarian.Soldtoparty);
				SetCellValue(sheet, "G", 2 + i, curHarian.Shiptoparty);
				SetCellValue(sheet, "H", 2 + i, curHarian.Material);

				if (curHarian.TargetOrderQty.HasValue) SetCellValue(sheet, "I", 2 + i, curHarian.TargetOrderQty.Value);
				else SetCellValue(sheet, "I", 2 + i, "");

				SetCellValue(sheet, "J", 2 + i, curHarian.UoM);
				SetCellValue(sheet, "K", 2 + i, curHarian.SDCurrency);
				SetCellValue(sheet, "L", 2 + i, curHarian.Plant);
				SetCellValue(sheet, "M", 2 + i, curHarian.ShippingPoint);
				SetCellValue(sheet, "N", 2 + i, curHarian.MaterialText);
			}
		}
		
		public void ProcessSapZTS(List<Business.Entities.SalesOrder> monthSalesOrders)
		{
			// proses data
			SAPExcelStub model = new SAPExcelStub(monthSalesOrders);

			// set sheet
			XSSFSheet sheet = (XSSFSheet)Workbook.GetSheetAt(0);

			// masuk2in data
			for (int i = 0; i < model.Harians.Count(); i++)
			{
				SAPExcelStub.Harian curHarian = model.Harians[i];
				SetCellValue(sheet, "A", 2 + i, curHarian.SalesOrganization);
				SetCellValue(sheet, "B", 2 + i, curHarian.DistributionChanel);
				SetCellValue(sheet, "C", 2 + i, curHarian.Division);
				SetCellValue(sheet, "D", 2 + i, curHarian.PricingDate);
				SetCellValue(sheet, "E", 2 + i, curHarian.OrdetType);
				SetCellValue(sheet, "F", 2 + i, curHarian.Soldtoparty);
				SetCellValue(sheet, "G", 2 + i, curHarian.Shiptoparty);
				SetCellValue(sheet, "H", 2 + i, curHarian.Material);

				if (curHarian.TargetOrderQty.HasValue) SetCellValue(sheet, "I", 2 + i, curHarian.TargetOrderQty.Value);
				else SetCellValue(sheet, "I", 2 + i, "");

				SetCellValue(sheet, "J", 2 + i, curHarian.UoM);
				SetCellValue(sheet, "K", 2 + i, curHarian.SDCurrency);
				SetCellValue(sheet, "L", 2 + i, curHarian.Plant);
				SetCellValue(sheet, "M", 2 + i, curHarian.ShippingPoint);
				SetCellValue(sheet, "N", 2 + i, curHarian.MaterialText);
				SetCellValue(sheet, "O", 2 + i, curHarian.Requisitioner);
				SetCellValue(sheet, "P", 2 + i, curHarian.PurchasingGroup);

				if (curHarian.PriceinPR.HasValue) SetCellValue(sheet, "Q", 2 + i, curHarian.PriceinPR.Value);
				else SetCellValue(sheet, "Q", 2 + i, "");

				SetCellValue(sheet, "R", 2 + i, curHarian.Currency);
				SetCellValue(sheet, "S", 2 + i, curHarian.PRReleaseDate);
			}
		}

		#endregion

		#region Helper

		#endregion


	}
}