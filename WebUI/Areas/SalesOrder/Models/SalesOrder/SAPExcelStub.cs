﻿using Business.Infrastructure;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.SalesOrder.Models
{
	public class SAPExcelStub
	{
		#region Fields
		
		public Harian[] Harians;

		public class Harian
		{

			public string SalesOrganization;
			public string DistributionChanel;
			public string Division;
			public string PricingDate;
			public string OrdetType;
			public string Soldtoparty;
			public string Shiptoparty;
			public string Material;
			public double? TargetOrderQty;
			public string UoM;
			public string SDCurrency;
			public string Plant;
			public string ShippingPoint;
			public string MaterialText;
			public string Requisitioner;
			public string PurchasingGroup;
			public double? PriceinPR;
			public string Currency;
			public string PRReleaseDate;

			public Harian()
			{
				SalesOrganization = "";
				DistributionChanel = "";
				Division = "";
				PricingDate = "";
				OrdetType = "";
				Soldtoparty = "";
				Shiptoparty = "";
				Material = "";
				TargetOrderQty = null;
				UoM = "";
				SDCurrency = "";
				Plant = "";
				ShippingPoint = "";
				MaterialText = "";
				Requisitioner = "";
				PurchasingGroup = "";
				PriceinPR = null;
				Currency = "";
				PRReleaseDate = "";
			}
		}
		
		#endregion

		#region Constructor

		public SAPExcelStub()
		{

		}

		public SAPExcelStub(List<Business.Entities.SalesOrder> monthSalesOrders) // buat ZTS
		{
			Harians = new Harian[31];
			for (int i = 0; i < Harians.Length; i++)
			{
				if (i < monthSalesOrders.Count)
				{
					Business.Entities.SalesOrder curSalesOrder = monthSalesOrders.ElementAt(i);
					if (!Enum.TryParse(curSalesOrder.PowerPlant.Material, out Common.Enums.Material curMaterial))
					{
						curMaterial = Common.Enums.Material.GEOTHERMAL_STEAM; // assign asal aja
					}
					Harians[i] = new Harian()
					{
						SalesOrganization = curSalesOrder.PowerPlant.SalesGroup,
						DistributionChanel = "",
						Division = "",
						PricingDate = curSalesOrder.Periode.Value.ToString("dd.MM.yyyy"),
						OrdetType = curSalesOrder.PowerPlant.SOType.Replace('_', ' '),
						Soldtoparty = curSalesOrder.PowerPlant.SoldToParty.Code,
						Shiptoparty = curSalesOrder.PowerPlant.Code,
						Material = Common.Enums.EnumExtension.ToDescription(curMaterial).Before(" - "),
						TargetOrderQty = curSalesOrder.Quantity,
						UoM = "kWh",
						SDCurrency = curSalesOrder.Currency,
						Plant = "",
						ShippingPoint = curSalesOrder.PowerPlant.ShippingPoint,
						MaterialText = curSalesOrder.PowerPlant.Material,
						Requisitioner = "",
						PurchasingGroup = "",
						PriceinPR = null,
						Currency = "",
						PRReleaseDate = "",
				};
				}
				else
				{
					Harians[i] = new Harian();
				}
			}
		}

		public SAPExcelStub(Business.Entities.SalesOrder salesOrder, List<Business.Entities.TaskReport> taskReports) // buat ZPGE
		{
			Harians = new Harian[31];
            double target = 0;

			for (int i = 0; i < Harians.Length; i++)
			{
                target = 0;
				if (i < taskReports.Count)
				{
					Business.Entities.TaskReport curTaskReport = taskReports.ElementAt(i);
					if (!Enum.TryParse(salesOrder.PowerPlant.Material, out Common.Enums.Material curMaterial))
					{
						curMaterial = Common.Enums.Material.GEOTHERMAL_STEAM; // assign asal aja
					}
                    target += curTaskReport.Pltps.Where(n => n.PowerPlant.Material == Common.Enums.Material.GEOTHERMAL_STEAM.ToString() && n.PowerPlantId == salesOrder.PowerPlantId).Sum(n => n.EnergyGross.Value);
                    target += curTaskReport.Pltps.Where(n => n.PowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString() && n.PowerPlantId == salesOrder.PowerPlantId).Sum(n => n.EnergyNett.Value);
                    Harians[i] = new Harian()
					{
						SalesOrganization = salesOrder.PowerPlant.SalesGroup,
						DistributionChanel = "",
						Division = "",
						PricingDate = curTaskReport.Periode.Value.ToString("dd.MM.yyyy"),
						OrdetType = salesOrder.PowerPlant.SOType,
						Soldtoparty = salesOrder.PowerPlant.SoldToParty.Code,
						Shiptoparty = salesOrder.PowerPlant.Code,
						Material = Common.Enums.EnumExtension.ToDescription(curMaterial).Before(" - "),
						TargetOrderQty = target,
						UoM = curTaskReport.Pltps.FirstOrDefault()?.EnergyNettUnit,
						SDCurrency = salesOrder.Currency,
						Plant = "",
						ShippingPoint = salesOrder.PowerPlant.ShippingPoint,
						MaterialText = salesOrder.PowerPlant.Material,
						Requisitioner = "",
						PurchasingGroup = "",
						PriceinPR = null,
						Currency = "",
						PRReleaseDate = "",
					};
				}
				else
				{
					Harians[i] = new Harian();
				}
			}
		}

		#endregion

	}
}