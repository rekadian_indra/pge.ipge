﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.SalesOrder.Models
{
    public class FormZtsStub
    {
        public List<WorkflowPresentationStub> Workflows;

        [DisplayName("Period")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public Nullable<DateTime> Periode { get; set; }

        [DisplayName("SO Type")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string SOType { get; set; }

        /*
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public List<SalesOrderFormStub> SalesOrders { get; set; }
		*/


        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public List<PowerPlantFormStub> PowerPlants { get; set; }

        public bool IsDraft { get; set; }

        //validasi data
        public bool IsValid { get; set; }

        public List<string> ValidationMessages { get; set; }

        public SalesOrderApproval LastSalesOrderApproval { get; set; }

        public bool IsEditable { get; set; }

        public bool IsRevisable { get; set; }

        public bool IsApprovable { get; set; }

        public bool IsBastDownloadable { get; set; }
        public bool IsSapDownloadable { get; set; }
        public bool IsBastSignedUploadable { get; set; }
        public bool IsBastSignedDownloadable { get; set; }


        #region Constructor

        public FormZtsStub() : base()
        {
            SOType = Common.Enums.SOType.ZTS.ToString();
        }

        public FormZtsStub(List<Business.Entities.Workflow> workflows) : base()
        {
            SOType = Common.Enums.SOType.ZTS.ToString();
            Workflows = ListMapper.MapList<Business.Entities.Workflow, WorkflowPresentationStub>(workflows);
        }
        public void ProcessPowerPlants(List<Business.Entities.PowerPlant> powerPlants, List<PPI> ppis, List<SteamPowerPlantPrice> steamPowerPlantPrices)
        {
            IsValid = true;
            PowerPlants = new List<PowerPlantFormStub>();
            List<string> errors = new List<string>();
            string error = "";

            foreach (Business.Entities.PowerPlant powerPlant in powerPlants)
            {
                if (!PowerPlants.Where(x => x.Id == powerPlant.Id).Any())
                {
                    PowerPlantFormStub curPowerPlant = new PowerPlantFormStub(powerPlant);

                    double netPrice = 0;
                    double? netPriceAdd = 0;
                    if (curPowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString())
                    {
                        DateTime pastPeriod = Periode.Value.AddMonths(-3);
                        byte pastQuarter = (byte)Math.Ceiling((decimal)pastPeriod.Month / 3);
                        List<PPI> curPPIs = ppis.Where(x =>
                            (x.PowerPlantId == curPowerPlant.Id) &&
                            (x.Year == pastPeriod.Year) &&
                            (x.QuarterNum == pastQuarter)
                        ).ToList();
                        if (!curPPIs.Any()) // blm ada harga yg masuk
                        {
                            error = $"Belum terdapat data harga {curPowerPlant.Name} untuk periode {Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
                            errors.Add(error);
                        }
                        else
                        {
                            PPI curPPI = curPPIs.First();
                            if (curPPI.Workflow.Approver != null) // belum finish, ga bisa diambil
                            {
                                error = $"Data harga {curPowerPlant.Name} untuk periode {Periode.Value.ToString(DisplayFormat.ShortDateFormat)} belum disetujui";
                                errors.Add(error);
                            }
                            else
                            {
                                netPrice = curPPI.NextPrice;
                                netPriceAdd = curPPI.NextPriceAdd;
                            }
                        }
                    }
                    else // if (curPowerPlant.Material == Common.Enums.Material.GEOTHERMAL_STEAM.ToString())
                    {
                        //IEnumerable<SteamPowerPlantPrice> curSteamPowerPlantPrices = steamPowerPlantPrices.Where(x =>
                        //	(x.PowerPlantId == curPowerPlant.Id) &&
                        //	(x.Year == Periode.Value.Year)
                        //);

                        IEnumerable<SteamPowerPlantPrice> curSteamPowerPlantPrices = steamPowerPlantPrices.Where(x =>
                            (x.PowerPlantId == curPowerPlant.Id) &&
                            (x.StartDate <= Periode) && (x.EndDate >= Periode)
                        ).OrderByDescending(n => n.Id);


                        if (!curSteamPowerPlantPrices.Any()) // blm ada harga yg masuk
                        {
                            error = $"Belum terdapat data harga {curPowerPlant.Name} untuk periode {Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
                            errors.Add(error);
                        }
                        else
                        {
                            SteamPowerPlantPrice curSteamPowerPlantPrice = curSteamPowerPlantPrices.First();
                            if (curSteamPowerPlantPrice.Workflow.Approver != null) // belum finish, ga bisa diambil
                            {
                                error = $"Data harga {curPowerPlant.Name} untuk periode {Periode.Value.ToString(DisplayFormat.ShortDateFormat)} belum disetujui";
                                errors.Add(error);
                            }
                            else
                            {
                                netPrice = curSteamPowerPlantPrice.Price;
                                netPriceAdd = curSteamPowerPlantPrice.PriceAdd;
                            }
                        }
                    }

                    if (netPrice != 0)
                    {
                        SalesOrderFormStub curSalesOrder = new SalesOrderFormStub()
                        {
                            IsValid = true,
                            Periode = new DateTime(Periode.Value.Year, Periode.Value.Month, DateTime.DaysInMonth(Periode.Value.Year, Periode.Value.Month)),
                        };
                        curSalesOrder.CalculatePeriodeStr();
                        SalesOrderFormStub curSalesOrder2 = new SalesOrderFormStub()
                        {
                            IsValid = false,
                            Periode = new DateTime(Periode.Value.Year, Periode.Value.Month, DateTime.DaysInMonth(Periode.Value.Year, Periode.Value.Month) - 1),
                        };
                        curSalesOrder2.CalculatePeriodeStr();

                        int workflowId = Workflows.FirstOrDefault(x => x.PreviousStep == null).Id;
                        curSalesOrder.Workflows = curSalesOrder2.Workflows = Workflows;
                        curSalesOrder.SetWorkflowId(workflowId);
                        curSalesOrder2.SetWorkflowId(workflowId);

                        curSalesOrder.IsEditable = curSalesOrder2.IsEditable = true;
                        curSalesOrder.IsRevisable = curSalesOrder2.IsRevisable = false;
                        curSalesOrder.IsApprovable = curSalesOrder2.IsApprovable = true;
                        curSalesOrder.ShipToPartyId = curSalesOrder2.ShipToPartyId = curPowerPlant.Id;
                        curSalesOrder.SOType = curSalesOrder2.SOType = curPowerPlant.SOType;
                        curSalesOrder.Currency = curSalesOrder2.Currency = "USDN";
                        curSalesOrder.Quantity = curSalesOrder2.Quantity = 0;
                        curSalesOrder.NetPrice = netPrice;
                        curSalesOrder2.NetPrice = netPriceAdd ?? 0;
                        curSalesOrder.NetValue = curSalesOrder.Quantity * curSalesOrder.NetPrice;
                        curSalesOrder2.NetValue = curSalesOrder2.Quantity * curSalesOrder2.NetPrice;

                        curPowerPlant.SalesOrders.Add(curSalesOrder);
                        curPowerPlant.SalesOrders.Add(curSalesOrder2);

                        PowerPlants.Add(curPowerPlant);
                    }
                }
                else
                {
                    PowerPlantFormStub curPowerPlant = PowerPlants.FirstOrDefault(x => x.Id == powerPlant.Id);

                    foreach (SalesOrderFormStub curSalesOrder in curPowerPlant.SalesOrders)
                    {
                        curSalesOrder.Quantity += 0;
                        curSalesOrder.NetValue = curSalesOrder.Quantity * curSalesOrder.NetPrice;
                    }
                }
            }
            SetValidationMessage(errors);
        }


        public void ProcessTaskReports(List<Business.Entities.TaskReport> taskReports, List<PPI> ppis, List<SteamPowerPlantPrice> steamPowerPlantPrices)
        {
            IsValid = true;
            PowerPlants = new List<PowerPlantFormStub>();
            List<string> errors = new List<string>();
            string error = "";

            foreach (TaskReport taskReport in taskReports)
            {
                foreach (Business.Entities.Pltp pltp in taskReport.Pltps)
                {
                    if (pltp.PowerPlantId.HasValue)
                    {
                        if (pltp.PowerPlant.SOType == SOType)
                        {
                            if (!PowerPlants.Where(x => x.Id == pltp.PowerPlant.Id).Any())
                            {
                                PowerPlantFormStub curPowerPlant = new PowerPlantFormStub(pltp.PowerPlant);

                                double netPrice = 0;
                                if (curPowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString())
                                {
                                    DateTime pastPeriod = Periode.Value.AddMonths(-3);
                                    byte pastQuarter = (byte)Math.Ceiling((decimal)pastPeriod.Month / 3);
                                    List<PPI> curPPIs = ppis.Where(x =>
                                        (x.PowerPlantId == curPowerPlant.Id) &&
                                        (x.Year == pastPeriod.Year) &&
                                        (x.QuarterNum == pastQuarter)
                                    ).ToList();
                                    if (!curPPIs.Any()) // blm ada harga yg masuk
                                    {
                                        error = $"Belum terdapat data harga {curPowerPlant.Name} untuk periode {Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
                                        errors.Add(error);
                                    }
                                    else
                                    {
                                        PPI curPPI = curPPIs.First();
                                        if (curPPI.Workflow.Approver != null) // belum finish, ga bisa diambil
                                        {
                                            error = $"Data harga {curPowerPlant.Name} untuk periode {Periode.Value.ToString(DisplayFormat.ShortDateFormat)} belum disetujui";
                                            errors.Add(error);
                                        }
                                        else
                                        {
                                            netPrice = curPPI.NextPrice;
                                        }
                                    }
                                }
                                else // if (curPowerPlant.Material == Common.Enums.Material.GEOTHERMAL_STEAM.ToString())
                                {
                                    //IEnumerable<SteamPowerPlantPrice> curSteamPowerPlantPrices = steamPowerPlantPrices.Where(x =>
                                    //	(x.PowerPlantId == curPowerPlant.Id) &&
                                    //	(x.Year == Periode.Value.Year)
                                    //);

                                    IEnumerable<SteamPowerPlantPrice> curSteamPowerPlantPrices = steamPowerPlantPrices.Where(x =>
                                           (x.PowerPlantId == curPowerPlant.Id) &&
                                           (x.StartDate <= Periode) && (x.EndDate >= Periode)
                                       ).OrderByDescending(n => n.Id);

                                    if (!curSteamPowerPlantPrices.Any()) // blm ada harga yg masuk
                                    {
                                        error = $"Belum terdapat data harga {curPowerPlant.Name} untuk periode {Periode.Value.ToString(DisplayFormat.MonthYearDateFormat)}";
                                        errors.Add(error);
                                    }
                                    else
                                    {
                                        SteamPowerPlantPrice curSteamPowerPlantPrice = curSteamPowerPlantPrices.First();
                                        if (curSteamPowerPlantPrice.Workflow.Approver != null) // belum finish, ga bisa diambil
                                        {
                                            error = $"Data harga {curPowerPlant.Name} untuk periode {Periode.Value.ToString(DisplayFormat.ShortDateFormat)} belum disetujui";
                                            errors.Add(error);
                                        }
                                        else
                                        {
                                            netPrice = curSteamPowerPlantPrice.Price;
                                        }
                                    }
                                }

                                if (netPrice != 0)
                                {
                                    SalesOrderFormStub curSalesOrder = new SalesOrderFormStub()
                                    {
                                        IsValid = true,
                                        Periode = new DateTime(Periode.Value.Year, Periode.Value.Month, DateTime.DaysInMonth(Periode.Value.Year, Periode.Value.Month)),
                                    };
                                    curSalesOrder.CalculatePeriodeStr();
                                    SalesOrderFormStub curSalesOrder2 = new SalesOrderFormStub()
                                    {
                                        IsValid = false,
                                        Periode = new DateTime(Periode.Value.Year, Periode.Value.Month, DateTime.DaysInMonth(Periode.Value.Year, Periode.Value.Month) - 1),
                                    };
                                    curSalesOrder2.CalculatePeriodeStr();

                                    int workflowId = Workflows.FirstOrDefault(x => x.PreviousStep == null).Id;
                                    curSalesOrder.Workflows = curSalesOrder2.Workflows = Workflows;
                                    curSalesOrder.SetWorkflowId(workflowId);
                                    curSalesOrder2.SetWorkflowId(workflowId);

                                    curSalesOrder.IsEditable = curSalesOrder2.IsEditable = true;
                                    curSalesOrder.IsRevisable = curSalesOrder2.IsRevisable = false;
                                    curSalesOrder.IsApprovable = curSalesOrder2.IsApprovable = true;
                                    curSalesOrder.ShipToPartyId = curSalesOrder2.ShipToPartyId = curPowerPlant.Id;
                                    curSalesOrder.SOType = curSalesOrder2.SOType = curPowerPlant.SOType;
                                    curSalesOrder.Currency = curSalesOrder2.Currency = "USDN";
                                    curSalesOrder.Quantity = curSalesOrder2.Quantity = pltp.EnergyNett.ToFloat();
                                    curSalesOrder.NetPrice = curSalesOrder2.NetPrice = netPrice;
                                    curSalesOrder.NetValue = curSalesOrder2.NetValue = curSalesOrder.Quantity * curSalesOrder.NetPrice;

                                    curPowerPlant.SalesOrders.Add(curSalesOrder);
                                    curPowerPlant.SalesOrders.Add(curSalesOrder2);

                                    PowerPlants.Add(curPowerPlant);
                                }
                            }
                            else
                            {
                                PowerPlantFormStub curPowerPlant = PowerPlants.FirstOrDefault(x => x.Id == pltp.PowerPlant.Id);

                                foreach (SalesOrderFormStub curSalesOrder in curPowerPlant.SalesOrders)
                                {
                                    curSalesOrder.Quantity += pltp.EnergyNett.ToFloat();
                                    curSalesOrder.NetValue = curSalesOrder.Quantity * curSalesOrder.NetPrice;
                                }
                            }
                        }
                    }
                }
            }
            SetValidationMessage(errors);
        }

        public void ProcessSalesOrders(List<Business.Entities.SalesOrder> salesOrders, List<PPI> ppis, List<SteamPowerPlantPrice> steamPowerPlantPrices)
        {
            if (salesOrders.Count > 0)
            {
                Periode = salesOrders.ElementAt(0).Periode;
                IsEditable = (salesOrders.ElementAt(0).WorkflowId != null) ? Workflows.Where(x => (x.Id == salesOrders.ElementAt(0).WorkflowId) && (x.PreviousStep == null)).Any() : true;
                IsApprovable = (salesOrders.ElementAt(0).WorkflowId != null) ? Workflows.Where(x => x.PreviousStep == salesOrders.ElementAt(0).WorkflowId).Any() : true;
                IsRevisable = !IsEditable && IsApprovable;

                IsBastDownloadable = (salesOrders.ElementAt(0).WorkflowId != null) ? Workflows.Where(x => (x.Id == salesOrders.ElementAt(0).WorkflowId) && x.IsBastDownloadable).Any() : false;
                IsSapDownloadable = (salesOrders.ElementAt(0).WorkflowId != null) ? Workflows.Where(x => (x.Id == salesOrders.ElementAt(0).WorkflowId) && x.IsSapDownloadable).Any() : false;
                IsBastSignedUploadable = (salesOrders.ElementAt(0).WorkflowId != null) ? Workflows.Where(x => (x.Id == salesOrders.ElementAt(0).WorkflowId) && x.IsBastSignedUploadable).Any() : false;
                IsBastSignedDownloadable = (salesOrders.ElementAt(0).WorkflowId != null) ? Workflows.Where(x => (x.Id == salesOrders.ElementAt(0).WorkflowId) && x.IsBastSignedDownloadable).Any() : false;

                IsValid = true;
            }

            PowerPlants = new List<PowerPlantFormStub>();
            foreach (Business.Entities.SalesOrder salesOrder in salesOrders)
            {
                SalesOrderFormStub newSalesOrder = new SalesOrderFormStub(salesOrder)
                {
                    IsValid = true,
                };
                newSalesOrder.CalculatePeriodeStr();
                newSalesOrder.Workflows = Workflows;
                if (salesOrder.WorkflowId.HasValue) newSalesOrder.SetWorkflowId(salesOrder.WorkflowId.Value);

                if (salesOrder.BastFiles != null)
                {
                    newSalesOrder.BastFiles = new List<BastFileFormStub>();
                    foreach (BastFile bastFile in salesOrder.BastFiles)
                    {
                        newSalesOrder.BastFiles.Add(new BastFileFormStub(bastFile));
                    }
                }

                if (!PowerPlants.Where(x => x.Id == salesOrder.PowerPlant.Id).Any())
                {
                    PowerPlantFormStub curPowerPlant = new PowerPlantFormStub(salesOrder.PowerPlant);

                    curPowerPlant.SalesOrders.Add(newSalesOrder);

                    PowerPlants.Add(curPowerPlant);
                }
                else
                {
                    PowerPlantFormStub curPowerPlant = PowerPlants.FirstOrDefault(x => x.Id == salesOrder.PowerPlant.Id);

                    curPowerPlant.IsAdditionalSODetail = true;
                    curPowerPlant.SalesOrders.Add(newSalesOrder);
                }
            }
            foreach (PowerPlantFormStub powerPlant in PowerPlants)
            {
                if (powerPlant.SalesOrders.Count < 2)
                {
                    if (powerPlant.SalesOrders.Count > 0)
                    {
                        // cari dulu harga additional nya
                        double? netPriceAdd = 0;
                        if (powerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString())
                        {
                            DateTime pastPeriod = Periode.Value.AddMonths(-3);
                            byte pastQuarter = (byte)Math.Ceiling((decimal)pastPeriod.Month / 3);
                            List<PPI> curPPIs = ppis.Where(x =>
                                (x.PowerPlantId == powerPlant.Id) &&
                                (x.Year == pastPeriod.Year) &&
                                (x.QuarterNum == pastQuarter)
                            ).ToList();
                            PPI curPPI = curPPIs.First();
                            netPriceAdd = curPPI.NextPriceAdd;
                        }
                        else // if (curPowerPlant.Material == Common.Enums.Material.GEOTHERMAL_STEAM.ToString())
                        {
                            //IEnumerable<SteamPowerPlantPrice> curSteamPowerPlantPrices = steamPowerPlantPrices.Where(x =>
                            //    (x.PowerPlantId == powerPlant.Id) &&
                            //    (x.Year == Periode.Value.Year)
                            //);

                            IEnumerable<SteamPowerPlantPrice> curSteamPowerPlantPrices = steamPowerPlantPrices.Where(x =>
                               (x.PowerPlantId == powerPlant.Id) &&
                               (x.StartDate <= Periode) && (x.EndDate >= Periode)
                           );
                            SteamPowerPlantPrice curSteamPowerPlantPrice = curSteamPowerPlantPrices != null && curSteamPowerPlantPrices.Any() ? curSteamPowerPlantPrices.First() : null;
                            netPriceAdd = curSteamPowerPlantPrice != null ? curSteamPowerPlantPrice.PriceAdd : 0;
                        }

                        // baru masukin salesOrder additionalnya
                        Business.Entities.SalesOrder curSalesOrderDb = new Business.Entities.SalesOrder();
                        powerPlant.SalesOrders.ElementAt(0).MapDbObject(curSalesOrderDb);
                        SalesOrderFormStub curSalesOrder = new SalesOrderFormStub(curSalesOrderDb)
                        {
                            Quantity = 0,
                            NetPrice = netPriceAdd ?? 0,
                            NetValue = 0,
                            Periode = curSalesOrderDb.Periode.Value.AddDays(-1),
                            Id = 0,
                            IsValid = false
                        };
                        curSalesOrder.CalculatePeriodeStr();
                        powerPlant.SalesOrders.Add(curSalesOrder);
                    }
                }
            }
        }

        #endregion

        public void SetWorkflow(List<Business.Entities.Workflow> workflows)
        {
            Workflows = ListMapper.MapList<Business.Entities.Workflow, WorkflowPresentationStub>(workflows);
        }

        public bool SetActiveWorkflow()
        {
            if (PowerPlants != null)
            {
                int workflowId = PowerPlants.ElementAt(0).SalesOrders.ElementAt(0).WorkflowId.Value;
                foreach (WorkflowPresentationStub workflow in Workflows)
                {
                    if (workflow.Id == workflowId)
                    {
                        workflow.Active = true;
                    }
                    else
                    {
                        workflow.Active = false;
                    }
                }

                IsBastDownloadable = Workflows.Where(x => (x.Id == workflowId) && x.IsBastDownloadable).Any();
                IsSapDownloadable = Workflows.Where(x => (x.Id == workflowId) && x.IsSapDownloadable).Any();
                IsBastSignedUploadable = Workflows.Where(x => (x.Id == workflowId) && x.IsBastSignedUploadable).Any();
                IsBastSignedDownloadable = Workflows.Where(x => (x.Id == workflowId) && x.IsBastSignedDownloadable).Any();

                return true;
            }
            return false;
        }

        public void SetValidationMessage(List<string> messages)
        {
            IsValid = false;
            ValidationMessages = messages;
        }

    }
}