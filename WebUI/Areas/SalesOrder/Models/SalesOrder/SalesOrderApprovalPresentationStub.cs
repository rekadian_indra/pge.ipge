﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.SalesOrder.Models
{
    public class SalesOrderPresentationStub : BasePresentationStub<Business.Entities.SalesOrder, SalesOrderPresentationStub>
    {
		#region "Properties"
		public int Id { get; set; }
		public string SOType { get; set; }
		public DateTime PeriodeValue { get; set; }
		public string Periode { get; set; }
		public string PeriodeDate { get; set; }
		public string Quantity { get; set; }
		public string NetPrice { get; set; }
		public string NetValue { get; set; }
        public double QuantitySO { get; set; }
        public double NetPriceSO { get; set; }
        public double NetValueSO { get; set; }
        public string Unit { get; set; }
		public string Status { get; set; }
		public Nullable<int> WorkflowId { get; set; }
		public bool IsRevisable { get; set; }
		public bool IsApprovable { get; set; }

		public PowerPlantPresentationStub PowerPlant { get; set; }

		[DisplayName("File BAST Ttd")]
		public List<BastFileSignedFormStub> BastFileSigneds { get; set; }

        public string PowerPlantName { get; set; }
        public string NoPo { get; set; }
        public string NoPr { get; set; }
        public string NoSo { get; set; }
        public int PowerPlantId { get; set; }
        public double TransmitionPrice { get; set; }
        public double TransmitionNetValue { get; set; }
        #endregion

        #region "Contructor"
        public SalesOrderPresentationStub() : base()
        {

        }

        public SalesOrderPresentationStub(Business.Entities.SalesOrder dbObject) : base(dbObject)
        {
            SOType = ((SOType)Enum.Parse(typeof(SOType), dbObject.SOType)).ToDescription();
			PeriodeValue = dbObject.Periode.Value;
			Periode = dbObject.Periode.Value.ToString(DateTimeFormat.MonthYear);
			PeriodeDate = dbObject.Periode.Value.ToString(DisplayFormat.FullDateFormat);
			Unit = dbObject.PowerPlant.ShipToPartyName;
			Status = dbObject.Workflow.Name;
			IsRevisable = dbObject.Workflow.PreviousStep == null;
			Quantity = DisplayFormat.NumberFormatThreeDecimal(dbObject.Quantity);
			NetPrice = DisplayFormat.NumberFormatFiveDecimal(dbObject.NetPrice);
			NetValue = DisplayFormat.NumberFormatFiveDecimal(dbObject.NetValue);
            QuantitySO = dbObject.Quantity.Value;
            NetPriceSO = dbObject.NetPrice.Value;
            NetValueSO = dbObject.NetValue.Value;
            PowerPlantName = dbObject.PowerPlant.ShipToPartyName;
            TransmitionPrice = dbObject.PowerPlant.TransmitionPrice ?? 0;
            TransmitionNetValue = TransmitionPrice * dbObject.Quantity.Value;
		}

        #endregion

        #region "Helper"
        protected override void Init()
        {
            //throw new NotImplementedException();
        }
        #endregion

    }
}