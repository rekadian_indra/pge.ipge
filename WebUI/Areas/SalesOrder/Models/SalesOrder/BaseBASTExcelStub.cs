﻿using Business.Infrastructure;
using NPOI.XSSF.UserModel;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.SalesOrder.Models
{
	public abstract class BaseBASTExcelStub
	{
		#region Fields

		// kamojang, karaha, lahendong, ulubelu
		public string Title;
		public string CompanyName;
		public string SoldToPartyName;
		public string Periode;
		public string SignaturePlace;
		public string SignatureDate;
		public string SignaturePlaceDate;
		public string SellerName;
		public string SellerPosition;
		public string BuyerName;
		public string BuyerPosition;

		public string UnitNumber;

		#endregion

		#region Constructor

		public BaseBASTExcelStub()
		{
			Title = "";
			CompanyName = "";
			SoldToPartyName = "";
			Periode = "";

			SignaturePlace = "";
			SignatureDate = "";
			SignaturePlaceDate = "";

			SellerName = "";
			SellerPosition = "";
			BuyerName = "";
			BuyerPosition = "";

			UnitNumber = "";
		}

		public BaseBASTExcelStub(Business.Entities.SalesOrder salesOrder)
		{
			// kamojang, karaha, lahendong, ulubelu
			Title				= "PLTP " + salesOrder.PowerPlant.ShipToPartyName;
			CompanyName			= salesOrder.PowerPlant.Area.Company.Name;
			SoldToPartyName		= salesOrder.PowerPlant.SoldToParty.Name;
			Periode				= salesOrder.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat);

			SignaturePlace		= salesOrder.PowerPlant.Area.Name;
			SignatureDate		= DateTime.Now.ToString(DisplayFormat.MonthYearDateFormat);
			SignaturePlaceDate	= SignaturePlace + ",       " + SignatureDate;
			SellerName = SellerPosition = BuyerName = BuyerPosition = "";

			string powerPlantName = salesOrder.PowerPlant.ShipToPartyName.Trim();
			if (powerPlantName.LastIndexOf(' ') < powerPlantName.Length + 1)
			{
				UnitNumber = powerPlantName;//.Substring(powerPlantName.LastIndexOf(' ') + 1);
			}
		}

		#endregion

	}
}