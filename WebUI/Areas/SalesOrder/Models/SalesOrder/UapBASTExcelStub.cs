﻿using Business.Infrastructure;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.SalesOrder.Models
{
	public class UapBASTExcelStub : BaseBASTExcelStub
	{
		#region Fields

		private string SteamRecapTime = System.Configuration.ConfigurationManager.AppSettings["SteamRecapTime"].ToString();

		// kamojang, lahendong, ulubelu
		public string NamaMeter;
		public string TipeMeter;
		public string PeriodeMulai;
		public string PeriodeSampai;
		public double KwhDariPembangkit;
		public double KwhKePembangkit;
		public string KvarhDariPembangkit;
		public string KvarhKePembangkit;
		public string Catatan;

		// rekapitulasi bulanan
		public Harian[] Harians;
		public double SumJPH;
		public double SumPembangkitanPerHari;
		public double SumUapTersalurkan;

		public class Harian
		{
			public DateTime? Tanggal;
			public string Waktu;

			public double? JPH;
			public double? KumulatifPembangkitan;
			public double? PembangkitanPerHari;
			public double? UapTersalurkan;
			public double? PotensiTOP;
			public double? PotensiDop;

			public string Keterangan;
			public string Paraf1;
			public string Paraf2;

			public Harian()
			{
				Tanggal = null;
				Waktu = "";

				JPH = null;
				KumulatifPembangkitan = null;
				PembangkitanPerHari = null;
				UapTersalurkan = null;
				PotensiTOP = null;
				PotensiDop = null;

				Keterangan = "";
				Paraf1 = "";
				Paraf2 = "";
			}
		}

		#endregion

		#region Constructor

		public UapBASTExcelStub() : base()
		{

		}

		public UapBASTExcelStub(Business.Entities.SalesOrder salesOrder, List<Business.Entities.TaskReport> allTaskReports = null) : base(salesOrder)
		{
			int curYear = salesOrder.Periode.Value.Year;
			int curMonth = salesOrder.Periode.Value.Month;

			// kamojang, lahendong, ulubelu
			NamaMeter = salesOrder.PowerPlant.MeterName;
			TipeMeter = salesOrder.PowerPlant.MeterType;
			PeriodeMulai = $"{(new DateTime(curYear, curMonth, 1)).ToString(DisplayFormat.FullDateFormat)} {SteamRecapTime}"; // cenah antara 1 jun 18 10:00 - 1 jul 18 10:00
			PeriodeSampai = $"{(new DateTime(curYear, curMonth, DateTime.DaysInMonth(curYear, curMonth))).ToString(DisplayFormat.FullDateFormat)} {SteamRecapTime}"; // atau 1 jun 18 00:00 - 30 jun 18 24:00

			KwhDariPembangkit = salesOrder.PowerPlant.Pltps.Sum(x => x.EnergyGross ?? 0); ; // katanya yg buat transaksi itu gross, oh kalau uap gross
			KwhKePembangkit = salesOrder.PowerPlant.Pltps.Sum(x => x.ImportEnergy ?? 0); // energi impor, tapi dari sisi pembangkit (PLN), gitu cenah tapi gatau gini bukan
			KvarhDariPembangkit = ""; // N/A cenah
			KvarhKePembangkit = ""; // N/A cenah

			Catatan = salesOrder.Notes;

			SumJPH = 0;
			SumPembangkitanPerHari = 0;
			SumUapTersalurkan = 0;

			Harians = new Harian[31];
			int jumlahHari = DateTime.DaysInMonth(salesOrder.Periode.Value.Year, salesOrder.Periode.Value.Month);
			for (int i = 0; i < Harians.Length; i++)
			{
				if (i < jumlahHari)
				{
					DateTime tanggal = new DateTime(salesOrder.Periode.Value.Year, salesOrder.Periode.Value.Month, i + 1);
					Harians[i] = new Harian()
					{
						Tanggal = tanggal,
						Waktu = SteamRecapTime,

						JPH = salesOrder.PowerPlant.URC * 24, // URC * 24
						KumulatifPembangkitan = 0, // katanya lahendong dikosongkan, kamojang / ulubelu blm tau. allTaskReports.Where(x => x.Periode.Value < tanggal)?.SelectMany(x => x.Pltps).Where(x => x.PowerPlantId == salesOrder.PowerPlantId).Sum(pltp => pltp.EnergyNett ?? 0),
						PembangkitanPerHari = allTaskReports.Where(x => (x.Periode.Value.Year == salesOrder.Periode.Value.Year) && (x.Periode.Value.Month == salesOrder.Periode.Value.Month) && (x.Periode.Value.Day == i + 1))?.SelectMany(x => x.Pltps)?.Where(x => x.PowerPlantId == salesOrder.PowerPlantId)?.Sum(pltp => pltp.EnergyGross ?? 0) ?? 0, // katanya yg buat transaksi itu gross
						UapTersalurkan = allTaskReports.Where(x => (x.Periode.Value.Year == salesOrder.Periode.Value.Year) && (x.Periode.Value.Month == salesOrder.Periode.Value.Month) && (x.Periode.Value.Day == salesOrder.Periode.Value.Day))?.SelectMany(x => x.Pltps)?.Where(x => x.PowerPlantId == salesOrder.PowerPlantId)?.Sum(pltp => pltp.SteamUtilization ?? 0) ?? 0,
						PotensiTOP = 0,
						PotensiDop = 0,

						Keterangan = "",
						Paraf1 = "",
						Paraf2 = ""
					};

					SumJPH += Harians[i].JPH ?? 0;
					SumPembangkitanPerHari += Harians[i].PembangkitanPerHari ?? 0;
					SumUapTersalurkan += Harians[i].UapTersalurkan ?? 0;
				}
				else
				{
					Harians[i] = new Harian();
				}
			}
		}

		#endregion

	}
}