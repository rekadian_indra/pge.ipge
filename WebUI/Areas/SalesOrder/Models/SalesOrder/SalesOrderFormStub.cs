﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.SalesOrder.Models
{
    public class SalesOrderFormStub : BaseFormStub<Business.Entities.SalesOrder, SalesOrderFormStub>
    {
        public List<WorkflowPresentationStub> Workflows;

		public int Id { get; set; }

        [DisplayName("Period")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public Nullable<DateTime> Periode { get; set; }
		public string PeriodeStr { get; set; }

		[DisplayName("SO Type")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string SOType { get; set; }

        [DisplayName("Ship To Party")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public Nullable<int> ShipToPartyId { get; set; }

		[DisplayName("No PO")]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string NoPo { get; set; }

		[DisplayName("No PR")]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string NoPr { get; set; }

		[DisplayName("No SO")]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string NoSo { get; set; }

		public PowerPlantPresentationStub PowerPlant { get; set; }
		public List<Pltp> Pltps { get; set; }

        //validasi data
        public bool IsValid { get; set; }

        public Nullable<double> Quantity { get; set; }

        public Nullable<double> NetPrice { get; set; }

        public Nullable<double> NetValue { get; set; }

        public Nullable<double> EnergyPrice { get; set; }

        public Nullable<double> SteamPrice { get; set; }

        public string Currency { get; set; }
        
        [DisplayName("Notes")]
        [StringLength(500, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Notes { get; set; }

        [DisplayName("File BAST")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public List<BastFileFormStub> BastFiles { get; set; }

        [DisplayName("Screenshot SAP")]        
        public List<ScreenshotSapFormStub> ScreenshotSaps { get; set; }

        [DisplayName("File BAST Ttd")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public List<BastFileSignedFormStub> BastFileSigneds { get; set; }

		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public Nullable<int> WorkflowId { get; set; }

        public List<string> ValidationMessages { get; set; }

		public SalesOrderApproval LastSalesOrderApproval { get; set; }

		public bool IsEditable { get; set; }

		public bool IsRevisable { get; set; }

		public bool IsApprovable { get; set; }
        public double TransmitionPrice { get; set; }
        public double TransmitionNetValue { get;set; }

		public SalesOrderFormStub() : base()
        {
			//BastFiles = new List<BastFileFormStub>();
			Notes = "";
		}

		public SalesOrderFormStub(Business.Entities.SalesOrder dbObject) : base(dbObject)
		{
			ShipToPartyId = dbObject.PowerPlantId;
			CalculatePeriodeStr();
		}

		public SalesOrderFormStub(List<Workflow> workflows) : base()
        {
            Workflows = ListMapper.MapList<Workflow, WorkflowPresentationStub>(workflows);
			//BastFiles = new List<BastFileFormStub>();
		}

        public SalesOrderFormStub(Business.Entities.SalesOrder dbObject, List<Workflow> workflows) : base(dbObject)
        {
			ShipToPartyId = dbObject.PowerPlantId;

			Workflows = ListMapper.MapList<Workflow, WorkflowPresentationStub>(workflows);
			if (dbObject.WorkflowId != null) SetWorkflowId(dbObject.WorkflowId.Value);

			if (dbObject.BastFiles != null)
			{
				BastFiles = new List<BastFileFormStub>();
				foreach (BastFile bastFile in dbObject.BastFiles)
				{
					BastFiles.Add(new BastFileFormStub(bastFile));
				}
			}

			if (dbObject.BastFileSigneds != null)
			{
				BastFileSigneds = new List<BastFileSignedFormStub>();
				foreach (BastFileSigned bastFileSigned in dbObject.BastFileSigneds)
				{
					BastFileSigneds.Add(new BastFileSignedFormStub(bastFileSigned));
				}
			}

            if (dbObject.ScreenshotSaps != null)
            {
                ScreenshotSaps = new List<ScreenshotSapFormStub>();
                foreach (ScreenshotSap screenshotSap in dbObject.ScreenshotSaps)
                {
                    ScreenshotSaps.Add(new ScreenshotSapFormStub(screenshotSap));
                }
            } 

            IsEditable = (dbObject.WorkflowId != null) ? Workflows.Where(x => (x.Id == dbObject.WorkflowId) && (x.PreviousStep == null)).Any() : true;
			IsApprovable = (dbObject.WorkflowId != null) ? Workflows.Where(x => x.PreviousStep == dbObject.WorkflowId).Any() : true;
			IsRevisable = !IsEditable && IsApprovable;
			IsValid = true;
            TransmitionPrice = dbObject.PowerPlant.TransmitionPrice ?? 0;
            TransmitionNetValue = TransmitionPrice * dbObject.Quantity.Value;
        }

        public override void MapDbObject(Business.Entities.SalesOrder dbObject)
        {
            base.MapDbObject(dbObject);

			dbObject.PowerPlantId = ShipToPartyId;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor      
            Periode = DateTime.Now.AddMonths(-1);
            IsValid = false;
        }

        public void SetValidationMessage(List<string> messages)
        {
            IsValid = false;
            ValidationMessages = messages;
        }

        public void SetPowerPlant(PowerPlant powerPlant)
        {
            PowerPlant = new PowerPlantPresentationStub(powerPlant);
        }

        public void SetWorkflow(List<Workflow> workflows)
        {
            Workflows = ListMapper.MapList<Workflow, WorkflowPresentationStub>(workflows);
        }

		public void SetWorkflowId(int workflowId)
		{
			WorkflowId = workflowId;
			foreach (WorkflowPresentationStub workflow in Workflows)
			{
				if (workflow.Id == workflowId)
				{
					workflow.Active = true;
				}
				else
				{
					workflow.Active = false;
				}
			}
		}

		public bool ProcessPltps(List<Pltp> pltps)
		{            
			if (pltps.Any())
			{
				SetPowerPlant(pltps.First().PowerPlant);
				if (PowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString())
				{
					Quantity = pltps.Sum(x => x.EnergyNett);                    
				}
				else // if (PowerPlant.Material == Common.Enums.Material.GEOTHERMAL_STEAM.ToString())
				{
					Quantity = pltps.Sum(x => x.EnergyGross);
				}
				return true;
			}
			return false;
		}

        public bool ProcessPltps(List<Pltp> pltps, double price)
        {
            double netValue = 0;
            if (pltps.Any())
            {
                SetPowerPlant(pltps.First().PowerPlant);
                if (PowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString())
                {
                    netValue = pltps.Sum(x => (x.EnergyGross * x.EnergyPrice)) ?? 0;
                    Quantity = pltps.Sum(x => x.EnergyNett);
                    NetPrice = price;
                    NetValue = netValue;
                }
                else // if (PowerPlant.Material == Common.Enums.Material.GEOTHERMAL_STEAM.ToString())
                {
                    netValue = pltps.Sum(x => (x.EnergyGross * x.SteamPrice)) ?? 0;
                    Quantity = pltps.Sum(x => x.EnergyGross);
                    SteamPrice = price;
                    NetValue = netValue;
                }

                NetPrice = netValue / Quantity;
                
                return true;
            }
            return false;
        }

        public void CalculatePeriodeStr()
		{
			PeriodeStr = Periode.Value.ToString(DateTimeFormat.LocalDateFormat);
		}

	}
}