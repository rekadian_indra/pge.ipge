﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Web;
using System.Linq;
using WebUI.Extension;
using Resources;
using WebUI.Areas.OperationData.Models;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Business.Entities.Views;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.OperationData.Controllers
{
    [CheckSessionTimeOut]
    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.OPERATION_ADMIN })]
    public class ReliabilityPlanController : BaseController<ReliabilityPlanStub>
    {
		const string TEMPDATA_AREAID = "areaId";
		const string TEMPDATA_POWERPLANTID = "powerPlantId";
		const string TEMPDATA_YEAR = "year";

		public ReliabilityPlanController
		(
            ILogRepository repoLog,
			IAreaRepository repoArea,
			IReliabilityPlanRepository repoReliabilityPlan,
			IPowerPlantRepository repoPowerPlant,
			IViewReliabilityPlanYearlyRepository repoViewReliabilityPlanYearly
		)
        {
            RepoLog = repoLog;
            RepoArea = repoArea;
			RepoReliabilityPlan = repoReliabilityPlan;
			RepoPowerPlant = repoPowerPlant;
			RepoViewReliabilityPlanYearly = repoViewReliabilityPlanYearly;
		}
        #region View CRUD
		[MvcSiteMapNode(Title = TitleSite.ReliabilityPlan, Area = AreaSite.OperationData, ParentKey = KeySite.Dashboard, Key = KeySite.IndexReliabilityPlan)]
		public override async Task<ActionResult> Index()
		{
			ViewBag.powerPlantId = TempData[TEMPDATA_POWERPLANTID] ?? 0;
            
			if (ViewBag.powerPlantId != 0)
			{
				Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync((int)ViewBag.powerPlantId);

				Task.WaitAll(powerPlantTask);
				Business.Entities.PowerPlant powerPlant = powerPlantTask.Result;

				ViewBag.areaId = powerPlant.AreaId;
			}

			ViewBag.year = TempData[TEMPDATA_YEAR] ?? 0;
			return await base.Index();
		}

		public ActionResult BackToIndex(int powerPlantId, int year)
		{
			TempData.Add(TEMPDATA_POWERPLANTID, powerPlantId);
			TempData.Add(TEMPDATA_YEAR, year);

			return RedirectToAction(ActionSite.Index);
		}

		public ActionResult Check(int powerPlantId, int year)
		{
			List<ReliabilityPlan> reliabilityPlans = RepoReliabilityPlan.GetFromPowerPlantId(powerPlantId, year);

			if (reliabilityPlans.Any())
			{
				ReliabilityPlan lastReliabilityPlan = reliabilityPlans.First();
				return RedirectToAction(ActionSite.Edit, new
				{
					powerPlantId = lastReliabilityPlan.PowerPlantId,
					year = lastReliabilityPlan.Periode.Year
				});
			}
			else
			{
				TempData.Add(TEMPDATA_POWERPLANTID, powerPlantId);
				TempData.Add(TEMPDATA_YEAR, year);
				return RedirectToAction(ActionSite.Create);
			}
		}
		
        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexReliabilityPlan, Key = KeySite.CreateReliabilityPlan)]
        public override async Task<ActionResult> Create()
		{
			int areaId = 0;
			int? powerPlantId = (int?)TempData[TEMPDATA_POWERPLANTID] ?? RepoPowerPlant.Find()?.Id;
			int? year = (int?)TempData[TEMPDATA_YEAR] ?? DateTime.Now.Year;
			
			if (powerPlantId.HasValue)
			{
				areaId = RepoPowerPlant.FindByPrimaryKey(powerPlantId).AreaId ?? 0;
			}

			ReliabilityPlanStub model = new ReliabilityPlanStub(areaId, powerPlantId ?? 0, year.Value);
			
			await Task.Delay(0);

			return View("Form", model);
		}

        [HttpPost]
        public override async Task<ActionResult> Create(ReliabilityPlanStub model)
		{
			TempData.Add(TEMPDATA_POWERPLANTID, model.PowerPlantId);
			TempData.Add(TEMPDATA_YEAR, model.Year);

			if (ModelState.IsValid)
			{
				Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync(model.PowerPlantId);

				List<ReliabilityPlan> dbObjects = new List<ReliabilityPlan>();                

                model.MapDbObjects(dbObjects);
				
                
                Task reliabilityPlansTask = RepoReliabilityPlan.SaveAllAsync(dbObjects);

				// wait plan & details
				Task.WaitAll(reliabilityPlansTask);
				
				//message
				Business.Entities.PowerPlant powerPlant = powerPlantTask.Result;
				string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
				this.SetMessage($"Plan {powerPlant.Name} Tahun {model.Year}", template);

				return RedirectToAction(ActionSite.Index);
			}
			else
			{
				return await Create();
			}
		}

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexReliabilityPlan, Key = KeySite.EditReliabilityPlan)]
		[RequiredParameter(QueryStringValue.PowerPlant, IncludePost = false)]
		public async Task<ActionResult> Edit(int powerPlantId, int year)
        {
			List<ReliabilityPlan> reliabilityPlans = RepoReliabilityPlan.GetFromPowerPlantId(powerPlantId, year);  

            Business.Entities.PowerPlant powerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(powerPlantId);

			ReliabilityPlanStub model = new ReliabilityPlanStub(powerPlant.AreaId ?? 0, powerPlantId, year, reliabilityPlans);
			
            ViewBag.Breadcrumb = $"Plan {powerPlant.Name} Tahun {year}";

			return View("Form", model);
		}

        [HttpPost]
        public override async Task<ActionResult> Edit(ReliabilityPlanStub model)
		{
			Business.Entities.PowerPlant powerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(model.PowerPlantId);
			if (ModelState.IsValid)
			{
				List<ReliabilityPlan> dbObjects = RepoReliabilityPlan.GetFromPowerPlantId(model.PowerPlantId, model.Year);

				model.MapDbObjects(dbObjects);

				Task reliabilityPlanTask = RepoReliabilityPlan.SaveAllAsync(dbObjects);

				//message
				string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
				this.SetMessage($"Plan {powerPlant.Name} Tahun {model.Year}", template);

				TempData.Add(TEMPDATA_POWERPLANTID, model.PowerPlantId);
				TempData.Add(TEMPDATA_YEAR, model.Year);

				return RedirectToAction(ActionSite.Index);
            }
            else
			{
				ViewBag.Breadcrumb = $"Plan {powerPlant.Name} Tahun {model.Year}";

				return View("Form", model);
			}
		}

        [HttpPost]
		[RequiredParameter(QueryStringValue.PowerPlant)]
		public async Task<ActionResult> DeletePlan(int powerPlantId, int year)
        {
			ResponseModel response = new ResponseModel(true);

			List<ReliabilityPlan> reliabilityPlanDb = RepoReliabilityPlan.GetFromPowerPlantId(powerPlantId, year);
			await RepoReliabilityPlan.DeleteAllAsync(reliabilityPlanDb);

			return Json(response);
		}
		
		#endregion

		#region Binding

		public ActionResult BindingReliabilityPlan()
		{
			QueryRequestParameter param = QueryRequestParameter.Current;
			
			List<ViewReliabilityPlanYearly> dbObjects = RepoViewReliabilityPlanYearly.FindAll(param.Skip, param.Take, param.Sorts, param.Filter);
            int count = dbObjects.Count;

			//List<ReliabilityPlanPresentationStub> reliabilityPlans = ListMapper.MapList<ViewReliabilityPlanYearly, ReliabilityPlanPresentationStub>(dbObjects);
			
			return Json(new { data = dbObjects, total = count } );
		}

		public async Task<string> BindingYear(params object[] args)
		{
			QueryRequestParameter param = QueryRequestParameter.Current;

			//List<ReliabilityPlan> dbObjects;
			int? year = args.ElementAt(0)?.ToInteger();
			List<SelectListItem> years = new List<SelectListItem>()
			{
				new SelectListItem()
				{
					Text = DateTime.Now.Year.ToString(),
					Value = DateTime.Now.Year.ToString()
				},
				new SelectListItem()
				{
					Text = (DateTime.Now.Year + 1).ToString(),
					Value = (DateTime.Now.Year + 1).ToString()
				}
			};
			if (year < DateTime.Now.Year)
			{
				years.Insert(0, new SelectListItem()
				{
					Text = year.ToString(),
					Value = year.ToString()
				});
			}
			if (year > DateTime.Now.Year + 1)
			{
				for (int i = DateTime.Now.Year + 2; i <= year; i++)
				{
					years.Add(new SelectListItem()
					{
						Text = i.ToString(),
						Value = i.ToString()
					});
				}
			}

			await Task.Delay(0);

			return JsonConvert.SerializeObject(years);
		}
        #endregion

		#region Helper

		#endregion
	}
}
