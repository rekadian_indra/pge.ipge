﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Web;
using System.Linq;
using WebUI.Extension;
using Resources;
using WebUI.Areas.OperationData.Models;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.OperationData.Controllers
{
    [CheckSessionTimeOut]
    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.OPERATION_ADMIN })]
    public class InputPlanController : BaseController<InputPlanFormStub>
    {
		const string TEMPDATA_AREAID = "areaId";
		const string TEMPDATA_POWERPLANTID = "powerPlantId";
		const string TEMPDATA_YEAR = "year";

		public InputPlanController
		(
            ILogRepository repoLog,
			IAreaRepository repoArea,
			IProductionPlanRepository repoProductionPlan,
			IProductionPlanDetailRepository repoProductionPlanDetail,
			IProductionPlanDailyRepository repoProductionPlanDaily,
			IPowerPlantRepository repoPowerPlant
		)
        {
            RepoLog = repoLog;
            RepoArea = repoArea;
			RepoProductionPlan = repoProductionPlan;
			RepoProductionPlanDetail = repoProductionPlanDetail;
			RepoProductionPlanDaily = repoProductionPlanDaily;
			RepoPowerPlant = repoPowerPlant;
		}
        #region View CRUD
		[MvcSiteMapNode(Title = TitleSite.InputPlan, Area = AreaSite.OperationData, ParentKey = KeySite.Dashboard, Key = KeySite.IndexInputPlan)]
		public override async Task<ActionResult> Index()
		{
			ViewBag.powerPlantId = TempData[TEMPDATA_POWERPLANTID] ?? 0;
            
			if (ViewBag.powerPlantId != 0)
			{
				Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync((int)ViewBag.powerPlantId);

				Task.WaitAll(powerPlantTask);
				Business.Entities.PowerPlant powerPlant = powerPlantTask.Result;

				ViewBag.areaId = powerPlant.AreaId;
			}

			ViewBag.year = TempData[TEMPDATA_YEAR] ?? 0;
			return await base.Index();
		}

		public ActionResult BackToIndex(int powerPlantId, int year)
		{
			TempData.Add(TEMPDATA_POWERPLANTID, powerPlantId);
			TempData.Add(TEMPDATA_YEAR, year);

			return RedirectToAction(ActionSite.Index);
		}

		public ActionResult Check(int powerPlantId, int year)
		{
			List<ProductionPlan> productionPlans = RepoProductionPlan.GetFromPowerPlantId(powerPlantId, year);

			if (productionPlans.Any())
			{
				ProductionPlan lastProductionPlan = productionPlans.First();
				return RedirectToAction(ActionSite.Edit, new
				{
					powerPlantId = lastProductionPlan.PowerPlantId,
					year = lastProductionPlan.Year
				});
			}
			else
			{
				TempData.Add(TEMPDATA_POWERPLANTID, powerPlantId);
				TempData.Add(TEMPDATA_YEAR, year);
				return RedirectToAction(ActionSite.Create);
			}
		}
		
        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexInputPlan, Key = KeySite.CreateInputPlan)]
        public override async Task<ActionResult> Create()
		{
			int? powerPlantId = (int?)TempData[TEMPDATA_POWERPLANTID];
			int? year = (int?)TempData[TEMPDATA_YEAR];

			InputPlanFormStub model;
			if (powerPlantId.HasValue)
			{
				Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync(powerPlantId);

				if (!year.HasValue)
				{
					List<ProductionPlan> productionPlans = RepoProductionPlan.GetFromPowerPlantId(powerPlantId);
					if (productionPlans.Any())
					{
						ProductionPlan lastProductionPlan = productionPlans.OrderBy(x => x.Year).Last();
						year = lastProductionPlan.Year + 1;
					}
					else
					{
						year = DateTime.Now.Year;
					}
				}
				model = new InputPlanFormStub(powerPlantId.Value, year.Value);

				Task.WaitAll(powerPlantTask);
				Business.Entities.PowerPlant powerPlant = powerPlantTask.Result;
				model.PowerPlant = powerPlant;
			}
			else
			{
				model = new InputPlanFormStub();
			}

			await Task.Delay(0);

			return View("Form", model);
		}

        [HttpPost]
        public override async Task<ActionResult> Create(InputPlanFormStub model)
		{
			TempData.Add(TEMPDATA_POWERPLANTID, model.PowerPlantId);
			TempData.Add(TEMPDATA_YEAR, model.Year);

			if (ModelState.IsValid)
			{
				Task<Business.Entities.PowerPlant> powerPlantTask = RepoPowerPlant.FindByPrimaryKeyAsync(model.PowerPlantId);

				ProductionPlan dbObjectSteam = new ProductionPlan();
				ProductionPlan dbObjectElectric = new ProductionPlan();
                ProductionPlan dbObjectDaysEffective = new ProductionPlan();

                model.MapDbObject(dbObjectSteam, dbObjectElectric, dbObjectDaysEffective);

				// save semua production plan
				Task productionPlanSteamTask = RepoProductionPlan.SaveAsync(dbObjectSteam);
				Task productionPlanElectricTask = RepoProductionPlan.SaveAsync(dbObjectElectric);
                Task productionPlanDaysEffectiveTask = RepoProductionPlan.SaveAsync(dbObjectDaysEffective);

                // save semua production plan detail
                List<ProductionPlanDetail> dbObjectDetails = new List<ProductionPlanDetail>();
				List<ProductionPlanDaily> dbObjectDailies = new List<ProductionPlanDaily>();
				model.ProductionPlanSteam.ProductionPlanDetails.ForEach(x => x.ProductionPlanId = dbObjectSteam.Id);
				model.ProductionPlanElectric.ProductionPlanDetails.ForEach(x => x.ProductionPlanId = dbObjectElectric.Id);
                model.ProductionPlanDaysEffective.ProductionPlanDetails.ForEach(x => x.ProductionPlanId = dbObjectDaysEffective.Id);
                foreach (ProductionPlanDetailFormStub productionPlanDetail in model.ProductionPlanSteam.ProductionPlanDetails)
				{
					ProductionPlanDetail dbObjectDetail = new ProductionPlanDetail();
					productionPlanDetail.MapDbObject(dbObjectDetail);
					dbObjectDetails.Add(dbObjectDetail);
				}
				foreach (ProductionPlanDetailFormStub productionPlanDetail in model.ProductionPlanElectric.ProductionPlanDetails)
				{
					ProductionPlanDetail dbObjectDetail = new ProductionPlanDetail();
					productionPlanDetail.MapDbObject(dbObjectDetail);
					dbObjectDetails.Add(dbObjectDetail);
				}
                foreach (ProductionPlanDetailFormStub productionPlanDetail in model.ProductionPlanDaysEffective.ProductionPlanDetails)
                {
                    ProductionPlanDetail dbObjectDetail = new ProductionPlanDetail();
                    productionPlanDetail.MapDbObject(dbObjectDetail);
                    dbObjectDetails.Add(dbObjectDetail);
                }
                Task productionPlanDetailTask = RepoProductionPlanDetail.SaveAllAsync(dbObjectDetails);

				// wait plan & details
				Task.WaitAll(productionPlanSteamTask, productionPlanElectricTask, productionPlanDaysEffectiveTask, productionPlanDetailTask, powerPlantTask);
				
				// save semua production plan daily
				foreach (ProductionPlanDetailFormStub productionPlanDetail in model.ProductionPlanSteam.ProductionPlanDetails)
				{
					productionPlanDetail.Id = dbObjectDetails.FirstOrDefault(x => (x.ProductionPlanId == dbObjectSteam.Id) && (x.Month == productionPlanDetail.Month))?.Id ?? 0;
					if (productionPlanDetail.Id != 0)
					{
						foreach (ProductionPlanDailyFormStub productionPlanDaily in productionPlanDetail.ProductionPlanDailies)
						{
							ProductionPlanDaily dbObjectDaily = new ProductionPlanDaily();
							productionPlanDaily.MapDbObject(dbObjectDaily);
							dbObjectDaily.ProductionPlanDetailId = productionPlanDetail.Id;
							dbObjectDailies.Add(dbObjectDaily);
						}
					}
				}
				foreach (ProductionPlanDetailFormStub productionPlanDetail in model.ProductionPlanElectric.ProductionPlanDetails)
				{
					productionPlanDetail.Id = dbObjectDetails.FirstOrDefault(x => (x.ProductionPlanId == dbObjectElectric.Id) && (x.Month == productionPlanDetail.Month))?.Id ?? 0;
					if (productionPlanDetail.Id != 0)
					{
						foreach (ProductionPlanDailyFormStub productionPlanDaily in productionPlanDetail.ProductionPlanDailies)
						{
							ProductionPlanDaily dbObjectDaily = new ProductionPlanDaily();
							productionPlanDaily.MapDbObject(dbObjectDaily);
							dbObjectDaily.ProductionPlanDetailId = productionPlanDetail.Id;
							dbObjectDailies.Add(dbObjectDaily);
						}
					}
				}
				Task productionPlanDailyTask = RepoProductionPlanDaily.SaveAllAsync(dbObjectDailies);

				// wait daily
				Task.WaitAll(productionPlanDailyTask);

				//message
				Business.Entities.PowerPlant powerPlant = powerPlantTask.Result;
				string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
				this.SetMessage($"Plan {powerPlant.Name} Tahun {model.Year}", template);

				return RedirectToAction(ActionSite.Index);
			}
			else
			{
				return await Create();
			}
		}

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexInputPlan, Key = KeySite.EditInputPlan)]
		[RequiredParameter(QueryStringValue.PowerPlant, IncludePost = false)]
		public async Task<ActionResult> Edit(int powerPlantId, int year)
        {
			List<ProductionPlan> dbObjects = RepoProductionPlan.GetFromPowerPlantId(powerPlantId, year);  

            Business.Entities.PowerPlant powerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(powerPlantId);

			InputPlanFormStub model = new InputPlanFormStub(dbObjects, powerPlant, year);
            //model.ProductionPlanElectric = new ProductionPlanFormStub();
            //model.ProductionPlanSteam = new ProductionPlanFormStub();
            //model.ProductionPlanDaysEffective = new ProductionPlanFormStub();

   //         foreach (ProductionPlan dbObject in dbObjects)
			//{
			//	if (dbObject.UtilType == ProductionUtilization.ELECTRIC.ToString())
			//	{                    
   //                 model.ProductionPlanElectric.ProductionPlanDetails = ListMapper.MapList<ProductionPlanDetail, ProductionPlanDetailFormStub>(dbObject.ProductionPlanDetails);
   //             }
			//	else if (dbObject.UtilType == ProductionUtilization.STEAM.ToString())
   //             {                    
   //                 model.ProductionPlanSteam.ProductionPlanDetails = ListMapper.MapList<ProductionPlanDetail, ProductionPlanDetailFormStub>(dbObject.ProductionPlanDetails);                    
   //             }
   //             else
   //             {                    
   //                 model.ProductionPlanDaysEffective.ProductionPlanDetails = ListMapper.MapList<ProductionPlanDetail, ProductionPlanDetailFormStub>(dbObject.ProductionPlanDetails);                    
   //             }
			//}

            ViewBag.Breadcrumb = $"Plan {powerPlant.Name} Tahun {year}";

			return View("Form", model);
		}

        [HttpPost]
        public override async Task<ActionResult> Edit(InputPlanFormStub model)
		{
			Business.Entities.PowerPlant powerPlant = await RepoPowerPlant.FindByPrimaryKeyAsync(model.PowerPlantId);
			if (ModelState.IsValid)
			{
				List<Task> productionPlanDetailTasks = new List<Task>();
				List<ProductionPlan> dbObjects = RepoProductionPlan.GetFromPowerPlantId(model.PowerPlantId, model.Year);

				model.MapDbObject(dbObjects);

				Task productionPlanTask = RepoProductionPlan.SaveAllAsync(dbObjects);
				productionPlanDetailTasks.Add(productionPlanTask);

				ProductionPlan daysEffective = dbObjects.FirstOrDefault(x => x.UtilType == ProductionUtilization.DAYSEFFECTIVE.ToString());
				if (daysEffective != null)
				{
					if (daysEffective.ProductionPlanDetails.Count == 0)
					{
						List<ProductionPlanDetail> daysEffectiveDetails = new List<ProductionPlanDetail>();
						foreach (ProductionPlanDetailFormStub productionPlanDetail in model.ProductionPlanDaysEffective.ProductionPlanDetails)
						{
							productionPlanDetail.ProductionPlanId = daysEffective.Id;

							ProductionPlanDetail daysEffectiveDetail = new ProductionPlanDetail();
							productionPlanDetail.MapDbObject(daysEffectiveDetail);

							daysEffectiveDetails.Add(daysEffectiveDetail);
						}
						Task daysEffectiveTask = RepoProductionPlanDetail.SaveAllAsync(daysEffectiveDetails);
						productionPlanDetailTasks.Add(daysEffectiveTask);
					}
				}

				//dbObjects.ForEach(dbObject =>
				//	productionPlanDetailTasks.Add(RepoProductionPlanDetail.SaveAllAsync(dbObject.ProductionPlanDetails))
				//);

				// wait
				Task.WaitAll(productionPlanDetailTasks.ToArray());

				//message
				string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
				this.SetMessage($"Plan {powerPlant.Name} Tahun {model.Year}", template);

				TempData.Add(TEMPDATA_POWERPLANTID, model.PowerPlantId);
				TempData.Add(TEMPDATA_YEAR, model.Year);

				return RedirectToAction(ActionSite.Index);
            }
            else
			{
				ViewBag.Breadcrumb = $"Plan {powerPlant.Name} Tahun {model.Year}";

				return View("Form", model);
			}
		}

        [HttpPost]
		[RequiredParameter(QueryStringValue.PowerPlant)]
		public async Task<ActionResult> DeletePlan(int powerPlantId, int year)
        {
			ResponseModel response = new ResponseModel(true);
			List<ProductionPlan> productionPlanDb = RepoProductionPlan.GetFromPowerPlantId(powerPlantId, year);
			//List<ProductionPlanDetail> productionPlanDetailDbs = productionPlanDb.SelectMany(x => x.ProductionPlanDetails).ToList();
			//List<ProductionPlanDaily> productionPlanDailyDbs = productionPlanDetailDbs.SelectMany(x => x.ProductionPlanDailies).ToList();

			//await RepoProductionPlanDaily.DeleteAllAsync(productionPlanDailyDbs);
			//await RepoProductionPlanDetail.DeleteAllAsync(productionPlanDetailDbs);
			await RepoProductionPlan.DeleteAllAsync(productionPlanDb);

			return Json(response);
		}
		
		#endregion

		#region Binding
		
		public async Task<JsonResult> BindingPlanSummary(params object[] args)
		{
			QueryRequestParameter param = QueryRequestParameter.Current;
			List<InputPlanPresentationStub> inputPlans = new List<InputPlanPresentationStub>();
			List<Business.Entities.PowerPlant> powerPlants = await RepoPowerPlant.FindAllAsync();

			foreach (Business.Entities.PowerPlant powerPlant in powerPlants)
			{
				List<int> years = powerPlant.ProductionPlans.Where(x => !x.IsDeleted).Select(x => x.Year).Distinct().ToList();
				foreach (int year in years)
				{
					inputPlans.Add(new InputPlanPresentationStub(powerPlant, year));
				}
			}

			//algorithm
			int count = inputPlans.Count;
            //inputPlans = inputPlans.OrderByDescending(x => x.Year).Skip(param.Skip).Take(param.Take).ToList();
            //         return Json(new { data = inputPlans, total = count });            
            return Json(inputPlans);
        }

		public ActionResult BindingProductionPlan(int powerPlantId, int year)
		{
			QueryRequestParameter param = QueryRequestParameter.Current;
			
            List<ProductionPlan> cekCount = RepoProductionPlan.GetFromPowerPlantId(powerPlantId, year);

            if (cekCount.Count == 2)
            {
                ProductionPlan dbObjectDaysEffective = new ProductionPlan();
				ProductionPlanFormStub m = new ProductionPlanFormStub()
				{
					Id = 0,
					Year = year,
					PowerPlantId = powerPlantId,
					UtilType = ProductionUtilization.DAYSEFFECTIVE.ToString()
				};
				m.MapDbObject(dbObjectDaysEffective);
				// save semua production plan
				if (RepoProductionPlan.Save(dbObjectDaysEffective))
				{
					List<ProductionPlanDetail> dbObjectDetails = new List<ProductionPlanDetail>();
					for (int i = 1; i < 13; i++)
					{
						ProductionPlanDetail dbObjectDetail = new ProductionPlanDetail();
						ProductionPlanDetailFormStub productionPlanDetail = new ProductionPlanDetailFormStub();
						productionPlanDetail.Id = 0;
						productionPlanDetail.ProductionPlanId = dbObjectDaysEffective.Id;
						productionPlanDetail.Month = i;
						productionPlanDetail.Value = cekCount.SelectMany(x => x.ProductionPlanDetails)?.Where(x => x.Month == i)?.SelectMany(x => x.ProductionPlanDailies)?.Count(x => x.Value > 0) ?? DateTime.DaysInMonth(year, i);
						productionPlanDetail.MapDbObject(dbObjectDetail);
						dbObjectDetails.Add(dbObjectDetail);
					}
					RepoProductionPlanDetail.SaveAll(dbObjectDetails);
				}
            }

			List<ProductionPlan> dbObjects = RepoProductionPlan.GetFromPowerPlantId(powerPlantId, year);

			List<ProductionPlanPresentationStub> productionPlans = ListMapper.MapList<ProductionPlan, ProductionPlanPresentationStub>(dbObjects);
            foreach (ProductionPlan dbObject in dbObjects)
            {                
                if (dbObject.UtilType == ProductionUtilization.DAYSEFFECTIVE.ToString())
                {
                    ProductionPlanPresentationStub productionPlan = productionPlans.First(x => x.UtilType == dbObject.UtilType);
                    productionPlan.ProductionPlanDetailsP = ListMapper.MapList<ProductionPlanDetail, ProductionPlanDetailPresentationStub>(dbObject.ProductionPlanDetails);
                    if (productionPlan.ProductionPlanDetailsP.Count == 0 )
                    {
                        List<ProductionPlanDetail> pd = new List<ProductionPlanDetail>();
                        pd = RepoProductionPlanDetail.FindAllAsync().Result;
                        List<ProductionPlanDetail> pdResult = pd.Where(p => p.ProductionPlanId == dbObject.Id).ToList();
                        productionPlan.ProductionPlanDetailsP = ListMapper.MapList<ProductionPlanDetail, ProductionPlanDetailPresentationStub>(pdResult);
                    }
                    
                }
                else
                {
                    ProductionPlanPresentationStub productionPlan = productionPlans.First(x => x.UtilType == dbObject.UtilType);
                    productionPlan.ProductionPlanDetailsP = ListMapper.MapList<ProductionPlanDetail, ProductionPlanDetailPresentationStub>(dbObject.ProductionPlanDetails);

                }
            }
			
            return Json(productionPlans);
		}

		public async Task<string> BindingYear(params object[] args)
		{
			QueryRequestParameter param = QueryRequestParameter.Current;

			//List<ProductionPlan> dbObjects;
			int? year = args.ElementAt(0)?.ToInteger();
			List<SelectListItem> years = new List<SelectListItem>()
			{
				new SelectListItem()
				{
					Text = DateTime.Now.Year.ToString(),
					Value = DateTime.Now.Year.ToString()
				},
				new SelectListItem()
				{
					Text = (DateTime.Now.Year + 1).ToString(),
					Value = (DateTime.Now.Year + 1).ToString()
				}
			};
			if (year < DateTime.Now.Year)
			{
				years.Insert(0, new SelectListItem()
				{
					Text = year.ToString(),
					Value = year.ToString()
				});
			}
			if (year > DateTime.Now.Year + 1)
			{
				for (int i = DateTime.Now.Year + 2; i <= year; i++)
				{
					years.Add(new SelectListItem()
					{
						Text = i.ToString(),
						Value = i.ToString()
					});
				}
			}

			//int? powerPlantId = args.ElementAt(0)?.ToInteger();
			//if (powerPlantId.HasValue)
			//{
			//	dbObjects = RepoProductionPlan.GetFromPowerPlantId(powerPlantId.Value);
			//	dbObjects.Select(x => x.Year).Distinct().ToList().ForEach(
			//		x => years.Add(
			//			new SelectListItem()
			//			{
			//				Text = x.ToString(),
			//				Value = x.ToString()
			//			}
			//		)
			//	);
			//}

			await Task.Delay(0);

			return JsonConvert.SerializeObject(years);
		}
        #endregion

		#region Helper

		#endregion
	}
}
