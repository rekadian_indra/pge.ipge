﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Web;
using System.Linq;
using WebUI.Extension;
using WebUI.Areas.OperationData.Models;
using Resources;
using WebUI.Areas.DataMaster.Models.Wells;
using Business.Entities.Views;
using System.Configuration;
using System.IO;
using WebUI.Areas.OperationData.Models.InputData;
using System.Net;
using ICSharpCode.SharpZipLib.Zip;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.OperationData.Controllers
{
    [CheckSessionTimeOut]
    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.OPERATION_ADMIN, UserRole.OPERATION_OPERATOR, UserRole.OPERATION_SUPERVISOR })]
    public class InputDataController : BaseController<TaskReportFormStub>
    {
        private const string KEY = "Documents";
        public InputDataController
        (
            ILogRepository repoLog,
            ITaskReportRepository repoTaskReport,
            IAreaRepository repoArea,
            IPowerPlantRepository repoPowerPlant,
            IPltpRepository repoPltp,
            IDkpRepository repoDkp,
            IWellRepository repoWell,
            IMstSeparatorRepository repoMasterSeparator,
            IInterfacePointRepository repoInterfacePoint,
            IScrubberRepository repoScrubber,
            IProductionRepository repoProduction,
            IReInjectionRepository repoReinjection,
            IMonitoringRepository repoMonitoring,
            ISeparatorRepository repoSeparator,
            IInterfaceScrubberRepository repoInterfaceScrubber,
            IActualPlanningRepository repoActualPlanning,
            ISalesOrderRepository repoSalesOrder,
            IViewWellRepository repoViewWell,
            IPpiRepository repoPPI,
            ISteamPowerPlantPriceRepository repoSteamPowerPlantPrice
        )
        {
            RepoLog = repoLog;
            RepoTaskReport = repoTaskReport;
            RepoArea = repoArea;
            RepoPowerPlant = repoPowerPlant;
            RepoPltp = repoPltp;
            RepoDkp = repoDkp;
            RepoWell = repoWell;
            RepoMasterSeparator = repoMasterSeparator;
            RepoInterfacePoint = repoInterfacePoint;
            RepoScrubber = repoScrubber;
            RepoProduction = repoProduction;
            RepoReinjection = repoReinjection;
            RepoMonitoring = repoMonitoring;
            RepoSeparator = repoSeparator;
            RepoInterfaceScrubber = repoInterfaceScrubber;
            RepoActualPlanning = repoActualPlanning;
            RepoSalesOrder = repoSalesOrder;
            RepoViewWell = repoViewWell;
            RepoPPI = repoPPI;
            RepoSteamPowerPlantPrice = repoSteamPowerPlantPrice;
        }


        [MvcSiteMapNode(Title = "Input Data", Key = "InputDataIndex", ParentKey = "Dashboard")]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        //[Route("Detail/{id:int}&&{tab:regex(^(?i)(tab1)$)")]
        [MvcSiteMapNode(Title = "Detail Task Report", Key = "Detail", ParentKey = "InputDataIndex")]
        public async Task<ActionResult> Detail(int? id, string tab)
        {
            //lib
            HttpCookie cookieTaskReport = Request.Cookies[AppSession.TaskReport];
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];

            if (id == null && string.IsNullOrEmpty(tab))
            {
                id = cookieTaskReport.GetExpiredValue<int>();
                tab = cookieTargetTaskDetail.GetExpiredValue<string>();

                return RedirectToAction(ActionSite.Detail, new { id, tab });
            }
            else
            {
                double expiredTime = Settings.GetValue<double>(Settings.AuthCookieExpired);
                DateTime expired = DateTime.UtcNow.AddMinutes(expiredTime);

                cookieTaskReport = new HttpCookie(AppSession.TaskReport)
                {
                    HttpOnly = true,
                    Expires = expired
                };
                cookieTargetTaskDetail = new HttpCookie(AppSession.TargetTaskDetail)
                {
                    HttpOnly = true,
                    Expires = expired
                };

                cookieTaskReport.SetExpiredValue(id);
                cookieTaskReport.EncryptValue();
                Response.Cookies.Set(cookieTaskReport);

                cookieTargetTaskDetail.SetExpiredValue(tab);
                cookieTargetTaskDetail.EncryptValue();
                Response.Cookies.Set(cookieTargetTaskDetail);
            }

            TaskReport taskReport = await RepoTaskReport.FindByPrimaryKeyAsync(id);

            TaskReportFormStub model = new TaskReportFormStub()
            {
                Id = id.Value,
                AreaId = taskReport.AreaId,
                Periode = taskReport.Periode,
                AreaName = taskReport.Area.Name,
                HasApproved = taskReport.Status == DataStatus.APPROVED.ToString() ? true : false
            };

            ViewBag.SelectedTab = tab;

            return View(PageSite.Detail, model);
        }

        #region "Action : Create"
        public override async Task<ActionResult> Create()
        {
            TaskReportFormStub model = new TaskReportFormStub
            {
                SourceInput = InputSource.WEB.ToString(),
                Status = DataStatus.WAITING.ToString()
            };

            await Task.Delay(0);

            return PartialView(PageSite.TaskReport, model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(TaskReportFormStub model)
        {
            //lib
            TaskReport dbObject = null;
            ResponseModel response = new ResponseModel(true);
            FilterQuery filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, model.AreaId);
            filter.AddFilter(Field.Periode, FilterOperator.Equals, model.Periode);
            filter.AddFilter(Field.Status, FilterOperator.Equals, DataStatus.WAITING.ToString());

            if (ModelState.IsValid)
            {
                dbObject = await RepoTaskReport.FindAsync(filter);

                if (dbObject != null)
                {
                    string msg = $"Task report: {dbObject.Area.Name}" +
                                $"\nPeriode: {model.Periode.Value.ToString(DisplayFormat.IdDateFormat)} sudah ada dalam antrian." +
                                $"\nStatus: {DataStatus.WAITING.ToDescription()}";

                    response.SetFail(msg);
                }
                else
                {
                    dbObject = new TaskReport();
                    model.MapDbObject(dbObject);

                    await RepoTaskReport.SaveAsync(dbObject);

                    //set message
                    response.Message = "Data task report berhasil disimpan";
                }
            }
            else
            {
                response.SetFail("Silahkan isi semua inputan yang berbintang (*)");
            }

            return Json(response);
        }

        [MvcSiteMapNode(Title = "Tambah Pltp", Key = "CreatePltp", ParentKey = "Detail", PreservedRouteParameters = "id,tab")]
        public async Task<ActionResult> CreatePltp(int taskId)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            TaskReport taskReport = await RepoTaskReport.FindByPrimaryKeyAsync(taskId);

            PltpFormStub model = new PltpFormStub()
            {
                TaskReportId = taskId,
                AreaId = taskReport.AreaId,
                Periode = taskReport.Periode.Value
            };

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_PLTP.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Pltp, model);
        }

        [HttpPost]
        public async Task<ActionResult> CreatePltp(PltpFormStub model)
        {
            if (ModelState.IsValid)
            {
                List<SortQuery> sortings = new List<SortQuery> { new SortQuery(Field.Id, SortOrder.Descending) };
                FilterQuery filter = new FilterQuery();

                Pltp dbObject = new Pltp();
                model.MapDbObject(dbObject);

                PPI energyPrice = RepoPPI.GetPrice(model.PowerPlantId.Value, model.Periode);

                filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, model.PowerPlantId);
                filter.AddFilter(Field.StartDate, FilterOperator.LessThanOrEquals, model.Periode);
                filter.AddFilter(Field.EndDate, FilterOperator.GreaterThanOrEquals, model.Periode);

                SteamPowerPlantPrice steamPrice = RepoSteamPowerPlantPrice.Find(sortings, filter);

                dbObject.EnergyPrice = energyPrice != null ? energyPrice.NextPrice : 1;
                dbObject.SteamPrice = steamPrice != null ? steamPrice.Price : 1;

                await RepoPltp.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Pltp", AppGlobalMessage.CreateSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_PLTP.ToDescription() });
            }
            else
            {
                return View(PageSite.Pltp, model);
            }
        }

        [MvcSiteMapNode(Title = "Tambah Dkp", Key = "CreateDkp", ParentKey = "Detail", PreservedRouteParameters = "id,tab")]
        public async Task<ActionResult> CreateDkp(int taskId)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            TaskReport taskReport = await RepoTaskReport.FindByPrimaryKeyAsync(taskId);

            DkpFormStub model = new DkpFormStub
            {
                TaskReportId = taskId,
                AreaId = taskReport.AreaId,
                Periode = taskReport.Periode.Value
            };

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_DKP.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Dkp, model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateDkp(DkpFormStub model)
        {
            if (ModelState.IsValid)
            {
                Dkp dbObject = new Dkp();
                model.MapDbObject(dbObject);

                await RepoDkp.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Dkp", AppGlobalMessage.CreateSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_DKP.ToDescription() });
            }
            else
            {
                //return View(PageSite.Dkp, model);
                return View("Form", model);
            }
        }

        [MvcSiteMapNode(Title = "Tambah Produksi", Key = "CreateProduction", ParentKey = "Detail")]
        public async Task<ActionResult> CreateProduction(int taskId)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            TaskReport taskReport = await RepoTaskReport.FindByPrimaryKeyAsync(taskId);

            ProductionFormStub model = new ProductionFormStub
            {
                TaskReportId = taskId,
                AreaId = taskReport.AreaId.Value,
                Periode = taskReport.Periode.Value,
                Status = DrawWellStatus.PRODUCTION.ToString(),
            };


            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_PRODUCTION.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Production, model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateProduction(ProductionFormStub model)
        {
            if (ModelState.IsValid)
            {
                Production dbObject = new Production();
                model.MapDbObject(dbObject);

                await RepoProduction.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Produksi", AppGlobalMessage.CreateSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_PRODUCTION.ToDescription() });
            }
            else
            {
                return View(PageSite.Production, model);
            }
        }

        [MvcSiteMapNode(Title = "Tambah Reinjeksi", Key = "CreateReinjeksi", ParentKey = "Detail")]
        public async Task<ActionResult> CreateReinjection(int taskId)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            TaskReport taskReport = await RepoTaskReport.FindByPrimaryKeyAsync(taskId);

            ReinjectionFormStub model = new ReinjectionFormStub
            {
                TaskReportId = taskId,
                AreaId = taskReport.AreaId.Value,
                Periode = taskReport.Periode.Value,
                Status = DrawWellStatus.INJECTION.ToString(),
            };

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_REINJECTION.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Reinjection, model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateReinjection(ReinjectionFormStub model)
        {
            if (ModelState.IsValid)
            {
                ReInjection dbObject = new ReInjection();
                model.MapDbObject(dbObject);

                await RepoReinjection.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Reinjeksi", AppGlobalMessage.CreateSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_REINJECTION.ToDescription() });
            }
            else
            {
                return View(PageSite.Reinjection, model);
            }
        }

        [MvcSiteMapNode(Title = "Tambah Monitoring", Key = "CreateMonitoring", ParentKey = "Detail")]
        public async Task<ActionResult> CreateMonitoring(int taskId)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            TaskReport taskReport = await RepoTaskReport.FindByPrimaryKeyAsync(taskId);

            MonitoringFormStub model = new MonitoringFormStub
            {
                TaskReportId = taskId,
                AreaId = taskReport.AreaId.Value,
                Periode = taskReport.Periode.Value,
                Status = DrawWellStatus.MONITORING.ToString()
            };

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_MONITORING.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Monitoring, model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateMonitoring(MonitoringFormStub model)
        {
            if (ModelState.IsValid)
            {
                Monitoring dbObject = new Monitoring();
                model.MapDbObject(dbObject);

                await RepoMonitoring.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Monitoring", AppGlobalMessage.CreateSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_MONITORING.ToDescription() });
            }
            else
            {
                return View(PageSite.Monitoring, model);
            }
        }

        [MvcSiteMapNode(Title = "Tambah Separator", Key = "CreateSeparator", ParentKey = "Detail")]
        public async Task<ActionResult> CreateSeparator(int taskId)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            TaskReport taskReport = await RepoTaskReport.FindByPrimaryKeyAsync(taskId);

            SeparatorFormStub model = new SeparatorFormStub
            {
                TaskReportId = taskId,
                AreaId = taskReport.AreaId.Value,
                Periode = taskReport.Periode.Value
            };

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_SEPARATOR.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Separator, model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateSeparator(SeparatorFormStub model)
        {
            if (ModelState.IsValid)
            {
                Separator dbObject = new Separator();
                model.MapDbObject(dbObject);

                await RepoSeparator.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Separator", AppGlobalMessage.CreateSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_SEPARATOR.ToDescription() });
            }
            else
            {
                return View(PageSite.Separator, model);
            }
        }

        [MvcSiteMapNode(Title = "Tambah Intreface Scrubber", Key = "CreateInterfaceScrubber", ParentKey = "Detail")]
        public async Task<ActionResult> CreateInterface(int taskId)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            TaskReport taskReport = await RepoTaskReport.FindByPrimaryKeyAsync(taskId);

            InterfaceScrubberFormStub model = new InterfaceScrubberFormStub
            {
                TaskReportId = taskId,
                AreaId = taskReport.AreaId.Value,
                Periode = taskReport.Periode.Value
            };

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_INTERFACE_SCRUBBER.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.InterfaceScrubber, model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateInterface(InterfaceScrubberFormStub model)
        {
            if (ModelState.IsValid)
            {
                InterfaceScrubber dbObject = new InterfaceScrubber();
                model.MapDbObject(dbObject);

                await RepoInterfaceScrubber.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Interface Scrubber", AppGlobalMessage.CreateSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_INTERFACE_SCRUBBER.ToDescription() });
            }
            else
            {
                return View(PageSite.InterfaceScrubber, model);
            }
        }

        [MvcSiteMapNode(Title = "Tambah Rencana Realisasi", Key = "CreateActualPlan", ParentKey = "Detail")]
        public async Task<ActionResult> CreateActualPlan(int taskId)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            TaskReport taskReport = await RepoTaskReport.FindByPrimaryKeyAsync(taskId);

            ActualPlanFormStub model = new ActualPlanFormStub
            {
                TaskReportId = taskId,
                AreaId = taskReport.AreaId.Value
            };

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_ACTUAL_PLAN.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.ActualPlan, model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateActualPlan(ActualPlanFormStub model)
        {
            if (ModelState.IsValid)
            {
                ActualPlanning dbObject = new ActualPlanning();
                model.MapDbObject(dbObject);

                await RepoActualPlanning.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Rencana Realisasi", AppGlobalMessage.CreateSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_ACTUAL_PLAN.ToDescription() });
            }
            else
            {
                return View(PageSite.ActualPlan, model);
            }
        }
        #endregion

        #region "Action: Edit"

        [HttpPost]
        public async Task<ActionResult> EdiTaskReportSelected(int[] listId)
        {
            ResponseModel response = new ResponseModel(true);
            List<int> idFailed = new List<int>();
            TaskReport dbObject;
            List<Pltp> pltps;
            List<Dkp> dkps;
            List<Production> productions;
            int countListId = 0;

            if (listId.Any())
            {
                foreach (var id in listId)
                {
                    countListId += 1;
                    dbObject = await RepoTaskReport.FindByPrimaryKeyAsync(id);
                    pltps = dbObject.Pltps.Where(m => m.IsDeleted == false).ToList();
                    dkps = dbObject.Dkps.Where(m => m.IsDeleted == false).ToList();
                    productions = dbObject.Productions.Where(m => m.IsDeleted == false).ToList();

                    if (dbObject != null && pltps.Count > 0 && productions.Count > 0)
                    {
                        TaskReportFormStub model = new TaskReportFormStub();

                        model.ApproveDbObject(dbObject);

                        await RepoTaskReport.SaveAsync(dbObject);
                    }
                    else
                    {
                        idFailed.Add(countListId);
                    }
                }
            }

            if (idFailed.Count < 1)
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedSuccess).ToString();
                response.Message = message;
            }
            else
            {
                countListId = 0;
                string error = "Approve data gagal pada baris ke ";
                foreach (var item in idFailed)
                {
                    countListId += 1;
                    if (countListId == idFailed.Count)
                    {
                        error = error + " " + item.ToString();
                    }
                    else
                    {
                        error = error + " " + item.ToString() + ",";
                    }

                }
                error = error + " data yang sudah dipilih." + Environment.NewLine + "(data PLTP dan Produksi harus diisi)";

                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalError, GlobalMessageField.ApprovedFailed).ToString();
                response.SetFail(error);
            }
            return Json(response);
        }


        [HttpPost]
        public async Task<ActionResult> EdiTaskReport(int id)
        {
            ResponseModel response = new ResponseModel(true);
            TaskReport dbObject = await RepoTaskReport.FindByPrimaryKeyAsync(id);
            List<Pltp> pltps = dbObject.Pltps.Where(m => m.IsDeleted == false).ToList();
            List<Dkp> dkps = dbObject.Dkps.Where(m => m.IsDeleted == false).ToList();
            List<Production> productions = dbObject.Productions.Where(m => m.IsDeleted == false).ToList();

            if (dbObject != null && pltps.Count > 0 && productions.Count > 0)
            {
                TaskReportFormStub model = new TaskReportFormStub();

                model.ApproveDbObject(dbObject);

                await RepoTaskReport.SaveAsync(dbObject);

                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.ApprovedSuccess).ToString();
                response.Message = message;
            }
            else
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalError, GlobalMessageField.ApprovedFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [MvcSiteMapNode(Title = "Edit Pltp", Key = "EditPltp", ParentKey = "Detail")]
        public async Task<ActionResult> EditPltp(int id)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            Pltp dbObject = await RepoPltp.FindByPrimaryKeyAsync(id);
            PltpFormStub model = new PltpFormStub(dbObject);

            ViewBag.SelectedPowerPlanId = model.PowerPlantId;

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_PLTP.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Pltp, model);
        }

        [HttpPost]
        public async Task<ActionResult> EditPltp(PltpFormStub model)
        {
            if (ModelState.IsValid)
            {
                Pltp dbObject = await RepoPltp.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoPltp.SaveAsync(dbObject);

                //update jika ada so status draft untuk zpge
                FilterQuery filter;
                filter = new FilterQuery(FilterLogic.And);
                Business.Entities.SalesOrder so = new Business.Entities.SalesOrder();
                float quantity = 0;
                List<Business.Entities.TaskReport> reportsFilter;
                List<Business.Entities.Pltp> pltps;

                DateTime periode;
                if (dbObject.PowerPlant.SOType == SOType.ZPGE.ToString())
                {
                    periode = new DateTime(dbObject.TaskReport.Periode.Value.Year, dbObject.TaskReport.Periode.Value.Month, 1);

                    filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, dbObject.PowerPlantId);
                    filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, periode);

                    so = RepoSalesOrder.Find(filter);

                    if (so != null)
                    {
                        reportsFilter = RepoTaskReport.GetFromPowerPlantIdPeriode(dbObject.PowerPlantId, dbObject.TaskReport.Periode);
                        pltps = reportsFilter.SelectMany(x => x.Pltps.Where(y => y.PowerPlantId == dbObject.PowerPlantId)).ToList();


                        if (dbObject.PowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString())
                        {
                            quantity = pltps.Sum(x => x.EnergyNett).ToFloat();
                        }
                        else // if (PowerPlant.Material == Common.Enums.Material.GEOTHERMAL_STEAM.ToString())
                        {
                            quantity = pltps.Sum(x => x.EnergyGross).ToFloat();
                        }

                        so.Quantity = quantity;
                        so.NetValue = quantity * so.NetPrice;

                        RepoSalesOrder.Save(so);
                    }
                }

                //message
                this.SetMessage("Data Pltp", AppGlobalMessage.EditSuccess);
                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_PLTP.ToDescription() });
            }
            else
            {
                return View(PageSite.Pltp, model);
            }
        }

        [MvcSiteMapNode(Title = "Edit Dkp", Key = "EditDkp", ParentKey = "Detail")]
        public async Task<ActionResult> EditDkp(int id)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            Dkp dbObject = await RepoDkp.FindByPrimaryKeyAsync(id);
            DkpFormStub model = new DkpFormStub(dbObject);

            ViewBag.SelectedPowerPlanId = model.PowerPlantId;

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_DKP.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Dkp, model);
        }

        [HttpPost]
        public async Task<ActionResult> EditDkp(DkpFormStub model)
        {
            if (ModelState.IsValid)
            {
                Dkp dbObject = await RepoDkp.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoDkp.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Dkp", AppGlobalMessage.EditSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_DKP.ToDescription() });
            }
            else
            {
                return View(PageSite.Dkp, model);
            }
        }

        [MvcSiteMapNode(Title = "Edit Produksi", Key = "EditProduction", ParentKey = "Detail")]
        public async Task<ActionResult> EditProduction(int id)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            Production dbObject = await RepoProduction.FindByPrimaryKeyAsync(id);
            ProductionFormStub model = new ProductionFormStub(dbObject);

            ViewBag.SelectedWell = model.WellId;

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_PRODUCTION.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Production, model);
        }

        [HttpPost]
        public async Task<ActionResult> EditProduction(ProductionFormStub model)
        {
            if (ModelState.IsValid)
            {
                Production dbObject = await RepoProduction.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoProduction.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Produksi", AppGlobalMessage.EditSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_PRODUCTION.ToDescription() });
            }
            else
            {
                return View(PageSite.Production, model);
            }
        }

        [MvcSiteMapNode(Title = "Edit Reinjeksi", Key = "EditReinjection", ParentKey = "Detail")]
        public async Task<ActionResult> EditReinjection(int id)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            ReInjection dbObject = await RepoReinjection.FindByPrimaryKeyAsync(id);
            ReinjectionFormStub model = new ReinjectionFormStub(dbObject);

            ViewBag.SelectedWell = model.WellId;

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_REINJECTION.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Reinjection, model);
        }

        [HttpPost]
        public async Task<ActionResult> EditReinjection(ReinjectionFormStub model)
        {
            if (ModelState.IsValid)
            {
                ReInjection dbObject = await RepoReinjection.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoReinjection.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Reinjeksi", AppGlobalMessage.EditSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_REINJECTION.ToDescription() });
            }
            else
            {
                return View(PageSite.Reinjection, model);
            }
        }

        [MvcSiteMapNode(Title = "Edit Monitoring", Key = "EditMonitoring", ParentKey = "Detail")]
        public async Task<ActionResult> EditMonitoring(int id)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            Monitoring dbObject = await RepoMonitoring.FindByPrimaryKeyAsync(id);
            MonitoringFormStub model = new MonitoringFormStub(dbObject);

            ViewBag.SelectedWell = model.WellId;

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_MONITORING.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Monitoring, model);
        }

        [HttpPost]
        public async Task<ActionResult> EditMonitoring(MonitoringFormStub model)
        {
            if (ModelState.IsValid)
            {
                Monitoring dbObject = await RepoMonitoring.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoMonitoring.SaveAsync(dbObject);

                //message
                this.SetMessage(dbObject.Well.Name, AppGlobalMessage.EditSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_MONITORING.ToDescription() });
            }
            else
            {
                return View(PageSite.Monitoring, model);
            }
        }

        [MvcSiteMapNode(Title = "Edit Separator", Key = "EditSeparator", ParentKey = "Detail")]
        public async Task<ActionResult> EditSeparator(int id)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            Separator dbObject = await RepoSeparator.FindByPrimaryKeyAsync(id);
            SeparatorFormStub model = new SeparatorFormStub(dbObject);

            ViewBag.SelectedSeparatorId = model.SeparatorId;

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_SEPARATOR.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.Separator, model);
        }

        [HttpPost]
        public async Task<ActionResult> EditSeparator(SeparatorFormStub model)
        {
            if (ModelState.IsValid)
            {
                Separator dbObject = await RepoSeparator.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoSeparator.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Separator", AppGlobalMessage.EditSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_SEPARATOR.ToDescription() });
            }
            else
            {
                return View(PageSite.Separator, model);
            }
        }

        [MvcSiteMapNode(Title = "Edit Interface Scrubber", Key = "EditInterfaceScrubber", ParentKey = "Detail")]
        public async Task<ActionResult> EditInterface(int id)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            InterfaceScrubber dbObject = await RepoInterfaceScrubber.FindByPrimaryKeyAsync(id);
            InterfaceScrubberFormStub model = new InterfaceScrubberFormStub(dbObject);

            ViewBag.SelectedInterfaceId = model.InterfaceId;
            ViewBag.SelectedScrubberId = model.ScrubberId;

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_INTERFACE_SCRUBBER.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.InterfaceScrubber, model);
        }

        [HttpPost]
        public async Task<ActionResult> EditInterface(InterfaceScrubberFormStub model)
        {
            if (ModelState.IsValid)
            {
                InterfaceScrubber dbObject = await RepoInterfaceScrubber.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoInterfaceScrubber.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Interface Scrubber", AppGlobalMessage.EditSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_INTERFACE_SCRUBBER.ToDescription() });
            }
            else
            {
                return View(PageSite.InterfaceScrubber, model);
            }
        }

        [MvcSiteMapNode(Title = "Edit Rencana Realisasi", Key = "EditActualPlan", ParentKey = "Detail")]
        public async Task<ActionResult> EditActualPlan(int id)
        {
            HttpCookie cookieTargetTaskDetail = Request.Cookies[AppSession.TargetTaskDetail];
            ActualPlanning dbObject = await RepoActualPlanning.FindByPrimaryKeyAsync(id);
            ActualPlanFormStub model = new ActualPlanFormStub(dbObject);

            cookieTargetTaskDetail.SetExpiredValue(TargetTab.TAB_ACTUAL_PLAN.ToDescription());
            cookieTargetTaskDetail.EncryptValue();
            Response.Cookies.Set(cookieTargetTaskDetail);

            return View(PageSite.ActualPlan, model);
        }

        [HttpPost]
        public async Task<ActionResult> EditActualPlan(ActualPlanFormStub model)
        {
            if (ModelState.IsValid)
            {
                ActualPlanning dbObject = await RepoActualPlanning.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoActualPlanning.SaveAsync(dbObject);

                //message
                this.SetMessage("Data Rencana Realisasi", AppGlobalMessage.EditSuccess);

                return RedirectToAction(ActionSite.Detail, new { id = model.TaskReportId.Value, tab = TargetTab.TAB_ACTUAL_PLAN.ToDescription() });
            }
            else
            {
                return View(PageSite.ActualPlan, model);
            }
        }
        #endregion

        #region "Action: Delete"

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            //lib
            FilterQuery filter;

            Task<List<Pltp>> taskPltps = null;
            Task<List<Dkp>> taskDkps = null;
            Task<List<Production>> taskProductions = null;
            Task<List<ReInjection>> taskReinjections = null;
            Task<List<Monitoring>> taskMonitorings = null;
            Task<List<Separator>> taskSeparators = null;
            Task<List<InterfaceScrubber>> taskInterfaceScrubbers = null;
            Task<List<ActualPlanning>> taskActualPlans = null;

            if (id == null)
                throw new ArgumentNullException();

            string primaryKey = id[0].ToString();
            ResponseModel response = new ResponseModel(true);
            TaskReport dbObject = await RepoTaskReport.FindByPrimaryKeyAsync(int.Parse(primaryKey));

            if (!(await RepoTaskReport.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }
            else
            {
                filter = new FilterQuery(Field.TaskReportId, FilterOperator.Equals, int.Parse(primaryKey));

                taskPltps = RepoPltp.FindAllAsync(filter);
                taskDkps = RepoDkp.FindAllAsync(filter);
                taskProductions = RepoProduction.FindAllAsync(filter);
                taskReinjections = RepoReinjection.FindAllAsync(filter);
                taskMonitorings = RepoMonitoring.FindAllAsync(filter);
                taskSeparators = RepoSeparator.FindAllAsync(filter);
                taskInterfaceScrubbers = RepoInterfaceScrubber.FindAllAsync(filter);
                taskActualPlans = RepoActualPlanning.FindAllAsync(filter);

                await Task.WhenAll(taskPltps, taskDkps, taskProductions, taskReinjections, taskMonitorings,
                                   taskSeparators, taskInterfaceScrubbers, taskActualPlans);

                if (taskPltps.Result.Any())
                {
                    await RepoPltp.DeleteAllAsync(taskPltps.Result);
                }
                if (taskDkps.Result.Any())
                {
                    await RepoDkp.DeleteAllAsync(taskDkps.Result);
                }
                if (taskProductions.Result.Any())
                {
                    await RepoProduction.DeleteAllAsync(taskProductions.Result);
                }
                if (taskReinjections.Result.Any())
                {
                    await RepoReinjection.DeleteAllAsync(taskReinjections.Result);
                }
                if (taskMonitorings.Result.Any())
                {
                    await RepoMonitoring.DeleteAllAsync(taskMonitorings.Result);
                }
                if (taskSeparators.Result.Any())
                {
                    await RepoSeparator.DeleteAllAsync(taskSeparators.Result);
                }
                if (taskInterfaceScrubbers.Result.Any())
                {
                    await RepoInterfaceScrubber.DeleteAllAsync(taskInterfaceScrubbers.Result);
                }
                if (taskActualPlans.Result.Any())
                {
                    await RepoActualPlanning.DeleteAllAsync(taskActualPlans.Result);
                }
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeletePltp(int id)
        {
            ResponseModel response = new ResponseModel(true);
            Pltp dbObject = await RepoPltp.FindByPrimaryKeyAsync(id);

            if (!(await RepoPltp.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteDkp(int id)
        {
            ResponseModel response = new ResponseModel(true);
            Dkp dbObject = await RepoDkp.FindByPrimaryKeyAsync(id);

            if (!(await RepoDkp.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteProduction(int id)
        {
            ResponseModel response = new ResponseModel(true);
            Production dbObject = await RepoProduction.FindByPrimaryKeyAsync(id);

            if (!(await RepoProduction.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteReinjection(int id)
        {
            ResponseModel response = new ResponseModel(true);
            ReInjection dbObject = await RepoReinjection.FindByPrimaryKeyAsync(id);

            if (!(await RepoReinjection.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteMonitoring(int id)
        {
            ResponseModel response = new ResponseModel(true);
            Monitoring dbObject = await RepoMonitoring.FindByPrimaryKeyAsync(id);

            if (!(await RepoMonitoring.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteSeparator(int id)
        {
            ResponseModel response = new ResponseModel(true);
            Separator dbObject = await RepoSeparator.FindByPrimaryKeyAsync(id);

            if (!(await RepoSeparator.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteInterface(int id)
        {
            ResponseModel response = new ResponseModel(true);
            InterfaceScrubber dbObject = await RepoInterfaceScrubber.FindByPrimaryKeyAsync(id);

            if (!(await RepoInterfaceScrubber.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteActualPlan(int id)
        {
            ResponseModel response = new ResponseModel(true);
            ActualPlanning dbObject = await RepoActualPlanning.FindByPrimaryKeyAsync(id);

            if (!(await RepoActualPlanning.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion
        [HttpPost]
        public async Task<ActionResult> Reject(params object[] id)
        {
            //lib
            FilterQuery filter;         

            if (id == null)
                throw new ArgumentNullException();

            string primaryKey = id[0].ToString();
            ResponseModel response = new ResponseModel(true);
            TaskReport dbObject = await RepoTaskReport.FindByPrimaryKeyAsync(int.Parse(primaryKey));
            dbObject.Status = DataStatus.WAITING.ToString();

            await RepoTaskReport.SaveAsync(dbObject);            

            return Json(response);
        }

        #region "Download and Upload Excel Template"

        public ActionResult DownloadData()
        { 
            return View(PageSite.DownloadData);
        }

        [HttpPost]
        public async Task<ActionResult> GetData(int areaId, DateTime startDate, DateTime endDate)
        {
            string FriendlyName = "";
            List<string> fileNames = new List<string>();
            string[] fileArray = Directory.GetFiles(Server.MapPath(DirectoryHelper.Area(areaId.ToString())));
            
            foreach (var item in fileArray)
            {
                FriendlyName = FileManagementController.FriendlyName(item);
                for (DateTime date = startDate; date.Date <= endDate.Date; date = date.AddDays(1))
                {
                    string[] tempFileName = FriendlyName.Split(' ');
                    if (tempFileName[0] == date.ToString("yyMMdd"))
                    {
                        fileNames.Add(item);
                    }
                }
            }

            //lib
            string filePath;
            FileStream fileStream;
            DownloadParameterStub downloadParam = null;
            ResponseModel response = new ResponseModel(true);

            if (fileNames.Count == 1)
            {
                string fileName = fileNames[0];
                filePath = fileName;
                //filePath = Path.Combine(Server.MapPath(DirectoryHelper.Area(modelParam.AreaId.ToString())), fileName);

                if (System.IO.File.Exists(filePath))
                {
                    fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    downloadParam = new DownloadParameterStub
                    {
                        MemoryStream = new MemoryStream(),
                        FileDownloadName = FileManagementController.FriendlyName(fileName),
                        ContentType = MimeMapping.GetMimeMapping(fileName)
                    };

                    fileStream.CopyTo(downloadParam.MemoryStream);
                    fileStream.Close();

                    TempData[KEY] = downloadParam;
                }
            }
            else
            {
                byte[] buffer = new byte[4096];
                MemoryStream memoryStream = new MemoryStream();
                ZipOutputStream zipOutputStream = new ZipOutputStream(memoryStream);

                //0-9, 9 being the highest level of compression
                zipOutputStream.SetLevel(9);

                foreach (string fileName in fileNames)
                {
                    //filePath = FileDirectory.GetDirectory(fileName);
                    filePath = Path.Combine(Server.MapPath(DirectoryHelper.Area(areaId.ToString())), fileName);

                    if (System.IO.File.Exists(filePath))
                    {
                        int count;
                        ZipEntry entry;

                        fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);

                        //Setting the Size provides WinXP built-in extractor compatibility,
                        //but if not available, you can set zipOutputStream.UseZip64 = UseZip64.Off instead.
                        string[] tempFileName = fileName.Split(' ');
                        entry = new ZipEntry(ZipEntry.CleanName(tempFileName[1]))
                        {
                            Size = fileStream.Length
                        };

                        zipOutputStream.PutNextEntry(entry);

                        count = fileStream.Read(buffer, 0, buffer.Length);

                        while (count > 0)
                        {
                            zipOutputStream.Write(buffer, 0, count);
                            count = fileStream.Read(buffer, 0, buffer.Length);
                        }

                        fileStream.Close();
                    }
                }

                if (memoryStream.Length > 0)
                {
                    string fileName = "DataOperation.zip";

                    zipOutputStream.CloseEntry();

                    // False stops the Close also Closing the underlying stream.
                    zipOutputStream.IsStreamOwner = false;

                    // Must finish the ZipOutputStream before using outputMemStream.
                    zipOutputStream.Close();

                    downloadParam = new DownloadParameterStub
                    {
                        MemoryStream = memoryStream,
                        FileDownloadName = fileName,
                        ContentType = MimeMapping.GetMimeMapping(fileName)
                    };

                    TempData[KEY] = downloadParam;
                }
            }
            

            if (downloadParam == null)
            {
                response.SetFail("File not found");
            }

            await Task.Delay(0);
            return Json(response);

        }

        public ActionResult DownloadDataOperation()
        {
            DownloadParameterStub param = TempData[KEY] as DownloadParameterStub;

            if (param is DownloadParameterStub)
            {
                return File(param.MemoryStream.ToArray(), param.ContentType, param.FileDownloadName);
            }

            return new EmptyResult();
        }

        public async Task<ActionResult> Download()
        {
            TaskReportFormStub model = new TaskReportFormStub();

            await Task.Delay(0);

            return PartialView(PageSite._Form, model);
        }

        public async Task<ActionResult> GetTemplate(int Id)
        {
            TaskParamStub param = new TaskParamStub();

            TaskReportFormStub model = new TaskReportFormStub();
            model.AreaId = Id;
            model.Periode = DateTime.Now.Date;
            FilterQuery filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, model.AreaId);
            List<SortQuery> sorts = new List<SortQuery> { new SortQuery(Field.Name, SortOrder.Ascending) };
            DateTime periode = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            Task<List<Business.Entities.PowerPlant>> plantTask = RepoPowerPlant.FindAllAsync(sorts, filter);
            Task<List<Well>> wellTask = RepoWell.FindAllAsync(sorts, filter);
            Task<List<MstSeparator>> separatorTask = RepoMasterSeparator.FindAllAsync(sorts, filter); ;
            Task<List<InterfacePoint>> interfacePointTask = RepoInterfacePoint.FindAllAsync(sorts, filter);
            Task<List<Scrubber>> scrubberTask = RepoScrubber.FindAllAsync(sorts, filter);

            filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, model.AreaId);
            filter.AddFilter(Field.Status, FilterOperator.Equals, DrawWellStatus.PRODUCTION);
            Task<List<ViewWell>> productionWellTask = RepoViewWell.FindAllAsync(filter);

            filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, model.AreaId);
            filter.AddFilter(Field.Status, FilterOperator.Equals, DrawWellStatus.INJECTION);
            Task<List<ViewWell>> injectionWellTask = RepoViewWell.FindAllAsync(filter);

            filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, model.AreaId);
            filter.AddFilter(Field.Status, FilterOperator.Equals, DrawWellStatus.MONITORING);
            Task<List<ViewWell>> monitoringWellTask = RepoViewWell.FindAllAsync(filter);

            await Task.WhenAll(plantTask, wellTask, separatorTask, interfacePointTask, scrubberTask, productionWellTask, injectionWellTask, monitoringWellTask);

            IEnumerable<Business.Entities.PowerPlant> plants = plantTask.Result;
            IEnumerable<Well> wells = wellTask.Result;
            IEnumerable<MstSeparator> separators = separatorTask.Result;
            IEnumerable<InterfacePoint> interfacePoints = interfacePointTask.Result;
            IEnumerable<Scrubber> scrubbers = scrubberTask.Result;
            IEnumerable<ViewWell> productionWells = productionWellTask.Result.Where(n => n.StartDate <= periode && n.EndDate >= periode);
            IEnumerable<ViewWell> injectionWells = injectionWellTask.Result.Where(n => n.StartDate <= periode && n.EndDate >= periode);
            IEnumerable<ViewWell> monitoringWells = monitoringWellTask.Result.Where(n => n.StartDate <= periode && n.EndDate >= periode);

            Area area = RepoArea.FindByPrimaryKey(model.AreaId);
            string code = area != null && !string.IsNullOrEmpty(area.Code) ? area.Code : "XXX";
            string filename =DateTime.Now.ToString("yyMMdd") + "_" + code + "_IPGE.xlsx";
            model.AreaName = area.Name;

            param.TaskReport = model;
            param.PowerPlants = plants;
            param.Wells = wells;
            param.Separators = separators;
            param.InterfacePoints = interfacePoints;
            param.Scrubbers = scrubbers;
            param.ProductionWells = productionWells;
            param.InjectionWells = injectionWells;
            param.MonitoringWells = monitoringWells;

            byte[] excel = TaskImportStub.GenerateTemplate(param);
            return File(excel, "application/x-msexcel", filename);
        }

        public async Task<ActionResult> UploadPopUp()
        {
            TaskReportFormStub model = new TaskReportFormStub() { Periode = DateTime.Now.Date };

            await Task.Delay(0);

            return PartialView("_FormUpload", model);
        }

        [HttpPost]
        public async Task<JsonResult> Upload(HttpPostedFileBase[] files, DateTime? periode)
        {
            ResponseModel resp = new ResponseModel(true)
            {
                Message = "Upload Excel Success"
            };

            List<Area> areasCode = await RepoArea.FindAllAsync();

            string filename = files.Length > 0 && files[0] != null ? files[0].FileName : string.Empty;
            bool isPeriodeValid = false;
            bool isCodeValid = false;
            bool isPureValid = false;
            bool isOldTemplate = false;
            int year = 0, month = 0, day = 0;
            int defaultYear = ConfigurationManager.AppSettings["DefaultYear"].ToInteger();

            string[] tempFileName = filename.Split('_');

            if (!string.IsNullOrEmpty(filename) && tempFileName.Count() == 3)
            {
                DateTime per;
                if (periode != null)
                {
                    per = periode.Value;
                    isPeriodeValid = per.ToString("yyMMdd") == tempFileName[0];
                }
                else
                {
                    year = month = day = 0;
                    int.TryParse(tempFileName[0].Substring(0, 2), out year);
                    int.TryParse(tempFileName[0].Substring(2, 2), out month);
                    int.TryParse(tempFileName[0].Substring(4, 2), out day);
                    if (year <= 0 || month <= 0 || day <= 0 || month > 12 || day > 31)
                        isPeriodeValid = false;
                    else
                    {
                        isPeriodeValid = true;
                        year += defaultYear;
                        periode = new DateTime(year, month, day);
                    }
                }

                //isPeriodeValid = per.ToString("yyMMdd") == tempFileName[0];


                if (areasCode != null && areasCode.Any())
                {
                    isCodeValid = areasCode.Any(a => a.Code == tempFileName[1]);
                }
                else
                {
                    isCodeValid = false;
                }

                isPureValid = (tempFileName[2] == "PURE.xlsx" || tempFileName[2] == "IPGE.xlsx");

                if (isPeriodeValid && isCodeValid && isPureValid)
                {
                    isOldTemplate = tempFileName[2] == "PURE.xlsx" ? true : false;
                    TaskImportStub importStub = await UploadExcel(files, isOldTemplate, periode);

                    //message
                    if (importStub.ErrParseExcel.Count > 0)
                    {
                        string errMsg = string.Join("<br/>", importStub.ErrParseExcel);
                        //string errMsg = string.Join("\n", importStub.ErrParseExcel);
                        resp.SetFail(errMsg);
                        //this.SetMessage(errMsg);
                    }
                    //}
                    //else
                    //{
                    //    resp.SetFail("Pressure line in row "+importStub.InvalidProductionRow+" sheet PRODUCTION must less than or equals to Tks!");
                    //}
                }
                else
                {
                    resp.SetFail("Invalid File Name! File name YYMMDD_XXX_PURE (Old Template) Or File name YYMMDD_XXX_IPGE (New Template), X = Area Code");
                }
            }
            else
            {
                resp.SetFail("Invalid File Name! File name YYMMDD_XXX_PURE (Old Template) Or File name YYMMDD_XXX_IPGE (New Template), X = Area Code");
            }

            return Json(resp);
        }

        [HttpPost]
        public async Task<JsonResult> UploadMultiple(HttpPostedFileBase[] files)
        {
            string fileName = "";
            string friendlyFilename = "";
            string identifier = "";
           
            // kamus            
            ResponseModel resp = new ResponseModel(true)
            {
                Message = "Upload Excel Success"
            };
            List<Task<TaskImportStub>> taskMapping = new List<Task<TaskImportStub>>();


            foreach (var file in files)
            {
                var temps = new HttpPostedFileBase[] { file };
                List<Area> areasCode = await RepoArea.FindAllAsync();

                string filename = file.FileName;
                bool isPeriodeValid = false;
                bool isCodeValid = false;
                bool isPureValid = false;
                bool isOldTemplate = false;
                int year = 0, month = 0, day = 0;
                int areaId = 0;
                int defaultYear = ConfigurationManager.AppSettings["DefaultYear"].ToInteger();

                string[] tempFileName = filename.Split('_');

                if (!string.IsNullOrEmpty(filename) && tempFileName.Count() == 3)
                {
                    DateTime? periode = null;

                    year = month = day = 0;
                    int.TryParse(tempFileName[0].Substring(0, 2), out year);
                    int.TryParse(tempFileName[0].Substring(2, 2), out month);
                    int.TryParse(tempFileName[0].Substring(4, 2), out day);
                    if (year <= 0 || month <= 0 || day <= 0 || month > 12 || day > 31)
                        isPeriodeValid = false;
                    else
                    {
                        isPeriodeValid = true;
                        year += defaultYear;
                        periode = new DateTime(year, month, day);
                    }

                    //isPeriodeValid = per.ToString("yyMMdd") == tempFileName[0];


                    if (areasCode != null && areasCode.Any())
                    {
                        isCodeValid = areasCode.Any(a => a.Code == tempFileName[1]);
                        if (isCodeValid)
                        {
                            areasCode = areasCode.Where(p => p.Code == tempFileName[1]).ToList();
                            Area area = areasCode.FirstOrDefault();
                            areaId = area.Id;
                        }
                        
                    }
                    else
                    {
                        isCodeValid = false;
                    }

                    isPureValid = (tempFileName[2] == "PURE.xlsx" || tempFileName[2] == "IPGE.xlsx");

                    if (isPeriodeValid && isCodeValid && isPureValid)
                    {
                        //save file
                        string serverPath;
                        fileName = Path.GetFileNameWithoutExtension(file.FileName);
                        friendlyFilename = $"{DisplayFormat.URLFriendly(fileName)}{Path.GetExtension(file.FileName)}";
                        fileName = $"{identifier} {friendlyFilename}";
                        serverPath = Path.Combine(Server.MapPath(DirectoryHelper.Area(areaId.ToString())), fileName);
                        if (System.IO.File.Exists(serverPath))
                        {
                            System.IO.File.Delete(serverPath);
                            file.SaveAs(serverPath);
                        }
                        else
                        {
                            file.SaveAs(serverPath);
                        }

                        //read file
                        isOldTemplate = tempFileName[2] == "PURE.xlsx" ? true : false;
                        taskMapping.Add(UploadExcel(temps, isOldTemplate, periode));
                    }
                    else
                    {
                        resp.SetFail("Invalid File Name! File name YYMMDD_XXX_PURE (Old Template) Or File name YYMMDD_XXX_IPGE (New Template), X = Area Code");
                    }
                }
                else
                {
                    resp.SetFail("Invalid File Name! File name YYMMDD_XXX_PURE (Old Template) Or File name YYMMDD_XXX_IPGE (New Template), X = Area Code");
                }

                await Task.WhenAll(taskMapping);

                IEnumerable<TaskImportStub> importStubs = taskMapping.Select(n => n.Result);
                IEnumerable<string> errParseExcels = importStubs.SelectMany(n => n.ErrParseExcel);


                //message
                if (errParseExcels.Any())
                {
                    string errMsg = string.Join("<br/>", errParseExcels);
                    //string errMsg = string.Join("\n", importStub.ErrParseExcel);
                    resp.SetFail(errMsg);
                    //this.SetMessage(errMsg);
                }
            }


            return Json(resp);
        }

        private async Task<TaskImportStub> UploadExcel(HttpPostedFileBase[] files, bool isOldTemplate, DateTime? periode = null)
        {
            TaskImportStub importStub = new TaskImportStub();
            FilterQuery filter;
            List<PltpImportFormStub> pltpModels = new List<PltpImportFormStub>();
            List<DkpImportFormStub> dkpModels = new List<DkpImportFormStub>();
            List<ProductionImportFormStub> productionModels = new List<ProductionImportFormStub>();
            List<ReinjectionImportFormStub> reInjectionModels = new List<ReinjectionImportFormStub>();
            List<MonitoringImportFormStub> monitoringModels = new List<MonitoringImportFormStub>();
            List<SeparatorImportFormStub> separatorModels = new List<SeparatorImportFormStub>();
            List<InterfaceScrubberImportFormStub> interfaceScrubberModels = new List<InterfaceScrubberImportFormStub>();
            List<ActualPlanningImportFormStub> actualPlanningModels = new List<ActualPlanningImportFormStub>();
            List<WellHistoryPresentationStub> wellHistoryModels = new List<WellHistoryPresentationStub>();

            Task<List<Business.Entities.PowerPlant>> plantTasks = null;
            List<Business.Entities.PowerPlant> plants;
            Task<List<Well>> wellTasks = null;
            List<Well> wells;
            Task<List<MstSeparator>> mstSeparatorTask = null;
            List<MstSeparator> mstSeparators;
            Task<List<InterfacePoint>> interfacePointTask = null;
            List<InterfacePoint> interfacePoints;
            Task<List<Scrubber>> scrubberTask = null;
            List<Scrubber> scrubbers;
            Task<List<Area>> areaTask = null;
            List<Area> areas;
            Task<List<ViewWell>> wellHistoriesTask = null;
            List<ViewWell> wellHistories;


            Pltp pltp;
            List<Pltp> pltps;
            Dkp dkp;
            List<Dkp> dkps = new List<Dkp>();
            Production production;
            List<Production> productions;
            ReInjection reInjection;
            List<ReInjection> reInjections;
            Monitoring monitoring;
            List<Monitoring> monitorings;
            Separator separator;
            List<Separator> separators;
            InterfaceScrubber interfaceScrubber;
            List<InterfaceScrubber> interfaceScrubbers;
            ActualPlanning actualPlan;
            List<ActualPlanning> actualPlans;


            TaskReport taskReport = new TaskReport();
            IEnumerable<TaskReport> taskReports = null;

            plantTasks = RepoPowerPlant.FindAllAsync();
            wellTasks = RepoWell.FindAllAsync();
            mstSeparatorTask = RepoMasterSeparator.FindAllAsync();
            interfacePointTask = RepoInterfacePoint.FindAllAsync();
            scrubberTask = RepoScrubber.FindAllAsync();
            areaTask = RepoArea.FindAllAsync();

            await Task.WhenAll(plantTasks, wellTasks, mstSeparatorTask,
                               interfacePointTask, scrubberTask, areaTask);

            plants = plantTasks.Result;
            wells = wellTasks.Result;
            mstSeparators = mstSeparatorTask.Result;
            interfacePoints = interfacePointTask.Result;
            scrubbers = scrubberTask.Result;
            areas = areaTask.Result;

            //parsing data row from worksheet file
            importStub.ParseFile(files, plants, wells, mstSeparators, interfacePoints, scrubbers, isOldTemplate);

            //map parsing file result to the model
            pltpModels = importStub.Pltpls;
            dkpModels = importStub.Dkps;
            productionModels = importStub.Productions;
            reInjectionModels = importStub.Reinjections;
            monitoringModels = importStub.Monitorings;
            separatorModels = importStub.Separators;
            interfaceScrubberModels = importStub.InterfaceScrubbers;
            actualPlanningModels = importStub.ActualPlannings;

            //if(importStub.InvalidProductionRow == -1)
            //{
            //filter parent data (TaskReport)
            //filter = new FilterQuery(Field.Periode, FilterOperator.Equals, importStub.TaskReportStub.Periode.Value);
            filter = new FilterQuery(Field.Periode, FilterOperator.Equals, periode);
            filter.AddFilter(Field.AreaId, FilterOperator.Equals, importStub.TaskReportStub.AreaId);
            taskReports = RepoTaskReport.FindAll(filter);
            try
            {
                if (!taskReports.Any())
                {
                    importStub.TaskReportStub.Status = DataStatus.WAITING.ToString();
                    importStub.TaskReportStub.SourceInput = InputSource.WEB.ToString();
                    importStub.TaskReportStub.Periode = periode;
                    importStub.TaskReportStub.MapDbObject(taskReport);
                }
                else
                {
                    taskReport = taskReports.First();
                }


                await RepoTaskReport.SaveAsync(taskReport);

                if (taskReport.Status != DataStatus.APPROVED.ToString())
                {
                    string status = DataStatus.APPROVED.ToString();
                    //string periode = importStub.TaskReportStub.Periode.Value.ToString(DisplayFormat.SqlDateFormat);
                    int areaId = importStub.TaskReportStub.AreaId.Value;

                    //saving Pltp
                    if (pltpModels.Count() > 0)
                    {
                        filter = new FilterQuery();

                        foreach (var item in pltpModels)
                        {
                            if (item.PowerPlantId.HasValue)
                            {
                                item.TaskReportId = taskReport.Id;
                                pltp = new Pltp();

                                filter = CustomeFilter(status, periode, areaId);
                                filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, item.PowerPlantId);
                                pltps = await RepoPltp.FindAllAsync(filter);

                                if (pltps.Count > 0)
                                {
                                    pltp = pltps.FirstOrDefault();
                                    item.UpdateDbObject(pltp);
                                }
                                else
                                {
                                    item.MapDbObject(pltp);
                                }

                                await RepoPltp.SaveAsync(pltp);
                            }
                        }
                    }

                    //delete old dkp
                    dkpModels.ForEach(n => n.TaskReportId = taskReport.Id);
                    if (dkpModels.Any())
                    {
                        filter = new FilterQuery();
                        filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Id), FilterOperator.Equals, taskReport.Id);                        
                        dkps = await RepoDkp.FindAllAsync(filter);
                        await RepoDkp.DeleteAllAsync(dkps);
                    }

                    //saving Dkp
                    if (dkpModels.Count() > 0)
                    {
                        dkps = new List<Dkp>();
                        foreach (var d in dkpModels)
                        {
                            if (d.PowerPlantId.HasValue)
                            {
                                d.TaskReportId = taskReport.Id;
                                dkp = new Dkp();
                                d.MapDbObject(dkp);
                                dkps.Add(dkp);                                        
                            }
                        }

                        await RepoDkp.SaveAllAsync(dkps);
                    }

                    //saving Production
                    if (productionModels.Count() > 0)
                    {

                        foreach (var p in productionModels)
                        {
                            if (p.WellId.HasValue)
                            {
                                p.TaskReportId = taskReport.Id;
                                production = new Production();

                                filter = new FilterQuery();
                                filter = CustomeFilter(status, periode, areaId);

                                filter.AddFilter(Field.WellId, FilterOperator.Equals, p.WellId);
                                productions = await RepoProduction.FindAllAsync(filter);

                                if (productions.Count > 0)
                                {
                                    production = productions.FirstOrDefault();
                                    p.UpdateDbObject(production);
                                }
                                else
                                {
                                    p.MapDbObject(production);
                                }

                                await RepoProduction.SaveAsync(production);
                            }
                        }

                    }

                    //saving Reinjection
                    if (reInjectionModels.Count() > 0)
                    {
                        foreach (var r in reInjectionModels)
                        {
                            if (r.WellId.HasValue)
                            {
                                r.TaskReportId = taskReport.Id;
                                reInjection = new ReInjection();

                                filter = new FilterQuery();
                                filter = CustomeFilter(status, periode, areaId);
                                filter.AddFilter(Field.WellId, FilterOperator.Equals, r.WellId);
                                reInjections = await RepoReinjection.FindAllAsync(filter);

                                if (reInjections.Count > 0)
                                {
                                    reInjection = reInjections.FirstOrDefault();
                                    r.UpdateDbObject(reInjection);
                                }
                                else
                                {
                                    r.MapDbObject(reInjection);
                                }

                                await RepoReinjection.SaveAsync(reInjection);
                            }
                        }
                    }

                    //saving Monitoring
                    if (monitoringModels.Count() > 0)
                    {
                        foreach (var m in monitoringModels)
                        {
                            if (m.WellId != null)
                            {
                                monitoring = new Monitoring();
                                m.TaskReportId = taskReport.Id;

                                filter = new FilterQuery();
                                filter = CustomeFilter(status, periode, areaId);

                                filter.AddFilter(Field.WellId, FilterOperator.Equals, m.WellId);
                                monitorings = await RepoMonitoring.FindAllAsync(filter);

                                if (monitorings.Count > 0)
                                {
                                    monitoring = monitorings.FirstOrDefault();
                                    m.UpdateDbObject(monitoring);
                                }
                                else
                                {
                                    m.MapDbObject(monitoring);
                                }
                                await RepoMonitoring.SaveAsync(monitoring);
                            }
                        }
                    }

                    //saving Separator
                    if (separatorModels.Count() > 0)
                    {
                        foreach (var s in separatorModels)
                        {
                            if (s.SeparatorId != null)
                            {
                                separator = new Separator();
                                s.TaskReportId = taskReport.Id;

                                filter = new FilterQuery();
                                filter = CustomeFilter(status, periode, areaId);

                                filter.AddFilter(Field.SeparatorId, FilterOperator.Equals, s.SeparatorId);
                                separators = await RepoSeparator.FindAllAsync(filter);

                                if (separators.Count > 0)
                                {
                                    separator = separators.FirstOrDefault();
                                    s.UpdateDbObject(separator);
                                }
                                else
                                {
                                    s.MapDbObject(separator);
                                }

                                await RepoSeparator.SaveAsync(separator);
                            }
                        }
                    }

                    //saving Interface Scrubber
                    if (interfaceScrubberModels.Count() > 0)
                    {
                        foreach (var i in interfaceScrubberModels)
                        {
                            interfaceScrubber = new InterfaceScrubber();
                            i.TaskReportId = taskReport.Id;

                            filter = new FilterQuery();
                            filter = CustomeFilter(status, periode, areaId);
                            if (i.InterfaceId != null)
                                filter.AddFilter(Field.InterfaceId, FilterOperator.Equals, i.InterfaceId);
                            if (i.ScrubberId != null)
                                filter.AddFilter(Field.ScrubberId, FilterOperator.Equals, i.ScrubberId);
                            interfaceScrubbers = await RepoInterfaceScrubber.FindAllAsync(filter);

                            if (interfaceScrubbers.Count > 0)
                            {
                                interfaceScrubber = interfaceScrubbers.FirstOrDefault();
                                i.UpdateDboObject(interfaceScrubber);
                            }
                            else
                            {
                                i.MapDbObject(interfaceScrubber);
                            }

                            await RepoInterfaceScrubber.SaveAsync(interfaceScrubber);
                        }
                    }

                    //saving Actual Plan
                    if (actualPlanningModels.Count() > 0)
                    {
                        foreach (var a in actualPlanningModels)
                        {
                            actualPlan = new ActualPlanning();
                            a.TaskReportId = taskReport.Id;

                            filter = new FilterQuery();
                            filter = CustomeFilter(status, periode, areaId);
                            filter.AddFilter(Field.Planning, FilterOperator.Equals, a.Planning);

                            actualPlans = await RepoActualPlanning.FindAllAsync(filter);

                            if (actualPlans.Count > 0)
                            {
                                actualPlan = actualPlans.FirstOrDefault();
                                a.UpdateDbObject(actualPlan);
                            }
                            else
                            {
                                a.MapDbObject(actualPlan);
                            }

                            await RepoActualPlanning.SaveAsync(actualPlan);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var cek = importStub;
            }
            return importStub;
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //kamus
            int count;
            List<TaskReport> dbObjects;
            List<TaskReportPresentationStub> models;
            Task<List<TaskReport>> taskDbObjects = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            List<Business.Entities.PowerPlant> plants = new List<Business.Entities.PowerPlant>();
            FilterQuery filter = new FilterQuery(FilterLogic.And);
            IEnumerable<int> areaZts;
            Area area;

            if (args != null && args.Any())
            {
                string status = args.ElementAt(0).ToString();

                if (param.Filter == null)
                    param.InstanceFilter();

                param.Filter.AddFilter(Field.Status, FilterOperator.Equals, status);

                if (User.AreaId.HasValue && User.AreaId.Value > 0)
                {
                    area = await RepoArea.FindByPrimaryKeyAsync(User.AreaId.Value);
                    if (area.PowerPlants.Any(n => n.SOType == SOType.ZTS.ToString()))
                    {
                        filter.AddFilter(Field.SOType, FilterOperator.Equals, SOType.ZTS);
                        plants = RepoPowerPlant.FindAll(filter);
                        areaZts = plants.Any() ? plants.Select(n => n.AreaId.Value).Distinct() : null;

                        filter = new FilterQuery(FilterLogic.Or);
                        foreach (int id in areaZts)
                        {
                            filter.AddFilter(Field.AreaId, FilterOperator.Equals, id);
                        }

                        param.Filter.AddFilter(filter);
                    }
                    else
                    {
                        param.Filter.AddFilter(Field.AreaId, FilterOperator.Equals, User.AreaId);
                    }
                }

            }

            taskDbObjects = RepoTaskReport.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoTaskReport.CountAsync(param.Filter);

            await Task.WhenAll(taskDbObjects, countTask);

            dbObjects = taskDbObjects.Result;
            count = countTask.Result;

            models = ListMapper.MapList<TaskReport, TaskReportPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingPltp(params object[] args)
        {
            //kamus
            int count;
            List<Pltp> dbObjects;
            List<PltpPresentationStub> models;
            Task<List<Pltp>> dbObjectTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (args != null && args.Any())
            {
                int taskReportId = int.Parse(args.ElementAt(0).ToString());

                param.InstanceFilter();
                param.Filter.AddFilter(Field.TaskReportId, FilterOperator.Equals, taskReportId);
            }

            dbObjectTask = RepoPltp.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoPltp.CountAsync(param.Filter);

            await Task.WhenAll(dbObjectTask, countTask);

            dbObjects = dbObjectTask.Result;
            count = countTask.Result;

            models = ListMapper.MapList<Pltp, PltpPresentationStub>(dbObjects);

            //set can edit
            FilterQuery filter;
            filter = new FilterQuery(FilterLogic.And);
            Business.Entities.SalesOrder so = new Business.Entities.SalesOrder();
            foreach (PltpPresentationStub m in models)
            {
                DateTime periode;
                if (m.SOType == SOType.ZPGE.ToString())
                    periode = new DateTime(m.Periode.Year, m.Periode.Month, 1);
                else
                    periode = new DateTime(m.Periode.Year, m.Periode.Month, 28);

                filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, m.PowerPlantId);
                filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, periode);
                filter.AddFilter(Field.IsDeleted, FilterOperator.Equals, false);

                so = RepoSalesOrder.Find(filter);

                m.SetCanEdit(so);
            }

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingDkp(params object[] args)
        {
            //kamus
            int count;
            List<Dkp> dbObjects;
            List<DkpPresentationStub> models;
            Task<List<Dkp>> dbObjectTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (args != null && args.Any())
            {
                int taskReportId = int.Parse(args.ElementAt(0).ToString());

                param.InstanceFilter();
                param.Filter.AddFilter(Field.TaskReportId, FilterOperator.Equals, taskReportId);
            }

            dbObjectTask = RepoDkp.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoDkp.CountAsync(param.Filter);

            await Task.WhenAll(dbObjectTask, countTask);

            dbObjects = dbObjectTask.Result;
            count = countTask.Result;

            models = ListMapper.MapList<Dkp, DkpPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingProduction(params object[] args)
        {
            //kamus
            int count;
            List<Production> dbObjects;
            List<ProductionPresentationStub> models;
            Task<List<Production>> dbObjectTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (args != null && args.Any())
            {
                int taskReportId = int.Parse(args.ElementAt(0).ToString());

                param.InstanceFilter();
                param.Filter.AddFilter(Field.TaskReportId, FilterOperator.Equals, taskReportId);
            }

            dbObjectTask = RepoProduction.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoProduction.CountAsync(param.Filter);

            await Task.WhenAll(dbObjectTask, countTask);

            dbObjects = dbObjectTask.Result;
            count = countTask.Result;

            models = ListMapper.MapList<Production, ProductionPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingReinjection(params object[] args)
        {
            //kamus
            int count;
            List<ReInjection> dbObjects;
            List<ReinjectionPresentationStub> models;
            Task<List<ReInjection>> dbObjectTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (args != null && args.Any())
            {
                int taskReportId = int.Parse(args.ElementAt(0).ToString());

                param.InstanceFilter();
                param.Filter.AddFilter(Field.TaskReportId, FilterOperator.Equals, taskReportId);
            }

            dbObjectTask = RepoReinjection.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoReinjection.CountAsync(param.Filter);

            await Task.WhenAll(dbObjectTask, countTask);

            dbObjects = dbObjectTask.Result;
            count = countTask.Result;

            models = ListMapper.MapList<ReInjection, ReinjectionPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingMonitoring(params object[] args)
        {
            //kamus
            int count;
            List<Monitoring> dbObjects;
            List<MonitoringPresentationStub> models;
            Task<List<Monitoring>> dbObjectTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (args != null && args.Any())
            {
                int taskReportId = int.Parse(args.ElementAt(0).ToString());

                param.InstanceFilter();
                param.Filter.AddFilter(Field.TaskReportId, FilterOperator.Equals, taskReportId);
            }

            dbObjectTask = RepoMonitoring.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoMonitoring.CountAsync(param.Filter);

            await Task.WhenAll(dbObjectTask, countTask);

            dbObjects = dbObjectTask.Result;
            count = countTask.Result;

            models = ListMapper.MapList<Monitoring, MonitoringPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingSeparator(params object[] args)
        {
            //kamus
            int count;
            List<Separator> dbObjects;
            List<SeparatorPresentationStub> models;
            Task<List<Separator>> dbObjectTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (args != null && args.Any())
            {
                int taskReportId = int.Parse(args.ElementAt(0).ToString());

                param.InstanceFilter();
                param.Filter.AddFilter(Field.TaskReportId, FilterOperator.Equals, taskReportId);
            }

            dbObjectTask = RepoSeparator.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoSeparator.CountAsync(param.Filter);

            await Task.WhenAll(dbObjectTask, countTask);

            dbObjects = dbObjectTask.Result;
            count = countTask.Result;

            models = ListMapper.MapList<Separator, SeparatorPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingInterface(params object[] args)
        {
            //kamus
            int count;
            List<InterfaceScrubber> dbObjects;
            List<InterfaceScrubberPresentationStub> models;
            Task<List<InterfaceScrubber>> dbObjectTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (args != null && args.Any())
            {
                int taskReportId = int.Parse(args.ElementAt(0).ToString());

                param.InstanceFilter();
                param.Filter.AddFilter(Field.TaskReportId, FilterOperator.Equals, taskReportId);
            }

            dbObjectTask = RepoInterfaceScrubber.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoInterfaceScrubber.CountAsync(param.Filter);

            await Task.WhenAll(dbObjectTask, countTask);

            dbObjects = dbObjectTask.Result;
            count = countTask.Result;

            models = ListMapper.MapList<InterfaceScrubber, InterfaceScrubberPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingActualPlan(params object[] args)
        {
            //kamus
            int count;
            List<ActualPlanning> dbObjects;
            List<ActualPlanningPresentationStub> models;
            Task<List<ActualPlanning>> dbObjectTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (args != null && args.Any())
            {
                int taskReportId = int.Parse(args.ElementAt(0).ToString());

                param.InstanceFilter();
                param.Filter.AddFilter(Field.TaskReportId, FilterOperator.Equals, taskReportId);
            }

            dbObjectTask = RepoActualPlanning.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoActualPlanning.CountAsync(param.Filter);

            await Task.WhenAll(dbObjectTask, countTask);

            dbObjects = dbObjectTask.Result;
            count = countTask.Result;

            models = ListMapper.MapList<ActualPlanning, ActualPlanningPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion

        #region "Helper"
        private FilterQuery CustomeFilter(string status, DateTime? periode, int areaId)
        {
            FilterQuery result = new FilterQuery(
                        new FieldHelper(
                        Table.TaskReport, Field.Status), FilterOperator.NotEquals, status);
            result.AddFilter(new FieldHelper(
                            Table.TaskReport, Field.Periode), FilterOperator.Equals, periode);
            result.AddFilter(new FieldHelper(
            Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);

            return result;
        }

        public async Task<JsonResult> CompareValue(string PressureLine, string Tks)
        {
            //lib
            bool isLTE;

            //algorithm
            if (PressureLine == null || Tks == null)
            {
                isLTE = false;
            }
            else
            {
                isLTE = PressureLine.ToDouble() <= Tks.ToDouble();
            }

            await Task.Delay(0);

            return Json(isLTE, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
