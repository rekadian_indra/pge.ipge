﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.OperationData.Models;
using WebUI.Areas.OperationData.Models.DataHistory;
using WebUI.Controllers;
using WebUI.Infrastructure;
using WebUI.Infrastructure.Attribute;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Controllers
{
    [CheckSessionTimeOut]
    [Authorize]
    public class DataHistoryController : BaseController
    {
        public DataHistoryController
        (
            IAreaRepository repoArea,
            IPltpRepository repoPltp, 
            ITaskReportRepository repoTaskReport,
            IPowerPlantRepository repoPowerPlant,
            IDkpRepository repoDkp,
            IProductionRepository repoProduction,
            IReInjectionRepository repoReinjection,
            IMonitoringRepository repoMonitoring,
            ISeparatorRepository repoSeparator,
            IInterfaceScrubberRepository repoInterfaceScrubber,
            IWellRepository repoWell,
            IMstSeparatorRepository repoMasterSeparator,
            IInterfacePointRepository repoInterfacePoint,
            IScrubberRepository repoScrubber,
            IViewWellRepository repoViewWell
            
        )
        {
            RepoArea = repoArea;
            RepoPltp = repoPltp;
            RepoTaskReport = repoTaskReport;
            RepoPowerPlant = repoPowerPlant;
            RepoDkp = repoDkp;
            RepoProduction = repoProduction;
            RepoReinjection = repoReinjection;
            RepoMonitoring = repoMonitoring;
            RepoSeparator = repoSeparator;
            RepoInterfaceScrubber = repoInterfaceScrubber;
            RepoWell = repoWell;
            RepoMasterSeparator = repoMasterSeparator;
            RepoInterfacePoint = repoInterfacePoint;
            RepoScrubber = repoScrubber;
            RepoViewWell = repoViewWell;
        }

        //public override async Task<ActionResult> Index()
        //{
        //    return await base.Index();
        //}

        #region "Main Action"

        #endregion

        [MvcSiteMapNode(Title = "Power Plant History", Key = "PowerPlantHistory", ParentKey = "Dashboard")]
        public async Task<ActionResult> PowerPlantHistory()
        {
            DataHistoryParamStub model = new DataHistoryParamStub()
            {
                StartDate = DateTime.Now.Date,
                EndDate = DateTime.Now.AddDays(1).Date
            };

            await Task.Delay(0);
            return View(PageSite.PltpHistory, model);
        }

        [MvcSiteMapNode(Title = "Data Kondisi Peningkat History", Key = "DkpHistory", ParentKey = "Dashboard")]
        public async Task<ActionResult> DkpHistory()
        {
            DataHistoryParamStub model = new DataHistoryParamStub()
            {
                StartDate = DateTime.Now.Date,
                EndDate = DateTime.Now.AddDays(1).Date
            };

            await Task.Delay(0);
            return View(PageSite.DkpHistory, model);
        }

        [MvcSiteMapNode(Title = "Sumur Produksi, Reinjeksi, dan Monitoring History", Key = "WellHistory", ParentKey = "Dashboard")]
        public async Task<ActionResult> WellHistory()
        {
            DataHistoryParamStub model = new DataHistoryParamStub()
            {
                StartDate = DateTime.Now.Date,
                EndDate = DateTime.Now.AddDays(1).Date
            };

            await Task.Delay(0);
            return View(PageSite.WellHistory, model);
        }

        [MvcSiteMapNode(Title = "Separator History", Key = "SeparatorHistory", ParentKey = "Dashboard")]
        public async Task<ActionResult> SeparatorHistory()
        {
            DataHistoryParamStub model = new DataHistoryParamStub()
            {
                StartDate = DateTime.Now.Date,
                EndDate = DateTime.Now.AddDays(1).Date
            };

            await Task.Delay(0);
            return View(PageSite.SeparatorHistory, model);
        }

        [MvcSiteMapNode(Title = "Interface Scrubber History", Key = "InterfaceScrubberHistory", ParentKey = "Dashboard")]
        public async Task<ActionResult> InterfaceScrubberHistory()
        {
            DataHistoryParamStub model = new DataHistoryParamStub()
            {
                StartDate = DateTime.Now.Date,
                EndDate = DateTime.Now.AddDays(1).Date
            };

            await Task.Delay(0);
            return View(PageSite.InterfaceHistory, model);
        }
        #region "Binding"
        public async Task<string> BindingPltpHistory(params object[] args)
        {
            int count;
            List<Pltp> dbObjects;
            List<PltpPresentationStub> models;
            Task<List<Pltp>> taskDbObjects = null;
            Task<int> countTask = null;
            DataHistoryParamStub model = new DataHistoryParamStub();
            QueryRequestParameter param = QueryRequestParameter.Current;



            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                    int.Parse(args.ElementAt(0).ToString()) : (int?)null;
                string startDate = args.ElementAt(1).ToString();
                string endDate = args.ElementAt(2).ToString();

                if (areaId != null && !string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    param.InstanceFilter();
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, startDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, endDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Status), FilterOperator.Equals, DataStatus.APPROVED.ToString());
                }
            }

            taskDbObjects = RepoPltp.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoPltp.CountAsync(param.Filter);

            await Task.WhenAll(taskDbObjects, countTask);

            dbObjects = taskDbObjects.Result;
            count = countTask.Result;

            models = ListMapper.MapList<Pltp, PltpPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingDkpHistory(params object[] args)
        {
            int count;
            List<Dkp> dbObjects;
            List<DkpPresentationStub> models;
            Task<List<Dkp>> taskDbObjects = null;
            Task<int> countTask = null;
            DataHistoryParamStub model = new DataHistoryParamStub();
            QueryRequestParameter param = QueryRequestParameter.Current;



            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                    int.Parse(args.ElementAt(0).ToString()) : (int?)null;
                string startDate = args.ElementAt(1).ToString();
                string endDate = args.ElementAt(2).ToString();

                if (areaId != null && !string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    param.InstanceFilter();
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, startDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, endDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Status), FilterOperator.Equals, DataStatus.APPROVED.ToString());
                }
            }

            taskDbObjects = RepoDkp.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoDkp.CountAsync(param.Filter);

            await Task.WhenAll(taskDbObjects, countTask);

            dbObjects = taskDbObjects.Result;
            count = countTask.Result;

            models = ListMapper.MapList<Dkp, DkpPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingSeparatorHistory(params object[] args)
        {
            int count;
            List<Separator> dbObjects;
            List<SeparatorPresentationStub> models;
            Task<List<Separator>> taskDbObjects = null;
            Task<int> countTask = null;
            DataHistoryParamStub model = new DataHistoryParamStub();
            QueryRequestParameter param = QueryRequestParameter.Current;



            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                    int.Parse(args.ElementAt(0).ToString()) : (int?)null;
                string startDate = args.ElementAt(1).ToString();
                string endDate = args.ElementAt(2).ToString();

                if (areaId != null && !string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    param.InstanceFilter();
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, startDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, endDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Status), FilterOperator.Equals, DataStatus.APPROVED.ToString());
                }
            }

            taskDbObjects = RepoSeparator.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoSeparator.CountAsync(param.Filter);

            await Task.WhenAll(taskDbObjects, countTask);

            dbObjects = taskDbObjects.Result;
            count = countTask.Result;

            models = ListMapper.MapList<Separator, SeparatorPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingInterfaceScrubberHistory(params object[] args)
        {
            int count;
            List<InterfaceScrubber> dbObjects;
            List<InterfaceScrubberPresentationStub> models;
            Task<List<InterfaceScrubber>> taskDbObjects = null;
            Task<int> countTask = null;
            DataHistoryParamStub model = new DataHistoryParamStub();
            QueryRequestParameter param = QueryRequestParameter.Current;



            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                    int.Parse(args.ElementAt(0).ToString()) : (int?)null;
                string startDate = args.ElementAt(1).ToString();
                string endDate = args.ElementAt(2).ToString();

                if (areaId != null && !string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    param.InstanceFilter();
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, startDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, endDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Status), FilterOperator.Equals, DataStatus.APPROVED.ToString());
                }
            }

            taskDbObjects = RepoInterfaceScrubber.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoInterfaceScrubber.CountAsync(param.Filter);

            await Task.WhenAll(taskDbObjects, countTask);

            dbObjects = taskDbObjects.Result;
            count = countTask.Result;

            models = ListMapper.MapList<InterfaceScrubber, InterfaceScrubberPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingProductionHistory(params object[] args)
        {
            int count;
            List<Production> dbObjects;
            List<ProductionPresentationStub> models;
            Task<List<Production>> taskDbObjects = null;
            Task<int> countTask = null;
            DataHistoryParamStub model = new DataHistoryParamStub();
            QueryRequestParameter param = QueryRequestParameter.Current;



            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                    int.Parse(args.ElementAt(0).ToString()) : (int?)null;
                string startDate = args.ElementAt(1).ToString();
                string endDate = args.ElementAt(2).ToString();

                if (areaId != null && !string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    param.InstanceFilter();
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, startDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, endDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Status), FilterOperator.Equals, DataStatus.APPROVED.ToString());
                }
            }

            taskDbObjects = RepoProduction.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoProduction.CountAsync(param.Filter);

            await Task.WhenAll(taskDbObjects, countTask);

            dbObjects = taskDbObjects.Result;
            count = countTask.Result;

            models = ListMapper.MapList<Production, ProductionPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingReinjectionHistory(params object[] args)
        {
            int count;
            List<ReInjection> dbObjects;
            List<ReinjectionPresentationStub> models;
            Task<List<ReInjection>> taskDbObjects = null;
            Task<int> countTask = null;
            DataHistoryParamStub model = new DataHistoryParamStub();
            QueryRequestParameter param = QueryRequestParameter.Current;



            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                    int.Parse(args.ElementAt(0).ToString()) : (int?)null;
                string startDate = args.ElementAt(1).ToString();
                string endDate = args.ElementAt(2).ToString();

                if (areaId != null && !string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    param.InstanceFilter();
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, startDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, endDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Status), FilterOperator.Equals, DataStatus.APPROVED.ToString());
                }
            }

            taskDbObjects = RepoReinjection.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoReinjection.CountAsync(param.Filter);

            await Task.WhenAll(taskDbObjects, countTask);

            dbObjects = taskDbObjects.Result;
            count = countTask.Result;

            models = ListMapper.MapList<ReInjection, ReinjectionPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingMonitoringHistory(params object[] args)
        {
            int count;
            List<Monitoring> dbObjects;
            List<MonitoringPresentationStub> models;
            Task<List<Monitoring>> taskDbObjects = null;
            Task<int> countTask = null;
            DataHistoryParamStub model = new DataHistoryParamStub();
            QueryRequestParameter param = QueryRequestParameter.Current;



            if (args != null && args.Any())
            {
                int? areaId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                    int.Parse(args.ElementAt(0).ToString()) : (int?)null;
                string startDate = args.ElementAt(1).ToString();
                string endDate = args.ElementAt(2).ToString();

                if (areaId != null && !string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    param.InstanceFilter();
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, areaId);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, startDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, endDate);
                    param.Filter.AddFilter(new FieldHelper(Table.TaskReport, Field.Status), FilterOperator.Equals, DataStatus.APPROVED.ToString());
                }
            }

            taskDbObjects = RepoMonitoring.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoMonitoring.CountAsync(param.Filter);
            
            await Task.WhenAll(taskDbObjects, countTask);

            dbObjects = taskDbObjects.Result;
            count = countTask.Result;

            models = ListMapper.MapList<Monitoring, MonitoringPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion

        [HttpPost]
        public async Task<ActionResult> ExportExcelGridDKP(string contentType, string base64, string fileName)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            byte[] excelFile = Convert.FromBase64String(base64);

            MemoryStream memoryStream = new MemoryStream(excelFile);

            XSSFWorkbook workbook = new XSSFWorkbook(memoryStream);
            XSSFSheet sheet = (XSSFSheet)workbook.GetSheetAt(0);
            XSSFDataFormat dataFormat = (XSSFDataFormat)workbook.CreateDataFormat();
            XSSFCellStyle periodeCellStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            periodeCellStyle.DataFormat = dataFormat.GetFormat(DisplayFormat.ShortDateFormat);
            XSSFCellStyle hourMinuteCellStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            hourMinuteCellStyle.Alignment = HorizontalAlignment.Right;
            XSSFCellStyle deratingCellStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            deratingCellStyle.DataFormat = dataFormat.GetFormat("0.00");
            XSSFRow row;
            XSSFCell cell;

            if (sheet.LastRowNum > 0)
            {
                for (int i = 1; i <= sheet.LastRowNum; i++)
                {
                    row = (XSSFRow)sheet.GetRow(i);
                    cell = (XSSFCell)row.GetCell(1);
                    cell.CellStyle = periodeCellStyle;
                    cell = (XSSFCell)row.GetCell(3);
                    if (!string.IsNullOrEmpty(cell.StringCellValue))
                    {
                        cell.SetCellValue(cell.DateCellValue.ToString(DisplayFormat.SqlShortTimeFormat));
                        cell.CellStyle = hourMinuteCellStyle;
                    }
                    else
                    {
                        cell.SetCellValue("");
                    }
                    
                    cell = (XSSFCell)row.GetCell(4);
                    if (!string.IsNullOrEmpty(cell.StringCellValue))
                    {
                        cell.SetCellValue(cell.DateCellValue.ToString(DisplayFormat.SqlShortTimeFormat) != "00:00" ? cell.DateCellValue.ToString(DisplayFormat.SqlShortTimeFormat) : "24:00");
                        cell.CellStyle = hourMinuteCellStyle;
                    }
                    else
                    {
                        cell.SetCellValue("");
                    }
                    
                    cell = (XSSFCell)row.GetCell(5);
                    cell.CellStyle = deratingCellStyle;
                }
            }

            memoryStream = new MemoryStream();

            workbook.Write(memoryStream);

            excelFile = memoryStream.ToArray();

            await Task.Delay(0);
            return File(excelFile, contentType, fileName);
        }
    }
}