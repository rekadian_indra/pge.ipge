﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Web;
using System.Linq;
using WebUI.Extension;
using Resources;
using WebUI.Areas.OperationData.Models;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.OperationData.Controllers
{
    [CheckSessionTimeOut]
    [Authorize]
    public class MonitoringController : BaseController
    {
		public MonitoringController
        (
            IAreaRepository areaRepository,
            ITaskReportRepository taskReportRepository,
            IPowerPlantRepository powerPlantRepository
		)
        {
            RepoArea = areaRepository;
            RepoTaskReport = taskReportRepository;
            RepoPowerPlant = powerPlantRepository;
		}

        [MvcSiteMapNode(Title = TitleSite.Monitoring, Area = AreaSite.OperationData, ParentKey = KeySite.Dashboard, Key = KeySite.IndexMonitoring)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [HttpPost]
        public async Task<string> GetListArea()
        {
            FilterQuery filterPowerPlan = new FilterQuery();
            FilterQuery filterArea = new FilterQuery();
            filterPowerPlan.AddFilter(Field.SOType,FilterOperator.Equals,SOType.ZPGE.ToString());

            List<Business.Entities.PowerPlant> powerPlants = new List<Business.Entities.PowerPlant>();
            List<Area> areas = new List<Area>();
            List<SelectListItem> models = new List<SelectListItem>();

            if(User.AreaId != null && User.AreaId > 0)
            {
                filterPowerPlan.AddFilter(Field.AreaId, FilterOperator.Equals, User.AreaId.Value);
                filterArea.AddFilter(Field.Id, FilterOperator.Equals, User.AreaId.Value);
            }

            powerPlants = await RepoPowerPlant.FindAllAsync(filterPowerPlan);
            areas = await RepoArea.FindAllAsync(new List<SortQuery>() { new SortQuery(Field.Name, SortOrder.Ascending) }, filterArea);

            if(areas != null && areas.Any() && powerPlants != null && powerPlants.Any())
            {
                areas = areas.FindAll(a => powerPlants.Any(b => b.AreaId == a.Id));
            }

            if (areas != null && areas.Any())
            {
                areas = areas.Where(n => n.PowerPlants.Any(m => m.SOType == SOType.ZPGE.ToString())).ToList();
                foreach(Area a in areas)
                {
                    models.Add(new SelectListItem { Value = a.Id.ToString(), Text = a.Name});
                }
            }

            return JsonConvert.SerializeObject(models);
        }

        [HttpPost]
        public async Task<string> GetTaskReportByParam(int areaId, DateTime startDate, DateTime endDate)
        {
            List<MonitoringPresentationStub> monitoringPresentationStubs = new List<MonitoringPresentationStub>();
            FilterQuery filterQuery = new FilterQuery();
            filterQuery.AddFilter(Field.AreaId,FilterOperator.Equals,areaId);
            filterQuery.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
            filterQuery.AddFilter(Field.Periode, FilterOperator.LessThanOrEquals, endDate);
            List<TaskReport> taskReports = null;

            taskReports = await RepoTaskReport.FindAllAsync(filterQuery);

            for(DateTime d = startDate; d <= endDate; d = d.AddDays(1))
            {
                if(taskReports != null && taskReports.Any())
                {
                    TaskReport taskReport = taskReports.FirstOrDefault(a => a.Periode == d);
                    if(taskReport != null)
                    {
                        monitoringPresentationStubs.Add(new MonitoringPresentationStub(taskReport.Id,taskReport.Status));
                    }
                    else
                    {
                        monitoringPresentationStubs.Add(new MonitoringPresentationStub());
                    }
                }
                else
                {
                    monitoringPresentationStubs.Add(new MonitoringPresentationStub());
                }
            }

            return JsonConvert.SerializeObject(monitoringPresentationStubs);
        }
    }
}
