﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class InterfaceScrubberPresentationStub : BasePresentationStub<InterfaceScrubber, InterfaceScrubberPresentationStub>
    {

        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? InterfaceId { get; set; }
        public string Name { get; set; }
        public int? ScrubberId { get; set; }
        public string ScrubberName { get; set; }
        public double? InterfacePressure { get; set; }
        public double? InterfaceTemperature { get; set; }
        public double? ScrubberPressure { get; set; }
        public double? ScrubberTemperature { get; set; }
        public bool HasApproved { get; set; }
        public string InterfacePressureUnit { get; set; }
        public string InterfaceTemperatureUnit { get; set; }
        public string ScrubberPressureUnit { get; set; }
        public string ScrubberTemperatureUnit { get; set; }
        public DateTime Periode { get; set; }
        #endregion

        public InterfaceScrubberPresentationStub() : base()
        {

        }

        public InterfaceScrubberPresentationStub(InterfaceScrubber dbObject) : base(dbObject)
        {
            Name = dbObject.InterfacePoint != null ? dbObject.InterfacePoint.Name : "";
            ScrubberName = dbObject.Scrubber != null ? dbObject.Scrubber.Name : "";
            HasApproved = dbObject.TaskReport.Status == DataStatus.APPROVED.ToString() ? true : false;
            Periode = dbObject.TaskReport.Periode.Value;
            InterfacePressure = dbObject.InterfacePressure != null ? InterfacePressure : 0;
            InterfaceTemperature = dbObject.InterfaceTemperature != null ? InterfaceTemperature : 0;
            ScrubberPressure = dbObject.ScrubberPressure != null ? ScrubberPressure : 0;
            ScrubberTemperature = dbObject.ScrubberTemperature != null ? ScrubberTemperature : 0;

            if (!string.IsNullOrEmpty(InterfacePressureUnit))
            {
                InterfacePressureUnit = ((PressureUnit)Enum.Parse(typeof(PressureUnit), dbObject.InterfacePressureUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(InterfaceTemperatureUnit))
            {
                InterfaceTemperatureUnit = ((TemperatueUnit)Enum.Parse(typeof(TemperatueUnit), dbObject.InterfaceTemperatureUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(ScrubberPressureUnit))
            {
                ScrubberPressureUnit = ((PressureUnit)Enum.Parse(typeof(PressureUnit), dbObject.ScrubberPressureUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(ScrubberTemperatureUnit))
            {
                ScrubberTemperatureUnit = ((TemperatueUnit)Enum.Parse(typeof(TemperatueUnit), dbObject.ScrubberTemperatureUnit)).ToDescription();
            }
        }

        protected override void Init()
        {
        }
    }
}