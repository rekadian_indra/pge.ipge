﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class InterfaceScrubberImportFormStub : BaseFormStub<InterfaceScrubber, InterfaceScrubberImportFormStub>
    {
        public int? TaskReportId { get; set; }
        public int? InterfaceId { get; set; }
        public int? ScrubberId { get; set; }
        public double? InterfacePressure { get; set; }
        public double? InterfaceTemperature { get; set; }
        public double? ScrubberPressure { get; set; }
        public double? ScrubberTemperature { get; set; }
        public string InterfacePressureUnit { get; set; }
        public string InterfaceTemperatureUnit { get; set; }
        public string ScrubberPressureUnit { get; set; }
        public string ScrubberTemperatureUnit { get; set; }


        public InterfaceScrubberImportFormStub() : base()
        {

        }

        public override void MapDbObject(InterfaceScrubber dbObject)
        {
            base.MapDbObject(dbObject);

            if (InterfacePressure == null) { dbObject.InterfacePressure = 0; }
            if (InterfaceTemperature == null) { dbObject.InterfaceTemperature = 0; }
            if (ScrubberPressure == null) { dbObject.ScrubberPressure = 0; }
            if (ScrubberTemperature == null) { dbObject.ScrubberTemperature = 0; }
        }

        public void UpdateDboObject(InterfaceScrubber dbObject)
        {
            if (InterfacePressure == null) { InterfacePressure = 0; }
            if (InterfaceTemperature == null) { InterfaceTemperature = 0; }
            if (ScrubberPressure == null) { ScrubberPressure = 0; }
            if (ScrubberTemperature == null) { ScrubberTemperature = 0; }

            dbObject.InterfacePressure = InterfacePressure;
            dbObject.InterfaceTemperature = InterfaceTemperature;
            dbObject.ScrubberPressure = ScrubberPressure;
            dbObject.ScrubberTemperature = ScrubberTemperature;
            dbObject.InterfacePressureUnit = InterfacePressureUnit;
            dbObject.InterfaceTemperatureUnit = InterfaceTemperatureUnit;
            dbObject.ScrubberPressureUnit = ScrubberPressureUnit;
            dbObject.ScrubberTemperatureUnit = ScrubberTemperatureUnit;

            dbObject.ModifiedBy = User.UserName;
            dbObject.ModifiedDateTimeUtc = DateTime.Now;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}