﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class InterfaceScrubberFormStub : BaseFormStub<InterfaceScrubber, InterfaceScrubberFormStub>
    {
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int AreaId { get; set; }
        public DateTime Periode { get; set; }

        [Display(Name = "Interface Point")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? InterfaceId { get; set; }

        [Display(Name = "Scrubber")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? ScrubberId { get; set; }

        [Display(Name = "Tekanan Interface Point")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? InterfacePressure { get; set; }

        [Display(Name = "Suhu Interface Point")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? InterfaceTemperature { get; set; }

        [Display(Name = "Tekanan Scrubber")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? ScrubberPressure { get; set; }

        [Display(Name = "Suhu Scrubber")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? ScrubberTemperature { get; set; }
        [Display(Name = "Unit")]
        public string InterfacePressureUnit { get; set; }
        [Display(Name = "Unit")]
        public string InterfaceTemperatureUnit { get; set; }
        [Display(Name = "Unit")]
        public string ScrubberPressureUnit { get; set; }
        [Display(Name = "Unit")]
        public string ScrubberTemperatureUnit { get; set; }


        public InterfaceScrubberFormStub() : base()
        {

        }

        public InterfaceScrubberFormStub(InterfaceScrubber dbObject) : base(dbObject)
        {
            AreaId = dbObject.TaskReport.AreaId.Value;
        }

        public override void MapDbObject(InterfaceScrubber dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}