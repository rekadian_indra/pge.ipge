﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using NPOI;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class TaskReportFormStub : BaseFormStub<TaskReport, TaskReportFormStub>
    {
        public int Id { get; set; }

        [DisplayName("Area")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? AreaId { get; set; }
        public DateTime? Periode { get; set; }
        public string AreaName { get; set; }
        [Display(Name = "Source Input")]
        public string SourceInput { get; set; }
        public string Status { get; set; }
        public string ApprovedBy { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }
        public bool HasApproved { get; set; }

        public TaskReportFormStub() : base() {
        }
        public TaskReportFormStub(TaskReport dbItem) : base(dbItem)
        {           
        }
        public override void MapDbObject(TaskReport dbObject)
        {            
            base.MapDbObject(dbObject);
            //TODO: Manual mapping object here
        }

        public void ApproveDbObject(TaskReport dbObject)
        {
            dbObject.Status = DataStatus.APPROVED.ToString();
            dbObject.ApprovedBy = User.UserName;
            dbObject.ModifiedBy = User.UserName;
            dbObject.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();
        }

        public void UpdateDbObject(TaskReport dbObject)
        {
            dbObject.Status = Status;
            dbObject.ModifiedBy = User.UserName;
            dbObject.ModifiedDateTimeUtc = DateTime.Now;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}