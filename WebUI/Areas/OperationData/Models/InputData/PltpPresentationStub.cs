﻿using Business.Entities;
using Common.Enums;
using System;

namespace WebUI.Models
{
    public class PltpPresentationStub : BasePresentationStub<Pltp, PltpPresentationStub>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? TaskReportId { get; set; }
        public int? PowerPlantId { get; set; }
        public double? EnergyGross { get; set; }
        public double? EnergyNett { get; set; }
        public double? SteamUtilization { get; set; }
        public double? WasteRockMufller { get; set; }
        public double? Ssc { get; set; }
        public double? GppsShutdown { get; set; }
        public double? ImportEnergy { get; set; }
        public double? SgssShutdown { get; set; }
        public string Notes { get; set; }
        public bool IsDeleted { get; set; }
        public bool HasApproved { get; set; }
        public string EnergyGrossUnit { get; set; }
        public string EnergyNettUnit { get; set; }
        public string SteamUtilizationUnit { get; set; }
        public string WasteRockMufllerUnit { get; set; }
        public string SscUnit { get; set; }
        public string GppsShutdownUnit { get; set; }
        public string ImportEnergyUnit { get; set; }
        public string SgssShutdownUnit { get; set; }
        public DateTime Periode { get; set; }
        public String SOType { get; set; }

        public bool CanEdit { get; set; }

        public PltpPresentationStub() : base()
        {
        }
        public PltpPresentationStub(Pltp dbObject) : base(dbObject)
        {
            Name = dbObject.PowerPlant.Name;
            HasApproved = dbObject.TaskReport.Status == DataStatus.APPROVED.ToString() ? true : false;

            EnergyGross = dbObject.EnergyGross != null ? dbObject.EnergyGross : 0;
            EnergyNett = dbObject.EnergyNett != null ? dbObject.EnergyNett : 0;

            SteamUtilization = dbObject.SteamUtilization != null ? dbObject.SteamUtilization : 0;
            WasteRockMufller = dbObject.WasteRockMufller != null ? dbObject.WasteRockMufller : 0;
            Ssc = dbObject.Ssc != null ? dbObject.Ssc : 0;
            GppsShutdown = dbObject.GppsShutdown != null ? dbObject.GppsShutdown : 0;
            ImportEnergy = dbObject.ImportEnergy != null ? dbObject.ImportEnergy : 0;
            SgssShutdown = dbObject.SgssShutdown != null ? dbObject.SgssShutdown : 0;

            if (!string.IsNullOrEmpty(EnergyGrossUnit))
            {
                try
                {
                    EnergyGrossUnit = ((EnergyUnit)Enum.Parse(typeof(EnergyUnit), dbObject.EnergyGrossUnit)).ToDescription();
                }
                catch (Exception)
                {
                    EnergyGrossUnit = dbObject.EnergyGrossUnit;
                }
            }
            if (!string.IsNullOrEmpty(EnergyNettUnit))
            {
                try
                {
                    EnergyNettUnit = ((EnergyUnit)Enum.Parse(typeof(EnergyUnit), dbObject.EnergyNettUnit)).ToDescription();
                }
                catch (Exception)
                {
                    EnergyNettUnit = dbObject.EnergyNettUnit;
                }
            }
            if (!string.IsNullOrEmpty(SteamUtilizationUnit))
            {
                SteamUtilizationUnit = ((RockMufflerUnit)Enum.Parse(typeof(RockMufflerUnit), dbObject.SteamUtilizationUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(WasteRockMufllerUnit))
            {
                WasteRockMufllerUnit = ((RockMufflerUnit)Enum.Parse(typeof(RockMufflerUnit), dbObject.WasteRockMufllerUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(SscUnit))
            {
                SscUnit = ((OtherUnit)Enum.Parse(typeof(OtherUnit), dbObject.SscUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(GppsShutdownUnit))
            {
                GppsShutdownUnit = ((OtherUnit)Enum.Parse(typeof(OtherUnit), dbObject.GppsShutdownUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(ImportEnergyUnit))
            {
                ImportEnergyUnit = ((EnergyUnit)Enum.Parse(typeof(EnergyUnit), dbObject.ImportEnergyUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(SgssShutdownUnit))
            {
                SgssShutdownUnit = ((OtherUnit)Enum.Parse(typeof(OtherUnit), dbObject.SgssShutdownUnit)).ToDescription();
            }

            Periode = dbObject.TaskReport.Periode.Value;
            SOType = dbObject.PowerPlant.SOType;
            CanEdit = false;
        }

        public void SetCanEdit(SalesOrder salesOrder)
        {
            //cek has sales order or no
            if (salesOrder != null)
            {
                if (salesOrder.Workflow.Code == Common.Enums.Workflow.DRAFT.ToString())
                {
                    CanEdit = true;
                }
            }
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}