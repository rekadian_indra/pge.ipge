﻿using Business.Entities;
using Common.Enums;
using System;

namespace WebUI.Models
{
    public class DkpPresentationStub : BasePresentationStub<Dkp, DkpPresentationStub>
    {
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? PowerPlantId { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public TimeSpan? StartHour { get; set; }
        public TimeSpan? EndHour { get; set; }
        public double? Derating { get; set; }
        public string Location { get; set; }
        public string Cause { get; set; }
        public string Notes { get; set; }       
        public bool IsDeleted { get; set; }
        public bool HasApproved { get; set; }
        public DateTime Periode { get; set; }


        public DkpPresentationStub() : base() {
        }
        public DkpPresentationStub(Dkp dbObject) : base(dbObject)
        {
            Name = dbObject.PowerPlant.Name;
            try
            {
                Status = ((InterruptionStatus)Enum.Parse(typeof(InterruptionStatus), dbObject.Status)).ToDescription();
            }
            catch
            {
                Status = dbObject.Status;
            }

            try
            {
                Location = ((InterruptionLocation)Enum.Parse(typeof(InterruptionLocation), dbObject.Location)).ToDescription();
            }
            catch
            {
                Location = dbObject.Location;
            }
            HasApproved = dbObject.TaskReport.Status == DataStatus.APPROVED.ToString() ? true : false;
            Periode = dbObject.TaskReport.Periode.Value;
            Derating = dbObject.Derating;
        }
        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}