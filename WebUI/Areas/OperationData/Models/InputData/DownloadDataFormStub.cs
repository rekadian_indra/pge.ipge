﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Areas.OperationData.Models.InputData
{
    public class DownloadDataFormStub
    {
       
        [DisplayName("Area")]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int AreaId { get; set; }
        [DisplayName("From ")]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public DateTime StartDate { get; set; }
        [DisplayName("To ")]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public DateTime EndDate { get; set; }
    }
}