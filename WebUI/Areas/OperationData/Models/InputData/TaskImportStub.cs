﻿using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using NPOI;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.OperationData.Models;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class TaskImportStub : BaseImportStub
    {
        public TaskReportFormStub TaskReportStub { get; set; }
        public List<PltpImportFormStub> Pltpls { get; set; }
        public List<DkpImportFormStub> Dkps { get; set; }
        public List<ProductionImportFormStub> Productions { get; set; }
        public List<ReinjectionImportFormStub> Reinjections { get; set; }
        public List<MonitoringImportFormStub> Monitorings { get; set; }
        public List<SeparatorImportFormStub> Separators { get; set; }
        public List<InterfaceScrubberImportFormStub> InterfaceScrubbers { get; set; }
        public List<ActualPlanningImportFormStub> ActualPlannings { get; set; }

        public int InvalidProductionRow { get; set; }

        public bool IsOldTemplate { get; set; }

        public TaskImportStub() : base()
        {

        }

        public static byte[] GenerateTemplate(TaskParamStub param)
        {
            //culture
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US"); //supaya file tidak corrupt
            int parseRecordNumber = 1000;
            int subHeaderIdx = 1;
            int startRowIndex = 2;
            int colNumber = 0; int rowNumber = 0;
            int i = 0,
                j = 3; //first row data master

            const string SHEET_PASSWORD = "rekadia123!";
            int countData = 0;
            //XSSFCellStyle styleDate;
            XSSFCellStyle styleNumeric;
            XSSFCellStyle styleDecimal;

            //kamus
            IWorkbook workbook = new XSSFWorkbook();
            ICellStyle styleTimeSpan = workbook.CreateCellStyle();
            ICellStyle styleDate = workbook.CreateCellStyle();
            IDataFormat dataFormat = workbook.CreateDataFormat();
            XSSFSheet sheet; XSSFRow row = null; XSSFCell cell = null;
            XSSFCellStyle style;
            //XSSFFont font;
            ICellStyle boldStyle = workbook.CreateCellStyle();
            IFont fontBold = workbook.CreateFont();

            IEnumerable<InterruptionStatus> interruptionStatuses = EnumExtension.EnumToList<InterruptionStatus>();
            IEnumerable<DrawWellSubStatus> wellStatutes = EnumExtension.EnumToList<DrawWellSubStatus>();
            IEnumerable<InterruptionLocation> locations = EnumExtension.EnumToList<InterruptionLocation>();
            IEnumerable<EnergyUnit> energyUnits = EnumExtension.EnumToList<EnergyUnit>();
            IEnumerable<TemperatueUnit> temperatueUnits = EnumExtension.EnumToList<TemperatueUnit>();
            IEnumerable<FlowUnit> flowUnits = EnumExtension.EnumToList<FlowUnit>();
            IEnumerable<RockMufflerUnit> rockMufflerUnits = EnumExtension.EnumToList<RockMufflerUnit>();
            IEnumerable<PressureUnit> pressureUnits = EnumExtension.EnumToList<PressureUnit>();
            IEnumerable<Fluida> fluidas = EnumExtension.EnumToList<Fluida>();

            List<string> timeSpans = TimeSpanHelper.GenerateTimeSpan();

            //set bold style
            fontBold.Boldweight = (short)FontBoldWeight.Bold;
            boldStyle.SetFont(fontBold);

            CellRangeAddressList addressList; XSSFDataValidationHelper dvHelper; XSSFDataValidationConstraint dvConstraint; XSSFDataValidation validation;

            styleNumeric = (XSSFCellStyle)workbook.CreateCellStyle();
            styleNumeric.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0");

            styleDecimal = (XSSFCellStyle)workbook.CreateCellStyle();
            styleDecimal.DataFormat = workbook.CreateDataFormat().GetFormat("0.00");

            //styleDate = (XSSFCellStyle)workbook.CreateCellStyle();
            //styleDate.DataFormat = workbook.CreateDataFormat().GetFormat("mm/dd/yyyy");
            styleDate.DataFormat = dataFormat.GetFormat(DisplayFormat.SqlDateFormat);
            styleTimeSpan.DataFormat = dataFormat.GetFormat(DisplayFormat.SqlShortTimeFormat);

            IEnumerable<SheetExcel> worksheets = EnumExtension.EnumToList<SheetExcel>();

            foreach (var ws in worksheets)
            {
                switch (ws)
                {
                    case SheetExcel.MASTER:
                        sheet = (XSSFSheet)workbook.CreateSheet(ws.ToString());

                        row = (XSSFRow)sheet.CreateRow(0);
                        cell = (XSSFCell)row.CreateCell(0);
                        cell.SetCellValue(param.TaskReport.Periode.Value.Date.ToString());
                        cell.CellStyle = styleDate;

                        row = (XSSFRow)sheet.CreateRow(1);
                        cell = (XSSFCell)row.CreateCell(0);
                        cell.SetCellValue(param.TaskReport.AreaId.Value.ToString());

                        row = (XSSFRow)sheet.CreateRow(2);
                        cell = (XSSFCell)row.CreateCell(0);
                        cell.SetCellValue("POWER PLAN");
                        cell.CellStyle = boldStyle;

                        //master header
                        if (sheet.GetRow(2) == null)
                        {
                            row = (XSSFRow)sheet.CreateRow(2);
                        }
                        else
                        {
                            row = (XSSFRow)sheet.GetRow(2);
                        }

                        cell = (XSSFCell)row.CreateCell(1);
                        cell.SetCellValue("STATUS GANGGUAN");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(2);
                        cell.SetCellValue("SUMUR");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(3);
                        cell.SetCellValue("STATUS SUMUR");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(4);
                        cell.SetCellValue("SEPARATOR");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(5);
                        cell.SetCellValue("INTERFACE POINT");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(6);
                        cell.SetCellValue("SCRUBBER");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(7);
                        cell.SetCellValue("AREA");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(8);
                        cell.SetCellValue("TIME SPAN");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(9);
                        cell.SetCellValue("LOKASI GANGGUAN");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(10);
                        cell.SetCellValue("SATUAN ENERGI");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(11);
                        cell.SetCellValue("SATUAN SUHU");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(12);
                        cell.SetCellValue("SATUAN SUHU");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(13);
                        cell.SetCellValue("SATUAN ROCK MUFFLER");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(14);
                        cell.SetCellValue("SATUAN TEKANAN");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(15);
                        cell.SetCellValue("SUMUR PRODUKSI");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(16);
                        cell.SetCellValue("SUMUR INJEKSI");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(17);
                        cell.SetCellValue("SUMUR MONITORING");
                        cell.CellStyle = boldStyle;

                        cell = (XSSFCell)row.CreateCell(18);
                        cell.SetCellValue("FLUIDA");
                        cell.CellStyle = boldStyle;

                        //master data
                        i = 3;
                        foreach (Business.Entities.PowerPlant pw in param.PowerPlants)
                        {
                            row = (XSSFRow)sheet.CreateRow(i);
                            cell = (XSSFCell)row.CreateCell(0);
                            cell.SetCellValue(pw.Name);
                            i++;
                        }

                        i = 3;
                        foreach (var ins in interruptionStatuses)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(1);
                            cell.SetCellValue(ins.ToDescription());

                            i++;
                        }

                        i = 3;
                        foreach (Well w in param.Wells)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(2);
                            cell.SetCellValue(w.Name);

                            i++;
                        }

                        i = 3;
                        foreach (var s in wellStatutes)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(3);
                            cell.SetCellValue(s.ToDescription());

                            i++;
                        }

                        i = 3;
                        foreach (MstSeparator m in param.Separators)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(4);
                            cell.SetCellValue(m.Name);

                            i++;
                        }

                        i = 3;
                        foreach (InterfacePoint m in param.InterfacePoints)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(5);
                            cell.SetCellValue(m.Name);

                            i++;
                        }

                        i = 3;
                        foreach (Scrubber m in param.Scrubbers)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(6);
                            cell.SetCellValue(m.Name);

                            i++;
                        }

                        i = 3;
                        if (sheet.GetRow(i) == null)
                        {
                            row = (XSSFRow)sheet.CreateRow(i);
                        }
                        else
                        {
                            row = (XSSFRow)sheet.GetRow(i);
                        }

                        cell = (XSSFCell)row.CreateCell(7);
                        cell.SetCellValue(param.TaskReport.AreaName);

                        i = 3;
                        foreach (var t in timeSpans)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(8);
                            cell.SetCellValue(t);

                            i++;
                        }

                        i = 3;
                        foreach (var l in locations)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(9);
                            cell.SetCellValue(l.ToDescription());

                            i++;
                        }

                        i = 3;
                        foreach (var u in energyUnits)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(10);
                            cell.SetCellValue(u.ToDescription());

                            i++;
                        }

                        i = 3;
                        foreach (var u in temperatueUnits)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(11);
                            cell.SetCellValue(u.ToDescription());

                            i++;
                        }

                        i = 3;
                        foreach (var u in flowUnits)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(12);
                            cell.SetCellValue(u.ToDescription());

                            i++;
                        }

                        i = 3;
                        foreach (var u in rockMufflerUnits)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(13);
                            cell.SetCellValue(u.ToDescription());

                            i++;
                        }

                        i = 3;
                        foreach (var u in pressureUnits)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(14);
                            cell.SetCellValue(u.ToDescription());

                            i++;
                        }

                        i = 3;
                        foreach (var u in param.ProductionWells)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(15);
                            cell.SetCellValue(u.WellName);

                            i++;
                        }

                        i = 3;
                        foreach (var u in param.InjectionWells)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(16);
                            cell.SetCellValue(u.WellName);

                            i++;
                        }

                        i = 3;
                        foreach (var u in param.MonitoringWells)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(17);
                            cell.SetCellValue(u.WellName);

                            i++;
                        }

                        i = 3;
                        foreach (var u in fluidas)
                        {
                            if (sheet.GetRow(i) == null)
                            {
                                row = (XSSFRow)sheet.CreateRow(i);
                            }
                            else
                            {
                                row = (XSSFRow)sheet.GetRow(i);
                            }

                            cell = (XSSFCell)row.CreateCell(18);
                            cell.SetCellValue(u.ToDescription());

                            i++;
                        }
                        sheet.ProtectSheet(SHEET_PASSWORD);

                        break;
                    case SheetExcel.PLTP:
                        IEnumerable<PltpExcelColumn> pltColumns = EnumExtension.EnumToList<PltpExcelColumn>();
                        colNumber = 0; rowNumber = 0;
                        sheet = (XSSFSheet)workbook.CreateSheet(ws.ToString());
                        dvHelper = new XSSFDataValidationHelper(sheet);


                        row = (XSSFRow)sheet.CreateRow(rowNumber);
                        foreach (var header in pltColumns)
                        {
                            cell = (XSSFCell)row.CreateCell(colNumber);

                            if (header.ToDescription() == PltpExcelColumn.POWERPLANT.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }
                            else if (header.ToDescription() == PltpExcelColumn.ENERGY_GROSS.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5100);
                            }
                            else if (header.ToDescription() == PltpExcelColumn.ENERGY_NETT.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5100);
                            }
                            else if (header.ToDescription() == PltpExcelColumn.STEAM_UTILIZATION.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }
                            else if (header.ToDescription() == PltpExcelColumn.WASTE_MUFFLER.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5100);
                            }
                            else if (header.ToDescription() == PltpExcelColumn.SSC.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3500);
                            }
                            else if (header.ToDescription() == PltpExcelColumn.GPP_SHUTDOWN.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3500);
                            }
                            else if (header.ToDescription() == PltpExcelColumn.SGSS_SHUTDOWN.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3500);
                            }
                            else if (header.ToDescription() == PltpExcelColumn.NOTES.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 10000);
                            }

                            style = (XSSFCellStyle)workbook.CreateCellStyle();
                            style.BorderBottom = style.BorderLeft = BorderStyle.Medium;
                            style.WrapText = true;
                            style.Alignment = HorizontalAlignment.Center;
                            style.VerticalAlignment = VerticalAlignment.Center;
                            style.IsLocked = true;
                            //font = (XSSFFont)workbook.CreateFont();
                            //font.FontHeight = 24;
                            //font.Boldweight = (short)FontBoldWeight.Bold;
                            //style.SetFont(font);
                            cell.CellStyle = style;
                            cell.SetCellValue(header.ToDescription());

                            //hide Column SSC in Sheet PLTP
                            if (header.ToDescription() == PltpExcelColumn.SSC.ToDescription())
                            {
                                sheet.SetColumnHidden(colNumber, true);
                            }

                            colNumber++;

                        }

                        row = (XSSFRow)sheet.CreateRow(1);
                        style = (XSSFCellStyle)workbook.CreateCellStyle();
                        style.BorderBottom = BorderStyle.Medium;
                        style.Alignment = HorizontalAlignment.Center;
                        style.VerticalAlignment = VerticalAlignment.Center;

                        cell = (XSSFCell)row.CreateCell(1);
                        cell.SetCellValue(EnergyUnit.KW_HR.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(2);
                        cell.SetCellValue(EnergyUnit.KW_HR.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(3);
                        cell.SetCellValue(RockMufflerUnit.T.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(4);
                        cell.SetCellValue(RockMufflerUnit.USTON.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(5);
                        cell.SetCellValue(OtherUnit.TON_MW_HR.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(5);
                        cell.SetCellValue(OtherUnit.TON_MW_HR.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(6);
                        cell.SetCellValue(OtherUnit.HR.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(7);
                        cell.SetCellValue(EnergyUnit.KW_HR.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(8);
                        cell.SetCellValue(OtherUnit.HR.ToDescription());
                        cell.CellStyle = style;

                        style = (XSSFCellStyle)workbook.CreateCellStyle();
                        style.BorderBottom = BorderStyle.Medium;
                        style.IsLocked = true;

                        cell = (XSSFCell)row.CreateCell(0);
                        cell.CellStyle = style;
                        cell = (XSSFCell)row.CreateCell(9);
                        cell.CellStyle = style;

                        if (param.PowerPlants.Any())
                        {
                            i = 4;
                            countData = param.PowerPlants.Count() == 0 ? i : param.PowerPlants.Count();
                            addressList = new CellRangeAddressList(startRowIndex, parseRecordNumber, 0, 0);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$A$" + i + ":$A$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        //if (energyUnits.Any())
                        //{
                        //    i = 4;
                        //    addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 1, 1);
                        //    dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$K$" + i + ":$K$", energyUnits.Count() + j));
                        //    validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                        //    validation.SuppressDropDownArrow = true;
                        //    validation.ShowErrorBox = true;
                        //    sheet.AddValidationData(validation);

                        //    addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 2, 2);
                        //    dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$K$" + i + ":$K$", energyUnits.Count() + j));
                        //    validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                        //    validation.SuppressDropDownArrow = true;
                        //    validation.ShowErrorBox = true;
                        //    sheet.AddValidationData(validation);

                        //    addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 7, 7);
                        //    dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$K$" + i + ":$K$", energyUnits.Count() + j));
                        //    validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                        //    validation.SuppressDropDownArrow = true;
                        //    validation.ShowErrorBox = true;
                        //    sheet.AddValidationData(validation);
                        //}

                        if (rockMufflerUnits.Any())
                        {
                            i = 4;
                            //addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 3, 3);
                            //dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$N$" + i + ":$N$", rockMufflerUnits.Count() + j));
                            //validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            //validation.SuppressDropDownArrow = true;
                            //validation.ShowErrorBox = true;
                            //sheet.AddValidationData(validation);
                            countData = rockMufflerUnits.Count() == 0 ? i : rockMufflerUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 4, 4);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$N$" + i + ":$N$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        break;
                    case SheetExcel.DKP:
                        IEnumerable<DKPColumnHeader> dkpColumns = EnumExtension.EnumToList<DKPColumnHeader>();
                        colNumber = 0; rowNumber = 0;
                        sheet = (XSSFSheet)workbook.CreateSheet(ws.ToString());
                        dvHelper = new XSSFDataValidationHelper(sheet);

                        row = (XSSFRow)sheet.CreateRow(rowNumber);
                        foreach (var header in dkpColumns)
                        {
                            cell = (XSSFCell)row.CreateCell(colNumber);

                            if (header.ToDescription() == DKPColumnHeader.PLTP.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }
                            else if (header.ToDescription() == DKPColumnHeader.INTERRUPTION_STATUS.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }
                            else if (header.ToDescription() == DKPColumnHeader.INTERRUPTION_START_TIME.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5100);
                            }
                            else if (header.ToDescription() == DKPColumnHeader.INTERRUPTION_FINISH_TIME.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5100);
                            }
                            else if (header.ToDescription() == DKPColumnHeader.DERATING.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3000);
                            }
                            else if (header.ToDescription() == DKPColumnHeader.INTERRUPTION_LOCATION.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }
                            else if (header.ToDescription() == DKPColumnHeader.INTERRUPTION_CAUSE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 8000);
                            }
                            else if (header.ToDescription() == DKPColumnHeader.NOTES.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 10000);
                            }

                            style = (XSSFCellStyle)workbook.CreateCellStyle();
                            style.BorderBottom = style.BorderLeft = style.BorderRight = BorderStyle.Medium;
                            style.WrapText = true;
                            style.Alignment = HorizontalAlignment.Center;
                            style.VerticalAlignment = VerticalAlignment.Center;
                            cell.CellStyle = style;
                            cell.SetCellValue(header.ToDescription());

                            colNumber++;
                        }

                        if (param.PowerPlants.Any())
                        {
                            i = 4;
                            countData = param.PowerPlants.Count() == 0 ? i : param.PowerPlants.Count();
                            addressList = new CellRangeAddressList(1, parseRecordNumber, 0, 0);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$A$" + i + ":$A$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (interruptionStatuses.Any())
                        {
                            i = 4;
                            countData = interruptionStatuses.Count() == 0 ? i : interruptionStatuses.Count();
                            addressList = new CellRangeAddressList(1, parseRecordNumber, 1, 1);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$B$" + i + ":$B$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (timeSpans.Any())
                        {
                            i = 4;
                            countData = timeSpans.Count() == 0 ? i : timeSpans.Count();
                            addressList = new CellRangeAddressList(1, parseRecordNumber, 2, 2);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$I$" + i + ":$I$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);

                            addressList = new CellRangeAddressList(1, parseRecordNumber, 3, 3);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$I$" + i + ":$I$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);

                        }

                        if (locations.Any())
                        {
                            i = 4;
                            countData = locations.Count() == 0 ? i : locations.Count();
                            addressList = new CellRangeAddressList(1, parseRecordNumber, 5, 5);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$J$" + i + ":$J$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        break;
                    case SheetExcel.PRODUKSI:
                        IEnumerable<ProductionColumnHeader> prodColumns = EnumExtension.EnumToList<ProductionColumnHeader>();
                        colNumber = 0; rowNumber = 0;
                        sheet = (XSSFSheet)workbook.CreateSheet(ws.ToString());
                        dvHelper = new XSSFDataValidationHelper(sheet);

                        row = (XSSFRow)sheet.CreateRow(rowNumber);
                        foreach (var header in prodColumns)
                        {
                            cell = (XSSFCell)row.CreateCell(colNumber);

                            if (header.ToDescription() == ProductionColumnHeader.WELL.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4000);
                            }
                            else if (header.ToDescription() == ProductionColumnHeader.TKS.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 2000);
                            }
                            else if (header.ToDescription() == ProductionColumnHeader.LINE_PRESSURE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3000);
                            }
                            else if (header.ToDescription() == ProductionColumnHeader.LINE_TEMPERATURE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3000);
                            }
                            else if (header.ToDescription() == ProductionColumnHeader.TRHOTTLE_VALVE_OPENING.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5100);
                            }
                            else if (header.ToDescription() == ProductionColumnHeader.FLOW_AVERAGE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3000);
                            }
                            else if (header.ToDescription() == ProductionColumnHeader.FLOW_TOTAL.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4100);
                            }
                            else if (header.ToDescription() == ProductionColumnHeader.PRESSURE_UPSTREAM.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4100);
                            }
                            else if (header.ToDescription() == ProductionColumnHeader.DIFFERENTIAL_PRESSURE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }
                            else if (header.ToDescription() == ProductionColumnHeader.WELL_STATUS.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3300);
                            }
                            else if (header.ToDescription() == ProductionColumnHeader.NOTES.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 8000);
                            }

                            style = (XSSFCellStyle)workbook.CreateCellStyle();
                            style.BorderBottom = style.BorderLeft = style.BorderRight = BorderStyle.Medium;
                            style.WrapText = true;
                            style.Alignment = HorizontalAlignment.Center;
                            style.VerticalAlignment = VerticalAlignment.Center;
                            cell.CellStyle = style;
                            cell.SetCellValue(header.ToDescription());


                            colNumber++;
                        }

                        row = (XSSFRow)sheet.CreateRow(1);
                        style = (XSSFCellStyle)workbook.CreateCellStyle();
                        style.BorderBottom = BorderStyle.Medium;
                        style.Alignment = HorizontalAlignment.Center;
                        style.VerticalAlignment = VerticalAlignment.Center;

                        cell = (XSSFCell)row.CreateCell(1);
                        cell.SetCellValue(PressureUnit.BARG.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(2);
                        cell.SetCellValue(PressureUnit.BARG.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(3);
                        cell.SetCellValue(TemperatueUnit.DEG_C.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(4);
                        cell.SetCellValue(OtherUnit.PERCENT.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(5);
                        cell.SetCellValue(FlowUnit.T_HR.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(6);
                        cell.SetCellValue(RockMufflerUnit.T.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(7);
                        cell.SetCellValue(PressureUnit.BARG.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(8);
                        cell.SetCellValue(PressureUnit.BARG.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(0);
                        cell.CellStyle = style;
                        cell = (XSSFCell)row.CreateCell(9);
                        cell.CellStyle = style;
                        //cell = (XSSFCell)row.CreateCell(10);
                        //cell.CellStyle = style;

                        if (param.Wells.Any())
                        {
                            i = 4;
                            countData = param.ProductionWells.Count() == 0 ? i : param.ProductionWells.Count();
                            addressList = new CellRangeAddressList(startRowIndex, parseRecordNumber, 0, 0);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$P$" + i + ":$P$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (wellStatutes.Any())
                        {
                            i = 4;
                            countData = wellStatutes.Count() == 0 ? i : wellStatutes.Count();
                            addressList = new CellRangeAddressList(startRowIndex, parseRecordNumber, 9, 9);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$D$" + i + ":$D$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (pressureUnits.Any())
                        {
                            i = 4;
                            countData = pressureUnits.Count() == 0 ? i : pressureUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 1, 1);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$O$" + i + ":$O$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);

                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 2, 2);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$O$" + i + ":$O$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);

                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 7, 7);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$O$" + i + ":$O$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);

                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 8, 8);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$O$" + i + ":$O$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (temperatueUnits.Any())
                        {
                            i = 4;
                            countData = temperatueUnits.Count() == 0 ? i : temperatueUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 3, 3);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$L$" + i + ":$L$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (flowUnits.Any())
                        {
                            i = 4;
                            countData = flowUnits.Count() == 0 ? i : flowUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 5, 5);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$M$" + i + ":$M$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        //sheet.ProtectSheet(SHEET_PASSWORD);

                        break;
                    case SheetExcel.REINJEKSI:
                        IEnumerable<ReinjectionColumnHeader> reinjectionColumns = EnumExtension.EnumToList<ReinjectionColumnHeader>();
                        colNumber = 0; rowNumber = 0;
                        sheet = (XSSFSheet)workbook.CreateSheet(ws.ToString());
                        dvHelper = new XSSFDataValidationHelper(sheet);

                        row = (XSSFRow)sheet.CreateRow(rowNumber);
                        foreach (var header in reinjectionColumns)
                        {
                            cell = (XSSFCell)row.CreateCell(colNumber);

                            if (header.ToDescription() == ReinjectionColumnHeader.WELL.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4000);
                            }
                            else if (header.ToDescription() == ReinjectionColumnHeader.TKS.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 2000);
                            }
                            else if (header.ToDescription() == ReinjectionColumnHeader.LINE_PRESSURE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3000);
                            }
                            else if (header.ToDescription() == ReinjectionColumnHeader.LINE_TEMPERATURE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3000);
                            }
                            else if (header.ToDescription() == ReinjectionColumnHeader.PH.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 2000);
                            }
                            else if (header.ToDescription() == ReinjectionColumnHeader.FLOW_AVERAGE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3000);
                            }
                            else if (header.ToDescription() == ReinjectionColumnHeader.FLOW_TOTAL.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3100);
                            }
                            else if (header.ToDescription() == ReinjectionColumnHeader.INJECTED_FLUID.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5500);
                            }
                            else if (header.ToDescription() == ReinjectionColumnHeader.WELL_STATUS.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4000);
                            }
                            else if (header.ToDescription() == ReinjectionColumnHeader.NOTES.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 8000);
                            }

                            style = (XSSFCellStyle)workbook.CreateCellStyle();
                            style.BorderBottom = style.BorderLeft = style.BorderRight = BorderStyle.Medium;
                            style.WrapText = true;
                            style.Alignment = HorizontalAlignment.Center;
                            style.VerticalAlignment = VerticalAlignment.Center;
                            cell.CellStyle = style;
                            cell.SetCellValue(header.ToDescription());

                            colNumber++;
                        }

                        row = (XSSFRow)sheet.CreateRow(1);
                        style = (XSSFCellStyle)workbook.CreateCellStyle();
                        style.BorderBottom = BorderStyle.Medium;
                        style.Alignment = HorizontalAlignment.Center;
                        style.VerticalAlignment = VerticalAlignment.Center;

                        cell = (XSSFCell)row.CreateCell(1);
                        cell.SetCellValue(PressureUnit.BARG.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(2);
                        cell.SetCellValue(PressureUnit.BARG.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(3);
                        cell.SetCellValue(TemperatueUnit.DEG_C.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(5);
                        cell.SetCellValue(FlowUnit.T_HR.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(6);
                        cell.SetCellValue(RockMufflerUnit.T.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(0);
                        cell.CellStyle = style;
                        cell = (XSSFCell)row.CreateCell(4);
                        cell.CellStyle = style;
                        cell = (XSSFCell)row.CreateCell(7);
                        cell.CellStyle = style;
                        cell = (XSSFCell)row.CreateCell(8);
                        cell.CellStyle = style;
                        //cell = (XSSFCell)row.CreateCell(9);
                        //cell.CellStyle = style;

                        if (param.Wells.Any())
                        {
                            i = 4;
                            countData = param.InjectionWells.Count() == 0 ? i : param.InjectionWells.Count();
                            addressList = new CellRangeAddressList(startRowIndex, parseRecordNumber, 0, 0);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$Q$" + i + ":$Q$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (wellStatutes.Any())
                        {
                            i = 4;
                            countData = wellStatutes.Count() == 0 ? i : wellStatutes.Count();
                            addressList = new CellRangeAddressList(startRowIndex, parseRecordNumber, 8, 8);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$D$" + i + ":$D$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (pressureUnits.Any())
                        {
                            i = 4;
                            countData = pressureUnits.Count() == 0 ? i : pressureUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 1, 1);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$O$" + i + ":$O$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);

                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 2, 2);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$O$" + i + ":$O$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (temperatueUnits.Any())
                        {
                            i = 4;
                            countData = temperatueUnits.Count() == 0 ? i : temperatueUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 3, 3);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$L$" + i + ":$L$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (flowUnits.Any())
                        {
                            i = 4;
                            countData = flowUnits.Count() == 0 ? i : flowUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 5, 5);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$M$" + i + ":$M$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (fluidas.Any())
                        {
                            i = 4;
                            countData = fluidas.Count() == 0 ? i : fluidas.Count();
                            addressList = new CellRangeAddressList(startRowIndex, parseRecordNumber, 7, 7);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$S$" + i + ":$S$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        //sheet.ProtectSheet(SHEET_PASSWORD);

                        break;
                    case SheetExcel.MONITORING:
                        IEnumerable<MonitoringColumnHeader> monitoringColumns = EnumExtension.EnumToList<MonitoringColumnHeader>();
                        colNumber = 0; rowNumber = 0;
                        sheet = (XSSFSheet)workbook.CreateSheet(ws.ToString());
                        dvHelper = new XSSFDataValidationHelper(sheet);

                        row = (XSSFRow)sheet.CreateRow(rowNumber);
                        foreach (var header in monitoringColumns)
                        {
                            cell = (XSSFCell)row.CreateCell(colNumber);

                            if (header.ToDescription() == MonitoringColumnHeader.WELL.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4000);
                            }
                            else if (header.ToDescription() == MonitoringColumnHeader.TKS.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3000);
                            }
                            else if (header.ToDescription() == MonitoringColumnHeader.WELL_STATUS.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4000);
                            }
                            else if (header.ToDescription() == MonitoringColumnHeader.NOTES.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 8000);
                            }

                            style = (XSSFCellStyle)workbook.CreateCellStyle();
                            style.BorderBottom = style.BorderLeft = style.BorderRight = BorderStyle.Medium;
                            style.WrapText = true;
                            style.Alignment = HorizontalAlignment.Center;
                            style.VerticalAlignment = VerticalAlignment.Center;
                            cell.CellStyle = style;
                            cell.SetCellValue(header.ToDescription());

                            colNumber++;
                        }

                        row = (XSSFRow)sheet.CreateRow(1);
                        style = (XSSFCellStyle)workbook.CreateCellStyle();
                        style.BorderBottom = BorderStyle.Medium;
                        style.Alignment = HorizontalAlignment.Center;
                        style.VerticalAlignment = VerticalAlignment.Center;

                        cell = (XSSFCell)row.CreateCell(1);
                        cell.SetCellValue(PressureUnit.BARG.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(0);
                        cell.CellStyle = style;
                        cell = (XSSFCell)row.CreateCell(2);
                        cell.CellStyle = style;
                        //cell = (XSSFCell)row.CreateCell(3);
                        //cell.CellStyle = style;

                        if (param.Wells.Any())
                        {
                            i = 4;
                            countData = param.MonitoringWells.Count() == 0 ? i : param.MonitoringWells.Count();
                            addressList = new CellRangeAddressList(startRowIndex, parseRecordNumber, 0, 0);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$R$" + i + ":$R$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }


                        if (wellStatutes.Any())
                        {
                            var monitorintStatus = wellStatutes.Where(n => n != DrawWellSubStatus.OPERATION);
                            i = 4;
                            countData = monitorintStatus.Count() == 0 ? i : monitorintStatus.Count();
                            addressList = new CellRangeAddressList(startRowIndex, parseRecordNumber, 2, 2);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$D$" + i + ":$D$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (pressureUnits.Any())
                        {
                            i = 4;
                            countData = pressureUnits.Count() == 0 ? i : pressureUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 1, 1);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$O$" + i + ":$O$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        //sheet.ProtectSheet(SHEET_PASSWORD);

                        break;
                    case SheetExcel.SEPARATOR:
                        IEnumerable<SeparatorColumnHeader> separatorColumns = EnumExtension.EnumToList<SeparatorColumnHeader>();
                        colNumber = 0; rowNumber = 0;
                        sheet = (XSSFSheet)workbook.CreateSheet(ws.ToString());
                        dvHelper = new XSSFDataValidationHelper(sheet);

                        row = (XSSFRow)sheet.CreateRow(rowNumber);
                        foreach (var header in separatorColumns)
                        {
                            cell = (XSSFCell)row.CreateCell(colNumber);

                            if (header.ToDescription() == SeparatorColumnHeader.SEPARATOR.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4000);
                            }
                            else if (header.ToDescription() == SeparatorColumnHeader.PRESSURE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3000);
                            }
                            else if (header.ToDescription() == SeparatorColumnHeader.TEMPERATURE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 3000);
                            }
                            else if (header.ToDescription() == SeparatorColumnHeader.LEVEL_BRINE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4000);
                            }
                            else if (header.ToDescription() == SeparatorColumnHeader.THROTTLE_VALVE_OPENING_BRINE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5100);
                            }
                            else if (header.ToDescription() == SeparatorColumnHeader.THROTTLE_VALVE_OPENING_STEAM.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5100);
                            }
                            else if (header.ToDescription() == SeparatorColumnHeader.AVERAGE_STEAM_FLOW.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4100);
                            }
                            else if (header.ToDescription() == SeparatorColumnHeader.AVERAGE_BRINE_FLOW.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4100);
                            }

                            style = (XSSFCellStyle)workbook.CreateCellStyle();
                            style.BorderBottom = style.BorderLeft = style.BorderRight = BorderStyle.Medium;
                            style.WrapText = true;
                            style.Alignment = HorizontalAlignment.Center;
                            style.VerticalAlignment = VerticalAlignment.Center;
                            cell.CellStyle = style;
                            cell.SetCellValue(header.ToDescription());

                            colNumber++;
                        }

                        row = (XSSFRow)sheet.CreateRow(1);
                        style = (XSSFCellStyle)workbook.CreateCellStyle();
                        style.BorderBottom = BorderStyle.Medium;
                        style.Alignment = HorizontalAlignment.Center;
                        style.VerticalAlignment = VerticalAlignment.Center;

                        cell = (XSSFCell)row.CreateCell(0);
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(1);
                        cell.SetCellValue(PressureUnit.BARG.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(2);
                        cell.SetCellValue(TemperatueUnit.DEG_C.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(3);
                        cell.SetCellValue(OtherUnit.PERCENT.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(4);
                        cell.SetCellValue(OtherUnit.PERCENT.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(5);
                        cell.SetCellValue(OtherUnit.PERCENT.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(6);
                        cell.SetCellValue(FlowUnit.T_HR.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(7);
                        cell.SetCellValue(FlowUnit.T_HR.ToDescription());
                        cell.CellStyle = style;


                        if (param.PowerPlants.Any())
                        {
                            i = 4;
                            countData = param.Separators.Count() == 0 ? i : param.Separators.Count();
                            addressList = new CellRangeAddressList(startRowIndex, parseRecordNumber, 0, 0);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$E$" + i + ":$E$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (pressureUnits.Any())
                        {
                            i = 4;
                            countData = pressureUnits.Count() == 0 ? i : pressureUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 1, 1);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$O$" + i + ":$O$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (temperatueUnits.Any())
                        {
                            i = 4;
                            countData = temperatueUnits.Count() == 0 ? i : temperatueUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 2, 2);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$L$" + i + ":$L$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (flowUnits.Any())
                        {
                            i = 4;
                            countData = flowUnits.Count() == 0 ? i : flowUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 6, 6);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$M$" + i + ":$M$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);

                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 7, 7);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$M$" + i + ":$M$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        //sheet.ProtectSheet(SHEET_PASSWORD);

                        break;
                    case SheetExcel.INTERFACE_SCRUBBER:
                        IEnumerable<InterfaceScrubberColumnHeader> interfaceScrubberColumns = EnumExtension.EnumToList<InterfaceScrubberColumnHeader>();
                        colNumber = 0; rowNumber = 0;
                        sheet = (XSSFSheet)workbook.CreateSheet(ws.ToString());
                        dvHelper = new XSSFDataValidationHelper(sheet);

                        row = (XSSFRow)sheet.CreateRow(rowNumber);
                        foreach (var header in interfaceScrubberColumns)
                        {
                            cell = (XSSFCell)row.CreateCell(colNumber);

                            if (header.ToDescription() == InterfaceScrubberColumnHeader.INTERFACE_POINT.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }
                            else if (header.ToDescription() == InterfaceScrubberColumnHeader.INTERFACE_POINT_PRESSURE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }
                            else if (header.ToDescription() == InterfaceScrubberColumnHeader.INTERFACE_POINT_PRESSURE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }
                            else if (header.ToDescription() == InterfaceScrubberColumnHeader.SCRUBBER.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }
                            else if (header.ToDescription() == InterfaceScrubberColumnHeader.SCRUBBER_PRESSURE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }
                            else if (header.ToDescription() == InterfaceScrubberColumnHeader.SCRUBBER_TEMPERATURE.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 5000);
                            }

                            style = (XSSFCellStyle)workbook.CreateCellStyle();
                            style.BorderBottom = style.BorderLeft = style.BorderRight = BorderStyle.Medium;
                            style.WrapText = true;
                            style.Alignment = HorizontalAlignment.Center;
                            style.VerticalAlignment = VerticalAlignment.Center;
                            cell.CellStyle = style;
                            cell.SetCellValue(header.ToDescription());

                            colNumber++;
                        }

                        row = (XSSFRow)sheet.CreateRow(1);
                        style = (XSSFCellStyle)workbook.CreateCellStyle();
                        style.BorderBottom = BorderStyle.Medium;
                        style.Alignment = HorizontalAlignment.Center;
                        style.VerticalAlignment = VerticalAlignment.Center;

                        cell = (XSSFCell)row.CreateCell(0);
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(1);
                        cell.SetCellValue(PressureUnit.BARG.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(2);
                        cell.SetCellValue(TemperatueUnit.DEG_C.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(3);
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(4);
                        cell.SetCellValue(PressureUnit.BARG.ToDescription());
                        cell.CellStyle = style;

                        cell = (XSSFCell)row.CreateCell(5);
                        cell.SetCellValue(TemperatueUnit.DEG_C.ToDescription());
                        cell.CellStyle = style;

                        if (param.InterfacePoints.Any())
                        {
                            i = 4;
                            countData = param.InterfacePoints.Count() == 0 ? i : param.InterfacePoints.Count();
                            addressList = new CellRangeAddressList(startRowIndex, parseRecordNumber, 0, 0);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$F$" + i + ":$F$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (param.Scrubbers.Any())
                        {
                            i = 4;
                            countData = param.Scrubbers.Count() == 0 ? i : param.Scrubbers.Count();
                            addressList = new CellRangeAddressList(startRowIndex, parseRecordNumber, 3, 3);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$G$" + i + ":$G$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (pressureUnits.Any())
                        {
                            i = 4;
                            countData = pressureUnits.Count() == 0 ? i : pressureUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 1, 1);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$O$" + i + ":$O$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);

                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 4, 4);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$O$" + i + ":$O$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        if (temperatueUnits.Any())
                        {
                            i = 4;
                            countData = temperatueUnits.Count() == 0 ? i : temperatueUnits.Count();
                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 4, 4);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$L$" + i + ":$L$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);

                            addressList = new CellRangeAddressList(subHeaderIdx, subHeaderIdx, 5, 5);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$L$" + i + ":$L$", countData + j));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }

                        //sheet.ProtectSheet(SHEET_PASSWORD);

                        break;
                    case SheetExcel.RENCANA_REALISASI:
                        IEnumerable<ActualPlanningColumnHeader> realitationColumns = EnumExtension.EnumToList<ActualPlanningColumnHeader>();
                        colNumber = 0; rowNumber = 0;
                        sheet = (XSSFSheet)workbook.CreateSheet(ws.ToString());
                        dvHelper = new XSSFDataValidationHelper(sheet);

                        row = (XSSFRow)sheet.CreateRow(rowNumber);
                        foreach (var header in realitationColumns)
                        {
                            cell = (XSSFCell)row.CreateCell(colNumber);

                            if (header.ToDescription() == ActualPlanningColumnHeader.AREA.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 4000);
                            }
                            else if (header.ToDescription() == ActualPlanningColumnHeader.PLANNING.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 6000);
                            }
                            else if (header.ToDescription() == ActualPlanningColumnHeader.LAST_ACTUAL.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 6000);
                            }
                            else if (header.ToDescription() == ActualPlanningColumnHeader.LAST_FINDINGS.ToDescription())
                            {
                                sheet.SetColumnWidth(colNumber, 6000);
                            }

                            style = (XSSFCellStyle)workbook.CreateCellStyle();
                            style.BorderBottom = style.BorderLeft = style.BorderRight = BorderStyle.Medium;
                            style.WrapText = true;
                            style.Alignment = HorizontalAlignment.Center;
                            style.VerticalAlignment = VerticalAlignment.Center;
                            cell.CellStyle = style;
                            cell.SetCellValue(header.ToDescription());

                            colNumber++;
                        }


                        if (param.PowerPlants.Any())
                        {
                            i = 4;
                            addressList = new CellRangeAddressList(1, parseRecordNumber, 0, 0);
                            dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint(string.Format("{0}{1}{2}", SheetExcel.MASTER, "!$H$" + i + ":$H$", i));
                            validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
                            validation.SuppressDropDownArrow = true;
                            validation.ShowErrorBox = true;
                            sheet.AddValidationData(validation);
                        }


                        //sheet.ProtectSheet(SHEET_PASSWORD);

                        break;
                    default:
                        break;
                }


            }

            //write to byte[]
            MemoryStream ms = new MemoryStream();


            workbook.SetActiveSheet(0);
            workbook.SetSheetHidden(8, SheetState.Hidden);

            workbook.Write(ms);

            return ms.ToArray();

        }

        public async void ParseFile
        (
            HttpPostedFileBase[] files,
            IEnumerable<Business.Entities.PowerPlant> plants,
            IEnumerable<Well> wells,
            IEnumerable<MstSeparator> separators,
            IEnumerable<InterfacePoint> interfacePoints,
            IEnumerable<Scrubber> scrubbers,
            bool isOldTemplate
        )
        {
            // kamus         
            int rowIndex = 2;
            ErrParseExcel = new List<string>();
            TaskReportStub = new TaskReportFormStub();
            Pltpls = new List<PltpImportFormStub>();
            Dkps = new List<DkpImportFormStub>();
            Productions = new List<ProductionImportFormStub>();
            Reinjections = new List<ReinjectionImportFormStub>();
            Monitorings = new List<MonitoringImportFormStub>();
            Separators = new List<SeparatorImportFormStub>();
            InterfaceScrubbers = new List<InterfaceScrubberImportFormStub>();
            ActualPlannings = new List<ActualPlanningImportFormStub>();
            IsOldTemplate = isOldTemplate;

            XSSFWorkbook book = null;
            XSSFSheet sheet = null;
            IRow row;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            List<Task> taskMapping = null;
            string temp;
            string message = "Upload excel sheet {0} failed because data(s)'s empty.";

            // algoritma
            if (files != null && files.Any())
            {
                if (files[0].ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
                    Path.GetExtension(files[0].FileName.ToLower()) == ".xlsx")
                {
                    try
                    {
                        FileName = files[0].FileName;
                        book = new XSSFWorkbook(files.First().InputStream);
                        IEnumerable<SheetExcel> worksheets = EnumExtension.EnumToList<SheetExcel>();

                        for (int i = 9 - 1; i >= 0; i--)
                        {
                            switch (i)
                            {

                                //uncomment for template ipge
                                case 8:
                                    //sheet = (XSSFSheet)book.GetSheet(SheetExcel.MASTER.ToString());
                                    if (IsOldTemplate == false)
                                    {
                                        sheet = (XSSFSheet)book.GetSheetAt(i);

                                        sheet.GetRow(0).GetCell(0).SetCellType(CellType.String);
                                        temp = sheet.GetRow(0).GetCell(0).StringCellValue;
                                        //TaskReportStub.Periode = DateTime.FromOADate(temp.ToDouble());

                                        sheet.GetRow(1).GetCell(0).SetCellType(CellType.String);
                                        temp = sheet.GetRow(1).GetCell(0).StringCellValue;
                                        TaskReportStub.AreaId = int.Parse(temp);

                                        plants = plants.Where(x => x.AreaId == TaskReportStub.AreaId);
                                        wells = wells.Where(x => x.AreaId == TaskReportStub.AreaId);
                                        separators = separators.Where(x => x.AreaId == TaskReportStub.AreaId);
                                        interfacePoints = interfacePoints.Where(x => x.AreaId == TaskReportStub.AreaId);
                                        scrubbers = scrubbers.Where(x => x.AreaId == TaskReportStub.AreaId);
                                    }
                                    else
                                    {
                                        string fileName = files[0].FileName.ToLower();
                                        if (fileName.Contains("kmj"))
                                            TaskReportStub.AreaId = 1;
                                        else if (fileName.Contains("lhd"))
                                            TaskReportStub.AreaId = 2;
                                        else if (fileName.Contains("krh"))
                                            TaskReportStub.AreaId = 3;
                                        else if (fileName.Contains("ubl"))
                                            TaskReportStub.AreaId = 4;
                                        else if (fileName.Contains("drj"))
                                            TaskReportStub.AreaId = 5;
                                        else if (fileName.Contains("slk"))
                                            TaskReportStub.AreaId = 6;
                                        else if (fileName.Contains("sby"))
                                            TaskReportStub.AreaId = 7;
                                        else if (fileName.Contains("lmd"))
                                            TaskReportStub.AreaId = 10;
                                        break;
                                    }
                                    break;
                                case 7:
                                    sheet = (XSSFSheet)book.GetSheet(SheetExcel.RENCANA_REALISASI.ToString());
                                    Valid = true;

                                    if (Valid && sheet.GetRow(1) != null)
                                    {
                                        int rowCount = sheet.LastRowNum;
                                        taskMapping = new List<Task>();

                                        for (int r = rowIndex; r <= rowCount; r++)
                                            taskMapping.Add(MappingObjectActualPlanAsync(sheet, r));

                                        await Task.WhenAll(taskMapping);
                                    }
                                    else
                                    {
                                        //ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                                        ErrParseExcel.Add(string.Format(message, SheetExcel.RENCANA_REALISASI.ToString()));
                                    }

                                    break;
                                case 6:
                                    sheet = (XSSFSheet)book.GetSheet(SheetExcel.INTERFACE_SCRUBBER.ToString());
                                    Valid = true;

                                    if (Valid && sheet.GetRow(rowIndex) != null)
                                    {
                                        int rowCount = sheet.LastRowNum;
                                        taskMapping = new List<Task>();

                                        for (int r = rowIndex; r <= rowCount; r++)
                                            taskMapping.Add(MappingObjectInterfaceScrubberAsync(sheet, r, interfacePoints, scrubbers));

                                        await Task.WhenAll(taskMapping);
                                    }
                                    else
                                    {
                                        //ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                                        ErrParseExcel.Add(string.Format(message, SheetExcel.INTERFACE_SCRUBBER.ToString()));
                                    }

                                    break;
                                case 5:
                                    sheet = (XSSFSheet)book.GetSheet(SheetExcel.SEPARATOR.ToString());
                                    Valid = true;

                                    if (Valid && sheet.GetRow(rowIndex) != null)
                                    {
                                        int rowCount = sheet.LastRowNum;
                                        taskMapping = new List<Task>();

                                        for (int r = rowIndex; r <= rowCount; r++)
                                            taskMapping.Add(MappingObjectSeparatorAsync(sheet, r, separators));

                                        await Task.WhenAll(taskMapping);
                                    }
                                    else
                                    {
                                        //ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                                        ErrParseExcel.Add(string.Format(message, SheetExcel.SEPARATOR.ToString()));
                                    }

                                    break;
                                case 4:
                                    sheet = (XSSFSheet)book.GetSheet(SheetExcel.MONITORING.ToString());
                                    Valid = true;

                                    if (Valid && sheet.GetRow(rowIndex) != null)
                                    {
                                        int rowCount = sheet.LastRowNum;
                                        taskMapping = new List<Task>();

                                        for (int r = rowIndex; r <= rowCount; r++)
                                            taskMapping.Add(MappingObjectMonitoringAsync(sheet, r, wells));

                                        await Task.WhenAll(taskMapping);
                                    }
                                    else
                                    {
                                        //ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                                        ErrParseExcel.Add(string.Format(message, SheetExcel.MONITORING.ToString()));
                                    }

                                    break;
                                case 3:
                                    sheet = (XSSFSheet)book.GetSheet(SheetExcel.REINJEKSI.ToString());
                                    Valid = true;

                                    if (Valid && sheet.GetRow(rowIndex) != null)
                                    {
                                        int rowCount = sheet.LastRowNum;
                                        taskMapping = new List<Task>();

                                        for (int r = rowIndex; r <= rowCount; r++)
                                            taskMapping.Add(MappingObjectReinjectionAsync(sheet, r, wells));

                                        await Task.WhenAll(taskMapping);
                                    }
                                    else
                                    {
                                        //ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                                        ErrParseExcel.Add(string.Format(message, SheetExcel.REINJEKSI.ToString()));
                                    }

                                    break;
                                case 2:
                                    InvalidProductionRow = -1;

                                    sheet = (XSSFSheet)book.GetSheet(SheetExcel.PRODUKSI.ToString());
                                    Valid = true;

                                    if (Valid && sheet.GetRow(rowIndex) != null)
                                    {
                                        int rowCount = sheet.LastRowNum;
                                        taskMapping = new List<Task>();

                                        for (int r = rowIndex; r <= rowCount; r++)
                                            taskMapping.Add(MappingObjectProductionAsync(sheet, r, wells));

                                        await Task.WhenAll(taskMapping);
                                    }
                                    else
                                    {
                                        //ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                                        ErrParseExcel.Add(string.Format(message, SheetExcel.PRODUKSI.ToString()));
                                    }

                                    break;
                                case 1:
                                    sheet = (XSSFSheet)book.GetSheet(SheetExcel.DKP.ToString());
                                    Valid = true;

                                    if (Valid && sheet.GetRow(1) != null)
                                    {
                                        int rowCount = sheet.LastRowNum;
                                        taskMapping = new List<Task>();

                                        for (int r = 1; r <= rowCount; r++)
                                            taskMapping.Add(MappingObjectDkpAsync(sheet, r, plants));

                                        await Task.WhenAll(taskMapping);
                                    }
                                    else
                                    {
                                        //ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                                        ErrParseExcel.Add(string.Format(message, SheetExcel.DKP.ToString()));
                                    }

                                    break;
                                case 0:
                                    sheet = (XSSFSheet)book.GetSheet(SheetExcel.PLTP.ToString());
                                    Valid = true;
                                    //Valid = CheckTemplate(sheet, Columns);

                                    if (Valid && sheet.GetRow(rowIndex) != null)
                                    {
                                        int rowCount = sheet.LastRowNum;
                                        taskMapping = new List<Task>();

                                        for (int r = rowIndex; r <= rowCount; r++)
                                            taskMapping.Add(MappingObjectPltpAsync(sheet, r, plants));

                                        await Task.WhenAll(taskMapping);
                                    }
                                    else
                                    {
                                        ErrParseExcel.Add(string.Format(message, SheetExcel.PLTP.ToString()));
                                    }

                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrParseExcel.Add(string.Format(MSG_ERROR, FileName));
                    }
                }
                else
                {
                    ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                }
            }
        }

        private Task MappingObjectPltpAsync(XSSFSheet sheet, int row, IEnumerable<Business.Entities.PowerPlant> plants = null)
        {
            string plant = null;
            string unit = string.Empty;
            PltpImportFormStub model = null;
            int errCount = 0; int i = 0;
            IEnumerable<Business.Entities.PowerPlant> temps;

            if (sheet.GetRow(row) != null)
            {
                model = new PltpImportFormStub();
                List<ColumnHelper> columns = GetColumnsPltp();

                foreach (var col in columns)
                {
                    switch (col.ColumnNum)
                    {
                        case 0:
                            plant = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                            if (!string.IsNullOrEmpty(plant))
                            {
                                plant = plant.Trim();
                                temps = plants != null ? plants.Where(n => n.Name.Trim() == plant) : null;
                                if (temps.Any())
                                    model.PowerPlantId = temps.First().Id;
                                else
                                {
                                    errCount++;
                                    ErrParseExcel.Add(string.Format("Data Power Plant at Sheet {0}, Row {1}, Column [{2}] not found on database.", sheet.SheetName, (row + 1), (col.ColumnName)));
                                }
                            }
                            break;
                        case 1:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);
                            double? energyGross = ParsingCell<double?>(sheet, col, row, ref errCount);

                            model.EnergyGross = energyGross != null ? energyGross : null;
                            if (unit != null)
                            {
                                try
                                {
                                    model.EnergyGrossUnit = EnumExtension.ToEnum<EnergyUnit>(unit.ToUpper()).ToString();
                                }
                                catch (Exception)
                                {
                                    model.EnergyGrossUnit = EnergyUnit.KW_HR.ToString();
                                }
                            }
                            break;
                        case 2:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);
                            double? energyNett = ParsingCell<double?>(sheet, col, row, ref errCount);

                            model.EnergyNett = energyNett != null ? energyNett : null;
                            if (unit != null)
                            {
                                try
                                {
                                    model.EnergyNettUnit = EnumExtension.ToEnum<EnergyUnit>(unit.ToUpper()).ToString();
                                }
                                catch (Exception)
                                {
                                    model.EnergyNettUnit = EnergyUnit.KW_HR.ToString();
                                }
                            }

                            break;
                        case 3:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);
                            double? steamUtilization = ParsingCell<double?>(sheet, col, row, ref errCount);

                            model.SteamUtilization = steamUtilization != null ? steamUtilization : null;
                            if (unit != null)
                                model.SteamUtilizationUnit = unit.ToUpper();

                            break;
                        case 4:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);
                            double? wasteRockMuffler = ParsingCell<double?>(sheet, col, row, ref errCount);

                            model.WasteRockMufller = wasteRockMuffler != null ? wasteRockMuffler : null;
                            if (unit != null)
                                model.WasteRockMufllerUnit = unit.ToUpper();

                            break;
                        case 5:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);
                            string temp = unit.ToUpper().Replace("/", "_");
                            double? ssc = ParsingCell<double?>(sheet, col, row, ref errCount);

                            model.Ssc = ssc != null ? ssc : null;
                            if (unit != null)
                                model.SscUnit = temp.Replace(".", "_");

                            break;
                        case 6:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);
                            double? gppsSd = ParsingCell<double?>(sheet, col, row, ref errCount);

                            model.GppsShutdown = gppsSd != null ? gppsSd : null;
                            if (unit != null)
                                model.GppsShutdownUnit = unit.ToUpper();
                            break;
                        case 7:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);
                            double? importEnergy = ParsingCell<double?>(sheet, col, row, ref errCount);

                            model.ImportEnergy = importEnergy != null ? importEnergy : null;
                            if (unit != null)
                            {
                                try
                                {
                                    model.ImportEnergyUnit = EnumExtension.ToEnum<EnergyUnit>(unit.ToUpper()).ToString();
                                }
                                catch (Exception)
                                {
                                    model.ImportEnergyUnit = EnergyUnit.KW_HR.ToString();
                                }
                            }
                            break;
                        case 8:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);
                            double? sgssSd = ParsingCell<double?>(sheet, col, row, ref errCount);

                            model.SgssShutdown = sgssSd != null ? sgssSd : null;
                            if (unit != null)
                                model.SgssShutdownUnit = unit.ToUpper();
                            break;
                        case 9:
                            model.Notes = ParsingCell<string>(sheet, col, row, ref errCount);

                            break;
                        default:
                            break;
                    }
                    i++;
                }

                if (errCount == 0)
                    Pltpls.Add(model);
            }

            return Task.FromResult(0);
        }

        private Task MappingObjectDkpAsync(XSSFSheet sheet, int row, IEnumerable<Business.Entities.PowerPlant> plants = null)
        {
            int errCount = 0; int i = 0;
            DkpImportFormStub model;
            IEnumerable<Business.Entities.PowerPlant> powerPlants;
            string powerPlanName, dateTime = string.Empty;
            //string periode = TaskReportStub.Periode.Value.ToString(DisplayFormat.SqlDateFormat);

            if (sheet.GetRow(row) != null)
            {
                model = new DkpImportFormStub();
                List<ColumnHelper> columns = GetDkpColumn();
                var lastCell = sheet.GetRow(row).LastCellNum;

                foreach (var col in columns)
                {
                    switch (col.ColumnNum)
                    {
                        case 0:
                            powerPlanName = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                            if (!string.IsNullOrEmpty(powerPlanName))
                            {
                                powerPlanName = powerPlanName.Trim();
                                powerPlants = plants?.Where(n => n.Name.Trim() == powerPlanName);

                                if (powerPlants.Any())
                                    model.PowerPlantId = powerPlants.First().Id;
                                else
                                {
                                    errCount++;
                                    ErrParseExcel.Add(string.Format("Data Power Plant at Sheet {0}, Row {1}, Column [{2}] not found on database.", sheet.SheetName, (row + 1), (col.ColumnName)));
                                }
                            }
                            break;
                        case 1:
                            string status = ParsingRequiredCell<string>(sheet, col, row, ref errCount);

                            try
                            {
                                model.Status = (EnumExtension.ToEnum<InterruptionStatus>(status)).ToString();
                            }
                            catch (Exception)
                            {
                                errCount++;
                                ErrParseExcel.Add(string.Format("Data DKP at Sheet {0}, Row {1}, Column [{2}] not found on database.", sheet.SheetName, (row + 1), (col.ColumnName)));
                            }

                            break;
                        case 2:
                            //dateTime = $"{periode} {ParsingRequiredCell<string>(sheet, col, row, ref errCount)}";
                            model.StartHour = ParsingCell<TimeSpan?>(sheet, col, row, ref errCount);

                            break;
                        case 3:
                            //dateTime = $"{periode} {ParsingRequiredCell<string>(sheet, col, row, ref errCount)}";
                            model.EndHour = ParsingCell<TimeSpan?>(sheet, col, row, ref errCount);

                            break;
                        case 4:
                            double? derating = ParsingCell<double?>(sheet, col, row, ref errCount);
                            model.Derating = derating != null ? derating : null;

                            break;
                        case 5:
                            string location;
                            if (!string.IsNullOrEmpty(model.Status) && model.Status.ToUpper() == Common.Enums.InterruptionStatus.NORMAL.ToString())
                                location = ParsingCell<string>(sheet, col, row, ref errCount);
                            else
                                location = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                            if (!string.IsNullOrEmpty(location))
                            {
                                model.Location = location.ToUpper();
                            }
                            else
                            {
                                model.Location = location;
                            }


                            //model.Location = ParsingRequiredCell<string>(sheet, col, row, ref errCount);

                            break;
                        case 6:
                            model.Cause = ParsingCell<string>(sheet, col, row, ref errCount);

                            break;
                        case 7:
                            model.Notes = ParsingCell<string>(sheet, col, row, ref errCount);

                            break;
                        default:
                            break;
                    }

                    i++;
                }

                if (errCount == 0)
                    Dkps.Add(model);
            }

            return Task.FromResult(0);
        }

        private Task MappingObjectProductionAsync(XSSFSheet sheet, int row, IEnumerable<Well> wells = null)
        {
            int errCount = 0; int i = 0;
            ProductionImportFormStub model;
            IEnumerable<Well> iWells;
            string wellName;
            string unit = string.Empty;

            if (sheet.GetRow(row) != null)
            {

                model = new ProductionImportFormStub();
                List<ColumnHelper> columns = GetProductionColumn();

                foreach (var col in columns)
                {
                    switch (col.ColumnNum)
                    {
                        case 0:
                            wellName = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                            if (!string.IsNullOrEmpty(wellName))
                            {
                                wellName = wellName.Trim();
                                iWells = wells?.Where(n => n.Name.Trim() == wellName);

                                if (iWells.Any())
                                    model.WellId = iWells.First().Id;
                                else
                                {
                                    errCount++;
                                    ErrParseExcel.Add(string.Format("Data Well at Sheet {0}, Row {1}, Column [{2}] not found on database.", sheet.SheetName, (row + 1), (col.ColumnName)));
                                }
                            }
                            break;
                        case 1:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.Tks = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.TksUnit = unit.ToUpper().Replace(" ", "_");

                            if (model.PressureLine != null && model.Tks != null)
                            {
                                if (model.PressureLine.Value > model.Tks)
                                {
                                    ++errCount;
                                    ErrParseExcel.Add("Pressure line in row " + (row + 1) + " sheet PRODUCTION must less than or equals to Tks!");
                                }

                            }
                            break;
                        case 2:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.PressureLine = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.PressureLineUnit = unit.ToUpper().Replace(" ", "_");

                            if (model.PressureLine != null && model.Tks != null)
                            {
                                if (model.PressureLine.Value > model.Tks)
                                {
                                    ++errCount;
                                    ErrParseExcel.Add("Pressure line in row " + (row + 1) + " sheet PRODUCTION must less than or equals to Tks!");
                                }
                            }

                            break;
                        case 3:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.Temperature = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.TemperatureUnit = unit.ToUpper().Replace(" ", "_");

                            break;
                        case 4:
                            unit = "PERCENT";   //static unit

                            model.ThrottleValve = ParsingCell<double?>(sheet, col, row, ref errCount);
                            model.ThrottleValveUnit = unit;

                            break;
                        case 5:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.FlowRate = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.FlowRateUnit = unit.ToUpper().Replace("/", "_");

                            break;
                        case 6:
                            unit = "T"; // static unit

                            model.FlowTotal = ParsingCell<double?>(sheet, col, row, ref errCount);
                            model.FlowTotalUnit = unit;

                            break;
                        case 7:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.PressureUpstream = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.PressureUpstreamUnit = unit.ToUpper().Replace(" ", "_");

                            break;
                        case 8:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.DifferentialPressure = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.DifferentialPressureUnit = unit.ToUpper().Replace(" ", "_");

                            break;
                        case 9:
                            string status = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                            //model.Status = string.IsNullOrEmpty(status) ? string.Empty : status.ToUpper().Replace(" ", "_");
                            try
                            {
                                model.Status = string.IsNullOrEmpty(status) ? string.Empty : EnumExtension.ToEnum<DrawWellSubStatus>(status.Trim()).ToString();
                            }
                            catch (Exception)
                            {
                                ++errCount;
                                ErrParseExcel.Add("Status in row " + (row + 1) + " sheet PRODUCTION not valid!");
                            }

                            break;

                        //case 9:
                        case 10:
                            model.Notes = ParsingCell<string>(sheet, col, row, ref errCount);

                            break;
                        default:
                            break;
                    }



                    i++;
                }



                if (errCount == 0)
                    Productions.Add(model);
            }

            return Task.FromResult(0);
        }

        private Task MappingObjectReinjectionAsync(XSSFSheet sheet, int row, IEnumerable<Well> wells = null)
        {
            int errCount = 0; int i = 0;
            ReinjectionImportFormStub model;
            IEnumerable<Well> iWells;
            string wellName;
            string unit = string.Empty;

            if (sheet.GetRow(row) != null)
            {
                model = new ReinjectionImportFormStub();
                List<ColumnHelper> columns = GetReinjectionColumn();

                foreach (var col in columns)
                {
                    switch (col.ColumnNum)
                    {
                        case 0:
                            wellName = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                            if (!string.IsNullOrEmpty(wellName))
                            {
                                wellName = wellName.Trim();
                                iWells = wells?.Where(n => n.Name.Trim() == wellName);

                                if (iWells.Any())
                                    model.WellId = iWells.First().Id;
                                else
                                {
                                    errCount++;
                                    ErrParseExcel.Add(string.Format("Data Well at Sheet {0}, Row {1}, Column [{2}] not found on database.", sheet.SheetName, (row + 1), (col.ColumnName)));
                                }
                            }
                            break;
                        case 1:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.Tks = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.TksUnit = unit.ToUpper().Replace(" ", "_");

                            break;
                        case 2:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.PressureLine = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.PressureLineUnit = unit.ToUpper().Replace(" ", "_");

                            break;
                        case 3:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.Temperature = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.TemperatureUnit = unit.ToUpper().Replace(" ", "_");

                            break;
                        case 4:
                            model.Ph = ParsingCell<double?>(sheet, col, row, ref errCount);

                            break;
                        case 5:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.FlowRate = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.FlowRateUnit = unit.ToUpper().Replace("/", "_");

                            break;
                        case 6:
                            unit = "T"; //static status
                            model.FlowTotal = ParsingCell<double?>(sheet, col, row, ref errCount);
                            model.FlowTotalUnit = unit;

                            break;
                        case 7:

                            model.Fluida = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                            if (!string.IsNullOrEmpty(model.Fluida))
                            {
                                try
                                {
                                    model.Fluida = EnumExtension.ToEnum<Fluida>(model.Fluida.Trim()).ToString();
                                }
                                catch (Exception)
                                {
                                    ++errCount;
                                    ErrParseExcel.Add("Fluida in row " + (row + 1) + " sheet "+ sheet.SheetName+" not valid!");
                                }
                            }

                            break;
                        case 8:
                            string status = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                            try
                            {
                                model.Status = string.IsNullOrEmpty(status) ? string.Empty : EnumExtension.ToEnum<DrawWellSubStatus>(status.Trim()).ToString();
                            }
                            catch (Exception)
                            {
                                ++errCount;
                                ErrParseExcel.Add("Status in row " + (row + 1) + " sheet " + sheet.SheetName + " not valid!");
                            }

                            break;

                        //case 8:
                        case 9:
                            model.Notes = ParsingCell<string>(sheet, col, row, ref errCount); ;

                            break;
                        default:
                            break;
                    }

                    i++;
                }

                if (errCount == 0)
                    Reinjections.Add(model);
            }

            return Task.FromResult(0);
        }

        private Task MappingObjectMonitoringAsync(XSSFSheet sheet, int row, IEnumerable<Well> wells = null)
        {
            int errCount = 0; int i = 0;
            MonitoringImportFormStub model;
            IEnumerable<Well> iWells;
            string wellName;
            try
            {
                if (sheet.GetRow(row) != null)
                {
                    model = new MonitoringImportFormStub();
                    List<ColumnHelper> columns = GetMonitoringColumn();

                    foreach (var col in columns)
                    {
                        switch (col.ColumnNum)
                        {
                            case 0:
                                wellName = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                if (!string.IsNullOrEmpty(wellName))
                                {
                                    wellName = wellName.Trim();
                                    iWells = wells?.Where(n => n.Name.Trim() == wellName);

                                    if (iWells.Any())
                                        model.WellId = iWells.First().Id;
                                    else
                                    {
                                        errCount++;
                                        ErrParseExcel.Add(string.Format("Data Well at Sheet {0}, Row {1}, Column [{2}] not found on database.", sheet.SheetName, (row + 1), (col.ColumnName)));
                                    }
                                }
                                break;
                            case 1:
                                string unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                                model.Tks = ParsingCell<double?>(sheet, col, row, ref errCount);
                                if (unit != null)
                                    model.TksUnit = unit.ToUpper().Replace(" ", "_");

                                break;
                            case 2:
                                //model.Status = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                string _status = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                try
                                {
                                    model.Status = string.IsNullOrEmpty(_status) ? string.Empty : EnumExtension.ToEnum<DrawWellSubStatus>(_status.Trim()).ToString();
                                }
                                catch (Exception)
                                {
                                    ++errCount;
                                    ErrParseExcel.Add("Status in row " + (row + 1) + " sheet " + sheet.SheetName + " not valid!");
                                }

                                break;

                            //case 2:
                            case 3:
                                model.Notes = ParsingCell<string>(sheet, col, row, ref errCount);

                                break;
                            default:
                                break;
                        }

                        i++;
                    }

                    if (errCount == 0)
                        Monitorings.Add(model);
                }
            }
            catch (Exception ex)
            { }
            return Task.FromResult(0);
        }

        private Task MappingObjectSeparatorAsync(XSSFSheet sheet, int row, IEnumerable<MstSeparator> separators = null)
        {
            int errCount = 0; int i = 0;
            SeparatorImportFormStub model;
            IEnumerable<MstSeparator> iSeparators;
            string separatorName = string.Empty;
            string unit = string.Empty;

            if (sheet.GetRow(row) != null)
            {
                model = new SeparatorImportFormStub();
                List<ColumnHelper> columns = GetSeparatorColumn();

                foreach (var col in columns)
                {
                    switch (col.ColumnNum)
                    {
                        case 0:
                            //separatorName = ParsingRequiredCell<string>(sheet, col, row, ref errCount).Trim();
                            separatorName = ParsingCell<string>(sheet, col, row, ref errCount);
                            if (!string.IsNullOrEmpty(separatorName))
                            {
                                separatorName = separatorName.Trim();
                                iSeparators = separators?.Where(n => n.Name.Trim() == separatorName);

                                if (iSeparators.Any())
                                    model.SeparatorId = iSeparators.First().Id;
                                else
                                {
                                    errCount++;
                                    ErrParseExcel.Add(string.Format("Data Separator at Sheet {0}, Row {1}, Column [{2}] not found on database.", sheet.SheetName, (row + 1), (col.ColumnName)));
                                }
                            }
                            break;
                        case 1:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.Pressure = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.PressureUnit = unit.ToUpper().Replace(" ", "_");

                            break;
                        case 2:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.Temperature = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.TemperatureUnit = unit.ToUpper().Replace(" ", "_");

                            break;
                        case 3:
                            model.LevelBrine = ParsingCell<double?>(sheet, col, row, ref errCount);
                            model.LevelBrineUnit = "PERCENT";

                            break;
                        case 4:
                            model.ThrottleValveBrine = ParsingCell<double?>(sheet, col, row, ref errCount);
                            model.ThrottleValveBrineUnit = "PERCENT";

                            break;
                        case 5:
                            model.ThrottleValveSteam = ParsingCell<double?>(sheet, col, row, ref errCount);
                            model.ThrottleValveSteamUnit = "PERCENT";

                            break;
                        case 6:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.SteamFlowRate = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.SteamFlowRateUnit = unit.ToUpper().Replace("/", "_");

                            break;
                        case 7:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.BrineFlowRate = ParsingCell<double?>(sheet, col, row, ref errCount);
                            if (unit != null)
                                model.BrineFlowRateUnit = unit.ToUpper().Replace("/", "_");

                            break;
                        default:
                            break;
                    }

                    i++;
                }

                if (errCount == 0)
                    Separators.Add(model);
            }

            return Task.FromResult(0);
        }

        private Task MappingObjectInterfaceScrubberAsync(XSSFSheet sheet, int row, IEnumerable<InterfacePoint> interfacePoints = null, IEnumerable<Scrubber> scrubbers = null)
        {
            int errCount = 0; int i = 0;
            InterfaceScrubberImportFormStub model;
            IEnumerable<InterfacePoint> iInterfacePoints;
            IEnumerable<Scrubber> iScrubbers;
            string unit = string.Empty;
            string intrefacePointName, scrubberName;
            List<string> tempValues = new List<string>();
            List<string> tempErrors = new List<string>();

            if (sheet.GetRow(row) != null)
            {
                model = new InterfaceScrubberImportFormStub();
                List<ColumnHelper> columns = GetInterfaceScrubberColumn();

                foreach (var col in columns)
                {
                    switch (col.ColumnNum)
                    {
                        case 0:
                            intrefacePointName = ParsingCell<string>(sheet, col, row, ref errCount);
                            tempValues.Add(intrefacePointName);

                            if (!string.IsNullOrEmpty(intrefacePointName))
                            {
                                intrefacePointName = intrefacePointName.Trim();

                                iInterfacePoints = interfacePoints?.Where(n => n.Name.Trim() == intrefacePointName);

                                if (iInterfacePoints.Any())
                                    model.InterfaceId = iInterfacePoints.First().Id;
                                else
                                {
                                    errCount++;
                                    ErrParseExcel.Add(string.Format("Data Interface at Sheet {0}, Row {1}, Column [{2}] not found on database.", sheet.SheetName, (row + 1), (col.ColumnName)));
                                }
                            }
                            break;
                        case 1:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.InterfacePressure = ParsingCell<double?>(sheet, col, row, ref errCount);
                            tempValues.Add(model.InterfacePressure.HasValue ? model.InterfacePressure.Value.ToString() : null);

                            if (unit != null)
                                model.InterfacePressureUnit = unit.ToUpper().Replace(" ", "_");

                            break;
                        case 2:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);

                            model.InterfaceTemperature = ParsingCell<double?>(sheet, col, row, ref errCount);
                            tempValues.Add(model.InterfaceTemperature.HasValue ? model.InterfaceTemperature.Value.ToString() : null);

                            if (unit != null)
                                model.InterfaceTemperatureUnit = unit.ToUpper().Replace(" ", "_");

                            break;
                        case 3:
                            scrubberName = ParsingCell<string>(sheet, col, row, ref errCount);
                            tempValues.Add(scrubberName);

                            if (scrubberName != null && !string.IsNullOrEmpty(scrubberName))
                            {
                                iScrubbers = scrubbers?.Where(n => n.Name.Trim() == scrubberName.Trim());

                                if (iScrubbers.Any())
                                    model.ScrubberId = iScrubbers.First().Id;
                                else
                                {
                                    errCount++;
                                    ErrParseExcel.Add(string.Format("Data Scrubber at Sheet {0}, Row {1}, Column [{2}] not found on database.", sheet.SheetName, (row + 1), (col.ColumnName)));
                                }
                            }
                            break;
                        case 4:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);
                            model.ScrubberPressure = ParsingCell<double?>(sheet, col, row, ref errCount);
                            tempValues.Add(model.ScrubberPressure.HasValue ? model.ScrubberPressure.Value.ToString() : null);

                            if (unit != null)
                                model.ScrubberPressureUnit = unit.ToUpper().Replace(" ", "_");

                            break;
                        case 5:
                            unit = ParsingCell<string>(sheet, col, 1, ref errCount);
                            model.ScrubberTemperature = ParsingCell<double?>(sheet, col, row, ref errCount);
                            tempValues.Add(model.ScrubberTemperature.HasValue ? model.ScrubberTemperature.Value.ToString() : null);

                            if (unit != null)
                                model.ScrubberTemperatureUnit = unit.ToUpper().Replace(" ", "_");
                            break;
                        default:
                            break;
                    }

                    i++;
                }

                if (errCount == 0)
                    InterfaceScrubbers.Add(model);
            }

            return Task.FromResult(0);
        }

        private Task MappingObjectActualPlanAsync(XSSFSheet sheet, int row)
        {
            int errCount = 0; int i = 0;
            ActualPlanningImportFormStub model;
            //Area area;
            //string areaName;

            if (sheet.GetRow(row) != null)
            {
                model = new ActualPlanningImportFormStub();
                List<ColumnHelper> columns = GetActualPlanningColumn();

                foreach (var col in columns)
                {
                    switch (col.ColumnNum)
                    {
                        case 0:
                            //areaName = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                            model.AreaId = TaskReportStub.AreaId;

                            break;
                        case 1:
                            model.Planning = ParsingRequiredCell<string>(sheet, col, row, ref errCount);

                            break;
                        case 2:
                            model.Actual = ParsingRequiredCell<string>(sheet, col, row, ref errCount);

                            break;
                        case 3:
                            model.ActualCase = ParsingRequiredCell<string>(sheet, col, row, ref errCount);

                            break;
                        default:
                            break;
                    }

                    i++;
                }

                if (errCount == 0)
                    ActualPlannings.Add(model);
            }

            return Task.FromResult(0);
        }

        protected List<ColumnHelper> GetColumnsPltp()
        {
            //Set columns to parse data
            //Remember column start at 0!       

            var result = new List<ColumnHelper>();
            result = new List<ColumnHelper>
            {
                ColumnHelper.SetColumn(0, PltpExcelColumn.POWERPLANT.ToDescription()),
                ColumnHelper.SetColumn(1, PltpExcelColumn.ENERGY_GROSS.ToDescription()),
                ColumnHelper.SetColumn(2, PltpExcelColumn.ENERGY_NETT.ToDescription()),
                ColumnHelper.SetColumn(3, PltpExcelColumn.STEAM_UTILIZATION.ToDescription()),
                ColumnHelper.SetColumn(4, PltpExcelColumn.WASTE_MUFFLER.ToDescription()),
                ColumnHelper.SetColumn(5, PltpExcelColumn.SSC.ToDescription()),
                ColumnHelper.SetColumn(6, PltpExcelColumn.GPP_SHUTDOWN.ToDescription()),
                ColumnHelper.SetColumn(7, PltpExcelColumn.IMPORT_ENERGY.ToDescription()),
                ColumnHelper.SetColumn(8, PltpExcelColumn.SGSS_SHUTDOWN.ToDescription()),
                ColumnHelper.SetColumn(9, PltpExcelColumn.NOTES.ToDescription())
            };

            //if (IsOldTemplate)
            //    result.Add(ColumnHelper.SetColumn(10, PltpExcelColumn.NOTES.ToDescription()));
            //else
            //    result.Add(ColumnHelper.SetColumn(9, PltpExcelColumn.NOTES.ToDescription()));

            //for template pure
            return result;
        }

        protected List<ColumnHelper> GetDkpColumn()
        {
            var result = new List<ColumnHelper>();
            result = new List<ColumnHelper>
            {
                ColumnHelper.SetColumn(0, DKPColumnHeader.PLTP.ToDescription()),
                ColumnHelper.SetColumn(1, DKPColumnHeader.INTERRUPTION_STATUS.ToDescription()),
                ColumnHelper.SetColumn(2, DKPColumnHeader.INTERRUPTION_START_TIME.ToDescription()),
                ColumnHelper.SetColumn(3, DKPColumnHeader.INTERRUPTION_FINISH_TIME.ToDescription()),
                ColumnHelper.SetColumn(4, DKPColumnHeader.DERATING.ToDescription()),
                ColumnHelper.SetColumn(5, DKPColumnHeader.INTERRUPTION_LOCATION.ToDescription()),
                ColumnHelper.SetColumn(6, DKPColumnHeader.INTERRUPTION_CAUSE.ToDescription()),
                ColumnHelper.SetColumn(7, DKPColumnHeader.NOTES.ToDescription())
            };

            //if (IsOldTemplate)
            //    result.Add(ColumnHelper.SetColumn(8, PltpExcelColumn.NOTES.ToDescription()));
            //else
            //    result.Add(ColumnHelper.SetColumn(7, DKPColumnHeader.NOTES.ToDescription()));
            return result;
        }

        protected List<ColumnHelper> GetProductionColumn()
        {
            var result = new List<ColumnHelper>();
            result = new List<ColumnHelper>
            {
                ColumnHelper.SetColumn(0, ProductionColumnHeader.WELL.ToDescription()),
                ColumnHelper.SetColumn(1, ProductionColumnHeader.TKS.ToDescription()),
                ColumnHelper.SetColumn(2, ProductionColumnHeader.LINE_PRESSURE.ToDescription()),
                ColumnHelper.SetColumn(3, ProductionColumnHeader.LINE_TEMPERATURE.ToDescription()),
                ColumnHelper.SetColumn(4, ProductionColumnHeader.TRHOTTLE_VALVE_OPENING.ToDescription()),
                ColumnHelper.SetColumn(5, ProductionColumnHeader.FLOW_AVERAGE.ToDescription()),
                ColumnHelper.SetColumn(6, ProductionColumnHeader.FLOW_TOTAL.ToDescription()),
                ColumnHelper.SetColumn(7, ProductionColumnHeader.PRESSURE_UPSTREAM.ToDescription()),
                ColumnHelper.SetColumn(8, ProductionColumnHeader.DIFFERENTIAL_PRESSURE.ToDescription()),
                ColumnHelper.SetColumn(9, ProductionColumnHeader.WELL_STATUS.ToDescription()),
                ColumnHelper.SetColumn(10, ProductionColumnHeader.NOTES.ToDescription())
        };

            //if (IsOldTemplate)
            //    result.Add(ColumnHelper.SetColumn(10, ProductionColumnHeader.NOTES.ToDescription()));
            //else
            //    result.Add(ColumnHelper.SetColumn(9, ProductionColumnHeader.NOTES.ToDescription()));
            return result;
        }

        protected List<ColumnHelper> GetReinjectionColumn()
        {
            var result = new List<ColumnHelper>();
            result = new List<ColumnHelper>
            {
                ColumnHelper.SetColumn(0, ReinjectionColumnHeader.WELL.ToDescription()),
                ColumnHelper.SetColumn(1, ReinjectionColumnHeader.TKS.ToDescription()),
                ColumnHelper.SetColumn(2, ReinjectionColumnHeader.LINE_PRESSURE.ToDescription()),
                ColumnHelper.SetColumn(3, ReinjectionColumnHeader.LINE_TEMPERATURE.ToDescription()),
                ColumnHelper.SetColumn(4, ReinjectionColumnHeader.PH.ToDescription()),
                ColumnHelper.SetColumn(5, ReinjectionColumnHeader.FLOW_AVERAGE.ToDescription()),
                ColumnHelper.SetColumn(6, ReinjectionColumnHeader.FLOW_TOTAL.ToDescription()),
                ColumnHelper.SetColumn(7, ReinjectionColumnHeader.INJECTED_FLUID.ToDescription()),
                ColumnHelper.SetColumn(8, ReinjectionColumnHeader.WELL_STATUS.ToDescription()),
                ColumnHelper.SetColumn(9, ReinjectionColumnHeader.NOTES.ToDescription()),
            };

            //if (IsOldTemplate)
            //    result.Add(ColumnHelper.SetColumn(9, ReinjectionColumnHeader.NOTES.ToDescription()));
            //else
            //    result.Add(ColumnHelper.SetColumn(8, ReinjectionColumnHeader.NOTES.ToDescription()));

            return result;
        }

        protected List<ColumnHelper> GetMonitoringColumn()
        {
            var result = new List<ColumnHelper>();
            result = new List<ColumnHelper>
            {
                ColumnHelper.SetColumn(0, MonitoringColumnHeader.WELL.ToDescription()),
                ColumnHelper.SetColumn(1, MonitoringColumnHeader.TKS.ToDescription()),
                ColumnHelper.SetColumn(2, MonitoringColumnHeader.WELL_STATUS.ToDescription()),
            };

            if (IsOldTemplate)
                result.Add(ColumnHelper.SetColumn(3, MonitoringColumnHeader.NOTES.ToDescription()));
            else
                result.Add(ColumnHelper.SetColumn(2, MonitoringColumnHeader.NOTES.ToDescription()));

            return result;
        }

        protected List<ColumnHelper> GetSeparatorColumn()
        {
            return new List<ColumnHelper>
            {
                ColumnHelper.SetColumn(0, SeparatorColumnHeader.SEPARATOR.ToDescription()),
                ColumnHelper.SetColumn(1, SeparatorColumnHeader.PRESSURE.ToDescription()),
                ColumnHelper.SetColumn(2, SeparatorColumnHeader.TEMPERATURE.ToDescription()),
                ColumnHelper.SetColumn(3, SeparatorColumnHeader.LEVEL_BRINE.ToDescription()),
                ColumnHelper.SetColumn(4, SeparatorColumnHeader.THROTTLE_VALVE_OPENING_BRINE.ToDescription()),
                ColumnHelper.SetColumn(5, SeparatorColumnHeader.THROTTLE_VALVE_OPENING_STEAM.ToDescription()),
                ColumnHelper.SetColumn(6, SeparatorColumnHeader.AVERAGE_STEAM_FLOW.ToDescription()),
                ColumnHelper.SetColumn(7, SeparatorColumnHeader.AVERAGE_BRINE_FLOW.ToDescription())
            };
        }

        protected List<ColumnHelper> GetInterfaceScrubberColumn()
        {
            return new List<ColumnHelper>
            {
                ColumnHelper.SetColumn(0, InterfaceScrubberColumnHeader.INTERFACE_POINT.ToDescription()),
                ColumnHelper.SetColumn(1, InterfaceScrubberColumnHeader.INTERFACE_POINT_PRESSURE.ToDescription()),
                ColumnHelper.SetColumn(2, InterfaceScrubberColumnHeader.INTERFACE_POINT_TEMPERATURE.ToDescription()),
                ColumnHelper.SetColumn(3, InterfaceScrubberColumnHeader.SCRUBBER.ToDescription()),
                ColumnHelper.SetColumn(4, InterfaceScrubberColumnHeader.SCRUBBER_PRESSURE.ToDescription()),
                ColumnHelper.SetColumn(5, InterfaceScrubberColumnHeader.SCRUBBER_TEMPERATURE.ToDescription())
            };
        }

        protected List<ColumnHelper> GetActualPlanningColumn()
        {
            return new List<ColumnHelper>
            {
                ColumnHelper.SetColumn(0, ActualPlanningColumnHeader.AREA.ToDescription()),
                ColumnHelper.SetColumn(1, ActualPlanningColumnHeader.PLANNING.ToDescription()),
                ColumnHelper.SetColumn(2, ActualPlanningColumnHeader.LAST_ACTUAL.ToDescription()),
                ColumnHelper.SetColumn(3, ActualPlanningColumnHeader.LAST_FINDINGS.ToDescription())
            };
        }
    }


    public class TaskParamStub
    {
        public TaskReportFormStub TaskReport { get; set; }
        public IEnumerable<Business.Entities.PowerPlant> PowerPlants { get; set; }
        public IEnumerable<Well> Wells { get; set; }
        public IEnumerable<MstSeparator> Separators { get; set; }
        public IEnumerable<InterfacePoint> InterfacePoints { get; set; }
        public IEnumerable<Scrubber> Scrubbers { get; set; }
        public IEnumerable<ViewWell> ProductionWells { get; set; }
        public IEnumerable<ViewWell> InjectionWells { get; set; }
        public IEnumerable<ViewWell> MonitoringWells { get; set; }

        public TaskParamStub()
        {
            TaskReport = new TaskReportFormStub();
        }
    }

    public class ModelFromExcel
    {
        public TaskReportFormStub TaskReportStub { get; set; }
        public List<PltpImportFormStub> PltpModels { get; set; }
        public List<Business.Entities.PowerPlant> PowerPLants { get; set; }
        public ModelFromExcel()
        {
            TaskReportStub = new TaskReportFormStub();
            PltpModels = new List<PltpImportFormStub>();
            PowerPLants = new List<Business.Entities.PowerPlant>();
        }
    }

    public class TimeSpanHelper
    {
        public static List<string> GenerateTimeSpan()
        {
            List<string> options = new List<string>();
            DateTime start = DateTime.ParseExact("00:00", "HH:mm", null);
            DateTime end = DateTime.ParseExact("23:59", "HH:mm", null);
            TimeSpan interval = new TimeSpan(0, 1, 0); //interval every one minute

            while (start <= end)
            {
                options.Add(start.ToString(DisplayFormat.SqlShortTimeFormat));
                start = start.Add(interval);
            }
            options.Add("24:00");
            return options;
        }
    }
}