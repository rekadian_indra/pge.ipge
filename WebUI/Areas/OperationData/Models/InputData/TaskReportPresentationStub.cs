﻿using Business.Entities;
using Common.Enums;
using System;
using System.Linq;

namespace WebUI.Models
{
    public class TaskReportPresentationStub : BasePresentationStub<TaskReport, TaskReportPresentationStub>
    {
        public int Id { get; set; }
        public int AreaId { get; set; }
        public string Name { get; set; }
        public DateTime? Periode { get; set; }
        public string SourceInput { get; set; }
        public string Status { get; set; }
        public string ApprovedBy { get; set; }
        public bool IsDeleted { get; set; }
        public bool HasApproved { get; set; }

        public bool canSpvEdit { get; set; }
        public bool HasSalesOrder { get; set; }

        public TaskReportPresentationStub() : base() {

        }
        public TaskReportPresentationStub(TaskReport dbObject) : base(dbObject)
        {
            Name = dbObject.Area.Name;
            SourceInput = ((InputSource)Enum.Parse(typeof(InputSource), dbObject.SourceInput)).ToDescription();
            Status = ((DataStatus)Enum.Parse(typeof(DataStatus), dbObject.Status)).ToDescription();
            HasApproved = dbObject.Status == DataStatus.APPROVED.ToString() ? true : false;
            HasSalesOrder = dbObject.Pltps.Any(n => n.PowerPlant.SalesOrders.Any(m=>m.Workflow.Name != Common.Enums.Workflow.DRAFT.ToString()));
        }
        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}