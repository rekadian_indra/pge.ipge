﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using WebUI.Infrastructure;

namespace WebUI.Areas.OperationData.Models.DataHistory
{
    public class DataHistoryParamStub
    {
        [DisplayName("Area")]
        public int AreaId { get; set; }
        [DisplayName("From")]
        [DisplayFormat(DataFormatString = DisplayFormat.JsIdDateFormat, ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        [DisplayName("To")]
        [DisplayFormat(DataFormatString = DisplayFormat.JsIdDateFormat, ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }
        [DisplayName("Target")]
        public string TargetWellHistory { get; set; }
    }
}