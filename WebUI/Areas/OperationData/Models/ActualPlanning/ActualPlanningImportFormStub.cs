﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class ActualPlanningImportFormStub : BaseFormStub<ActualPlanning, ActualPlanningImportFormStub>
    {
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? AreaId { get; set; }
        public string Planning { get; set; }
        public string Actual { get; set; }
        public string ActualCase { get; set; }

        public ActualPlanningImportFormStub() : base()
        {

        }

        public override void MapDbObject(ActualPlanning dbObject)
        {
            base.MapDbObject(dbObject);
        }

        public void UpdateDbObject(ActualPlanning dbObject)
        {
            dbObject.Planning = Planning;
            dbObject.Actual = Actual;
            dbObject.ActualCase = ActualCase;

            dbObject.ModifiedBy = User.UserName;
            dbObject.ModifiedDateTimeUtc = DateTime.Now;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}