﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class ActualPlanFormStub : BaseFormStub<ActualPlanning, ActualPlanFormStub>
    {
        public int Id { get; set; }
        public int? TaskReportId { get; set; }

        [Display(Name = "Area")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? AreaId { get; set; }

        [Display(Name = "Rencana Pekerjaan")]
        //[Remote(ActionSite.PlannigExist, ControllerSite.InputData, AreaSite.OperationData, AdditionalFields = nameof(CurrentPlanning), ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Planning { get; set; }

        [Display(Name = "Realisasi Kemarin")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Actual { get; set; }

        [Display(Name = "Temuan Kemarin")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string ActualCase { get; set; }

        //public string CurrentPlanning { get; set; }


        public ActualPlanFormStub() : base()
        {
                
        }

        public ActualPlanFormStub(ActualPlanning dbObject) : base(dbObject)
        {
            AreaId = dbObject.TaskReport.AreaId.Value;
            //CurrentPlanning = dbObject.Planning;
        }

        public override void MapDbObject(ActualPlanning dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}