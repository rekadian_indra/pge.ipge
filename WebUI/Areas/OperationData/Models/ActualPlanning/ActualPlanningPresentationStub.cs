﻿using Business.Entities;
using Common.Enums;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class ActualPlanningPresentationStub : BasePresentationStub<ActualPlanning, ActualPlanningPresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? AreaId { get; set; }
        public string Name { get; set; }
        public string Planning { get; set; }
        public string Actual { get; set; }
        public string ActualCase { get; set; }
        public bool HasApproved { get; set; }

        #endregion

        public ActualPlanningPresentationStub() : base()
        {

        }

        public ActualPlanningPresentationStub(ActualPlanning dbObject) : base(dbObject)
        {
            Name = dbObject.Area.Name;
            HasApproved = dbObject.TaskReport.Status == DataStatus.APPROVED.ToString() ? true : false;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}