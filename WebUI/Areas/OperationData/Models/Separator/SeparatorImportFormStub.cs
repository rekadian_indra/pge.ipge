﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class SeparatorImportFormStub : BaseFormStub<Separator, SeparatorImportFormStub>
    {
        public int? TaskReportId { get; set; }
        public int? SeparatorId { get; set; }
        public double? Pressure { get; set; }
        public double? Temperature { get; set; }
        public double? LevelBrine { get; set; }
        public double? ThrottleValveBrine { get; set; }
        public double? ThrottleValveSteam { get; set; }
        public double? SteamFlowRate { get; set; }
        public double? BrineFlowRate { get; set; }
        public string PressureUnit { get; set; }
        public string TemperatureUnit { get; set; }
        public string LevelBrineUnit { get; set; }
        public string ThrottleValveBrineUnit { get; set; }
        public string ThrottleValveSteamUnit { get; set; }
        public string SteamFlowRateUnit { get; set; }
        public string BrineFlowRateUnit { get; set; }


        public SeparatorImportFormStub() : base()
        {

        }
        public override void MapDbObject(Separator dbObject)
        {
            base.MapDbObject(dbObject);

            if (Pressure == null) { dbObject.Pressure = 0; }
            if (Temperature == null) { dbObject.Temperature = 0; }
            if (LevelBrine == null) { dbObject.LevelBrine = 0; }
            if (ThrottleValveBrine == null) { dbObject.ThrottleValveBrine = 0; }
            if (ThrottleValveSteam == null) { dbObject.ThrottleValveSteam = 0; }
            if (SteamFlowRate == null) { dbObject.SteamFlowRate = 0; }
            if (BrineFlowRate == null) { dbObject.BrineFlowRate = 0; }
        }

        public void UpdateDbObject(Separator dbObject)
        {
            if (Pressure == null) { Pressure = 0; }
            if (Temperature == null) { Temperature = 0; }
            if (LevelBrine == null) { LevelBrine = 0; }
            if (ThrottleValveBrine == null) { ThrottleValveBrine = 0; }
            if (ThrottleValveSteam == null) { ThrottleValveSteam = 0; }
            if (SteamFlowRate == null) { SteamFlowRate = 0; }
            if (BrineFlowRate == null) { BrineFlowRate = 0; }

            dbObject.Pressure = Pressure;
            dbObject.Temperature = Temperature;
            dbObject.LevelBrine = LevelBrine;
            dbObject.ThrottleValveBrine = ThrottleValveBrine;
            dbObject.ThrottleValveSteam = ThrottleValveSteam;
            dbObject.SteamFlowRate = SteamFlowRate;
            dbObject.BrineFlowRate = BrineFlowRate;
            dbObject.PressureUnit = PressureUnit;
            dbObject.TemperatureUnit = TemperatureUnit;
            dbObject.LevelBrineUnit = LevelBrineUnit;
            dbObject.ThrottleValveBrineUnit = ThrottleValveBrineUnit;
            dbObject.ThrottleValveSteamUnit = ThrottleValveSteamUnit;
            dbObject.SteamFlowRateUnit = SteamFlowRateUnit;
            dbObject.BrineFlowRateUnit = BrineFlowRateUnit;

            dbObject.ModifiedBy = User.UserName;
            dbObject.ModifiedDateTimeUtc = DateTime.Now;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}