﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class SeparatorFormStub : BaseFormStub<Separator, SeparatorFormStub>
    {
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int AreaId { get; set; }
        public DateTime Periode { get; set; }

        [Display(Name = "Separator")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? SeparatorId { get; set; }

        [Display(Name = "Tekanan")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? Pressure { get; set; }

        [Display(Name = "Suhu")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? Temperature { get; set; }

        [Display(Name = "Level Brine (%)")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? LevelBrine { get; set; }

        [Display(Name = "Throttle Valve Opening Brine (%)")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? ThrottleValveBrine { get; set; }

        [Display(Name = "Throttle Valve Opening Stream (%)")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? ThrottleValveSteam { get; set; }

        [Display(Name = "Steam Flow Rata2")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? SteamFlowRate { get; set; }

        [Display(Name = "Brine Flow Rata2")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? BrineFlowRate { get; set; }
        [Display(Name = "Unit")]
        public string PressureUnit { get; set; }
        [Display(Name = "Unit")]
        public string TemperatureUnit { get; set; }
        [Display(Name = "Unit")]
        public string LevelBrineUnit { get; set; }
        [Display(Name = "Unit")]
        public string ThrottleValveBrineUnit { get; set; }
        [Display(Name = "Unit")]
        public string ThrottleValveSteamUnit { get; set; }
        [Display(Name = "Unit")]
        public string SteamFlowRateUnit { get; set; }
        [Display(Name = "Unit")]
        public string BrineFlowRateUnit { get; set; }


        public SeparatorFormStub() : base()
        {

        }

        public SeparatorFormStub(Separator dbObject) : base(dbObject)
        {
            AreaId = dbObject.TaskReport.AreaId.Value;
        }

        public override void MapDbObject(Separator dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}