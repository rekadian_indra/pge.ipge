﻿using Business.Entities;
using Common.Enums;
using System;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class SeparatorPresentationStub : BasePresentationStub<Separator, SeparatorPresentationStub>
    {

        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? SeparatorId { get; set; }
        public string Name { get; set; }
        public double? Pressure { get; set; }
        public double? Temperature { get; set; }
        public double? LevelBrine { get; set; }
        public double? ThrottleValveBrine { get; set; }
        public double? ThrottleValveSteam { get; set; }
        public double? SteamFlowRate { get; set; }
        public double? BrineFlowRate { get; set; }
        public bool HasApproved { get; set; }
        public string PressureUnit { get; set; }
        public string TemperatureUnit { get; set; }
        public string LevelBrineUnit { get; set; }
        public string ThrottleValveBrineUnit { get; set; }
        public string ThrottleValveSteamUnit { get; set; }
        public string SteamFlowRateUnit { get; set; }
        public string BrineFlowRateUnit { get; set; }
        public DateTime Periode { get; set; }
        #endregion

        public SeparatorPresentationStub() : base()
        {

        }

        public SeparatorPresentationStub(Separator dbObject) : base(dbObject)
        {            
            Name = dbObject.MstSeparator != null ?  dbObject.MstSeparator.Name : "";
            HasApproved = dbObject.TaskReport.Status == DataStatus.APPROVED.ToString() ? true : false;
            Periode = dbObject.TaskReport.Periode.Value;
            Pressure = dbObject.Pressure != null ? Pressure : 0;
            Temperature = dbObject.Temperature != null ? Temperature : 0;
            LevelBrine = dbObject.LevelBrine != null ? LevelBrine : 0;
            ThrottleValveBrine = dbObject.ThrottleValveBrine != null ? ThrottleValveBrine : 0;
            ThrottleValveSteam = dbObject.ThrottleValveSteam != null ? ThrottleValveSteam : 0;
            SteamFlowRate = dbObject.SteamFlowRate != null ? SteamFlowRate : 0;
            BrineFlowRate = dbObject.BrineFlowRate != null ? BrineFlowRate : 0;

            if (!string.IsNullOrEmpty(PressureUnit))
            {
                PressureUnit = ((PressureUnit)Enum.Parse(typeof(PressureUnit), dbObject.PressureUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(TemperatureUnit))
            {
                TemperatureUnit = ((TemperatueUnit)Enum.Parse(typeof(TemperatueUnit), dbObject.TemperatureUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(LevelBrineUnit))
            {
                LevelBrineUnit = ((OtherUnit)Enum.Parse(typeof(OtherUnit), dbObject.LevelBrineUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(ThrottleValveBrineUnit))
            {
                ThrottleValveBrineUnit = ((OtherUnit)Enum.Parse(typeof(OtherUnit), dbObject.ThrottleValveBrineUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(ThrottleValveSteamUnit))
            {
                ThrottleValveSteamUnit = ((OtherUnit)Enum.Parse(typeof(OtherUnit), dbObject.ThrottleValveSteamUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(SteamFlowRateUnit))
            {
                SteamFlowRateUnit = ((FlowUnit)Enum.Parse(typeof(FlowUnit), dbObject.SteamFlowRateUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(BrineFlowRateUnit))
            {
                BrineFlowRateUnit = ((FlowUnit)Enum.Parse(typeof(FlowUnit), dbObject.BrineFlowRateUnit)).ToDescription();
            }
        }

        protected override void Init()
        {
        }
    }
}