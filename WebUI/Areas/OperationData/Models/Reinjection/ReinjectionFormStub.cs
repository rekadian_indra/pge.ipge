﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class ReinjectionFormStub : BaseFormStub<ReInjection, ReinjectionFormStub>
    {
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int AreaId { get; set; }
        public DateTime Periode { get; set; }

        [Display(Name = "Sumur")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? WellId { get; set; }

        [Display(Name = "TKS")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? Tks { get; set; }

        [Display(Name = "Tekanan Jalur")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? PressureLine { get; set; }

        [Display(Name = "Suhu Jalur")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? Temperature { get; set; }

        [Display(Name = "Ph")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? Ph { get; set; }

        [Display(Name = "Flow Rata2")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? FlowRate { get; set; }

        [Display(Name = "Flow Total 24 Jam")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? FlowTotal { get; set; }

        [Display(Name = "Fluida yg Diinjeksikan")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Fluida { get; set; }

        [Display(Name = "Status Sumur")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Status { get; set; }

        [Display(Name = "Keterangan")]
        public string Notes { get; set; }
        [Display(Name = "Unit")]
        public string TksUnit { get; set; }
        [Display(Name = "Unit")]
        public string PressureLineUnit { get; set; }
        [Display(Name = "Unit")]
        public string TemperatureUnit { get; set; }
        [Display(Name = "Unit")]
        public string FlowRateUnit { get; set; }
        [Display(Name = "Unit")]
        public string FlowTotalUnit { get; set; }


        public ReinjectionFormStub() : base()
        {

        }

        public ReinjectionFormStub(ReInjection dbObject) : base(dbObject)
        {
            AreaId = dbObject.TaskReport.AreaId.Value;
            Status = DrawWellStatus.INJECTION.ToString();
        }

        public override void MapDbObject(ReInjection dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}