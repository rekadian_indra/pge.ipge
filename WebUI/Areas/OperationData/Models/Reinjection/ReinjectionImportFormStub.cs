﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class ReinjectionImportFormStub : BaseFormStub<ReInjection, ReinjectionImportFormStub>
    {
        //public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? WellId { get; set; }
        public double? Tks { get; set; }
        public double? PressureLine { get; set; }
        public double? Temperature { get; set; }
        public double? Ph { get; set; }
        public double? FlowRate { get; set; }
        public double? FlowTotal { get; set; }
        public string Fluida { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public string TksUnit { get; set; }
        public string PressureLineUnit { get; set; }
        public string TemperatureUnit { get; set; }
        public string FlowRateUnit { get; set; }
        public string FlowTotalUnit { get; set; }


        public ReinjectionImportFormStub() : base()
        {

        }

        public override void MapDbObject(ReInjection dbObject)
        {
            base.MapDbObject(dbObject);

            if (Tks == null) { dbObject.Tks = 0; }
            if (PressureLine == null) { dbObject.PressureLine = 0; }
            if (Temperature == null) { dbObject.Temperature = 0; }
            if (Ph == null) { dbObject.Ph = 0; }
            if (FlowRate == null) { dbObject.FlowRate = 0; }
            if (FlowTotal == null) { dbObject.FlowTotal = 0; }
        }

        public void UpdateDbObject(ReInjection dbObject)
        {
            if (Tks == null) { Tks = 0; }
            if (PressureLine == null) { PressureLine = 0; }
            if (Temperature == null) { Temperature = 0; }
            if (Ph == null) { Ph = 0; }
            if (FlowRate == null) { FlowRate = 0; }
            if (FlowTotal == null) { FlowTotal = 0; }

            dbObject.Tks = Tks;
            dbObject.PressureLine = PressureLine;
            dbObject.Temperature = Temperature;
            dbObject.Ph = Ph;
            dbObject.FlowRate = FlowRate;
            dbObject.FlowTotal = FlowTotal;
            dbObject.Fluida = Fluida;
            dbObject.Status = Status;
            dbObject.Notes = Notes;
            dbObject.TksUnit = TksUnit;
            dbObject.PressureLineUnit = PressureLineUnit;
            dbObject.TemperatureUnit = TemperatureUnit;
            dbObject.FlowRateUnit = FlowRateUnit;
            dbObject.FlowTotalUnit = FlowTotalUnit;

            dbObject.ModifiedBy = User.UserName;
            dbObject.ModifiedDateTimeUtc = DateTime.Now;

        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}