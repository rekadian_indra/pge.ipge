﻿using Business.Entities;
using Common.Enums;
using System;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class ReinjectionPresentationStub : BasePresentationStub<ReInjection, ReinjectionPresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? WellId { get; set; }
        public string Name { get; set; }
        public double? Tks { get; set; }
        public double? PressureLine { get; set; }
        public double? Temperature { get; set; }
        public double? Ph { get; set; }
        public double? FlowRate { get; set; }
        public double? FlowTotal { get; set; }
        public string Fluida { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public bool HasApproved { get; set; }
        public string TksUnit { get; set; }
        public string PressureLineUnit { get; set; }
        public string TemperatureUnit { get; set; }
        public string FlowRateUnit { get; set; }
        public string FlowTotalUnit { get; set; }
        public DateTime Periode { get; set; }
        #endregion

        public ReinjectionPresentationStub() : base()
        {

        }

        public ReinjectionPresentationStub(ReInjection dbObject) : base(dbObject)
        {
            Name = dbObject.Well.Name;
            Status = !string.IsNullOrEmpty(dbObject.Status) ? ((DrawWellStatus)Enum.Parse(typeof(DrawWellStatus), dbObject.Status.Trim())).ToDescription() : string.Empty;
            HasApproved = dbObject.TaskReport.Status == DataStatus.APPROVED.ToString() ? true : false;
            Periode = dbObject.TaskReport.Periode.Value;
            Tks = dbObject.Tks != null ? Tks : 0;
            PressureLine = dbObject.PressureLine != null ? PressureLine : 0;
            Temperature = dbObject.Temperature != null ? Temperature : 0;
            Ph = dbObject.Ph != null ? Ph : 0;
            FlowRate = dbObject.FlowRate != null ? FlowRate : 0;
            FlowTotal = dbObject.FlowTotal != null ? FlowTotal : 0;

            if (!string.IsNullOrEmpty(TksUnit))
            {
                TksUnit = ((PressureUnit)Enum.Parse(typeof(PressureUnit), dbObject.TksUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(PressureLineUnit))
            {
                PressureLineUnit = ((PressureUnit)Enum.Parse(typeof(PressureUnit), dbObject.PressureLineUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(TemperatureUnit))
            {
                TemperatureUnit = ((TemperatueUnit)Enum.Parse(typeof(TemperatueUnit), dbObject.TemperatureUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(FlowRateUnit))
            {
                FlowRateUnit = ((FlowUnit)Enum.Parse(typeof(FlowUnit), dbObject.FlowRateUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(FlowTotalUnit))
            {
                FlowTotalUnit = ((RockMufflerUnit)Enum.Parse(typeof(RockMufflerUnit), dbObject.FlowTotalUnit)).ToDescription();
            }
        }

        protected override void Init()
        {
            
        }
    }
}