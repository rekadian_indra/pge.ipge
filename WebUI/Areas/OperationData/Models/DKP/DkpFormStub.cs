﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class DkpFormStub : BaseFormStub<Dkp, DkpFormStub>
    {
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? AreaId { get; set; }
        public DateTime Periode { get; set; }

        [Display(Name = "PLTP")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? PowerPlantId { get; set; }

        [Display(Name = "Status Gangguan")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Status { get; set; }

        [Display(Name = "Jam Awal Gangguan")]
        [RequiredIf("Status", "NORMAL", AllowEmptyStrings = true, Operator = Common.Enums.FilterOperator.NotEquals, ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public TimeSpan? StartHour { get; set; }

        [Display(Name = "Jam Akhir Gangguan")]
        [RequiredIf("Status", "NORMAL", AllowEmptyStrings = true, Operator = Common.Enums.FilterOperator.NotEquals, ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public TimeSpan? EndHour { get; set; }

        [Display(Name = "Derating (MW)")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [RequiredIf("Status", "NORMAL", AllowEmptyStrings = true, Operator = Common.Enums.FilterOperator.NotEquals, ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? Derating { get; set; }

        [Display(Name = "Lokasi Gangguan")]        
        [RequiredIf("Status", "NORMAL", AllowEmptyStrings =true, Operator = Common.Enums.FilterOperator.NotEquals, ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Location { get; set; }

        [Display(Name = "Penyebab Gangguan")]
        [RequiredIf("Status", "NORMAL", AllowEmptyStrings = true, Operator = Common.Enums.FilterOperator.NotEquals, ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Cause { get; set; }

        [Display(Name = "Keterangan")]
        public string Notes { get; set; }


        public DkpFormStub() : base()
        {
        }

        public DkpFormStub(Dkp dbObject) : base(dbObject)
        {
            ObjectMapper.MapObject<Dkp, DkpFormStub>(dbObject, this);
            AreaId = dbObject.TaskReport.AreaId.Value;
            Periode = dbObject.TaskReport.Periode.Value;
        }

        public override void MapDbObject(Dkp dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor            
        }
    }
}