﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class DkpImportFormStub : BaseFormStub<Dkp, DkpImportFormStub>
    {
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? PowerPlantId { get; set; }
        public string Status { get; set; }
        public TimeSpan? StartHour { get; set; }
        public TimeSpan? EndHour { get; set; }
        public double? Derating { get; set; }
        public string Location { get; set; }
        public string Cause { get; set; }
        public string Notes { get; set; }

        public DkpImportFormStub() : base()
        {

        }

        public override void MapDbObject(Dkp dbObject)
        {
            base.MapDbObject(dbObject);

            //if (Derating == null) { dbObject.Derating = 0; }

            //if(Status != "")
            //{
            //    try
            //    {
            //        dbObject.Status = (EnumExtension.ToEnum<InterruptionStatus>(Status)).ToString();
            //    }
            //    catch (Exception)
            //    {
            //        try
            //        {
            //            dbObject.Status = (EnumExtension.ToEnum<InterruptionStatus>(Status.ToUpper())).ToString();
            //        }
            //        catch (Exception)
            //        {
            //            dbObject.Status = InterruptionStatus.NORMAL.ToString();
            //        }
            //    }
            //}
        }

        public void UpdateDbObject(Dkp dbObject)
        {
            //if (Derating == null) { Derating = 0; }

            try
            {
                dbObject.Status = (EnumExtension.ToEnum<InterruptionStatus>(Status)).ToString();
            }
            catch (Exception)
            {
                dbObject.Status = Status;
            }
            dbObject.StartHour = StartHour;
            dbObject.EndHour = EndHour;
            dbObject.Derating = Derating;
            dbObject.Location = Location;
            dbObject.Cause = Cause;
            dbObject.Notes = Notes;

            dbObject.ModifiedBy = User.UserName;
            dbObject.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor       
        }
    }
}