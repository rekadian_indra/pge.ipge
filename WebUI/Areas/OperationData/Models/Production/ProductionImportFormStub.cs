﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class ProductionImportFormStub : BaseFormStub<Production, ProductionImportFormStub>
    {
        //public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? WellId { get; set; }
        public double? Tks { get; set; }
        public double? PressureLine { get; set; }
        public double? Temperature { get; set; }
        public double? ThrottleValve { get; set; }
        public double? FlowRate { get; set; }
        public double? FlowTotal { get; set; }
        public double? PressureUpstream { get; set; }
        public double? DifferentialPressure { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public string TksUnit { get; set; }
        public string PressureLineUnit { get; set; }
        public string TemperatureUnit { get; set; }
        public string ThrottleValveUnit { get; set; }
        public string FlowRateUnit { get; set; }
        public string FlowTotalUnit { get; set; }
        public string PressureUpstreamUnit { get; set; }
        public string DifferentialPressureUnit { get; set; }


        public ProductionImportFormStub() : base()
        {

        }

        public override void MapDbObject(Production dbObject)
        {
            base.MapDbObject(dbObject);

            if (Tks == null) { dbObject.Tks = 0; }
            if (PressureLine == null) { dbObject.PressureLine = 0; }
            if (Temperature == null) { dbObject.Temperature = 0; }
            if (ThrottleValve == null) { dbObject.ThrottleValve = 0; }
            if (FlowRate == null) { dbObject.FlowRate = 0; }
            if (FlowTotal == null) { dbObject.FlowTotal = 0; }
            if (PressureUpstream == null) { dbObject.PressureUpstream = 0; }
            if (DifferentialPressure == null) { dbObject.DifferentialPressure = 0; }
    }

        public void UpdateDbObject(Production dbObject)
        {
            if (Tks == null) { Tks = 0; }
            if (PressureLine == null) { PressureLine = 0; }
            if (Temperature == null) { Temperature = 0; }
            if (ThrottleValve == null) { ThrottleValve = 0; }
            if (FlowRate == null) { FlowRate = 0; }
            if (FlowTotal == null) { FlowTotal = 0; }
            if (PressureUpstream == null) { PressureUpstream = 0; }
            if (DifferentialPressure == null) { DifferentialPressure = 0; }

            dbObject.Tks = Tks;
            dbObject.PressureLine = PressureLine;
            dbObject.Temperature = Temperature;
            dbObject.ThrottleValve = ThrottleValve;
            dbObject.FlowRate = FlowRate;
            dbObject.FlowTotal = FlowTotal;
            dbObject.PressureUpstream = PressureUpstream;
            dbObject.DifferentialPressure = DifferentialPressure;
            dbObject.Status = Status;
            dbObject.Notes = Notes;
            dbObject.TksUnit = TksUnit;
            dbObject.PressureLineUnit = PressureLineUnit;
            dbObject.TemperatureUnit = TemperatureUnit;
            dbObject.ThrottleValveUnit = ThrottleValveUnit;
            dbObject.FlowRateUnit = FlowRateUnit;
            dbObject.FlowTotalUnit = FlowTotalUnit;
            dbObject.PressureUpstreamUnit = PressureUpstreamUnit;
            dbObject.DifferentialPressureUnit = DifferentialPressureUnit;

            dbObject.ModifiedBy = User.UserName;
            dbObject.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}