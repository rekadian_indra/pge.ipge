﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class ProductionFormStub : BaseFormStub<Production, ProductionFormStub>
    {
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int AreaId { get; set; }
        public DateTime Periode { get; set; }

        [Display(Name = "Sumur")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? WellId { get; set; }

        [Display(Name = "TKS")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? Tks { get; set; }

        [Display(Name = "Tekanan Jalur")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.CompareValue, ControllerSite.InputData, AdditionalFields = "Tks", ErrorMessage = "Tekanan Jalur harus lebih kecil atau sama dengan Tks")]
        public double? PressureLine { get; set; }

        [Display(Name = "Suhu Jalur")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? Temperature { get; set; }

        [Display(Name = "Throttle Valve Opening")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? ThrottleValve { get; set; }

        [Display(Name = "Flow Rata2")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? FlowRate { get; set; }

        [Display(Name = "Flow Total 24 Jam")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? FlowTotal { get; set; }

        [Display(Name = "Pressure Upstream")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? PressureUpstream { get; set; }

        [Display(Name = "Differential Pressure")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? DifferentialPressure { get; set; }

        [Display(Name = "Status Sumur")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Status { get; set; }

        [Display(Name = "Keterangan")]
        public string Notes { get; set; }

        [Display(Name = "Unit")]
        public string TksUnit { get; set; }
        [Display(Name = "Unit")]
        public string PressureLineUnit { get; set; }
        [Display(Name = "Unit")]
        public string TemperatureUnit { get; set; }
        [Display(Name = "Unit")]
        public string ThrottleValveUnit { get; set; }
        [Display(Name = "Unit")]
        public string FlowRateUnit { get; set; }
        [Display(Name = "Unit")]
        public string FlowTotalUnit { get; set; }
        [Display(Name = "Unit")]
        public string PressureUpstreamUnit { get; set; }
        [Display(Name = "Unit")]
        public string DifferentialPressureUnit { get; set; }


        public ProductionFormStub() : base()
        {

        }

        public ProductionFormStub(Production dbObject) : base(dbObject)
        {
            //ObjectMapper.MapObject<Production, ProductionFormStub>(dbObject, this);
            AreaId = dbObject.TaskReport.AreaId.Value;
            Status = DrawWellStatus.PRODUCTION.ToString();
        }

        public override void MapDbObject(Production dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}