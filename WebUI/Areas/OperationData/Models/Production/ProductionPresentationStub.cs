﻿using Business.Entities;
using Common.Enums;
using System;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class ProductionPresentationStub : BasePresentationStub<Production, ProductionPresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? WellId { get; set; }
        public string Name { get; set; }
        public double? Tks { get; set; }
        public double? PressureLine { get; set; }
        public double? Temperature { get; set; }
        public double? ThrottleValve { get; set; }
        public double? FlowRate { get; set; }
        public double? FlowTotal { get; set; }
        public double? PressureUpstream { get; set; }
        public double? DifferentialPressure { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public bool HasApproved { get; set; }
        public string TksUnit { get; set; }
        public string PressureLineUnit { get; set; }
        public string TemperatureUnit { get; set; }
        public string ThrottleValveUnit { get; set; }
        public string FlowRateUnit { get; set; }
        public string FlowTotalUnit { get; set; }
        public string PressureUpstreamUnit { get; set; }
        public string DifferentialPressureUnit { get; set; }
        public DateTime Periode { get; set; }
        #endregion

        public ProductionPresentationStub() : base()
        {

        }

        public ProductionPresentationStub(Production dbObject) : base(dbObject)
        {
            Name = dbObject.Well.Name;
            Status = !string.IsNullOrEmpty(dbObject.Status) ? ((DrawWellStatus)Enum.Parse(typeof(DrawWellStatus), dbObject.Status.Trim())).ToDescription() : string.Empty;
            HasApproved = dbObject.TaskReport.Status == DataStatus.APPROVED.ToString() ? true : false;
            Periode = dbObject.TaskReport.Periode.Value;
            Tks = dbObject.Tks != null ? Tks : 0;
            PressureLine = dbObject.PressureLine != null ? PressureLine : 0;
            Temperature = dbObject.Temperature != null ? Temperature : 0;
            ThrottleValve = dbObject.ThrottleValve != null ? ThrottleValve : 0;
            FlowRate = dbObject.FlowRate != null ? FlowRate : 0;
            FlowTotal = dbObject.FlowTotal != null ? FlowTotal : 0;
            PressureUpstream = dbObject.PressureUpstream != null ? PressureUpstream : 0;
            DifferentialPressure = dbObject.DifferentialPressure != null ? DifferentialPressure : 0;

            if (!string.IsNullOrEmpty(TksUnit))
            {
                TksUnit = ((PressureUnit)Enum.Parse(typeof(PressureUnit), dbObject.TksUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(PressureLineUnit))
            {
                PressureLineUnit = ((PressureUnit)Enum.Parse(typeof(PressureUnit), dbObject.PressureLineUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(TemperatureUnit))
            {
                TemperatureUnit = ((TemperatueUnit)Enum.Parse(typeof(TemperatueUnit), dbObject.TemperatureUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(ThrottleValveUnit))
            {
                ThrottleValveUnit = ((OtherUnit)Enum.Parse(typeof(OtherUnit), dbObject.ThrottleValveUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(FlowRateUnit))
            {
                FlowRateUnit = ((FlowUnit)Enum.Parse(typeof(FlowUnit), dbObject.FlowRateUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(FlowTotalUnit))
            {
                FlowTotalUnit = ((RockMufflerUnit)Enum.Parse(typeof(RockMufflerUnit), dbObject.FlowTotalUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(PressureUpstreamUnit))
            {
                PressureUpstreamUnit = ((PressureUnit)Enum.Parse(typeof(PressureUnit), dbObject.PressureUpstreamUnit)).ToDescription();
            }
            if (!string.IsNullOrEmpty(DifferentialPressureUnit))
            {
                DifferentialPressureUnit = ((PressureUnit)Enum.Parse(typeof(PressureUnit), dbObject.DifferentialPressureUnit)).ToDescription();
            }
        }

        protected override void Init()
        {
        }
    }
}