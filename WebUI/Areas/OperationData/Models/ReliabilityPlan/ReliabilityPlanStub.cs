﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
	public class ReliabilityPlanStub
	{
		[DisplayName("Power Plant")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public int PowerPlantId { get; set; }

		[DisplayName("Area")]
		public int AreaId { get; set; }

		[DisplayName("Tahun")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public int Year { get; set; }

		public List<ReliabilityPlanFormStub> MonthlyPlans { get; set; }

		public bool IsReadyInput { get; set; }

		public ReliabilityPlanStub()
		{
			IsReadyInput = false;
		}

		public ReliabilityPlanStub(int areaId, int powerPlantId, int year) : this()
		{
			AreaId = areaId;
			PowerPlantId = powerPlantId;
			Year = year;
			MonthlyPlans = new List<ReliabilityPlanFormStub>();
			for (int month = 1; month <= 12; month++)
			{
				MonthlyPlans.Add(new ReliabilityPlanFormStub(powerPlantId, year, month));
			}
			IsReadyInput = true;
		}

		public ReliabilityPlanStub(int areaId, int powerPlantId, int year, List<ReliabilityPlan> reliabilityPlans) : this(areaId, powerPlantId, year)
		{
			MonthlyPlans = ListMapper.MapList<ReliabilityPlan, ReliabilityPlanFormStub>(reliabilityPlans);
			MonthlyPlans = MonthlyPlans.OrderBy(x => x.Periode).ToList();
            var temp = MonthlyPlans;
            temp.ForEach(n => n.EFDH = n.EFDH * 100);
            temp.ForEach(n => n.EMDH = n.EMDH * 100);
            temp.ForEach(n => n.EPDH = n.EPDH * 100);
            temp.ForEach(n => n.FOH = n.FOH * 100);
            temp.ForEach(n => n.MOH = n.MOH * 100);
            temp.ForEach(n => n.POH = n.POH * 100);
            MonthlyPlans = temp.ToList();
        }

		public void MapDbObjects(List<ReliabilityPlan> reliabilityPlans)
		{
			foreach (ReliabilityPlanFormStub monthlyPlan in MonthlyPlans)
			{
                monthlyPlan.EFDH /= 100;
                monthlyPlan.EMDH /= 100;
                monthlyPlan.EPDH /= 100;
                monthlyPlan.FOH /= 100;
                monthlyPlan.MOH /= 100;
                monthlyPlan.POH /= 100;

				if (monthlyPlan.Id != 0)
				{
					ReliabilityPlan curReliabilityPlan = reliabilityPlans.FirstOrDefault(x => x.Id == monthlyPlan.Id);
					if (curReliabilityPlan == null)
					{
						curReliabilityPlan = new ReliabilityPlan();
					}
					monthlyPlan.MapDbObject(curReliabilityPlan);
				}
				else
				{
					ReliabilityPlan curReliabilityPlan = new ReliabilityPlan();
					monthlyPlan.MapDbObject(curReliabilityPlan);
					reliabilityPlans.Add(curReliabilityPlan);
				}
			}
		}
	}
}