﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
	public class ReliabilityPlanFormStub : BaseFormStub<ReliabilityPlan, ReliabilityPlanFormStub>
	{
		public int Id { get; set; }

		[DisplayName("Power Plant")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public int PowerPlantId { get; set; }

		public System.DateTime Periode { get; set; }
		public double FOH { get; set; }
		public double MOH { get; set; }
		public double POH { get; set; }
		public double EFDH { get; set; }
		public double EMDH { get; set; }
		public double EPDH { get; set; }

		public string MonthStr { get; set; }
		public int Month { get; set; }
		public int Year { get; set; }

		public ReliabilityPlanFormStub() : base()
		{

		}

		public ReliabilityPlanFormStub(int powerPlantId, int year, int month) : this()
		{
			PowerPlantId = powerPlantId;
			Periode = new DateTime(year, month, 1);

			Year = year;
			Month = month;
			MonthStr = Periode.ToString("MMMM");
		}

		public ReliabilityPlanFormStub(ReliabilityPlan dbObject) : base(dbObject)
		{
			MonthStr = Periode.ToString("MMMM");
			Year = Periode.Year;
			Month = Periode.Month;
		}

		public override void MapDbObject(ReliabilityPlan reliabilityPlan)
		{
			base.MapDbObject(reliabilityPlan);
		}

		protected override void Init()
		{

		}
	}
}