﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class ReliabilityPlanPresentationStub : ReliabilityPlanFormStub
	{
		public string PowerPlant { get; set; }

		public ReliabilityPlanPresentationStub() : base()
		{

		}

		public ReliabilityPlanPresentationStub(ReliabilityPlan dbObject) : base(dbObject)
		{
			PowerPlant = dbObject.PowerPlant.Name;
		}
	}
}