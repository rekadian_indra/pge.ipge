﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class MonitoringImportFormStub : BaseFormStub<Monitoring, MonitoringImportFormStub>
    {
        public int? TaskReportId { get; set; }
        public int? WellId { get; set; }
        public double? Tks { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public string TksUnit { get; set; }

        public MonitoringImportFormStub() : base()
        {

        }

        public override void MapDbObject(Monitoring dbObject)
        {
            base.MapDbObject(dbObject);

            if (Tks == null) { dbObject.Tks = 0; }
        }

        public void UpdateDbObject(Monitoring dbObject)
        {
            if (Tks == null) { Tks = 0; }

            dbObject.Tks = Tks;
            dbObject.Status = Status;
            dbObject.Notes = Notes;
            dbObject.TksUnit = TksUnit;

            dbObject.ModifiedBy = User.UserName;
            dbObject.ModifiedDateTimeUtc = DateTime.Now;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}