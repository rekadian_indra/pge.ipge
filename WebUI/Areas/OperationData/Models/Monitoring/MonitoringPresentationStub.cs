﻿using Business.Entities;
using Common.Enums;
using System;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class MonitoringPresentationStub : BasePresentationStub<Monitoring, MonitoringPresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int? WellId { get; set; }
        public string Name { get; set; }
        public double? Tks { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public bool HasApproved { get; set; }
        public string TksUnit { get; set; }
        public DateTime Periode { get; set; }
        #endregion

        public MonitoringPresentationStub() : base()
        {

        }

        public MonitoringPresentationStub(int id, string status) : this()
        {
            Id = id;
            Status = status;
        }

        public MonitoringPresentationStub(Monitoring dbObject) : base(dbObject)
        {
            Name = dbObject.Well.Name;
            Status = !string.IsNullOrEmpty(dbObject.Status) ? ((DrawWellStatus)Enum.Parse(typeof(DrawWellStatus), dbObject.Status.Trim())).ToDescription() : string.Empty;
            HasApproved = dbObject.TaskReport.Status == DataStatus.APPROVED.ToString() ? true : false;
            Periode = dbObject.TaskReport.Periode.Value;
            Tks = dbObject.Tks != null ? Tks : 0;

            if (!string.IsNullOrEmpty(TksUnit))
                TksUnit = ((PressureUnit)Enum.Parse(typeof(PressureUnit), dbObject.TksUnit)).ToDescription();
        }

        protected override void Init()
        {
        }
    }
}