﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class MonitoringFormStub : BaseFormStub<Monitoring, MonitoringFormStub>
    {
        public int Id { get; set; }
        public int? TaskReportId { get; set; }
        public int AreaId { get; set; }
        public DateTime Periode { get; set; }

        [Display(Name = "Sumur")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? WellId { get; set; }

        [Display(Name = "TKS")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? Tks { get; set; }

        [Display(Name = "Status Sumur")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Status { get; set; }

        [Display(Name = "Keterangan")]
        public string Notes { get; set; }
        [Display(Name = "Unit")]
        public string TksUnit { get; set; }
        public MonitoringFormStub() : base()
        {

        }

        public MonitoringFormStub(Monitoring dbObject) : base(dbObject)
        {
            AreaId = dbObject.TaskReport.AreaId.Value;
            Status = DrawWellStatus.MONITORING.ToString();
        }

        public override void MapDbObject(Monitoring dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}