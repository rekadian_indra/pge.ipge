﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class PltpFormStub : BaseFormStub<Pltp, PltpFormStub>
    {
        public int Id { get; set; }
        public int? AreaId { get; set; }

        public int? TaskReportId { get; set; }
        public DateTime Periode { get; set; }

        [Display(Name = "Power Plant")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? PowerPlantId { get; set; }

        [Display(Name = "Energy Listrik 24 Jam (Gross)")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? EnergyGross { get; set; }

        [Display(Name = "Energy Listrik 24 Jam (Nett)")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? EnergyNett { get; set; }

        [Display(Name = "Pemanfaatan Uap oleh Pembangkit")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? SteamUtilization { get; set; }

        [Display(Name = "Buangan ke Rock Muffler")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? WasteRockMufller { get; set; }

        [Display(Name = "SSC")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? Ssc { get; set; }

        [Display(Name = "GPP Shutdown (Hour)")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? GppsShutdown { get; set; }

        [Display(Name = "Import Energy")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? ImportEnergy { get; set; }

        [Display(Name = "SGS Shutdown (Hour)")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = GlobalErrorField.Range, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double? SgssShutdown { get; set; }

        [Display(Name = "Keterangan")]
        public string Notes { get; set; }

        [Display(Name = "Unit")]
        public string EnergyGrossUnit { get; set; }
        [Display(Name = "Unit")]
        public string EnergyNettUnit { get; set; }
        [Display(Name = "Unit")]
        public string SteamUtilizationUnit { get; set; }
        [Display(Name = "Unit")]
        public string WasteRockMufllerUnit { get; set; }
        [Display(Name = "Unit")]
        public string SscUnit { get; set; }
        [Display(Name = "Unit")]
        public string GppsShutdownUnit { get; set; }
        [Display(Name = "Unit")]
        public string ImportEnergyUnit { get; set; }
        [Display(Name = "Unit")]
        public string SgssShutdownUnit { get; set; }

        public PltpFormStub() : base()
        {

        }

        public PltpFormStub(Pltp dbObject) : base(dbObject)
        {
            AreaId = dbObject.TaskReport.AreaId.Value;
        }

        public override void MapDbObject(Pltp dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor            
        }
    }
}