﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using NPOI;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class PltpImportFormStub : BaseFormStub<Pltp, PltpImportFormStub>
    {
        public int? TaskReportId { get; set; }
        public int? PowerPlantId { get; set; }
        public double? EnergyGross { get; set; }
        public double? EnergyNett { get; set; }
        public double? SteamUtilization { get; set; }
        public double? WasteRockMufller { get; set; }
        public double? Ssc { get; set; }
        public double? GppsShutdown { get; set; }
        public double? ImportEnergy { get; set; }
        public double? SgssShutdown { get; set; }
        public string Notes { get; set; }
        public string EnergyGrossUnit { get; set; }
        public string EnergyNettUnit { get; set; }
        public string SteamUtilizationUnit { get; set; }
        public string WasteRockMufllerUnit { get; set; }
        public string SscUnit { get; set; }
        public string GppsShutdownUnit { get; set; }
        public string ImportEnergyUnit { get; set; }
        public string SgssShutdownUnit { get; set; }

        public PltpImportFormStub() : base() { }

        public override void MapDbObject(Pltp dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here

            if (EnergyGross == null) { dbObject.EnergyGross = 0; }
            if (EnergyNett == null) { dbObject.EnergyNett = 0; }
            if (SteamUtilization == null) { dbObject.SteamUtilization = 0; }
            if (WasteRockMufller == null) { dbObject.WasteRockMufller = 0; }
            //if (Ssc == null) { dbObject.Ssc = 0; }

            if(SteamUtilization != null && EnergyGross != null)
            {
                dbObject.Ssc = SteamUtilization.Value / (EnergyGross.Value/1000);
                dbObject.Ssc = double.IsInfinity(dbObject.Ssc.Value) || double.IsNaN(dbObject.Ssc.Value) ? 0 : dbObject.Ssc;
            }
            else
            {
                dbObject.Ssc = 0;
            }

            
            if (GppsShutdown == null) { dbObject.GppsShutdown = 0; }
            if (ImportEnergy == null) { dbObject.ImportEnergy = 0; }
            if (SgssShutdown == null) { dbObject.SgssShutdown = 0; }
        }

        public void UpdateDbObject(Pltp dbObject)
        {
            if (EnergyGross == null) { EnergyGross = 0; }
            if (EnergyNett == null) { EnergyNett = 0; }
            if (SteamUtilization == null) { SteamUtilization = 0; }
            if (WasteRockMufller == null) { WasteRockMufller = 0; }
            if (Ssc == null) { Ssc = 0; }
            if (GppsShutdown == null) { GppsShutdown = 0; }
            if (ImportEnergy == null) { ImportEnergy = 0; }
            if (SgssShutdown == null) { SgssShutdown = 0; }

            dbObject.EnergyGross = EnergyGross;
            dbObject.EnergyNett = EnergyNett;
            dbObject.SteamUtilization = SteamUtilization;
            WasteRockMufller = dbObject.WasteRockMufller;
            dbObject.Ssc = Ssc;
            dbObject.GppsShutdown = GppsShutdown;
            dbObject.ImportEnergy = ImportEnergy;
            dbObject.SgssShutdown = SgssShutdown;
            dbObject.Notes = Notes;
            dbObject.EnergyGrossUnit = EnergyGrossUnit;
            dbObject.EnergyNettUnit = EnergyNettUnit;
            dbObject.SteamUtilizationUnit = SteamUtilizationUnit;
            dbObject.WasteRockMufllerUnit = WasteRockMufllerUnit;
            dbObject.SscUnit = SscUnit;
            dbObject.GppsShutdownUnit = GppsShutdownUnit;
            dbObject.ImportEnergyUnit = ImportEnergyUnit;
            dbObject.SgssShutdownUnit = SgssShutdownUnit;

            dbObject.ModifiedBy = User.UserName;
            dbObject.ModifiedDateTimeUtc = DateTime.Now.ToUtcDateTime();
        }

        public void MapDbObject(Pltp pltp, Business.Entities.PowerPlant plant, TaskParamStub param)
        {
            base.MapDbObject(pltp);
            pltp.TaskReportId = param.TaskReport.Id;
            pltp.PowerPlantId = plant.Id;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor            
        }
    }
}