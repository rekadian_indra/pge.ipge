﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
	public class InputPlanFormStub
	{
		[DisplayName("Power Plant")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public int PowerPlantId { get; set; }

		[DisplayName("Tahun")]
		[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
		public int Year { get; set; }

		public Business.Entities.PowerPlant PowerPlant { get; set; }

		public ProductionPlanFormStub ProductionPlanSteam { get; set; }
		public ProductionPlanFormStub ProductionPlanElectric { get; set; }
        public ProductionPlanFormStub ProductionPlanDaysEffective { get; set; }

		public bool IsReadyInput { get; set; }

        public InputPlanFormStub()
		{
			Year = DateTime.Now.Year;

			ProductionPlanSteam = new ProductionPlanFormStub(ProductionUtilization.STEAM, Year);
			ProductionPlanElectric = new ProductionPlanFormStub(ProductionUtilization.ELECTRIC, Year);
			ProductionPlanDaysEffective = new ProductionPlanFormStub(ProductionUtilization.DAYSEFFECTIVE, Year);

			IsReadyInput = false;
		}

		public InputPlanFormStub(int powerPlantId, int year)
		{
			PowerPlantId = powerPlantId;
			Year = year;

			ProductionPlanSteam = new ProductionPlanFormStub(ProductionUtilization.STEAM, Year);
			ProductionPlanElectric = new ProductionPlanFormStub(ProductionUtilization.ELECTRIC, Year);
			ProductionPlanDaysEffective = new ProductionPlanFormStub(ProductionUtilization.DAYSEFFECTIVE, Year);

			IsReadyInput = true;
		}

		public InputPlanFormStub(List<ProductionPlan> productionPlans, Business.Entities.PowerPlant powerPlant, int year)
		{
			PowerPlantId = powerPlant.Id;
			Year = year;
			PowerPlant = powerPlant;
			foreach (ProductionPlan productionPlan in productionPlans)
			{
				if (productionPlan.UtilType == ProductionUtilization.STEAM.ToString())
				{
					ProductionPlanSteam = new ProductionPlanFormStub(productionPlan);
				}
				else  if (productionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString())
				{
					ProductionPlanElectric = new ProductionPlanFormStub(productionPlan);
				}
                else
                {
                    ProductionPlanDaysEffective = new ProductionPlanFormStub(productionPlan);
                }
			}

			if (ProductionPlanDaysEffective == null)
			{
				ProductionPlanDaysEffective = new ProductionPlanFormStub(ProductionUtilization.DAYSEFFECTIVE, year)
				{
					PowerPlantId = powerPlant.Id,
					
				};
				foreach (ProductionPlanDetailFormStub detail in ProductionPlanDaysEffective.ProductionPlanDetails)
				{
					detail.Value = 0;
				}
			}

			IsReadyInput = true;
		}

		public void MapDbObject(ProductionPlan productionPlanSteam, ProductionPlan productionPlanElectric, ProductionPlan productionPlanDaysEffective)
		{
			ProductionPlanSteam.PowerPlantId = ProductionPlanElectric.PowerPlantId = ProductionPlanDaysEffective.PowerPlantId = PowerPlantId;
			ProductionPlanSteam.Year = ProductionPlanElectric.Year = ProductionPlanDaysEffective.Year = Year;

			ProductionPlanSteam.MapDbObject(productionPlanSteam);
			ProductionPlanElectric.MapDbObject(productionPlanElectric);
            ProductionPlanDaysEffective.MapDbObject(productionPlanDaysEffective);

        }

		public void MapDbObject(List<ProductionPlan> productionPlans)
		{
			ProductionPlanSteam.PowerPlantId = ProductionPlanElectric.PowerPlantId = ProductionPlanDaysEffective.PowerPlantId = PowerPlantId;
			ProductionPlanSteam.Year = ProductionPlanElectric.Year = ProductionPlanDaysEffective.Year = Year;

			if (!productionPlans.Any(x => x.UtilType == ProductionUtilization.STEAM.ToString()))
			{
				productionPlans.Add(new ProductionPlan()
				{
					UtilType = ProductionUtilization.STEAM.ToString()
				});
			}
			if (!productionPlans.Any(x => x.UtilType == ProductionUtilization.ELECTRIC.ToString()))
			{
				productionPlans.Add(new ProductionPlan()
				{
					UtilType = ProductionUtilization.ELECTRIC.ToString()
				});
			}
			if (!productionPlans.Any(x => x.UtilType == ProductionUtilization.DAYSEFFECTIVE.ToString()))
			{
				productionPlans.Add(new ProductionPlan()
				{
					UtilType = ProductionUtilization.DAYSEFFECTIVE.ToString()
				});
			}

			foreach (ProductionPlan productionPlan in productionPlans)
			{
				if (productionPlan.UtilType == ProductionUtilization.STEAM.ToString())
				{
					ProductionPlanSteam.MapDbObject(productionPlan);
					foreach (ProductionPlanDetailFormStub productionPlanDetail in ProductionPlanSteam.ProductionPlanDetails)
					{
						ProductionPlanDetail dbObjectDetail = productionPlan.ProductionPlanDetails.First(x => x.Id == productionPlanDetail.Id);
						productionPlanDetail.MapDbObject(dbObjectDetail);

						if (dbObjectDetail.ProductionPlanDailies == null)
						{
							dbObjectDetail.ProductionPlanDailies = new List<ProductionPlanDaily>();
						}
						foreach (ProductionPlanDailyFormStub productionPlanDaily in productionPlanDetail.ProductionPlanDailies)
						{
							ProductionPlanDaily dbObjectDaily = dbObjectDetail.ProductionPlanDailies.FirstOrDefault(x => (x.Id == productionPlanDaily.Id) && (x.Day == productionPlanDaily.Day));
							if (dbObjectDaily == null)
							{
								dbObjectDaily = new ProductionPlanDaily()
								{
									ProductionPlanDetailId = dbObjectDetail.Id,
									Value = productionPlanDaily.Value,
									Day = productionPlanDaily.Day
								};
								dbObjectDetail.ProductionPlanDailies.Add(dbObjectDaily);
							}
							productionPlanDaily.MapDbObject(dbObjectDaily);
						}
					}
				}
				else  if (productionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString())
				{
					ProductionPlanElectric.MapDbObject(productionPlan);
					foreach (ProductionPlanDetailFormStub productionPlanDetail in ProductionPlanElectric.ProductionPlanDetails)
					{
						ProductionPlanDetail dbObjectDetail = productionPlan.ProductionPlanDetails.First(x => x.Id == productionPlanDetail.Id);
						productionPlanDetail.MapDbObject(dbObjectDetail);

						if (dbObjectDetail.ProductionPlanDailies == null)
						{
							dbObjectDetail.ProductionPlanDailies = new List<ProductionPlanDaily>();
						}
						foreach (ProductionPlanDailyFormStub productionPlanDaily in productionPlanDetail.ProductionPlanDailies)
						{
							ProductionPlanDaily dbObjectDaily = dbObjectDetail.ProductionPlanDailies.FirstOrDefault(x => (x.Id == productionPlanDaily.Id) && (x.Day == productionPlanDaily.Day));
							if (dbObjectDaily == null)
							{
								dbObjectDaily = new ProductionPlanDaily()
								{
									ProductionPlanDetailId = dbObjectDetail.Id,
									Value = productionPlanDaily.Value,
									Day = productionPlanDaily.Day
								};
								dbObjectDetail.ProductionPlanDailies.Add(dbObjectDaily);
							}
							productionPlanDaily.MapDbObject(dbObjectDaily);
						}
					}
                }
                else
                {
                    ProductionPlanDaysEffective.MapDbObject(productionPlan);
                    foreach (ProductionPlanDetailFormStub productionPlanDetail in ProductionPlanDaysEffective.ProductionPlanDetails)
                    {
                        ProductionPlanDetail dbObjectDetail = productionPlan.ProductionPlanDetails.FirstOrDefault(x => x.Id == productionPlanDetail.Id);
						if (dbObjectDetail == null)
						{
							dbObjectDetail = new ProductionPlanDetail();
							productionPlanDetail.ProductionPlanId = productionPlan.Id;
						}
                        productionPlanDetail.MapDbObject(dbObjectDetail);
                    }
                }
			}
		}
	}
}