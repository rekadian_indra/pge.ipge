﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.OperationData.Models
{
    public class InputPlanPresentationStub
	{
		public int PowerPlantId { get; set; }

		public string Area { get; set; }
		public string PowerPlant { get; set; }
		public int Year { get; set; }
		public string Electric { get; set; }
		public string Steam { get; set; }
		public double ElectricValue { get; set; }
		public double SteamValue { get; set; }
		public int DaysEffective { get; set; }

		public InputPlanPresentationStub()
		{
			PowerPlantId = 0;
			Year = 1;
		}

		public InputPlanPresentationStub(Business.Entities.PowerPlant powerPlantDb, int year)
		{
			Area = powerPlantDb.Area?.Name ?? "";
			PowerPlantId = powerPlantDb.Id;
			PowerPlant = powerPlantDb.Name;
			Year = year;

			List<ProductionPlan> productionPlans = powerPlantDb.ProductionPlans?.Where(x => !x.IsDeleted && x.Year == year)?.ToList();
			if (productionPlans != null)
			{
				foreach (ProductionPlan productionPlan in productionPlans)
				{
					if (productionPlan.UtilType == ProductionUtilization.ELECTRIC.ToString())
					{
						ElectricValue = productionPlan.ProductionPlanDetails?.Sum(x => x.Value) ?? 0;
					}
					else if (productionPlan.UtilType == ProductionUtilization.STEAM.ToString())
					{
						SteamValue = productionPlan.ProductionPlanDetails?.Sum(x => x.Value) ?? 0;
					}
					else if (productionPlan.UtilType == ProductionUtilization.DAYSEFFECTIVE.ToString())
					{
						DaysEffective = (int?)productionPlan.ProductionPlanDetails?.Sum(x => x.Value) ?? 0;
					}
				}
				Electric = DisplayFormat.NumberFormatTwoDecimal(ElectricValue);
				Steam = DisplayFormat.NumberFormatTwoDecimal(SteamValue);
				//Electric = ElectricValue.ToString("#.###,00");
				//Steam = SteamValue.ToString("#.###,00");
			}
		}
	}
}