﻿using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class ReportReliabilityStub
	{
        [DisplayName("Kumulatif Tahunan")]
        public bool IsCumulative { get; set; }
		public List<SelectListItem> MonthDropdown { get; set; }
		public List<SelectListItem> YearDropdown { get; set; }

		public int Month { get; set; }
		public int Year { get; set; }

		public ReportReliabilityStub()
		{
            IsCumulative = false;
            Month = DateTime.Now.Month;
			Year = DateTime.Now.Year;
		}

		public ReportReliabilityStub(List<int> years) : this()
		{
			MonthDropdown = new List<SelectListItem>();
			for (int month = 1; month <= 12; month++)
			{
				MonthDropdown.Add(new SelectListItem()
				{
					Text = (new DateTime(DateTime.Now.Year, month, 1)).ToString("MMMM"),
					Value = month.ToString()
				});
			}

			YearDropdown = new List<SelectListItem>
			{
				new SelectListItem()
				{
					Text = DateTime.Now.Year.ToString(),
					Value = DateTime.Now.Year.ToString()
				}
			};
			if (years != null)
			{
				years = years.OrderByDescending(x => x).ToList();
				foreach (int year in years)
				{
					if (!YearDropdown.Select(dropdown => dropdown.Value).Contains(year.ToString()))
						YearDropdown.Add(new SelectListItem()
						{
							Text = year.ToString(),
							Value = year.ToString()
						});
				}
			}
		}
	}
}