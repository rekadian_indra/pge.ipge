﻿using Business.Entities.Views;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.SalesOrder.Models;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class DailyMonthlyStub
	{
		#region Fields

		public DateTime Date { get; set; }
		public int Year { get; set; }

		public List<DailyListrik> DailyListriks { get; set; }
		public List<DailyUap> DailyUaps { get; set; }
		public List<DailyDKP> DailyDKPs { get; set; }
		public List<DailySumurProduksi> DailySumurProduksis { get; set; }
		public List<DailySumurInjeksi> DailySumurInjeksis { get; set; }
		public List<DailySumurMonitoring> DailySumurMonitorings { get; set; }
		public List<DailyInterfaceScrubber> DailyInterfaceScrubbers { get; set; }
		public List<MonthlyProduction> MonthlyProductionTable { get; set; }
		public List<MonthlySummary> MonthlySummaries { get; set; }

		#endregion

		public class DailyListrik
		{
			public string NamaPltp { get; set; }
			public double Kapasitas { get; set; }
			public double EnergyGross { get; set; }
			public double EnergyGrossRata { get; set; }
			public double EnergyNett { get; set; }
			public double EnergyNettRata { get; set; }
			public double MTDGross { get; set; }
			public double MTDNetto { get; set; }
			public double YTDGross { get; set; }
			public double YTDNetto { get; set; }
			public string Keterangan { get; set; }

			public DailyListrik()
			{

			}

			public DailyListrik(Business.Entities.PowerPlant powerPlant, List<ViewPltp> pltps, DateTime date)
			{
				NamaPltp = powerPlant.Name;
				Kapasitas = (powerPlant.URC ?? 0) / 1000;
				EnergyGross = (pltps?.Where(x => (x.Periode == date)).Sum(x => x.EnergyGross) ?? 0) / 1000;
				EnergyGrossRata = EnergyGross / 24;
				EnergyNett = (pltps?.Where(x => (x.Periode == date)).Sum(x => x.EnergyNett) ?? 0) / 1000;
				EnergyNettRata = EnergyNett / 24;
				MTDGross = (pltps?.Where(x => (x.Periode <= date) && (x.Year == date.Year) && (x.Month == date.Month)).Sum(x => x.EnergyGross) ?? 0) / 1000;
				MTDNetto = (pltps?.Where(x => (x.Periode <= date) && (x.Year == date.Year) && (x.Month == date.Month)).Sum(x => x.EnergyNett) ?? 0) / 1000;
				YTDGross = (pltps?.Where(x => (x.Periode <= date) && (x.Year == date.Year)).Sum(x => x.EnergyGross) ?? 0) / 1000;
				YTDNetto = (pltps?.Where(x => (x.Periode <= date) && (x.Year == date.Year)).Sum(x => x.EnergyNett) ?? 0) / 1000;
				Keterangan = pltps.FirstOrDefault(x => x.Notes != null)?.Notes ?? "";
			}
		}

		public class DailyUap
		{
			public string NamaPltp { get; set; }
			public double ProduksiSumurRata { get; set; }
			public double ProduksiSumur24 { get; set; }
			public double ProduksiSumurMTD { get; set; }
			public double ProduksiSumurYTD { get; set; }
			public double PemanfaatanPembangkitRata { get; set; }
			public double PemanfaatanPembangkit24 { get; set; }
			public double PemanfaatanPembangkitMTD { get; set; }
			public double PemanfaatanPembangkitYTD { get; set; }
			public double RockMufflerRata { get; set; }
			public double RockMuffler24 { get; set; }
			public double RockMufflerMTD { get; set; }
			public double RockMufflerYTD { get; set; }
			public double SSC { get; set; }

			public DailyUap()
			{

			}

			public DailyUap(Business.Entities.PowerPlant powerPlant, List<ViewPltp> pltps, DateTime date)
			{
				NamaPltp					= powerPlant.Name;
				ProduksiSumurRata			= (pltps?.Where(x => x.Periode == date).Sum(x => x.Ssc)) ?? 0; // ga yakin bener Ssc atau bukan
				ProduksiSumur24				= ProduksiSumurRata * 24;
				ProduksiSumurMTD			= (pltps?.Where(x => (x.Periode <= date) && (x.Year == date.Year) && (x.Month == date.Month)).Sum(x => x.Ssc) ?? 0) / 1000;
				ProduksiSumurYTD			= (pltps?.Where(x => (x.Periode <= date) && (x.Year == date.Year))?.Sum(x => x.Ssc) ?? 0) / 1000;
				PemanfaatanPembangkit24		= (pltps?.Where(x => x.Periode == date).Sum(x => x.SteamUtilization)) ?? 0;
				PemanfaatanPembangkitRata	= PemanfaatanPembangkit24 / 24;
				PemanfaatanPembangkitMTD 	= (pltps?.Where(x => (x.Periode <= date) && (x.Year == date.Year) && (x.Month == date.Month)).Sum(x => x.SteamUtilization) ?? 0) / 1000;
				PemanfaatanPembangkitYTD 	= (pltps?.Where(x => (x.Periode <= date) && (x.Year == date.Year))?.Sum(x => x.SteamUtilization) ?? 0) / 1000;
				RockMuffler24				= (pltps?.Where(x => x.Periode == date).Sum(x => x.WasteRockMuffler)) ?? 0;
				RockMufflerRata				= RockMuffler24 / 24;
				RockMufflerMTD				= (pltps?.Where(x => (x.Periode <= date) && (x.Year == date.Year) && (x.Month == date.Month)).Sum(x => x.WasteRockMuffler) ?? 0) / 1000;
				RockMufflerYTD				= (pltps?.Where(x => (x.Periode <= date) && (x.Year == date.Year))?.Sum(x => x.WasteRockMuffler) ?? 0) / 1000;
				SSC							= (pltps?.Where(x => x.Periode == date).Sum(x => x.Ssc)) ?? 0;
			}
		}

		public class DailyDKP
		{
			public string NamaPltp { get; set; }
			public double DMN { get; set; }
			public double NCF { get; set; }
			public double GCF { get; set; }
			public string StatusGangguan { get; set; }
			public TimeSpan JamAwalGangguan { get; set; }
			public TimeSpan JamAkhirGangguan { get; set; }
			public TimeSpan DurasiGangguan { get; set; }
			public double Derating { get; set; }
			public double EnergiHilang { get; set; }
			public string LokasiGangguan { get; set; }
			public string PenyebabGangguan { get; set; }
			public string Keterangan { get; set; }

			public DailyDKP()
			{

			}

			public DailyDKP(Business.Entities.Dkp dkp)
			{
				NamaPltp		 = dkp.PowerPlant.Name;
				DMN 			 = (dkp.PowerPlant.URC ?? 0) / 1000;
				NCF 			 = 0; // dafuq dis
				GCF  			 = 0; // dafuq dis
				StatusGangguan	 = dkp.Status;
				JamAwalGangguan	 = dkp.StartHour ?? new TimeSpan();
				JamAkhirGangguan = dkp.EndHour ?? new TimeSpan();
				DurasiGangguan	 = JamAkhirGangguan - JamAwalGangguan;
				Derating		 = dkp.Derating ?? 0;
				EnergiHilang	 = 0; // apa bedanya sama derating
				LokasiGangguan	 = dkp.Location;
				PenyebabGangguan = dkp.Cause;
				Keterangan		 = dkp.Notes;
			}
		}

		public class DailySumurProduksi
		{
			public string NamaSumur { get; set; }
			public double TKS { get; set; }
			public double TekananJalur { get; set; }
			public double SuhuJalur { get; set; }
			public double ThrottleValve { get; set; }
			public double FlowRata { get; set; }
			public double FlowTotal { get; set; }
			public double YtdKumulatif { get; set; }
			public string Status { get; set; }
			public string Keterangan { get; set; }

			public DailySumurProduksi()
			{

			}

			public DailySumurProduksi(Business.Entities.Production production, DateTime date)
			{
				NamaSumur = production.Well.Name;
				TKS = production.Tks ?? 0;
				TekananJalur = production.PressureLine ?? 0;
				SuhuJalur = production.Temperature ?? 0;
				ThrottleValve = production.ThrottleValve ?? 0;
				FlowRata = production.FlowRate ?? 0;
				FlowTotal = production.FlowTotal ?? 0;
				YtdKumulatif = (production.Well.Productions.Where(x => (x.TaskReport != null) && (x.TaskReport.Periode <= date) && (x.TaskReport.Periode?.Year == date.Year) && !x.IsDeleted)?.Sum(x => x.FlowTotal) ?? 0) / 1000;
				Status = production.Status;
				Keterangan = production.Notes;
			}
		}

		public class DailySumurInjeksi
		{
			public string NamaSumur { get; set; }
			public double TKS { get; set; }
			public double TekananJalur { get; set; }
			public double SuhuJalur { get; set; }
			public double PH { get; set; }
			public double LajuFluidaRata { get; set; }
			public double LajuFluida24 { get; set; }
			public double KumulatifSejakOperasi { get; set; }
			public string FluidaInjeksi { get; set; }
			public string Status { get; set; }
			public string Keterangan { get; set; }

			public DailySumurInjeksi()
			{

			}

			public DailySumurInjeksi(Business.Entities.ReInjection reinjection, DateTime date)
			{
				NamaSumur = reinjection.Well.Name;
				TKS = reinjection.Tks ?? 0;
				TekananJalur = reinjection.PressureLine ?? 0;
				SuhuJalur = reinjection.Temperature ?? 0;
				PH = reinjection.Ph ?? 0;
				LajuFluidaRata = reinjection.FlowRate ?? 0;
				LajuFluida24 = reinjection.FlowTotal ?? 0;
				KumulatifSejakOperasi = (reinjection.Well.ReInjections.Where(x => (x.TaskReport != null) && (x.TaskReport.Periode <= date) && !x.IsDeleted)?.Sum(x => x.FlowTotal) ?? 0) / 1000;
				FluidaInjeksi = reinjection.Fluida;
				Status = reinjection.Status;
				Keterangan = reinjection.Notes;
			}
		}

		public class DailySumurMonitoring
		{
			public string NamaSumur { get; set; }
			public double TKS { get; set; }
			public string Status { get; set; }
			public string Keterangan { get; set; }

			public DailySumurMonitoring()
			{

			}

			public DailySumurMonitoring(Business.Entities.Monitoring monitoring)
			{
				NamaSumur = monitoring.Well.Name;
				TKS = monitoring.Tks ?? 0;
				Status = monitoring.Status;
				Keterangan = monitoring.Notes;
			}
		}

		public class DailyInterfaceScrubber
		{
			public string NamaPltp { get; set; }
			public double InterfaceTekanan { get; set; }
			public double InterfaceSuhu { get; set; }
			public double ScrubberTekanan { get; set; }
			public double ScrubberSuhu { get; set; }

			public DailyInterfaceScrubber()
			{

			}

			public DailyInterfaceScrubber(Business.Entities.InterfaceScrubber interfaceScrubber)
			{
				NamaPltp			= interfaceScrubber.InterfacePoint.Area.Name;
				InterfaceTekanan	= interfaceScrubber.InterfacePressure ?? 0;
				InterfaceSuhu		= interfaceScrubber.InterfaceTemperature ?? 0;
				ScrubberTekanan		= interfaceScrubber.ScrubberPressure ?? 0;
				ScrubberSuhu		= interfaceScrubber.ScrubberTemperature ?? 0;
			}
		}
		
		public class MonthlyProduction
		{
			public List<MonthlyProductionRow> MonthlyProductionRows { get; set; }

			public string PowerPlantName { get; set; }
			public double UapActualKumulatif { get; set; }
			public double ListrikPlanPembangkitanKumulatif { get; set; }
			public double ListrikPlanKumulatifIDR { get; set; }
			public double ListrikActualPembangkitanKumulatif { get; set; }
			public double ListrikActualKumulatifIDR { get; set; }

			public class MonthlyProductionRow
			{
				public int Month { get; set; }

				public string PowerPlantName { get; set; }
				public string Bulan { get; set; }
				public string Hari { get; set; }
				public double UapPlan { get; set; }
				public double UapActual { get; set; }
				public double UapActualKumulatif { get; set; }
				public double ListrikPlanPembangkitan { get; set; }
				public double ListrikPlanPembangkitanKumulatif { get; set; }
				public double ListrikPlanHargaJual { get; set; }
				public double ListrikPlanDapatUSD { get; set; }
				public double ListrikPlanDapatIDR { get; set; }
				public double ListrikPlanKumulatifIDR { get; set; }
				public double ListrikActualPembangkitan { get; set; }
				public double ListrikActualPembangkitanKumulatif { get; set; }
				public double ListrikActualHargaJual { get; set; }
				public double ListrikActualDapatUSD { get; set; }
				public double ListrikActualDapatIDR { get; set; }
				public double ListrikActualKumulatifIDR { get; set; }

				public MonthlyProductionRow()
				{

				}

				public MonthlyProductionRow(Business.Entities.PowerPlant powerPlant, List<ViewPltp> yearlyPltp, List<ViewPpi> yearlyPpi, List<ViewProductionPlanDetail> yearlyPlanDetailSteam, List<ViewProductionPlanDetail> yearlyPlanDetailElectric, int year, int month)
				{
					// kyny lebih bener pake powerplant deh, jadi ga usah ada class row lg
					Month = month;
					double kursUSDIDR = double.Parse(System.Configuration.ConfigurationManager.AppSettings["KursUSDIDR"]);

					List<ViewPltp> monthlyPltp = yearlyPltp.Where(x => x.Month == month).ToList();

					PowerPlantName = powerPlant.Name;
					Bulan = (new DateTime(year, month, 1)).ToString("MMMM");
					Hari = DateTime.DaysInMonth(year, month).ToString();

					UapPlan = yearlyPlanDetailSteam?.Where(x => x.Month == month)?.Sum(x => x.Value) ?? 0;
					UapActual = monthlyPltp?.Sum(x => x.SteamUtilization) ?? 0;
					ListrikPlanPembangkitan = (yearlyPlanDetailElectric?.Where(x => x.Month == month)?.Sum(x => x.Value) ?? 0) * 1000;
					ListrikPlanHargaJual = yearlyPpi.Where(x => x.Month == month)?.Sum(x => x.Price) ?? 0;
					ListrikPlanDapatUSD = ListrikPlanPembangkitan / 1000 * ListrikPlanHargaJual / 1000; // dibagi 1000 soalnya ListrikPlanPembangkitan udah dalam MWh 
					ListrikPlanDapatIDR = ListrikPlanDapatUSD / 1000 * kursUSDIDR; // wkwkwkwkwkwkwkwk kurs terkini
					ListrikActualPembangkitan = (monthlyPltp?.Sum(x => (powerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString()) ? x.EnergyNett : x.EnergyGross) ?? 0);
					ListrikActualHargaJual = ListrikPlanHargaJual;
					ListrikActualDapatUSD = ListrikActualPembangkitan / 1000 * ListrikActualHargaJual / 1000;
					ListrikActualDapatIDR = ListrikActualDapatUSD / 1000 * kursUSDIDR; // wkwkwkwkwkwkwkwk kurs terkini
				}

				public MonthlyProductionRow(Business.Entities.PowerPlant powerPlant, List<Business.Entities.Pltp> yearlyPltp, List<Business.Entities.PPI> ppis, List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailSteam, List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailElectric, int year, int month)
				{
					// kyny lebih bener pake powerplant deh, jadi ga usah ada class row lg
					Month = month;
					double kursUSDIDR = double.Parse(System.Configuration.ConfigurationManager.AppSettings["KursUSDIDR"]);

					//List<Business.Entities.Pltp> yearlyPltp = powerPlant.Pltps.Where(x => (!x.IsDeleted) && (x.TaskReport != null) && (!x.TaskReport.IsDeleted) && (x.TaskReport.Periode?.Year == year)).ToList();
					//List<Business.Entities.Pltp> monthlyPltp = yearlyPltp?.Where(x => x.TaskReport.Periode?.Month == month)?.ToList();
					List<Business.Entities.Pltp> monthlyPltp = yearlyPltp.Where(x=> x.TaskReport.Periode?.Month == month).ToList();
					
					PowerPlantName = powerPlant.Name;
					Bulan = (new DateTime(year, month, 1)).ToString("MMMM");
					Hari = DateTime.DaysInMonth(year, month).ToString();

					UapPlan = yearlyPlanDetailSteam?.Where(x => x.Month == month)?.Sum(x => x.Value) ?? 0;
					UapActual = monthlyPltp?.Sum(x => x.SteamUtilization) ?? 0;
					ListrikPlanPembangkitan = (yearlyPlanDetailElectric?.Where(x => x.Month == month)?.Sum(x => x.Value) ?? 0) * 1000;
					ListrikPlanHargaJual = ppis.FirstOrDefault(x => (x.QuarterNum == DateHelper.MonthToPrevQuarter(month)) && (x.Year == DateHelper.MonthYearToPrevQuarterYear(month, year)))?.NextPrice ?? 0;
					ListrikPlanDapatUSD = ListrikPlanPembangkitan / 1000 * ListrikPlanHargaJual / 1000; // dibagi 1000 soalnya ListrikPlanPembangkitan udah dalam MWh 
					ListrikPlanDapatIDR = ListrikPlanDapatUSD / 1000 * kursUSDIDR; // wkwkwkwkwkwkwkwk kurs terkini
					ListrikActualPembangkitan = (monthlyPltp?.Sum(x => (powerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString()) ? x.EnergyNett : x.EnergyGross) ?? 0);
					ListrikActualHargaJual = ListrikPlanHargaJual;
					ListrikActualDapatUSD = ListrikActualPembangkitan / 1000 * ListrikActualHargaJual / 1000 ;
					ListrikActualDapatIDR = ListrikActualDapatUSD / 1000 * kursUSDIDR; // wkwkwkwkwkwkwkwk kurs terkini
				}
			}

			public MonthlyProduction()
			{
				UapActualKumulatif = 0;
				ListrikPlanPembangkitanKumulatif = 0;
				ListrikActualPembangkitanKumulatif = 0;

				MonthlyProductionRows = new List<MonthlyProductionRow>();
			}

			public MonthlyProduction(Business.Entities.PowerPlant powerPlant, List<ViewPltp> yearlyPltp, List<ViewPpi> yearlyPpi, List<ViewProductionPlanDetail> yearlyPlanDetailSteam, List<ViewProductionPlanDetail> yearlyPlanDetailElectric, int year)
			{
				PowerPlantName = powerPlant.Name;

				UapActualKumulatif = 0;
				ListrikPlanPembangkitanKumulatif = 0;
				ListrikPlanKumulatifIDR = 0;
				ListrikActualPembangkitanKumulatif = 0;
				ListrikActualKumulatifIDR = 0;

				MonthlyProductionRows = new List<MonthlyProductionRow>();

				for (int month = 1; month <= 12; month++)
				{
					MonthlyProductionRow monthlyProductionRow = new MonthlyProductionRow(powerPlant, yearlyPltp, yearlyPpi, yearlyPlanDetailSteam, yearlyPlanDetailElectric, year, month);

					UapActualKumulatif += monthlyProductionRow.UapActual;
					ListrikPlanPembangkitanKumulatif += monthlyProductionRow.ListrikPlanPembangkitan;
					ListrikPlanKumulatifIDR += monthlyProductionRow.ListrikPlanKumulatifIDR;
					ListrikActualPembangkitanKumulatif += monthlyProductionRow.ListrikActualPembangkitan;
					ListrikActualKumulatifIDR += monthlyProductionRow.ListrikActualKumulatifIDR;

					monthlyProductionRow.UapActualKumulatif = UapActualKumulatif;
					monthlyProductionRow.ListrikPlanPembangkitanKumulatif = ListrikPlanPembangkitanKumulatif;
					monthlyProductionRow.ListrikPlanKumulatifIDR = ListrikPlanKumulatifIDR;
					monthlyProductionRow.ListrikActualPembangkitanKumulatif = ListrikActualPembangkitanKumulatif;
					monthlyProductionRow.ListrikActualKumulatifIDR = ListrikActualKumulatifIDR;

					MonthlyProductionRows.Add(monthlyProductionRow);
				}
			}

			public MonthlyProduction(Business.Entities.PowerPlant powerPlant, List<Business.Entities.Pltp> yearlyPltp, List<Business.Entities.PPI> ppis, List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailSteam, List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailElectric, int year)
			{
				PowerPlantName = powerPlant.Name;

				UapActualKumulatif = 0;
				ListrikPlanPembangkitanKumulatif = 0;
				ListrikPlanKumulatifIDR = 0;
				ListrikActualPembangkitanKumulatif = 0;
				ListrikActualKumulatifIDR = 0;

				MonthlyProductionRows = new List<MonthlyProductionRow>();

				for (int month = 1; month <= 12; month++)
				{
					MonthlyProductionRow monthlyProductionRow = new MonthlyProductionRow(powerPlant, yearlyPltp, ppis, yearlyPlanDetailSteam, yearlyPlanDetailElectric, year, month);

					UapActualKumulatif += monthlyProductionRow.UapActual;
					ListrikPlanPembangkitanKumulatif += monthlyProductionRow.ListrikPlanPembangkitan;
					ListrikPlanKumulatifIDR += monthlyProductionRow.ListrikPlanKumulatifIDR;
					ListrikActualPembangkitanKumulatif += monthlyProductionRow.ListrikActualPembangkitan;
					ListrikActualKumulatifIDR  += monthlyProductionRow.ListrikActualKumulatifIDR;

					monthlyProductionRow.UapActualKumulatif = UapActualKumulatif;
					monthlyProductionRow.ListrikPlanPembangkitanKumulatif = ListrikPlanPembangkitanKumulatif;
					monthlyProductionRow.ListrikPlanKumulatifIDR = ListrikPlanKumulatifIDR;
					monthlyProductionRow.ListrikActualPembangkitanKumulatif = ListrikActualPembangkitanKumulatif;
					monthlyProductionRow.ListrikActualKumulatifIDR = ListrikActualKumulatifIDR;

					MonthlyProductionRows.Add(monthlyProductionRow);
				}
			}
		}

		public class MonthlySummary
		{
			public string PowerPlantName { get; set; }
			public string Bulan { get; set; }
			public double UapPlan { get; set; }
			public double UapActual { get; set; }
			public double UapActualKumulatif { get; set; }
			public double ListrikPlanPembangkitan { get; set; }
			public double ListrikPlanPembangkitanKumulatif { get; set; }
			public double ListrikPlanDapatUSD { get; set; }
			public double ListrikPlanDapatIDR { get; set; }
			public double ListrikPlanKumulatifIDR { get; set; }
			public double ListrikActualPembangkitan { get; set; }
			public double ListrikActualPembangkitanKumulatif { get; set; }
			public double ListrikActualDapatUSD { get; set; }
			public double ListrikActualDapatIDR { get; set; }
			public double ListrikActualKumulatifIDR { get; set; }

			public MonthlySummary()
			{

			}

			public MonthlySummary(List<Business.Entities.Pltp> monthlyPltp)
			{

			}
		}

		public DailyMonthlyStub()
		{
			DailyListriks = new List<DailyListrik>();
			DailyUaps = new List<DailyUap>();
			DailyDKPs = new List<DailyDKP>();
			DailySumurProduksis = new List<DailySumurProduksi>();
			DailySumurInjeksis = new List<DailySumurInjeksi>();
			DailySumurMonitorings = new List<DailySumurMonitoring>();
			DailyInterfaceScrubbers = new List<DailyInterfaceScrubber>();
			MonthlyProductionTable = new List<MonthlyProduction>();
			MonthlySummaries = new List<MonthlySummary>();
		}

		public DailyMonthlyStub(DateTime date)
		{
			Date = date;

			DailyListriks = new List<DailyListrik>();
			DailyUaps = new List<DailyUap>();
			DailyDKPs = new List<DailyDKP>();
			DailySumurProduksis = new List<DailySumurProduksi>();
			DailySumurInjeksis = new List<DailySumurInjeksi>();
			DailySumurMonitorings = new List<DailySumurMonitoring>();
			DailyInterfaceScrubbers = new List<DailyInterfaceScrubber>();
		}

		public DailyMonthlyStub(int year)
		{
			Year = year;

			MonthlyProductionTable = new List<MonthlyProduction>();
			MonthlySummaries = new List<MonthlySummary>();
		}

		public List<DailyListrik> GenerateDailyListriks(List<Business.Entities.PowerPlant> powerPlants, List<ViewPltp> pltps)
		{
			foreach (Business.Entities.PowerPlant powerPlant in powerPlants)
			{
				List<ViewPltp> curPltps = pltps.Where(x => x.PowerPlantId == powerPlant.Id).ToList();
				DailyListriks.Add(new DailyListrik(powerPlant, curPltps, Date));
			}
			return DailyListriks;
		}

		public List<DailyUap> GenerateDailyUaps(List<Business.Entities.PowerPlant> powerPlants, List<ViewPltp> pltps)
		{
			foreach (Business.Entities.PowerPlant powerPlant in powerPlants)
			{
				List<ViewPltp> curPltps = pltps.Where(x => x.PowerPlantId == powerPlant.Id).ToList();
				DailyUaps.Add(new DailyUap(powerPlant, curPltps, Date));
			}
			return DailyUaps;
		}

		public List<DailyDKP> GenerateDailyDKPs(List<Business.Entities.Dkp> dkps)
		{
			foreach (Business.Entities.Dkp dkp in dkps)
			{
				DailyDKPs.Add(new DailyDKP(dkp));
			}
			return DailyDKPs;
		}

		public List<DailySumurProduksi> GenerateDailySumurProduksis(List<Business.Entities.Production> productions)
		{
			foreach (Business.Entities.Production production in productions)
			{
				DailySumurProduksis.Add(new DailySumurProduksi(production, Date));
			}
			return DailySumurProduksis;
		}

		public List<DailySumurInjeksi> GenerateDailySumurInjeksis(List<Business.Entities.ReInjection> reinjections)
		{
			foreach (Business.Entities.ReInjection reinjection in reinjections)
			{
				DailySumurInjeksis.Add(new DailySumurInjeksi(reinjection, Date));
			}
			return DailySumurInjeksis;
		}

		public List<DailySumurMonitoring> GenerateDailySumurMonitorings(List<Business.Entities.Monitoring> monitorings)
		{
			foreach (Business.Entities.Monitoring monitoring in monitorings)
			{
				DailySumurMonitorings.Add(new DailySumurMonitoring(monitoring));
			}
			return DailySumurMonitorings;
		}

		public List<DailyInterfaceScrubber> GenerateDailyInterfaceScrubbers(List<Business.Entities.InterfaceScrubber> interfaceScrubbers)
		{
			foreach (Business.Entities.InterfaceScrubber interfaceScrubber in interfaceScrubbers)
			{
				DailyInterfaceScrubbers.Add(new DailyInterfaceScrubber(interfaceScrubber));
			}
			return DailyInterfaceScrubbers;
		}

		public List<MonthlySummary> GenerateMonthlySummaries(string areaName)
		{
			string powerPlantName = $"POWER PLANT {areaName} AREA";
			//foreach (MonthlyProduction monthlyProduction in MonthlyProductionTable)
			//{
			//	powerPlantName += ((powerPlantName == "") ? "" : ", ") + monthlyProduction.PowerPlantName;
			//}

			for (int month = 1; month <= 12; month++)
			{
				List<MonthlyProduction.MonthlyProductionRow> monthlyProductionRows = MonthlyProductionTable.SelectMany(x => x.MonthlyProductionRows)?.Where(x => x.Month == month)?.ToList();
				MonthlySummaries.Add(new MonthlySummary()
				{
					PowerPlantName						= powerPlantName,
					Bulan								= (new DateTime(DateTime.Now.Year, month, 1)).ToString("MMMM"),
					UapPlan								= monthlyProductionRows?.Sum(x => x.UapPlan) ?? 0,
					UapActual							= monthlyProductionRows?.Sum(x => x.UapActual) ?? 0,
					UapActualKumulatif					= monthlyProductionRows?.Sum(x => x.UapActualKumulatif) ?? 0,
					ListrikPlanPembangkitan				= monthlyProductionRows?.Sum(x => x.ListrikPlanPembangkitan) ?? 0,
					ListrikPlanPembangkitanKumulatif	= monthlyProductionRows?.Sum(x => x.ListrikPlanPembangkitanKumulatif) ?? 0,
					ListrikPlanDapatUSD					= monthlyProductionRows?.Sum(x => x.ListrikPlanDapatUSD) ?? 0,
					ListrikPlanDapatIDR					= monthlyProductionRows?.Sum(x => x.ListrikPlanDapatIDR) ?? 0,
					ListrikPlanKumulatifIDR				= monthlyProductionRows?.Sum(x => x.ListrikPlanKumulatifIDR) ?? 0,
					ListrikActualPembangkitan			= monthlyProductionRows?.Sum(x => x.ListrikActualPembangkitan) ?? 0,
					ListrikActualPembangkitanKumulatif	= monthlyProductionRows?.Sum(x => x.ListrikActualPembangkitanKumulatif) ?? 0,
					ListrikActualDapatUSD				= monthlyProductionRows?.Sum(x => x.ListrikActualDapatUSD) ?? 0,
					ListrikActualDapatIDR				= monthlyProductionRows?.Sum(x => x.ListrikActualDapatIDR) ?? 0,
					ListrikActualKumulatifIDR			= monthlyProductionRows?.Sum(x => x.ListrikActualKumulatifIDR) ?? 0
				});
			}

			return MonthlySummaries;
		}

		public List<MonthlySummary> GenerateMonthlySummaries(List<Business.Entities.Pltp> yearlyPltp, int year)
		{
			//for (int month = 1; month <= 12; month++)
			//{
			//	List<Business.Entities.Pltp> monthlyPltp = yearlyPltp.Where()
			//	MonthlySummaries.Add(new MonthlySummary(yearlyPltp, year, month));
			//}
			return MonthlySummaries;
		}
	}
}