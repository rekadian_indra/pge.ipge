﻿using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.SalesOrder.Models;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class ManagementRowStub
	{
		public string Pltp { get; set; }
		public int Kapasitas { get; set; }
		public double UapBulanan { get; set; }
		public double UapHarian { get; set; }
		public double UapRealisasi { get; set; }
		public double ListrikBulanan { get; set; }
		public double ListrikHarian { get; set; }
		public double ListrikRealisasi { get; set; }
		public double ListrikPencapaian { get; set; }
		public double RataPembangkitan { get; set; }
		public double CapacityFactor { get; set; }
		public string Keterangan { get; set; }

		public ManagementRowStub()
		{

		}

		public ManagementRowStub(Business.Entities.Pltp pltp)
		{
			// kamus
			DateTime? periode;
			List<Business.Entities.ProductionPlan> curProductionPlans = null;
			Business.Entities.ProductionPlanDetail curProductionPlanDetail = null;
			Business.Entities.ProductionPlanDaily curProductionPlanDaily = null;

			// algo
			UapBulanan = 0;
			UapHarian = 0;
			ListrikBulanan = 0;
			ListrikHarian = 0;

			periode = pltp.TaskReport?.Periode;
			if (periode != null)
			{
				curProductionPlans = pltp.PowerPlant.ProductionPlans.Where(plan => plan.Year == periode.Value.Year && !plan.IsDeleted).ToList();
				if (curProductionPlans.Any())
				{
					// ambil plan uap
					Business.Entities.ProductionPlan curProductionPlanSteam = curProductionPlans.FirstOrDefault(plan => plan.UtilType == Common.Enums.ProductionUtilization.STEAM.ToString());
					if (curProductionPlanSteam != null)
					{
						curProductionPlanDetail = curProductionPlanSteam.ProductionPlanDetails.FirstOrDefault(detail => detail.Month == periode.Value.Month);
						if (curProductionPlanDetail != null)
						{
							UapBulanan = curProductionPlanDetail.Value;

							curProductionPlanDaily = curProductionPlanDetail.ProductionPlanDailies.FirstOrDefault(x => x.Day == periode.Value.Day);
							UapHarian = curProductionPlanDaily?.Value ?? UapBulanan / DateTime.DaysInMonth(periode.Value.Year, periode.Value.Month);
						}
					}
					// ambil plan listrik
					Business.Entities.ProductionPlan curProductionPlanElectric = curProductionPlans.FirstOrDefault(plan => plan.UtilType == Common.Enums.ProductionUtilization.ELECTRIC.ToString());
					if (curProductionPlanElectric != null)
					{
						curProductionPlanDetail = curProductionPlanElectric.ProductionPlanDetails.FirstOrDefault(detail => detail.Month == periode.Value.Month);
						if (curProductionPlanDetail != null)
						{
							ListrikBulanan = curProductionPlanDetail.Value;

							curProductionPlanDaily = curProductionPlanDetail.ProductionPlanDailies.FirstOrDefault(x => x.Day == periode.Value.Day);
							ListrikHarian = curProductionPlanDaily?.Value ?? ListrikBulanan / DateTime.DaysInMonth(periode.Value.Year, periode.Value.Month);
						}
					}
				}
			}

			Pltp = pltp.PowerPlant.Name;
			Kapasitas = (pltp.PowerPlant.URC ?? 0) / 1000;

			UapRealisasi = pltp.SteamUtilization ?? 0;
			ListrikRealisasi = (pltp.EnergyNett ?? 0) / 1000;
			ListrikPencapaian = (ListrikHarian != 0) ? ListrikRealisasi / ListrikHarian : 1;

			RataPembangkitan = ListrikRealisasi / 24;
			CapacityFactor = ListrikRealisasi / (24 * Kapasitas);

			Keterangan = pltp.Notes;
		}
	}
}