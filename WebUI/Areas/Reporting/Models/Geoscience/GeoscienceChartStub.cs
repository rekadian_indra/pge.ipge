﻿using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.SalesOrder.Models;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class GeoscienceChartStub
	{
		static Dictionary<string, double> ShoesMD = new Dictionary<string, double>()
		{
			{"30",		33.4 },
			{"20",		500.4 },
			{"13 3/8",	1439.9 },
			{"10 3/4",	1740.4 },
			{"8 5/8",	2286 },
			{"7",		2330 },
		};
		static Dictionary<string, double> ShoesTVD = new Dictionary<string, double>()
		{
			{"30",		33.4 },
			{"20",		499 },
			{"13 3/8",	1397 },
			{"10 3/4",	1677 },
			{"8 5/8",	2188 },
			{"7",		2230 },
		};
		static Dictionary<string, double> ShoesElevation = new Dictionary<string, double>()
		{
			{"30",		874 },
			{"20",		407 },
			{"13 3/8",	-489 },
			{"10 3/4",	-769 },
			{"8 5/8",	-1281 },
			{"7",		-1323 },
		};

		public string Type { get; set; }
		public DateTime Date { get; set; }
		public double Depth { get; set; }
		public double Value { get; set; }
		public string Label { get; set; }

		public GeoscienceChartStub()
		{
			Type = "";
			Date = DateTime.Now;
			Depth = 0;
			Value = 0;
			Label = "";
		}

		public List<GeoscienceChartStub> GenerateChartMD(List<Business.Entities.Exploration> explorationDbs)
		{
			DateTime startDate = explorationDbs?.Min(x => x?.ExplorationDate) ?? DateTime.Now;

			List<GeoscienceChartStub> result = new List<GeoscienceChartStub>();
			foreach (Business.Entities.Exploration explorationDb in explorationDbs)
			{
				foreach (Business.Entities.ExplorationDetail explorationDetail in explorationDb.ExplorationDetails)
				{
					string countDayStr = ((explorationDb.ExplorationDate - startDate).TotalDays + 1).ToString("0");
					string curDateStr = explorationDb.ExplorationDate.ToString(DisplayFormat.IdDateFormat);

					// Tekanan
					result.Add(new GeoscienceChartStub()
					{
						Type = $"P {countDayStr}D ({curDateStr})",
						Date = explorationDb.ExplorationDate,
						Depth = explorationDetail.Mku,
						Value = explorationDetail.Tds
					});

					// Suhu
					result.Add(new GeoscienceChartStub()
					{
						Type = $"T {countDayStr}D ({curDateStr})",
						Date = explorationDb.ExplorationDate,
						Depth = explorationDetail.Mku,
						Value = explorationDetail.Sds
					});

					// BPD
					if (explorationDetail.Mku >= 800)
					{
						result.Add(new GeoscienceChartStub()
						{
							Type = $"BPD ({curDateStr})",
							Date = explorationDb.ExplorationDate,
							Depth = explorationDetail.Mku,
							Value = BoilingPoint(explorationDetail.Tds * 0.98) - 273.15
						});
					}
				}
			}

			// Shoes
			foreach (KeyValuePair<string, double> shoe in ShoesMD)
			{
				result.Add(new GeoscienceChartStub()
				{
					Type = "shoe",
					Depth = shoe.Value,
					Value = 392,
					Label = $"shoe {shoe.Key}\" @ {shoe.Value} m"
				});
			}
			return result;
		}

		public List<GeoscienceChartStub> GenerateChartTVD(List<Business.Entities.Exploration> explorationDbs)
		{
			DateTime startDate = explorationDbs?.Min(x => x?.ExplorationDate) ?? DateTime.Now;

			List<GeoscienceChartStub> result = new List<GeoscienceChartStub>();
			foreach (Business.Entities.Exploration explorationDb in explorationDbs)
			{
				foreach (Business.Entities.ExplorationDetail explorationDetail in explorationDb.ExplorationDetails)
				{
					string countDayStr = ((explorationDb.ExplorationDate - startDate).TotalDays + 1).ToString("0");
					string curDateStr = explorationDb.ExplorationDate.ToString(DisplayFormat.IdDateFormat);

					// Tekanan
					result.Add(new GeoscienceChartStub()
					{
						Type = $"P {countDayStr}D ({curDateStr})",
						Date = explorationDb.ExplorationDate,
						Depth = explorationDetail.Mkt,
						Value = explorationDetail.Tds
					});

					// Suhu
					result.Add(new GeoscienceChartStub()
					{
						Type = $"T {countDayStr}D ({curDateStr})",
						Date = explorationDb.ExplorationDate,
						Depth = explorationDetail.Mkt,
						Value = explorationDetail.Sds
					});

					// BPD
					if (explorationDetail.Mku >= 800)
					{
						result.Add(new GeoscienceChartStub()
						{
							Type = $"BPD ({curDateStr})",
							Date = explorationDb.ExplorationDate,
							Depth = explorationDetail.Mkt,
							Value = BoilingPoint(explorationDetail.Tds * 0.98) - 273.15
						});
					}
				}
			}

			// Shoes
			foreach (KeyValuePair<string, double> shoe in ShoesTVD)
			{
				result.Add(new GeoscienceChartStub()
				{
					Type = "shoe",
					Depth = shoe.Value,
					Value = 392,
					Label = $"shoe {shoe.Key}\" @ {shoe.Value} m"
				});
			}
			return result;
		}
		
		public List<GeoscienceChartStub> GenerateChartElevation(List<Business.Entities.Exploration> explorationDbs)
		{
			DateTime startDate = explorationDbs?.Min(x => x?.ExplorationDate) ?? DateTime.Now;

			List<GeoscienceChartStub> result = new List<GeoscienceChartStub>();
			foreach (Business.Entities.Exploration explorationDb in explorationDbs)
			{
				foreach (Business.Entities.ExplorationDetail explorationDetail in explorationDb.ExplorationDetails)
				{
					string countDayStr = ((explorationDb.ExplorationDate - startDate).TotalDays + 1).ToString("0");
					string curDateStr = explorationDb.ExplorationDate.ToString(DisplayFormat.IdDateFormat);

					// Tekanan
					result.Add(new GeoscienceChartStub()
					{
						Type = $"P {countDayStr}D ({curDateStr})",
						Date = explorationDb.ExplorationDate,
						Depth = explorationDetail.Mdpl,
						Value = explorationDetail.Tds
					});

					// Suhu
					result.Add(new GeoscienceChartStub()
					{
						Type = $"T {countDayStr}D ({curDateStr})",
						Date = explorationDb.ExplorationDate,
						Depth = explorationDetail.Mdpl,
						Value = explorationDetail.Sds
					});

					// BPD
					if (explorationDetail.Mku >= 800)
					{
						result.Add(new GeoscienceChartStub()
						{
							Type = $"BPD ({curDateStr})",
							Date = explorationDb.ExplorationDate,
							Depth = explorationDetail.Mdpl,
							Value = BoilingPoint(explorationDetail.Tds * 0.98) - 273.15
						});
					}
				}
			}

			// Shoes
			foreach (KeyValuePair<string, double> shoe in ShoesElevation)
			{
				result.Add(new GeoscienceChartStub()
				{
					Type = "shoe",
					Depth = shoe.Value,
					Value = 392,
					Label = $"shoe {shoe.Key}\" @ {shoe.Value} m"
				});
			}
			return result;
		}

		public static List<GeoscienceChartStub> GetShoesMD()
		{
			List<GeoscienceChartStub> result = new List<GeoscienceChartStub>();
			foreach(KeyValuePair<string, double> shoe in ShoesMD)
			{
				result.Add(new GeoscienceChartStub()
				{
					Type = "shoe",
					Depth = shoe.Value,
					Value = 392,
					Label = $"shoe {shoe.Key}\" @ {shoe.Value} m"
				});
			}
			return result;
		}

		public static List<GeoscienceChartStub> GetShoesTVD()
		{
			List<GeoscienceChartStub> result = new List<GeoscienceChartStub>();
			foreach (KeyValuePair<string, double> shoe in ShoesTVD)
			{
				result.Add(new GeoscienceChartStub()
				{
					Type = "shoe",
					Depth = shoe.Value,
					Value = 392,
					Label = $"shoe {shoe.Key}\" @ {shoe.Value} m"
				});
			}
			return result;
		}

		public static List<GeoscienceChartStub> GetShoesElevation()
		{
			List<GeoscienceChartStub> result = new List<GeoscienceChartStub>();
			foreach (KeyValuePair<string, double> shoe in ShoesElevation)
			{
				result.Add(new GeoscienceChartStub()
				{
					Type = "shoe",
					Depth = shoe.Value,
					Value = 392,
					Label = $"shoe {shoe.Key}\" @ {shoe.Value} m"
				});
			}
			return result;
		}

		/****************************************************************/
		/*  Siedetemp. in Abhängigkeit vom Druck NACH IFC 68            */
		/*  unter Verwendung der Function VAPP und Intervall-           */
		/*  halbierungsmethode                                          */
		/*  Bereich: 0.01 bar < P < 220 bar                             */
		/*  P = Druck in bar                                            */
		/*  BOILP = Siedetemperatur in K                                */
		/*  BOILP = -1: Druck AUSSERHALB BEREICH                        */
		/*  Translated to C# by Kenji Prahyudi                          */
		/****************************************************************/
		private double BoilingPoint(double pressure)
		{
			double Tunt = 280.15;
			double Tob = 646.85;
			double Tm = 0.5 * (Tunt + Tob);
			double Punt = VAPP(Tunt);
			double Pob = VAPP(Tob);
			double Pm = VAPP(Tm);
			double Dpunt = Punt - pressure;
			double Dpob = Pob - pressure;
			double Dpm = Pm - pressure;

			if (Dpunt * Dpob > 0)
			{
				return - 1;
			}
			else
			{
				for (int i = 1; i <= 10000; i++)
				{
					if (Dpm * Dpunt < 0)
					{
						Tob = Tm;
						Pob = Pm;
						Dpob = Dpm;
					}
					else
					{
						Tunt = Tm;
						Punt = Pm;
						Dpunt = Dpm;
					}
					Tm = 0.5 * (Tunt + Tob);
					if (Tm - Tunt < 0.0005)
					{
						return Tm;
					}
					else
					{
						Pm = VAPP(Tm);
						Dpm = Pm - pressure;
					}
				}
				return -1;
			}
		}

		/****************************************************************/
		/*  DAMPFDRUCKKURVE NACH IFC 68                                 */
		/*  T = SIEDETEMPERATUR in K                                    */
		/*  VAPP = SIEDEDRUCK in bar                                    */
		/*  VAPP = -1: TEMPERATUR AUSSERHALB BEREICH                    */
		/*  Translated to C# by Kenji Prahyudi                          */
		/****************************************************************/
		private double VAPP(double temp)
		{
			List<double> AK, BK, Tz;
			double Tc = 647.3;
			double Pc = 221.2;
			AK = new List<double>()
			{
				-4.0596821,
				5.1322555,
				-1.1842407,
				0.11779592,
				-0.005157642,
				-0.0014689537,
				0.00053622818,
				0.00012455399,
				-0.000049154288,
				0.000046302565,
				0.000015301334,
				-0.00002095453
			};

			BK = new List<double>()
			{
				2,
				0.95,
				1.45220717,
				-0.84878953
			};

			double Tr = temp / Tc;

			if ((Tr < 0.421) || (Tr > 1.658))
			{
				return -1;
			}
			else
			{
				double U = (BK[0] * Math.Pow(1 / Tr - BK[1], 0.4) - BK[2]) / BK[3];
				Tz = new List<double>()
				{
					1,
					U
				};
				for (int i = 2; i < 12; i++)
				{
					Tz.Add(2 * U * Tz[i - 1] - Tz[i - 2]);
				}
				double sum = 0;
				for (int i = 0; i < 12; i++)
				{
					sum += AK[i] * Tz[i];
				}
				double PR = Math.Exp(sum);
				return PR * Pc;
			}
		}
	}
}