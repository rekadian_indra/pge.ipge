﻿using System;
using System.Collections.Generic;

namespace WebUI.Areas.Reporting.Models.ReservoirReport
{
    public class ReservoirReportParamStub
    {
        public string ReportModule { get; set; }
        public int AreaId { get; set; }
        public List<int> RelationId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<string> Column { get; set; }
        public string ChartImage { get; set; }
    }
}