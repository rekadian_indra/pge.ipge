﻿using System;
using System.Collections.Generic;

namespace WebUI.Areas.Reporting.Models.ReservoirReport
{
    public class ReservoirReportDataPresentationStub
    {
        public string Relation { get; set; }
        public int? RelationRowSpan { get; set; }
        public string Column { get; set; }
        public List<double> ColumnValue { get; set; }

        public ReservoirReportDataPresentationStub()
        {
            ColumnValue = new List<double>();
        }
    }
}