﻿using System;
using System.Collections.Generic;

namespace WebUI.Areas.Reporting.Models.ReservoirReport
{
    public class ReservoirReportPresentationStub
    {
        public List<string> DateHeader { get; set; }
        public int DateColSpan { get; set; }
        public List<ReservoirReportDataPresentationStub> DataRow { get; set; }

        public ReservoirReportPresentationStub() {
            DateHeader = new List<string>();
            DataRow = new List<ReservoirReportDataPresentationStub>();
        }

    }
}