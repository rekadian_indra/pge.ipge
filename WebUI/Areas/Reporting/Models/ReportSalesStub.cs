﻿using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class ReportSalesStub
	{
		public int Year { get; set; }
		public List<SelectListItem> YearChoices { get; set; }

		public ReportSalesStub()
		{

		}

		public ReportSalesStub(List<Business.Entities.SalesOrder> salesOrders)
		{
			List<int> years = salesOrders?.Where(x => x.Periode.HasValue)?.Select(x => x.Periode.Value.Year).Distinct().OrderBy(x => x).ToList();
			if (years.Count <= 0) years.Add(DateTime.Now.Year);

			Year = years.Last();

			YearChoices = new List<SelectListItem>();
			foreach (int year in years)
			{
				YearChoices.Add(new SelectListItem()
				{
					Text = year.ToString(),
					Value = year.ToString()
				});
			}
		}
	}
}