﻿using Business.Entities;
using Business.Entities.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Areas.Reporting.Models.Kpi
{
    public class KpiStub
    {
        public string Period_01 { get; set; }
        public string PowerPlantName_02 { get; set; }
        public string BAAcceptDate_03 { get; set; }
        public string InvoiceDate_04 { get; set; }
        public double CountDays_05_4_3 { get; set; }
        public string AcceptedDateBy_06 { get; set; }
        public string InvoiceNo_07 { get; set; }
        public double NetValue_08 { get; set; }
        public double CountDays_09_6_4 { get; set; }
        public string TargetSentBackDate_10 { get; set; }
        public string ActualSentBackDate_11 { get; set; }
        public double CountDays_12_11_6 { get; set; }
        public string InvoiceSentDate_13 { get; set; }
        public double TransmitionValue_14 { get; set; }
        public int MonthValue_15 { get; set; }
        public string MonthText_16 { get; set; }
        public string TransmitionInvoice_17 { get; set; }

        public KpiStub()
        {

        }

        public static List<KpiStub> MapList(List<PowerPlant> plants, List<ViewInvoicingKpi> kpis, int year, int startMonth, int endMonth)
        {
            //lib
            List<KpiStub> kpiModels = new List<KpiStub>();
            KpiStub kpiModel = new KpiStub();

            //algorithm
            for (int i = startMonth; i <= endMonth; i++)
            {
                foreach (PowerPlant p in plants)
                {
                    KpiStub temp = new KpiStub()
                    {
                        Period_01 = new DateTime(year, i, 1).ToString("MMM yy"),
                        PowerPlantName_02 = p.Name,
                        BAAcceptDate_03 = "",
                        InvoiceDate_04 = "",
                        CountDays_05_4_3 = 0,
                        AcceptedDateBy_06 = "",
                        InvoiceNo_07 = "",
                        NetValue_08 = 0,
                        CountDays_09_6_4 = 0,
                        TargetSentBackDate_10 = "",
                        ActualSentBackDate_11 = "",
                        CountDays_12_11_6 = 0,
                        InvoiceSentDate_13 = "",
                        TransmitionValue_14 = 0,
                        MonthValue_15 = i,
                        MonthText_16 = new DateTime(year, i, 1).ToString("MMM"),
                        TransmitionInvoice_17 = ""
                    };

                    kpiModels.Add(temp);
                }
            }

            foreach (KpiStub n in kpiModels)
            {
                foreach (ViewInvoicingKpi m in kpis)
                {
                    if (n.PowerPlantName_02 == m.Name && n.MonthValue_15 == m.Month)
                    {
                        kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                    .FirstOrDefault().BAAcceptDate_03 = new DateTime(m.BASentDate.Year, m.BASentDate.Month, m.BASentDate.Day).ToString("dd-MMM-yy");

                        kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                            .FirstOrDefault().InvoiceNo_07 = m.InvoiceNo;

                        kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                    .FirstOrDefault().InvoiceDate_04 = new DateTime(m.InvoiceDate.Year, m.InvoiceDate.Month, m.InvoiceDate.Day).ToString("dd-MMM-yy");

                        //hitung hari (kolom 4-3)
                        TimeSpan ts = new TimeSpan();
                        DateTime invoiceDate = new DateTime(m.InvoiceDate.Year, m.InvoiceDate.Month, m.InvoiceDate.Day);

                        ts = invoiceDate.Subtract(m.BASentDate);

                        kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                .FirstOrDefault().CountDays_05_4_3 = ts.Days;

                        kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                    .FirstOrDefault().NetValue_08 = m.NetValue;

                        //hitung hari (kolom 6-4)
                        ts = new TimeSpan();
                        if (m.AcceptedDateBy.Value.Year > 1900)
                        {
                            DateTime targetSentBackDate = m.AcceptedDateBy.Value.AddDays(30);
                            ts = m.AcceptedDateBy.Value.Subtract(invoiceDate);
                            kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                        .FirstOrDefault().AcceptedDateBy_06 = new DateTime(m.AcceptedDateBy.Value.Year, m.AcceptedDateBy.Value.Month, m.AcceptedDateBy.Value.Day).ToString("dd-MMM-yy");


                            kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                    .FirstOrDefault().CountDays_09_6_4 = ts.Days;

                            kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                .FirstOrDefault().TargetSentBackDate_10 = targetSentBackDate.ToString("dd-MMM-yyyy");
                        }

                        //kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                        //    .FirstOrDefault().TargetSentBackDate_10 = new DateTime(m.TargetSentBackDate.Value.Year, m.TargetSentBackDate.Value.Month, m.TargetSentBackDate.Value.Day).ToString("dd-MMM-yyyy");

                        //hitung hari (kolom 11-6)
                        ts = new TimeSpan();
                        if (m.ActualSentBackDate.Value.Year > 1900)
                        {
                            ts = m.ActualSentBackDate.Value.Subtract(m.AcceptedDateBy.Value);

                            kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                        .FirstOrDefault().ActualSentBackDate_11 = new DateTime(m.ActualSentBackDate.Value.Year, m.ActualSentBackDate.Value.Month, m.ActualSentBackDate.Value.Day).ToString("dd-MMM-yy");

                            kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                    .FirstOrDefault().CountDays_12_11_6 = ts.Days;
                        }

                        if (m.InvoiceSentDate.Year > 1000)
                            kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                        .FirstOrDefault().InvoiceSentDate_13 = new DateTime(m.InvoiceSentDate.Year, m.InvoiceSentDate.Month, m.InvoiceSentDate.Day).ToString("dd-MMM-yy");

                        kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                    .FirstOrDefault().TransmitionValue_14 = m.TransmitionValue;

                        if (m.TransmitionValue > 0)
                            kpiModels.Where(x => x.PowerPlantName_02 == m.Name && x.MonthValue_15 == m.Month)
                                        .FirstOrDefault().TransmitionInvoice_17 = $"{m.InvoiceNo}-1";
                    }
                }
            }

            return kpiModels;
        }
    }
}