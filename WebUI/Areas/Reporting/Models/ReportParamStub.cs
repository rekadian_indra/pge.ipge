﻿using System;
using System.ComponentModel;

namespace WebUI.Areas.Reporting.Models
{
    public class ReportParamStub
    {
        [DisplayName("Area")]
        public int AreaId { get; set; }
        [DisplayName("Tahun")]
        public int Year { get; set; }
        [DisplayName("Bulan")]
        public int StartMonth { get; set; }
        public int EndMonth { get; set; }

        public string AreaName { get; set; }
        public string StartMonthName { get; set; }
        public string EndMonthName { get; set; }

        #region "contructor"
        public ReportParamStub()
        {
            AreaId = 0;
            Year = DateTime.Now.Year;
            StartMonth = 1;
            EndMonth = 1;
            AreaName = "Semua";
            StartMonthName = "Jan";
            EndMonthName = "Jan";
        }
        #endregion
    }
}