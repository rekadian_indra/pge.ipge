﻿using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class ReportManagementStub
	{
		public int AreaId { get; set; }
		public List<SelectListItem> AreaDropdown { get; set; }
		public DateTime Date { get; set; }

		public ReportManagementStub()
		{
			Date = DateTime.Now;
			AreaDropdown = new List<SelectListItem>();
		}

		public ReportManagementStub(List<Business.Entities.Area> areas)
		{
			Date = DateTime.Now;

			AreaDropdown = new List<SelectListItem>
			{
				new SelectListItem()
				{
					Text = "Semua",
					Value = "0"
				}
			};
			foreach (Business.Entities.Area area in areas)
			{
				AreaDropdown.Add(new SelectListItem()
				{
					Text = area.Name,
					Value = area.Id.ToString()
				});
			}
		}
	}
}