﻿using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class ReportGeoscienceStub
	{
		public int AreaId { get; set; }
		public List<SelectListItem> AreaDropdown { get; set; }
		public int ClusterId { get; set; }
		public List<SelectListItem> ClusterDropdown { get; set; }
		public int WellId { get; set; }
		public List<SelectListItem> WellDropdown { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }

		public ReportGeoscienceStub()
		{
			EndDate = DateTime.Now;
			StartDate = EndDate.AddMonths(-1);
		}

		public ReportGeoscienceStub(List<Business.Entities.Area> areas)
		{
			EndDate = DateTime.Now;
			StartDate = EndDate.AddMonths(-1);
			Business.Entities.Area selectedArea = null;
			Business.Entities.Cluster selectedCluster = null;
			WellId = 0;

			AreaDropdown = new List<SelectListItem>();
			foreach (Business.Entities.Area area in areas)
			{
				if (selectedArea == null)
				{
					AreaId = area.Id;
					selectedArea = area;
				}
				AreaDropdown.Add(new SelectListItem()
				{
					Text = area.Name,
					Value = area.Id.ToString()
				});
			}

			ClusterDropdown = new List<SelectListItem>();
			if (selectedArea != null)
				foreach (Business.Entities.Cluster cluster in selectedArea.Clusters)
				{
					if (selectedCluster == null)
					{
						ClusterId = cluster.Id;
						selectedCluster = cluster;
					}
					ClusterDropdown.Add(new SelectListItem()
					{
						Text = cluster.Name,
						Value = cluster.Id.ToString()
					});
				}

			WellDropdown = new List<SelectListItem>();
			if (selectedCluster != null)
				foreach (Business.Entities.Well well in selectedCluster.Wells)
				{
					if (WellId == 0)
					{
						WellId = well.Id;
					}
					WellDropdown.Add(new SelectListItem()
					{
						Text = well.Name,
						Value = well.Id.ToString()
					});
				}
		}
	}
}