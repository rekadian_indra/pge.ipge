﻿using Business.Entities.Views;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.SalesOrder.Models;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class ReliabilityChartStub
	{
		public double Realization { get; set; }
		public double NotAbsorbedInternal { get; set; }
		public double NotAbsorbedExternal { get; set; }

		public ReliabilityChartStub()
		{

		}

		public ReliabilityChartStub(List<Business.Entities.PowerPlant> powerPlants, List<ViewDkp> dkps)
		{

		}
	}
}