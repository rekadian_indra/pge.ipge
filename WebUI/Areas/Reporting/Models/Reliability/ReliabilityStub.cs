﻿using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.SalesOrder.Models;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class ReliabilityStub
	{
		
		public ReliabilityTableStub Table { get; set; }
		public ReliabilityChartStub Chart { get; set; }
		
		public class ReliabilityTableStub
		{
			public List<ReliabilityRowStub> Rows { get; set; }

			public class ReliabilityRowStub
			{
				public string Area { get; set; }
				public string Unit { get; set; }
				public double DMN { get; set; }
				public double RENEFOR { get; set; }
				public double RENEAF { get; set; }
				public double RELEFOR { get; set; }
				public double RELEAF { get; set; }

				public ReliabilityRowStub()
				{

				}

				public ReliabilityRowStub(Business.Entities.PowerPlant powerPlant, List<ViewReliabilityPlan> reliabilityPlans, List<ViewDkp> dkps, int year, int month, bool isCumulative) : this()
				{
                    if(powerPlant.Name.Contains("Unit 4"))
                    {
                        string a = "3";
                    }
                    double PH = DateTime.DaysInMonth(year, month) * 24;
                    if (isCumulative)
                    {
                        for (int i = 1; i < month; i++)
                        {
                            PH += DateTime.DaysInMonth(year, i) * 24;
                        }
                    }

					double RENFOH = reliabilityPlans.Sum(x => x.FOH * PH);
					double RENMOH = reliabilityPlans.Sum(x => x.MOH * PH);
					double RENPOH = reliabilityPlans.Sum(x => x.POH * PH);
					double RENAH = PH - RENFOH - RENMOH - RENPOH;
					double RENEFDH = reliabilityPlans.Sum(x => x.EFDH * RENAH);
					double RENEMDH = reliabilityPlans.Sum(x => x.EMDH * RENAH);
					double RENEPDH = reliabilityPlans.Sum(x => x.EPDH * RENAH);
					
                    if(powerPlant.Material == Material.GEOTHERMAL_STEAM.ToString())
                    {
                        dkps = dkps.Where(x => x.Location == InterruptionLocation.SGS.ToString()).ToList();
                    }
                    else
                    {
                        dkps = dkps.Where(x => x.Location != InterruptionLocation.EKSTERNAL.ToString()).ToList();
                    }
                    

					double RELFOH = dkps.Where(x => GetStatusCategory(x.Status) == "FOH").Sum(x => (x.EndHour.TotalHours >= x.StartHour.TotalHours ? x.EndHour.TotalHours : x.EndHour.TotalHours + 24) - x.StartHour.TotalHours);
					double RELMOH = dkps.Where(x => GetStatusCategory(x.Status) == "MOH").Sum(x => (x.EndHour.TotalHours >= x.StartHour.TotalHours ? x.EndHour.TotalHours : x.EndHour.TotalHours + 24) - x.StartHour.TotalHours);
					double RELPOH = dkps.Where(x => GetStatusCategory(x.Status) == "POH").Sum(x => (x.EndHour.TotalHours >= x.StartHour.TotalHours ? x.EndHour.TotalHours : x.EndHour.TotalHours + 24) - x.StartHour.TotalHours);
					double RELAH = PH - RELFOH - RELMOH - RELPOH;
					double RELEFDH = dkps.Where(x => GetStatusCategory(x.Status) == "EFDH").Sum(x => (x.EndHour.TotalHours >= x.StartHour.TotalHours ? x.EndHour.TotalHours : x.EndHour.TotalHours + 24) - x.StartHour.TotalHours);
					double RELEMDH = dkps.Where(x => GetStatusCategory(x.Status) == "EMDH").Sum(x => (x.EndHour.TotalHours >= x.StartHour.TotalHours ? x.EndHour.TotalHours : x.EndHour.TotalHours + 24) - x.StartHour.TotalHours);
					double RELEPDH = dkps.Where(x => GetStatusCategory(x.Status) == "EPDH").Sum(x => (x.EndHour.TotalHours >= x.StartHour.TotalHours ? x.EndHour.TotalHours : x.EndHour.TotalHours + 24) - x.StartHour.TotalHours);

					Area = powerPlant.Area.Name;
					Unit = powerPlant.Name;
					DMN = (powerPlant.URC ?? 0) / 1000;

					RENEFOR = (RENFOH + RENEFDH) / (RENFOH + PH);
					RENEAF = (RENAH - RENEFDH - RENEMDH - RENEPDH) / PH;

					RELEFOR = (RELFOH + RELEFDH) / (RELFOH + PH);
					RELEAF = (RELAH - RELEFDH - RELEMDH - RELEPDH) / PH;
				}
				
				private string GetStatusCategory(string status)
				{
					if (Enum.TryParse(status, out InterruptionStatus statusEnum))
					{
						switch (statusEnum)
						{
							case (InterruptionStatus.FO1): return "FOH";
							case (InterruptionStatus.FO2): return "FOH";
							case (InterruptionStatus.FO3): return "FOH";
							case (InterruptionStatus.MO): return "MOH";
							case (InterruptionStatus.ME): return "MOH";
							case (InterruptionStatus.PO): return "POH";
							case (InterruptionStatus.FD1): return "EFDH";
							case (InterruptionStatus.FD2): return "EFDH";
							case (InterruptionStatus.FD3): return "EFDH";
							case (InterruptionStatus.MD): return "EMDH";
							case (InterruptionStatus.MDE): return "EMDH";
							case (InterruptionStatus.PD): return "EPDH";
							case (InterruptionStatus.PDE): return "EPDH";
							default: return "";
						}
					}
					return "";
				}
			}

			public ReliabilityTableStub()
			{
				Rows = new List<ReliabilityRowStub>();
			}

			public ReliabilityTableStub(List<Business.Entities.PowerPlant> powerPlants, List<ViewReliabilityPlan> reliabilityPlans, List<ViewDkp> dkps, int year, int month, bool isCumulative) : this()
			{
				foreach (Business.Entities.PowerPlant powerPlant in powerPlants)
				{
					List<ViewDkp> curDkps = dkps.Where(x => x.PowerPlantId == powerPlant.Id).ToList();
					List<ViewReliabilityPlan> curReliabilityPlans = reliabilityPlans.Where(x => x.PowerPlantId == powerPlant.Id).ToList();
					Rows.Add(new ReliabilityRowStub(powerPlant, curReliabilityPlans, curDkps, year, month, isCumulative));
				}
			}
		}

		public class ReliabilityChartStub
		{
			public List<ReliabilityRowStub> Rows { get; set; }

			public class ReliabilityRowStub
			{
				public string Category { get; set; }
				public string SubCategory { get; set; }
				public double Value { get; set; }
                public double Nett { get; set; }

                public ReliabilityRowStub()
				{

				}

				public ReliabilityRowStub(ReliabilityCategory category, ReliabilitySubCategory subCategory, double value)
				{
					Category = Common.Enums.EnumExtension.ToDescription(category);
					SubCategory = Common.Enums.EnumExtension.ToDescription(subCategory);
					Value = value;
				}

                public ReliabilityRowStub(ReliabilityCategory category, ReliabilitySubCategory subCategory, double value, double nett)
                {
                    Category = Common.Enums.EnumExtension.ToDescription(category);
                    SubCategory = Common.Enums.EnumExtension.ToDescription(subCategory);
                    Value = value;
                    Nett = nett;
                }
            }

			public ReliabilityChartStub()
			{

			}

			public ReliabilityChartStub(double realization, double notAbsorbedInternal, double notAbsorbedExternal)
			{
				Rows = new List<ReliabilityRowStub>()
				{
					new ReliabilityRowStub(ReliabilityCategory.REALIZATION, ReliabilitySubCategory.REALIZATION, realization),
					new ReliabilityRowStub(ReliabilityCategory.NOT_ABSORBED, ReliabilitySubCategory.NOT_ABSORBED_INTERNAL, notAbsorbedInternal),
					new ReliabilityRowStub(ReliabilityCategory.NOT_ABSORBED, ReliabilitySubCategory.NOT_ABSORBED_EXTERNAL, notAbsorbedExternal)
				};
			}

            public ReliabilityChartStub(double realization, double notAbsorbedInternal, double notAbsorbedExternal, double realNett, double internalNett, double externalNett)
            {
                Rows = new List<ReliabilityRowStub>()
                {
                    new ReliabilityRowStub(ReliabilityCategory.REALIZATION, ReliabilitySubCategory.REALIZATION, realization, realNett),
                    new ReliabilityRowStub(ReliabilityCategory.NOT_ABSORBED, ReliabilitySubCategory.NOT_ABSORBED_INTERNAL, notAbsorbedInternal, internalNett),
                    new ReliabilityRowStub(ReliabilityCategory.NOT_ABSORBED, ReliabilitySubCategory.NOT_ABSORBED_EXTERNAL, notAbsorbedExternal, externalNett)
                };
            }
        }
		public ReliabilityStub()
		{
			Table = new ReliabilityTableStub();
		}

		public ReliabilityStub(List<Business.Entities.PowerPlant> powerPlants, List<ViewPltp> pltps, List<ViewReliabilityPlan> reliabilityPlans, List<ViewDkp> dkps, int year, int month, bool isCumulative)
		{
			Table = new ReliabilityTableStub(powerPlants, reliabilityPlans, dkps, year, month, isCumulative);
			
			int totalDMN = (powerPlants.Sum(x => x.URC) ?? 0) / 1000;
			double PH = DateTime.DaysInMonth(year, month) * 24;
            if (isCumulative)
            {
                for (int i = 1; i < month; i++)
                {
                    PH += DateTime.DaysInMonth(year, i) * 24;
                }
            }

            double planReadiness = totalDMN * PH;
			double actualReadiness = Table.Rows.Sum(x => x.RELEAF * x.DMN) / totalDMN * planReadiness;
            double actualProduction = 0;

            if (isCumulative)
            {
                actualProduction += (pltps.Where(x => x.Year == year && x.Month <= month)?.Where(x => x.Material == Material.ELECTRIC_POWER.ToString())?.Sum(x => x.EnergyNett) ?? 0) / 1000; // powerPlants.Sum(x => x.Pltps?.Where(y => !y.IsDeleted && y.TaskReport != null && y.TaskReport.Periode.Value.Year == year && y.TaskReport.Periode.Value.Month == month)?.Sum(y => y.EnergyGross + y.EnergyNett)) / 1000 ?? 0;
			    actualProduction += (pltps.Where(x => x.Year == year && x.Month <= month)?.Where(x => x.Material == Material.GEOTHERMAL_STEAM.ToString())?.Sum(x => x.EnergyGross) ?? 0) / 1000;
            }
            else
            {
                actualProduction += (pltps.Where(x => x.Year == year && x.Month == month)?.Where(x => x.Material == Material.ELECTRIC_POWER.ToString())?.Sum(x => x.EnergyNett) ?? 0) / 1000; // powerPlants.Sum(x => x.Pltps?.Where(y => !y.IsDeleted && y.TaskReport != null && y.TaskReport.Periode.Value.Year == year && y.TaskReport.Periode.Value.Month == month)?.Sum(y => y.EnergyGross + y.EnergyNett)) / 1000 ?? 0;
			    actualProduction += (pltps.Where(x => x.Year == year && x.Month == month)?.Where(x => x.Material == Material.GEOTHERMAL_STEAM.ToString())?.Sum(x => x.EnergyGross) ?? 0) / 1000;
            }

			double realization = actualProduction / planReadiness; // kalo utk produksi pake gross kan?
			double notAbsorbedInternal = (planReadiness - actualReadiness) / planReadiness;
			double notAbsorbedExternal = (actualReadiness - actualProduction) / planReadiness;

            double realizationNett = actualProduction; // kalo utk produksi pake gross kan?
            double notAbsorbedInternalNett = (planReadiness - actualReadiness);
            double notAbsorbedExternalNett = (actualReadiness - actualProduction);

            Chart = new ReliabilityChartStub(realization, notAbsorbedInternal, notAbsorbedExternal, realizationNett, notAbsorbedInternalNett, notAbsorbedExternalNett);
		}

        public ReliabilityStub(List<Business.Entities.PowerPlant> powerPlants, List<ViewPltp> pltps, List<ViewReliabilityPlan> reliabilityPlans, List<ViewDkp> dkps)
        {
            List<int> years = new List<int>() { DateTime.Now.Year};
            years = pltps.Any() ? pltps.Select(n => n.Year).ToList() : years;
            int month = 12;
            double realization = 0; // kalo utk produksi pake gross kan?
            double notAbsorbedInternal = 0;
            double notAbsorbedExternal = 0;

            double realizationNett = 0; // kalo utk produksi pake gross kan?
            double notAbsorbedInternalNett = 0;
            double notAbsorbedExternalNett = 0;

            foreach (int year in years)
            {
                Table = new ReliabilityTableStub(powerPlants, reliabilityPlans, dkps, year, month, isCumulative: false);

                int totalDMN = (powerPlants.Sum(x => x.URC) ?? 0) / 1000;
                double PH = DateTime.DaysInMonth(year, month) * 24;

                double planReadiness = totalDMN * PH;
                double actualReadiness = Table.Rows.Sum(x => x.RELEAF * x.DMN) / totalDMN * planReadiness;
                double actualProduction = (pltps.Where(x => x.Year == year && x.Month == month)?.Sum(x => x.EnergyGross + x.EnergyNett) ?? 0) / 1000; // powerPlants.Sum(x => x.Pltps?.Where(y => !y.IsDeleted && y.TaskReport != null && y.TaskReport.Periode.Value.Year == year && y.TaskReport.Periode.Value.Month == month)?.Sum(y => y.EnergyGross + y.EnergyNett)) / 1000 ?? 0;

                realization = actualProduction / planReadiness; // kalo utk produksi pake gross kan?
                notAbsorbedInternal = (planReadiness - actualReadiness) / planReadiness;
                notAbsorbedExternal = (actualReadiness - actualProduction) / planReadiness;

                realization = double.IsNaN(realization) || double.IsInfinity(realization) ? 0 : realization;
                notAbsorbedInternal = double.IsNaN(notAbsorbedInternal) || double.IsInfinity(notAbsorbedInternal) ? 0 : notAbsorbedInternal;
                notAbsorbedExternal = double.IsNaN(notAbsorbedExternal) || double.IsInfinity(notAbsorbedExternal) ? 0 : notAbsorbedExternal;

                realizationNett = actualProduction; // kalo utk produksi pake gross kan?
                notAbsorbedInternalNett = (planReadiness - actualReadiness);
                notAbsorbedExternalNett = (actualReadiness - actualProduction);
            }

            Chart = new ReliabilityChartStub(realization, notAbsorbedInternal, notAbsorbedExternal, realizationNett, notAbsorbedInternalNett, notAbsorbedExternalNett);
        }
    }
}