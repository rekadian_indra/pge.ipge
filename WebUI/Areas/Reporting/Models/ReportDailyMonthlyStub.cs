﻿using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class ReportDailyMonthlyStub
	{
		public int AreaId { get; set; }
		public List<SelectListItem> AreaDropdown { get; set; }

		public int Year { get; set; }
		public List<SelectListItem> YearDropdown { get; set; }

		public DateTime Date { get; set; }

		public string SummaryAreaName { get; set; }

		public ReportDailyMonthlyStub()
		{
			Date = DateTime.Now;
		}

		public ReportDailyMonthlyStub(List<Business.Entities.Area> areas, List<int> years)
		{
			Date = DateTime.Now;

			AreaDropdown = new List<SelectListItem>
			{
				new SelectListItem()
				{
					Text = "Semua",
					Value = "0"
				}
			};
			foreach (Business.Entities.Area area in areas)
			{
				AreaDropdown.Add(new SelectListItem()
				{
					Text = area.Name,
					Value = area.Id.ToString()
				});
			}

			YearDropdown = new List<SelectListItem>
			{
				new SelectListItem()
				{
					Text = DateTime.Now.Year.ToString(),
					Value = DateTime.Now.Year.ToString()
				}
			};
			if (years != null)
			{
				years = years.OrderByDescending(x => x).ToList();
				foreach (int year in years)
				{
					if (!YearDropdown.Select(dropdown => dropdown.Value).Contains(year.ToString()))
						YearDropdown.Add(new SelectListItem()
						{
							Text = year.ToString(),
							Value = year.ToString()
						});
				}
			}
		}
	}
}