﻿using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.SalesOrder.Models;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class KobTableStub
	{
		public string PowerPlantName { get; set; }
		public string PeriodeStr { get; set; }
		public double QuantityLT30 { get; set; }
		public double NetPriceLT30 { get; set; }
		public double QuantityGT30 { get; set; }
		public double NetPriceGT30 { get; set; }
		public double Quantity { get; set; }
		public double NetValue { get; set; }

		public KobTableStub()
		{

		}

		public KobTableStub(List<Business.Entities.SalesOrder> salesOrders)
		{
			if (salesOrders.Count > 0)
			{
				PowerPlantName = salesOrders.ElementAt(0).PowerPlant.Name;
				PeriodeStr = salesOrders.ElementAt(0).Periode.Value.ToString(DisplayFormat.MonthYearDateFormat);
			}

			if (salesOrders.Count == 1)
			{
				QuantityLT30 = Math.Round(salesOrders.ElementAt(0).Quantity ?? 0, 2);
				NetPriceLT30 = Math.Round(salesOrders.ElementAt(0).NetPrice ?? 0, 5);
				QuantityGT30 = 0;
				NetPriceGT30 = 0;
				Quantity = Math.Round(salesOrders.ElementAt(0).Quantity ?? 0, 2);
				NetValue = Math.Round(salesOrders.ElementAt(0).NetValue ?? 0, 5);
			}
			else if (salesOrders.Count > 1)
			{
				QuantityLT30 = Math.Round(salesOrders.ElementAt(0).Quantity ?? 0, 2);
				NetPriceLT30 = Math.Round(salesOrders.ElementAt(0).NetPrice ?? 0, 5);
				QuantityGT30 = Math.Round(salesOrders.ElementAt(1).Quantity ?? 0, 2);
				NetPriceGT30 = Math.Round(salesOrders.ElementAt(1).NetPrice ?? 0, 5);
				Quantity = Math.Round(salesOrders.Sum(so => so.Quantity) ?? 0, 2);
				NetValue = Math.Round(salesOrders.Sum(so => so.NetValue) ?? 0, 5);
			}
			//QuantityLT30 = Math.Round(Math.Min(salesOrders.ElementAt(0).Quantity.Value, 30000000), 2);
			//NetPriceLT30 = Math.Round(salesOrders.ElementAt(0).NetPrice ?? 0, 5);
			//QuantityGT30 = Math.Round(salesOrders.ElementAt(0).Quantity.Value - Math.Min(salesOrder.Quantity.Value, 30000000), 2);
			//NetPriceGT30 = Math.Round(salesOrders.ElementAt(0).NetPrice ?? 0, 5);
		}
	}
}