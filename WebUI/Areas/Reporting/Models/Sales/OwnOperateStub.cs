﻿using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.SalesOrder.Models;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class OwnOperateStub
	{
		public string PowerPlantName { get; set; }
		public string PeriodeStr { get; set; }
		public double Ton { get; set; }
		public double BOE { get; set; }
		public double KWh { get; set; }
		public double PricePerKwh { get; set; }
		public double NetValue { get; set; }

		public OwnOperateStub()
		{

		}

		public OwnOperateStub(Business.Entities.SalesOrder salesOrder, bool hasTransmition = false)
		{
			bool isElectric = salesOrder.PowerPlant.Material == Material.ELECTRIC_POWER.ToString();

			PowerPlantName = salesOrder.PowerPlant.Name;
			PeriodeStr = salesOrder.Periode.Value.ToString(DisplayFormat.MonthYearDateFormat);
			Ton = Math.Round(salesOrder.PowerPlant?.Pltps?.Where(x => !x.IsDeleted && (x.TaskReport != null) && (x.TaskReport.Periode.HasValue) && (x.TaskReport.Periode.Value.Year == salesOrder.Periode?.Year) && (x.TaskReport.Periode.Value.Month == salesOrder.Periode?.Month))?.Sum(x => x.SteamUtilization) ?? 0, 2);
			BOE = Ton * 0.246356;
			KWh = Math.Round(salesOrder.Quantity ?? 0, 2);
            PricePerKwh = Math.Round(salesOrder.NetPrice ?? 0, 5);
            NetValue = Math.Round(salesOrder.NetValue ?? 0, 5);

            if (hasTransmition)
            {
                double transmitionPrice = salesOrder.PowerPlant.TransmitionPrice ?? 0;

                PricePerKwh = Math.Round(transmitionPrice, 5);
                NetValue = Math.Round((KWh * transmitionPrice), 5);
            }
		}
    }
}