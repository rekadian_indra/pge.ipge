﻿using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.SalesOrder.Models;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.Reporting.Models
{
	public class SummaryStub
	{
		public List<KobRow> KobRows { get; set; }
		public List<OwnOperateRow> OwnOperateRows { get; set; }
		public List<SummaryRow> SummaryRows { get; set; }

		public class KobRow
		{
			public string PowerPlantName;
			public double Quantity;
			public double NilaiPenjualan;

			public KobRow() { }
		}

		public class OwnOperateRow
		{
			public string PowerPlantName;
			public double Quantity;
			public double NilaiPenjualan;

			public OwnOperateRow() { }
		}

		public class SummaryRow
		{
			public string Deskripsi;
			public double Quantity;
			public double NilaiPenjualan;

			public SummaryRow() { }
		}

		public SummaryStub()
		{
			KobRows = new List<KobRow>();
			OwnOperateRows = new List<OwnOperateRow>();
			SummaryRows = new List<SummaryRow>();
		}

		public List<KobRow> GenerateKobRows(List<Business.Entities.PowerPlant> powerPlants, int year)
		{
			foreach (Business.Entities.PowerPlant powerPlant in powerPlants)
			{
				double? quantity = powerPlant.SalesOrders.Where(so => so.Periode.Value.Year == year && !so.IsDeleted).Sum(so => so.Quantity);
                double? netValue = powerPlant.SalesOrders.Where(so => so.Periode.Value.Year == year && !so.IsDeleted).Sum(so => so.NetValue);
				KobRows.Add(new KobRow()
				{
					PowerPlantName = powerPlant.Name,
					Quantity = Math.Round(quantity ?? 0, 2),
					NilaiPenjualan = Math.Round(netValue ?? 0, 5)
				});
			}

			return KobRows;
		}

        public List<OwnOperateRow> GenerateOwnOperateRows(List<Business.Entities.PowerPlant> powerPlants, int year)
		{
			foreach (Business.Entities.PowerPlant powerPlant in powerPlants)
			{
				double? quantity = powerPlant.SalesOrders.Where(so => so.Periode.Value.Year == year && !so.IsDeleted).Sum(so => so.Quantity);
				double? netValue = powerPlant.SalesOrders.Where(so => so.Periode.Value.Year == year && !so.IsDeleted).Sum(so => so.NetValue);
				OwnOperateRows.Add(new OwnOperateRow()
				{
					PowerPlantName = powerPlant.Name,
					Quantity = Math.Round(quantity ?? 0, 2),
					NilaiPenjualan = Math.Round(netValue ?? 0, 5)
				});
			}

			return OwnOperateRows;
		}

        public List<OwnOperateRow> GenerateOwnOperateTransmitionRows(List<Business.Entities.PowerPlant> powerPlants, int year)
        {
            foreach (Business.Entities.PowerPlant powerPlant in powerPlants)
            {
                double transmitionPrice = powerPlant.TransmitionPrice ?? 0;

                if (transmitionPrice > 0)
                {
                    double? quantity = powerPlant.SalesOrders.Where(so => so.Periode.Value.Year == year && !so.IsDeleted).Sum(so => so.Quantity);
                    double? netValue = quantity * transmitionPrice;

                    OwnOperateRows.Add(new OwnOperateRow()
                    {
                        PowerPlantName = powerPlant.Name,
                        Quantity = Math.Round(quantity ?? 0, 2),
                        NilaiPenjualan = Math.Round(netValue ?? 0, 5)
                    });
                }
            }

            return OwnOperateRows;
        }

        public List<SummaryRow> GenerateSummaryRows(List<Business.Entities.PowerPlant> powerPlants, int year)
		{
            double? quantity = powerPlants.SelectMany(pp => pp.SalesOrders).Where(so => so.Periode.Value.Year == year && !so.IsDeleted).Sum(so => so.Quantity);
			double? netValue = powerPlants.SelectMany(pp => pp.SalesOrders).Where(so => so.Periode.Value.Year == year && !so.IsDeleted).Sum(so => so.NetValue);
			SummaryRows.Add(new SummaryRow()
			{
				Deskripsi = "KOB & Own Operate",
				Quantity = Math.Round(quantity ?? 0, 2),
				NilaiPenjualan = Math.Round(netValue ?? 0, 5),
			});

            //listing jika transmition price != null
            powerPlants = powerPlants.Where(m => m.TransmitionPrice != null).ToList();
            if (powerPlants.Any())
            {
                List<SummaryRow> temps = new List<SummaryRow>();

                foreach(var m in powerPlants)
                {
                    double? transmitionPrice = m.TransmitionPrice ?? 0;
                    double? quantityTransmition = m.SalesOrders.Where(n => n.Periode.Value.Year == year && !n.IsDeleted).Sum(n => n.Quantity);
                    double? netValueTransmition = transmitionPrice * quantityTransmition;

                    temps.Add(new SummaryRow()
                    {
                        Quantity = quantityTransmition ?? 0,
                        NilaiPenjualan = netValueTransmition ?? 0
                    });
                }

                SummaryRows.Add(new SummaryRow()
                {
                    Deskripsi = "Own Operate (Transmition)",
                    Quantity = Math.Round(temps.Sum(s => s.Quantity), 2),
                    NilaiPenjualan = Math.Round(temps.Sum(s => s.NilaiPenjualan), 5),
                });
            }

            return SummaryRows;
		}
	}
}