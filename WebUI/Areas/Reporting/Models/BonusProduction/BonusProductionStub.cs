﻿using Business.Entities;
using Business.Entities.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Areas.Reporting.Models.BonusProduction
{
    public class BonusProductionStub
    {
        public int MonthValue { get; set; }
        public string Month { get; set; }
        public string PowerPlantName { get; set; }
        public double? Quantity { get; set; }
        public double? NetPrice { get; set; }
        public double? NetValue { get; set; }

        public BonusProductionStub()
        {

        }

        public static List<BonusProductionStub> MapList(List<PowerPlant> plants, List<ViewInvoicingSalesOrder> salesOrders, int year, int startMonth, int endMonth)
        {
            BonusProductionStub bonusModel = new BonusProductionStub();
            List<BonusProductionStub> bonusModels = new List<BonusProductionStub>();

            for(int i = startMonth; i <= endMonth; i++)
            {
                foreach(PowerPlant p in plants)
                {
                    BonusProductionStub temp = new BonusProductionStub()
                    {
                        PowerPlantName = p.Name,
                        MonthValue = i,
                        Month = new DateTime(year, i, 1).ToString("MMM"),
                        Quantity = 0,
                        NetPrice = 0,
                        NetValue = 0
                    };
                    bonusModels.Add(temp);
                }
            }

            foreach (BonusProductionStub n in bonusModels)
            {
                foreach (ViewInvoicingSalesOrder m in salesOrders)
                {
                    if (n.PowerPlantName == m.Name && n.MonthValue == m.Month)
                    {
                        bonusModels.Where(x => x.PowerPlantName == m.Name && x.MonthValue == m.Month)
                            .FirstOrDefault().Quantity = m.Quantity;

                        bonusModels.Where(x => x.PowerPlantName == m.Name && x.MonthValue == m.Month)
                            .FirstOrDefault().NetPrice = m.NetPrice;

                        bonusModels.Where(x => x.PowerPlantName == m.Name && x.MonthValue == m.Month)
                            .FirstOrDefault().NetValue = m.NetValue;
                    }
                }
            }
            return bonusModels;
        }
    }
}