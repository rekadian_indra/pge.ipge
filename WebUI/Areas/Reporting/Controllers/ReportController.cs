﻿using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Infrastructure;
using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using WebUI.Controllers;
using Business.Infrastructure;
using System;
using System.Web;
using System.Linq;
using WebUI.Extension;
using Resources;
using WebUI.Areas.DataMaster.Models;
using MvcSiteMapProvider.Web.Mvc.Filters;
using NPOI.XSSF.UserModel;
using System.IO;
using WebUI.Areas.Reporting.Models;
using Business.Entities.Views;
using WebUI.Areas.Reporting.Models.BonusProduction;
using WebUI.Areas.Reporting.Models.Kpi;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.Reporting.Controllers
{
    //[Authorize]
    //[CheckSessionTimeOut]
    public class ReportController : BaseController<ReportSalesStub>
    {
		#region Constructor
		public ReportController
		(
            ILogRepository repoLog,
            IPpiRepository repoPPI,
			ISteamPowerPlantPriceRepository repoSteamPowerPlantPrice,
			IPowerPlantRepository repoPowerPlant,
			IWorkflowRepository repoWorkflow,
			ISalesOrderRepository repoSalesOrder,
			IPpiApprovalRepository repoPpiApproval,
			IPltpRepository repoPltp,
			IDkpRepository repoDkp,
			IReInjectionRepository repoReInjection,
			IMonitoringRepository repoMonitoring,
			IProductionRepository repoProduction,
			IProductionPlanRepository repoProductionPlan,
			IInterfaceScrubberRepository repoInterfaceScrubber,
			ITaskReportRepository repoTaskReport,
			IAreaRepository repoArea,
			IClusterRepository repoCluster,
			IWellRepository repoWell,
			IExplorationRepository repoExploration,
			IViewPltpRepository repoViewPltp,
			IViewPpiRepository repoViewPpi,
			IViewProductionPlanDetailRepository repoViewProductionPlanDetail,
			IViewDkpRepository repoViewDkp,
			IViewReliabilityPlanRepository repoViewReliabilityPlan,
			IViewReliabilityActualRepository repoViewReliabilityActual,
            IViewInvoicingSalesOrderRepository repoViewInvoicingSalesOrder,
            IViewInvoicingKpiRepository repoViewInvoicingKpi

        )
		{
            RepoLog = repoLog;
            RepoPPI = repoPPI;
			RepoSteamPowerPlantPrice = repoSteamPowerPlantPrice;
			RepoPowerPlant = repoPowerPlant;
			RepoWorkflow = repoWorkflow;
			RepoSalesOrder = repoSalesOrder;
			RepoPpiApproval = repoPpiApproval;
			RepoPltp = repoPltp;
			RepoDkp = repoDkp;
			RepoReinjection = repoReInjection;
			RepoMonitoring = repoMonitoring;
			RepoProduction = repoProduction;
			RepoProductionPlan = repoProductionPlan;
			RepoInterfaceScrubber = repoInterfaceScrubber;
			RepoTaskReport = repoTaskReport;
			RepoArea = repoArea;
			RepoCluster = repoCluster;
			RepoWell = repoWell;
			RepoExploration = repoExploration;
			RepoViewPltp = repoViewPltp;
			RepoViewPpi = repoViewPpi;
			RepoViewProductionPlanDetail = repoViewProductionPlanDetail;
			RepoViewDkp = repoViewDkp;
			RepoViewReliabilityPlan = repoViewReliabilityPlan;
			RepoViewReliabilityActual = repoViewReliabilityActual;
            RepoViewInvoicingSalesOrder = repoViewInvoicingSalesOrder;
            RepoViewInvoicingKpi = repoViewInvoicingKpi;
        }

		#endregion

		#region View

		[MvcSiteMapNode(Title = TitleSite.ReportSales, Area = AreaSite.Reporting, ParentKey = KeySite.Dashboard, Key = KeySite.ReportSales)]
		public async Task<ActionResult> Sales()
		{
			List<Business.Entities.SalesOrder> salesOrderDbs = await RepoSalesOrder.FindAllAsync();

			ReportSalesStub reportSales = new ReportSalesStub(salesOrderDbs);

			await Task.Delay(0);

			return View(reportSales);
		}

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.OPERATION_ADMIN, UserRole.OPERATION_OPERATOR, UserRole.OPERATION_SUPERVISOR })]
        [MvcSiteMapNode(Title = TitleSite.ReportManagement, Area = AreaSite.Reporting, ParentKey = KeySite.Dashboard, Key = KeySite.ReportManagement)]
		public async Task<ActionResult> Management()
		{
			List<Business.Entities.Area> areaDbs = await RepoArea.FindAllAsync();

			ReportManagementStub reportManagement = new ReportManagementStub(areaDbs);

			await Task.Delay(0);

			return View(reportManagement);
		}

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.OPERATION_ADMIN, UserRole.OPERATION_OPERATOR, UserRole.OPERATION_SUPERVISOR })]
        [MvcSiteMapNode(Title = TitleSite.ReportDailyMonthly, Area = AreaSite.Reporting, ParentKey = KeySite.Dashboard, Key = KeySite.ReportDailyMonthly)]
		public async Task<ActionResult> DailyMonthly()
		{
			List<Business.Entities.Area> areaDbs = await RepoArea.FindAllAsync();
			List< Business.Entities.TaskReport> taskReports = (await RepoTaskReport.FindAllAsync()).Where(report => report.Periode.HasValue)?.ToList();
			List<int> reportExistYears = taskReports?.Select(report => report.Periode.Value.Year)?.ToList();

			ReportDailyMonthlyStub reportDailyMonthly = new ReportDailyMonthlyStub(areaDbs, reportExistYears);

			await Task.Delay(0);

			return View(reportDailyMonthly);
		}

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.OPERATION_ADMIN, UserRole.OPERATION_OPERATOR, UserRole.OPERATION_SUPERVISOR })]
        [MvcSiteMapNode(Title = TitleSite.ReportReliability, Area = AreaSite.Reporting, ParentKey = KeySite.Dashboard, Key = KeySite.ReportReliability)]
		public async Task<ActionResult> Reliability()
		{
			List<ViewDkp> dkps = RepoViewDkp.FindAll();
			List<ViewReliabilityPlan> reliabilityPlans = RepoViewReliabilityPlan.FindAll();

			List<int> reliabilityExistYears = (dkps?.Select(x => x.Year).Concat(reliabilityPlans?.Select(x => x.Periode.Year) ?? new List<int>()) ?? new List<int>()).Distinct().OrderByDescending(x => x).ToList();
			ReportReliabilityStub reportReliability = new ReportReliabilityStub(reliabilityExistYears);

			await Task.Delay(0);

			return View(reportReliability);
		}

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.RESERVOIR })]
        [MvcSiteMapNode(Title = TitleSite.ReportGeoscience, Area = AreaSite.Reporting, ParentKey = KeySite.Dashboard, Key = KeySite.ReportGeoscience)]
		public async Task<ActionResult> Geoscience()
		{
			List<Business.Entities.Area> areaDbs = await RepoArea.FindAllAsync();
			ReportGeoscienceStub reportGeoscience = new ReportGeoscienceStub(areaDbs);

			await Task.Delay(0);

			return View(reportGeoscience);
		}

        [MvcSiteMapNode(Title = TitleSite.ReportBonusProduction, Area = AreaSite.Reporting, ParentKey = KeySite.Dashboard, Key = KeySite.ReportBonusProduction)]
        public async Task<ActionResult> BonusProduction()
        {
            ReportParamStub model = new ReportParamStub();

            await Task.Delay(0);

            return View(model);
        }

        [MvcSiteMapNode(Title = TitleSite.ReportKpi, Area = AreaSite.Reporting, ParentKey = KeySite.Dashboard, Key = KeySite.ReportKpi)]
        public async Task<ActionResult> KpiCollection()
        {
            ReportParamStub model = new ReportParamStub();

            await Task.Delay(0);

            return View("KpiApAr", model);
        }
		#endregion

		#region Binding Sales Report (via service)

		public async Task<string> BindingKobTables(int year)
		{
			//lib
			List<Business.Entities.PowerPlant> powerPlantDbs = await RepoPowerPlant.FindAllBySOType(SOType.ZTS);
			List<KobTableStub> kobTables = new List<KobTableStub>();

			DateTime curPeriode = new DateTime(year, 1, 1);
			//List<Business.Entities.SalesOrder> salesOrderDbs = RepoSalesOrder.GetFromPeriodeSOType(curPeriode, SOType.ZTS.ToString(), RangeData.YEARLY);

			foreach (Business.Entities.PowerPlant powerPlantDb in powerPlantDbs)
			{
				List<Business.Entities.SalesOrder> salesOrderDbs = RepoSalesOrder.GetFromPeriodePowerPlant(curPeriode, powerPlantDb.Id, RangeData.YEARLY);
				DateTime iteratePeriode = curPeriode.AddMonths(-1);
				while ((iteratePeriode = iteratePeriode.AddMonths(1)) < curPeriode.AddYears(1)) // iterasi sampe tahunnya lebih dari curPeriode
				{
					List<Business.Entities.SalesOrder> curMonthSalesOrders = salesOrderDbs.Where(so => so.Periode.Value.Month == iteratePeriode.Month).ToList();
					if (curMonthSalesOrders.Any())
					{
						kobTables.Add(new KobTableStub(curMonthSalesOrders));
					}
				}
			}

			await Task.Delay(0);

			return JsonConvert.SerializeObject(kobTables);
		}

		public async Task<string> BindingOwnOperate(int year)
		{
			//lib
			List<OwnOperateStub> ownTables = new List<OwnOperateStub>();

			DateTime curPeriode = new DateTime(year, 1, 1);
			List<Business.Entities.SalesOrder> salesOrderDbs = RepoSalesOrder.GetFromPeriodeSOType(curPeriode, SOType.ZPGE.ToString(), RangeData.YEARLY);

            //sort by periode
            salesOrderDbs = salesOrderDbs.OrderBy(m => m.Periode.Value).ToList();

            foreach (Business.Entities.SalesOrder salesOrderDb in salesOrderDbs)
			{
				ownTables.Add(new OwnOperateStub(salesOrderDb));
			}

			await Task.Delay(0);

			return JsonConvert.SerializeObject(ownTables);
		}

        public async Task<string> BindingOwnOperateTransmition(int year)
        {
            //lib
            List<OwnOperateStub> ownTables = new List<OwnOperateStub>();

            DateTime curPeriode = new DateTime(year, 1, 1);
            List<Business.Entities.SalesOrder> salesOrderDbs = RepoSalesOrder.GetFromPeriodeSOType(curPeriode, SOType.ZPGE.ToString(), RangeData.YEARLY);

            //sort by periode
            salesOrderDbs = salesOrderDbs.Where(m => m.PowerPlant.TransmitionPrice != null)
                .OrderBy(m => m.Periode.Value).ToList();

            foreach (Business.Entities.SalesOrder salesOrderDb in salesOrderDbs)
            {
                ownTables.Add(new OwnOperateStub(salesOrderDb, true));
            }

            await Task.Delay(0);

            return JsonConvert.SerializeObject(ownTables);
        }

        public async Task<string> BindingSummaryKob(int year)
		{
			//lib
			SummaryStub summary = new SummaryStub();
			List<Business.Entities.PowerPlant> powerPlantDbs = await RepoPowerPlant.FindAllBySOType(SOType.ZTS);
			summary.GenerateKobRows(powerPlantDbs, year);

			return JsonConvert.SerializeObject(summary.KobRows);
		}

		public async Task<string> BindingSummaryOwnOperate(int year)
		{
			//lib
			SummaryStub summary = new SummaryStub();
			List<Business.Entities.PowerPlant> powerPlantDbs = await RepoPowerPlant.FindAllBySOType(SOType.ZPGE);
			summary.GenerateOwnOperateRows(powerPlantDbs, year);

			return JsonConvert.SerializeObject(summary.OwnOperateRows);
		}

        public async Task<string> BindingSummaryOwnOperateTransmition(int year)
        {
            //lib
            SummaryStub summary = new SummaryStub();
            List<Business.Entities.PowerPlant> powerPlantDbs = await RepoPowerPlant.FindAllBySOType(SOType.ZPGE);
            summary.GenerateOwnOperateTransmitionRows(powerPlantDbs, year);

            return JsonConvert.SerializeObject(summary.OwnOperateRows);
        }

        public async Task<string> BindingSummarySummary(int year)
		{
			//lib
			SummaryStub summary = new SummaryStub();
			List<Business.Entities.PowerPlant> powerPlantDbs = await RepoPowerPlant.FindAllAsync();
			summary.GenerateSummaryRows(powerPlantDbs, year);

			return JsonConvert.SerializeObject(summary.SummaryRows);
		}

		#endregion

		#region Binding Operation Report (via service)

		public async Task<string> BindingOperationManagement(DateTime date, int areaId)
		{
			//lib
			List<Business.Entities.Pltp> pltpDbs = await RepoPltp.FindAllByPeriodeArea(date, areaId);

			List<ManagementRowStub> managementRows = new List<ManagementRowStub>();

			if (pltpDbs.Any())
			foreach (Business.Entities.Pltp pltpDb in pltpDbs)
			{
				managementRows.Add(new ManagementRowStub(pltpDb));
			}

			return JsonConvert.SerializeObject(managementRows);
		}

		public async Task<string> BindingOperationDailyListrik(DateTime date, int areaId)
		{
			//get data
			List<Business.Entities.PowerPlant> powerPlantDbs = await RepoPowerPlant.FindAllByArea(areaId);
			List<ViewPltp> pltps = await RepoViewPltp.FindAllAsync();

			DailyMonthlyStub dailyMonthly = new DailyMonthlyStub(date);
			dailyMonthly.GenerateDailyListriks(powerPlantDbs, pltps);

			return JsonConvert.SerializeObject(dailyMonthly.DailyListriks);
		}

		public async Task<string> BindingOperationDailyUap(DateTime date, int areaId)
		{
			//get data
			List<Business.Entities.PowerPlant> powerPlantDbs = await RepoPowerPlant.FindAllByArea(areaId);
			List<ViewPltp> pltps = await RepoViewPltp.FindAllAsync();

			DailyMonthlyStub dailyMonthly = new DailyMonthlyStub(date);
			dailyMonthly.GenerateDailyUaps(powerPlantDbs, pltps);

			return JsonConvert.SerializeObject(dailyMonthly.DailyUaps);
		}

		public async Task<string> BindingOperationDailyDKP(DateTime date, int areaId)
		{
			//get data
			List<Business.Entities.Dkp> dkpDbs = await RepoDkp.FindAllByPeriodeArea(date, areaId);

			DailyMonthlyStub dailyMonthly = new DailyMonthlyStub(date);
			dailyMonthly.GenerateDailyDKPs(dkpDbs);

			return JsonConvert.SerializeObject(dailyMonthly.DailyDKPs);
		}

		public async Task<string> BindingOperationDailySumurProduksi(DateTime date, int areaId)
		{
			//get data
			List<Business.Entities.Production> productionDbs = await RepoProduction.FindAllByPeriodeArea(date, areaId);

			DailyMonthlyStub dailyMonthly = new DailyMonthlyStub(date);
			dailyMonthly.GenerateDailySumurProduksis(productionDbs);

			return JsonConvert.SerializeObject(dailyMonthly.DailySumurProduksis);
		}

		public async Task<string> BindingOperationDailySumurInjeksi(DateTime date, int areaId)
		{
			//get data
			List<Business.Entities.ReInjection> reinjectionDbs = await RepoReinjection.FindAllByPeriodeArea(date, areaId);

			DailyMonthlyStub dailyMonthly = new DailyMonthlyStub(date);
			dailyMonthly.GenerateDailySumurInjeksis(reinjectionDbs);

			return JsonConvert.SerializeObject(dailyMonthly.DailySumurInjeksis);
		}

		public async Task<string> BindingOperationDailySumurMonitoring(DateTime date, int areaId)
		{
			//get data
			List<Business.Entities.Monitoring> monitoringDbs = await RepoMonitoring.FindAllByPeriodeArea(date, areaId);

			DailyMonthlyStub dailyMonthly = new DailyMonthlyStub(date);
			dailyMonthly.GenerateDailySumurMonitorings(monitoringDbs);

			return JsonConvert.SerializeObject(dailyMonthly.DailySumurMonitorings);
		}

		public async Task<string> BindingOperationDailyInterfaceScrubber(DateTime date, int areaId)
		{
			//get data
			List<Business.Entities.InterfaceScrubber> interfaceScrubberDbs = await RepoInterfaceScrubber.FindAllByPeriodeArea(date, areaId);

			DailyMonthlyStub dailyMonthly = new DailyMonthlyStub(date);
			dailyMonthly.GenerateDailyInterfaceScrubbers(interfaceScrubberDbs);

			return JsonConvert.SerializeObject(dailyMonthly.DailyInterfaceScrubbers);
		}

		public async Task<string> BindingOperationMonthlyPowerPlants(int year, int areaId)
		{
			List<Business.Entities.PowerPlant> powerPlantDbs = await RepoPowerPlant.FindAllByArea(areaId);
			DailyMonthlyStub dailyMonthly = new DailyMonthlyStub(year);

			DateTime curPeriode = new DateTime(year, 1, 1);

			Task<List<Business.Entities.Views.ViewPltp>> pltpsT = RepoViewPltp.FindAllAsync();
			Task<List<Business.Entities.Views.ViewPpi>> ppisT = RepoViewPpi.FindAllAsync();
			Task<List<Business.Entities.Views.ViewProductionPlanDetail>> productionPlanDetailT = RepoViewProductionPlanDetail.FindAllAsync();

			Task.WaitAll(pltpsT, ppisT, productionPlanDetailT);

			List<Business.Entities.Views.ViewPltp> pltps = pltpsT.Result;
			List<Business.Entities.Views.ViewPpi> ppis = ppisT.Result;
			List<Business.Entities.Views.ViewProductionPlanDetail> productionPlanDetails = productionPlanDetailT.Result;

			foreach (Business.Entities.PowerPlant powerPlantDb in powerPlantDbs)
			{
				List<Business.Entities.Views.ViewPltp> yearlyPltp = pltps.Where(x => x.PowerPlantId == powerPlantDb.Id && x.Year == year).ToList();
				List<Business.Entities.Views.ViewPpi> yearlyPpi = ppis.Where(x => x.PowerPlantId == powerPlantDb.Id && x.Year == year).ToList();
				List<Business.Entities.Views.ViewProductionPlanDetail> yearlyPlanDetailSteam = productionPlanDetails.Where(x => x.PowerPlantId == powerPlantDb.Id && x.UtilType == Common.Enums.ProductionUtilization.STEAM.ToString() && x.Year == year).ToList();
				List<Business.Entities.Views.ViewProductionPlanDetail> yearlyPlanDetailElectric = productionPlanDetails.Where(x => x.PowerPlantId == powerPlantDb.Id && x.UtilType == Common.Enums.ProductionUtilization.ELECTRIC.ToString() && x.Year == year).ToList();
				DailyMonthlyStub.MonthlyProduction monthlyProductionTable = new DailyMonthlyStub.MonthlyProduction(powerPlantDb, yearlyPltp, yearlyPpi, yearlyPlanDetailSteam, yearlyPlanDetailElectric, year);
				dailyMonthly.MonthlyProductionTable.Add(monthlyProductionTable);
			}

			//Task<List<Business.Entities.Pltp>>[] yearlyPltpT = new Task<List<Pltp>>[powerPlantDbs.Count];
			//Task<List<Business.Entities.ProductionPlan>>[] yearlyPlanSteamT = new Task<List<ProductionPlan>>[powerPlantDbs.Count];
			//Task<List<Business.Entities.ProductionPlan>>[] yearlyPlanElectricT = new Task<List<ProductionPlan>>[powerPlantDbs.Count];
			//int i = 0;
			//foreach (Business.Entities.PowerPlant powerPlantDb in powerPlantDbs)
			//{
			//	yearlyPltpT[i] = RepoPltp.FindAllByPowerPlant(powerPlantDb.Id, year);
			//	yearlyPlanSteamT[i] = RepoProductionPlan.GetFromPowerPlantIdAsync(powerPlantDb.Id, year, ProductionUtilization.STEAM);
			//	yearlyPlanElectricT[i] = RepoProductionPlan.GetFromPowerPlantIdAsync(powerPlantDb.Id, year, ProductionUtilization.ELECTRIC);
			//	i++;
			//}

			//i = 0;
			//foreach (Business.Entities.PowerPlant powerPlantDb in powerPlantDbs)
			//{
			//	//List<Business.Entities.Pltp> yearlyPltp = powerPlantDb.Pltps.Where(x => (!x.IsDeleted) && (x.TaskReport != null) && (!x.TaskReport.IsDeleted) && (x.TaskReport.Periode?.Year == year)).ToList();
			//	List<Business.Entities.PPI> ppis = powerPlantDb.PPIs?.Where(x => !x.IsDeleted).ToList();
			//	//List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailSteam = powerPlantDb.ProductionPlans?.FirstOrDefault(x => (!x.IsDeleted) && (x.Year == year) && (x.UtilType == Common.Enums.ProductionUtilization.STEAM.ToString()))?.ProductionPlanDetails.ToList();
			//	//List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailElectric = powerPlantDb.ProductionPlans?.FirstOrDefault(x => (!x.IsDeleted) && (x.Year == year) && (x.UtilType == Common.Enums.ProductionUtilization.ELECTRIC.ToString()))?.ProductionPlanDetails.ToList();
			//	Task.WaitAll(yearlyPltpT[i], yearlyPlanSteamT[i], yearlyPlanElectricT[i]);
			//	List<Business.Entities.Pltp> yearlyPltp = yearlyPltpT[i].Result;
			//	List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailSteam = yearlyPlanSteamT[i].Result.FirstOrDefault()?.ProductionPlanDetails?.ToList();
			//	List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailElectric = yearlyPlanElectricT[i].Result.FirstOrDefault()?.ProductionPlanDetails?.ToList();
			//	DailyMonthlyStub.MonthlyProduction monthlyProductionTable = new DailyMonthlyStub.MonthlyProduction(powerPlantDb, yearlyPltp, ppis, yearlyPlanDetailSteam, yearlyPlanDetailElectric, year);
			//	dailyMonthly.MonthlyProductionTable.Add(monthlyProductionTable);

			//	i++;
			//}

			List<DailyMonthlyStub.MonthlyProduction.MonthlyProductionRow> monthlyProductionRows = dailyMonthly.MonthlyProductionTable.SelectMany(x => x.MonthlyProductionRows).ToList();
			return JsonConvert.SerializeObject(monthlyProductionRows);
		}

		public async Task<string> BindingOperationMonthlySummary(int year, int areaId)
		{
			List<Business.Entities.PowerPlant> powerPlantDbs = await RepoPowerPlant.FindAllByArea(areaId);
			DailyMonthlyStub dailyMonthly = new DailyMonthlyStub(year);

			DateTime curPeriode = new DateTime(year, 1, 1);

			Task<List<Business.Entities.Views.ViewPltp>> pltpsT = RepoViewPltp.FindAllAsync();
			Task<List<Business.Entities.Views.ViewPpi>> ppisT = RepoViewPpi.FindAllAsync();
			Task<List<Business.Entities.Views.ViewProductionPlanDetail>> productionPlanDetailT = RepoViewProductionPlanDetail.FindAllAsync();

			Task.WaitAll(pltpsT, ppisT, productionPlanDetailT);

			List<Business.Entities.Views.ViewPltp> pltps = pltpsT.Result;
			List<Business.Entities.Views.ViewPpi> ppis = ppisT.Result;
			List<Business.Entities.Views.ViewProductionPlanDetail> productionPlanDetails = productionPlanDetailT.Result;

			foreach (Business.Entities.PowerPlant powerPlantDb in powerPlantDbs)
			{
				List<Business.Entities.Views.ViewPltp> yearlyPltp = pltps.Where(x => x.PowerPlantId == powerPlantDb.Id && x.Year == year).ToList();
				List<Business.Entities.Views.ViewPpi> yearlyPpi = ppis.Where(x => x.PowerPlantId == powerPlantDb.Id && x.Year == year).ToList();
				List<Business.Entities.Views.ViewProductionPlanDetail> yearlyPlanDetailSteam = productionPlanDetails.Where(x => x.PowerPlantId == powerPlantDb.Id && x.UtilType == Common.Enums.ProductionUtilization.STEAM.ToString() && x.Year == year).ToList();
				List<Business.Entities.Views.ViewProductionPlanDetail> yearlyPlanDetailElectric = productionPlanDetails.Where(x => x.PowerPlantId == powerPlantDb.Id && x.UtilType == Common.Enums.ProductionUtilization.ELECTRIC.ToString() && x.Year == year).ToList();
				DailyMonthlyStub.MonthlyProduction monthlyProductionTable = new DailyMonthlyStub.MonthlyProduction(powerPlantDb, yearlyPltp, yearlyPpi, yearlyPlanDetailSteam, yearlyPlanDetailElectric, year);
				dailyMonthly.MonthlyProductionTable.Add(monthlyProductionTable);
			}

			//Task<List<Business.Entities.Pltp>>[] yearlyPltpT = new Task<List<Pltp>>[powerPlantDbs.Count];
			//Task<List<Business.Entities.ProductionPlan>>[] yearlyPlanSteamT = new Task<List<ProductionPlan>>[powerPlantDbs.Count];
			//Task<List<Business.Entities.ProductionPlan>>[] yearlyPlanElectricT = new Task<List<ProductionPlan>>[powerPlantDbs.Count];
			//int i = 0;
			//foreach (Business.Entities.PowerPlant powerPlantDb in powerPlantDbs)
			//{
			//	yearlyPltpT[i] = RepoPltp.FindAllByPowerPlant(powerPlantDb.Id, year);
			//	yearlyPlanSteamT[i] = RepoProductionPlan.GetFromPowerPlantIdAsync(powerPlantDb.Id, year, ProductionUtilization.STEAM);
			//	yearlyPlanElectricT[i] = RepoProductionPlan.GetFromPowerPlantIdAsync(powerPlantDb.Id, year, ProductionUtilization.ELECTRIC);
			//	i++;
			//}

			//i = 0;
			//foreach (Business.Entities.PowerPlant powerPlantDb in powerPlantDbs)
			//{
			//	//List<Business.Entities.Pltp> yearlyPltp = powerPlantDb.Pltps.Where(x => (!x.IsDeleted) && (x.TaskReport != null) && (!x.TaskReport.IsDeleted) && (x.TaskReport.Periode?.Year == year)).ToList();
			//	List<Business.Entities.PPI> ppis = powerPlantDb.PPIs?.Where(x => !x.IsDeleted).ToList();
			//	//List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailSteam = powerPlantDb.ProductionPlans?.FirstOrDefault(x => (!x.IsDeleted) && (x.Year == year) && (x.UtilType == Common.Enums.ProductionUtilization.STEAM.ToString()))?.ProductionPlanDetails.ToList();
			//	//List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailElectric = powerPlantDb.ProductionPlans?.FirstOrDefault(x => (!x.IsDeleted) && (x.Year == year) && (x.UtilType == Common.Enums.ProductionUtilization.ELECTRIC.ToString()))?.ProductionPlanDetails.ToList();
			//	Task.WaitAll(yearlyPltpT[i], yearlyPlanSteamT[i], yearlyPlanElectricT[i]);
			//	List<Business.Entities.Pltp> yearlyPltp = yearlyPltpT[i].Result;
			//	List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailSteam = yearlyPlanSteamT[i].Result.FirstOrDefault()?.ProductionPlanDetails?.ToList();
			//	List<Business.Entities.ProductionPlanDetail> yearlyPlanDetailElectric = yearlyPlanElectricT[i].Result.FirstOrDefault()?.ProductionPlanDetails?.ToList();
			//	DailyMonthlyStub.MonthlyProduction monthlyProductionTable = new DailyMonthlyStub.MonthlyProduction(powerPlantDb, yearlyPltp, ppis, yearlyPlanDetailSteam, yearlyPlanDetailElectric, year);
			//	dailyMonthly.MonthlyProductionTable.Add(monthlyProductionTable);
			//}

			string areaName = "SEMUA";
			if (areaId != 0)
			{
				areaName = RepoArea.FindByPrimaryKey(areaId)?.Name?.ToUpper();
			}
			dailyMonthly.GenerateMonthlySummaries(areaName);

			return JsonConvert.SerializeObject(dailyMonthly.MonthlySummaries);
		}

		public async Task<string> BindingOperationReliability(int year, int month, bool isCumulative)
		{
			//get data
			Task<List<Business.Entities.PowerPlant>> powerPlantDbsT = RepoPowerPlant.FindAllBySOType(SOType.ZPGE);
			Task<List<ViewDkp>> dkpsT = isCumulative ? RepoViewDkp.FindAllUntilPeriodeYearlyAsync(year, month) : RepoViewDkp.FindAllByPeriodeAsync(year, month);
			Task<List<ViewReliabilityPlan>> reliabilityPlansT = isCumulative ? RepoViewReliabilityPlan.FindAllUntilPeriodeYearlyAsync(year, month) : RepoViewReliabilityPlan.FindAllByPeriodeAsync(year, month);
			Task<List<ViewPltp>> pltpsT = RepoViewPltp.FindAllAsync();

			Task.WaitAll(powerPlantDbsT, dkpsT, reliabilityPlansT, pltpsT);

			List<Business.Entities.PowerPlant> powerPlantDbs = powerPlantDbsT.Result;
			List<ViewDkp> dkps = dkpsT.Result;
			List<ViewReliabilityPlan> reliabilityPlans = reliabilityPlansT.Result;
			List<ViewPltp> pltps = pltpsT.Result;

			ReliabilityStub model = new ReliabilityStub(powerPlantDbs, pltps, reliabilityPlans, dkps, year, month, isCumulative);
			
			await Task.Delay(0);
			//DailyMonthlyStub dailyMonthly = new DailyMonthlyStub(date);
			//dailyMonthly.GenerateDailySumurProduksis(productionDbs);

			return JsonConvert.SerializeObject(model);
		}

		#endregion

		#region Binding Geoscience (via service)

		public async Task<string> BindingGeoscienceMD(DateTime startDate, DateTime endDate, int wellId)
		{
			List<Business.Entities.Exploration> explorationDbs = await RepoExploration.GetFromPeriodeWell(startDate, endDate, wellId);
			GeoscienceChartStub geoscienceChart = new GeoscienceChartStub();

			List<GeoscienceChartStub> chart = geoscienceChart.GenerateChartMD(explorationDbs);
			if (chart.Count <= 0)
			{
				chart.Add(new GeoscienceChartStub());
			}

			await Task.Delay(0);

			return JsonConvert.SerializeObject(chart);
		}

		public async Task<string> BindingGeoscienceTVD(DateTime startDate, DateTime endDate, int wellId)
		{
			List<Business.Entities.Exploration> explorationDbs = await RepoExploration.GetFromPeriodeWell(startDate, endDate, wellId);
			GeoscienceChartStub geoscienceChart = new GeoscienceChartStub();

			List<GeoscienceChartStub> chart = geoscienceChart.GenerateChartTVD(explorationDbs);
			if (chart.Count <= 0)
			{
				chart.Add(new GeoscienceChartStub());
			}

			await Task.Delay(0);

			return JsonConvert.SerializeObject(chart);
		}

		public async Task<string> BindingGeoscienceElevation(DateTime startDate, DateTime endDate, int wellId)
		{
			List<Business.Entities.Exploration> explorationDbs = await RepoExploration.GetFromPeriodeWell(startDate, endDate, wellId);
			GeoscienceChartStub geoscienceChart = new GeoscienceChartStub();

			List<GeoscienceChartStub> chart = geoscienceChart.GenerateChartElevation(explorationDbs);
			if (chart.Count <= 0)
			{
				chart.Add(new GeoscienceChartStub());
			}

			await Task.Delay(0);

			return JsonConvert.SerializeObject(chart);
		}

		public async Task<string> BindingGeoscienceShoesMD()
		{
			GeoscienceChartStub geoscienceChart = new GeoscienceChartStub();

			List<GeoscienceChartStub> chart = GeoscienceChartStub.GetShoesMD();
			if (chart.Count <= 0)
			{
				chart.Add(new GeoscienceChartStub());
			}

			await Task.Delay(0);

			return JsonConvert.SerializeObject(chart);
		}

		public async Task<string> BindingGeoscienceShoesTVD()
		{
			GeoscienceChartStub geoscienceChart = new GeoscienceChartStub();

			List<GeoscienceChartStub> chart = GeoscienceChartStub.GetShoesTVD();
			if (chart.Count <= 0)
			{
				chart.Add(new GeoscienceChartStub());
			}

			await Task.Delay(0);

			return JsonConvert.SerializeObject(chart);
		}

		public async Task<string> BindingGeoscienceShoesElevation()
		{
			GeoscienceChartStub geoscienceChart = new GeoscienceChartStub();

			List<GeoscienceChartStub> chart = GeoscienceChartStub.GetShoesElevation();
			if (chart.Count <= 0)
			{
				chart.Add(new GeoscienceChartStub());
			}

			await Task.Delay(0);

			return JsonConvert.SerializeObject(chart);
		}

        #endregion

        #region "Binding Finance Report"
        public async Task<string> BindingBonusProductionSteam(int areaId, int year, int startMonth, int endMonth)
        {
            List<Business.Entities.PowerPlant> plants;
            List<ViewInvoicingSalesOrder> salesOrders;
            Task<List<ViewInvoicingSalesOrder>> taskSalesOrders = null;
            Task<List<Business.Entities.PowerPlant>> taskPlants;
            List<BonusProductionStub> models = new List<BonusProductionStub>();
            List<SortQuery> sorts = new List<SortQuery>
            {
                new SortQuery(Field.Name, SortOrder.Ascending)
            };

            FilterQuery filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, areaId);
            filter.AddFilter(Field.Year, FilterOperator.Equals, year);
            filter.AddFilter(Field.Month, FilterOperator.GreaterThanOrEquals, startMonth);
            filter.AddFilter(Field.Month, FilterOperator.LessThanOrEquals, endMonth);
            filter.AddFilter(Field.Material, FilterOperator.Equals, Material.GEOTHERMAL_STEAM.ToString());

            FilterQuery filter1 = new FilterQuery(Field.AreaId, FilterOperator.Equals, areaId);
            filter1.AddFilter(Field.Material, FilterOperator.Equals, Material.GEOTHERMAL_STEAM.ToString());

            if (areaId == 0)
            {
                if (filter.Filters.Any())
                {
                    if (filter.Filters.Exists(f => f.Field == Field.AreaId))
                    {
                        filter.RemoveFilter(Field.AreaId);
                    }
                }

                if (filter1.Filters.Any())
                {
                    if (filter1.Filters.Exists(f => f.Field == Field.AreaId))
                    {
                        filter1.RemoveFilter(Field.AreaId);
                    }
                }
            }

            taskSalesOrders = RepoViewInvoicingSalesOrder.FindAllAsync(sorts, filter);
            taskPlants = RepoPowerPlant.FindAllAsync(sorts, filter1);

            await Task.WhenAll(taskSalesOrders, taskPlants);

            salesOrders = taskSalesOrders.Result;
            plants = taskPlants.Result;

            models = BonusProductionStub.MapList(plants, salesOrders, year, startMonth, endMonth);
            return JsonConvert.SerializeObject(models);
        }

        public async Task<string> BindingBonusProductionElectric(int areaId, int year, int startMonth, int endMonth)
        {
            //lib
            List<Business.Entities.PowerPlant> plants;
            List<ViewInvoicingSalesOrder> salesOrders;
            Task<List<ViewInvoicingSalesOrder>> taskSalesOrders = null;
            Task<List<Business.Entities.PowerPlant>> taskPlants = null;
            List<BonusProductionStub> models = new List<BonusProductionStub>();
            List<SortQuery> sorts = new List<SortQuery>
            {
                new SortQuery(Field.Name, SortOrder.Ascending)
            };

            FilterQuery filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, areaId);
            filter.AddFilter(Field.Year, FilterOperator.Equals, year);
            filter.AddFilter(Field.Month, FilterOperator.GreaterThanOrEquals, startMonth);
            filter.AddFilter(Field.Month, FilterOperator.LessThanOrEquals, endMonth);
            filter.AddFilter(Field.Material, FilterOperator.Equals, Material.ELECTRIC_POWER.ToString());

            FilterQuery filter1 = new FilterQuery(Field.AreaId, FilterOperator.Equals, areaId);
            filter1.AddFilter(Field.Material, FilterOperator.Equals, Material.ELECTRIC_POWER.ToString());


            //algorithm
            if (areaId == 0)
            {
                if (filter.Filters.Any())
                {
                    if (filter.Filters.Exists(f => f.Field == Field.AreaId))
                    {
                        filter.RemoveFilter(Field.AreaId);
                    }
                }

                if (filter1.Filters.Any())
                {
                    if (filter1.Filters.Exists(f => f.Field == Field.AreaId))
                    {
                        filter1.RemoveFilter(Field.AreaId);
                    }
                }
            }

            taskSalesOrders = RepoViewInvoicingSalesOrder.FindAllAsync(sorts, filter);
            taskPlants = RepoPowerPlant.FindAllAsync(sorts, filter1);

            await Task.WhenAll(taskSalesOrders, taskPlants);

            salesOrders = taskSalesOrders.Result;
            plants = taskPlants.Result;

            models = BonusProductionStub.MapList(plants, salesOrders, year, startMonth, endMonth);
            return JsonConvert.SerializeObject(models);
        }

        public async Task<string> BindingBonusProductionSummary(int areaId, int year, int startMonth, int endMonth)
        {
            //lib
            List<ViewInvoicingSalesOrder> salesOrders;
            Task<List<ViewInvoicingSalesOrder>> taskSalesOrders = null;

            FilterQuery filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, areaId);
            filter.AddFilter(Field.Year, FilterOperator.Equals, year);
            filter.AddFilter(Field.Month, FilterOperator.GreaterThanOrEquals, startMonth);
            filter.AddFilter(Field.Month, FilterOperator.LessThanOrEquals, endMonth);

            //algorithm
            if (areaId == 0)
            {
                if (filter.Filters.Any())
                {
                    if (filter.Filters.Exists(f => f.Field == Field.AreaId))
                    {
                        filter.RemoveFilter(Field.AreaId);
                    }
                }
            }

            taskSalesOrders = RepoViewInvoicingSalesOrder.FindAllAsync(filter);

            await Task.WhenAll(taskSalesOrders);

            salesOrders = taskSalesOrders.Result;

            BonusProductionSummaryStub model = new BonusProductionSummaryStub
            {
                SteamRevenue = salesOrders.Where(m => m.Material == Material.GEOTHERMAL_STEAM.ToString()).Sum(m => m.NetValue),
                ElectricRevenue = salesOrders.Where(m => m.Material == Material.ELECTRIC_POWER.ToString()).Sum(m => m.NetValue),
                SteamBonusValue = (salesOrders.Where(m => m.Material == Material.GEOTHERMAL_STEAM.ToString()).Sum(m => m.NetValue) * 0.01),
                ElectricBonusValue = (salesOrders.Where(m => m.Material == Material.ELECTRIC_POWER.ToString()).Sum(m => m.NetValue) * 0.005)
            };

            return JsonConvert.SerializeObject(model);
        }

        public async Task<string> BindingKpi(int areaId, int year, int startMonth, int endMonth)
        {
            //lib
            List<Business.Entities.PowerPlant> plants;
            List<ViewInvoicingKpi> kpis;
            Task<List<ViewInvoicingKpi>> tasKpis = null;
            Task<List<Business.Entities.PowerPlant>> taskPlants = null;
            List<KpiStub> models = new List<KpiStub>();

            List<SortQuery> sorts = new List<SortQuery>
            {
                new SortQuery(Field.Name, SortOrder.Ascending)
            };

            FilterQuery filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, areaId);
            filter.AddFilter(Field.Year, FilterOperator.Equals, year);
            filter.AddFilter(Field.Month, FilterOperator.GreaterThanOrEquals, startMonth);
            filter.AddFilter(Field.Month, FilterOperator.LessThanOrEquals, endMonth);

            FilterQuery filter1 = new FilterQuery(Field.AreaId, FilterOperator.Equals, areaId);

            //algorithm
            if (areaId == 0)
            {
                if (filter.Filters.Any())
                {
                    if (filter.Filters.Exists(f => f.Field == Field.AreaId))
                    {
                        filter.RemoveFilter(Field.AreaId);
                    }
                }

                if (filter1.Filters.Any())
                {
                    if (filter1.Filters.Exists(f => f.Field == Field.AreaId))
                    {
                        filter1.RemoveFilter(Field.AreaId);
                    }
                }
            }

            tasKpis = RepoViewInvoicingKpi.FindAllAsync(sorts, filter);
            taskPlants = RepoPowerPlant.FindAllAsync(sorts, filter1);

            await Task.WhenAll(tasKpis, taskPlants);

            kpis = tasKpis.Result;
            plants = taskPlants.Result;

            models = KpiStub.MapList(plants, kpis, year, startMonth, endMonth);

            return JsonConvert.SerializeObject(models);
        }

        public async Task<string> BindingKpiTransmition(int areaId, int year, int startMonth, int endMonth)
        {
            //lib
            List<Business.Entities.PowerPlant> plants;
            List<ViewInvoicingKpi> kpis;
            Task<List<ViewInvoicingKpi>> tasKpis = null;
            Task<List<Business.Entities.PowerPlant>> taskPlants = null;
            List<KpiStub> models = new List<KpiStub>();

            List<SortQuery> sorts = new List<SortQuery>
            {
                new SortQuery(Field.Name, SortOrder.Ascending)
            };

            FilterQuery filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, areaId);
            filter.AddFilter(Field.Year, FilterOperator.Equals, year);
            filter.AddFilter(Field.Month, FilterOperator.GreaterThanOrEquals, startMonth);
            filter.AddFilter(Field.Month, FilterOperator.LessThanOrEquals, endMonth);
            filter.AddFilter(Field.TransmitionValue, FilterOperator.Greater, 0);

            FilterQuery filter1 = new FilterQuery(Field.AreaId, FilterOperator.Equals, areaId);

            //algorithm
            if (areaId == 0)
            {
                if (filter.Filters.Any())
                {
                    if (filter.Filters.Exists(f => f.Field == Field.AreaId))
                    {
                        filter.RemoveFilter(Field.AreaId);
                    }
                }

                if (filter1.Filters.Any())
                {
                    if (filter1.Filters.Exists(f => f.Field == Field.AreaId))
                    {
                        filter1.RemoveFilter(Field.AreaId);
                    }
                }
            }

            tasKpis = RepoViewInvoicingKpi.FindAllAsync(sorts, filter);
            taskPlants = RepoPowerPlant.FindAllAsync(sorts, filter1);

            await Task.WhenAll(tasKpis, taskPlants);

            kpis = tasKpis.Result;
            plants = taskPlants.Result;

            models = KpiStub.MapList(plants, kpis, year, startMonth, endMonth);

            return JsonConvert.SerializeObject(models);
        }
        #endregion

        #region Helper

        #endregion
    }

    public class BonusProductionSummaryStub
    {
        public double? SteamRevenue { get; set; }
        public double? ElectricRevenue { get; set; }
        public double? SteamBonusValue { get; set; }
        public double? ElectricBonusValue { get; set; }
    }
}
