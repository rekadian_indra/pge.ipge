﻿using MvcSiteMapProvider;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Infrastructure;
using Business.Abstract;
using WebUI.Controllers;
using WebUI.Areas.Reporting.Models.ReservoirReport;
using WebUI.Models;
using System;
using Common.Enums;
using System.Collections.Generic;
using Business.Entities;
using Business.Infrastructure;
using System.Linq;
using System.Globalization;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using WebUI.Infrastructure.Attribute;

namespace WebUI.Areas.Reporting.Controllers
{
    [Authorize]
    [CheckSessionTimeOut]
    public class ReservoirReportController : BaseController
    {
        private const string KEY = "ExcelFile";
        public ReservoirReportController
        (
            IAreaRepository areaRepository,
            IPowerPlantRepository powerPlantRepository,
            IWellRepository wellRepository,
            IInterfacePointRepository interfacePointRepository,
            IScrubberRepository scrubberRepository,
            IMstSeparatorRepository mstSeparatorRepository,
            IPltpRepository pltpRepository,
            IProductionRepository productionRepository,
            IReInjectionRepository reInjectionRepository,
            IMonitoringRepository monitoringRepository,
            IInterfaceScrubberRepository interfaceScrubberRepository,
            ISeparatorRepository separatorRepository
        )
		{
            RepoArea = areaRepository;
            RepoPowerPlant = powerPlantRepository;
            RepoWell = wellRepository;
            RepoInterfacePoint = interfacePointRepository;
            RepoScrubber = scrubberRepository;
            RepoMasterSeparator = mstSeparatorRepository;
            RepoPltp = pltpRepository;
            RepoProduction = productionRepository;
            RepoReinjection = reInjectionRepository;
            RepoMonitoring = monitoringRepository;
            RepoInterfaceScrubber = interfaceScrubberRepository;
            RepoSeparator = separatorRepository;
        }

        [MvcSiteMapNode(Title = TitleSite.ReservoirReport, Area = AreaSite.Reporting, ParentKey = KeySite.Dashboard, Key = KeySite.IndexReservoirReport)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        public async Task<ReservoirReportPresentationStub> ModelByParam(ReservoirReportParamStub model)
        {
            List<InterfacePoint> masterInterface = await RepoInterfacePoint.FindAllAsync();
            List<Scrubber> masterScrubber = await RepoScrubber.FindAllAsync();
            List<Business.Entities.PowerPlant> masterPowerPlant = await RepoPowerPlant.FindAllAsync();
            List<MstSeparator> masterSeparator = await RepoMasterSeparator.FindAllAsync();
            List<Well> masterWell = await RepoWell.FindAllAsync();
            ReservoirReportPresentationStub modelByParam = new ReservoirReportPresentationStub();
            FilterQuery filterQuery;
            List<InterfaceScrubber> interfaceScrubbers;
            List<Pltp> pltps;
            List<Separator> separators;
            List<ReInjection> reInjections;
            List<Monitoring> monitorings;
            List<Production> productions;
            int rowLength, colCounter, relationCounter;
            ReservoirReportDataPresentationStub dataRow;

            for (DateTime s = model.StartDate; s <= model.EndDate; s = s.AddDays(1))
            {
                modelByParam.DateHeader.Add(s.ToString(DisplayFormat.SqlDateFormat));
            }

            modelByParam.DateColSpan = modelByParam.DateHeader.Count;

            ReportModule reportModule;
            if (Enum.TryParse(model.ReportModule, out reportModule))
            {
                switch (reportModule)
                {
                    case ReportModule.INTERFACE:
                        filterQuery = new FilterQuery();
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.InterfacePoint, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, model.StartDate);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, model.EndDate);
                        FilterQuery filterInterface = new FilterQuery();
                        filterInterface.Logic = FilterLogic.Or;
                        foreach (int interfaceId in model.RelationId)
                        {
                            filterInterface.AddFilter(Field.InterfaceId, FilterOperator.Equals, interfaceId);
                        }
                        filterQuery.Filters.Add(filterInterface);
                        interfaceScrubbers = await RepoInterfaceScrubber.FindAllAsync(filterQuery);
                        rowLength = model.Column.Count * model.RelationId.Count;
                        colCounter = 0;
                        relationCounter = 0;
                        for (int a = 0; a < rowLength; a++)
                        {
                            dataRow = new ReservoirReportDataPresentationStub();
                            dataRow.Column = model.Column[colCounter];
                            if (masterInterface != null && masterInterface.Any() && colCounter == 0)
                            {
                                dataRow.RelationRowSpan = model.Column.Count;
                                InterfacePoint b = masterInterface.FirstOrDefault(i => i.Id == model.RelationId[relationCounter]);
                                if (b != null)
                                {
                                    dataRow.Relation = b.Name;
                                }
                                else
                                {
                                    dataRow.Relation = null;
                                }
                            }
                            else
                            {
                                dataRow.Relation = null;
                            }
                            for (DateTime s = model.StartDate; s <= model.EndDate; s = s.AddDays(1))
                            {
                                if (interfaceScrubbers != null && interfaceScrubbers.Any())
                                {
                                    InterfaceScrubber data = interfaceScrubbers.FirstOrDefault
                                        (
                                            c => c.TaskReport.Periode >= s && c.TaskReport.Periode <= s &&
                                            (c.InterfaceId != null ? c.InterfaceId.Value == model.RelationId[relationCounter] : c.InterfaceId == null)
                                        );
                                    switch (model.Column[colCounter])
                                    {
                                        case "Tekanan":
                                            if (data != null && data.InterfacePressure != null)
                                            {
                                                dataRow.ColumnValue.Add(data.InterfacePressure.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Suhu":
                                            if (data != null && data.InterfaceTemperature != null)
                                            {
                                                dataRow.ColumnValue.Add(data.InterfaceTemperature.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    dataRow.ColumnValue.Add(0);
                                }
                            }

                            modelByParam.DataRow.Add(dataRow);

                            colCounter++;

                            if (colCounter == model.Column.Count)
                            {
                                colCounter = 0;
                                relationCounter++;
                            }
                        }
                        break;
                    case ReportModule.PEL:
                        filterQuery = new FilterQuery();
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.PowerPlant, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, model.StartDate);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, model.EndDate);
                        FilterQuery filterPEL = new FilterQuery();
                        filterPEL.Logic = FilterLogic.Or;
                        foreach (int powerPlantId in model.RelationId)
                        {
                            filterPEL.AddFilter(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
                        }
                        filterQuery.Filters.Add(filterPEL);
                        pltps = await RepoPltp.FindAllAsync(filterQuery);
                        rowLength = model.Column.Count * model.RelationId.Count;
                        colCounter = 0;
                        relationCounter = 0;
                        for (int a = 0; a < rowLength; a++)
                        {
                            dataRow = new ReservoirReportDataPresentationStub();
                            dataRow.Column = model.Column[colCounter] + " (mWh)";
                            if (masterPowerPlant != null && masterPowerPlant.Any() && colCounter == 0)
                            {
                                dataRow.RelationRowSpan = model.Column.Count;
                                Business.Entities.PowerPlant b = masterPowerPlant.FirstOrDefault(i => i.Id == model.RelationId[relationCounter]);
                                if (b != null)
                                {
                                    dataRow.Relation = b.Name;
                                }
                                else
                                {
                                    dataRow.Relation = null;
                                }
                            }
                            else
                            {
                                dataRow.Relation = null;
                            }
                            for (DateTime s = model.StartDate; s <= model.EndDate; s = s.AddDays(1))
                            {
                                if (pltps != null && pltps.Any())
                                {
                                    Pltp data = pltps.FirstOrDefault
                                        (
                                            c => c.TaskReport.Periode >= s && c.TaskReport.Periode <= s &&
                                            (c.PowerPlantId != null ? c.PowerPlantId.Value == model.RelationId[relationCounter] : c.PowerPlantId == null)
                                        );
                                    switch (model.Column[colCounter])
                                    {
                                        case "Gross":
                                            if (data != null && data.EnergyGross != null)
                                            {
                                                dataRow.ColumnValue.Add((data.EnergyGross.Value/1000));
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Nett":
                                            if (data != null && data.EnergyNett != null)
                                            {
                                                dataRow.ColumnValue.Add((data.EnergyNett.Value/1000));
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    dataRow.ColumnValue.Add(0);
                                }
                            }

                            modelByParam.DataRow.Add(dataRow);

                            colCounter++;

                            if (colCounter == model.Column.Count)
                            {
                                colCounter = 0;
                                relationCounter++;
                            }
                        }
                        break;
                    case ReportModule.PU:
                        filterQuery = new FilterQuery();
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.PowerPlant, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, model.StartDate);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, model.EndDate);
                        FilterQuery filterPU = new FilterQuery();
                        filterPU.Logic = FilterLogic.Or;
                        foreach (int powerPlantId in model.RelationId)
                        {
                            filterPU.AddFilter(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
                        }
                        filterQuery.Filters.Add(filterPU);
                        pltps = await RepoPltp.FindAllAsync(filterQuery);
                        rowLength = model.Column.Count * model.RelationId.Count;
                        colCounter = 0;
                        relationCounter = 0;
                        for (int a = 0; a < rowLength; a++)
                        {
                            dataRow = new ReservoirReportDataPresentationStub();
                            dataRow.Column = model.Column[colCounter] + " (ton)";
                            if (masterPowerPlant != null && masterPowerPlant.Any() && colCounter == 0)
                            {
                                dataRow.RelationRowSpan = model.Column.Count;
                                Business.Entities.PowerPlant b = masterPowerPlant.FirstOrDefault(i => i.Id == model.RelationId[relationCounter]);
                                if (b != null)
                                {
                                    dataRow.Relation = b.Name;
                                }
                                else
                                {
                                    dataRow.Relation = null;
                                }
                            }
                            else
                            {
                                dataRow.Relation = null;
                            }
                            for (DateTime s = model.StartDate; s <= model.EndDate; s = s.AddDays(1))
                            {
                                if (pltps != null && pltps.Any())
                                {
                                    Pltp data = pltps.FirstOrDefault
                                        (
                                            c => c.TaskReport.Periode >= s && c.TaskReport.Periode <= s &&
                                            (c.PowerPlantId != null ? c.PowerPlantId.Value == model.RelationId[relationCounter] : c.PowerPlantId == null)
                                        );
                                    switch (model.Column[colCounter])
                                    {
                                        case "Pemanfaatan Uap oleh Pembangkit":
                                            if (data != null && data.SteamUtilization != null)
                                            {
                                                dataRow.ColumnValue.Add(data.SteamUtilization.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    dataRow.ColumnValue.Add(0);
                                }
                            }

                            modelByParam.DataRow.Add(dataRow);

                            colCounter++;

                            if (colCounter == model.Column.Count)
                            {
                                colCounter = 0;
                                relationCounter++;
                            }
                        }
                        break;
                    case ReportModule.SCRUBBER:
                        filterQuery = new FilterQuery();
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.Scrubber, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, model.StartDate);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, model.EndDate);
                        FilterQuery filterScrubber = new FilterQuery();
                        filterScrubber.Logic = FilterLogic.Or;
                        foreach (int scrubberId in model.RelationId)
                        {
                            filterScrubber.AddFilter(Field.ScrubberId, FilterOperator.Equals, scrubberId);
                        }
                        filterQuery.Filters.Add(filterScrubber);
                        interfaceScrubbers = await RepoInterfaceScrubber.FindAllAsync(filterQuery);
                        rowLength = model.Column.Count * model.RelationId.Count;
                        colCounter = 0;
                        relationCounter = 0;
                        for (int a = 0; a < rowLength; a++)
                        {
                            dataRow = new ReservoirReportDataPresentationStub();
                            dataRow.Column = model.Column[colCounter];
                            if (masterScrubber != null && masterScrubber.Any() && colCounter == 0)
                            {
                                dataRow.RelationRowSpan = model.Column.Count;
                                Scrubber b = masterScrubber.FirstOrDefault(i => i.Id == model.RelationId[relationCounter]);
                                if (b != null)
                                {
                                    dataRow.Relation = b.Name;
                                }
                                else
                                {
                                    dataRow.Relation = null;
                                }
                            }
                            else
                            {
                                dataRow.Relation = null;
                            }
                            for (DateTime s = model.StartDate; s <= model.EndDate; s = s.AddDays(1))
                            {
                                if (interfaceScrubbers != null && interfaceScrubbers.Any())
                                {
                                    InterfaceScrubber data = interfaceScrubbers.FirstOrDefault
                                        (
                                            c => c.TaskReport.Periode >= s && c.TaskReport.Periode <= s &&
                                            (c.ScrubberId != null ? c.ScrubberId.Value == model.RelationId[relationCounter] : c.ScrubberId == null)
                                        );
                                    switch (model.Column[colCounter])
                                    {
                                        case "Tekanan":
                                            if (data != null && data.ScrubberPressure != null)
                                            {
                                                dataRow.ColumnValue.Add(data.ScrubberPressure.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Suhu":
                                            if (data != null && data.ScrubberTemperature != null)
                                            {
                                                dataRow.ColumnValue.Add(data.ScrubberTemperature.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    dataRow.ColumnValue.Add(0);
                                }
                            }

                            modelByParam.DataRow.Add(dataRow);

                            colCounter++;

                            if (colCounter == model.Column.Count)
                            {
                                colCounter = 0;
                                relationCounter++;
                            }
                        }
                        break;
                    case ReportModule.SEPARATOR:
                        filterQuery = new FilterQuery();
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.MstSeparator, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, model.StartDate);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, model.EndDate);
                        FilterQuery filterSeparator = new FilterQuery();
                        filterSeparator.Logic = FilterLogic.Or;
                        foreach (int separatorId in model.RelationId)
                        {
                            filterSeparator.AddFilter(Field.SeparatorId, FilterOperator.Equals, separatorId);
                        }
                        filterQuery.Filters.Add(filterSeparator);
                        separators = await RepoSeparator.FindAllAsync(filterQuery);
                        rowLength = model.Column.Count * model.RelationId.Count;
                        colCounter = 0;
                        relationCounter = 0;
                        for (int a = 0; a < rowLength; a++)
                        {
                            dataRow = new ReservoirReportDataPresentationStub();
                            dataRow.Column = model.Column[colCounter];
                            if (masterSeparator != null && masterSeparator.Any() && colCounter == 0)
                            {
                                dataRow.RelationRowSpan = model.Column.Count;
                                MstSeparator b = masterSeparator.FirstOrDefault(i => i.Id == model.RelationId[relationCounter]);
                                if (b != null)
                                {
                                    dataRow.Relation = b.Name;
                                }
                                else
                                {
                                    dataRow.Relation = null;
                                }
                            }
                            else
                            {
                                dataRow.Relation = null;
                            }
                            for (DateTime s = model.StartDate; s <= model.EndDate; s = s.AddDays(1))
                            {
                                if (separators != null && separators.Any())
                                {
                                    Separator data = separators.FirstOrDefault
                                        (
                                            c => c.TaskReport.Periode >= s && c.TaskReport.Periode <= s &&
                                            (c.SeparatorId != null ? c.SeparatorId.Value == model.RelationId[relationCounter] : c.SeparatorId == null)
                                        );
                                    switch (model.Column[colCounter])
                                    {
                                        case "Tekanan":
                                            if (data != null && data.Pressure != null)
                                            {
                                                dataRow.ColumnValue.Add(data.Pressure.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Suhu":
                                            if (data != null && data.Temperature != null)
                                            {
                                                dataRow.ColumnValue.Add(data.Temperature.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Level Brine":
                                            if (data != null && data.LevelBrine != null)
                                            {
                                                dataRow.ColumnValue.Add(data.LevelBrine.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Throttle Valve Opening Brine":
                                            if (data != null && data.ThrottleValveBrine != null)
                                            {
                                                dataRow.ColumnValue.Add(data.ThrottleValveBrine.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Throttle Valve Opening Stream":
                                            if (data != null && data.ThrottleValveSteam != null)
                                            {
                                                dataRow.ColumnValue.Add(data.ThrottleValveSteam.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Steam Flow Rata-Rata":
                                            if (data != null && data.SteamFlowRate != null)
                                            {
                                                dataRow.ColumnValue.Add(data.SteamFlowRate.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Brine Flow Rata-Rata":
                                            if (data != null && data.BrineFlowRate != null)
                                            {
                                                dataRow.ColumnValue.Add(data.BrineFlowRate.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    dataRow.ColumnValue.Add(0);
                                }
                            }

                            modelByParam.DataRow.Add(dataRow);

                            colCounter++;

                            if (colCounter == model.Column.Count)
                            {
                                colCounter = 0;
                                relationCounter++;
                            }
                        }
                        break;
                    case ReportModule.SI:
                        filterQuery = new FilterQuery();
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.Well, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, model.StartDate);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, model.EndDate);
                        FilterQuery filterSI = new FilterQuery();
                        filterSI.Logic = FilterLogic.Or;
                        foreach (int wellId in model.RelationId)
                        {
                            filterSI.AddFilter(Field.WellId, FilterOperator.Equals, wellId);
                        }
                        filterQuery.Filters.Add(filterSI);
                        reInjections = await RepoReinjection.FindAllAsync(filterQuery);
                        rowLength = model.Column.Count * model.RelationId.Count;
                        colCounter = 0;
                        relationCounter = 0;
                        for (int a = 0; a < rowLength; a++)
                        {
                            dataRow = new ReservoirReportDataPresentationStub();
                            dataRow.Column = model.Column[colCounter];
                            if (masterWell != null && masterWell.Any() && colCounter == 0)
                            {
                                dataRow.RelationRowSpan = model.Column.Count;
                                Well b = masterWell.FirstOrDefault(i => i.Id == model.RelationId[relationCounter]);
                                if (b != null)
                                {
                                    dataRow.Relation = b.Description;
                                }
                                else
                                {
                                    dataRow.Relation = null;
                                }
                            }
                            else
                            {
                                dataRow.Relation = null;
                            }
                            for (DateTime s = model.StartDate; s <= model.EndDate; s = s.AddDays(1))
                            {
                                if (reInjections != null && reInjections.Any())
                                {
                                    ReInjection data = reInjections.FirstOrDefault
                                        (
                                            c => c.TaskReport.Periode >= s && c.TaskReport.Periode <= s &&
                                            (c.WellId != null ? c.WellId.Value == model.RelationId[relationCounter] : c.WellId == null)
                                        );
                                    switch (model.Column[colCounter])
                                    {
                                        case "TKS":
                                            if (data != null && data.Tks != null)
                                            {
                                                dataRow.ColumnValue.Add(data.Tks.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Tekanan Jalur":
                                            if (data != null && data.PressureLine != null)
                                            {
                                                dataRow.ColumnValue.Add(data.PressureLine.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Suhu Jalur":
                                            if (data != null && data.Temperature != null)
                                            {
                                                dataRow.ColumnValue.Add(data.Temperature.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Ph":
                                            if (data != null && data.Ph != null)
                                            {
                                                dataRow.ColumnValue.Add(data.Ph.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Flow Rata-rata":
                                            if (data != null && data.FlowRate != null)
                                            {
                                                dataRow.ColumnValue.Add(data.FlowRate.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Flow Total 24 Jam":
                                            if (data != null && data.FlowTotal != null)
                                            {
                                                dataRow.ColumnValue.Add(data.FlowTotal.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    dataRow.ColumnValue.Add(0);
                                }
                            }

                            modelByParam.DataRow.Add(dataRow);

                            colCounter++;

                            if (colCounter == model.Column.Count)
                            {
                                colCounter = 0;
                                relationCounter++;
                            }
                        }
                        break;
                    case ReportModule.SMA:
                        filterQuery = new FilterQuery();
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.Well, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, model.StartDate);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, model.EndDate);
                        FilterQuery filterSMA = new FilterQuery();
                        filterSMA.Logic = FilterLogic.Or;
                        foreach (int wellId in model.RelationId)
                        {
                            filterSMA.AddFilter(Field.WellId, FilterOperator.Equals, wellId);
                        }
                        filterQuery.Filters.Add(filterSMA);
                        monitorings = await RepoMonitoring.FindAllAsync(filterQuery);
                        rowLength = model.Column.Count * model.RelationId.Count;
                        colCounter = 0;
                        relationCounter = 0;
                        for (int a = 0; a < rowLength; a++)
                        {
                            dataRow = new ReservoirReportDataPresentationStub();
                            dataRow.Column = model.Column[colCounter];
                            if (masterWell != null && masterWell.Any() && colCounter == 0)
                            {
                                dataRow.RelationRowSpan = model.Column.Count;
                                Well b = masterWell.FirstOrDefault(i => i.Id == model.RelationId[relationCounter]);
                                if (b != null)
                                {
                                    dataRow.Relation = b.Description;
                                }
                                else
                                {
                                    dataRow.Relation = null;
                                }
                            }
                            else
                            {
                                dataRow.Relation = null;
                            }
                            for (DateTime s = model.StartDate; s <= model.EndDate; s = s.AddDays(1))
                            {
                                if (monitorings != null && monitorings.Any())
                                {
                                    Monitoring data = monitorings.FirstOrDefault
                                        (
                                            c => c.TaskReport.Periode >= s && c.TaskReport.Periode <= s &&
                                            (c.WellId != null ? c.WellId.Value == model.RelationId[relationCounter] : c.WellId == null)
                                        );
                                    switch (model.Column[colCounter])
                                    {
                                        case "TKS":
                                            if (data != null && data.Tks != null)
                                            {
                                                dataRow.ColumnValue.Add(data.Tks.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    dataRow.ColumnValue.Add(0);
                                }
                            }

                            modelByParam.DataRow.Add(dataRow);

                            colCounter++;

                            if (colCounter == model.Column.Count)
                            {
                                colCounter = 0;
                                relationCounter++;
                            }
                        }
                        break;
                    case ReportModule.SP:
                        filterQuery = new FilterQuery();
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.Well, Field.AreaId), FilterOperator.Equals, model.AreaId);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.GreaterThanOrEquals, model.StartDate);
                        filterQuery.AddFilter(new FieldHelper(Table.TaskReport, Field.Periode), FilterOperator.LessThanOrEquals, model.EndDate);
                        FilterQuery filterSP = new FilterQuery();
                        filterSP.Logic = FilterLogic.Or;
                        foreach (int wellId in model.RelationId)
                        {
                            filterSP.AddFilter(Field.WellId, FilterOperator.Equals, wellId);
                        }
                        filterQuery.Filters.Add(filterSP);
                        productions = await RepoProduction.FindAllAsync(filterQuery);
                        rowLength = model.Column.Count * model.RelationId.Count;
                        colCounter = 0;
                        relationCounter = 0;
                        for (int a = 0; a < rowLength; a++)
                        {
                            dataRow = new ReservoirReportDataPresentationStub();
                            dataRow.Column = model.Column[colCounter];
                            if (masterWell != null && masterWell.Any() && colCounter == 0)
                            {
                                dataRow.RelationRowSpan = model.Column.Count;
                                Well b = masterWell.FirstOrDefault(i => i.Id == model.RelationId[relationCounter]);
                                if (b != null)
                                {
                                    dataRow.Relation = b.Description;
                                }
                                else
                                {
                                    dataRow.Relation = null;
                                }
                            }
                            else
                            {
                                dataRow.Relation = null;
                            }
                            for (DateTime s = model.StartDate; s <= model.EndDate; s = s.AddDays(1))
                            {
                                if (productions != null && productions.Any())
                                {
                                    Production data = productions.FirstOrDefault
                                        (
                                            c => c.TaskReport.Periode >= s && c.TaskReport.Periode <= s &&
                                            (c.WellId != null ? c.WellId.Value == model.RelationId[relationCounter] : c.WellId == null)
                                        );
                                    switch (model.Column[colCounter])
                                    {
                                        case "TKS":
                                            if (data != null && data.Tks != null)
                                            {
                                                dataRow.ColumnValue.Add(data.Tks.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Tekanan Jalur":
                                            if (data != null && data.PressureLine != null)
                                            {
                                                dataRow.ColumnValue.Add(data.PressureLine.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Suhu Jalur":
                                            if (data != null && data.Temperature != null)
                                            {
                                                dataRow.ColumnValue.Add(data.Temperature.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Throttle Valve Opening":
                                            if (data != null && data.ThrottleValve != null)
                                            {
                                                dataRow.ColumnValue.Add(data.ThrottleValve.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Flow Rata-rata":
                                            if (data != null && data.FlowRate != null)
                                            {
                                                dataRow.ColumnValue.Add(data.FlowRate.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Flow Total 24 Jam":
                                            if (data != null && data.FlowTotal != null)
                                            {
                                                dataRow.ColumnValue.Add(data.FlowTotal.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Pressure Upstream":
                                            if (data != null && data.PressureUpstream != null)
                                            {
                                                dataRow.ColumnValue.Add(data.PressureUpstream.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                        case "Differential Pressure":
                                            if (data != null && data.DifferentialPressure != null)
                                            {
                                                dataRow.ColumnValue.Add(data.DifferentialPressure.Value);
                                            }
                                            else
                                            {
                                                dataRow.ColumnValue.Add(0);
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    dataRow.ColumnValue.Add(0);
                                }
                            }

                            modelByParam.DataRow.Add(dataRow);

                            colCounter++;

                            if (colCounter == model.Column.Count)
                            {
                                colCounter = 0;
                                relationCounter++;
                            }
                        }
                        break;
                }
            }

            return modelByParam;
        }

        [HttpPost]
        public async Task<ActionResult> GetDataReport(ReservoirReportParamStub model)
        {
            ReservoirReportPresentationStub reservoirReportPresentationStub = await ModelByParam(model);

            return Json(reservoirReportPresentationStub);
        }

        [HttpPost]
        public async Task<ActionResult> ExcelReport(ReservoirReportParamStub model)
        {
            ResponseModel responseModel = new ResponseModel(true);

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
            DownloadParameterStub downloadParameterStub = new DownloadParameterStub();

            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet;
            XSSFRow row;
            XSSFCell cell;
            XSSFCellStyle boldTextLeftMidAlign, boldTextCenter, textLeft, textLeftMidAlign, textRight, dateStyle;
            XSSFCreationHelper helper;
            XSSFDrawing drawing;
            XSSFClientAnchor anchor;
            XSSFPicture pict;
            byte[] data;
            int picInd;
            XSSFDataFormat dataFormat = (XSSFDataFormat)workbook.CreateDataFormat();
            XSSFFont boldFont = (XSSFFont)workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            boldTextLeftMidAlign = (XSSFCellStyle)workbook.CreateCellStyle();
            boldTextLeftMidAlign.SetFont(boldFont);
            boldTextLeftMidAlign.VerticalAlignment = VerticalAlignment.Center;
            boldTextLeftMidAlign.Alignment = HorizontalAlignment.Left;
            boldTextLeftMidAlign.FillForegroundColor = IndexedColors.Yellow.Index;
            boldTextLeftMidAlign.FillPattern = FillPattern.SolidForeground;

            boldTextCenter = (XSSFCellStyle)workbook.CreateCellStyle();
            boldTextCenter.SetFont(boldFont);
            boldTextCenter.Alignment = HorizontalAlignment.Center;
            boldTextCenter.FillForegroundColor = IndexedColors.Yellow.Index;
            boldTextCenter.FillPattern = FillPattern.SolidForeground;

            textRight = (XSSFCellStyle)workbook.CreateCellStyle();
            textRight.Alignment = HorizontalAlignment.Right;

            textLeftMidAlign = (XSSFCellStyle)workbook.CreateCellStyle();
            textLeftMidAlign.VerticalAlignment = VerticalAlignment.Center;
            textLeftMidAlign.Alignment = HorizontalAlignment.Left;

            textLeft = (XSSFCellStyle)workbook.CreateCellStyle();
            textLeft.Alignment = HorizontalAlignment.Left;

            dateStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            dateStyle.DataFormat = dataFormat.GetFormat(DisplayFormat.SqlDateFormat);
            dateStyle.SetFont(boldFont);
            dateStyle.Alignment = HorizontalAlignment.Center;
            dateStyle.FillForegroundColor = IndexedColors.Yellow.Index;
            dateStyle.FillPattern = FillPattern.SolidForeground;

            ReservoirReportPresentationStub reservoirReportPresentationStub = await ModelByParam(model);
            
            ReportModule reportModule;
            if (Enum.TryParse(model.ReportModule, out reportModule))
            {
                string relation = "";
                switch (reportModule)
                {
                    case ReportModule.INTERFACE:
                        relation = "Interface";
                        break;
                    case ReportModule.PEL:
                    case ReportModule.PU:
                        relation = "Power Plant";
                        break;
                    case ReportModule.SCRUBBER:
                        relation = "Scrubber";
                        break;
                    case ReportModule.SEPARATOR:
                        relation = "Separator";
                        break;
                    case ReportModule.SI:
                    case ReportModule.SP:
                    case ReportModule.SMA:
                        relation = "Well";
                        break;
                }

                sheet = (XSSFSheet)workbook.CreateSheet(reportModule.ToDescription());

                sheet.AddMergedRegion(new CellRangeAddress(0,1,0,0));
                sheet.SetColumnWidth(0, 10000);

                sheet.AddMergedRegion(new CellRangeAddress(0, 1, 1, 1));
                sheet.SetColumnWidth(1, 7000);

                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 2, (2 + (reservoirReportPresentationStub.DateColSpan-1))));

                row = (XSSFRow)sheet.CreateRow(0);

                cell = (XSSFCell)row.CreateCell(0);
                cell.SetCellValue(relation);
                cell.CellStyle = boldTextLeftMidAlign;
                cell = (XSSFCell)row.CreateCell(1);
                cell.SetCellValue("Kolom");
                cell.CellStyle = boldTextLeftMidAlign;
                cell = (XSSFCell)row.CreateCell(2);
                cell.SetCellValue("Tanggal");
                cell.CellStyle = boldTextCenter;

                row = (XSSFRow)sheet.CreateRow(1);
                int colIndex = 2;
                for (DateTime s = model.StartDate; s <= model.EndDate; s = s.AddDays(1))
                {
                    sheet.SetColumnWidth(colIndex, 3000);
                    cell = (XSSFCell)row.CreateCell(colIndex);
                    cell.SetCellValue(s);
                    cell.CellStyle = dateStyle;
                    colIndex++;
                }

                int rowIndex = 2;
                foreach(ReservoirReportDataPresentationStub dataRow in reservoirReportPresentationStub.DataRow)
                {
                    row = (XSSFRow)sheet.CreateRow(rowIndex);
                    if (dataRow.Relation != null)
                    {
                        sheet.AddMergedRegion(new CellRangeAddress(rowIndex, (rowIndex + (dataRow.RelationRowSpan.Value - 1)), 0, 0));
                        cell = (XSSFCell)row.CreateCell(0);
                        cell.SetCellValue(dataRow.Relation);
                        cell.CellStyle = textLeftMidAlign;
                    }
                    cell = (XSSFCell)row.CreateCell(1);
                    cell.SetCellValue(dataRow.Column);
                    cell.CellStyle = textLeft;
                    colIndex = 2;
                    foreach(double v in dataRow.ColumnValue)
                    {
                        cell = (XSSFCell)row.CreateCell(colIndex);
                        cell.SetCellValue(v);
                        cell.CellStyle = textRight;
                        colIndex++;
                    }
                    rowIndex++;
                }

                rowIndex = rowIndex + 5;
                string base64StringImage = Regex.Match(model.ChartImage, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                data = Convert.FromBase64String(base64StringImage);
                picInd = workbook.AddPicture(data, PictureType.PNG);
                helper = workbook.GetCreationHelper() as XSSFCreationHelper;
                drawing = sheet.CreateDrawingPatriarch() as XSSFDrawing;
                anchor = helper.CreateClientAnchor() as XSSFClientAnchor;
                anchor.Col1 = 0;
                anchor.Row1 = rowIndex;
                pict = drawing.CreatePicture(anchor, picInd) as XSSFPicture;
                pict.Resize(10.0, 40.0);

                string fileDownloadName = "Reservoir Report - " + reportModule.ToDescription() + ".xlsx";
                string contentType = MimeMapping.GetMimeMapping(fileDownloadName);
                MemoryStream memoryStream = new MemoryStream();
                workbook.Write(memoryStream);

                downloadParameterStub = new DownloadParameterStub(memoryStream,contentType,fileDownloadName);

                TempData[KEY] = downloadParameterStub;
            }

            return Json(responseModel);
        }

        public ActionResult Download()
        {
            if (TempData[KEY] != null)
            {
                DownloadParameterStub param = TempData[KEY] as DownloadParameterStub;

                return File(param.MemoryStream.ToArray(), param.ContentType, param.FileDownloadName);
            }

            return new EmptyResult();
        }
    }
}
