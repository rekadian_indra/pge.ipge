﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //Enable attribute routing
            //routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Executive", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "MyTaskDetail",
                url: "{area}/{controller}/{action}/{id}/{tab}",
                defaults: new { area = "OperationData", controller = "InputData", action = "Detail",
                    id = UrlParameter.Optional, tab = UrlParameter.Optional }
            );

            routes.MapRoute("NotFound", "{*url}", new { controller = "Error", action = "Http404" });
        }
    }
}