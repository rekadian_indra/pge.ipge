﻿using System.ComponentModel;

namespace Common.Enums
{
    public enum FilterOperator
    {
        [Description("eq")]
        Equals,
        [Description("neq")]
        NotEquals,
        [Description("gt")]
        Greater,
        [Description("gte")]
        GreaterThanOrEquals,
        [Description("lt")]
        LessThan,
        [Description("lte")]
        LessThanOrEquals,
        [Description("startswith")]
        StartsWith,
        [Description("endswith")]
        EndsWith,
        [Description("contains")]
        Contains,
        [Description("doesnotcontain")]
        NotContains,
        [Description("isnull")]
        IsNull,
        [Description("isnotnull")]
        IsNotNull
    }

    public enum FilterLogic
    {
        [Description("and")]
        And,
        [Description("or")]
        Or
    }

    public enum SortOrder
    {
        [Description("asc")]
        Ascending,
        [Description("desc")]
        Descending
    }
}
