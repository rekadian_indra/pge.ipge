﻿using System.ComponentModel;

namespace Common.Enums
{
    public enum UserModule
    {
        [Description("User Management")]
        UserManagement,
        [Description("Data Master")]
        DataMaster,
        [Description("Crew")]
        Crew,
        [Description("Whitelist")]
        Whitelist,
    }

    public enum UserRole
    {
       
        [Description("Administrator")]
        Administrator,
        [Description("Superadmin")]
        Superadmin,

        [Description("Operation Admin")]
        OPERATION_ADMIN,
        [Description("Operation Operator")]
        OPERATION_OPERATOR,
        [Description("Operation Supervisor")]
        OPERATION_SUPERVISOR,

        [Description("Analis Reservoir")]
        RESERVOIR,
        [Description("Manager Operation Area")]
        MANAGER_AREA,
        [Description("Asmen Operation Manager Area")]
        ASMEN,
        [Description("Finance Area")]
        FINANCE_AREA,
        [Description("Finance Controller")]
        FINANCE_CONTROLLER,
        [Description("Finance Treasury")]
        FINANCE_TREASURY,
        [Description("OE Verification")]
        OE_VERIFICATION,
        [Description("Manager OE")]
        MANAGER_OE,
        [Description("Partnership Manager Verification")]
        PARTNERSHIP,
        [Description("Asmen Partnership")]
        ASMEN_PARTNERSHIP,
        [Description("Management")]
        MANAGEMENT
    }
}
