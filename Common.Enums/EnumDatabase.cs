﻿namespace Common.Enums
{
    public enum Field
    {
        //reuseable field
        IsDeleted,
        CreatedBy,
        CreatedDateTime,
        CreatedDateTimeUtc,
        ModifiedBy,
        ModifiedDateTime,
        ModifiedDateTimeUtc,
        Text,
        Value,
        StartDate,
        EndDate,
        HasApproved,
        ApprovedBy,
        HasRelation,
        CanEdit,

        //user management field
        ApplicationName,
        ApplicationId,
        UserId,
        UserName,
		EmployeeNo,
        IsAnonymous,
        LastActivityDate,
        RoleId,
        RoleName,
        Description,
        PropertyNames,
        PropertyValueStrings,
        PropertyValueBinary,
        LastUpdatedDate,
        ModuleId,
        ModuleName,
        ParentModule,
        ActionId,
        ActionName,
        Password,
        PasswordFormat,
        PasswordSalt,
        Email,
        PasswordQuestion,
        PasswordAnswer,
        IsApproved,
        IsLockedOut,
        CreateDate,
        LastLoginDate,
        LastPasswordChangedDate,
        LastLockoutDate,
        FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowsStart,
        Comment,

        //app field
        CrewId,
        CrewName,
        Status,
        RegisterDate,
        CompanyId,
        CompanyName,
        CompanyAddress,
        AirportId,
        AirportName,
        AirportAddress,
        WhitelistId,
        Source,

        //Sales Order
        SOType,
		Quantity,
		NetPrice,
		NetValue,
		Currency,
		Notes,
		WorkflowId,

		Unit,
		Period,


		DocDate,
        ConfirmQty,
        Ton,
        StartHour,
        EndHour,

		//Bast File
		SalesOrderId,
		FilePath,

        //Task Report
        AreaId,
        Periode,
        SourceInput,
        Name,
        PowerPlantName,
        Id,
        TaskPeriode,
        TaskStatus,

        //Pltp
        TaskReportId,
        PowerPlanId,
        PowerPlantId,
        EnergyGross,
        EnergyNett,
        SteamUtilization,
        WasteRockMufller,
        GppsShutdown,
        ImportEnergy,
        SgssShutdown,
        Ssc,



        //dkp                            
        Derating,
        Location,
        Cause,

        WellId,
        WellStatus,
        WellName,
        HistoryDateTimeUtc, 

        SeparatorId,
        InterfacePointId,
        ScrubberId,

        Planning,
        Actual,
        ActualCase,
        Tks,
        PressureLine,
        Temperature,
        ThrottleValve,
        FlowRate,
        FlowTotal,
        PressureUpstream,
        DifferentialPressure,
        Ph,
        Fluida,
        Pressure,
        LevelBrine,
        ThrottleValveBrine,
        ThrottleValveSteam,
        SteamFlowRate,
        BrineFlowRate,
        InterfaceId,
        ScrubberName,
        InterfacePressure,
        InterfaceTemperature,
        ScrubberPressure,
        ScrubberTemperature,

        ExplorationId,
        ExplorationDate,
        AreaName,
        Mku,
        Mkt,
        Mdpl,
        Sds,
        Tds,

        EnergyGrossUnit,
        EnergyNettUnit,
        SteamUtilizationUnit,
        WasteRockMufllerUnit,
        SscUnit,
        GppsShutdownUnit,
        ImportEnergyUnit,
        SgssShutdownUnit,

        TksUnit,
        PressureLineUnit,
        TemperatureUnit,
        ThrottleValveUnit,
        FlowRateUnit,
        FlowTotalUnit,
        PressureUpstreamUnit,
        DifferentialPressureUnit,

        PressureUnit,
        LevelBrineUnit,
        ThrottleValveBrineUnit,
        ThrottleValveSteamUnit,
        SteamFlowRateUnit,
        BrineFlowRateUnit,

        InterfacePressureUnit,
        InterfaceTemperatureUnit,
        ScrubberPressureUnit,
        ScrubberTemperatureUnit,

        TargetWellHistory,

        //workflow
        WorkflowType,
        Code,
        PreviousStep,
        Approver,

		//powerplant
		InvoiceGroup,

        //sales order
        ShipToParty,
        ShipToPartyId,
        Material,
        SoldToPartyId,
        ShipToPartyName,

        //ppi (producer price index)
        Year,
		QuarterNum,
		ValueMonth1,
		ValueMonth2,
		ValueMonth3,
		InflationIndex,
		NextPrice,
		ValueMonth1Add,
		ValueMonth2Add,
		ValueMonth3Add,
		InflationIndexAdd,
		NextPriceAdd,
		StrQuarterNum,
		StrMonth1,
		StrMonth2,
		StrMonth3,
		StrWorkflow,
		FormSequence,

		//steam power plant price
		IncreasePercentage,
		Price,
		IncreasePercentageAdd,
		PriceAdd,

		//other
		RangeData,

        //production plan
        UtilType,
        URC,
        AgreementName,
        AgreementDate,
        MeterName,
        MeterType,
        BasePrice,
        BasePricePPI,
        TransmitionPrice,
        TransmitionNetValue,

        // maintain harga approval
        SteamPowerPlantPriceId,
		PpiId,

		// invoice approval
		InvoiceId,
		InvoiceNo,
		InvoiceSequence,
		SignatureName,
		SignaturePosition,
        AdditionalText,

        // invoice files
        InvoiceFiles,
		EFakturFiles,

        //Invoice
        InvoiceAcceptDate,

        //cluster
        ClusterName,
        ClusterId,


        InvoiceArea,
        PlanDetailArea,
        UserArea,
        WorkflowName,
        WorkflowCode,
        Month,
        NoPo,
        NoPr,
        NoSo,

        //AutoNumber
        TransactionCode,
        
        StatusDesc,
        StartMonth,
        EndMonth,
        Capacity,
        Productivity,
        InvoiceOrderMonth,
        InvoiceUnit,
        SalesOrderMonth,
        TransmitionValue,
        ProductionActual,
        ProductionPercent
    }

    public enum Table
    {
        //user management table
        Action,
        Application,
        Membership,
        Module,
        Profile,
        Role,
        User,

        //app table
        Cluster,
        Crew,
        Company,
        Airport,
        WhiteList,
        TaskReport,
        Area,
        MstSeparator,
        Scrubber,
        Well,
        PowerPlant,
        InterfacePoint,
        ProductionPlan,
        ProductionPlanDetail,
        PPI,
        Workflow,
        WellHistory,
        SalesOrder
    }
}
