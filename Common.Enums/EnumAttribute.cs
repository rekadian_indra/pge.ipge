﻿namespace Common.Enums
{
    public enum MatchMode : int
    {
        All,
        Any,
        None
    }
}
