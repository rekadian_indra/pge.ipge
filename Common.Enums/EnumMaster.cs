﻿using System.ComponentModel;

namespace Common.Enums
{
     public enum UserAccount
    {
        //default user
        [Description("superadmin")]
        SuperAdmin
    }

    public enum Day
    {
        SUNDAY = 0,
        MONDAY = 1,
        TUESDAY = 2,
        WEDNESDAY = 3,
        THURSDAY = 4,
        FRIDAY = 5,
        SATURDAY = 6,
    }

    public enum Month
    {
        [Description("Jan")]
        JAN = 1,
        [Description("Feb")]
        FEB = 2,
        [Description("Mar")]
        MAR = 3,
        [Description("Apr")]
        APR = 4,
        [Description("Mei")]
        MEI = 5,
        [Description("Jun")]
        JUN = 6,
        [Description("Jul")]
        JUL = 7,
        [Description("Agt")]
        AUG = 8,
        [Description("Sep")]
        SEP = 9,
        [Description("Okt")]
        OKT = 10,
        [Description("Nov")]
        NOV = 11,
        [Description("Des")]
        DES = 12,
	}

	public enum Bilangan
	{
		[Description("Nol")]
		NOL = 0,
		[Description("Satu")]
		SATU = 1,
		[Description("Dua")]
		DUA = 2,
		[Description("Tiga")]
		TIGA = 3,
		[Description("Empat")]
		EMPAT = 4,
		[Description("Lima")]
		LIMA = 5,
		[Description("Enam")]
		ENAM = 6,
		[Description("Tujuh")]
		TUJUH = 7,
		[Description("Delapan")]
		DELAPAN = 8,
		[Description("Sembilan")]
		SEMBILAN = 9,
	}

	public enum NamaSeparator
	{
		[Description("Ribu")]
		RIBU = 1,
		[Description("Juta")]
		JUTA = 2,
		[Description("Milyar")]
		MILYAR = 3,
		[Description("Triliun")]
		TRILIUN = 4,
		[Description("Ribu Triliun")]
		RIBU_TRILIUN = 5,
	}

	public enum OptionType
    {
        DIAGRAM,
        CLUSTER,
        AREA,
        AREAALL,
        AREA_USER,
        AREA_ZPGE,
        COMPANY,
        STATUS,
        DATASTATUS,
        POWERPLANT,
        SEPARATOR,
        INTERFACE_POINT,
        SCRUBBER,
        DRAWWELL,
        DRAWWELL_STATUS,
        INTERRUPTION_STATUS,
        TIME_SPAN,
        ENERGY_UNIT,
        TEMPERATURE_UNIT,
        PRESSURE_UNIT,
        FLOW_UNIT,
        MUFFLER_UNIT,
        OTHER_UNIT,
        INTERRUPTION_LOCATION,
        WELL_HISTORY_TARGET,
        SHIP_TO_PARTY,
        SO_TYPE,
        MATERIAL,
        SOLD_TO_PARTY,
        YEAR_BY_TASK,
        RANGE_DATA,
		USERINFO,
		USER,
		USERFULLNAME,
        BUSINESS_OWNER,
        YEAR_BY_INVOICE,
        PRODUCT_UTILIZATION,
		WELL,
        MONTH,
        RANGE_DATA_MOBILE,
        YEAR,
        AREA_REPORT,
        DRAWWELL_CATEGORY,
        REPORT_MODULE,
        POWERPLANT_BY_AREA,
        WELL_BY_AREA,
        INTERFACE_POINT_BY_AREA,
        SCRUBBER_BY_AREA,
        SEPARATOR_BY_AREA,
        VIEW_OPTIONS
    }

    public enum CrewStatus
    {
        [Description("Pilot")]
        PILOT,
        [Description("Pramugari")]
        STEWARDESS
    }

    public enum DataStatus
    {
        [Description("Waiting To Be Approved")]
        WAITING,
        [Description("Approved")]
        APPROVED
    }

    public enum PowerPlant
    {
        [Description("KMJ - Unit 1 (IP)")]
        KMJIP1,
        [Description("KMJ - Unit 2 (IP)")]
        KMJIP2,
        [Description("KMJ - Unit 3 (IP)")]
        KMJIP3,
        [Description("KMJ - Unit 4 (PGE)")]
        KMJPGE4,
        [Description("KMJ - Unit 5 (PGE)")]
        KMJPGE5,
    }

    public enum DrawWellKmJ
    {
        [Description("KMJ.11")]
        KMJ11,
        [Description("KMJ.14")]
        KMJ14,
        [Description("KMJ.17")]
        KMJ17,
        [Description("KMJ.18")]
        KMJ18,
        [Description("KMJ.67")]
        KMJ67,
        [Description("KMJ.24")]
        KMJ24,
        [Description("KMJ.25")]
        KMJ25,
        [Description("KMJ.43")]
        KMJ43,
        [Description("KMJ.44")]
        KMJ44
    }

    public enum DrawWellStatus
    {
        [Description("Bleeding")]
        BLEEDING,
        [Description("Operation")]
        OPERATION,
        [Description("Shut In")]
        SHUT_IN,
        [Description("Test")]
        TEST,
        [Description("Production")]
        PRODUCTION,
        [Description("Injection")]
        INJECTION,
        [Description("Monitoring")]
        MONITORING,
    }

    public enum DrawWellSubStatus
    {
        [Description("Bleeding")]
        BLEEDING,
        [Description("Shut In")]
        SHUT_IN,
        [Description("Test")]
        TEST,
        [Description("Operation")]
        OPERATION,               
    }

    public enum Fluida
    {
        [Description("Hot Brine")]
        HOT_BRINE,
        [Description("Cold Brine")]
        COLD_BRINE,
        [Description("Condesat")]
        CONDENSAT
    }

    public enum InterruptionStatus
    {
        [Description("PO (Planned Outage)")]
        PO,
        [Description("PE (Planned Outage Extension)")]
        PE,
        [Description("MO (Maintenance Outage)")]
        MO,
        [Description("ME (Maintenance Outage Extension)")]
        ME,
        [Description("FO1 (Forced Outage Immediate)")]
        FO1,
        [Description("FO2 (Forced Outage Delayed)")]
        FO2,
        [Description("FO3 (Forced Outage Postponed)")]
        FO3,
        [Description("SF (Startup Failure)")]
        SF,
        [Description("PD (Planned Derating)")]
        PD,
        [Description("PDE (Planned Derating Extension)")]
        PDE,
        [Description("MD (Maintenance Derating Extension)")]
        MD,
        [Description("MDE (Maintenance Derating Extension)")]
        MDE,
        [Description("FD1 (Forced Derating Immediate)")]
        FD1,
        [Description("FD2 (Forced Derating Delayed)")]
        FD2,
        [Description("FD3 (Forced Derating Postponed)")]
        FD3,
        [Description("NORMAL")]
        NORMAL
    }

    public enum InterruptionLocation
    {
        [Description("SGS")]
        SGS,
        [Description("GPP")]
        GPP,
        [Description("Eksternal")]
        EKSTERNAL
    }

    public enum SheetExcel
    {
        PLTP,
        DKP,
        PRODUKSI,
        REINJEKSI,
        MONITORING,
        SEPARATOR,
        INTERFACE_SCRUBBER,
        RENCANA_REALISASI,
        MASTER
    }

    public enum PltpExcelColumn
    {
        //PLTP
        [Description("Power Plant")]
        POWERPLANT,
        [Description("Energi Listrik (24 Jam) Gross")]
        ENERGY_GROSS,
        [Description("Energi Listrik (24 Jam) Nett")]
        ENERGY_NETT,
        [Description("Pemanfaatan Uap Oleh Pembangkit")]
        STEAM_UTILIZATION,
        [Description("Buangan ke Rock Muffler")]
        WASTE_MUFFLER,
        [Description("SSC")]
        SSC,
        [Description("GPP Shutdown (Hour)")]
        GPP_SHUTDOWN,
        [Description("Import Energy")]
        IMPORT_ENERGY,
        [Description("SGSS Shutdown (Hour)")]
        SGSS_SHUTDOWN,
        [Description("Keterangan")]
        NOTES,
    }

    public enum DKPColumnHeader
    {
        [Description("PLTP")]
        PLTP,
        [Description("Status Gangguan")]
        INTERRUPTION_STATUS,
        [Description("Jam Awal Gangguan (HH:mm) => gunakan tanda petik satu (') sebelum mengetik jam")]
        INTERRUPTION_START_TIME,
        [Description("Jam Akhir Gangguan (HH:mm) => gunakan tanda petik satu (') sebelum mengetik jam")]
        INTERRUPTION_FINISH_TIME,
        [Description("Derating (MW)")]
        DERATING,
        [Description("Lokasi Gangguan")]
        INTERRUPTION_LOCATION,
        [Description("Penyebab Gangguan")]
        INTERRUPTION_CAUSE,
        [Description("Keterangan")]
        NOTES
    }

    public enum ProductionColumnHeader
    {
        [Description("SUMUR")]
        WELL,
        [Description("TKS")]
        TKS,
        [Description("Tekanan Jalur")]
        LINE_PRESSURE,
        [Description("Suhu Jalur")]
        LINE_TEMPERATURE,
        [Description("Trottle Valve Opening")]
        TRHOTTLE_VALVE_OPENING,
        [Description("Flow Rata2")]
        FLOW_AVERAGE,
        [Description("Flow Total 24 Jam")]
        FLOW_TOTAL,
        [Description("Pressure Upstream")]
        PRESSURE_UPSTREAM,
        [Description("Differential Pressure")]
        DIFFERENTIAL_PRESSURE,
        [Description("Status Sumur")]
        WELL_STATUS,
        [Description("Keterangan")]
        NOTES
    }

    public enum ReinjectionColumnHeader
    {
        [Description("SUMUR")]
        WELL,
        [Description("TKS")]
        TKS,
        [Description("Tekanan Jalur")]
        LINE_PRESSURE,
        [Description("Suhu Jalur")]
        LINE_TEMPERATURE,
        [Description("pH")]
        PH,
        [Description("Flow Rata2")]
        FLOW_AVERAGE,
        [Description("Flow Total 24 Jam")]
        FLOW_TOTAL,
        [Description("Fluida yang Diinjeksikan")]
        INJECTED_FLUID,
        [Description("Status Sumur")]
        WELL_STATUS,
        [Description("Keterangan")]
        NOTES
    }

    public enum MonitoringColumnHeader
    {
        [Description("SUMUR")]
        WELL,
        [Description("TKS")]
        TKS,
        [Description("Status Sumur")]
        WELL_STATUS,
        [Description("Keterangan")]
        NOTES
    }

    public enum SeparatorColumnHeader
    {
        [Description("SEPARATOR")]
        SEPARATOR,
        [Description("Tekanan")]
        PRESSURE,
        [Description("Suhu")]
        TEMPERATURE,
        [Description("Level Brine (%)")]
        LEVEL_BRINE,
        [Description("Trottle Valve Opening Brine (%)")]
        THROTTLE_VALVE_OPENING_BRINE,
        [Description("Trottle Valve Opening Steam (%)")]
        THROTTLE_VALVE_OPENING_STEAM,
        [Description("Steam Flow Rata2")]
        AVERAGE_STEAM_FLOW,
        [Description("Brine Flow Rata2")]
        AVERAGE_BRINE_FLOW
    }

    public enum InterfaceScrubberColumnHeader
    {
        [Description("INTERFACE POINT")]
        INTERFACE_POINT,
        [Description("Tekanan Interface Point")]
        INTERFACE_POINT_PRESSURE,
        [Description("Suhu Interface Point")]
        INTERFACE_POINT_TEMPERATURE,
        [Description("SCRUBBER")]
        SCRUBBER,
        [Description("Tekanan Scrubber")]
        SCRUBBER_PRESSURE,
        [Description("Suhu Scrubber")]
        SCRUBBER_TEMPERATURE
    }

    public enum ActualPlanningColumnHeader
    {
        [Description("AREA")]
        AREA,
        [Description("Rencana")]
        PLANNING,
        [Description("Realisasi Kemarin")]
        LAST_ACTUAL,
        [Description("Temuan Kemarin")]
        LAST_FINDINGS
    }

    public enum InputSource
    {
        [Description("Web Application")]
        WEB,
        [Description("Mobile Application")]
        MOBILE
    }

    public enum TabName
    {
        [Description("PLTP")]
        PLTP,
        [Description("DKP")]
        DKP,
        [Description("Produksi")]
        PRODUCTION,
        [Description("Reinjeksi")]
        REINJECTION,
        [Description("Monitoring")]
        MONITORING,
        [Description("Separator")]
        SEPARATOR,
        [Description("Interface Scrubber")]
        INTERFACE_SCRUBBER,
        [Description("Rencana Realisasi")]
        ACTUAL_PLAN
    }
    public enum TargetTab
    {
        [Description("tab1")]
        TAB_PLTP,
        [Description("tab2")]
        TAB_DKP,
        [Description("tab3")]
        TAB_PRODUCTION,
        [Description("tab4")]
        TAB_REINJECTION,
        [Description("tab5")]
        TAB_MONITORING,
        [Description("tab6")]
        TAB_SEPARATOR,
        [Description("tab7")]
        TAB_INTERFACE_SCRUBBER,
        [Description("tab8")]
        TAB_ACTUAL_PLAN
    }

    public enum EnergyUnit
    {
        [Description("BTU")]
        BTU,
        [Description("J")]
        J,
        [Description("KJ")]
        KJ,
        [Description("KWh")]
        KW_HR,
        [Description("MJ")]
        MJ,
        [Description("MWh")]
        MW_HR,
        [Description("TJ")]
        TJ,
        [Description("W.s")]
        W_S
    }

    public enum TemperatueUnit
    {
        [Description("deg C")]
        DEG_C,
        [Description("deg F")]
        DEG_F,
        [Description("deg R")]
        DEG_R,
        [Description("K")]
        K,
        [Description("Rk")]
        RK
    }

    public enum FlowUnit
    {
        [Description("bbl/day200")]
        BBL_DAY200,
        [Description("bbl/day300")]
        BBL_DAY330,
        [Description("gpm200")]
        GPM200,
        [Description("gpm300")]
        GPM330,
        [Description("kg/hr")]
        KG_HR,
        [Description("kg/s")]
        KG_S,
        [Description("klb/hr")]
        KLB_HR,
        [Description("l/min")]
        L_MIN,
        [Description("l/s")]
        L_S,
        [Description("lb/hr")]
        LB_HR,
        [Description("lb/s")]
        LB_S,
        [Description("t/day")]
        T_DAY,
        [Description("t/hr")]
        T_HR,
        [Description("USton/day")]
        USTON_DAY,
        [Description("USton/hr")]
        USTON_HR
    }

    public enum RockMufflerUnit
    {
        [Description("g")]
        G,
        [Description("kg")]
        KG,
        [Description("klb")]
        KLB,
        [Description("kt")]
        KT,
        [Description("lb")]
        LB,
        [Description("t")]
        T,
        [Description("USton")]
        USTON
    }

    public enum PressureUnit
    {
        [Description("atm")]
        ATM,
        [Description("bara")]
        BARA,
        [Description("barg")]
        BARG,
        [Description("inch Hg")]
        INCH_HG,
        [Description("inch water")]
        INCH_WATER,
        [Description("ksca")]
        KSCA,
        [Description("kscg")]
        KSCG,
        [Description("kPaa")]
        KPAA,
        [Description("kPag")]
        KPAG,
        [Description("mba")]
        MBA,
        [Description("mbg")]
        MBG,
        [Description("mm Hg")]
        MM_HG,
        [Description("mm water")]
        MM_WATER,
        [Description("MPaa")]
        MPAA,
        [Description("MPag")]
        MPAG,
        [Description("Paa")]
        PAA,
        [Description("Pag")]
        PAG,
        [Description("psia")]
        PSIA,
        [Description("psig")]
        PSIG
    }

    public enum OtherUnit
    {
        [Description("%")]
        PERCENT,
        [Description("ton/MW.hr")]
        TON_MW_HR,
        [Description("hr")]
        HR
    }

    public enum WellHistoryTarget
    {
        [Description("Produksi")]
        PRODUCTION,
        [Description("Monitoring")]
        MONITORING,
        [Description("Reinjeksi")]
        REINJECTION
    }

    public enum Workflow
    {
        [Description("Draft")]
        DRAFT,
        [Description("Area Operation Manager Verification")]
        AREA_OPS_MANAGER_VERIFICATION,
        [Description("Area Finance Verification")]
        AREA_FINANCE_VERIFICATION,
        [Description("Manager Operation OE Verification")]
        MANAGER_OPS_OE_VERIFICATION,
        [Description("Finish")]
        FINISH,
        [Description("Partnership Manager Verification")]
        PARTNERSHIP_MANAGER_VERIFICATION,
        [Description("Update PR And PO")]
        UPDATE_PR_AND_PO,
        [Description("Print Cover Letter")]
        PRINT_COVER_LETTER
    }

	public enum WorkflowType
	{
		ZTS, // so zts
		ZPGE, // so zpge
		PRICE, // maintain harga
		INVEL, // invoice electric
		INVSM // invoice steam
	}

	public enum SOType
    {
        [Description("ZTS")]
        ZTS,
        [Description("Own Operation")]
        ZPGE
    }

    public enum Material
    {
        [Description("A200900006 - Geothermal Steam")]
        GEOTHERMAL_STEAM,
        [Description("A200900005 - Electric Power")]
        ELECTRIC_POWER
	}

	public enum ProductionUtilization
	{
		[Description("Uap")]
		STEAM,
		[Description("Listrik")]
		ELECTRIC,
        [Description("Days Effective")]
        DAYSEFFECTIVE
        
    }

	public enum QuarterYear
	{
		[Description("Triwulan I")]
		I = 1,
		[Description("Triwulan II")]
		II = 2,
		[Description("Triwulan III")]
		III = 3,
		[Description("Triwulan IV")]
		IV = 4,
	}

    public enum RangeData
    {
        [Description("Monthly")]
        MONTHLY,
        [Description("Yearly")]
        YEARLY,
        [Description("Daily")]
        DAILY
    }

    public enum BussinessOwner
    {
        [Description("Own Operation")]
        OWN_OPERATION,
        [Description("KOB")]
        KOB
    }

    public enum Diagram
    {
        [Description("Production")]
        PRODUCTION,
        [Description("Financial")]
        FINANCE
    }

    public enum WorkflowStatus
    {
        FINISH,
	}

	public enum ReliabilityCategory
	{
		[Description("Realisasi")]
		REALIZATION,
		[Description("Tidak terserap")]
		NOT_ABSORBED
	}

	public enum ReliabilitySubCategory
	{
		[Description("Realisasi")]
		REALIZATION,
		[Description("Tidak terserap karena faktor internal")]
		NOT_ABSORBED_INTERNAL,
		[Description("Tidak terserap karena faktor eksternal")]
		NOT_ABSORBED_EXTERNAL
	}

    public enum DefaultAdditionalText
    {
        DEFAULT
    }

    public enum ReportModule
    {
        [Description("Pembangkit Energi Listrik")]
        PEL,
        [Description("Pemanfaatan UAP")]
        PU,
        [Description("Sumur Produksi")]
        SP,
        [Description("Sumur Injeksi")]
        SI,
        [Description("Sumur Monitoring & Abandoned")]
        SMA,
        [Description("Interface")]
        INTERFACE,
        [Description("Scrubber")]
        SCRUBBER,
        [Description("Separator")]
        SEPARATOR
    }

    public enum ViewOptions
    {
        [Description("Executive")]
        EXECUTIVE,
        [Description("Operation")]
        OPERATION,
        [Description("Financial")]
        FINANCIAL
    }
}
