﻿using Common.Enums;
using System;

namespace Business.Infrastructure
{
    public class FieldHelper
    {
        public Table? Table { get; set; }
        public Field? Field { get; set; }
        public string Parse
        {
            get
            {
                if (Table != null && Field != null)
                    return $"{Table}.{Field}";

                return null;
            }
        }

        private FieldHelper()
        {
        }

        public FieldHelper(Table table, Field field)
        {
            Table = table;
            Field = field;
        }

        public FieldHelper(string table, string field)
        {
            Table = (Table)Enum.Parse(typeof(Table), table);
            Field = (Field)Enum.Parse(typeof(Field), field);
        }

        public static string ParseRelationField(Table table, Field field)
        {
            return $"{table}.{field}";
        }

        public bool Is(FieldHelper f)
        {
            return f != null && Table != null && Field != null 
                && Table == f.Table && Field == f.Field;
        }
    }
}
