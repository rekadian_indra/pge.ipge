﻿namespace Business.Infrastructure
{
    public static class DateTimeFormat
    {
        public const string CompactDayDateTimeFormat = "ddd MMM d yyyy HH:mm:ss";  //Mon Apr 23 2018 13:58:23
        public const string CompactDayDateFormat = "ddd MMM d yyyy";  //Mon Apr 23 2018
        public const string CompactDateFormat = "MMM d yyyy";  //Apr 23 2018
        public const string LocalDateFormat = "d MMMM yyyy";  //23 April 2018
		public const string MonthYear = "MMM yyyy";  //Apr 2018
	}
}
