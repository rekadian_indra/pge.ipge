﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Infrastructure
{
    public static class ListMapper
    {
        /// <summary>
        /// Melakukan pemetaan dari source collection ke target collection dengan satu parameter pada constructor target.
        /// </summary>
        /// <param name="sourceCollection">Collection yang akan dipetakan</param>
        /// <returns>List hasil dari pemetaan.</returns>
        public static List<TModel> MapList<TEntity, TModel>(IEnumerable<TEntity> sourceCollection)
        {
            return MapList<TEntity, TModel>(sourceCollection, null);
        }

        /// <summary>
        /// Melakukan pemetaan dari source collection ke target collection dengan banyak parameter pada constructor target.
        /// </summary>
        /// <param name="sourceCollection">Collection yang akan dipetakan</param>
        /// <param name="args">Parameter tambahan yang ada pada constructor target</param>
        /// <returns>List hasil dari pemetaan.</returns>
        public static List<TModel> MapList<TEntity, TModel>(IEnumerable<TEntity> sourceCollection, params object[] args)
        {
            List<TModel> models = new List<TModel>();

            foreach (TEntity o in sourceCollection)
            {
                TModel model;

                if (args != null && args.Any())
                {
                    List<object> objects = new List<object>();

                    objects.Add(o);
                    objects.AddRange(args);

                    model = (TModel)Activator.CreateInstance(typeof(TModel), objects.ToArray());
                }
                else
                {
                    model = (TModel)Activator.CreateInstance(typeof(TModel), o);
                }

                models.Add(model);
            }

            return models;
        }
    }
}