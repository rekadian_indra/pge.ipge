//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ViewInvoice
    {
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public string ShipToPartyName { get; set; }
        public string PowerPlantName { get; set; }
        public Nullable<int> PowerPlantId { get; set; }
        public string Material { get; set; }
        public Nullable<System.DateTime> TaskPeriode { get; set; }
        public string TaskStatus { get; set; }
        public Nullable<double> EnergyNett { get; set; }
        public Nullable<double> EnergyGross { get; set; }
        public Nullable<double> SteamUtilization { get; set; }
        public Nullable<double> NetPrice { get; set; }
        public Nullable<double> NetValue { get; set; }
        public Nullable<System.DateTime> SalesPeriode { get; set; }
        public Nullable<int> InvoiceId { get; set; }
        public string WorkFlowCode { get; set; }
        public string SOType { get; set; }
        public Nullable<double> Quantity { get; set; }
    }
}
