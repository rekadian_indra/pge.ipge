﻿using System.ComponentModel.DataAnnotations;

namespace Business.Entities
{
    [MetadataType(typeof(MembershipMetadata))]
    public partial class Membership
    {
    }

    public class MembershipMetadata
    {
        [Key]
        public System.Guid UserId { get; set; }

        //[ForeignKey("ApplicationId")]
        //public virtual Application Application { get; set; }

        //[ForeignKey("UserId")]
        //public virtual User User { get; set; }
    }
}
