//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalesOrder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SalesOrder()
        {
            this.BastFiles = new HashSet<BastFile>();
            this.BastFileSigneds = new HashSet<BastFileSigned>();
            this.SalesOrderApprovals = new HashSet<SalesOrderApproval>();
            this.ScreenshotSaps = new HashSet<ScreenshotSap>();
            this.Invoices = new HashSet<Invoice>();
        }
    
        public int Id { get; set; }
        public Nullable<System.DateTime> Periode { get; set; }
        public string SOType { get; set; }
        public Nullable<int> PowerPlantId { get; set; }
        public Nullable<double> Quantity { get; set; }
        public Nullable<double> NetPrice { get; set; }
        public Nullable<double> NetValue { get; set; }
        public string Currency { get; set; }
        public string Notes { get; set; }
        public Nullable<int> WorkflowId { get; set; }
        public string NoPo { get; set; }
        public string NoPr { get; set; }
        public string NoSo { get; set; }
        public string FormMiroNo { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BastFile> BastFiles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BastFileSigned> BastFileSigneds { get; set; }
        public virtual Workflow Workflow { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesOrderApproval> SalesOrderApprovals { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScreenshotSap> ScreenshotSaps { get; set; }
        public virtual PowerPlant PowerPlant { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice> Invoices { get; set; }
    }
}
