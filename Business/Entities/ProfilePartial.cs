﻿using System.ComponentModel.DataAnnotations;

namespace Business.Entities
{
    [MetadataType(typeof(ProfileMetadata))]
    public partial class Profile
    {
    }

    public class ProfileMetadata
    {
        [Key]
        public System.Guid UserId { get; set; }

        //[ForeignKey("ApplicationId")]
        //public virtual Application Application { get; set; }
    }
}
