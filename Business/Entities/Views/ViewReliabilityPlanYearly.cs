﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewReliabilityPlanYearly : IView
    {
        [Key]
        public string Id { get; set; }
        public int PowerPlantId { get; set; }
        public string PowerPlantName { get; set; }
		public int Year { get; set; }
	}
}
