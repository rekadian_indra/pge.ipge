﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewPlanPrice : IView
    {
        [Key]
        public int Id { get; set; }
        public int PowerPlantId { get; set; }
        public DateTime Periode { get; set; }        
        public double Price { get; set; }
        public double Value { get; set; }
        public double Total
        {
            get
            {
                return Price * Value;
            }
        }
    }
}
