﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewDkp : IView
    {
        [Key]
        public int Id { get; set; }
        public int PowerPlantId { get; set; }
		public DateTime Periode { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
		public double PowerPlantDMN { get; set; }
		public string Status { get; set; }
        public string StatusCategory { get; set; }
		public TimeSpan StartHour { get; set; }
		public TimeSpan EndHour { get; set; }
		public TimeSpan Duration { get; set; }

        public string Location { get; set; }
		
		public double FOH { get; set; }
		public double MOH { get; set; }
		public double POH { get; set; }
		public double EFDH { get; set; }
		public double EMDH { get; set; }
		public double EPDH { get; set; }

		public int PH { get; set; }
		public double FOHValue { get; set; }
		public double MOHValue { get; set; }
		public double POHValue { get; set; }
		public double EFDHValue { get; set; }
		public double EMDHValue { get; set; }
		public double EPDHValue { get; set; }
		public double AH { get; set; }
		public double EFOR { get; set; }
		public double EAF { get; set; }


	}
}
