﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entities.Views
{
    public class ViewSalesOrderApproval : IView
    {
        public int Id { get; set; }
        public int SalesOrderId { get; set; }      
        public string CreatedBy { get; set; }        
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public DateTime Periode { get; set; }       
        public int Year { get; set; }
        public int Month { get; set; }
        public string SOType { get; set; }
    }
}

