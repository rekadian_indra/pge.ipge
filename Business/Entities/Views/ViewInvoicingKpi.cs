﻿using System;

namespace Business.Entities.Views
{
    public class ViewInvoicingKpi : IView
    {
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int SalesOrderId { get; set; }
        public DateTime InvoiceSentDate { get; set; }
        public string Name { get; set; }
        public int AreaId { get; set; }
        public DateTime BASentDate { get; set; }
        public DateTime? AcceptedDateBy { get; set; }
        public DateTime? TargetSentBackDate { get; set; }
        public DateTime? ActualSentBackDate { get; set; }
        public double NetValue { get; set; }
        public double TransmitionValue { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
    }
}
