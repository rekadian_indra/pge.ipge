﻿using Common.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewWell : IView
    {
        [Key]
        public int Id { get; set; }
        public int AreaId { get; set; }
        public int WellId { get; set; }
        public string WellName { get; set; }
        public string ClusterName { get; set; }
        public string Status { get; set; }
        public string StatusDesc
        {
            get
            {
                return ((DrawWellStatus)Enum.Parse(typeof(DrawWellStatus), Status)).ToDescription();
            }
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        //{
        //    get
        //    {
        //        DateTime result = LastHistory.HistoryDateTimeUtc;

        //        if (LastHistory.Id == Id && result <= StartDate)
        //            result = DateTime.Now;

        //        return result;
        //    }
        //}
        public WellHistory LastHistory { get; set; }
        public bool IsDeleted { get; set; }
    }
}
