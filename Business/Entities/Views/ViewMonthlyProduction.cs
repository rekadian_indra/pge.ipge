﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewMonthlyProduction : IView
    {
        [Key] //Must have one key for default sorting or first property to be set key
		public int PowerPlantId { get; set; }

		public int Year { get; set; }
		public int Month { get; set; }

		public string PowerPlantName { get; set; }
		public string Bulan { get; set; }
		public string Hari { get; set; }
		public double UapPlan { get; set; }
		public double UapActual { get; set; }
		public double UapActualKumulatif { get; set; }
		public double ListrikPlanPembangkitan { get; set; }
		public double ListrikPlanPembangkitanKumulatif { get; set; }
		public double ListrikPlanHargaJual { get; set; }
		public double ListrikPlanDapatUSD { get; set; }
		public double ListrikPlanDapatIDR { get; set; }
		public double ListrikPlanKumulatifIDR { get; set; }
		public double ListrikActualPembangkitan { get; set; }
		public double ListrikActualPembangkitanKumulatif { get; set; }
		public double ListrikActualHargaJual { get; set; }
		public double ListrikActualDapatUSD { get; set; }
		public double ListrikActualDapatIDR { get; set; }
		public double ListrikActualKumulatifIDR { get; set; }
	}
}
