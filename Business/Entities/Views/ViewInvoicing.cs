﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entities.Views
{
    public class ViewInvoicing : IView
    {
        public int AreaId { get; set; }
        public int PowerPlantId { get; set; }
        public string AreaName { get; set; }
        public string PowerPlantName { get; set; }
        public DateTime? Periode { get; set; }
        public DateTime? SalesPeriode { get; set; }
        public string Material { get; set; }
        public string WorkFlowCode { get; set; }
        public double Quantity { get; set; }
        public double NetPrice { get; set; }
        public double NetValue { get; set; }
        //public double NetValue
        //{
        //    get
        //    {
        //        return Quantity * NetPrice;
        //    }
        //}
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public bool HasInvoice { get; set; }
        public int TotalInvoice { get; set; }
    }
}
