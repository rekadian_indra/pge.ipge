﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewProductionPlanDetail : IView
    {
        [Key]
        public int Id { get; set; }
        public int PowerPlantId { get; set; }
		public int Year { get; set; }
        public int Month { get; set; }
		public string UtilType { get; set; }
		public double Value { get; set; }
	}
}
