﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entities.Views
{
    public class ViewCapacityFactor : IView
    {
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int PowerPlantId { get; set; }
        public string PowerPlantName { get; set; }        
        public double Capacity { get; set; }
        public double Actual { get; set; }
        public string SOType { get; set; }
    }
}
