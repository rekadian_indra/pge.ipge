﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entities.Views
{
    public class ViewInvoicingSalesOrder : IView
    {
        public Nullable<int> AreaId { get; set; }
        public string Name { get; set; }
        public string Material { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public Nullable<double> Quantity { get; set; }
        public Nullable<double> NetPrice { get; set; }
        public Nullable<double> NetValue { get; set; }
        public List<ViewPltp> Pltps { get; set; }
    }
}
