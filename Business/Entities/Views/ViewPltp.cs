﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewPltp : IView
    {
        [Key]
        public int Id { get; set; }
        public int PowerPlantId { get; set; }
		public string Material { get; set; }
		public int Year { get; set; }
		public int Month { get; set; }
		public DateTime Periode { get; set; }
		public double EnergyGross { get; set; }
        public double EnergyNett { get; set; }
        public double SteamUtilization { get; set; }
        public double WasteRockMuffler { get; set; }
		public double Ssc { get; set; }
		public string Notes { get; set; }
    }
}
