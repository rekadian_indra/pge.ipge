﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entities.Views
{
    public class ViewSalesOrderFinance : IView
    {
        public int AreaId { get; set; }
        public int PowerPlantId { get; set; }
        public string AreaName { get; set; }
        public string PowerPlantName { get; set; }
        public DateTime Periode { get; set; }
        public string WorkFlowCode { get; set; }
        public double Quantity { get; set; }
        public double NetPrice { get; set; }
        public string Status { get; set; }
        public double NetValue
        {
            get
            {
                return Quantity * NetPrice;
            }
        }
        public int Year
        {
            get
            {
                return Periode.Year;
            }
        }
        public int Month {
            get
            {
                return Periode.Month;
            }
        }
        public int Day {
            get
            {
                return Periode.Day;
            }
        }
        public bool HasInvoice { get; set; }
    }
}
