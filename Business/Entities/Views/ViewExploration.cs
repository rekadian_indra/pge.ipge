﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewExploration : IView
    {
        [Key]
        public DateTime ExplorationDate { get; set; }
        public int AreaId { get; set; }
        public string AreaName { get; set; }
    }
}
