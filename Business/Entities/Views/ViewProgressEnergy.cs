﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewProgressEnergy : IView
    {
        public int? AreaId { get; set; }
        public string AreaName { get; set; }
        public int? PowerPlantId { get; set; }
        public DateTime? Periode { get; set; }
        public string PowerPlant { get; set; }
        public string Material { get; set; }
        public double? EnergyPrice { get; set; }
        public double? SteamPrice { get; set; }
        public double? EnergyNett { get; set; }
        public double? EnergyGross { get; set; }
        public double? SteamUtilization { get; set; }
        public double? EnergyPlan { get; set; }
        public double? SteamPlan { get; set; }
        public int? Quarter { get; set; }
        public int? QuarterYear { get; set; }
        public int? Year { get; set; }
        public int Month { get; set; }
        public int? LastDay { get; set; }
        public string SOType { get; set; }
        public double? DayEffective { get; set; }
        public double EnergyNettPriceNett { get; set; }
        public double EnergyGrossPriceNett { get; set; }
        public bool IsDeleted { get; set; }

        public DateTime? LastDate
        {
            get
            {
                if (Periode.HasValue)
                {
                    DateTime lastDate = new DateTime(Periode.Value.Year, Periode.Value.Month, 1).AddMonths(1);
                    return lastDate = lastDate.AddDays(-1);
                }
                else
                    return null;
            }
        }
        public double AverageEnergy
        {
            get
            {
                int lastDate = LastDate.HasValue ? LastDate.Value.Day : 0;
                //int lastDay = LastDate.HasValue ? LastDate.Value.Day : 0;
                double lastDay = DayEffective.HasValue ? DayEffective.Value : lastDate;
                double energy = EnergyNett.HasValue ? EnergyNett.Value : 0;
                if (lastDay > 0)
                    return energy / lastDay;
                else
                    return 0;
            }
        }

        public double AverageSteam
        {
            get
            {
                int lastDate = LastDate.HasValue ? LastDate.Value.Day : 0;
                //int lastDay = LastDate.HasValue ? LastDate.Value.Day : 0;
                double lastDay = DayEffective.HasValue ? DayEffective.Value : lastDate;
                double steam = SteamUtilization.HasValue ? SteamUtilization.Value : 0;
                if (lastDay > 0)
                    return steam / lastDay;
                else
                    return 0;
            }
        }
        public double AverageEnergyPlan
        {
            get
            {
                int lastDate = LastDate.HasValue ? LastDate.Value.Day : 0;
                //int lastDay = LastDate.HasValue ? LastDate.Value.Day : 0;
                double lastDay = DayEffective.HasValue ? DayEffective.Value : lastDate;
                double energyPlan = EnergyPlan.HasValue ? EnergyPlan.Value : 0;
                if (lastDay > 0)
                    return energyPlan / lastDay;
                else
                    return 0;
            }
        }

        public double AverageSteamPlan
        {
            get
            {
                int lastDate = LastDate.HasValue ? LastDate.Value.Day : 0;
                //int lastDay = LastDate.HasValue ? LastDate.Value.Day : 0;
                double lastDay = DayEffective.HasValue ? DayEffective.Value : lastDate;
                double steamPlan = SteamPlan.HasValue ? SteamPlan.Value : 0;
                if (lastDay > 0)
                    return steamPlan / lastDay;
                else
                    return 0;
            }
        }
        public double TotalEnergyActual
        {
            get
            {
                double nett = EnergyNett.HasValue ? EnergyNett.Value : 0;
                double price = EnergyPrice.HasValue ? EnergyPrice.Value : 0;
                return nett * (price * 1000);
            }
        }
        public double TotalSteamActual
        {
            get
            {
                double steam = SteamUtilization.HasValue ? SteamUtilization.Value : 0;
                double price = SteamPrice.HasValue ? SteamPrice.Value : 0;
                return steam * price;
            }
        }

        public double TotalEnergyPlan
        {
            get
            {
                double price = EnergyPrice.HasValue ? EnergyPrice.Value : 0;
                return AverageEnergyPlan * price * 1000;
            }
        }
        public double TotalSteamPlan
        {
            get
            {
                double price = SteamPrice.HasValue ? SteamPrice.Value : 0;
                return AverageSteam * price;
            }
        }       
    }
}
