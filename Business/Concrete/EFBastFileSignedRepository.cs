﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;

namespace Business.Concrete
{
    public class EFBastFileSignedRepository : EFBaseRepository<BastFileSigned>, IBastFileSignedRepository
	{
        public EFBastFileSignedRepository() : base(true)
        {
		}
		
		public List<BastFileSigned> GetFromFilePath(string filePath)
		{
			// kamus
			FilterQuery filter;
			List<BastFileSigned> bastFileSigneds;

			// algo
			filter = new FilterQuery(Field.FilePath, FilterOperator.Equals, filePath);
			bastFileSigneds = FindAll(filter);

			return bastFileSigneds;
		}
	}
}
