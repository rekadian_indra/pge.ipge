﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFViewInvoiceRepository : EFBaseRepository<ViewInvoice>, IViewInvoiceRepository
	{
        public EFViewInvoiceRepository() : base(true)
        {

		}
	}
}
