﻿using Business.Abstract;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFViewReliabilityActualRepository : EFBaseRepository<ViewReliabilityActual>, IViewReliabilityActualRepository
	{
        public EFViewReliabilityActualRepository() : base(true)
        {

        }

		public Task<List<ViewReliabilityActual>> FindAllByPeriodeAsync(int year, int month)
		{
			FilterQuery filter;

			DateTime startDate = new DateTime(year, month, 1);
			DateTime endDate = startDate.AddMonths(1);

			filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
			filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);

			return FindAllAsync(filter);
		}

        protected override IQueryable<ViewReliabilityActual> GenerateView()
        {
			return Context.Dkps
				.Where(x => !x.IsDeleted && x.TaskReport != null && x.TaskReport.Periode.HasValue && x.PowerPlantId.HasValue && x.StartHour.HasValue && x.EndHour.HasValue)
				//.GroupBy(x => new { x.PowerPlantId, x.TaskReportId })
				.Select(x => new 
				{
					Id = x.Id,
					PowerPlantId = x.PowerPlantId.Value,
					Periode = x.TaskReport.Periode.Value,
					Month = x.TaskReport.Periode.Value.Month,
					Year = x.TaskReport.Periode.Value.Year,
					Status = x.Status,
					StartHour = x.StartHour.Value,
					EndHour = x.EndHour.Value
				})
				.GroupBy(x => new { x.PowerPlantId, x.Month, x.Year })
				.Select(x => new ViewReliabilityActual
				{
					Id = x.First().Id,
					PowerPlantId = x.First().PowerPlantId,
					Periode = x.First().Periode,
					Month = x.First().Month,
					Year = x.First().Year,
					Status = x.First().Status,
					PH = DateTime.DaysInMonth(x.First().Year, x.First().Month) * 24,
					FOHValue = x.Where(y => GetStatusCategory(y.Status) == "FOH").Sum(y => y.EndHour.TotalHours - y.StartHour.TotalHours),
					MOHValue = x.Where(y => GetStatusCategory(y.Status) == "MOH").Sum(y => y.EndHour.TotalHours - y.StartHour.TotalHours),
					POHValue = x.Where(y => GetStatusCategory(y.Status) == "POH").Sum(y => y.EndHour.TotalHours - y.StartHour.TotalHours),
					EMDHValue = x.Where(y => GetStatusCategory(y.Status) == "EMDH").Sum(y => y.EndHour.TotalHours - y.StartHour.TotalHours),
					EPDHValue = x.Where(y => GetStatusCategory(y.Status) == "EPDH").Sum(y => y.EndHour.TotalHours - y.StartHour.TotalHours),
					EFDHValue = x.Where(y => GetStatusCategory(y.Status) == "EFDH").Sum(y => y.EndHour.TotalHours - y.StartHour.TotalHours)
				})
				.Select(x => new ViewReliabilityActual
				{
					Id = x.Id,
					PowerPlantId = x.PowerPlantId,
					Periode = x.Periode,
					Month = x.Month,
					Year = x.Year,
					Status = x.Status,
					PH = x.PH,
					FOHValue = x.FOHValue,
					MOHValue = x.MOHValue,
					POHValue = x.POHValue,
					EFDHValue = x.EFDHValue,
					EMDHValue = x.EMDHValue,
					EPDHValue = x.EPDHValue,
				})
				.Select(x => new ViewReliabilityActual
				{
					Id = x.Id,
					PowerPlantId = x.PowerPlantId,
					Periode = x.Periode,
					Month = x.Month,
					Year = x.Year,
					Status = x.Status,
					PH = x.PH,
					FOHValue = x.FOHValue,
					MOHValue = x.MOHValue,
					POHValue = x.POHValue,
					EFDHValue = x.EFDHValue,
					EMDHValue = x.EMDHValue,
					EPDHValue = x.EPDHValue,
					AH = x.PH - x.FOHValue - x.POHValue - x.MOHValue
				})
				.Select(x => new ViewReliabilityActual
				{
					Id = x.Id,
					PowerPlantId = x.PowerPlantId,
					Periode = x.Periode,
					Month = x.Month,
					Year = x.Year,
					Status = x.Status,
					PH = x.PH,
					FOHValue = x.FOHValue,
					MOHValue = x.MOHValue,
					POHValue = x.POHValue,
					EFDHValue = x.EFDHValue,
					EMDHValue = x.EMDHValue,
					EPDHValue = x.EPDHValue,
					AH = x.AH,
					FOH = x.FOHValue / x.PH,
					MOH = x.MOHValue / x.PH,
					POH = x.POHValue / x.PH,
					EFDH = x.EFDHValue / x.AH,
					EMDH = x.EMDHValue / x.AH,
					EPDH = x.EPDHValue / x.AH,
					EFOR = (x.FOHValue + x.EFDHValue) / (x.FOHValue + x.PH),
					EAF = (x.AH - x.EFDHValue - x.EMDHValue - x.EPDHValue) / x.PH
				})
				.Distinct();
		}

		private string GetStatusCategory(string status)
		{
			if (Enum.TryParse(status, out InterruptionStatus statusEnum))
			{
				switch (statusEnum)
				{
					case (InterruptionStatus.FO1): return "FOH";
					case (InterruptionStatus.FO2): return "FOH";
					case (InterruptionStatus.FO3): return "FOH";
					case (InterruptionStatus.MO): return "MOH";
					case (InterruptionStatus.ME): return "MOH";
					case (InterruptionStatus.PO): return "POH";
					case (InterruptionStatus.FD1): return "EFDH";
					case (InterruptionStatus.FD2): return "EFDH";
					case (InterruptionStatus.FD3): return "EFDH";
					case (InterruptionStatus.MD): return "EMDH";
					case (InterruptionStatus.MDE): return "EMDH";
					case (InterruptionStatus.PD): return "EPDH";
					case (InterruptionStatus.PDE): return "EPDH";
					default: return "";
				}
			}
			return "";
		}
	}
}
