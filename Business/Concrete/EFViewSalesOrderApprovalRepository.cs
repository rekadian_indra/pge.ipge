﻿using Business.Abstract;
using Business.Entities.Views;
using Common.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewSalesOrderApprovalRepository : EFBaseRepository<ViewSalesOrderApproval>, IViewSalesOrderApprovalRepository
    {
        public EFViewSalesOrderApprovalRepository() : base(true)
        {

        }

        protected override IQueryable<ViewSalesOrderApproval> GenerateView()
        {
            return Context.Invoices
                .Select(o => new ViewSalesOrderApproval
                {
                    Id = o.Id,
                    SalesOrderId = o.SalesOrderId,
                    CreatedBy = o.CreatedBy,
                    AreaId = o.SalesOrder.PowerPlant.AreaId.Value,
                    AreaName = o.SalesOrder.SOType == SOType.ZTS.ToString() ? SOType.ZTS.ToString() : o.SalesOrder.PowerPlant.Area.Name,
                    Periode = o.SalesOrder.Periode.Value,
                    Year = o.SalesOrder.Periode.Value.Year,
                    Month = o.SalesOrder.Periode.Value.Month,
                    SOType = o.SalesOrder.SOType,
                }).GroupBy(n => new {n.AreaName, n.Periode }).Select(n => n.FirstOrDefault());
        }
    }
}
