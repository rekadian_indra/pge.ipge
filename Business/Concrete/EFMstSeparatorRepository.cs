﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFMstSeparatorRepository : EFBaseRepository<MstSeparator>, IMstSeparatorRepository
    {
        public EFMstSeparatorRepository() : base(true)
        {
        }
    }
}
