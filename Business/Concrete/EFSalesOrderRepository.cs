﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;

namespace Business.Concrete
{
    public class EFSalesOrderRepository : EFBaseRepository<SalesOrder>, ISalesOrderRepository
	{
        public EFSalesOrderRepository() : base(true)
        {
        }

		public List<SalesOrder> GetFromPowerPlant(int powerPlantId)
		{
			// kamus
			FilterQuery filter;
			List<SalesOrder> salesOrders;

			// algo
			filter = new FilterQuery(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
			salesOrders = FindAll(filter);

			return salesOrders;
		}

		public List<SalesOrder> GetFromPeriode(DateTime? periode, RangeData rangeData = RangeData.MONTHLY)
		{
			// kamus
			FilterQuery filter;
			List<SalesOrder> salesOrders;

			// algo
			DateTime startDate = (rangeData == RangeData.MONTHLY) ? new DateTime(periode.Value.Year, periode.Value.Month, 1) : new DateTime(periode.Value.Year, 1, 1);
			DateTime endDate = (rangeData == RangeData.MONTHLY) ? startDate.AddMonths(1) : startDate.AddYears(1);

			filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
			filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);
			salesOrders = FindAll(filter);

			return salesOrders;
		}
		
		public List<SalesOrder> GetFromPeriodeSOType(DateTime? periode, string soType, RangeData rangeData = RangeData.MONTHLY)
		{
			// kamus
			FilterQuery filter;
			List<SalesOrder> salesOrders;

			// algo
			DateTime startDate = (rangeData == RangeData.MONTHLY) ? new DateTime(periode.Value.Year, periode.Value.Month, 1) : new DateTime(periode.Value.Year, 1, 1);
			DateTime endDate = (rangeData == RangeData.MONTHLY) ? startDate.AddMonths(1) : startDate.AddYears(1);

			filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
			filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);
			filter.AddFilter(Field.SOType, FilterOperator.Equals, soType);
			salesOrders = FindAll(filter);

			return salesOrders;
		}

		public List<SalesOrder> GetFromPeriodeSOTypePowerPlant(DateTime? periode, string soType, int powerPlantId, RangeData rangeData = RangeData.MONTHLY)
		{
			// kamus
			FilterQuery filter;
			List<SalesOrder> salesOrders;

			// algo
			DateTime startDate = (rangeData == RangeData.MONTHLY) ? new DateTime(periode.Value.Year, periode.Value.Month, 1) : new DateTime(periode.Value.Year, 1, 1);
			DateTime endDate = (rangeData == RangeData.MONTHLY) ? startDate.AddMonths(1) : startDate.AddYears(1);

			filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
			filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);
			filter.AddFilter(Field.SOType, FilterOperator.Equals, soType);
			filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
			salesOrders = FindAll(filter);

			return salesOrders;
		}

		public List<SalesOrder> GetFromPeriodePowerPlant(DateTime? periode, int powerPlantId, RangeData rangeData = RangeData.MONTHLY)
		{
			// kamus
			FilterQuery filter;
			List<SalesOrder> salesOrders;

			// algo
			DateTime startDate = (rangeData == RangeData.MONTHLY) ? new DateTime(periode.Value.Year, periode.Value.Month, 1) : new DateTime(periode.Value.Year, 1, 1);
			DateTime endDate = (rangeData == RangeData.MONTHLY) ? startDate.AddMonths(1) : startDate.AddYears(1);

			filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
			filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);
			filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
			salesOrders = FindAll(filter);

			return salesOrders;
		}
	}
}
