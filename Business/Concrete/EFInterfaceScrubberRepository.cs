﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFInterfaceScrubberRepository : EFBaseRepository<InterfaceScrubber>, IInterfaceScrubberRepository
    {
        public EFInterfaceScrubberRepository() : base(true)
        {
        }

		public async Task<List<InterfaceScrubber>> FindAllByPeriodeArea(DateTime date, int areaId)
		{
			// kamus
			List<Business.Entities.InterfaceScrubber> interfaceScrubbers;

			// algo
			interfaceScrubbers = await FindAllAsync();
			interfaceScrubbers = interfaceScrubbers.Where(interfaceScrubber =>
				(interfaceScrubber.TaskReportId != null) &&
				((areaId == 0) || (interfaceScrubber.TaskReport.AreaId == areaId)) &&
				(interfaceScrubber.TaskReport.Periode == date) &&
				(!interfaceScrubber.TaskReport.IsDeleted)
			).ToList();

			return interfaceScrubbers;
		}
	}
}