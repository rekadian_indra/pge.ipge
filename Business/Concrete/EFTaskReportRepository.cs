﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;

namespace Business.Concrete
{
    public class EFTaskReportRepository : EFBaseRepository<TaskReport>, ITaskReportRepository
    {
        public EFTaskReportRepository() : base(true)
        {
        }

		public List<TaskReport> GetFromPowerPlantIdPeriode(int? powerPlantId, DateTime? periode = null, RangeData rangeData = RangeData.MONTHLY)
		{
			// kamus
			FilterQuery filter;
			List<TaskReport> reports;
			List<TaskReport> reportsFiltered;

			// algo
			if (periode != null)
			{
				DateTime startDate = (rangeData == RangeData.MONTHLY) ? new DateTime(periode.Value.Year, periode.Value.Month, 1) : new DateTime(periode.Value.Year, 1, 1);
				DateTime endDate = (rangeData == RangeData.MONTHLY) ? startDate.AddMonths(1) : startDate.AddYears(1);

				filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
				filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);
			}
			else
			{
				filter = new FilterQuery();
			}
			reports = FindAll(filter);

			reportsFiltered = new List<TaskReport>();
			foreach (TaskReport t in reports)
			{
				if (t.Pltps.Where(x => x.PowerPlantId == powerPlantId).ToList().Any())
				{
					reportsFiltered.Add(t);
				}
			}

			return reportsFiltered;
		}

		public List<TaskReport> GetFromPeriode(DateTime? periode, bool yearly = false)
		{
			// kamus
			FilterQuery filter;
			List<TaskReport> reports;           
            // algo
            if (!yearly)
            {
                filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, new DateTime(periode.Value.Year, periode.Value.Month, 1));

                if(periode.Value.Month < 12)
                    filter.AddFilter(Field.Periode, FilterOperator.LessThan, new DateTime(periode.Value.Year, periode.Value.Month + 1, 1));
                else
                    filter.AddFilter(Field.Periode, FilterOperator.LessThan, new DateTime(periode.Value.Year + 1, 1, 1));
                reports = FindAll(filter);
            }
            else
            {
                filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, new DateTime(periode.Value.Year, 1, 1));
                filter.AddFilter(Field.Periode, FilterOperator.LessThan, new DateTime(periode.Value.Year, 12, 31));
                reports = FindAll(filter);
            }
			

			return reports;
		}

        //public TaskReport GetLastData(int? areaId = null, int? year = null)
        //{
        //    //kamus
        //    FilterQuery filter;
        //    SortQuery sort;
        //    List<SortQuery> sorts = new List<SortQuery>();
        //    TaskReport result;

        //    //algo
        //    sort = new SortQuery(Field.Periode, SortOrder.Descending);
        //    sorts.Add(sort);

        //    filter = new FilterQuery(Field.Status, FilterOperator.Equals, DataStatus.APPROVED);
        //    if (areaId != null) filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId.Value);
        //    if(year != null)
        //    {
        //        filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, new DateTime(year.Value, 1, 1));
        //        filter.AddFilter(Field.Periode, FilterOperator.LessThan, new DateTime(year.Value, 12, 31));
        //    }
        //    return result = Find(sorts, filter);
        //}

        public TaskReport GetLastData(int? areaId = null, int? month = null, int? year = null)
        {
            //kamus
            FilterQuery filter;
            SortQuery sort;
            List<SortQuery> sorts = new List<SortQuery>();
            TaskReport result;
            Task<TaskReport> taskTaskReport;

            //algo
            sort = new SortQuery(Field.Periode, SortOrder.Descending);
            sorts.Add(sort);

            filter = new FilterQuery(Field.Status, FilterOperator.Equals, DataStatus.APPROVED);
            if (areaId != null) filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId.Value);

            if(month != null && year != null)
            {
                DateTime d = new DateTime(year.Value, month.Value, 1);
                filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, d);
                filter.AddFilter(Field.Periode, FilterOperator.LessThan, d.AddMonths(1));
            }
            else if (year != null)
            {
                DateTime d = new DateTime(year.Value, 1, 1);
                filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, d);
                filter.AddFilter(Field.Periode, FilterOperator.LessThan, d.AddYears(1));
            }
            taskTaskReport = FindAsync(sorts, filter);
            Task.WhenAll(taskTaskReport);
            return result = taskTaskReport.Result;
        }

        public Task<TaskReport> GetLastDataAsync(int? areaId = null, int? month = null, int? year = null)
        {
            return Task.FromResult(GetLastData(areaId,month,year));
        }
    }
}