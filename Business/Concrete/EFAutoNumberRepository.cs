﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFAutoNumberRepository : EFBaseRepository<AutoNumber>, IAutoNumberRepository
    {
        public EFAutoNumberRepository() : base()
        {
		}
	}
}