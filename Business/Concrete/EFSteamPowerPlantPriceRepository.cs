﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFSteamPowerPlantPriceRepository : EFBaseRepository<SteamPowerPlantPrice>, ISteamPowerPlantPriceRepository
	{
        public EFSteamPowerPlantPriceRepository() : base(true)
        {
		}

		public List<SteamPowerPlantPrice> GetFromPowerPlantId(int powerPlantId, DateTime? startDate, DateTime? endDate)
		{
			// kamus
			FilterQuery filter;
			List<SteamPowerPlantPrice> steamPowerPlantPrice;

			// algo
			filter = new FilterQuery(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
            if (startDate != null)
            {
                filter.AddFilter(Field.StartDate, FilterOperator.Equals, startDate);
            }
            if (endDate != null)
            {
                filter.AddFilter(Field.EndDate, FilterOperator.Equals, endDate);
            }
            steamPowerPlantPrice = FindAll(filter);

			return steamPowerPlantPrice;
		}

		public SteamPowerPlantPrice GetPrevPrice(int powerPlantId, DateTime periode)
		{
			// kamus
			FilterQuery filter;
			SteamPowerPlantPrice steamPowerPlantPrice;

			periode = periode.AddYears(-1);

			// algo
			filter = new FilterQuery(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
			filter.AddFilter(Field.Year, FilterOperator.Equals, periode.Year);

			steamPowerPlantPrice = Find(filter);

			return steamPowerPlantPrice;
		}

		public SteamPowerPlantPrice GetPrice(int powerPlantId, DateTime periode)
		{
			// kamus
			FilterQuery filter;
			SteamPowerPlantPrice steamPowerPlantPrice;

			// algo
			filter = new FilterQuery(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
			filter.AddFilter(Field.Year, FilterOperator.Equals, periode.Year);

            steamPowerPlantPrice = Find(filter);

			return steamPowerPlantPrice;
		}

		public async Task<int> GetLastFormSequence()
		{
			FilterQuery filter = new FilterQuery(Field.FormSequence, FilterOperator.IsNotNull);
			return await CountAsync(filter);
		}
	}
}
