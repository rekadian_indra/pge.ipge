﻿using Business.Abstract;
using Business.Entities.Views;
using System.Collections.Generic;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewPpiRepository : EFBaseRepository<ViewPpi>, IViewPpiRepository
    {
        public EFViewPpiRepository() : base(true)
        {

        }

        protected override IQueryable<ViewPpi> GenerateView()
        {
			return Context.PPIs
				.Where(x => !x.IsDeleted)
				.SelectMany(x => new List<ViewPpi>()
				{
					new ViewPpi()
					{
						Id = x.Id,
						PowerPlantId = x.PowerPlantId,
						Year = x.QuarterNum < 4 ? x.Year : x.Year + 1,
						Month = ((x.QuarterNum * 3) % 12) + 1,
						Price = x.NextPrice
					},
					new ViewPpi()
					{
						Id = x.Id,
						PowerPlantId = x.PowerPlantId,
						Year = x.QuarterNum < 4 ? x.Year : x.Year + 1,
						Month = ((x.QuarterNum * 3) % 12) + 2,
						Price = x.NextPrice
					},
					new ViewPpi()
					{
						Id = x.Id,
						PowerPlantId = x.PowerPlantId,
						Year = x.QuarterNum < 4 ? x.Year : x.Year + 1,
						Month = ((x.QuarterNum * 3) % 12) + 3,
						Price = x.NextPrice
					}
				}).Distinct();
        }
    }
}
