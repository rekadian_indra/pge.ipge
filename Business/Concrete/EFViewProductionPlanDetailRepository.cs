﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewProductionPlanDetailRepository : EFBaseRepository<ViewProductionPlanDetail>, IViewProductionPlanDetailRepository
    {
        public EFViewProductionPlanDetailRepository() : base(true)
        {

        }

        protected override IQueryable<ViewProductionPlanDetail> GenerateView()
        {
			return Context.ProductionPlanDetails
				.Where(x => !x.ProductionPlan.IsDeleted)
				.Select(x => new ViewProductionPlanDetail
				{
					Id = x.Id,
					PowerPlantId = x.ProductionPlan.PowerPlantId,
					Year = x.ProductionPlan.Year,
					Month = x.Month,
					UtilType = x.ProductionPlan.UtilType,
					Value = x.Value
				}).Distinct();
        }
    }
}
