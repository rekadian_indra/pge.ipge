﻿using Business.Abstract;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFViewReliabilityPlanRepository : EFBaseRepository<ViewReliabilityPlan>, IViewReliabilityPlanRepository
	{
        public EFViewReliabilityPlanRepository() : base(true)
        {

        }

        public Task<List<ViewReliabilityPlan>> FindAllUntilPeriodeYearlyAsync(int year, int month)
        {
            FilterQuery filter;

            DateTime startDate = new DateTime(year, 1, 1);
            DateTime endDate = new DateTime(year, month, 1);
            endDate = endDate.AddMonths(1).AddDays(-1);

            filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
            filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);

            return FindAllAsync(filter);
        }

        public Task<List<ViewReliabilityPlan>> FindAllByPeriodeAsync(int year, int month)
		{
			FilterQuery filter;

			DateTime startDate = new DateTime(year, month, 1);
			DateTime endDate = startDate.AddMonths(1);

			filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
			filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);

			return FindAllAsync(filter);
		}

        protected override IQueryable<ViewReliabilityPlan> GenerateView()
        {
			return Context.ReliabilityPlans
				.Where(x => !x.IsDeleted)
				//.ToList()
				.Select(x => new ViewReliabilityPlan
				{
					Id = x.Id,
					PowerPlantId = x.PowerPlantId,
					Periode = x.Periode,
					Year = x.Periode.Year,
					Month = x.Periode.Month,
					FOH = x.FOH,
					MOH = x.MOH,
					POH = x.POH,
					EFDH = x.EFDH,
					EMDH = x.EMDH,
					EPDH = x.EPDH,
					//PH = 744// DateTime.DaysInMonth(x.Periode.Year, x.Periode.Month) * 24
				})
				//.AsQueryable()
				//.Select(x => new ViewReliabilityPlan
				//{
				//	Id = x.Id,
				//	PowerPlantId = x.PowerPlantId,
				//	Periode = x.Periode,
				//	FOH = x.FOH,
				//	MOH = x.MOH,
				//	POH = x.POH,
				//	EFDH = x.EFDH,
				//	EMDH = x.EMDH,
				//	EPDH = x.EPDH,
				//	PH = x.PH
				//})
				//.Select(x => new
				//{
				//	Id = x.Id,
				//	PowerPlantId = x.PowerPlantId,
				//	Periode = x.Periode,
				//	FOH = x.FOH,
				//	MOH = x.MOH,
				//	POH = x.POH,
				//	EFDH = x.EFDH,
				//	EMDH = x.EMDH,
				//	EPDH = x.EPDH,
				//	PH = x.PH,
				//	FOHValue = x.FOH * x.PH,
				//	MOHValue = x.MOH * x.PH,
				//	POHValue = x.POH * x.PH
				//})
				//.Select(x => new
				//{
				//	Id = x.Id,
				//	PowerPlantId = x.PowerPlantId,
				//	Periode = x.Periode,
				//	FOH = x.FOH,
				//	MOH = x.MOH,
				//	POH = x.POH,
				//	EFDH = x.EFDH,
				//	EMDH = x.EMDH,
				//	EPDH = x.EPDH,
				//	PH = x.PH,
				//	FOHValue = x.FOHValue,
				//	MOHValue = x.MOHValue,
				//	POHValue = x.POHValue,
				//	AH = x.PH - x.FOHValue - x.MOHValue - x.POHValue
				//})
				//.Select(x => new
				//{
				//	Id = x.Id,
				//	PowerPlantId = x.PowerPlantId,
				//	Periode = x.Periode,
				//	FOH = x.FOH,
				//	MOH = x.MOH,
				//	POH = x.POH,
				//	EFDH = x.EFDH,
				//	EMDH = x.EMDH,
				//	EPDH = x.EPDH,
				//	PH = x.PH,
				//	FOHValue = x.FOHValue,
				//	MOHValue = x.MOHValue,
				//	POHValue = x.POHValue,
				//	AH = x.AH,
				//	EFDHValue = x.EFDH * x.AH,
				//	EMDHValue = x.EMDH * x.AH,
				//	EPDHValue = x.EPDH * x.AH,
				//})
				//.Select(x => new ViewReliabilityPlan
				//{
				//	Id = x.Id,
				//	PowerPlantId = x.PowerPlantId,
				//	Periode = x.Periode,
				//	FOH = x.FOH,
				//	MOH = x.MOH,
				//	POH = x.POH,
				//	EFDH = x.EFDH,
				//	EMDH = x.EMDH,
				//	EPDH = x.EPDH,
				//	PH = x.PH,
				//	FOHValue = x.FOHValue,
				//	MOHValue = x.MOHValue,
				//	POHValue = x.POHValue,
				//	AH = x.AH,
				//	EFDHValue = x.EFDHValue,
				//	EMDHValue = x.EMDHValue,
				//	EPDHValue = x.EPDHValue,
				//	EFOR = (x.FOHValue + x.EFDHValue) / (x.FOHValue + x.PH),
				//	EAF = (x.AH - x.EFDHValue - x.EMDHValue - x.EPDHValue) / x.PH
				//})
				.Distinct();
        }
    }
}
