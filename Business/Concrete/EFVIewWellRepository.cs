﻿using Business.Abstract;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFViewWellRepository : EFBaseRepository<ViewWell>, IViewWellRepository
    {
        public EFViewWellRepository() : base(true)
        {

        }

        protected override IQueryable<ViewWell> GenerateView()
        {
            return Context.WellHistories
                .Select(o => new ViewWell
                {
                    Id = o.Id,
                    WellId = o.WellId,
                    WellName = o.Well != null ? o.Well.Name : "",
                    AreaId = o.Well != null && o.Well.AreaId.HasValue ? o.Well.AreaId.Value : 0,
                    ClusterName = o.Well != null && o.Well.Cluster != null ? o.Well.Cluster.Name : "",
                    Status = o.Status,
                    LastHistory = Context.WellHistories.Where(n => n.WellId == o.WellId && n.IsDeleted == false).
                                        OrderByDescending(n => n.HistoryDateTimeUtc).AsQueryable().FirstOrDefault(),
                    StartDate = o.HistoryDateTimeUtc,
                    EndDate = Context.WellHistories.Where(n => n.WellId == o.WellId && n.IsDeleted == false).
                                        OrderByDescending(n => n.HistoryDateTimeUtc).AsQueryable().FirstOrDefault().Id == o.Id ?
                                        DbFunctions.AddDays(DateTime.Now, 1).Value :
                                        Context.WellHistories.Where(n => n.WellId == o.WellId && n.IsDeleted == false).
                                            OrderByDescending(n => n.HistoryDateTimeUtc).AsQueryable().FirstOrDefault().HistoryDateTimeUtc,
                    IsDeleted = o.IsDeleted,
                });
        }

        public List<ViewWell> FindViewWellByParam(List<SortQuery> sorts, FilterQuery filter)
        {
            IQueryable<ViewWell> viewWellQueryable = GenerateView();
            List<ViewWell> dbObjects = new List<ViewWell>();

            DateTime startDate = DateTime.Now, endDate = DateTime.Now.AddDays(1);

            if (filter != null && filter.Filters.Any())
            {
                FilterQuery f;
                f = filter.Filters.FirstOrDefault(a => a.Field == Field.StartDate);
                if (f != null)
                {
                    startDate = f.Value.ToDateTime();
                    filter.Filters.Remove(f);
                }
                f = filter.Filters.FirstOrDefault(a => a.Field == Field.EndDate);
                if (f != null)
                {
                    endDate = f.Value.ToDateTime();
                    filter.Filters.Remove(f);
                }
            }

            CustomFilterProcess(filter, ref viewWellQueryable);
            FilterSortProcess(sorts, filter, ref viewWellQueryable);

            viewWellQueryable = viewWellQueryable.Where(a =>
                (DbFunctions.TruncateTime(a.StartDate) >= startDate && DbFunctions.TruncateTime(a.StartDate) <= endDate) ||
                (DbFunctions.TruncateTime(a.EndDate) >= startDate && DbFunctions.TruncateTime(a.EndDate) <= endDate) ||
                (startDate >= DbFunctions.TruncateTime(a.StartDate) && startDate <= DbFunctions.TruncateTime(a.EndDate)) ||
                (endDate >= DbFunctions.TruncateTime(a.StartDate) && endDate <= DbFunctions.TruncateTime(a.EndDate))
                );

            if (viewWellQueryable.Any())
            {
                dbObjects = viewWellQueryable.ToList();
            }

            return dbObjects;
        }

        public Task<List<ViewWell>> FindViewWellByParamAsync(List<SortQuery> sorts, FilterQuery filter)
        {
            return Task.FromResult(FindViewWellByParam(sorts, filter));
        }
    }
}
