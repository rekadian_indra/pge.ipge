﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFWellRepository : EFBaseRepository<Well>, IWellRepository
    {
        public EFWellRepository() : base(true)
        {
        }
    }
}
