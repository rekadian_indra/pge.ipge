﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFProductionPlanDetailRepository : EFBaseRepository<Business.Entities.ProductionPlanDetail>, IProductionPlanDetailRepository
	{
        public EFProductionPlanDetailRepository() : base(true)
        {

		}
	}
}
