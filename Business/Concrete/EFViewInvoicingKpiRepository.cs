﻿using Business.Abstract;
using Business.Entities.Views;
using System;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewInvoicingKpiRepository : EFBaseRepository<ViewInvoicingKpi>, IViewInvoicingKpiRepository
    {
        public EFViewInvoicingKpiRepository() : base(true)
        {

        }

        protected override IQueryable<ViewInvoicingKpi> GenerateView()
        {
            return Context.InvoiceApprovals.Where(m => m.WorkflowId == 1020 || m.WorkflowId == 1027)
                .Select(o => new
                {
                    invoiceNo = o.Invoice.InvoiceNo,
                    invoiceDate = o.CreatedDateTimeUtc,
                    salesOrderId = o.Invoice.SalesOrderId,
                    invoiceSentDate = o.Invoice.InvoiceSentDate ?? new DateTime(1000, 1, 1),
                    acceptedDateBy = o.Invoice.AcceptedDateBy ?? new DateTime(1900, 1, 30),
                    targetSentBack = o.Invoice.TargetSendBackDate ?? new DateTime(1900, 1, 30),
                    actualSentBack = o.Invoice.ActualSendBackDate ?? new DateTime(1900, 1, 30),
                    netValue = o.Invoice.SalesOrder.NetValue ?? 0,
                    transmitionPrice = o.Invoice.SalesOrder.PowerPlant.TransmitionPrice ?? 0,
                    qty = o.Invoice.SalesOrder.Quantity ?? 0,
                    salesOrder = o.Invoice.SalesOrder.SalesOrderApprovals.Where(x => x.WorkflowId == 6 || x.WorkflowId == 10)
                        .GroupBy(x => x.SalesOrderId).Select(x => x.OrderByDescending(y => y.CreatedDateTimeUtc)).FirstOrDefault()
                        .Select(h => new
                        {
                            powerPlantName = h.SalesOrder.PowerPlant.Name,
                            areaId = h.SalesOrder.PowerPlant.AreaId,
                            sentDateBA = h.CreatedDateTimeUtc,
                            period = h.SalesOrder.Periode.Value
                        })
                })
                .GroupBy(o => o.salesOrderId)
                .Select(o => o.OrderByDescending(n => n.invoiceDate).FirstOrDefault())
                .Select(o => new ViewInvoicingKpi
                {
                    InvoiceNo = o.invoiceNo,
                    InvoiceDate = o.invoiceDate,
                    SalesOrderId = o.salesOrderId,
                    InvoiceSentDate = o.invoiceSentDate,
                    Name = o.salesOrder.FirstOrDefault().powerPlantName,
                    AreaId = o.salesOrder.FirstOrDefault().areaId.Value,
                    BASentDate = o.salesOrder.FirstOrDefault().sentDateBA,
                    AcceptedDateBy = o.acceptedDateBy,
                    TargetSentBackDate = o.targetSentBack,
                    ActualSentBackDate = o.actualSentBack,
                    NetValue = o.netValue,
                    TransmitionValue = o.transmitionPrice * o.qty,
                    Year = o.salesOrder.FirstOrDefault().period.Year,
                    Month = o.salesOrder.FirstOrDefault().period.Month
                });

        }
    }
}
