﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Concrete
{
	public class EFProductionPlanRepository : EFBaseRepository<Business.Entities.ProductionPlan>, IProductionPlanRepository
	{
		public EFProductionPlanRepository() : base(true)
		{

		}

		public List<ProductionPlan> GetFromPowerPlantId(int? powerPlantId, int? year = null, ProductionUtilization? type = null)
		{
			// kamus
			FilterQuery filter = new FilterQuery();
			List<ProductionPlan> productionPlans;

            // algo
            if (powerPlantId != null) filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
			if (year != null) filter.AddFilter(Field.Year, FilterOperator.Equals, year);
            if (type != null) filter.AddFilter(Field.UtilType, FilterOperator.Equals, type.ToString());

            productionPlans = FindAll(filter);

			return productionPlans;
		}

        public Task<List<ProductionPlan>> GetFromPowerPlantIdAsync(int? powerPlantId, int? year = null, ProductionUtilization? type = null)
        {
            return Task.FromResult(GetFromPowerPlantId(powerPlantId, year, type));
        }
    }
}
