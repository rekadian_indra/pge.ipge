﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;

namespace Business.Concrete
{
    public class EFAdditionalInvoiceFileRepository : EFBaseRepository<AdditionalInvoiceFile>, IAdditionalInvoiceFileRepository
	{
        public EFAdditionalInvoiceFileRepository() : base(true)
        {
		}

		public List<AdditionalInvoiceFile> GetFromFilePath(string filePath)
		{
			// kamus
			FilterQuery filter;
			List<AdditionalInvoiceFile> additionalFiles;

			// algo
			filter = new FilterQuery(Field.FilePath, FilterOperator.Equals, filePath);
			additionalFiles = FindAll(filter);

			return additionalFiles;
		}
	}
}