﻿using Business.Abstract;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFViewCapacityFactorRepository : EFBaseRepository<ViewCapacityFactor>, IViewCapacityFactorRepository
    {
        public EFViewCapacityFactorRepository() : base(true)
        {

        }

        protected override IQueryable<ViewCapacityFactor> GenerateView()
        {
            return Context.ProductionPlans.SelectMany(m => m.ProductionPlanDetails.Where(n => n.ProductionPlanId == m.Id &&
            m.IsDeleted == false &&
            m.UtilType == ProductionUtilization.ELECTRIC.ToString()),
            (m, n) => new
            {
                plan = m,
                detail = n
            })
            .Select(o => new
            {
                areaId = o.plan.PowerPlant.AreaId.Value,
                areaName = o.plan.PowerPlant.Area.Name,
                powerPlantId = o.plan.PowerPlantId,
                powerPlantName = o.plan.PowerPlant.Name,
                year = o.plan.Year,
                month = o.detail.Month,
                value = o.detail.Value,
                material = o.plan.PowerPlant.Material,
                //////////////////////////////////
                soType = o.plan.PowerPlant.SOType
            })
            .Select(o => new
            {
                o.areaId,
                o.areaName,
                o.powerPlantId,
                o.powerPlantName,
                o.year,
                o.month,
                o.value,
                o.soType,
                o.material
            }).GroupBy(o => new { o.areaId, o.areaName, o.powerPlantId, o.powerPlantName, o.year, o.month, o.material,

                /////////
                o.soType })
            .Select(o => new
            {
                o.Key.areaId,
                o.Key.areaName,
                o.Key.powerPlantId,
                o.Key.powerPlantName,
                o.Key.year,
                o.Key.month,
                o.Key.material,
                capcity = o.Sum(x => x.value),
                
                /////////////
                o.Key.soType
            })
            .Select(o => new
            {
                 o.areaId,
                o.areaName,
                o.powerPlantId,
                o.powerPlantName,
                o.year,
                o.month,
                o.capcity,
                pltps = Context.Pltps.Where(m => m.TaskReport.Status == DataStatus.APPROVED.ToString() && m.TaskReport.IsDeleted == false)
                .Select(n => new
                {
                    n.PowerPlantId,
                    n.TaskReport.Periode.Value.Year,
                    n.TaskReport.Periode.Value.Month,
                    n.EnergyNett,
                    n.EnergyGross
                }).GroupBy(n => new { n.PowerPlantId, n.Year, n.Month})
                .Select(n => new
                {
                    n.Key.PowerPlantId, n.Key.Year, n.Key.Month, actual =  o.material == Common.Enums.Material.ELECTRIC_POWER.ToString() ? n.Sum(e => e.EnergyNett) : n.Sum(e => e.EnergyGross)
                }),

                /////////
                o.soType
            })
            .Select(o => new
            {
                o.areaId,
                o.areaName,
                o.powerPlantId,
                o.powerPlantName,
                o.year,
                o.month,
                o.capcity,
                actual = o.pltps.Where(m => m.PowerPlantId == o.powerPlantId && m.Year == o.year && m.Month == o.month).Sum(a => a.actual),

                /////////
                o.soType
            })
            .Select(o => new ViewCapacityFactor
            {
                AreaId = o.areaId,
                AreaName = o.areaName,
                PowerPlantId = o.powerPlantId,
                PowerPlantName = o.powerPlantName,
                Year = o.year,
                Month = o.month,
                Capacity = o.capcity,
                Actual = (o.actual / 1000) ?? 0,

                //////////////////
                SOType = o.soType
            });
        }
    }
}
