﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFPowerPlantRepository : EFBaseRepository<Business.Entities.PowerPlant>, IPowerPlantRepository
    {
        public EFPowerPlantRepository() : base(true)
        {

		}

		public async Task<List<Business.Entities.PowerPlant>> FindAllByMaterial(object material)
		{
			// kamus
			FilterQuery filter;
			List<Business.Entities.PowerPlant> powerPlants;

			// algo
			filter = new FilterQuery(Field.Material, FilterOperator.Equals, material);

			powerPlants = await FindAllAsync(filter);

			return powerPlants;
		}

		public async Task<List<Business.Entities.PowerPlant>> FindAllBySOType(object soType)
		{
			// kamus
			FilterQuery filter;
			List<Business.Entities.PowerPlant> powerPlants;

			// algo
			filter = new FilterQuery(Field.SOType, FilterOperator.Equals, soType);

			powerPlants = await FindAllAsync(filter);

			return powerPlants;
		}

		public async Task<List<Business.Entities.PowerPlant>> FindAllByMaterialSOType(object material, object soType)
		{
			// kamus
			FilterQuery filter;
			List<Business.Entities.PowerPlant> powerPlants;

			// algo
			filter = new FilterQuery(Field.SOType, FilterOperator.Equals, soType);
			filter.AddFilter(Field.Material, FilterOperator.Equals, material);

			powerPlants = await FindAllAsync(filter);

			return powerPlants;
		}

		public async Task<List<Business.Entities.PowerPlant>> FindAllByInvoiceGroup(int invoiceGroup)
		{
			// kamus
			FilterQuery filter;
			List<Business.Entities.PowerPlant> powerPlants;

			// algo
			filter = new FilterQuery(Field.InvoiceGroup, FilterOperator.Equals, invoiceGroup);

			powerPlants = await FindAllAsync(filter);

			return powerPlants;
		}

		public async Task<List<Business.Entities.PowerPlant>> FindAllByArea(int areaId)
		{
			// kamus
			FilterQuery filter;
			List<Business.Entities.PowerPlant> powerPlants;

			// algo
			if (areaId != 0)
			{
				filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, areaId);
			}
			else // kalau search semua area
			{
				filter = null;
			}

			powerPlants = await FindAllAsync(filter);

			return powerPlants;
		}
	}
}
