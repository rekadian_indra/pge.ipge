﻿using Business.Abstract;
using Business.Entities;
using Business.Extension;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFRoleRepository : EFBaseRepository<Role, UmEntities>, IRoleRepository
    {
        public void AddModuleAndAction(string[] modules, string role)
        {
            Role r = EntitySet.Where(x => x.RoleName == role).FirstOrDefault();
            IEnumerable<ModulesInRole> listModule = r.ModulesInRoles;

            foreach (ModulesInRole mInRole in listModule)
            {
                if (mInRole.Actions.Count > 0)
                {
                    var actions = mInRole.Actions.ToList();
                    foreach (var a in actions)
                    {
                        mInRole.Actions.Remove(a);
                    }
                }
            }

            Context.ModulesInRoles.RemoveRange(listModule);
            Context.SaveChanges();

            foreach (string s in modules)
            {
                string[] temp = s.Split(';');
                ModulesInRole mr;
                Guid moduleId = new Guid(temp.First());
                Guid actionId = new Guid(temp.Last());

                ModulesInRole available = Context.ModulesInRoles.Where(x => x.RoleId == r.RoleId).Where(x => x.ModuleId == moduleId).FirstOrDefault();
                Entities.Action a = Context.Actions.Find(actionId);

                if (available != null)
                {
                    if (!available.Actions.Contains(a))
                    {
                        available.Actions.Add(a);
                        Context.SaveChanges();
                    }
                }
                else
                {
                    mr = new ModulesInRole()
                    {
                        Id = Guid.NewGuid(),
                        RoleId = r.RoleId,
                        ModuleId = new Guid(temp.First())
                    };
                    mr.Actions.Add(a);
                    r.ModulesInRoles.Add(mr);
                    Context.SaveChanges();
                }
            }
        }

        public Role FindByName(string roleName) {
            return EntitySet.Where(x => x.RoleName == roleName).FirstOrDefault();
        }

        protected override void CustomFilterProcess(FilterQuery filter, ref IQueryable<Role> queryable)
        {
            //lib
            FilterQuery removedFilter;
            object value;
            FilterOperator @operator;

            //algorithm
            if (filter != null && filter.Filters.Any(m => m.Field == Field.UserId || m.Field == Field.UserName))
            {
                //filter UserId
                removedFilter = filter.RemoveFilter(Field.UserId);

                if (removedFilter.HasValue())
                {
                    value = removedFilter.Value;
                    @operator = removedFilter.Operator.Value;

                    switch (@operator)
                    {
                        case FilterOperator.Equals:
                            queryable = queryable.Where(m => m.Users.Any(n => n.UserId.ToString() == value.ToString()));

                            break;
                        case FilterOperator.NotEquals:
                            queryable = queryable.Where(m => !m.Users.Any(n => n.UserId.ToString() == value.ToString()));

                            break;
                        default:
                            throw new NotSupportedException(@operator.ToString());
                    }
                }

                //filter UserName
                removedFilter = filter.RemoveFilter(Field.UserName);

                if (removedFilter.HasValue())
                {
                    value = removedFilter.Value;
                    @operator = removedFilter.Operator.Value;

                    switch (@operator)
                    {
                        case FilterOperator.Equals:
                            queryable = queryable.Where(m => m.Users.Any(n => n.UserName == value.ToString()));

                            break;
                        case FilterOperator.NotEquals:
                            queryable = queryable.Where(m => !m.Users.Any(n => n.UserName == value.ToString()));

                            break;
                        default:
                            throw new NotSupportedException(@operator.ToString());
                    }
                }
            }
        }

        public void AssignModule(Role role, List<Module> modules)
        {
            if (role == null)
                throw new ArgumentNullException(typeof(Role).Name);

            if (modules == null)
                throw new ArgumentNullException(typeof(Module).Name);

            if (modules.Any())
            {
                foreach (Module m in modules)
                {
                    Module module = Context.Modules.Find(m.ModuleId);

                    if (module != null)
                    {
                        role.ModulesInRoles.Add(new ModulesInRole { Id = Guid.NewGuid(), RoleId = role.RoleId, ModuleId = module.ModuleId });
                    }
                }

                Context.SaveChanges();
            }
        }

        public void RevokeModule(Role role, List<Module> modules)
        {
            if (role == null)
                throw new ArgumentNullException(typeof(Role).Name);

            if (modules == null)
                throw new ArgumentNullException(typeof(Module).Name);

            if (modules.Any() && role.ModulesInRoles.Any())
            {
                foreach (Module m in modules)
                {
                    ModulesInRole module = role.ModulesInRoles.FirstOrDefault(n => n.ModuleId == m.ModuleId);

                    if (module != null)
                    {
                        Context.ModulesInRoles.Remove(module);
                    }
                }

                Context.SaveChanges();
            }
        }

        public override bool Delete(Role dbItem)
        {
            if (dbItem == null)
                throw new ArgumentNullException();

            //clear ModulesInRole
            if (dbItem.ModulesInRoles.Any())
                Context.ModulesInRoles.RemoveRange(dbItem.ModulesInRoles);

            //clear UsersInRoles
            dbItem.Users.Clear();

            return base.Delete(dbItem);
        }

        public Task AssignModuleAsync(Role role, List<Module> modules)
        {
            AssignModule(role, modules);

            return Task.FromResult(0);
        }

        public Task RevokeModuleAsync(Role role, List<Module> modules)
        {
            RevokeModule(role, modules);

            return Task.FromResult(0);
        }
    }
}
