﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;
using Common.Enums;
using Business.Infrastructure;

namespace Business.Concrete
{
    public class EFSalesOrderApprovalRepository : EFBaseRepository<SalesOrderApproval>, ISalesOrderApprovalRepository
	{
        public EFSalesOrderApprovalRepository() : base(true)
        {
        }

		public async Task<SalesOrderApproval> GetLastApproval(int salesOrderId)
		{
			// kamus
			FilterQuery filter;
			SortQuery sort;
			List<SortQuery> sorts = new List<SortQuery>();
			SalesOrderApproval salesOrderApproval;

			// algo
			filter = new FilterQuery(Field.SalesOrderId, FilterOperator.Equals, salesOrderId);
			sort = new SortQuery(Field.CreatedDateTimeUtc, SortOrder.Descending);
			sorts.Add(sort);
			salesOrderApproval = await FindAsync(sorts, filter);

			return salesOrderApproval;
		}
	}
}