﻿using Business.Abstract;
using Business.Entities.Views;
using Common.Enums;
using System.Data.Entity;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewInvoicingRepository : EFBaseRepository<ViewInvoicing>, IViewInvoicingRepository
    {
        public EFViewInvoicingRepository() : base(true)
        {

        }

        protected override IQueryable<ViewInvoicing> GenerateView()
        {
            var data = Context.Pltps.Where(n => !n.IsDeleted)
                            .GroupJoin(Context.SalesOrders.Where(n => !n.IsDeleted && n.Workflow.Code == WorkflowStatus.FINISH.ToString()),
                                p => new { p.PowerPlantId, p.TaskReport.Periode.Value.Year, p.TaskReport.Periode.Value.Month },
                                s => new { s.PowerPlantId, s.Periode.Value.Year, s.Periode.Value.Month },
                                (p, s) => new
                                {
                                    pltp = p,
                                    so = s.GroupJoin(Context.Invoices.Where(n => !n.IsDeleted),
                                            so => so.Id,
                                            i => i.SalesOrderId,
                                            (so, i) => new { order = so, invoice = i.AsQueryable().FirstOrDefault() }).AsQueryable().FirstOrDefault(),
                                })
                                .Select(n => new ViewInvoicing
                                {
                                    AreaId = n.pltp.PowerPlant.AreaId.Value,
                                    AreaName = n.pltp.PowerPlant.Area.Name,
                                    PowerPlantId = n.pltp.PowerPlantId.Value,
                                    PowerPlantName = n.pltp.PowerPlant.Name,
                                    Material = n.pltp.PowerPlant.Material,
                                    Periode = n.pltp.TaskReport != null ? n.pltp.TaskReport.Periode : null,
                                    SalesPeriode = n.so.order.Periode,
                                    Year = n.pltp.TaskReport != null && n.pltp.TaskReport.Periode.HasValue ? n.pltp.TaskReport.Periode.Value.Year : 0,
                                    Month = n.pltp.TaskReport != null && n.pltp.TaskReport.Periode.HasValue ? n.pltp.TaskReport.Periode.Value.Month : 0,
                                    Day = n.pltp.TaskReport != null && n.pltp.TaskReport.Periode.HasValue ? n.pltp.TaskReport.Periode.Value.Day : 0,
                                    Quantity = n.pltp.TaskReport != null && n.so.order.Quantity.HasValue ? n.so.order.Quantity.Value : 0,
                                    NetPrice = n.so.order.NetPrice.HasValue ? n.so.order.NetPrice.Value : 0,
                                    NetValue = n.so.order.NetValue.HasValue ? n.so.order.NetValue.Value : 0,
                                    HasInvoice = n.so.invoice != null && n.so.invoice.Workflow.Code == WorkflowStatus.FINISH.ToString(),
                                    WorkFlowCode = n.so.invoice != null ? n.so.invoice.Workflow.Code : string.Empty
                                });
            return data;
        }
    }
}
