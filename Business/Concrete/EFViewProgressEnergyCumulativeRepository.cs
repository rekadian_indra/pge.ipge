﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Extension;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFViewProgressEnergyCumulativeRepository : EFBaseRepository<ViewProgressEnergy>, IViewProgressEnergyCumulativeRepository
    {
        public EFViewProgressEnergyCumulativeRepository() : base(true)
        {

        }

        protected override IQueryable<ViewProgressEnergy> GenerateView()
        {
            var query = Context.Pltps.Where(n => n.IsDeleted == false)                            
                            .Select(n => new
                            {
                                Pltps = n,
                                AreaId = n.PowerPlant.AreaId,
                                AreaName =  n.PowerPlant.Area.Name,
                                Periode =  n.TaskReport.Periode,
                                PowerPlant = n.PowerPlant,
                                TaskReport = n.TaskReport,
                                Quarter = n.TaskReport.Periode.HasValue ? (n.TaskReport.Periode.Value.Month <= 3 ? 4 :
                                    (n.TaskReport.Periode.Value.Month <= 6 ? 1 : (n.TaskReport.Periode.Value.Month <= 9 ? 2 : 3))) : 0,
                                EnergyGross = n.EnergyGross.HasValue ? n.EnergyGross.Value / 1000 : 0,
                                EnergyNett = n.PowerPlant.Material == Common.Enums.Material.ELECTRIC_POWER.ToString() ? 
                                        (n.EnergyNett.HasValue ? n.EnergyNett.Value / 1000 : 0) : (n.EnergyGross.HasValue ? n.EnergyGross.Value / 1000 : 0),
                                SteamUtilization = n.SteamUtilization.Value,
                                EnergyProductionPlan = n.PowerPlant.ProductionPlans.Where(m => m.IsDeleted == false
                                                && m.UtilType == ProductionUtilization.ELECTRIC.ToString()
                                                && m.Year == n.TaskReport.Periode.Value.Year).AsQueryable().FirstOrDefault(),
                                SteamProductionPlan = n.PowerPlant.ProductionPlans.Where(m => m.IsDeleted == false
                                                && m.UtilType == ProductionUtilization.STEAM.ToString()
                                                && m.Year == n.TaskReport.Periode.Value.Year).AsQueryable().FirstOrDefault(),                                                
                                DayEffective = n.PowerPlant.ProductionPlans.Where(m => m.IsDeleted == false
                                                && m.UtilType == ProductionUtilization.DAYSEFFECTIVE.ToString()
                                                && m.Year == n.TaskReport.Periode.Value.Year).AsQueryable().FirstOrDefault(),
                                SOType = n.PowerPlant.SOType,
                                IsDeleted = n.TaskReport.IsDeleted
                            })
                            .Select(n => new
                            {
                                AreaId = n.AreaId,
                                AreaName = n.AreaName,
                                Periode = n.Periode,
                                Quarter = n.Quarter,
                                Year = n.Quarter == 4 ? n.Periode.Value.Year - 1 : n.Periode.Value.Year,
                                EnergyProductionPlan = n.EnergyProductionPlan,
                                SteamProductionPlan = n.SteamProductionPlan,
                                Pltps = n.Pltps,
                                PowerPlant = n.PowerPlant,
                                TaskReport = n.TaskReport,
                                EnergyPlan = n.EnergyProductionPlan.ProductionPlanDetails.Where(m => m.Month == n.Periode.Value.Month).AsQueryable().FirstOrDefault(),
                                SteamPlan = n.SteamProductionPlan.ProductionPlanDetails.Where(m => m.Month == n.Periode.Value.Month).AsQueryable().FirstOrDefault(),
                                DayEffective = n.DayEffective.ProductionPlanDetails.Where(m => m.Month == n.Periode.Value.Month).AsQueryable().FirstOrDefault(),
                                EnergyGross = n.EnergyGross,
                                EnergyNett = n.EnergyNett,                                
                                SOType = n.SOType
                            })
                            .Select(n => new
                            {
                                AreaId = n.AreaId,
                                AreaName = n.AreaName,
                                Periode = n.Periode,
                                Quarter = n.Quarter,
                                Year = n.Year,
                                EnergyProductionPlan = n.EnergyProductionPlan,
                                SteamProductionPlan = n.SteamProductionPlan,
                                Pltps = n.Pltps,
                                PowerPlant = n.PowerPlant,
                                TaskReport = n.TaskReport,
                                EnergyPlan = n.EnergyPlan,
                                SteamPlan = n.SteamPlan,
                                EnergyGross = n.EnergyGross,
                                EnergyNett = n.EnergyNett,
                                Ppi = n.PowerPlant.PPIs.Where(m => m.IsDeleted == false && m.QuarterNum == n.Quarter && m.Year == n.Year).AsQueryable().FirstOrDefault(),
                                SteamPrice = n.PowerPlant.SteamPowerPlantPrices.Where(m => m.IsDeleted == false &&  DbFunctions.TruncateTime(m.StartDate) <= DbFunctions.TruncateTime(n.Periode.Value) && DbFunctions.TruncateTime(m.EndDate) >= DbFunctions.TruncateTime(n.Periode.Value)).AsQueryable().FirstOrDefault(),
                                DayEffective = n.DayEffective.Value,
                                SOType = n.SOType,
                                IsDeleted = n.TaskReport.IsDeleted
                            })
                            .Select(n => new ViewProgressEnergy
                            {
                                AreaId = n.AreaId,
                                AreaName = n.AreaName,
                                Periode = n.Periode,
                                EnergyGross = n.EnergyGross,
                                EnergyNett = n.EnergyNett,
                                SteamUtilization = n.Pltps.SteamUtilization.Value,
                                EnergyPlan = n.EnergyPlan != null ? n.EnergyPlan.Value : 0,
                                SteamPlan = n.SteamPlan != null ? n.SteamPlan.Value : 0,
                                EnergyPrice = n.Ppi != null ? n.Ppi.NextPrice : 0,
                                SteamPrice = n.SteamPrice != null ? n.SteamPrice.Price : 0,
                                Material = n.PowerPlant.Material,
                                PowerPlant = n.PowerPlant.Name,
                                Quarter = n.Quarter,
                                QuarterYear = n.Year,
                                Year = n.Periode.Value.Year,
                                PowerPlantId = n.PowerPlant.Id,
                                DayEffective = n.DayEffective,
                                SOType = n.SOType,
                                Month = n.Periode.Value.Month,
                                EnergyNettPriceNett = n.Ppi != null ? (n.Ppi.NextPrice * 1000) * n.EnergyNett : 0,
                                EnergyGrossPriceNett = n.Ppi != null ? (n.Ppi.NextPrice * 1000) * n.EnergyGross : 0,
                                IsDeleted = n.TaskReport.IsDeleted
                            }).Distinct();            

            return query;

        }
    }
}
