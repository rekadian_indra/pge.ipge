﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFPltpRepository : EFBaseRepository<Pltp>, IPltpRepository
    {
        public EFPltpRepository() : base(true)
        {
		}

		public async Task<List<Pltp>> FindAllByPowerPlant(int powerPlantId, int? year=null, int? month=null)
		{
			// kamus
			List<Business.Entities.Pltp> pltps;

			// algo
			FilterQuery filter = new FilterQuery(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
			pltps = await FindAllAsync(filter);
			pltps = pltps.Where(pltp =>
				(pltp.TaskReportId != null) &&
				(!year.HasValue || pltp.TaskReport.Periode.Value.Year == year) &&
				(!month.HasValue || pltp.TaskReport.Periode.Value.Month == month) &&
				(!pltp.TaskReport.IsDeleted)
			).ToList();

			return pltps;
		}

		public async Task<List<Pltp>> FindAllByPeriodeArea(DateTime date, int areaId)
		{
			// kamus
			List<Business.Entities.Pltp> pltps;

			// algo
			pltps = await FindAllAsync();
			pltps = pltps.Where(pltp =>
				(pltp.TaskReportId != null) &&
				((areaId == 0) || (pltp.TaskReport.AreaId == areaId)) &&
				(pltp.TaskReport.Periode == date) &&
				(!pltp.TaskReport.IsDeleted)
			).ToList();

			return pltps;
		}
	}
}
