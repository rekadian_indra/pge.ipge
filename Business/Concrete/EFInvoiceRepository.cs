﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;

namespace Business.Concrete
{
    public class EFInvoiceRepository : EFBaseRepository<Invoice>, IInvoiceRepository
	{
        public EFInvoiceRepository() : base(true)
        {
        }

		public Task<Invoice> FindBySalesOrderIdAsync(int salesOrderId)
		{
			// kamus
			FilterQuery filter;

			// algo
			filter = new FilterQuery(Field.SalesOrderId, FilterOperator.Equals, salesOrderId);
			return FindAsync(filter);
		}
		
		public async Task<int> GetLastInvoiceSequence()
		{
			FilterQuery filter = new FilterQuery(Field.InvoiceSequence, FilterOperator.IsNotNull);
			return await CountAsync(filter);
		}
	}
}