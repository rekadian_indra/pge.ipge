﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFInterfacePointRepository : EFBaseRepository<InterfacePoint>, IInterfacePointRepository
    {
        public EFInterfacePointRepository() : base(true)
        {
        }
    }
}
