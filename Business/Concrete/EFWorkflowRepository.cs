﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;

namespace Business.Concrete
{
    public class EFWorkflowRepository : EFBaseRepository<Workflow>, IWorkflowRepository
    {
        public EFWorkflowRepository() : base()
        {
        }

		public async Task<List<Workflow>> FindByTypeAsync(object type)
		{
			FilterQuery filter;
			List<SortQuery> sorts = new List<SortQuery>();
			SortQuery sort;

			filter = new FilterQuery(Common.Enums.Field.WorkflowType, Common.Enums.FilterOperator.Equals, type);
			sort = new SortQuery(Common.Enums.Field.Id, Common.Enums.SortOrder.Ascending);
			sorts.Add(sort);

			return await FindAllAsync(sorts, filter);
		}


		public override Task<List<Workflow>> FindAllAsync(List<SortQuery> sorts, FilterQuery filter)
		{
			List<Workflow> workflows = FindAll(sorts, filter);
			workflows.Sort((a, b) => Compare(a, b));
			return Task.FromResult(workflows);
		}

		public int Compare(Workflow a, Workflow b)
		{
			if (a.Id == b.PreviousStep)
				return -1;
			if (a.PreviousStep == b.Id)
				return 1;

			return 0;
		}

		public Task<Workflow> FindNextAsync(int id)
		{
			FilterQuery filter;

			filter = new FilterQuery(Common.Enums.Field.PreviousStep, Common.Enums.FilterOperator.Equals, id);
			return FindAsync(filter);
		}

		public Task<Workflow> FindFirstAsync(object type)
		{
			FilterQuery filter;

			filter = new FilterQuery(Common.Enums.Field.WorkflowType, Common.Enums.FilterOperator.Equals, type);
			filter.AddFilter(Common.Enums.Field.PreviousStep, Common.Enums.FilterOperator.Equals, null);

			return FindAsync(filter);
		}

		public Task<Workflow> FindLastAsync(object type)
		{
			FilterQuery filter;

			filter = new FilterQuery(Common.Enums.Field.WorkflowType, Common.Enums.FilterOperator.Equals, type);
			filter.AddFilter(Common.Enums.Field.Approver, Common.Enums.FilterOperator.Equals, null);

			return FindAsync(filter);
		}
	}
}