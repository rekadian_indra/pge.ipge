﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFPpiRepository : EFBaseRepository<PPI>, IPpiRepository
    {
        public EFPpiRepository() : base(true)
        {
        }
		
		public List<PPI> GetFromPowerPlantId(int powerPlantId, int? year = null, byte? quarterNum = null)
		{
			// kamus
			FilterQuery filter;
			List<PPI> ppis;

			// algo
			filter = new FilterQuery(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
			if (year != null) filter.AddFilter(Field.Year, FilterOperator.Equals, year);
			if (quarterNum != null) filter.AddFilter(Field.QuarterNum, FilterOperator.Equals, quarterNum);

			ppis = FindAll(filter);

			return ppis;
		}

		public PPI GetPrice(int powerPlantId, DateTime periode)
		{
			// kamus
			FilterQuery filter;
			PPI ppi;

			periode = periode.AddMonths(-3);

			// algo
			filter = new FilterQuery(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
			filter.AddFilter(Field.Year, FilterOperator.Equals, periode.Year);
			filter.AddFilter(Field.QuarterNum, FilterOperator.Equals, Math.Ceiling((decimal)periode.Month / 3));

			ppi = Find(filter);

			return ppi;
		}

		public async Task<int> GetLastFormSequence()
		{
			FilterQuery filter = new FilterQuery(Field.FormSequence, FilterOperator.IsNotNull);
			return await CountAsync(filter);
		}
	}
}
