﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;

namespace Business.Concrete
{
    public class EFScreenshotSapRepository : EFBaseRepository<ScreenshotSap>, IScreenshotSapRepository
	{
        public EFScreenshotSapRepository() : base(true)
        {
		}
		
		public List<ScreenshotSap> GetFromFilePath(string filePath)
		{
			// kamus
			FilterQuery filter;
			List<ScreenshotSap> ScreenshotSaps;

			// algo
			filter = new FilterQuery(Field.FilePath, FilterOperator.Equals, filePath);
			ScreenshotSaps = FindAll(filter);

			return ScreenshotSaps;
		}
	}
}
