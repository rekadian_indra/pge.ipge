﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFReInjectionRepository : EFBaseRepository<ReInjection>, IReInjectionRepository
    {
        public EFReInjectionRepository() : base(true)
        {
		}

		public async Task<List<ReInjection>> FindAllByPeriodeArea(DateTime date, int areaId)
		{
			// kamus
			List<Business.Entities.ReInjection> reinjections;

			// algo
			reinjections = await FindAllAsync();
			reinjections = reinjections.Where(reinjection =>
				(reinjection.TaskReportId != null) &&
				((areaId == 0) || (reinjection.TaskReport.AreaId == areaId)) &&
				(reinjection.TaskReport.Periode == date) &&
				(!reinjection.TaskReport.IsDeleted)
			).ToList();

			return reinjections;
		}
	}
}