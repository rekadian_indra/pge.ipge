﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;

namespace Business.Concrete
{
    public class EFInvoiceFileRepository : EFBaseRepository<InvoiceFile>, IInvoiceFileRepository
	{
        public EFInvoiceFileRepository() : base(true)
        {
		}

		public List<InvoiceFile> GetFromFilePath(string filePath)
		{
			// kamus
			FilterQuery filter;
			List<InvoiceFile> invoiceFiles;

			// algo
			filter = new FilterQuery(Field.FilePath, FilterOperator.Equals, filePath);
			invoiceFiles = FindAll(filter);

			return invoiceFiles;
		}
	}
}