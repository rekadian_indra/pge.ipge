﻿using Business.Abstract;
using Business.Entities.Views;
using Common.Enums;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewSalesOrderFinanceRepository : EFBaseRepository<ViewSalesOrderFinance>, IViewSalesOrderFinanceRepository
    {
        public EFViewSalesOrderFinanceRepository() : base(true)
        {

        }

        protected override IQueryable<ViewSalesOrderFinance> GenerateView()
        {
            var data = Context.SalesOrders.SelectMany(m => m.Invoices.Where(n => m.Id == n.SalesOrderId && m.IsDeleted == false), (m, n) => new
                        {
                            salesOrder = m,
                            invoice = n
                        })
                        .Select(n => new 
                        {
                            areaId = n.salesOrder.PowerPlant.AreaId.Value,
                            areaName = n.salesOrder.PowerPlant.Area.Name,
                            powerPlantId = n.salesOrder.PowerPlantId.Value,
                            powerPlantName = n.salesOrder.PowerPlant.Name,
                            netPrice = n.salesOrder.NetPrice.Value,
                            hasInvoice = n.invoice != null,
                            workFlowCode = n.invoice != null ? n.invoice.Workflow.Code : string.Empty,
                            status = n.invoice.Workflow.Code,
                            periode = n.salesOrder.Periode,
                            pltp = Context.Pltps.Where(m => m.PowerPlantId == n.salesOrder.PowerPlantId && m.TaskReport.IsDeleted == false)
                                .Select(p => new
                                {
                                    quantity = n.salesOrder.PowerPlant.Material == Material.ELECTRIC_POWER.ToString() ? p.EnergyNett.Value : p.EnergyGross.Value,
                                })
                        })
                         .Select(x => new ViewSalesOrderFinance
                         {
                             AreaId = x.areaId,
                             AreaName = x.areaName,
                             PowerPlantId = x.powerPlantId,
                             PowerPlantName =x.powerPlantName,
                             Periode = x.periode.Value,
                             Quantity = x.pltp.FirstOrDefault().quantity,
                             NetPrice = x.netPrice,
                             HasInvoice = x.hasInvoice,
                             WorkFlowCode = x.workFlowCode,
                             Status = x.status
                         });

            //Context.Invoices.Where(i => i.IsDeleted == false)
            //            .Join(
            //                    Context.SalesOrders.Where(s => s.IsDeleted == false && s.PowerPlant.IsDeleted == false),
            //                    i => i.SalesOrderId,
            //                    s => s.Id,
            //                    (i, s) => new { invoice = i, salesOrder = s }
            //                )
            //            .Join(
            //                    Context.Pltps.Where(p => p.IsDeleted == false && p.TaskReport.IsDeleted == false),
            //                    si => si.salesOrder.PowerPlantId,
            //                    p => p.PowerPlantId,
            //                    (si, p) => new { item = si, pltp = p }
            //                   )
            //             .Select(n => new ViewSalesOrderFinance
            //             {
            //                 AreaId = n.item.salesOrder.PowerPlant.AreaId.Value,
            //                 AreaName = n.item.salesOrder.PowerPlant.Area.Name,
            //                 PowerPlantId = n.item.salesOrder.PowerPlantId.Value,
            //                 PowerPlantName = n.item.salesOrder.PowerPlant.Name,
            //                 Periode = n.pltp.TaskReport.Periode.Value,
            //                 Quantity = n.item.salesOrder.PowerPlant.Material == Material.ELECTRIC_POWER.ToString() ? n.pltp.EnergyNett.Value : n.pltp.EnergyGross.Value,
            //                 NetPrice = n.item.salesOrder.NetPrice.Value,
            //                 HasInvoice = n.item.invoice != null,
            //                 WorkFlowCode = n.item.invoice != null ? n.item.invoice.Workflow.Code : string.Empty,
            //                 Status = n.item.invoice.Workflow.Code
            //             });
            return data;
        }
    }
}
