﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFAirportRepository : EFBaseRepository<Airport>, IAirportRepository
    {
        public EFAirportRepository() : base(true)
        {
        }
    }
}
