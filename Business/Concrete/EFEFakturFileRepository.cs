﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;

namespace Business.Concrete
{
    public class EFEFakturFileRepository : EFBaseRepository<EFakturFile>, IEFakturFileRepository
	{
        public EFEFakturFileRepository() : base(true)
        {
		}

		public List<EFakturFile> GetFromFilePath(string filePath)
		{
			// kamus
			FilterQuery filter;
			List<EFakturFile> eFakturFiles;

			// algo
			filter = new FilterQuery(Field.FilePath, FilterOperator.Equals, filePath);
			eFakturFiles = FindAll(filter);

			return eFakturFiles;
		}
	}
}