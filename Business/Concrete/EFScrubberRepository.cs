﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFScrubberRepository : EFBaseRepository<Scrubber>, IScrubberRepository
    {
        public EFScrubberRepository() : base(true)
        {
        }
    }
}
