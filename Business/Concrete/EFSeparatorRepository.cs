﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFSeparatorRepository : EFBaseRepository<Separator>, ISeparatorRepository
    {
        public EFSeparatorRepository() : base(true)
        {
        }
    }
}