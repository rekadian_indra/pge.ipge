﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFProductionRepository : EFBaseRepository<Production>, IProductionRepository
    {
        public EFProductionRepository() : base(true)
        {
		}

		public async Task<List<Production>> FindAllByPeriodeArea(DateTime date, int areaId)
		{
			// kamus
			List<Business.Entities.Production> productions;

			// algo
			productions = await FindAllAsync();
			productions = productions.Where(production =>
				(production.TaskReportId != null) &&
				((areaId == 0) || (production.TaskReport.AreaId == areaId)) &&
				(production.TaskReport.Periode == date) &&
				(!production.TaskReport.IsDeleted)
			).ToList();

			return productions;
		}
	}
}