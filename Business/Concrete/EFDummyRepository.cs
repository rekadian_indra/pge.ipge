﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using System.Linq;

namespace Business.Concrete
{
    public class EFDummyRepository : EFBaseRepository<DummyTable>, IDummyRepository
    {
        protected override void CustomFilterProcess(FilterQuery filters, ref IQueryable<DummyTable> queryable)
        {
            //kamus
            //string customFilter;

            //FilterInfo nextEventFilter = null;

            //algoritma
            //if (filters != null)
            //{
            //    //menghapus field NextEventId dari filter info
            //    if (filters.Filters != null && filters.Filters.Any(m => m.Field == "NextEventId"))
            //    {
            //        nextEventFilter = filters.Filters.FirstOrDefault(m => m.Field == "NextEventId");
            //        filters.Filters.Remove(nextEventFilter);
            //    }
            //}

            ////custom filter
            //if (nextEventFilter != null)
            //{
            //    int? value = int.Parse(nextEventFilter.Value);

            //    //inner join procurement & eventSla
            //    var join = items.SelectMany(m => m.EventSlas.Where(n => n.ProcurementId == m.ProcurementId), (m, n) => new
            //    {
            //        procurement = m,
            //        eventSla = n
            //    });

            //    switch (nextEventFilter.Operator.ToLower())
            //    {
            //        case "eq":
            //        case "==":
            //            items = join.Where(m => m.eventSla.Event.NextEventId == value).Select(m => m.procurement);
            //            break;
            //        case "neq":
            //        case "!=":
            //            items = join.Where(m => m.eventSla.Event.NextEventId != value).Select(m => m.procurement);
            //            break;
            //        default:
            //            break;
            //    }
        }
    }
}
