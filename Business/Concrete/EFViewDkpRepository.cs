﻿using Business.Abstract;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFViewDkpRepository : EFBaseRepository<ViewDkp>, IViewDkpRepository
	{
        public EFViewDkpRepository() : base(true)
        {

        }

		public Task<List<ViewDkp>> FindAllUntilPeriodeYearlyAsync(int year, int month)
		{
			FilterQuery filter;

			DateTime startDate = new DateTime(year, 1, 1);
            DateTime endDate = new DateTime(year, month, 1);
            endDate = endDate.AddMonths(1).AddDays(-1);

            filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
			filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);

			return FindAllAsync(filter);
		}

		public Task<List<ViewDkp>> FindAllByPeriodeAsync(int year, int month)
		{
			FilterQuery filter;

			DateTime startDate = new DateTime(year, month, 1);
			DateTime endDate = startDate.AddMonths(1);

			filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
			filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);

			return FindAllAsync(filter);
		}

        protected override IQueryable<ViewDkp> GenerateView()
        {
            return Context.Dkps
                .Where(x => !x.IsDeleted && x.TaskReport != null && x.TaskReportId.HasValue && x.PowerPlantId.HasValue)
                .Select(x => new ViewDkp
                {
                    Id = x.Id,
                    PowerPlantId = x.PowerPlantId.Value,
                    Periode = x.TaskReport.Periode.Value,
                    Year = x.TaskReport.Periode.Value.Year,
                    Month = x.TaskReport.Periode.Value.Month,
                    PowerPlantDMN = (x.PowerPlant.URC ?? 0) / 1000,
                    Status = x.Status,
                    //StatusCategory = GetStatusCategory(x.Status),
                    StartHour = x.StartHour ?? new System.TimeSpan(0),
                    EndHour = x.EndHour ?? new System.TimeSpan(0),
                    Location = x.Location
					//Duration = (x.StartHour - x.EndHour) ?? new System.TimeSpan(0)
				})
				.Distinct();
        }

		private string GetStatusCategory(string status)
		{
			string category = "test";
			return category;
		}
    }
}
