﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewReliabilityPlanYearlyRepository : EFBaseRepository<ViewReliabilityPlanYearly>, IViewReliabilityPlanYearlyRepository
	{
        public EFViewReliabilityPlanYearlyRepository() : base(true)
        {

        }

        protected override IQueryable<ViewReliabilityPlanYearly> GenerateView()
        {
			return Context.ReliabilityPlans
				.Where(x => !x.IsDeleted)
				.Select(x => new Entities.Views.ViewReliabilityPlanYearly()
				{
					Id = x.PowerPlantId + "-" + x.Periode.Year,
					PowerPlantId = x.PowerPlantId,
					PowerPlantName = x.PowerPlant.Name,
					Year = x.Periode.Year
				})
				.Distinct();
		}
    }
}
