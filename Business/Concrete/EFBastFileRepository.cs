﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;

namespace Business.Concrete
{
    public class EFBastFileRepository : EFBaseRepository<BastFile>, IBastFileRepository
	{
        public EFBastFileRepository() : base(true)
        {
		}
		
		public List<BastFile> GetFromFilePath(string filePath)
		{
			// kamus
			FilterQuery filter;
			List<BastFile> bastFiles;

			// algo
			filter = new FilterQuery(Field.FilePath, FilterOperator.Equals, filePath);
			bastFiles = FindAll(filter);

			return bastFiles;
		}
	}
}
