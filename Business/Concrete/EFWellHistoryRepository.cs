﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFWellHistoryRepository : EFBaseRepository<WellHistory>, IWellHistoryRepository
    {
        public EFWellHistoryRepository() : base(true)
        {
        }
    }
}
