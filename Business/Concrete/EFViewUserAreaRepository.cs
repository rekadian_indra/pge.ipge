﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Extension;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFViewUserAreaRepository : EFBaseRepository<ViewUser, UmEntities>, IViewUserAreaRepository
    {
        public EFViewUserAreaRepository() : base()
        {
        }

        protected override IQueryable<ViewUser> GenerateView()
        {
            var result = Context.ViewUserAreas.Select(n => new ViewUser
            {
                ApplicationId = n.ApplicationId,
                AreaId = n.AreaId,
                Code = n.AreaId.HasValue ? (n.AreaId > 0 ? n.Code : "ALL") : string.Empty,
                Comment = n.Comment,
                CreateDate = n.CreateDate,
                Email = n.Email,
                FailedPasswordAnswerAttemptCount = n.FailedPasswordAnswerAttemptCount,
                FailedPasswordAnswerAttemptWindowsStart = n.FailedPasswordAnswerAttemptWindowsStart,
                FailedPasswordAttemptCount = n.FailedPasswordAttemptCount,
                FailedPasswordAttemptWindowStart = n.FailedPasswordAttemptWindowStart,
                IsAnonymous = n.IsAnonymous,
                IsApproved = n.IsApproved,
                IsLockedOut = n.IsLockedOut,
                LastActivityDate = n.LastActivityDate,
                LastLockoutDate = n.LastLockoutDate,
                LastLoginDate = n.LastLoginDate,
                LastPasswordChangedDate = n.LastPasswordChangedDate,
                Name = n.AreaId.HasValue ? (n.AreaId > 0 ? n.Name : "Semua Area") : null,
                Password = n.Password,
                PasswordAnswer = n.PasswordAnswer,
                PasswordFormat = n.PasswordFormat,
                PasswordQuestion = n.PasswordQuestion,
                PasswordSalt = n.PasswordSalt,                
                UserId = n.UserId,
                UserName = n.UserName
            });

            return result;
        }
    }
}
