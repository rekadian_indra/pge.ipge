﻿using Business.Abstract;
using Business.Entities.Views;
using System;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewMonthlyProductionRepository : EFBaseRepository<ViewMonthlyProduction>, IViewMonthlyProductionRepository
    {
        public EFViewMonthlyProductionRepository() : base(true)
        {

        }

		//protected IQueryable<ViewMonthlyProduction>

		protected IQueryable<ViewMonthlyProduction> GenerateView(int year, int month)
		{
			return Context.Pltps
				//.Select(planDetail => new
				//{
				//	PowerPlant = planDetail.ProductionPlan.PowerPlant,
				//	Month = planDetail.Month,
				//	Year = planDetail.ProductionPlan.Year,
				//	TaskReports = 
				//})
				//.Select(powerPlant => new ViewMonthlyProduction
				//{
				//	PowerPlantId = powerPlant.Id,
				//	PowerPlantName = powerPlant.Name,
				//	Bulan = (new DateTime(year, month, 1)).ToString("MMMM"),
				//	Hari = DateTime.DaysInMonth(year, month).ToString(),

				//	UapPlan = powerPlant.ProductionPlans?.FirstOrDefault(x => (!x.IsDeleted) && (x.Year == year) && (x.UtilType == Common.Enums.ProductionUtilization.STEAM.ToString()))?.ProductionPlanDetails?.Where(x => x.Month == month)?.Sum(x => x.Value) ?? 0,
				//	UapActual = monthlyPltp?.Sum(x => x.SteamUtilization) ?? 0,
				//	ListrikPlanPembangkitan = (powerPlant.ProductionPlans?.FirstOrDefault(x => (!x.IsDeleted) && (x.Year == year) && (x.UtilType == Common.Enums.ProductionUtilization.ELECTRIC.ToString()))?.ProductionPlanDetails?.Where(x => x.Month == month)?.Sum(x => x.Value) ?? 0) / 1000,
				//	ListrikPlanHargaJual = powerPlant.PPIs?.FirstOrDefault(x => (!x.IsDeleted) && (x.QuarterNum == DateHelper.MonthToPrevQuarter(month)) && (x.Year == DateHelper.MonthYearToPrevQuarterYear(month, year)))?.NextPrice ?? 0,
				//})
				.Select(pltp => new ViewMonthlyProduction
				{
					PowerPlantId = pltp.PowerPlantId.Value,
					PowerPlantName = pltp.PowerPlant.Name,

				})
				//.Select(monthlyProduction => new ViewMonthlyProduction
				//{
				//	ListrikPlanDapatUSD = monthlyProduction.ListrikPlanPembangkitan * monthlyProduction.ListrikPlanHargaJual / 1000, // dibagi 1000 soalnya ListrikPlanPembangkitan udah dalam MWh 
				//	ListrikPlanDapatIDR = monthlyProduction.ListrikPlanDapatUSD / 1000 * kursUSDIDR,
				//	ListrikActualPembangkitan = (monthlyPltp?.Sum(x => x.EnergyNett) ?? 0) / 1000,
				//	ListrikActualHargaJual = monthlyProduction.ListrikPlanHargaJual,
				//	ListrikActualDapatUSD = monthlyProduction.ListrikActualPembangkitan * monthlyProduction.ListrikActualHargaJual / 1000,
				//	ListrikActualDapatIDR = monthlyProduction.ListrikActualDapatUSD / 1000 * kursUSDIDR

				//})
				.Distinct();
        }
    }
}
