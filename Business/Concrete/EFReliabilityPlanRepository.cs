﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFReliabilityPlanRepository : EFBaseRepository<ReliabilityPlan>, IReliabilityPlanRepository
	{
        public EFReliabilityPlanRepository() : base(true)
        {

		}

		public Task<List<ReliabilityPlan>> FindAllByPeriodeAsync(int year, int month)
		{
			FilterQuery filter;

			DateTime startDate = new DateTime(year, month, 1);
			DateTime endDate = startDate.AddMonths(1);

			filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
			filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);

			return FindAllAsync(filter);
		}
		
		public List<ReliabilityPlan> GetFromPowerPlantId(int? powerPlantId, int? year = null)
		{
			// kamus
			FilterQuery filter = new FilterQuery();
			List<ReliabilityPlan> reliabilityPlans;

			// algo
			if (powerPlantId != null) filter.AddFilter(Field.PowerPlantId, FilterOperator.Equals, powerPlantId);
			if (year != null) filter.AddFilter(Field.Periode, FilterOperator.GreaterThanOrEquals, new DateTime(year.Value, 1, 1));
			if (year != null) filter.AddFilter(Field.Periode, FilterOperator.LessThan, new DateTime(year.Value + 1, 1, 1));

			reliabilityPlans = FindAll(filter);

			return reliabilityPlans;
		}

		public Task<List<ReliabilityPlan>> GetFromPowerPlantIdAsync(int? powerPlantId, int? year = null)
		{
			return Task.FromResult(GetFromPowerPlantId(powerPlantId, year));
		}
	}
}
