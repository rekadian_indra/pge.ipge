﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFDummyFlagRepository : EFBaseRepository<DummyFlagTable>, IDummyFlagRepository
    {
        public EFDummyFlagRepository() : base(true)
        {
        }
    }
}
