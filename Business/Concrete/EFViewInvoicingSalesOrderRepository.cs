﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewInvoicingSalesOrderRepository : EFBaseRepository<ViewInvoicingSalesOrder>, IViewInvoicingSalesOrderRepository
    {
        public EFViewInvoicingSalesOrderRepository() : base(true)
        {

        }

        protected override IQueryable<ViewInvoicingSalesOrder> GenerateView()
        {
            return Context.SalesOrders.SelectMany(m => m.Invoices.Where(n => m.Id == n.SalesOrderId && m.IsDeleted == false), (m, n) => new
            {
                salesOrder = m,
                invoice = n
            })
            .Select(o => new
            {
                areaId = o.salesOrder.PowerPlant.AreaId,
                powerPlant = o.salesOrder.PowerPlant.Name,
                material = o.salesOrder.PowerPlant.Material,
                year = o.salesOrder.Periode.Value.Year,
                month = o.salesOrder.Periode.Value.Month,
                quantity = o.salesOrder.Quantity,
                netPrice = o.salesOrder.NetPrice,
                netValue = o.salesOrder.NetValue                               
            })
            .GroupBy(o => new { o.areaId, o.powerPlant, o.material, o.year, o.month })
            .Select(o => new ViewInvoicingSalesOrder
            {
                AreaId = o.Key.areaId,
                Name = o.Key.powerPlant,
                Material = o.Key.material,
                Year = o.Key.year,
                Month = o.Key.month,
                Quantity = o.Sum(x => x.quantity),
                NetPrice = o.Sum(x => x.netPrice),
                NetValue = o.Sum(x => x.netValue)
            });
        }
    }
}
