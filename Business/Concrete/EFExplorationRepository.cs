﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using Common.Enums;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFExplorationRepository : EFBaseRepository<Exploration>, IExplorationRepository
    {
        public EFExplorationRepository() : base(true)
        {

		}

		public List<Exploration> GetFromPeriodeArea(DateTime startDate, DateTime endDate, int areaId)
		{
			// kamus
			FilterQuery filter;
			List<Exploration> reports = null;

			// algo
			filter = new FilterQuery(Field.ExplorationDate, FilterOperator.GreaterThanOrEquals, startDate);
			filter.AddFilter(Field.ExplorationDate, FilterOperator.LessThanOrEquals, endDate);
			if (areaId != 0) filter.AddFilter(Field.AreaId, FilterOperator.Equals, areaId);
			reports = FindAll(filter);

			return reports;
		}

		public async Task<List<Exploration>> GetFromPeriodeWell(DateTime startDate, DateTime endDate, int wellId)
		{
			// kamus
			FilterQuery filter;
			List<Exploration> reports = null;

			// algo
			filter = new FilterQuery(Field.ExplorationDate, FilterOperator.GreaterThanOrEquals, startDate);
			filter.AddFilter(Field.ExplorationDate, FilterOperator.LessThanOrEquals, endDate);
			reports = await FindAllAsync(filter);

			return reports?.Where(x => x.Area?.Clusters?.SelectMany(c => c.Wells)?.Any(y => y.Id == wellId) ?? false)?.ToList();
		}
	}
}
