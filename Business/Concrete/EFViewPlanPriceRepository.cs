﻿using Business.Abstract;
using Business.Entities.Views;
using System.Collections.Generic;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewPlanPriceRepository : EFBaseRepository<ViewPlanPrice>, IViewPlanPriceRepository
    {
        public EFViewPlanPriceRepository() : base(true)
        {

        }

        protected override IQueryable<ViewPlanPrice> GenerateView()
        {
            return Context.ProductionPlans
                .GroupJoin(Context.SteamPowerPlantPrices, x=>x.PowerPlantId, y=>y.PowerPlantId,)

			return Context.SteamPowerPlantPrices
				.Where(x => !x.IsDeleted)                
				.SelectMany(x => new List<ViewSteamPrice>()
				{
					new ViewSteamPrice()
					{
						Id = x.Id,
						PowerPlantId = x.PowerPlantId,
						Year = x.sta,
						Month = ((x.QuarterNum * 3) % 12) + 1,
						Price = x.NextPrice
					},
					new ViewSteamPrice()
					{
						Id = x.Id,
						PowerPlantId = x.PowerPlantId,
						Year = x.QuarterNum < 4 ? x.Year : x.Year + 1,
						Month = ((x.QuarterNum * 3) % 12) + 2,
						Price = x.NextPrice
					},
					new ViewSteamPrice()
					{
						Id = x.Id,
						PowerPlantId = x.PowerPlantId,
						Year = x.QuarterNum < 4 ? x.Year : x.Year + 1,
						Month = ((x.QuarterNum * 3) % 12) + 3,
						Price = x.NextPrice
					}
				}).Distinct();
        }
    }
}
