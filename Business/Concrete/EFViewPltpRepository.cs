﻿using Business.Abstract;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFViewPltpRepository : EFBaseRepository<ViewPltp>, IViewPltpRepository
    {
        public EFViewPltpRepository() : base(true)
        {

		}

		//public Task<List<ViewPltp>> FindAllByPeriodeAsync(int year, int month)
		//{
		//	FilterQuery filter;

		//	DateTime startDate = new DateTime(year, month, 1);
		//	DateTime endDate = startDate.AddMonths(1);

		//	filter = new FilterQuery(Field.Periode, FilterOperator.GreaterThanOrEquals, startDate);
		//	filter.AddFilter(Field.Periode, FilterOperator.LessThan, endDate);

		//	return FindAllAsync(filter);
		//}


		protected override IQueryable<ViewPltp> GenerateView()
        {
			return Context.Pltps
				.Where(x => !x.IsDeleted && x.TaskReport != null && x.PowerPlantId.HasValue)
				.Select(x => new ViewPltp
				{
					Id = x.Id,
					PowerPlantId = x.PowerPlantId.Value,
					Material = x.PowerPlant.Material,
					Year = x.TaskReport.Periode.Value.Year,
					Month = x.TaskReport.Periode.Value.Month,
					Periode = x.TaskReport.Periode.Value,
					EnergyGross = x.EnergyGross ?? 0,
					EnergyNett = x.EnergyNett ?? 0,
					SteamUtilization = x.SteamUtilization ?? 0,
					Ssc = x.Ssc ?? 0,
					WasteRockMuffler = x.WasteRockMufller ?? 0,
					Notes = x.Notes,                    
				})
				.Distinct();
        }
    }
}
