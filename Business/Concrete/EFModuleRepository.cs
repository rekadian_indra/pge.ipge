﻿using Business.Abstract;
using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Business.Linq;
using System.Threading.Tasks;
using Business.Infrastructure;
using System.Text.RegularExpressions;

namespace Business.Concrete
{
    public class EFModuleRepository : EFBaseRepository<Module, UmEntities>, IModuleRepository
    {
        //        public Module FindByName(string moduleName)
        //        {
        //            return EntitySet.Where(x => x.ModuleName == moduleName).FirstOrDefault();
        //        }

        //        public void Delete(string moduleName, bool foreignKey)
        //        {
        //            List<Module> delete = new List<Module>();
        //            GetAllChildrenInModule(moduleName, ref delete, 0);

        //            for (int i = 0; i < delete.Count; ++i)
        //            {
        //                if (delete[i].Actions.Count > 0)
        //                {
        //                    delete[i].Actions.Clear();
        //                }

        //                if (!foreignKey)
        //                {
        //                    for (int j = 0; j < delete[i].ModulesInRoles.Count; j++)
        //                    {
        //                        delete[i].ModulesInRoles.ElementAt(j).Actions.Clear();
        //                    }
        //                    Context.ModulesInRoles.RemoveRange(delete[i].ModulesInRoles);
        //                    Context.SaveChanges();
        //                }
        //            }

        //            Context.Modules.RemoveRange(delete);
        //            Context.SaveChanges();
        //        }

        //        public void addAction(Guid moduleId, Guid actionId)
        //        {
        //            Module m = EntitySet.Find(moduleId);
        //            Entities.Action a = Context.Actions.Find(actionId);
        //            if (!m.Actions.Contains(a))
        //            {
        //                m.Actions.Add(a);
        //                Context.SaveChanges();
        //            }
        //        }

        //        public void removeAction(Guid moduleId, Guid actionId)
        //        {
        //            Module m = EntitySet.Find(moduleId);
        //            Entities.Action a = Context.Actions.Find(actionId);
        //            if (m.Actions.Contains(a))
        //            {
        //                m.Actions.Remove(a);
        //                Context.SaveChanges();
        //            }
        //        }

        //        public List<Entities.Action> GetActionsInModule(string moduleName)
        //        {
        //            List<Entities.Action> result = EntitySet.Where(x => x.ModuleName == moduleName).First().Actions.ToList();
        //            for (int i = 0; i < result.Count; ++i)
        //            {
        //                result[i].Modules.Clear();
        //                result[i].ModulesInRoles.Clear();
        //            }
        //            return result;
        //        }

        //        public void GetAllChildrenInModule(string moduleName, ref List<Module> result, int lvl)
        //        {
        //            lvl = lvl + 1;
        //            Module m = EntitySet.Where(x => x.ModuleName == moduleName).FirstOrDefault();
        //            //m.lvl = lvl;
        //            List<Module> childrens = EntitySet.Where(x => x.ParentModule == moduleName).ToList();
        //            if (m != null)
        //            {
        //                result.Add(m);
        //            }

        //            foreach (Module x in childrens)
        //            {
        //                GetAllChildrenInModule(x.ModuleName, ref result, lvl);
        //            }
        //        }

        //public Task<List<Entities.Action>> GetActionsInModuleAsync(string moduleName)
        //{
        //    return Task.FromResult(GetActionsInModule(moduleName));
        //}
        //    }
        //}


        public Task<List<Entities.Action>> GetActionsInModuleAsync(string moduleName)
        {
            return Task.FromResult(GetActionsInModule(moduleName));
        }

        public void Create(Module module)
        {
            Context.Modules.Add(module);
            Context.SaveChanges();
        }

        public void Delete(string moduleName, bool foreignKey)
        {
            List<Module> delete = new List<Module>();
            GetAllChildrenInModule(moduleName, ref delete, 0);

            for (int i = 0; i < delete.Count; ++i)
            {
                if (delete[i].Actions.Count > 0)
                {
                    delete[i].Actions.Clear();
                }

                if (!foreignKey)
                {
                    for (int j = 0; j < delete[i].ModulesInRoles.Count; j++)
                    {
                        delete[i].ModulesInRoles.ElementAt(j).Actions.Clear();
                    }
                    Context.ModulesInRoles.RemoveRange(delete[i].ModulesInRoles);
                    Context.SaveChanges();
                }
            }

            Context.Modules.RemoveRange(delete);
            Context.SaveChanges();
        }

        public List<Module> Find(int skip = 0, int? take = null, List<SortQuery> sortings = null, FilterQuery filters = null, string filterLogic = null)
        {
            IQueryable<Module> modules = Context.Modules;

            if (filters != null && (filters.Filters != null && filters.Filters.Count > 0))
            {
                QueryHelper.FilterProcess(filters, ref modules);
            }

            if (sortings != null && sortings.Count > 0)
            {
                QueryHelper.SortProcess(sortings, ref modules);
            }
            else
            {
                modules = modules.OrderBy("ModuleName asc");
            }

            var takeModules = modules;
            if (take != null)
            {
                takeModules = modules.Skip(skip).Take((int)take);
            }

            List<Module> moduleList = takeModules.ToList();

            return moduleList;
        }

        public Module FindByName(string moduleName)
        {
            return Context.Modules.Where(x => x.ModuleName == moduleName).SingleOrDefault();
        }

        public int Count(List<FilterQuery> filters, string filterLogic)
        {
            throw (new NotImplementedException());
        }

        public Module FindByPk(Guid id)
        {
            return Context.Modules.Find(id);
        }


        public List<Entities.Action> GetActionsInModule(string moduleName)
        {
            List<Entities.Action> result = Context.Modules.Where(x => x.ModuleName == moduleName).First().Actions.ToList();
            for (int i = 0; i < result.Count; ++i)
            {
                result[i].Modules.Clear();
                result[i].ModulesInRoles.Clear();
            }
            return result;
        }

        public string MapSort(string sortOn)
        {
            string mapSortOn = sortOn;
            mapSortOn = Regex.Replace(sortOn, @"(\p{Ll})(\p{Lu})", "$1_$2");

            return mapSortOn;
        }

        public void GetAllChildrenInModule(string moduleName, ref List<Module> result, int lvl)
        {
            lvl = lvl + 1;
            //Module m = Context.Modules.Where(x => x.ModuleName == moduleName).FirstOrDefault();
            ////m.lvl = lvl;
            //List<Module> childrens = Context.Modules.Where(x => x.ParentModule == moduleName).ToList();
            //if (m != null)
            //{
            //    result.Add(m);
            //}

            //foreach (Module x in childrens)
            //{
            //    GetAllChildrenInModule(x.ModuleName, ref result, lvl);
            //}
        }

        public void addAction(Guid moduleId, Guid actionId)
        {
            Module m = Context.Modules.Find(moduleId);
            Entities.Action a = Context.Actions.Find(actionId);
            if (!m.Actions.Contains(a))
            {
                m.Actions.Add(a);
                Context.SaveChanges();
            }
        }

        public void removeAction(Guid moduleId, Guid actionId)
        {
            Module m = Context.Modules.Find(moduleId);
            Entities.Action a = Context.Actions.Find(actionId);
            if (m.Actions.Contains(a))
            {
                m.Actions.Remove(a);
                Context.SaveChanges();
            }
        }
    }
}

