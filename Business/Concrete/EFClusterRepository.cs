﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFClusterRepository : EFBaseRepository<Cluster>, IClusterRepository
    {
        public EFClusterRepository() : base(true)
        {
        }
    }
}
