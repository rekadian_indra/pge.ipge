﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFAreaRepository : EFBaseRepository<Area>, IAreaRepository
    {
        public EFAreaRepository() : base(true)
        {
        }
    }
}
