﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewExplorationRepository : EFBaseRepository<ViewExploration>, IViewExplorationRepository
    {
        public EFViewExplorationRepository() : base(true)
        {

        }

        protected override IQueryable<ViewExploration> GenerateView()
        {
            return Context.Explorations.Select(x => new ViewExploration
            {
                ExplorationDate = x.ExplorationDate,
                AreaId = x.AreaId,
                AreaName = x.Area.Name
            }).Distinct()
            .OrderBy(y => y.ExplorationDate);
        }
    }
}
