﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFProductionPlanDailyRepository : EFBaseRepository<Business.Entities.ProductionPlanDaily>, IProductionPlanDailyRepository
	{
        public EFProductionPlanDailyRepository() : base(true)
        {

		}
	}
}
