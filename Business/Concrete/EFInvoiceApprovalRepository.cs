﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;

namespace Business.Concrete
{
    public class EFInvoiceApprovalRepository : EFBaseRepository<InvoiceApproval>, IInvoiceApprovalRepository
	{
        public EFInvoiceApprovalRepository() : base(false)
        {
		}

		public async Task<InvoiceApproval> GetLastApproval(int invoiceId)
		{
			// kamus
			FilterQuery filter;
			SortQuery sort;
			List<SortQuery> sorts = new List<SortQuery>();
			InvoiceApproval invoiceApproval;

			// algo
			filter = new FilterQuery(Field.InvoiceId, FilterOperator.Equals, invoiceId);
			sort = new SortQuery(Field.CreatedDateTimeUtc, SortOrder.Descending);
			sorts.Add(sort);
			invoiceApproval = await FindAsync(sorts, filter);

			return invoiceApproval;
		}
	}
}