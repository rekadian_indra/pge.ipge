﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;

namespace Business.Concrete
{
    public class EFSteamPowerPlantPriceApprovalRepository : EFBaseRepository<SteamPowerPlantPriceApproval>, ISteamPowerPlantPriceApprovalRepository
	{
        public EFSteamPowerPlantPriceApprovalRepository() : base(true)
        {
		}

		public async Task<SteamPowerPlantPriceApproval> GetLastApproval(int steamPowerPlantPriceId)
		{
			// kamus
			FilterQuery filter;
			SortQuery sort;
			List<SortQuery> sorts = new List<SortQuery>();
			SteamPowerPlantPriceApproval steamPowerPlantPriceApproval;

			// algo
			filter = new FilterQuery(Field.SteamPowerPlantPriceId, FilterOperator.Equals, steamPowerPlantPriceId);
			sort = new SortQuery(Field.CreatedDateTimeUtc, SortOrder.Descending);
			sorts.Add(sort);
			steamPowerPlantPriceApproval = await FindAsync(sorts, filter);

			return steamPowerPlantPriceApproval;
		}
	}
}