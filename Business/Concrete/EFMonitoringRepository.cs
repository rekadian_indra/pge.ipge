﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFMonitoringRepository : EFBaseRepository<Monitoring>, IMonitoringRepository
    {
        public EFMonitoringRepository() : base(true)
        {
		}

		public async Task<List<Monitoring>> FindAllByPeriodeArea(DateTime date, int areaId)
		{
			// kamus
			List<Business.Entities.Monitoring> monitorings;

			// algo
			monitorings = await FindAllAsync();
			monitorings = monitorings.Where(monitoring =>
				(monitoring.TaskReportId != null) &&
				((areaId == 0) || (monitoring.TaskReport.AreaId == areaId)) &&
				(monitoring.TaskReport.Periode == date) &&
				(!monitoring.TaskReport.IsDeleted)
			).ToList();

			return monitorings;
		}
	}
}