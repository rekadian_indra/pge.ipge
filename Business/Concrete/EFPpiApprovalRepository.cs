﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;

namespace Business.Concrete
{
    public class EFPpiApprovalRepository : EFBaseRepository<PpiApproval>, IPpiApprovalRepository
	{
        public EFPpiApprovalRepository() : base(true)
        {
		}

		public async Task<PpiApproval> GetLastApproval(int ppiId)
		{
			// kamus
			FilterQuery filter;
			SortQuery sort;
			List<SortQuery> sorts = new List<SortQuery>();
			PpiApproval ppiApproval;

			// algo
			filter = new FilterQuery(Field.PpiId, FilterOperator.Equals, ppiId);
			sort = new SortQuery(Field.CreatedDateTimeUtc, SortOrder.Descending);
			sorts.Add(sort);
			ppiApproval = await FindAsync(sorts, filter);

			return ppiApproval;
		}
	}
}