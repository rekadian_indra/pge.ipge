﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFDkpREpository : EFBaseRepository<Dkp>, IDkpRepository
    {
        public EFDkpREpository() : base(true)
        {
		}

		public async Task<List<Dkp>> FindAllByPeriodeArea(DateTime date, int areaId)
		{
			// kamus
			List<Business.Entities.Dkp> dkps;

			// algo
			dkps = await FindAllAsync();
			dkps = dkps.Where(dkp =>
				(dkp.TaskReportId != null) &&
				((areaId == 0) || (dkp.TaskReport.AreaId == areaId)) &&
				(dkp.TaskReport.Periode == date) &&
				(!dkp.TaskReport.IsDeleted)
			).ToList();

			return dkps;
		}
	}
}