﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFExplorationDetailRepository : EFBaseRepository<ExplorationDetail>, IExplorationDetailRepository
    {
        public EFExplorationDetailRepository() : base(true)
        {

        }
    }
}
