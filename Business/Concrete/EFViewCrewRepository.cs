﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewCrewRepository : EFBaseRepository<ViewCrew>, IViewCrewRepository
    {
        public EFViewCrewRepository() : base()
        {
        }

        protected override IQueryable<ViewCrew> GenerateView()
        {
            //DefaultIfEmpty() method meaning LEFT OUTER JOIN. Just remove DefaultIfEmpty() method for INNER JOIN
            return Context.Crews.SelectMany(m => m.Companies.Where(n => n.IsDeleted == false && n.Crews.Contains(m)).DefaultIfEmpty(), (m, n) => new
            {
                crew = m,
                company = n
            })
            //Only use parameterless constructor. Entity to LINQ doesnot support constructor with parameter
            .Select(o => new ViewCrew
            {
                CrewId = o.crew.CrewId,
                CrewName = o.crew.CrewName,
                Status = o.crew.Status,
                RegisterDate = o.crew.RegisterDate,
                CompanyId = o.company != null ? o.company.CompanyId : (int?)null,
                CompanyName = o.company != null ? o.company.CompanyName : string.Empty,
                CreatedBy = o.crew.CreatedBy,
                CreatedDateTimeUtc = o.crew.CreatedDateTimeUtc,
                ModifiedBy = o.crew.ModifiedBy,
                ModifiedDateTimeUtc = o.crew.ModifiedDateTimeUtc
            });
        }
    }
}
