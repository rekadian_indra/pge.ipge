﻿using Business.Entities.Views;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
	public interface IViewReliabilityActualRepository : IRepository<ViewReliabilityActual>
	{
		Task<List<ViewReliabilityActual>> FindAllByPeriodeAsync(int year, int month);

	}
}
