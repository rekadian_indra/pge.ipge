﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IPpiRepository : IRepository<PPI>
    {
		List<PPI> GetFromPowerPlantId(int powerPlantId, int? year = null, byte? quarterNum = null);

		PPI GetPrice(int powerPlantId, DateTime periode);

		Task<int> GetLastFormSequence();
	}
}
