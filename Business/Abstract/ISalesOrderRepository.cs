﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface ISalesOrderRepository : IRepository<SalesOrder>
	{
		List<SalesOrder> GetFromPowerPlant(int powerPlantId);

		List<SalesOrder> GetFromPeriode(DateTime? periode, RangeData rangeData = RangeData.MONTHLY);

		List<SalesOrder> GetFromPeriodeSOType(DateTime? periode, string soType, RangeData rangeData = RangeData.MONTHLY);

		List<SalesOrder> GetFromPeriodeSOTypePowerPlant(DateTime? periode, string soType, int powerPlantId, RangeData rangeData = RangeData.MONTHLY);

		List<SalesOrder> GetFromPeriodePowerPlant(DateTime? periode, int powerPlantId, RangeData rangeData = RangeData.MONTHLY);
	}
}
