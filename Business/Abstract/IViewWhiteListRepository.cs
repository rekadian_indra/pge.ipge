﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewWhitelistRepository : IRepository<ViewWhitelist>
    {
    }
}
