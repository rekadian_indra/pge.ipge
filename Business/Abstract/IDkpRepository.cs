﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;

namespace Business.Abstract
{
    public interface IDkpRepository : IRepository<Dkp>
    {
		Task<List<Dkp>> FindAllByPeriodeArea(DateTime date, int areaId);

	}
}
