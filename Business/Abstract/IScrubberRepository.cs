﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IScrubberRepository : IRepository<Scrubber>
    {
    }
}
