﻿using Business.Entities;

namespace Business.Abstract
{
    public interface ICompanyRepository : IRepository<Company>
    {
    }
}
