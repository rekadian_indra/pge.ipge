﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewCrewRepository : IRepository<ViewCrew>
    {
    }
}
