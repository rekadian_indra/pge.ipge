﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewPlanPriceRepository : IRepository<ViewPlanPrice>
    {
    }
}
