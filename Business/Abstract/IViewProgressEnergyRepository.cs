﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewProgressEnergyRepository : IRepository<ViewProgressEnergy>
    {
    }
}
