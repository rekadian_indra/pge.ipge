﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewProductionPlanDetailRepository : IRepository<ViewProductionPlanDetail>
    {
    }
}
