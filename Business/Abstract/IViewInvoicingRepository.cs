﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewInvoicingSalesOrderRepository : IRepository<ViewInvoicingSalesOrder>
    {
    }
}
