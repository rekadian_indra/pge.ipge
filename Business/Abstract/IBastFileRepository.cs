﻿using Business.Entities;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface IBastFileRepository : IRepository<BastFile>
    {
		List<BastFile> GetFromFilePath(string filePath);

	}
}
