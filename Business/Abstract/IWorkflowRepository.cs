﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;

namespace Business.Abstract
{
    public interface IWorkflowRepository : IRepository<Workflow>
    {
		Task<List<Workflow>> FindByTypeAsync(object type);

		Task<Workflow> FindNextAsync(int id);

		Task<Workflow> FindFirstAsync(object type);

		Task<Workflow> FindLastAsync(object type);
	}
}
