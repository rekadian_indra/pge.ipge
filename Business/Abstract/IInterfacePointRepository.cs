﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IInterfacePointRepository : IRepository<InterfacePoint>
    {
    }
}
