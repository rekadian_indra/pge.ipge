﻿using Business.Entities;

namespace Business.Abstract
{
    public interface ISeparatorRepository : IRepository<Separator>
    {
    }
}
