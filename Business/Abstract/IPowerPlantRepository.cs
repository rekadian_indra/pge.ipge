﻿using Business.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IPowerPlantRepository : IRepository<PowerPlant>
    {
		Task<List<Business.Entities.PowerPlant>> FindAllByMaterial(object material);
		Task<List<Business.Entities.PowerPlant>> FindAllBySOType(object soType);
		Task<List<Business.Entities.PowerPlant>> FindAllByMaterialSOType(object material, object soType);
		Task<List<Business.Entities.PowerPlant>> FindAllByInvoiceGroup(int invoiceGroup);
		Task<List<Business.Entities.PowerPlant>> FindAllByArea(int areaId);

	}
}
