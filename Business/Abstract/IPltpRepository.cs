﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IPltpRepository : IRepository<Pltp>
    {
		Task<List<Pltp>> FindAllByPowerPlant(int powerPlantId, int? year=null, int? month=null);
		Task<List<Pltp>> FindAllByPeriodeArea(DateTime date, int areaId);

	}
}
