﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewSalesOrderApprovalRepository : IRepository<ViewSalesOrderApproval>
    {
    }
}
