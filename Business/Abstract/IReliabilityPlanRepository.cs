﻿using Business.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IReliabilityPlanRepository : IRepository<ReliabilityPlan>
    {
		Task<List<ReliabilityPlan>> FindAllByPeriodeAsync(int year, int month);

		List<ReliabilityPlan> GetFromPowerPlantId(int? powerPlantId, int? year = null);

		Task<List<ReliabilityPlan>> GetFromPowerPlantIdAsync(int? powerPlantId, int? year = null);
	}
}
