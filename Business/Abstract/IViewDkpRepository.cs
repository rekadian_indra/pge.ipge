﻿using Business.Entities.Views;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IViewDkpRepository : IRepository<ViewDkp>
    {
        Task<List<ViewDkp>> FindAllUntilPeriodeYearlyAsync(int year, int month);
        
        Task<List<ViewDkp>> FindAllByPeriodeAsync(int year, int month);

	}
}
