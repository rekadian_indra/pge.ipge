﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewMonthlyProductionRepository : IRepository<ViewMonthlyProduction>
    {
    }
}
