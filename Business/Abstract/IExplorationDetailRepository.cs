﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IExplorationDetailRepository : IRepository<ExplorationDetail>
    {
    }
}
