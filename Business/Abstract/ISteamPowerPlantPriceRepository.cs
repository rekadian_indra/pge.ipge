﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface ISteamPowerPlantPriceRepository : IRepository<SteamPowerPlantPrice>
    {
		List<SteamPowerPlantPrice> GetFromPowerPlantId(int powerPlantId, DateTime? startDate, DateTime? endDate);

		SteamPowerPlantPrice GetPrice(int powerPlantId, DateTime periode);

		SteamPowerPlantPrice GetPrevPrice(int powerPlantId, DateTime periode);

		Task<int> GetLastFormSequence();
	}
}
