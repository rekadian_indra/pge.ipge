﻿using Business.Entities;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface IScreenshotSapRepository : IRepository<ScreenshotSap>
    {
		List<ScreenshotSap> GetFromFilePath(string filePath);

	}
}
