﻿using Business.Abstract;
using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewInvoicingKpiRepository : IRepository<ViewInvoicingKpi>
    {
    }
}
