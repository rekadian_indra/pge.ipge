﻿using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Abstract
{
    /// <summary>
    ///     Interface yang mengekspos manajemen dasar repository.
    /// </summary>
    /// <typeparam name="TEntity">Tipe yang mengenkapsulasi entity.</typeparam>
    public interface IRepository<TEntity> : IDisposable 
        where TEntity : class
    {
        #region Properties

        /// <summary>
        /// Objek queryable untuk menangangi query khusus.
        /// </summary>
        IQueryable<TEntity> Queryable
        {
            get;
        }

        #endregion

        #region Synchronous

        /// <summary>
        /// Melakukan listing terhadap data dengan mengambil data sesuai dengan filter dan default sort.
        /// </summary>
        /// <param name="filter">Filter yang dilakukan terhadap data</param>
        /// <returns>List data yang telah difilter.</returns>
        List<TEntity> FindAll(FilterQuery filter);

        /// <summary>
        /// Melakukan listing terhadap data dengan mengambil semua data setelah disort.
        /// </summary>
        /// <param name="sorts">Sort yang dilakukan terhadapa data</param>
        /// <returns>List data yang telah disort.</returns>
        List<TEntity> FindAll(List<SortQuery> sorts);

        /// <summary>
        /// Melakukan listing terhadap data dari hasil sorting yang sudah difilter terlebih dahulu.
        /// </summary>
        /// <param name="sorts">Sort yang dilakukan terhadapa data</param>
        /// <param name="filter">Filter yang dilakukan terhadap data</param>
        /// <returns>List data yang telah difilter, disort.</returns>
        List<TEntity> FindAll(List<SortQuery> sorts, FilterQuery filter);

        /// <summary>
        /// Melakukan listing terhadap data dengan skip beberapa data di awal jika skip tidak null
        /// lalu mengambil data sesuai dengan jumlah take jika tidak null setelah data disort dan difilter terlebih dahulu.
        /// </summary>
        /// <param name="skip">Jumlah data yang tidak diambil diawal.</param>
        /// <param name="take">Jumlah data yang diambil.</param>
        /// <param name="sorts">Sort yang dilakukan terhadapa data</param>
        /// <param name="filter">Filter yang dilakukan terhadap data</param>
        /// <returns>List data yang telah difilter, disort, dan diambil beberapa bagian.</returns>
        List<TEntity> FindAll(int? skip = null, int? take = null, List<SortQuery> sorts = null, FilterQuery filter = null);

        /// <summary>
        /// Mendapatkan data pertama sesuai dengan filter dan default sort.
        /// </summary>
        /// <param name="filter">Filter yang dilakukan terhadap data</param>
        /// <returns>Data sesuai dengan filter.</returns>
        TEntity Find(FilterQuery filter);

        /// <summary>
        /// Mendapatkan data pertama sesuai dengan filter dan sort.
        /// </summary>
        /// <param name="sorts">Sort yang dilakukan terhadapa data</param>
        /// <param name="filter">Filter yang dilakukan terhadap data</param>
        /// <returns>Data sesuai dengan filter dan sort.</returns>
        TEntity Find(List<SortQuery> sorts = null, FilterQuery filter = null);

        /// <summary>
        /// Mendapatkan data berdasarkan primary key.
        /// </summary>
        /// <param name="keys">Primary key dari data</param>
        /// <returns>Data berdasarkan primary key</returns>
        TEntity FindByPrimaryKey(params object[] keys);

        /// <summary>
        /// Menghitung total semua data atau data yang difilter.
        /// </summary>
        /// <param name="filter">Filter yang dilakukan terhadap data.</param>
        /// <returns>Total data yang masuk dalam filter yang dilakukan.</returns>
        int Count(FilterQuery filter = null);

        /// <summary>
        /// Menambah atau mengubah sebuah data lalu disimpan ke dalam database.
        /// </summary>
        /// <param name="dbItem">Data yang akan disimpan ke dalam database.</param>
        /// <returns>Berhasil atau tidak menyimpan data.</returns>
        bool Save(TEntity dbItem);

        /// <summary>
        /// Menambah atau mengubah collection data lalu disimpan ke dalam database.
        /// </summary>
        /// <param name="dbItems">Data collection yang akan disimpan ke dalam database.</param>
        /// <param name="autoDetectChanges">Jika bernilai false entity tidak dapat mengubah relasinya tetapi mendapat performa yang maksimal.</param>
        void SaveAll(IEnumerable<TEntity> dbItems, bool autoDetectChanges = true);

        /// <summary>
        /// Menghapus sebuah data yang sudah difilter sesuai dengan parameter.
        /// </summary>
        /// <param name="dbItem">Data yang akan dihapus dari database.</param>
        /// <returns>Berhasil atau tidak menghapus data.</returns>
        bool Delete(TEntity dbItem);

        /// <summary>
        /// Menghapus collection data yang ada pada database sesuai dengan parameter.
        /// </summary>
        /// <param name="dbItems">Collection data yang akan dihapus dari database.</param>
        /// <param name="autoDetectChanges">Jika bernilai false entity tidak dapat mengubah relasinya tetapi mendapat performa yang maksimal.</param>
        void DeleteAll(IEnumerable<TEntity> dbItems, bool autoDetectChanges = true);

        #endregion

        #region Asynchronous

        /// <summary>
        /// Melakukan listing terhadap data dengan mengambil data sesuai dengan filter dan default sort.
        /// </summary>
        /// <param name="filter">Filter yang dilakukan terhadap data</param>
        /// <returns>List data yang telah difilter.</returns>
        Task<List<TEntity>> FindAllAsync(FilterQuery filter);

        /// <summary>
        /// Melakukan listing terhadap data dengan mengambil semua data setelah disort.
        /// </summary>
        /// <param name="sorts">Sort yang dilakukan terhadapa data</param>
        /// <returns>List data yang telah disort.</returns>
        Task<List<TEntity>> FindAllAsync(List<SortQuery> sorts);

        /// <summary>
        /// Melakukan listing terhadap data dari hasil sorting yang sudah difilter terlebih dahulu.
        /// </summary>
        /// <param name="sorts">Sort yang dilakukan terhadapa data</param>
        /// <param name="filter">Filter yang dilakukan terhadap data</param>
        /// <returns>List data yang telah difilter, disort.</returns>
        Task<List<TEntity>> FindAllAsync(List<SortQuery> sorts, FilterQuery filter);

        /// <summary>
        /// Melakukan listing terhadap data dengan skip beberapa data di awal jika skip tidak null
        /// lalu mengambil data sesuai dengan jumlah take jika tidak null setelah data disort dan difilter terlebih dahulu.
        /// </summary>
        /// <param name="skip">Jumlah data yang tidak diambil diawal.</param>
        /// <param name="take">Jumlah data yang diambil.</param>
        /// <param name="sorts">Sort yang dilakukan terhadapa data</param>
        /// <param name="filter">Filter yang dilakukan terhadap data</param>
        /// <returns>List data yang telah difilter, disort, dan diambil beberapa bagian.</returns>
        Task<List<TEntity>> FindAllAsync(int? skip = null, int? take = null, List<SortQuery> sorts = null, FilterQuery filter = null);

        /// <summary>
        /// Mendapatkan data pertama sesuai dengan filter dan default sort.
        /// </summary>
        /// <param name="filter">Filter yang dilakukan terhadap data</param>
        /// <returns>Data sesuai dengan filter.</returns>
        Task<TEntity> FindAsync(FilterQuery filter);

        /// <summary>
        /// Mendapatkan data pertama sesuai dengan filter dan sort.
        /// </summary>
        /// <param name="sorts">Sort yang dilakukan terhadapa data</param>
        /// <param name="filter">Filter yang dilakukan terhadap data</param>
        /// <returns>Data sesuai dengan filter dan sort.</returns>
        Task<TEntity> FindAsync(List<SortQuery> sorts = null, FilterQuery filter = null);

        /// <summary>
        /// Mendapatkan data berdasarkan primary key.
        /// </summary>
        /// <param name="keys">Primary key dari data</param>
        /// <returns>Data berdasarkan primary key</returns>
        Task<TEntity> FindByPrimaryKeyAsync(params object[] keys);

        /// <summary>
        /// Menghitung total semua data atau data yang difilter.
        /// </summary>
        /// <param name="filter">Filter yang dilakukan terhadap data.</param>
        /// <returns>Total data yang masuk dalam filter yang dilakukan.</returns>
        Task<int> CountAsync(FilterQuery filter = null);

        /// <summary>
        /// Menambah atau mengubah sebuah data dan disimpan ke dalam database.
        /// </summary>
        /// <param name="dbItem">Data yang akan disimpan ke dalam database.</param>
        /// <returns>Berhasil atau tidak menyimpan data.</returns>
        Task<bool> SaveAsync(TEntity dbItem);

        /// <summary>
        /// Menambah atau mengubah collection data dan disimpan ke dalam database.
        /// </summary>
        /// <param name="dbItems">Data collection yang akan disimpan ke dalam database.</param>
        /// <param name="autoDetectChanges">Jika bernilai false entity tidak dapat mengubah relasinya tetapi mendapat performa yang maksimal.</param>
        Task SaveAllAsync(IEnumerable<TEntity> dbItems, bool autoDetectChanges = true);

        /// <summary>
        /// Menghapus sebuah data sesuai dengan parameter.
        /// </summary>
        /// <param name="dbItem">Data yang akan dihapus dari database.</param>
        /// <returns>Berhasil atau tidak menghapus data.</returns>
        Task<bool> DeleteAsync(TEntity dbItem);

        /// <summary>
        /// Menghapus collection data yang ada pada database sesuai dengan parameter.
        /// </summary>
        /// <param name="dbItems">Data collection yang akan dihapus dari database.</param>
        /// <param name="autoDetectChanges">Jika bernilai false entity tidak dapat mengubah relasinya tetapi mendapat performa yang maksimal.</param>
        Task DeleteAllAsync(IEnumerable<TEntity> dbItems, bool autoDetectChanges = true);

        #endregion
    }
}
