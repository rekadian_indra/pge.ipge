﻿using Business.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IProductionPlanDailyRepository : IRepository<ProductionPlanDaily>
    {

	}
}
