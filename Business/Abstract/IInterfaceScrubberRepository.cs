﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;

namespace Business.Abstract
{
    public interface IInterfaceScrubberRepository : IRepository<InterfaceScrubber>
    {
		Task<List<InterfaceScrubber>> FindAllByPeriodeArea(DateTime date, int areaId);

	}
}
