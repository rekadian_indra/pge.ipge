﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewInvoicingRepository : IRepository<ViewInvoicing>
    {
    }
}
