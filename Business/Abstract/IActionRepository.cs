﻿using Business.Entities;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface IActionRepository : IRepository<Action>
    {
        void Delete(string actionName, bool fk);
        List<Module> GetModulesInAction(string actionName);
    }
}
