﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewSalesOrderFinanceRepository : IRepository<ViewSalesOrderFinance>
    {
    }
}
