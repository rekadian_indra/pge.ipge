﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;
using Common.Enums;

namespace Business.Abstract
{
    public interface ITaskReportRepository : IRepository<TaskReport>
    {
		List<TaskReport> GetFromPowerPlantIdPeriode(int? powerPlantId, DateTime? periode = null, RangeData rangeData = RangeData.MONTHLY);

		List<TaskReport> GetFromPeriode(DateTime? periode,bool yearly = false);

        //TaskReport GetLastData(int? areaId = null, int? year = null);

        TaskReport GetLastData(int? areaId = null, int? month = null, int? year = null);

        Task<TaskReport> GetLastDataAsync(int? areaId = null, int? month = null, int? year = null);
    }
}
