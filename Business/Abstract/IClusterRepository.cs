﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IClusterRepository : IRepository<Cluster>
    {
    }
}
