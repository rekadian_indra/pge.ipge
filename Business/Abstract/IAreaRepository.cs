﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IAreaRepository : IRepository<Area>
    {
    }
}
