﻿using Business.Entities.Views;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
	public interface IViewReliabilityPlanRepository : IRepository<ViewReliabilityPlan>
	{
        Task<List<ViewReliabilityPlan>> FindAllUntilPeriodeYearlyAsync(int year, int month);

        Task<List<ViewReliabilityPlan>> FindAllByPeriodeAsync(int year, int month);

	}
}
