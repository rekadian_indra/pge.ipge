﻿using Business.Entities;
using Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IProductionPlanRepository : IRepository<ProductionPlan>
    {
        List<ProductionPlan> GetFromPowerPlantId(int? powerPlantId, int? year = null, ProductionUtilization? type = null);

        Task<List<ProductionPlan>> GetFromPowerPlantIdAsync(int? powerPlantId, int? year = null, ProductionUtilization? type = null);
    }
}
