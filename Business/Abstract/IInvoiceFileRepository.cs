﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;

namespace Business.Abstract
{
    public interface IInvoiceFileRepository : IRepository<InvoiceFile>
    {
		List<InvoiceFile> GetFromFilePath(string filePath);

	}
}
