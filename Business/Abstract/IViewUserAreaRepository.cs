﻿using Business.Entities;
using Business.Entities.Views;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IViewUserAreaRepository : IRepository<ViewUser>
    {
        
    }
}
