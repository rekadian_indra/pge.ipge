﻿using Business.Entities.Views;
using Business.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace Business.Abstract
{
    public interface IViewCapacityFactorRepository : IRepository<ViewCapacityFactor>
    {
        //IQueryable<ViewCapacityFactor> FindAllGroupBy(int? skip = null, int? take = null, List<SortQuery> sorts = null, FilterQuery filter = null);
    }
}
