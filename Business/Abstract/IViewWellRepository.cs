﻿using Business.Entities.Views;
using Business.Infrastructure;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IViewWellRepository : IRepository<ViewWell>
    {
        List<ViewWell> FindViewWellByParam(List<SortQuery> sorts, FilterQuery filter);
        Task<List<ViewWell>> FindViewWellByParamAsync(List<SortQuery> sorts, FilterQuery filter);
    }
}
