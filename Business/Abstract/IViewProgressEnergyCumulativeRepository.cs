﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewProgressEnergyCumulativeRepository : IRepository<ViewProgressEnergy>
    {
    }
}
