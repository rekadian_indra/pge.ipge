﻿using Business.Entities;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface IBastFileSignedRepository : IRepository<BastFileSigned>
    {
		List<BastFileSigned> GetFromFilePath(string filePath);

	}
}
