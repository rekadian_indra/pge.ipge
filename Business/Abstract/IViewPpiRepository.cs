﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewPpiRepository : IRepository<ViewPpi>
    {
    }
}
