﻿using Business.Entities;
using System;

namespace Business.Abstract
{
    public interface IModulesInRoleRepository : IRepository<ModulesInRole>
    {
        ModulesInRole FindByRoleAndModule(Guid roleId, Guid moduleId);
        void RemoveAction(Guid moduleId,Guid actionId);
        void DeleteByModule(Guid moduleId);
        void DeleteByRole(Guid roleId);
    }
}
