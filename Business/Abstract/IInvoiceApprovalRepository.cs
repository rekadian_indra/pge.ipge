﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;

namespace Business.Abstract
{
    public interface IInvoiceApprovalRepository : IRepository<InvoiceApproval>
    {
		Task<InvoiceApproval> GetLastApproval(int invoiceId);

	}
}
