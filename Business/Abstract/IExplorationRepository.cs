﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IExplorationRepository : IRepository<Exploration>
	{
		List<Exploration> GetFromPeriodeArea(DateTime startDate, DateTime endDate, int areaId);
		Task<List<Exploration>> GetFromPeriodeWell(DateTime startDate, DateTime endDate, int wellId);

	}
}
