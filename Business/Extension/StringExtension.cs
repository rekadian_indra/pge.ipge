﻿using System.ComponentModel;
using System.Reflection;

/**
* code by pazto (topaz@rekadia.co.id)
* @2018-05-09
*/
namespace System
{
    public static class StringExtension
    {
        /// <summary>
        /// Get string value between key.
        /// </summary>
        public static string Between(this string s, string key)
        {
            return s.Between(key, key);
        }

        /// <summary>
        /// Get string value between key1 and key2.
        /// </summary>
        public static string Between(this string s, string key1, string key2)
        {
            int adjustedIndex;
            int firstIndex = s.IndexOf(key1);
            int lastIndex = s.LastIndexOf(key2);

            if (firstIndex == -1)
            {
                return string.Empty;
            }

            if (lastIndex == -1)
            {
                return string.Empty;
            }

            adjustedIndex = firstIndex + key1.Length;

            if (adjustedIndex >= lastIndex)
            {
                return string.Empty;
            }

            return s.Substring(adjustedIndex, lastIndex - adjustedIndex);
        }

        /// <summary>
        /// Get string value before key.
        /// </summary>
        public static string Before(this string s, string key)
        {
            int index = s.IndexOf(key);

            if (index == -1)
            {
                return string.Empty;
            }

            return s.Substring(0, index);
        }

        /// <summary>
        /// Get string value after [last] a.
        /// </summary>
        public static string After(this string s, string key)
        {
            int adjustedIndex;
            int index = s.LastIndexOf(key);

            if (index == -1)
            {
                return string.Empty;
            }

            adjustedIndex = index + key.Length;

            if (adjustedIndex >= s.Length)
            {
                return string.Empty;
            }

            return s.Substring(adjustedIndex);
        }

        public static T ToEnum<T>(this string s)
            where T : struct
        {
            Type type = typeof(T);

            // Can't use generic type constraints on value types
            if (!type.IsEnum)
                throw new InvalidCastException("T must be type of System.Enum");

            if (Enum.TryParse(s, out T result))
                return result;
            else
            {
                foreach (FieldInfo field in type.GetFields())
                {
                    if (Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                    {
                        if (attribute.Description == s)
                            return (T)field.GetValue(null);
                    }
                    else
                    {
                        if (field.Name == s)
                            return (T)field.GetValue(null);
                    }
                }
            }

            throw new ArgumentException($"{s} Not found in {type.Name}");
        }
    }
}
