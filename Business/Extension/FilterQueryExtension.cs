﻿using Business.Infrastructure;

namespace Business.Extension
{
    public static class FilterQueryExtension
    {
        public static bool HasValue(this FilterQuery filter)
        {
            return filter != null && filter.Value != null;
        }
    }
}
