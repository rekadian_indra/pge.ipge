﻿using Business.Infrastructure;
using System.Globalization;

/**
 * code by pazto (topaz@rekadia.co.id)
 * @2018-05-09
*/
namespace System
{
    public static class ObjectExtension
    {
        public static int ToInteger(this object args)
        {
            return int.Parse(args.ToString());
        }

        public static long ToLong(this object args)
        {
            return long.Parse(args.ToString());
        }

        public static short ToShort(this object args)
        {
            return short.Parse(args.ToString());
        }

        public static double ToDouble(this object args)
        {
            return double.Parse(args.ToString());
        }

        public static float ToFloat(this object args)
        {
            return float.Parse(args.ToString());
        }

        public static byte ToByte(this object args)
        {
            return byte.Parse(args.ToString());
        }

        public static bool ToBoolean(this object args)
        {
            return bool.Parse(args.ToString());
        }

        public static Guid ToGuid(this object args)
        {
            return Guid.Parse(args.ToString());
        }


        public static DateTime ToDateTime(this object args, DateTimeKind kind = DateTimeKind.Utc)
        {
            DateTime? dt = null;
            Type type = args.GetType();

            if (type != typeof(string) && type != typeof(DateTime) && type != typeof(DateTime?))
                throw new NotSupportedException(type.Name);

            if (type == typeof(string))
            {
                try
                {
                    string s = args.ToString();

                    if (s.Length >= 24)
                        dt = DateTime.ParseExact(s.Substring(0, 24), DateTimeFormat.CompactDayDateTimeFormat, CultureInfo.InvariantCulture);
                    else
                        dt = DateTime.Parse(s);

                    return DateTime.SpecifyKind(dt.Value, kind);
                }
                catch
                {
                    throw new ArgumentException("Invalid DateTime format");
                }
            }

            if (type == typeof(DateTime))
            {
                dt = (DateTime)args;
            }

            if (type == typeof(DateTime?))
            {
                if (args == null)
                    throw new ArgumentNullException();

                dt = ((DateTime?)args);
            }

            return ConvertDateTime(dt.Value, kind);
        }

        private static DateTime ConvertDateTime(DateTime dt, DateTimeKind kind)
        {
            if (dt.Kind == DateTimeKind.Utc || dt.Kind == DateTimeKind.Local)
            {
                if (dt.Kind == kind)
                {
                    return dt;
                }
                else
                {
                    if (kind == DateTimeKind.Utc || kind == DateTimeKind.Unspecified)
                    {
                        return dt.ToUtcDateTime();
                    }
                    else
                    {
                        return dt.ToLocalDateTime();
                    }
                }
            }
            else
            {
                return DateTime.SpecifyKind(dt, kind);
            }
        }
    }
}
