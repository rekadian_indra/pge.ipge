﻿//using Business.Infrastructure;
//using System;
//using System.Globalization;

//namespace Business.Extension
namespace System
{
    public static class DateTimeExtension
    {
        public static DateTime? ToLocalDateTime(this DateTime? dateTime)
        {
            if (dateTime.HasValue)
                return ToLocalDateTime(dateTime.Value);

            return null;
        }

        public static DateTime ToLocalDateTime(this DateTime dateTime)
        {
            if (dateTime.Kind == DateTimeKind.Local)
                return dateTime;

            return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc).ToLocalTime();
        }

        public static DateTime? ToUtcDateTime(this DateTime? dateTime)
        {
            if (dateTime.HasValue)
                return ToUtcDateTime(dateTime.Value);

            return null;
        }

        public static DateTime ToUtcDateTime(this DateTime dateTime)
        {
            if (dateTime.Kind == DateTimeKind.Utc && dateTime.Kind == DateTimeKind.Unspecified)
                return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);

            return dateTime.ToUniversalTime();
        }

        public static DateTime? ToClientDateTime(this DateTime? dateTime, double? offsetHour = null)
        {
            if (dateTime.HasValue)
                return ToClientDateTime(dateTime.Value, offsetHour);

            return null;
        }

        public static DateTime ToClientDateTime(this DateTime dateTime, double? offsetHour = null)
        {
            if (offsetHour.HasValue)
            {
                if (dateTime.Kind != DateTimeKind.Utc)
                    dateTime = dateTime.ToUtcDateTime();

                dateTime = dateTime.AddHours(offsetHour.Value);

                return DateTime.SpecifyKind(dateTime, DateTimeKind.Local);
            }
            else
            {
                return dateTime.ToLocalDateTime();
            }
        }

        public static DateTime? ToServerDateTime(this DateTime? dateTime, double? offsetHour = null)
        {
            if (dateTime.HasValue)
                return ToServerDateTime(dateTime.Value, offsetHour);

            return null;
        }

        public static DateTime ToServerDateTime(this DateTime dateTime, double? offsetHour = null)
        {
            if (offsetHour.HasValue)
            {
                if (dateTime.Kind != DateTimeKind.Local)
                    dateTime = dateTime.ToLocalDateTime();

                dateTime = dateTime.AddHours((offsetHour.Value * -1));

                return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
            }
            else
            {
                return dateTime.ToUtcDateTime();
            }
        }

        //public static DateTime? ToLocalDateTime(this DateTime? dateTime)
        //{
        //    if (dateTime.HasValue)
        //        return ToLocalDateTime(dateTime.Value);

        //    return null;
        //}

        //public static DateTime ToLocalDateTime(this DateTime dateTime)
        //{
        //    DateTimeOffset dto = new DateTimeOffset(DateTime.SpecifyKind(dateTime, DateTimeKind.Utc));
        //    DateTime? dt = new DateTime?(dto.LocalDateTime);

        //    return dt.Value;
        //}

        //public static DateTime? ToUtcDateTime(this DateTime? dateTime)
        //{
        //    if (dateTime.HasValue)
        //        return ToUtcDateTime(dateTime.Value);

        //    return null;
        //}

        //public static DateTime ToUtcDateTime(this DateTime dateTime)
        //{
        //    //if (dateTime.Kind == DateTimeKind.Utc && dateTime.Kind == DateTimeKind.Unspecified)
        //    //    return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);

        //    //return dateTime.ToUniversalTime();
        //}

        //public static DateTime ToDateTime(this object dateTime)
        //{
        //    try
        //    {
        //        DateTime dt;
        //        string s = dateTime.ToString();

        //        if (s.Length >= 24)
        //            dt = DateTime.ParseExact(s.Substring(0, 24), DateTimeFormat.CompactDayDateTimeFormat, CultureInfo.InvariantCulture);
        //        else
        //            dt = DateTime.Parse(s);

        //        return dt;
        //    }
        //    catch
        //    {
        //        throw new ArgumentException("Not valid DateTime format");
        //    }
        //}
    }
}
