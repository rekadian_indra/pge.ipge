﻿using System.Collections.Generic;

namespace WebUI.Models.Service
{
    public class ServiceResponseStub<T>
    {
        public int status { get; set; }
        public string message { get; set; }
        public int total { get; set; }
        public List<T> data { get; set; }
    }
}