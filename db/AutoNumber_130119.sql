USE [PGE.IPGE.APP]
GO
/****** Object:  Table [dbo].[AutoNumber]    Script Date: 13/01/2019 14.59.31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutoNumber](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TransactionCode] [varchar](2) NOT NULL,
	[TransactionName] [varchar](50) NOT NULL,
	[Year] [int] NOT NULL,
	[Prefix] [varchar](50) NOT NULL,
	[IncrementNumber] [int] NOT NULL,
 CONSTRAINT [PK_AutoNumber] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AutoNumber] ON 

INSERT [dbo].[AutoNumber] ([Id], [TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (1, N'01', N'Download Form Miro', 2019, N'PT', 3)
INSERT [dbo].[AutoNumber] ([Id], [TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (2, N'01', N'Download Form Miro', 2020, N'PT', 0)
INSERT [dbo].[AutoNumber] ([Id], [TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (3, N'01', N'Download Form Miro', 2021, N'PT', 0)
INSERT [dbo].[AutoNumber] ([Id], [TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (4, N'01', N'Download Form Miro', 2022, N'PT', 0)
INSERT [dbo].[AutoNumber] ([Id], [TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (5, N'01', N'Download Form Miro', 2023, N'PT', 0)
INSERT [dbo].[AutoNumber] ([Id], [TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (6, N'01', N'Download Form Miro', 2024, N'PT', 0)
INSERT [dbo].[AutoNumber] ([Id], [TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (7, N'01', N'Download Form Miro', 2025, N'PT', 0)
SET IDENTITY_INSERT [dbo].[AutoNumber] OFF
