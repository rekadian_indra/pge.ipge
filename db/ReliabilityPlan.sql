USE [PGE.IPGE.APP]
GO
/****** Object:  Table [dbo].[ReliabilityPlan]    Script Date: 24-Jan-19 5:10:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReliabilityPlan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PowerPlantId] [int] NOT NULL,
	[Periode] [datetime] NOT NULL,
	[FOH] [float] NOT NULL,
	[MOH] [float] NOT NULL,
	[POH] [float] NOT NULL,
	[EFDH] [float] NOT NULL,
	[EMDH] [float] NOT NULL,
	[EPDH] [float] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_ReliabilityPlan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ReliabilityPlan]  WITH CHECK ADD  CONSTRAINT [FK_ReliabilityPlan_PowerPlant] FOREIGN KEY([PowerPlantId])
REFERENCES [dbo].[PowerPlant] ([Id])
GO
ALTER TABLE [dbo].[ReliabilityPlan] CHECK CONSTRAINT [FK_ReliabilityPlan_PowerPlant]
GO
