USE [PGE.IPGE.APP]
GO
/****** Object:  Table [dbo].[WellHistory]    Script Date: 16/01/2019 13:31:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WellHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WellId] [int] NOT NULL,
	[HistoryDateTimeUtc] [datetime] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_WellHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[WellHistory]  WITH CHECK ADD  CONSTRAINT [FK_WellHistory_Well] FOREIGN KEY([WellId])
REFERENCES [dbo].[Well] ([Id])
GO
ALTER TABLE [dbo].[WellHistory] CHECK CONSTRAINT [FK_WellHistory_Well]
GO
