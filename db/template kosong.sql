USE [PGE.IPGE.APP]
GO
/****** Object:  UserDefinedFunction [dbo].[bebas]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[bebas]()
returns int 
as 
begin
	declare @Hasil int;
		set @Hasil = (select count(*) from Crew);
	return @Hasil;

end





GO
/****** Object:  Table [dbo].[ActualPlanning]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActualPlanning](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskReportId] [int] NULL,
	[AreaId] [int] NULL,
	[Planning] [varchar](max) NULL,
	[Actual] [varchar](max) NULL,
	[ActualCase] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_ActualPlanning] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Area]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Area](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NULL,
	[Name] [varchar](250) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Plant] [varchar](10) NULL,
	[Description] [varchar](500) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BastFile]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BastFile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesOrderId] [int] NOT NULL,
	[FilePath] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_BastFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BastFileSigned]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BastFileSigned](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesOrderId] [int] NOT NULL,
	[FilePath] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_BastFileSigned] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cluster]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cluster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NULL,
	[Name] [varchar](250) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Cluster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Company]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Company](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dkp]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dkp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskReportId] [int] NULL,
	[PowerPlantId] [int] NULL,
	[Status] [varchar](50) NULL,
	[StartHour] [time](7) NULL,
	[EndHour] [time](7) NULL,
	[Derating] [float] NULL,
	[Location] [varchar](500) NULL,
	[Cause] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Dkp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EFakturFile]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EFakturFile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[FilePath] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_EFakturFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Exploration]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Exploration](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NOT NULL,
	[ExplorationDate] [date] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_Exploration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExplorationDetail]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExplorationDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExplorationId] [int] NOT NULL,
	[Mku] [float] NOT NULL,
	[Mkt] [float] NOT NULL,
	[Mdpl] [float] NOT NULL,
	[Sds] [float] NOT NULL,
	[Tds] [float] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ExplorationDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InterfacePoint]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InterfacePoint](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NULL,
	[Name] [varchar](250) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_InterfacePoint] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InterfaceScrubber]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InterfaceScrubber](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskReportId] [int] NULL,
	[InterfaceId] [int] NULL,
	[ScrubberId] [int] NULL,
	[InterfacePressure] [float] NULL,
	[InterfaceTemperature] [float] NULL,
	[ScrubberPressure] [float] NULL,
	[ScrubberTemperature] [float] NULL,
	[InterfacePressureUnit] [varchar](20) NULL,
	[InterfaceTemperatureUnit] [varchar](20) NULL,
	[ScrubberPressureUnit] [varchar](20) NULL,
	[ScrubberTemperatureUnit] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_InterfaceScrubber] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Invoice]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Invoice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesOrderId] [int] NOT NULL,
	[WorkflowId] [int] NOT NULL,
	[InvoiceUploadDate] [datetime] NULL,
	[InvoiceSentDate] [datetime] NULL,
	[InvoiceAcceptDate] [datetime] NULL,
	[ClearingDocDate] [datetime] NULL,
	[InvoiceNo] [varchar](30) NULL,
	[InvoiceSequence] [varchar](8) NULL,
	[SignatureName] [varchar](100) NULL,
	[SignaturePosition] [varchar](100) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InvoiceApproval]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InvoiceApproval](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[WorkflowId] [int] NOT NULL,
	[Notes] [text] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
 CONSTRAINT [PK_InvoiceApproval] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InvoiceFile]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InvoiceFile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[FilePath] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_InvoiceFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Monitoring]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monitoring](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskReportId] [int] NULL,
	[WellId] [int] NULL,
	[Tks] [float] NULL,
	[Status] [varchar](50) NULL,
	[Notes] [varchar](max) NULL,
	[TksUnit] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Monitoring] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MstSeparator]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstSeparator](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NULL,
	[Name] [varchar](250) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstSeparator] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pltp]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pltp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskReportId] [int] NULL,
	[PowerPlantId] [int] NULL,
	[EnergyGross] [float] NULL,
	[EnergyNett] [float] NULL,
	[SteamUtilization] [float] NULL,
	[WasteRockMufller] [float] NULL,
	[Ssc] [float] NULL,
	[GppsShutdown] [float] NULL,
	[ImportEnergy] [float] NULL,
	[SgssShutdown] [float] NULL,
	[Notes] [varchar](max) NULL,
	[EnergyGrossUnit] [varchar](20) NULL,
	[EnergyNettUnit] [varchar](20) NULL,
	[SteamUtilizationUnit] [varchar](20) NULL,
	[WasteRockMufllerUnit] [varchar](20) NULL,
	[SscUnit] [varchar](20) NULL,
	[GppsShutdownUnit] [varchar](20) NULL,
	[ImportEnergyUnit] [varchar](20) NULL,
	[SgssShutdownUnit] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Pltp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PowerPlant]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PowerPlant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Address] [varchar](255) NULL,
	[SalesGroup] [varchar](50) NULL,
	[ShippingPoint] [varchar](50) NULL,
	[SoldToPartyId] [int] NULL,
	[Material] [varchar](50) NULL,
	[URC] [int] NULL,
	[SOType] [varchar](50) NULL,
	[Description] [varchar](500) NOT NULL,
	[AgreementName] [varchar](max) NULL,
	[AgreementDate] [datetime] NULL,
	[MeterName] [varchar](50) NULL,
	[MeterType] [varchar](50) NULL,
	[BasePrice] [float] NOT NULL CONSTRAINT [DF_PowerPlant_BasePrice]  DEFAULT ((0)),
	[InvoiceGroup] [int] NOT NULL CONSTRAINT [DF_PowerPlant_InvoiceGroup]  DEFAULT ((0)),
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[TemplateBastFileName] [text] NULL,
 CONSTRAINT [PK_PowerPlant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PPI]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PPI](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PowerPlantId] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[QuarterNum] [tinyint] NOT NULL,
	[ValueMonth1] [float] NOT NULL,
	[ValueMonth2] [float] NOT NULL,
	[ValueMonth3] [float] NOT NULL,
	[InflationIndex] [float] NOT NULL,
	[NextPrice] [float] NOT NULL,
	[ValueMonth1Add] [float] NULL,
	[ValueMonth2Add] [float] NULL,
	[ValueMonth3Add] [float] NULL,
	[InflationIndexAdd] [float] NULL,
	[NextPriceAdd] [float] NULL,
	[WorkflowId] [int] NOT NULL,
	[FormSequence] [varchar](8) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_PPI] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PpiApproval]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PpiApproval](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PpiId] [int] NOT NULL,
	[WorkflowId] [int] NOT NULL,
	[Notes] [text] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
 CONSTRAINT [PK_PPIApproval] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Production]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Production](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskReportId] [int] NULL,
	[WellId] [int] NULL,
	[Tks] [float] NULL,
	[PressureLine] [float] NULL,
	[Temperature] [float] NULL,
	[ThrottleValve] [float] NULL,
	[FlowRate] [float] NULL,
	[FlowTotal] [float] NULL,
	[PressureUpstream] [float] NULL,
	[DifferentialPressure] [float] NULL,
	[Status] [varchar](50) NULL,
	[Notes] [varchar](max) NULL,
	[TksUnit] [varchar](20) NULL,
	[PressureLineUnit] [varchar](20) NULL,
	[TemperatureUnit] [varchar](20) NULL,
	[ThrottleValveUnit] [varchar](20) NULL,
	[FlowRateUnit] [varchar](20) NULL,
	[FlowTotalUnit] [varchar](20) NULL,
	[PressureUpstreamUnit] [varchar](20) NULL,
	[DifferentialPressureUnit] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Production] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductionPlan]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductionPlan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PowerPlantId] [int] NOT NULL,
	[UtilType] [varchar](50) NOT NULL,
	[Year] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_PlanYearly] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductionPlanDetail]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductionPlanDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductionPlanId] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Value] [float] NOT NULL,
 CONSTRAINT [PK_PlanMonthly] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReInjection]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReInjection](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskReportId] [int] NULL,
	[WellId] [int] NULL,
	[Tks] [float] NULL,
	[PressureLine] [float] NULL,
	[Temperature] [float] NULL,
	[Ph] [float] NULL,
	[FlowRate] [float] NULL,
	[FlowTotal] [float] NULL,
	[Fluida] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
	[Notes] [varchar](max) NULL,
	[TksUnit] [varchar](20) NULL,
	[PressureLineUnit] [varchar](20) NULL,
	[TemperatureUnit] [varchar](20) NULL,
	[FlowRateUnit] [varchar](20) NULL,
	[FlowTotalUnit] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_ReInjection] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesOrder]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Periode] [datetime] NULL,
	[SOType] [varchar](10) NULL,
	[PowerPlantId] [int] NULL,
	[Quantity] [float] NULL,
	[NetPrice] [float] NULL,
	[NetValue] [float] NULL,
	[Currency] [varchar](10) NULL,
	[Notes] [text] NULL,
	[WorkflowId] [int] NULL,
	[NoPo] [varchar](50) NULL,
	[NoPr] [varchar](50) NULL,
	[NoSo] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_SalesOrder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesOrderApproval]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesOrderApproval](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesOrderId] [int] NOT NULL,
	[WorkflowId] [int] NOT NULL,
	[Notes] [text] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
 CONSTRAINT [PK_SalesOrderApproval] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Scrubber]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Scrubber](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NULL,
	[Name] [varchar](250) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Scrubber] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Separator]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Separator](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskReportId] [int] NULL,
	[SeparatorId] [int] NULL,
	[Pressure] [float] NULL,
	[Temperature] [float] NULL,
	[LevelBrine] [float] NULL,
	[ThrottleValveBrine] [float] NULL,
	[ThrottleValveSteam] [float] NULL,
	[SteamFlowRate] [float] NULL,
	[BrineFlowRate] [float] NULL,
	[PressureUnit] [varchar](20) NULL,
	[TemperatureUnit] [varchar](20) NULL,
	[LevelBrineUnit] [varchar](20) NULL,
	[ThrottleValveBrineUnit] [varchar](20) NULL,
	[ThrottleValveSteamUnit] [varchar](20) NULL,
	[SteamFlowRateUnit] [varchar](20) NULL,
	[BrineFlowRateUnit] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Separator] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SoldToParty]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SoldToParty](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_SoldToParty] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SteamPowerPlantPrice]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SteamPowerPlantPrice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PowerPlantId] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[IncreasePercentage] [float] NOT NULL,
	[Price] [float] NOT NULL,
	[IncreasePercentageAdd] [float] NULL,
	[PriceAdd] [float] NULL,
	[WorkflowId] [int] NOT NULL,
	[FormSequence] [varchar](8) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_SteamPowerPlantPrice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SteamPowerPlantPriceApproval]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SteamPowerPlantPriceApproval](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SteamPowerPlantPriceId] [int] NOT NULL,
	[WorkflowId] [int] NOT NULL,
	[Notes] [text] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
 CONSTRAINT [PK_SteamPowerPlantPriceApproval] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaskRelation]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskRelation](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[RelationId] [int] NULL,
	[Category] [int] NULL,
 CONSTRAINT [PK_TaskRelation] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaskReport]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaskReport](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NULL,
	[Periode] [datetime] NULL,
	[SourceInput] [varchar](10) NULL,
	[Status] [varchar](50) NULL,
	[ApprovedBy] [varchar](100) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Well]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Well](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NULL,
	[ClusterId] [int] NULL,
	[Name] [varchar](250) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Well] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Workflow]    Script Date: 11/30/2018 10:38:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Workflow](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkflowType] [varchar](5) NOT NULL,
	[Code] [varchar](100) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[PreviousStep] [int] NULL,
	[Approver] [varchar](100) NULL,
	[IsBastDownloadable] [bit] NOT NULL CONSTRAINT [DF_Workflow_IsBastAvailable]  DEFAULT ((0)),
	[IsSapDownloadable] [bit] NOT NULL CONSTRAINT [DF_Workflow_IsSapAvailable]  DEFAULT ((0)),
	[IsBastSignedUploadable] [bit] NOT NULL CONSTRAINT [DF_Workflow_IsBastSignedAvailable]  DEFAULT ((0)),
	[IsBastSignedDownloadable] [bit] NOT NULL CONSTRAINT [DF_Workflow_IsBastSignedDownloadable]  DEFAULT ((0)),
 CONSTRAINT [PK_Workflow] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Area] ON 

GO
INSERT [dbo].[Area] ([Id], [CompanyId], [Name], [Code], [Plant], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, 1, N'Kamojang', N'KMJ', N'E002', N'Kamojang', N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Area] ([Id], [CompanyId], [Name], [Code], [Plant], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 1, N'Lahendong', N'LHD', N'E003', N'Lahendong', N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Area] ([Id], [CompanyId], [Name], [Code], [Plant], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 1, N'Karaha', N'KRH', N'E006', N'Karaha', N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Area] ([Id], [CompanyId], [Name], [Code], [Plant], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (4, 1, N'Ulubelu', N'UBL', N'E004', N'Ulubelu', N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Area] ([Id], [CompanyId], [Name], [Code], [Plant], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (5, 1, N'Darajat', N'', N'', N'Darajat', N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Area] ([Id], [CompanyId], [Name], [Code], [Plant], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (6, 1, N'Salak', N'', N'', N'Salak', N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'system', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[Area] OFF
GO
SET IDENTITY_INSERT [dbo].[Company] ON 

GO
INSERT [dbo].[Company] ([Id], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'Pertamina Geothermal Energy', N'PGE', N'Pertamina Geothermal', N'System', CAST(N'2018-08-27 00:00:00.000' AS DateTime), N'System', CAST(N'2018-08-27 00:00:00.000' AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[Company] OFF
GO
SET IDENTITY_INSERT [dbo].[Exploration] ON 

GO
INSERT [dbo].[Exploration] ([Id], [AreaId], [ExplorationDate], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (1, 4, CAST(N'2018-06-21' AS Date), 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[Exploration] ([Id], [AreaId], [ExplorationDate], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (2, 4, CAST(N'2018-06-22' AS Date), 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[Exploration] ([Id], [AreaId], [ExplorationDate], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (3, 4, CAST(N'2018-06-25' AS Date), 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[Exploration] ([Id], [AreaId], [ExplorationDate], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (4, 4, CAST(N'2018-06-27' AS Date), 0, N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime))
GO
INSERT [dbo].[Exploration] ([Id], [AreaId], [ExplorationDate], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (5, 4, CAST(N'2018-06-29' AS Date), 0, N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime))
GO
INSERT [dbo].[Exploration] ([Id], [AreaId], [ExplorationDate], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (6, 4, CAST(N'2018-07-02' AS Date), 0, N'superadmin', CAST(N'2018-11-30 10:02:07.273' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.273' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Exploration] OFF
GO
SET IDENTITY_INSERT [dbo].[ExplorationDetail] ON 

GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (1, 1, 0, 0, 1141.67, 19.43607295238095, 0.037036631428571436, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.177' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.177' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (2, 1, 200, 199.877187396842, 941.992812603158, 26.6950876, 0.33725303, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (3, 1, 400, 398.71290488863292, 743.157095111367, 27.0581705, 0.33938149062500012, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (4, 1, 600, 595.89582871565642, 545.97417128434347, 28.660175437500005, 0.34164724000000007, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (5, 1, 800, 785.0796422863101, 356.79035771368979, 29.44562393333333, 14.548020266666667, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (6, 1, 1000, 960.94387000664881, 180.92612999335108, 30.408220400000005, 32.199794133333327, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (7, 1, 1050, 1002.5979994997364, 139.27200050026352, 30.742958375000004, 36.348079875, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (8, 1, 1100, 1043.1269470562531, 98.743052943746761, 31.063052066666668, 40.234471866666667, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (9, 1, 1150, 1082.642113380042, 59.227886619957872, 31.350326125000002, 44.208094562499987, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (10, 1, 1200, 1121.5940371357019, 20.275962864297981, 82.852774492058828, 46.8261696014134, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (11, 1, 1250, 1158.665765749887, -16.795765749887096, 122.49436518973981, 49.357650647595513, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (12, 1, 1300, 1197.8912884273834, -56.021288427383524, 122.23963857815654, 52.42887430258839, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.200' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (13, 1, 1350, 1234.6055121529341, -92.735512152934234, 189.8755652654518, 54.799059463070115, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (14, 1, 1400, 1270.6837560482716, -128.81375604827167, 223.14400369949496, 55.996930461489889, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (15, 1, 1450, 1306.5221765948077, -164.65217659480777, 224.13118217482221, 57.416245693854222, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (16, 1, 1500, 1341.8555549069301, -199.98555490693025, 223.42596536313053, 59.039566934582631, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (17, 1, 1550, 1376.5968134073708, -234.72681340737086, 223.78302211300505, 60.587328955357926, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (18, 1, 1600, 1410.6113840006647, -268.74138400066477, 225.02322788320706, 62.274409038194449, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (19, 1, 1650, 1444.2631891016652, -302.39318910166526, 223.19554484552043, 64.0652784451883, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (20, 1, 1700, 1478.9916807003017, -337.12168070030179, 224.2895844281401, 66.15465598375603, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (21, 1, 1750, 1515.2962443542233, -373.4262443542234, 223.95491286435785, 68.60015341175145, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (22, 1, 1800, 1553.0053544226198, -411.13535442261991, 224.15809381906021, 71.737039847321029, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (23, 1, 1850, 1591.7220968126981, -449.85209681269816, 224.33928280946967, 74.966576215075762, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (24, 1, 1900, 1631.888351776902, -490.01835177690214, 224.52017505808081, 78.309756161856043, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (25, 1, 1950, 1673.273815221288, -531.40381522128814, 224.6654069055775, 81.7678065879776, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.203' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (26, 1, 2000, 1716.139753766982, -574.26975376698215, 224.6035885608751, 85.336354431974087, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (27, 1, 2050, 1760.4861581759058, -618.61615817590587, 224.83345745778982, 89.031006383442033, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (28, 1, 2080, 1787.8214999761269, -645.951499976127, 224.8386437111111, 91.322462734027781, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (29, 2, 0, 0, 1141.67, 29.42303575, 50.151709444120492, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (30, 2, 200, 199.877187396842, 941.992812603158, 32.92271516666667, 51.6325800941205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (31, 2, 400, 398.71290488863292, 743.157095111367, 42.4985115, 53.352585607453832, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (32, 2, 600, 595.89582871565642, 545.97417128434347, 69.16025, 54.99023273745383, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (33, 2, 800, 785.0796422863101, 356.79035771368979, 100.98245228571429, 56.4165375941205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (34, 2, 1000, 960.94387000664881, 180.92612999335108, 132.88016333333334, 57.655649804120493, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (35, 2, 1050, 1002.5979994997364, 139.27200050026352, 137.600315, 57.936813927453834, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (36, 2, 1100, 1043.1269470562531, 98.743052943746761, 155.24020285714283, 58.200420254120495, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (37, 2, 1150, 1082.642113380042, 59.227886619957872, 237.69616833333336, 58.425757650787162, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (38, 2, 1200, 1121.5940371357019, 20.275962864297981, 266.84972999999997, 58.5710947541205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.207' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (39, 2, 1250, 1158.665765749887, -16.795765749887096, 267.20059833333329, 58.697592827453818, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (40, 2, 1300, 1197.8912884273834, -56.021288427383524, 267.40948499999996, 58.820860820787168, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (41, 2, 1350, 1234.6055121529341, -92.735512152934234, 231.857582, 58.947285002120495, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (42, 2, 1400, 1270.6837560482716, -128.81375604827167, 231.67842833333336, 59.097000647453832, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (43, 2, 1450, 1306.5221765948077, -164.65217659480777, 231.60117285714287, 59.251154974120482, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (44, 2, 1500, 1341.8555549069301, -199.98555490693025, 231.29180166666666, 59.400236330787166, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (45, 2, 1550, 1376.5968134073708, -234.72681340737086, 231.10194833333335, 59.546829630787165, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (46, 2, 1600, 1410.6113840006647, -268.74138400066477, 230.95023, 59.696087714120488, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (47, 2, 1650, 1444.2631891016652, -302.39318910166526, 230.03320833333336, 59.834990954120492, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (48, 2, 1700, 1478.9916807003017, -337.12168070030179, 226.52661666666666, 59.985890537453834, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (49, 2, 1750, 1515.2962443542233, -373.4262443542234, 229.81134833333331, 60.139302514120487, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (50, 2, 1800, 1553.0053544226198, -411.13535442261991, 231.79401166666665, 60.308863747453827, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.213' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (51, 2, 1850, 1591.7220968126981, -449.85209681269816, 234.27923833333333, 61.6649870941205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (52, 2, 1900, 1631.888351776902, -490.01835177690214, 235.65398000000002, 64.914264150787162, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (53, 2, 1950, 1673.273815221288, -531.40381522128814, 235.77982333333333, 68.269941767453844, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (54, 2, 2000, 1716.139753766982, -574.26975376698215, 234.93347333333335, 71.732743837453825, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (55, 2, 2050, 1760.4861581759058, -618.61615817590587, 234.92566666666664, 75.335852344120482, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (56, 2, 2080, 1787.8214999761269, -645.951499976127, 233.61583587628866, 77.262579677110324, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (57, 3, 0, 0, 1141.67, 22.616499166666671, 51.704616692787127, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (58, 3, 200, 199.877187396842, 941.992812603158, 32.515115333333334, 52.891634440787158, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (59, 3, 400, 398.71290488863292, 743.157095111367, 41.813940333333328, 54.2012830841205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (60, 3, 600, 595.89582871565642, 545.97417128434347, 84.921875, 55.694133254120494, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (61, 3, 800, 785.0796422863101, 356.79035771368979, 127.88067066666667, 56.987813614120505, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (62, 3, 1000, 960.94387000664881, 180.92612999335108, 169.60823499999998, 58.082869860787163, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (63, 3, 1050, 1002.5979994997364, 139.27200050026352, 177.1247433333333, 58.323842714120495, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (64, 3, 1100, 1043.1269470562531, 98.743052943746761, 182.883455, 58.549383507453825, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.217' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (65, 3, 1150, 1082.642113380042, 59.227886619957872, 211.58753333333334, 58.751779804120488, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (66, 3, 1200, 1121.5940371357019, 20.275962864297981, 232.44081666666668, 58.924929797453821, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (67, 3, 1250, 1158.665765749887, -16.795765749887096, 240.62236333333331, 59.07869882078716, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (68, 3, 1300, 1197.8912884273834, -56.021288427383524, 250.31075333333334, 59.223677407453835, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (69, 3, 1350, 1234.6055121529341, -92.735512152934234, 242.41657166666664, 59.35735136745383, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (70, 3, 1400, 1270.6837560482716, -128.81375604827167, 242.64305875000002, 59.5067251316205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (71, 3, 1450, 1306.5221765948077, -164.65217659480777, 242.36133666666669, 59.65556242745383, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (72, 3, 1500, 1341.8555549069301, -199.98555490693025, 242.52014166666666, 59.800721007453838, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (73, 3, 1550, 1376.5968134073708, -234.72681340737086, 242.72315666666668, 59.941572650787165, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (74, 3, 1600, 1410.6113840006647, -268.74138400066477, 241.61988571428569, 60.080119054120487, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (75, 3, 1650, 1444.2631891016652, -302.39318910166526, 242.64812333333336, 60.217174620787162, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (76, 3, 1700, 1478.9916807003017, -337.12168070030179, 242.57033166666668, 60.359642120787157, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (77, 3, 1750, 1515.2962443542233, -373.4262443542234, 243.32951333333335, 60.508569617453823, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.220' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (78, 3, 1800, 1553.0053544226198, -411.13535442261991, 246.17139166666667, 60.66628657078715, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (79, 3, 1850, 1591.7220968126981, -449.85209681269816, 249.374915, 61.445185834120494, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (80, 3, 1900, 1631.888351776902, -490.01835177690214, 249.17283833333332, 64.608875080787158, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (81, 3, 1950, 1673.273815221288, -531.40381522128814, 249.6240766666667, 67.895295280787167, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (82, 3, 2000, 1716.139753766982, -574.26975376698215, 249.20743666666667, 71.255100984120489, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (83, 3, 2050, 1760.4861581759058, -618.61615817590587, 249.66698499999998, 74.774599320787161, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (84, 3, 2080, 1787.8214999761269, -645.951499976127, 247.85326771929815, 76.805712493535552, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (85, 4, 0, 0, 1141.67, 24.848766572580626, 51.880507559604389, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (86, 4, 200, 199.877187396842, 941.992812603158, 33.064463166666663, 53.063882344120493, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (87, 4, 400, 398.71290488863292, 743.157095111367, 43.180217833333337, 54.387889947453829, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (88, 4, 600, 595.89582871565642, 545.97417128434347, 90.919833166666663, 55.862796807453826, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (89, 4, 800, 785.0796422863101, 356.79035771368979, 136.78740666666667, 57.127589870787162, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (90, 4, 1000, 960.94387000664881, 180.92612999335108, 181.40913999999998, 58.187474407453834, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.223' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (91, 4, 1050, 1002.5979994997364, 139.27200050026352, 188.62339833333337, 58.415888724120485, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (92, 4, 1100, 1043.1269470562531, 98.743052943746761, 196.60972333333334, 58.628332797453837, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (93, 4, 1150, 1082.642113380042, 59.227886619957872, 226.248255, 58.8167332241205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (94, 4, 1200, 1121.5940371357019, 20.275962864297981, 245.27048499999998, 58.970681750787165, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (95, 4, 1250, 1158.665765749887, -16.795765749887096, 243.91572499999998, 59.121406077453827, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (96, 4, 1300, 1197.8912884273834, -56.021288427383524, 250.149225, 59.266202220787157, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (97, 4, 1350, 1234.6055121529341, -92.735512152934234, 246.601895, 59.4038242741205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (98, 4, 1400, 1270.6837560482716, -128.81375604827167, 246.23038666666665, 59.5452152441205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (99, 4, 1450, 1306.5221765948077, -164.65217659480777, 246.26356833333333, 59.687321940787157, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (100, 4, 1500, 1341.8555549069301, -199.98555490693025, 247.25283333333337, 59.826558707453835, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (101, 4, 1550, 1376.5968134073708, -234.72681340737086, 247.4626283333333, 59.964360100787154, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (102, 4, 1600, 1410.6113840006647, -268.74138400066477, 246.48453800000002, 60.0990039341205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (103, 4, 1650, 1444.2631891016652, -302.39318910166526, 248.04042666666666, 60.2326058641205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.227' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (104, 4, 1700, 1478.9916807003017, -337.12168070030179, 248.31815999999998, 60.3698680941205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (105, 4, 1750, 1515.2962443542233, -373.4262443542234, 247.68073166666667, 60.512693947453833, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (106, 4, 1800, 1553.0053544226198, -411.13535442261991, 250.34813999999997, 60.660723274120492, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (107, 4, 1850, 1591.7220968126981, -449.85209681269816, 252.94596, 61.502855698120484, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (108, 4, 1900, 1631.888351776902, -490.01835177690214, 252.46779666666666, 64.6456575841205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (109, 4, 1950, 1673.273815221288, -531.40381522128814, 252.98513166666666, 67.908930020787153, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.233' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (110, 4, 2000, 1716.139753766982, -574.26975376698215, 252.65901666666665, 71.261203280787157, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (111, 4, 2050, 1760.4861581759058, -618.61615817590587, 253.23643666666666, 74.7446345141205, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (112, 4, 2080, 1787.8214999761269, -645.951499976127, 251.56236206896566, 76.7554832559596, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (113, 5, 0, 0, 1141.67, 39.11021908158, 50.329045811218108, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (114, 5, 200, 199.877187396842, 941.992812603158, 47.493712534530005, 51.639779734932759, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.237' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (115, 5, 400, 398.71290488863292, 743.157095111367, 85.448073803340009, 52.9505136586474, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (116, 5, 600, 595.89582871565642, 545.97417128434347, 126.29855099226, 54.26124758236206, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (117, 5, 800, 785.0796422863101, 356.79035771368979, 172.02669709926005, 54.698158890266932, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (118, 5, 1000, 960.94387000664881, 180.92612999335108, 195.50047876752, 55.135070198171817, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (119, 5, 1050, 1002.5979994997364, 139.27200050026352, 0, 0, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (120, 5, 1100, 1043.1269470562531, 98.743052943746761, 0, 0, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (121, 5, 1150, 1082.642113380042, 59.227886619957872, 0, 0, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.240' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (122, 5, 1200, 1121.5940371357019, 20.275962864297981, 251.44124417175, 55.57198150607671, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (123, 5, 1250, 1158.665765749887, -16.795765749887096, 254.64221439924, 56.445804121886468, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (124, 5, 1300, 1197.8912884273834, -56.021288427383524, 261.95871777636, 56.7370783271564, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (125, 5, 1350, 1234.6055121529341, -92.735512152934234, 262.26357208374003, 56.7370783271564, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (126, 5, 1400, 1270.6837560482716, -128.81375604827167, 260.12959193208, 56.7370783271564, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (127, 5, 1450, 1306.5221765948077, -164.65217659480777, 259.06260185625, 56.882715429791354, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (128, 5, 1500, 1341.8555549069301, -199.98555490693025, 258.75774754887004, 56.591441224521439, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (129, 5, 1550, 1376.5968134073708, -234.72681340737086, 259.06260185625, 57.028352532426311, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (130, 5, 1600, 1410.6113840006647, -268.74138400066477, 258.60532039518006, 56.7370783271564, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.243' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (131, 5, 1650, 1444.2631891016652, -302.39318910166526, 259.51988331732, 57.028352532426311, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (132, 5, 1700, 1478.9916807003017, -337.12168070030179, 260.89172770053, 57.319626737696225, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (133, 5, 1750, 1515.2962443542233, -373.4262443542234, 260.89172770053, 57.610900942966154, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (134, 5, 1800, 1553.0053544226198, -411.13535442261991, 260.89172770053, 57.7565380456011, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (135, 5, 1850, 1591.7220968126981, -449.85209681269816, 262.11114493005005, 58.484723558775912, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (136, 5, 1900, 1631.888351776902, -490.01835177690214, 262.8732806985, 61.543102714110091, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.247' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (137, 5, 1950, 1673.273815221288, -531.40381522128814, 262.56842639112, 65.038393177349136, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (138, 5, 2000, 1716.139753766982, -574.26975376698215, 263.33056215957004, 68.096772332683315, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (139, 5, 2050, 1760.4861581759058, -618.61615817590587, 263.17813500588, 71.446425693287409, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (140, 5, 2080, 1787.8214999761269, -645.951499976127, 262.41599923743, 73.922256438081746, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (141, 6, 0, 0, 1141.67, 50.76982480902, 49.719174950608533, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (142, 6, 200, 199.877187396842, 941.992812603158, 50.76982480902, 50.884271771688212, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.253' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (143, 6, 400, 398.71290488863292, 743.157095111367, 60.196780423199996, 52.195005695402855, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.257' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.257' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (144, 6, 600, 595.89582871565642, 545.97417128434347, 107.86515975527999, 53.505739619117513, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.257' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.257' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (145, 6, 800, 785.0796422863101, 356.79035771368979, 156.77860869678, 54.088288029657356, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.257' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.257' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (146, 6, 1000, 960.94387000664881, 180.92612999335108, 212.62887403361998, 54.525199337562242, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.257' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.257' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (147, 6, 1050, 1002.5979994997364, 139.27200050026352, 228.10331060783997, 54.816473542832163, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.257' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.257' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (148, 6, 1100, 1043.1269470562531, 98.743052943746761, 239.30893709262, 55.399021953372007, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (149, 6, 1150, 1082.642113380042, 59.227886619957872, 257.27351288568, 55.399021953372007, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (150, 6, 1200, 1121.5940371357019, 20.275962864297981, 269.1906077187, 55.98157036391185, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (151, 6, 1250, 1158.665765749887, -16.795765749887096, 271.14714567635997, 56.272844569181764, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (152, 6, 1300, 1197.8912884273834, -56.021288427383524, 273.45941780814, 56.418481671816721, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (153, 6, 1350, 1234.6055121529341, -92.735512152934234, 275.06022159168, 56.70975587708665, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.260' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (154, 6, 1400, 1270.6837560482716, -128.81375604827167, 273.45941780814, 56.855392979721593, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.263' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.263' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (155, 6, 1450, 1306.5221765948077, -164.65217659480777, 273.28155072107995, 57.001030082356564, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.263' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.263' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (156, 6, 1500, 1341.8555549069301, -199.98555490693025, 273.45941780814, 57.146667184991522, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.263' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.263' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (157, 6, 1550, 1376.5968134073708, -234.72681340737086, 273.99301906932, 57.292304287626479, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.263' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.263' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (158, 6, 1600, 1410.6113840006647, -268.74138400066477, 274.17088615638, 57.43794139026145, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.263' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.263' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (159, 6, 1650, 1444.2631891016652, -302.39318910166526, 274.88235450462, 57.729215595531379, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (160, 6, 1700, 1478.9916807003017, -337.12168070030179, 276.12742411403997, 57.729215595531379, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (161, 6, 1750, 1515.2962443542233, -373.4262443542234, 276.12742411403997, 57.874852698166336, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (162, 6, 1800, 1553.0053544226198, -411.13535442261991, 275.4159557658, 57.874852698166336, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (163, 6, 1850, 1591.7220968126981, -449.85209681269816, 276.12742411403997, 58.7486753139761, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (164, 6, 1900, 1631.888351776902, -490.01835177690214, 276.12742411403997, 61.95269157194523, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.267' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (165, 6, 1950, 1673.273815221288, -531.40381522128814, 275.4159557658, 65.447982035184282, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.273' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.273' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (166, 6, 2000, 1716.139753766982, -574.26975376698215, 275.94955702698, 68.506361190518462, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.273' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.273' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (167, 6, 2050, 1760.4861581759058, -618.61615817590587, 276.66102537522, 72.147288756392484, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.273' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.273' AS DateTime))
GO
INSERT [dbo].[ExplorationDetail] ([Id], [ExplorationId], [Mku], [Mkt], [Mdpl], [Sds], [Tds], [IsDeleted], [CreatedBy], [CreatedDateTime], [ModifiedBy], [ModifiedDateTime]) VALUES (168, 6, 2080, 1787.8214999761269, -645.951499976127, 276.48315828816, 74.040571090646978, 0, N'superadmin', CAST(N'2018-11-30 10:02:07.273' AS DateTime), N'superadmin', CAST(N'2018-11-30 10:02:07.273' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[ExplorationDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[Pltp] ON 

GO
INSERT [dbo].[Pltp] ([Id], [TaskReportId], [PowerPlantId], [EnergyGross], [EnergyNett], [SteamUtilization], [WasteRockMufller], [Ssc], [GppsShutdown], [ImportEnergy], [SgssShutdown], [Notes], [EnergyGrossUnit], [EnergyNettUnit], [SteamUtilizationUnit], [WasteRockMufllerUnit], [SscUnit], [GppsShutdownUnit], [ImportEnergyUnit], [SgssShutdownUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, 2, 1024, 262069, 0, 1936.68994140625, 1231.31005859375, 7.3899998664855957, 0, 0, 0, NULL, N'KW_HR', N'KW_HR', N'T', N'USTON', N'TON_MW_HR', N'HR', N'KW_HR', N'HR', N'superadmin', CAST(N'2018-11-30 03:25:13.000' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.827' AS DateTime), 0)
GO
INSERT [dbo].[Pltp] ([Id], [TaskReportId], [PowerPlantId], [EnergyGross], [EnergyNett], [SteamUtilization], [WasteRockMufller], [Ssc], [GppsShutdown], [ImportEnergy], [SgssShutdown], [Notes], [EnergyGrossUnit], [EnergyNettUnit], [SteamUtilizationUnit], [WasteRockMufllerUnit], [SscUnit], [GppsShutdownUnit], [ImportEnergyUnit], [SgssShutdownUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 2, 1025, 425382, 0, 3190.364990234375, 169.63499450683594, 7.5, 0, 0, 0, NULL, N'KW_HR', N'KW_HR', N'T', N'USTON', N'TON_MW_HR', N'HR', N'KW_HR', N'HR', N'superadmin', CAST(N'2018-11-30 03:25:13.030' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.837' AS DateTime), 0)
GO
INSERT [dbo].[Pltp] ([Id], [TaskReportId], [PowerPlantId], [EnergyGross], [EnergyNett], [SteamUtilization], [WasteRockMufller], [Ssc], [GppsShutdown], [ImportEnergy], [SgssShutdown], [Notes], [EnergyGrossUnit], [EnergyNettUnit], [SteamUtilizationUnit], [WasteRockMufllerUnit], [SscUnit], [GppsShutdownUnit], [ImportEnergyUnit], [SgssShutdownUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 2, 1026, 291000, 0, 2877.360107421875, 23.040000915527344, 10.119999885559082, 0, 0, 0, NULL, N'KW_HR', N'KW_HR', N'T', N'USTON', N'TON_MW_HR', N'HR', N'KW_HR', N'HR', N'superadmin', CAST(N'2018-11-30 03:25:13.050' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.843' AS DateTime), 0)
GO
INSERT [dbo].[Pltp] ([Id], [TaskReportId], [PowerPlantId], [EnergyGross], [EnergyNett], [SteamUtilization], [WasteRockMufller], [Ssc], [GppsShutdown], [ImportEnergy], [SgssShutdown], [Notes], [EnergyGrossUnit], [EnergyNettUnit], [SteamUtilizationUnit], [WasteRockMufllerUnit], [SscUnit], [GppsShutdownUnit], [ImportEnergyUnit], [SgssShutdownUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (4, 2, 1027, 239900, 0, 2400, 0, 10.270000457763672, 0, 0, 0, NULL, N'KW_HR', N'KW_HR', N'T', N'USTON', N'TON_MW_HR', N'HR', N'KW_HR', N'HR', N'superadmin', CAST(N'2018-11-30 03:25:13.070' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.850' AS DateTime), 0)
GO
INSERT [dbo].[Pltp] ([Id], [TaskReportId], [PowerPlantId], [EnergyGross], [EnergyNett], [SteamUtilization], [WasteRockMufller], [Ssc], [GppsShutdown], [ImportEnergy], [SgssShutdown], [Notes], [EnergyGrossUnit], [EnergyNettUnit], [SteamUtilizationUnit], [WasteRockMufllerUnit], [SscUnit], [GppsShutdownUnit], [ImportEnergyUnit], [SgssShutdownUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (5, 2, 1028, 0, 496474, 3376.25, 314.5, 6.8000001907348633, 0, 0, 0, NULL, N'KW_HR', N'KW_HR', N'T', N'USTON', N'TON_MW_HR', N'HR', N'KW_HR', N'HR', N'superadmin', CAST(N'2018-11-30 03:25:13.083' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.863' AS DateTime), 0)
GO
INSERT [dbo].[Pltp] ([Id], [TaskReportId], [PowerPlantId], [EnergyGross], [EnergyNett], [SteamUtilization], [WasteRockMufller], [Ssc], [GppsShutdown], [ImportEnergy], [SgssShutdown], [Notes], [EnergyGrossUnit], [EnergyNettUnit], [SteamUtilizationUnit], [WasteRockMufllerUnit], [SscUnit], [GppsShutdownUnit], [ImportEnergyUnit], [SgssShutdownUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (6, 2, 1029, 0, 484869, 3361, 114.55000305175781, 6.929999828338623, 0, 0, 0, NULL, N'KW_HR', N'KW_HR', N'T', N'USTON', N'TON_MW_HR', N'HR', N'KW_HR', N'HR', N'superadmin', CAST(N'2018-11-30 03:25:13.100' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.870' AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[Pltp] OFF
GO
SET IDENTITY_INSERT [dbo].[PowerPlant] ON 

GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1018, 1, N'718177', N'PLN KAMOJANG AREA I', N'', N'821', N'E02A', 1, N'GEOTHERMAL_STEAM', 30000, N'ZPGE', N'', N'', NULL, N'', N'', 0, 1, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_KAMOJANG_UAP.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1019, 1, N'751197', N'PLN KAMOJANG AREA II', N'', N'822', N'E02B', 1, N'GEOTHERMAL_STEAM', 55000, N'ZPGE', N'', N'', NULL, N'', N'', 0, 1, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_KAMOJANG_UAP.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1020, 1, N'751198', N'PLN KAMOJANG AREA III', N'', N'822', N'E02C', 1, N'GEOTHERMAL_STEAM', 55000, N'ZPGE', N'', N'', NULL, N'', N'', 0, 1, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_KAMOJANG_UAP.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1021, 1, N'751200', N'PLN KAMOJANG AREA IV', N'', N'823', N'E02D', 2, N'ELECTRIC_POWER', 60854, N'ZPGE', N'', N'KAMOJANG PROJECT ARESC AMENDMENT AGREEMENT', CAST(N'2004-01-26 00:00:00.000' AS DateTime), N'', N'', 0.094, 2, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_KAMOJANG_LISTRIK.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1022, 1, N'858632', N'PLN KAMOJANG AREA V', N'', N'832', N'E02E', 2, N'ELECTRIC_POWER', 33000, N'ZPGE', N'', N'PERJANJIAN JUAL BELI TENAGA LISTRIK UNTUK PLTP KAMOJANG UNIT V', CAST(N'2011-03-11 00:00:00.000' AS DateTime), N'', N'', 0.094, 3, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_KAMOJANG_LISTRIK.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1023, 3, N'880142', N'PLN KARAHA AREA I', N'', N'837', N'E06A', 2, N'ELECTRIC_POWER', 30500, N'ZPGE', N'', N'PERJANJIAN JUAL BELI TENAGA LISTRIK PLTP KARAHA', CAST(N'2011-03-11 00:00:00.000' AS DateTime), N'', N'', 0.0825, 10, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_KARAHA_LISTRIK.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1024, 2, N'718178', N'LHD - Unit 1 (PLN)', N'', N'824', N'E03A', 2, N'GEOTHERMAL_STEAM', 20000, N'ZPGE', N'', N'', NULL, N'Schlumberger', N'-', 0, 7, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_LAHENDONG_UAP.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1025, 2, N'751201', N'LHD - Unit 2 (PLN)', N'', N'825', N'E03B', 2, N'GEOTHERMAL_STEAM', 18000, N'ZPGE', N'', N'', NULL, N'Schneider', N'ION 7650', 0, 7, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_LAHENDONG_UAP.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1026, 2, N'751202', N'LHD - Unit 3 (PLN)', N'', N'828', N'E03C', 2, N'GEOTHERMAL_STEAM', 18000, N'ZPGE', N'', N'', NULL, N'Schneider', N'ION 7650', 0, 7, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_LAHENDONG_UAP.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1027, 2, N'811806', N'LHD - Unit 4 (PLN)', N'', N'829', N'E03D', 2, N'GEOTHERMAL_STEAM', 20000, N'ZPGE', N'', N'', NULL, N'Schneider', N'ION 7650', 0, 7, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_LAHENDONG_UAP.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1028, 2, N'880143', N'LHD - Unit 5 (PGE)', N'', N'835', N'E03E', 2, N'ELECTRIC_POWER', 20105, N'ZPGE', N'', N'LAHENDONG PROJECT AMENDMENT AGREEMENT', CAST(N'2016-06-30 00:00:00.000' AS DateTime), N'', N'', 0.114147, 8, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_LAHENDONG_LISTRIK.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1029, 2, N'880164', N'LHD - Unit 6 (PGE)', N'', N'836', N'E03F', 2, N'ELECTRIC_POWER', 20272, N'ZPGE', N'', N'LAHENDONG PROJECT AMENDMENT AGREEMENT', CAST(N'2016-06-30 00:00:00.000' AS DateTime), N'', N'', 0.114147, 9, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_LAHENDONG_LISTRIK.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1030, 4, N'819701', N'PLN ULUBELU AREA I', N'', N'830', N'E04A', 2, N'GEOTHERMAL_STEAM', 55000, N'ZPGE', N'', N'', NULL, N'EDMI', N'MK6E #216439412', 0, 4, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_ULUBELU_UAP.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1031, 4, N'819702', N'PLN ULUBELU AREA II', N'', N'831', N'E04B', 2, N'GEOTHERMAL_STEAM', 55000, N'ZPGE', N'', N'', NULL, N'EDMI', N'MK6E #216439413', 0, 4, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_ULUBELU_UAP.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1032, 4, N'880165', N'PLN ULUBELU AREA III', N'', N'833', N'E04C', 2, N'ELECTRIC_POWER', 50058, N'ZPGE', N'', N'PERJANJIAN JUAL BELI TENAGA LISTRIK PLTP ULUBELU UNIT III', NULL, N'', N'', 0, 5, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_ULUBELU_LISTRIK.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1033, 4, N'880166', N'PLN ULUBELU AREA IV', N'', N'834', N'E04D', 2, N'ELECTRIC_POWER', 40804, N'ZPGE', N'', N'PERJANJIAN JUAL BELI TENAGA LISTRIK PLTP ULUBELU UNIT IV', NULL, N'', N'', 0, 6, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, N'BA_ULUBELU_LISTRIK.xlsx')
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1034, 5, N'774474', N'SEG DARAJAT UNIT I', N'', N'820', N'CE02', 2, N'GEOTHERMAL_STEAM', 0, N'ZTS', N'', N'', NULL, N'', N'', 0, 11, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1035, 5, N'774467', N'SEG DARAJAT UNIT II', N'', N'820', N'CE03', 2, N'ELECTRIC_POWER', 0, N'ZTS', N'', N'', NULL, N'', N'', 0, 12, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1036, 5, N'774465', N'SEG DARAJAT UNIT III', N'', N'820', N'CE04', 2, N'ELECTRIC_POWER', 0, N'ZTS', N'', N'', NULL, N'', N'', 0, 13, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1037, 6, N'774468', N'SEG SALAK PRATAMA UNIT IV-VI', N'', N'820', N'CE06', 2, N'GEOTHERMAL_STEAM', 0, N'ZTS', N'', N'', NULL, N'', N'', 0, 14, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, NULL)
GO
INSERT [dbo].[PowerPlant] ([Id], [AreaId], [Code], [Name], [Address], [SalesGroup], [ShippingPoint], [SoldToPartyId], [Material], [URC], [SOType], [Description], [AgreementName], [AgreementDate], [MeterName], [MeterType], [BasePrice], [InvoiceGroup], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted], [TemplateBastFileName]) VALUES (1038, 6, N'774466', N'SEG SALAK UNIT I-III', N'', N'820', N'CE05', 2, N'ELECTRIC_POWER', 0, N'ZTS', N'', N'', NULL, N'', N'', 0, 15, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[PowerPlant] OFF
GO
SET IDENTITY_INSERT [dbo].[Production] ON 

GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, 2, 20, 19.209999084472656, 0, 0, 0, 0, 0, 0, 0, N'TEST', N'Uji Produksi', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.137' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.890' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 2, 21, 15.800000190734863, 13.069999694824219, 187, 30, 0, 0, 0, 0, N'OPERATION', N'Suplay Ke unit 1', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.183' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.907' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 2, 22, 16.1299991607666, 13.329999923706055, 193, 35, 0, 0, 0, 0, N'OPERATION', N'Suplay Ke unit 1', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.197' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.913' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (4, 2, 23, 27, 18.100000381469727, 231.33000183105469, 33, 0, 0, 0, 0, N'OPERATION', N'Suplay Ke unit 1', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.197' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.923' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (5, 2, 24, 16, 15.130000114440918, 0, 51.599998474121094, 0, 0, 0, 0, N'OPERATION', N'Suplay Ke unit 1', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.233' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.930' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (6, 2, 25, 11.300000190734863, 10.529999732971191, 180.5, 68, 0, 0, 0, 0, N'OPERATION', N'Suplay Ke unit 2', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.250' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.937' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (7, 2, 26, 12.800000190734863, 10.5, 175, 100, 0, 0, 0, 0, N'OPERATION', N'Suplay Ke unit 2', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.267' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.947' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (8, 2, 27, 4.8299999237060547, 3.2999999523162842, 0, 50, 0, 0, 0, 0, N'TEST', N'Buka datar', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.283' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.953' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (9, 2, 28, 14.300000190734863, 10.800000190734863, 175.61000061035156, 100, 0, 0, 0, 0, N'OPERATION', N'Suplay ke Unit 3', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.303' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.957' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (10, 2, 29, 27.5, 14.670000076293945, 197, 10, 0, 0, 0, 0, N'OPERATION', N'Suplay ke Unit 1', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.327' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.967' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (11, 2, 30, 84.330001831054688, 0.76999998092651367, 125.19999694824219, 27, 0, 0, 0, 0, N'SHUT_IN', N'Buka datar', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.333' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.973' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (12, 2, 31, 21, 13.6899995803833, 214.19999694824219, 21, 0, 0, 0, 0, N'OPERATION', N'Suplay ke Unit 3', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.350' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.983' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (13, 2, 32, -0.800000011920929, 0, 0, 0, 0, 0, 0, 0, N'TEST', N'Quenching', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.367' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.987' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (14, 2, 33, 11.630000114440918, 8.369999885559082, 173, 35, 0, 0, 0, 0, N'OPERATION', N'Suplay ke Unit 3', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.387' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.990' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (15, 2, 34, 10.0600004196167, 0.800000011920929, 95, 15, 0, 0, 0, 0, N'TEST', N'Uji Produksi', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.403' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.993' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (16, 2, 35, 13.199999809265137, 5.6700000762939453, 130, 10, 0, 0, 0, 0, N'OPERATION', N'Suplay ke Binery', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.413' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:13.997' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (17, 2, 36, 4.9000000953674316, 2, 130, 10, 0, 0, 0, 0, N'TEST', N'Buka datar', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.433' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:14.007' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (18, 2, 37, 15.260000228881836, 14.899999618530273, 0, 25, 0, 0, 0, 0, N'OPERATION', N'Suplay ke unit 5', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.450' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:14.013' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (19, 2, 38, 14.800000190734863, 14.930000305175781, 0, 28, 0, 0, 0, 0, N'OPERATION', N'Suplay ke Unit 5', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.493' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:14.023' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (20, 2, 39, 28.940000534057617, 28.790000915527344, 0, 36, 0, 0, 0, 0, N'OPERATION', N'Suplay ke Unit 5', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.503' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:14.030' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (21, 2, 40, 12.5600004196167, 12.260000228881836, 0, 25, 0, 0, 0, 0, N'OPERATION', N'Suplay ke Unit 6', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.513' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:14.037' AS DateTime), 0)
GO
INSERT [dbo].[Production] ([Id], [TaskReportId], [WellId], [Tks], [PressureLine], [Temperature], [ThrottleValve], [FlowRate], [FlowTotal], [PressureUpstream], [DifferentialPressure], [Status], [Notes], [TksUnit], [PressureLineUnit], [TemperatureUnit], [ThrottleValveUnit], [FlowRateUnit], [FlowTotalUnit], [PressureUpstreamUnit], [DifferentialPressureUnit], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (22, 2, 41, 18.969999313354492, 18.3799991607666, 0, 75, 0, 0, 0, 0, N'OPERATION', N'Suplay ke Unit 6', N'BARG', N'BARG', N'DEG_C', N'PERCENT', N'T_HR', N'T', N'BARG', N'BARG', N'superadmin', CAST(N'2018-11-30 03:25:13.523' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:14.043' AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[Production] OFF
GO
SET IDENTITY_INSERT [dbo].[SoldToParty] ON 

GO
INSERT [dbo].[SoldToParty] ([Id], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'PT. INDONESIA POWER', N'704665', N'PT. INDONESIA POWER', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[SoldToParty] ([Id], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, N'PT PLN', N'700019', N'700019 - PT PLN', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[SoldToParty] OFF
GO
SET IDENTITY_INSERT [dbo].[TaskReport] ON 

GO
INSERT [dbo].[TaskReport] ([Id], [AreaId], [Periode], [SourceInput], [Status], [ApprovedBy], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, 1, CAST(N'2018-11-27 00:00:00.000' AS DateTime), N'WEB', N'WAITING', NULL, N'User.UserName', CAST(N'2018-11-27 10:33:33.700' AS DateTime), N'User.UserName', CAST(N'2018-11-27 10:33:33.700' AS DateTime), 1)
GO
INSERT [dbo].[TaskReport] ([Id], [AreaId], [Periode], [SourceInput], [Status], [ApprovedBy], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 2, CAST(N'2018-10-02 00:00:00.000' AS DateTime), N'WEB', N'APPROVED', N'superadmin', N'superadmin', CAST(N'2018-11-30 03:25:12.947' AS DateTime), N'superadmin', CAST(N'2018-11-30 03:25:41.690' AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[TaskReport] OFF
GO
SET IDENTITY_INSERT [dbo].[Well] ON 

GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, 1, NULL, N'KMJ.11', N'KMJ.11', N'Kamojang 11', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 1, NULL, N'KMJ.14', N'KMJ.14', N'Kamojang 14', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 1, NULL, N'KMJ.17', N'KMJ.17', N'Kamojang 17', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (4, 1, NULL, N'KMJ.18', N'KMJ.18', N'Kamojang 18', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (5, 1, NULL, N'KMJ.22', N'KMJ.22', N'Kamojang 22', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (6, 1, NULL, N'KMJ.24', N'KMJ.24', N'Kamojang 24', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (7, 1, NULL, N'KMJ.25', N'KMJ.25', N'Kamojang 25', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (8, 1, NULL, N'KMJ.26', N'KMJ.26', N'Kamojang 26', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (9, 1, NULL, N'KMJ.27', N'KMJ.27', N'Kamojang 27', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (10, 1, NULL, N'KMJ.28', N'KMJ.28', N'Kamojang 28', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (11, 1, NULL, N'KMJ.31', N'KMJ.31', N'Kamojang 31', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (12, 1, NULL, N'KMJ.33', N'KMJ.33', N'Kamojang 33', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (13, 1, NULL, N'KMJ.34', N'KMJ.34', N'Kamojang 34', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (14, 1, NULL, N'KMJ.37', N'KMJ.37', N'Kamojang 37', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (15, 1, NULL, N'KMJ.38', N'KMJ.38', N'Kamojang 38', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (16, 1, NULL, N'KMJ.41', N'KMJ.41', N'Kamojang 41', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (17, 1, NULL, N'KMJ.44', N'KMJ.44', N'Kamojang 44', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (18, 1, NULL, N'KMJ.45', N'KMJ.45', N'Kamojang 45', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (19, 1, NULL, N'KMJ.51', N'KMJ.51', N'Kamojang 51', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (20, 2, NULL, N'LHD.5', N'LHD.5', N'Lahendong Sumur 5', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (21, 2, NULL, N'LHD.8', N'LHD.8', N'Lahendong Sumur 8', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (22, 2, NULL, N'LHD.11', N'LHD.11', N'Lahendong Sumur 11', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (23, 2, NULL, N'LHD.12', N'LHD.12', N'Lahendong Sumur 12', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (24, 2, NULL, N'LHD.15', N'LHD.15', N'Lahendong Sumur 15', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (25, 2, NULL, N'LHD.17', N'LHD.17', N'Lahendong Sumur 17', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (26, 2, NULL, N'LHD.18', N'LHD.18', N'Lahendong Sumur 18', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (27, 2, NULL, N'LHD.23', N'LHD.23', N'Lahendong Sumur 23', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (28, 2, NULL, N'LHD.28', N'LHD.28', N'Lahendong Sumur 28', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (29, 2, NULL, N'LHD.38', N'LHD.38', N'Lahendong Sumur 38', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (30, 2, NULL, N'LHD.47', N'LHD.47', N'Lahendong Sumur 47', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (31, 2, NULL, N'LHD.48', N'LHD.48', N'Lahendong Sumur 48', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (32, 2, NULL, N'LHD.49', N'LHD.49', N'Lahendong Sumur 49', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (33, 2, NULL, N'LHD.37', N'LHD.37', N'Lahendong Sumur 37', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (34, 2, NULL, N'LHD.29', N'LHD.29', N'Lahendong Sumur 29', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (35, 2, NULL, N'LHD.21', N'LHD.21', N'Lahendong Sumur 21', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (36, 2, NULL, N'LHD.53', N'LHD.53', N'Lahendong Sumur 53', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (37, 2, NULL, N'LHD.27', N'LHD.27', N'Lahendong Sumur 27', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (38, 2, NULL, N'LHD.31', N'LHD.31', N'Lahendong Sumur 31', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (39, 2, NULL, N'LHD.34', N'LHD.34', N'Lahendong Sumur 34', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (40, 2, NULL, N'LHD.42', N'LHD.42', N'Lahendong Sumur 42', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Well] ([Id], [AreaId], [ClusterId], [Name], [Code], [Description], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (41, 2, NULL, N'LHD.43', N'LHD.43', N'Lahendong Sumur 43', N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), N'Superadmin', CAST(N'2018-11-21 00:00:00.000' AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[Well] OFF
GO
SET IDENTITY_INSERT [dbo].[Workflow] ON 

GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (3, N'ZPGE', N'DRAFT', N'Upload Sales Order', NULL, N'Send to Area Operation Manager', 0, 0, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (4, N'ZPGE', N'AREA_OPS_MANAGER_VERIFICATION', N'Area Ops Manager Verification', 3, N'Send to Area Finance', 0, 0, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (5, N'ZPGE', N'AREA_FINANCE_VERIFICATION', N'Area Finance Verification', 4, N'Approve', 1, 1, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (6, N'ZPGE', N'FINISH', N'Finish', 11, NULL, 0, 1, 0, 1)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (7, N'ZTS', N'DRAFT', N'Draft', NULL, N'Send to Partnership Manager', 0, 0, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (8, N'ZTS', N'PARTNERSHIP_MANAGER_VERIFICATION', N'Partnership Manager Verification', 7, N'Update PR dan PO', 0, 0, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (9, N'ZTS', N'UPDATE_PR_AND_PO', N'Update PR dan PO', 8, N'Send to Manager Ops OE', 0, 1, 1, 1)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (10, N'ZTS', N'FINISH', N'Finish', 9, NULL, 0, 1, 0, 1)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (11, N'ZPGE', N'UPLOAD_BAST', N'Upload BAST', 5, N'Mark as Finished', 1, 1, 1, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (14, N'PRICE', N'DRAFT', N'Draft', NULL, N'Send to Manager Ops', 0, 0, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (15, N'PRICE', N'MANAGER_OPS_VERIFICATION', N'Manager Ops Verification', 14, N'Send to OE', 0, 0, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (17, N'PRICE', N'OE_VERIFICATION', N'OE Verification', 15, N'Send to Manager Ops OE', 0, 0, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (18, N'PRICE', N'FINISH', N'Finish', 19, NULL, 0, 0, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (19, N'PRICE', N'MANAGER_OPS_OE_VERIFICATION', N'Manager Ops OE Verification', 17, N'Mark as Finished', 0, 1, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (1019, N'INVEL', N'PRINT_COVER_LETTER', N'Print Surat Pengantar', NULL, N'Send E-Faktur Request', 1, 0, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (1020, N'INVEL', N'UPLOAD_EFAKTUR', N'Upload E-Faktur', 1019, N'Send to Asment AP AR', 0, 1, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (1021, N'INVEL', N'UPLOAD_INVOICE', N'Upload Invoice', 1020, N'Send to Treasury', 0, 0, 1, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (1022, N'INVEL', N'UPDATE_CLEARING', N'Update Dokumen Kliring', 1021, N'Mark as Finished', 0, 0, 0, 1)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (1023, N'INVEL', N'FINISH', N'Finish', 1022, NULL, 0, 0, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (1026, N'INVSM', N'PRINT_COVER_LETTER', N'Print Surat Pengantar', NULL, N'Send to Asment AP AR', 1, 0, 0, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (1027, N'INVSM', N'UPLOAD_INVOICE', N'Upload Invoice', 1026, N'Send to Treasury', 0, 0, 1, 0)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (1028, N'INVSM', N'UPDATE_CLEARING', N'Update Dokumen Kliring', 1027, N'Mark as Finished', 0, 0, 0, 1)
GO
INSERT [dbo].[Workflow] ([Id], [WorkflowType], [Code], [Name], [PreviousStep], [Approver], [IsBastDownloadable], [IsSapDownloadable], [IsBastSignedUploadable], [IsBastSignedDownloadable]) VALUES (1029, N'INVSM', N'FINISH', N'Finish', 1028, NULL, 0, 0, 0, 0)
GO
SET IDENTITY_INSERT [dbo].[Workflow] OFF
GO
ALTER TABLE [dbo].[BastFile] ADD  CONSTRAINT [DF_BastFile_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[BastFileSigned] ADD  CONSTRAINT [DF_BastFileSigned_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[EFakturFile] ADD  CONSTRAINT [DF_EFakturFile_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Invoice] ADD  CONSTRAINT [DF_Invoice_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[InvoiceFile] ADD  CONSTRAINT [DF_InvoiceFile_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[PPI] ADD  CONSTRAINT [DF_PPI_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ProductionPlan] ADD  CONSTRAINT [DF_PlanYearly_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SalesOrder] ADD  CONSTRAINT [DF_SalesOrder_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SteamPowerPlantPrice] ADD  CONSTRAINT [DF_SteamPowerPlantPrice_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ActualPlanning]  WITH CHECK ADD  CONSTRAINT [FK_ActualPlanning_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ActualPlanning] CHECK CONSTRAINT [FK_ActualPlanning_Area]
GO
ALTER TABLE [dbo].[ActualPlanning]  WITH CHECK ADD  CONSTRAINT [FK_ActualPlanning_TaskReport] FOREIGN KEY([TaskReportId])
REFERENCES [dbo].[TaskReport] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ActualPlanning] CHECK CONSTRAINT [FK_ActualPlanning_TaskReport]
GO
ALTER TABLE [dbo].[Area]  WITH CHECK ADD  CONSTRAINT [FK_Area_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Area] CHECK CONSTRAINT [FK_Area_Company]
GO
ALTER TABLE [dbo].[BastFile]  WITH CHECK ADD  CONSTRAINT [FK_BastFile_SalesOrder] FOREIGN KEY([SalesOrderId])
REFERENCES [dbo].[SalesOrder] ([Id])
GO
ALTER TABLE [dbo].[BastFile] CHECK CONSTRAINT [FK_BastFile_SalesOrder]
GO
ALTER TABLE [dbo].[BastFileSigned]  WITH CHECK ADD  CONSTRAINT [FK_BastFileSigned_SalesOrder] FOREIGN KEY([SalesOrderId])
REFERENCES [dbo].[SalesOrder] ([Id])
GO
ALTER TABLE [dbo].[BastFileSigned] CHECK CONSTRAINT [FK_BastFileSigned_SalesOrder]
GO
ALTER TABLE [dbo].[Cluster]  WITH CHECK ADD  CONSTRAINT [FK_Cluster_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Cluster] CHECK CONSTRAINT [FK_Cluster_Area]
GO
ALTER TABLE [dbo].[Dkp]  WITH CHECK ADD  CONSTRAINT [FK_Dkp_PowerPlant] FOREIGN KEY([PowerPlantId])
REFERENCES [dbo].[PowerPlant] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Dkp] CHECK CONSTRAINT [FK_Dkp_PowerPlant]
GO
ALTER TABLE [dbo].[Dkp]  WITH CHECK ADD  CONSTRAINT [FK_Dkp_TaskReport] FOREIGN KEY([TaskReportId])
REFERENCES [dbo].[TaskReport] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Dkp] CHECK CONSTRAINT [FK_Dkp_TaskReport]
GO
ALTER TABLE [dbo].[EFakturFile]  WITH CHECK ADD  CONSTRAINT [FK_EFakturFile_Invoice] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoice] ([Id])
GO
ALTER TABLE [dbo].[EFakturFile] CHECK CONSTRAINT [FK_EFakturFile_Invoice]
GO
ALTER TABLE [dbo].[Exploration]  WITH CHECK ADD  CONSTRAINT [FK_Exploration_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
GO
ALTER TABLE [dbo].[Exploration] CHECK CONSTRAINT [FK_Exploration_Area]
GO
ALTER TABLE [dbo].[ExplorationDetail]  WITH CHECK ADD  CONSTRAINT [FK_ExplorationDetail_Exploration] FOREIGN KEY([ExplorationId])
REFERENCES [dbo].[Exploration] ([Id])
GO
ALTER TABLE [dbo].[ExplorationDetail] CHECK CONSTRAINT [FK_ExplorationDetail_Exploration]
GO
ALTER TABLE [dbo].[InterfacePoint]  WITH CHECK ADD  CONSTRAINT [FK_InterfacePoint_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[InterfacePoint] CHECK CONSTRAINT [FK_InterfacePoint_Area]
GO
ALTER TABLE [dbo].[InterfaceScrubber]  WITH CHECK ADD  CONSTRAINT [FK_InterfaceScrubber_InterfacePoint] FOREIGN KEY([InterfaceId])
REFERENCES [dbo].[InterfacePoint] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[InterfaceScrubber] CHECK CONSTRAINT [FK_InterfaceScrubber_InterfacePoint]
GO
ALTER TABLE [dbo].[InterfaceScrubber]  WITH CHECK ADD  CONSTRAINT [FK_InterfaceScrubber_Scrubber] FOREIGN KEY([ScrubberId])
REFERENCES [dbo].[Scrubber] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[InterfaceScrubber] CHECK CONSTRAINT [FK_InterfaceScrubber_Scrubber]
GO
ALTER TABLE [dbo].[InterfaceScrubber]  WITH CHECK ADD  CONSTRAINT [FK_InterfaceScrubber_TaskReport] FOREIGN KEY([TaskReportId])
REFERENCES [dbo].[TaskReport] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[InterfaceScrubber] CHECK CONSTRAINT [FK_InterfaceScrubber_TaskReport]
GO
ALTER TABLE [dbo].[Invoice]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_SalesOrder] FOREIGN KEY([SalesOrderId])
REFERENCES [dbo].[SalesOrder] ([Id])
GO
ALTER TABLE [dbo].[Invoice] CHECK CONSTRAINT [FK_Invoice_SalesOrder]
GO
ALTER TABLE [dbo].[Invoice]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Workflow] FOREIGN KEY([WorkflowId])
REFERENCES [dbo].[Workflow] ([Id])
GO
ALTER TABLE [dbo].[Invoice] CHECK CONSTRAINT [FK_Invoice_Workflow]
GO
ALTER TABLE [dbo].[InvoiceApproval]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceApproval_Invoice] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoice] ([Id])
GO
ALTER TABLE [dbo].[InvoiceApproval] CHECK CONSTRAINT [FK_InvoiceApproval_Invoice]
GO
ALTER TABLE [dbo].[InvoiceApproval]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceApproval_Workflow] FOREIGN KEY([WorkflowId])
REFERENCES [dbo].[Workflow] ([Id])
GO
ALTER TABLE [dbo].[InvoiceApproval] CHECK CONSTRAINT [FK_InvoiceApproval_Workflow]
GO
ALTER TABLE [dbo].[InvoiceFile]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceFile_Invoice] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoice] ([Id])
GO
ALTER TABLE [dbo].[InvoiceFile] CHECK CONSTRAINT [FK_InvoiceFile_Invoice]
GO
ALTER TABLE [dbo].[Monitoring]  WITH CHECK ADD  CONSTRAINT [FK_Monitoring_TaskReport] FOREIGN KEY([TaskReportId])
REFERENCES [dbo].[TaskReport] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Monitoring] CHECK CONSTRAINT [FK_Monitoring_TaskReport]
GO
ALTER TABLE [dbo].[Monitoring]  WITH CHECK ADD  CONSTRAINT [FK_Monitoring_Well] FOREIGN KEY([WellId])
REFERENCES [dbo].[Well] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Monitoring] CHECK CONSTRAINT [FK_Monitoring_Well]
GO
ALTER TABLE [dbo].[MstSeparator]  WITH CHECK ADD  CONSTRAINT [FK_MstSeparator_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[MstSeparator] CHECK CONSTRAINT [FK_MstSeparator_Area]
GO
ALTER TABLE [dbo].[Pltp]  WITH CHECK ADD  CONSTRAINT [FK_Pltp_PowerPlant] FOREIGN KEY([PowerPlantId])
REFERENCES [dbo].[PowerPlant] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Pltp] CHECK CONSTRAINT [FK_Pltp_PowerPlant]
GO
ALTER TABLE [dbo].[Pltp]  WITH CHECK ADD  CONSTRAINT [FK_Pltp_TaskReport] FOREIGN KEY([TaskReportId])
REFERENCES [dbo].[TaskReport] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Pltp] CHECK CONSTRAINT [FK_Pltp_TaskReport]
GO
ALTER TABLE [dbo].[PowerPlant]  WITH CHECK ADD  CONSTRAINT [FK_PowerPlant_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[PowerPlant] CHECK CONSTRAINT [FK_PowerPlant_Area]
GO
ALTER TABLE [dbo].[PowerPlant]  WITH CHECK ADD  CONSTRAINT [FK_PowerPlant_SoldToParty] FOREIGN KEY([SoldToPartyId])
REFERENCES [dbo].[SoldToParty] ([Id])
GO
ALTER TABLE [dbo].[PowerPlant] CHECK CONSTRAINT [FK_PowerPlant_SoldToParty]
GO
ALTER TABLE [dbo].[PPI]  WITH CHECK ADD  CONSTRAINT [FK_PPI_PowerPlant] FOREIGN KEY([PowerPlantId])
REFERENCES [dbo].[PowerPlant] ([Id])
GO
ALTER TABLE [dbo].[PPI] CHECK CONSTRAINT [FK_PPI_PowerPlant]
GO
ALTER TABLE [dbo].[PPI]  WITH CHECK ADD  CONSTRAINT [FK_PPI_Workflow] FOREIGN KEY([WorkflowId])
REFERENCES [dbo].[Workflow] ([Id])
GO
ALTER TABLE [dbo].[PPI] CHECK CONSTRAINT [FK_PPI_Workflow]
GO
ALTER TABLE [dbo].[PpiApproval]  WITH CHECK ADD  CONSTRAINT [FK_PPIApproval_PPI] FOREIGN KEY([PpiId])
REFERENCES [dbo].[PPI] ([Id])
GO
ALTER TABLE [dbo].[PpiApproval] CHECK CONSTRAINT [FK_PPIApproval_PPI]
GO
ALTER TABLE [dbo].[PpiApproval]  WITH CHECK ADD  CONSTRAINT [FK_PPIApproval_Workflow] FOREIGN KEY([WorkflowId])
REFERENCES [dbo].[Workflow] ([Id])
GO
ALTER TABLE [dbo].[PpiApproval] CHECK CONSTRAINT [FK_PPIApproval_Workflow]
GO
ALTER TABLE [dbo].[Production]  WITH CHECK ADD  CONSTRAINT [FK_Production_TaskReport] FOREIGN KEY([TaskReportId])
REFERENCES [dbo].[TaskReport] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Production] CHECK CONSTRAINT [FK_Production_TaskReport]
GO
ALTER TABLE [dbo].[Production]  WITH CHECK ADD  CONSTRAINT [FK_Production_Well] FOREIGN KEY([WellId])
REFERENCES [dbo].[Well] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Production] CHECK CONSTRAINT [FK_Production_Well]
GO
ALTER TABLE [dbo].[ProductionPlan]  WITH CHECK ADD  CONSTRAINT [FK_PlanYearly_PowerPlant] FOREIGN KEY([PowerPlantId])
REFERENCES [dbo].[PowerPlant] ([Id])
GO
ALTER TABLE [dbo].[ProductionPlan] CHECK CONSTRAINT [FK_PlanYearly_PowerPlant]
GO
ALTER TABLE [dbo].[ProductionPlanDetail]  WITH CHECK ADD  CONSTRAINT [FK_PlanMonthly_PlanYearly] FOREIGN KEY([ProductionPlanId])
REFERENCES [dbo].[ProductionPlan] ([Id])
GO
ALTER TABLE [dbo].[ProductionPlanDetail] CHECK CONSTRAINT [FK_PlanMonthly_PlanYearly]
GO
ALTER TABLE [dbo].[ReInjection]  WITH CHECK ADD  CONSTRAINT [FK_ReInjection_TaskReport] FOREIGN KEY([TaskReportId])
REFERENCES [dbo].[TaskReport] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ReInjection] CHECK CONSTRAINT [FK_ReInjection_TaskReport]
GO
ALTER TABLE [dbo].[ReInjection]  WITH CHECK ADD  CONSTRAINT [FK_ReInjection_Well] FOREIGN KEY([WellId])
REFERENCES [dbo].[Well] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ReInjection] CHECK CONSTRAINT [FK_ReInjection_Well]
GO
ALTER TABLE [dbo].[SalesOrder]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrder_PowerPlant] FOREIGN KEY([PowerPlantId])
REFERENCES [dbo].[PowerPlant] ([Id])
GO
ALTER TABLE [dbo].[SalesOrder] CHECK CONSTRAINT [FK_SalesOrder_PowerPlant]
GO
ALTER TABLE [dbo].[SalesOrder]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrder_Workflow] FOREIGN KEY([WorkflowId])
REFERENCES [dbo].[Workflow] ([Id])
GO
ALTER TABLE [dbo].[SalesOrder] CHECK CONSTRAINT [FK_SalesOrder_Workflow]
GO
ALTER TABLE [dbo].[SalesOrderApproval]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrderApproval_SalesOrder] FOREIGN KEY([SalesOrderId])
REFERENCES [dbo].[SalesOrder] ([Id])
GO
ALTER TABLE [dbo].[SalesOrderApproval] CHECK CONSTRAINT [FK_SalesOrderApproval_SalesOrder]
GO
ALTER TABLE [dbo].[SalesOrderApproval]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrderApproval_Workflow] FOREIGN KEY([WorkflowId])
REFERENCES [dbo].[Workflow] ([Id])
GO
ALTER TABLE [dbo].[SalesOrderApproval] CHECK CONSTRAINT [FK_SalesOrderApproval_Workflow]
GO
ALTER TABLE [dbo].[Scrubber]  WITH CHECK ADD  CONSTRAINT [FK_Scrubber_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Scrubber] CHECK CONSTRAINT [FK_Scrubber_Area]
GO
ALTER TABLE [dbo].[Separator]  WITH CHECK ADD  CONSTRAINT [FK_Separator_MstSeparator] FOREIGN KEY([SeparatorId])
REFERENCES [dbo].[MstSeparator] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Separator] CHECK CONSTRAINT [FK_Separator_MstSeparator]
GO
ALTER TABLE [dbo].[Separator]  WITH CHECK ADD  CONSTRAINT [FK_Separator_TaskReport] FOREIGN KEY([TaskReportId])
REFERENCES [dbo].[TaskReport] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Separator] CHECK CONSTRAINT [FK_Separator_TaskReport]
GO
ALTER TABLE [dbo].[SteamPowerPlantPrice]  WITH CHECK ADD  CONSTRAINT [FK_SteamPowerPlantPrice_PowerPlant] FOREIGN KEY([PowerPlantId])
REFERENCES [dbo].[PowerPlant] ([Id])
GO
ALTER TABLE [dbo].[SteamPowerPlantPrice] CHECK CONSTRAINT [FK_SteamPowerPlantPrice_PowerPlant]
GO
ALTER TABLE [dbo].[SteamPowerPlantPrice]  WITH CHECK ADD  CONSTRAINT [FK_SteamPowerPlantPrice_Workflow] FOREIGN KEY([WorkflowId])
REFERENCES [dbo].[Workflow] ([Id])
GO
ALTER TABLE [dbo].[SteamPowerPlantPrice] CHECK CONSTRAINT [FK_SteamPowerPlantPrice_Workflow]
GO
ALTER TABLE [dbo].[SteamPowerPlantPriceApproval]  WITH CHECK ADD  CONSTRAINT [FK_SteamPowerPlantPriceApproval_SteamPowerPlantPrice] FOREIGN KEY([SteamPowerPlantPriceId])
REFERENCES [dbo].[SteamPowerPlantPrice] ([Id])
GO
ALTER TABLE [dbo].[SteamPowerPlantPriceApproval] CHECK CONSTRAINT [FK_SteamPowerPlantPriceApproval_SteamPowerPlantPrice]
GO
ALTER TABLE [dbo].[SteamPowerPlantPriceApproval]  WITH CHECK ADD  CONSTRAINT [FK_SteamPowerPlantPriceApproval_Workflow] FOREIGN KEY([WorkflowId])
REFERENCES [dbo].[Workflow] ([Id])
GO
ALTER TABLE [dbo].[SteamPowerPlantPriceApproval] CHECK CONSTRAINT [FK_SteamPowerPlantPriceApproval_Workflow]
GO
ALTER TABLE [dbo].[TaskRelation]  WITH CHECK ADD  CONSTRAINT [FK_TaskRelation_Pltp] FOREIGN KEY([Category])
REFERENCES [dbo].[Pltp] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TaskRelation] CHECK CONSTRAINT [FK_TaskRelation_Pltp]
GO
ALTER TABLE [dbo].[TaskRelation]  WITH CHECK ADD  CONSTRAINT [FK_TaskRelation_Task] FOREIGN KEY([RelationId])
REFERENCES [dbo].[TaskReport] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TaskRelation] CHECK CONSTRAINT [FK_TaskRelation_Task]
GO
ALTER TABLE [dbo].[TaskReport]  WITH CHECK ADD  CONSTRAINT [FK_Task_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TaskReport] CHECK CONSTRAINT [FK_Task_Area]
GO
ALTER TABLE [dbo].[Well]  WITH CHECK ADD  CONSTRAINT [FK_Well_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Well] CHECK CONSTRAINT [FK_Well_Area]
GO
ALTER TABLE [dbo].[Well]  WITH CHECK ADD  CONSTRAINT [FK_Well_Cluster] FOREIGN KEY([ClusterId])
REFERENCES [dbo].[Cluster] ([Id])
GO
ALTER TABLE [dbo].[Well] CHECK CONSTRAINT [FK_Well_Cluster]
GO
USE [master]
GO
ALTER DATABASE [PGE.IPGE.APP] SET  READ_WRITE 
GO
