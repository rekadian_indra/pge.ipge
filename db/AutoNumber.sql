USE [PGE.IPGE.APP]
GO
/****** Object:  Table [dbo].[AutoNumber]    Script Date: 12/01/2019 03.18.10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutoNumber](
	[TransactionCode] [varchar](2) NOT NULL,
	[TransactionName] [varchar](50) NOT NULL,
	[Year] [int] NOT NULL,
	[Prefix] [varchar](50) NOT NULL,
	[IncrementNumber] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[AutoNumber] ([TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (N'01', N'Download Form Miro', 2019, N'PT', 0)
INSERT [dbo].[AutoNumber] ([TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (N'01', N'Download Form Miro', 2020, N'PT', 0)
INSERT [dbo].[AutoNumber] ([TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (N'01', N'Download Form Miro', 2021, N'PT', 0)
INSERT [dbo].[AutoNumber] ([TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (N'01', N'Download Form Miro', 2022, N'PT', 0)
INSERT [dbo].[AutoNumber] ([TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (N'01', N'Download Form Miro', 2023, N'PT', 0)
INSERT [dbo].[AutoNumber] ([TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (N'01', N'Download Form Miro', 2024, N'PT', 0)
INSERT [dbo].[AutoNumber] ([TransactionCode], [TransactionName], [Year], [Prefix], [IncrementNumber]) VALUES (N'01', N'Download Form Miro', 2025, N'PT', 0)
