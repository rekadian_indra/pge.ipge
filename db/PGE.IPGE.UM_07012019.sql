USE [PGE.IPGE.UM]
GO
/****** Object:  Table [dbo].[Actions]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actions](
	[ActionId] [uniqueidentifier] NOT NULL,
	[ActionName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Actions] PRIMARY KEY CLUSTERED 
(
	[ActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ActionsInModules]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActionsInModules](
	[ActionId] [uniqueidentifier] NOT NULL,
	[ModuleId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_ActionsInModules] PRIMARY KEY CLUSTERED 
(
	[ActionId] ASC,
	[ModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ActionsInModulesChosen]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActionsInModulesChosen](
	[ModuleInRoleId] [uniqueidentifier] NOT NULL,
	[ActionId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_ActionsInModulesChosen] PRIMARY KEY CLUSTERED 
(
	[ModuleInRoleId] ASC,
	[ActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Applications]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Applications](
	[ApplicationName] [nvarchar](235) NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Memberships]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Memberships](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[PasswordQuestion] [nvarchar](256) NULL,
	[PasswordAnswer] [nvarchar](128) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowsStart] [datetime] NOT NULL,
	[Comment] [nvarchar](256) NULL,
	[AreaId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Modules]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Modules](
	[ModuleId] [uniqueidentifier] NOT NULL,
	[ModuleName] [nvarchar](256) NOT NULL,
	[ParentModuleId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Modules] PRIMARY KEY CLUSTERED 
(
	[ModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ModulesInRoles]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModulesInRoles](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[ModuleId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_ModulesInRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Profiles]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profiles](
	[UserId] [uniqueidentifier] NOT NULL,
	[PropertyNames] [nvarchar](4000) NOT NULL,
	[PropertyValueStrings] [nvarchar](4000) NOT NULL,
	[PropertyValueBinary] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsersInRoles]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersInRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[ViewUserArea]    Script Date: 07/01/2019 03:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewUserArea]
AS
SELECT u.ApplicationId, u.UserId, u.UserName, u.IsAnonymous, u.LastActivityDate, m.Password, m.PasswordFormat, m.PasswordSalt, m.Email, m.PasswordQuestion, m.PasswordAnswer, m.IsApproved, m.IsLockedOut, m.CreateDate, 
                  m.LastLoginDate, m.LastPasswordChangedDate, m.LastLockoutDate, m.FailedPasswordAttemptCount, m.FailedPasswordAttemptWindowStart, m.FailedPasswordAnswerAttemptCount, m.FailedPasswordAnswerAttemptWindowsStart, 
                  m.Comment, p.Id, p.Code, p.Name, m.AreaId
FROM     dbo.Users AS u INNER JOIN
                  dbo.Memberships AS m ON u.UserId = m.UserId LEFT OUTER JOIN
                  [PGE.IPGE.APP].dbo.Area AS p ON m.AreaId = p.Id




GO
INSERT [dbo].[Applications] ([ApplicationName], [ApplicationId], [Description]) VALUES (N'PGE.IPGE', N'0ca90253-4739-439e-a748-e57b4ddebfe5', NULL)
GO
INSERT [dbo].[Memberships] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [Email], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowsStart], [Comment], [AreaId]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'1ae7c7bb-19f5-42d3-a212-0dc0b2222658', N'iPDJ6FFrEbTuZWlVS0d2g+/ZT2FXoSttw0hwxNWWiBs=', 1, N'GF1zsbU/LDfbxi9SaWq2yQ==', N'mhilman.fadli@pertamina.com', NULL, NULL, 1, 0, CAST(N'2018-12-04 07:39:25.537' AS DateTime), CAST(N'2018-12-04 07:39:25.537' AS DateTime), CAST(N'2018-12-04 07:39:25.537' AS DateTime), CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), NULL, 2)
GO
INSERT [dbo].[Memberships] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [Email], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowsStart], [Comment], [AreaId]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'8b5029cc-9530-40e4-97da-2e7252b4649d', N'897xU2huF+Va1lYRHp5Er1xJlA0teFu8VOM6N1g0nZY=', 1, N'Aa8P+rnt0rju2Fo+4dZGvw==', N'mk.myudistiro@mitrakerja.pertamina.com', NULL, NULL, 1, 0, CAST(N'2018-12-06 14:31:59.487' AS DateTime), CAST(N'2018-12-07 09:29:32.660' AS DateTime), CAST(N'2018-12-06 14:50:37.670' AS DateTime), CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), NULL, 2)
GO
INSERT [dbo].[Memberships] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [Email], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowsStart], [Comment], [AreaId]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'0ff6ac18-b7cb-4d41-8c42-5ef5283b1f2c', N'c0qOZUJwa+4hVUKXmq5EskpbLHiiz9B2qbTuqBcKLZY=', 1, N'Mqf3mNv30kuMs5e+PstFXg==', N'superadmin@mail.com', NULL, NULL, 1, 0, CAST(N'2017-10-16 06:10:32.147' AS DateTime), CAST(N'2018-12-07 03:02:27.187' AS DateTime), CAST(N'2017-10-16 06:10:32.147' AS DateTime), CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Memberships] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [Email], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowsStart], [Comment], [AreaId]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'97ff22da-fd01-4e2c-9658-74f921272246', N'at7J41WCe/35v0Iu1eYlrXp8wG7hhYFaXZU9HAyWnYc=', 1, N'KMUZKVeXWVulrh31ab461g==', N'admina@mail.com', NULL, NULL, 1, 0, CAST(N'2018-04-11 15:55:43.557' AS DateTime), CAST(N'2018-04-11 18:12:29.530' AS DateTime), CAST(N'2018-04-11 15:55:43.557' AS DateTime), CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), NULL, 0)
GO
INSERT [dbo].[Memberships] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [Email], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowsStart], [Comment], [AreaId]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'41e7d879-79a2-44da-9105-92acf0ac3062', N'e2o70apsih3LIJDShtwBBUYfYcA30+OId1t/yFIB3lc=', 1, N'HdPtcueAHlkjtfgzj8qANw==', N'tesha@pertamina.com', NULL, NULL, 1, 0, CAST(N'2018-11-09 08:30:45.917' AS DateTime), CAST(N'2018-11-09 08:30:45.917' AS DateTime), CAST(N'2018-11-09 08:30:45.917' AS DateTime), CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), NULL, 1)
GO
INSERT [dbo].[Memberships] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [Email], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowsStart], [Comment], [AreaId]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'638d476d-b95a-45bb-a356-982c85b8a495', N'qEYUmdi9Sw3hieYkXeHdi6fU7Jl1E40pooSBNWOg9Qc=', 1, N'KvANlq3CS5YUq2hkT2xoYg==', N'budyi.permono@pertamina.com', NULL, NULL, 1, 0, CAST(N'2018-12-07 09:20:09.180' AS DateTime), CAST(N'2018-12-07 09:20:09.180' AS DateTime), CAST(N'2018-12-07 09:20:09.180' AS DateTime), CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), NULL, 2)
GO
INSERT [dbo].[Memberships] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [Email], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowsStart], [Comment], [AreaId]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'5dcfe3ed-1d86-4117-ad74-c14f82d30d7e', N'BUSFO8V9hX5n7O/qHu0NOmFOEmgb2DsdwwpukN4IF78=', 1, N'+DrgVRIQBd+2oY3/CMOepA==', N'reza.rahman.pge@pertamina.com', NULL, NULL, 1, 0, CAST(N'2018-12-07 03:26:29.810' AS DateTime), CAST(N'2018-12-07 03:29:42.703' AS DateTime), CAST(N'2018-12-07 03:29:42.797' AS DateTime), CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Memberships] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [Email], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowsStart], [Comment], [AreaId]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'87615a26-c4a1-4891-8558-ecd7da792e7f', N'YS3EMRtVXxu2W4pfZf0wxjwH8fvMRaNJ4mlennUKdPo=', 1, N'OWth3EiuxFRmiyMRlxDeaw==', N'wanda.kurniawan@Pertamina.com', NULL, NULL, 1, 0, CAST(N'2018-12-07 03:28:23.593' AS DateTime), CAST(N'2018-12-07 03:28:23.593' AS DateTime), CAST(N'2018-12-07 03:28:23.593' AS DateTime), CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Modules] ([ModuleId], [ModuleName], [ParentModuleId]) VALUES (N'40e58de3-f3f1-46f8-bf64-2f3e60497f70', N'Crew', NULL)
GO
INSERT [dbo].[Modules] ([ModuleId], [ModuleName], [ParentModuleId]) VALUES (N'9de4e116-012a-4c42-8657-5ef2c7886c6d', N'Whitelist', NULL)
GO
INSERT [dbo].[Modules] ([ModuleId], [ModuleName], [ParentModuleId]) VALUES (N'3cf8b68b-0ded-4206-8e31-711423f1ebb3', N'Company', N'2cdf85c5-2b85-4c01-8bca-bb41b41a40b9')
GO
INSERT [dbo].[Modules] ([ModuleId], [ModuleName], [ParentModuleId]) VALUES (N'3a17863e-31b4-4b69-abe5-ba2a264b07be', N'User Management', NULL)
GO
INSERT [dbo].[Modules] ([ModuleId], [ModuleName], [ParentModuleId]) VALUES (N'2cdf85c5-2b85-4c01-8bca-bb41b41a40b9', N'Data Master', NULL)
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'5c4b9c72-5a20-49e4-84bb-09586877d691', N'OE_VERIFICATION', N'OE Verification')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'f72e3e4e-0680-43d7-8fa3-0d298f4aef3e', N'FINANCE_AREA', N'Finance Area')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'a9689c4b-a973-4b4b-b773-243340da98cc', N'PARTNERSHIP', N'Partnership Manager Verification')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'8b269900-4d21-4b59-8a5b-2855b56e5bd7', N'MANAGER_AREA', N'Manager Operation Area')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'78972d6f-b66d-4780-9fbb-515f180f0d49', N'OPERATION_ADMIN', N'Operation Admin')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'5e37f8e3-e446-4f49-b29c-5ce1cfa60220', N'ASMEN', N'Asmen Operation Manager Area')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'e9b1c009-0197-4f88-857d-6dc14db7843a', N'OPERATION_OPERATOR', N'Operation Operator')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'c71497bb-6c8d-4c94-912c-987f9c2df209', N'OPERATION_SUPERVISOR', N'Operation Supervisor')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'f32b008e-cc0f-4494-b39d-c731e96e6582', N'Superadmin', N'Superadmin')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'e946a21c-d703-4429-8dbf-d62113840fbd', N'FINANCE_CONTROLLER', N'Finance Controller')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'db0829c6-6788-46d9-a78c-e3638216aa46', N'FINANCE_TREASURY', N'Finance Treasury')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'5f107eae-78f9-43d4-80af-e71cfa678ce6', N'RESERVOIR', N'Analis Reservoir')
GO
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'850e0dfc-186d-4c49-815f-e791837b8668', N'MANAGER_OE', N'Manager OE')
GO
INSERT [dbo].[Users] ([ApplicationId], [UserId], [UserName], [IsAnonymous], [LastActivityDate]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'1ae7c7bb-19f5-42d3-a212-0dc0b2222658', N'mhilman.fadli', 0, CAST(N'2018-12-04 07:39:25.750' AS DateTime))
GO
INSERT [dbo].[Users] ([ApplicationId], [UserId], [UserName], [IsAnonymous], [LastActivityDate]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'8b5029cc-9530-40e4-97da-2e7252b4649d', N'mk.myudistiro', 0, CAST(N'2018-12-07 09:29:32.660' AS DateTime))
GO
INSERT [dbo].[Users] ([ApplicationId], [UserId], [UserName], [IsAnonymous], [LastActivityDate]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'0ff6ac18-b7cb-4d41-8c42-5ef5283b1f2c', N'superadmin', 0, CAST(N'2018-12-07 03:02:27.187' AS DateTime))
GO
INSERT [dbo].[Users] ([ApplicationId], [UserId], [UserName], [IsAnonymous], [LastActivityDate]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'97ff22da-fd01-4e2c-9658-74f921272246', N'admin', 0, CAST(N'2018-04-11 18:12:29.530' AS DateTime))
GO
INSERT [dbo].[Users] ([ApplicationId], [UserId], [UserName], [IsAnonymous], [LastActivityDate]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'41e7d879-79a2-44da-9105-92acf0ac3062', N'tesha', 0, CAST(N'2018-11-09 08:30:45.967' AS DateTime))
GO
INSERT [dbo].[Users] ([ApplicationId], [UserId], [UserName], [IsAnonymous], [LastActivityDate]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'638d476d-b95a-45bb-a356-982c85b8a495', N'budyi.permono', 0, CAST(N'2018-12-07 09:20:09.257' AS DateTime))
GO
INSERT [dbo].[Users] ([ApplicationId], [UserId], [UserName], [IsAnonymous], [LastActivityDate]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'5dcfe3ed-1d86-4117-ad74-c14f82d30d7e', N'reza.rahman.pge', 0, CAST(N'2018-12-07 03:29:42.703' AS DateTime))
GO
INSERT [dbo].[Users] ([ApplicationId], [UserId], [UserName], [IsAnonymous], [LastActivityDate]) VALUES (N'0ca90253-4739-439e-a748-e57b4ddebfe5', N'87615a26-c4a1-4891-8558-ecd7da792e7f', N'wanda.kurniawan', 0, CAST(N'2018-12-07 03:28:23.593' AS DateTime))
GO
INSERT [dbo].[UsersInRoles] ([UserId], [RoleId]) VALUES (N'8b5029cc-9530-40e4-97da-2e7252b4649d', N'e946a21c-d703-4429-8dbf-d62113840fbd')
GO
INSERT [dbo].[UsersInRoles] ([UserId], [RoleId]) VALUES (N'8b5029cc-9530-40e4-97da-2e7252b4649d', N'db0829c6-6788-46d9-a78c-e3638216aa46')
GO
INSERT [dbo].[UsersInRoles] ([UserId], [RoleId]) VALUES (N'0ff6ac18-b7cb-4d41-8c42-5ef5283b1f2c', N'f32b008e-cc0f-4494-b39d-c731e96e6582')
GO
INSERT [dbo].[UsersInRoles] ([UserId], [RoleId]) VALUES (N'41e7d879-79a2-44da-9105-92acf0ac3062', N'f32b008e-cc0f-4494-b39d-c731e96e6582')
GO
INSERT [dbo].[UsersInRoles] ([UserId], [RoleId]) VALUES (N'638d476d-b95a-45bb-a356-982c85b8a495', N'8b269900-4d21-4b59-8a5b-2855b56e5bd7')
GO
INSERT [dbo].[UsersInRoles] ([UserId], [RoleId]) VALUES (N'5dcfe3ed-1d86-4117-ad74-c14f82d30d7e', N'a9689c4b-a973-4b4b-b773-243340da98cc')
GO
INSERT [dbo].[UsersInRoles] ([UserId], [RoleId]) VALUES (N'87615a26-c4a1-4891-8558-ecd7da792e7f', N'e9b1c009-0197-4f88-857d-6dc14db7843a')
GO
INSERT [dbo].[UsersInRoles] ([UserId], [RoleId]) VALUES (N'87615a26-c4a1-4891-8558-ecd7da792e7f', N'c71497bb-6c8d-4c94-912c-987f9c2df209')
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_UserName]    Script Date: 07/01/2019 03:47:01 ******/
CREATE NONCLUSTERED INDEX [IDX_UserName] ON [dbo].[Users]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionsInModules]  WITH CHECK ADD  CONSTRAINT [ActionsInModulesActions] FOREIGN KEY([ActionId])
REFERENCES [dbo].[Actions] ([ActionId])
GO
ALTER TABLE [dbo].[ActionsInModules] CHECK CONSTRAINT [ActionsInModulesActions]
GO
ALTER TABLE [dbo].[ActionsInModules]  WITH CHECK ADD  CONSTRAINT [ActionsInModulesModules] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Modules] ([ModuleId])
GO
ALTER TABLE [dbo].[ActionsInModules] CHECK CONSTRAINT [ActionsInModulesModules]
GO
ALTER TABLE [dbo].[ActionsInModulesChosen]  WITH CHECK ADD  CONSTRAINT [ActionsInModulesChosenActions] FOREIGN KEY([ActionId])
REFERENCES [dbo].[Actions] ([ActionId])
GO
ALTER TABLE [dbo].[ActionsInModulesChosen] CHECK CONSTRAINT [ActionsInModulesChosenActions]
GO
ALTER TABLE [dbo].[ActionsInModulesChosen]  WITH CHECK ADD  CONSTRAINT [ActionsInModulesChosenModulesInRoles] FOREIGN KEY([ModuleInRoleId])
REFERENCES [dbo].[ModulesInRoles] ([Id])
GO
ALTER TABLE [dbo].[ActionsInModulesChosen] CHECK CONSTRAINT [ActionsInModulesChosenModulesInRoles]
GO
ALTER TABLE [dbo].[Memberships]  WITH CHECK ADD  CONSTRAINT [FK_Memberships_Applications] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Memberships] CHECK CONSTRAINT [FK_Memberships_Applications]
GO
ALTER TABLE [dbo].[Memberships]  WITH CHECK ADD  CONSTRAINT [FK_Memberships_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Memberships] CHECK CONSTRAINT [FK_Memberships_Users]
GO
ALTER TABLE [dbo].[Modules]  WITH CHECK ADD  CONSTRAINT [FK_Modules_Modules1] FOREIGN KEY([ParentModuleId])
REFERENCES [dbo].[Modules] ([ModuleId])
GO
ALTER TABLE [dbo].[Modules] CHECK CONSTRAINT [FK_Modules_Modules1]
GO
ALTER TABLE [dbo].[ModulesInRoles]  WITH CHECK ADD  CONSTRAINT [ModulesInRolesModules] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Modules] ([ModuleId])
GO
ALTER TABLE [dbo].[ModulesInRoles] CHECK CONSTRAINT [ModulesInRolesModules]
GO
ALTER TABLE [dbo].[ModulesInRoles]  WITH CHECK ADD  CONSTRAINT [ModulesInRolesRoles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[ModulesInRoles] CHECK CONSTRAINT [ModulesInRolesRoles]
GO
ALTER TABLE [dbo].[Profiles]  WITH CHECK ADD  CONSTRAINT [UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Profiles] CHECK CONSTRAINT [UserProfile]
GO
ALTER TABLE [dbo].[Roles]  WITH CHECK ADD  CONSTRAINT [RoleApplication] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Roles] CHECK CONSTRAINT [RoleApplication]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [UserApplication] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [UserApplication]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [UsersInRoleRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [UsersInRoleRole]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [UsersInRoleUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [UsersInRoleUser]
GO
USE [master]
GO
ALTER DATABASE [PGE.IPGE.UM] SET  READ_WRITE 
GO
