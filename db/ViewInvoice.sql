USE [PGE.IPGE.APP]
GO
/****** Object:  View [dbo].[ViewInvoice]    Script Date: 27/05/2019 09:24:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewInvoice]
AS
SELECT        a.Id AS AreaId, a.Name AS AreaName, p.ShipToPartyName, p.Name AS PowerPlantName, p.Id AS PowerPlantId, p.Material, t.Periode AS TaskPeriode, t.Status AS TaskStatus, pl.EnergyNett, pl.EnergyGross, pl.SteamUtilization, 
                         s.NetPrice, s.NetValue, s.Periode AS SalesPeriode, i.Id AS InvoiceId, w.Code AS WorkFlowCode, p.SOType, s.Quantity
FROM            dbo.Area AS a LEFT OUTER JOIN
                         dbo.PowerPlant AS p ON a.Id = p.AreaId LEFT OUTER JOIN
                         dbo.TaskReport AS t ON t.AreaId = a.Id AND t.IsDeleted = 0 LEFT OUTER JOIN
                         dbo.Pltp AS pl ON pl.TaskReportId = t.Id AND pl.IsDeleted = 0 LEFT OUTER JOIN
                         dbo.SalesOrder AS s ON s.PowerPlantId = pl.PowerPlantId AND s.WorkflowId IN (6, 10, 18, 1023, 1029) AND YEAR(s.Periode) = YEAR(t.Periode) AND MONTH(s.Periode) = MONTH(t.Periode) LEFT OUTER JOIN
                         dbo.Invoice AS i ON i.SalesOrderId = s.Id LEFT OUTER JOIN
                         dbo.Workflow AS w ON i.WorkflowId = w.Id

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 282
               Bottom = 136
               Right = 493
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 531
               Bottom = 136
               Right = 737
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pl"
            Begin Extent = 
               Top = 6
               Left = 775
               Bottom = 136
               Right = 982
            End
            DisplayFlags = 280
            TopColumn = 23
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 1020
               Bottom = 136
               Right = 1226
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "i"
            Begin Extent = 
               Top = 6
               Left = 1264
               Bottom = 136
               Right = 1470
            End
            DisplayFlags = 280
            TopColumn = 16
         End
         Begin Table = "w"
            Begin Extent = 
               Top = 6
               Left = 1508
               Bottom = 136
               Right = 1739
            End
            DisplayFlags = 280
            TopColumn = 1
         End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewInvoice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewInvoice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewInvoice'
GO
