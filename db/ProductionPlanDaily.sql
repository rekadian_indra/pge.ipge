USE [PGE.IPGE.APP]
GO
/****** Object:  Table [dbo].[ProductionPlanDaily]    Script Date: 07-Jan-19 11:33:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductionPlanDaily](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductionPlanDetailId] [int] NOT NULL,
	[Day] [int] NOT NULL,
	[Value] [float] NOT NULL,
 CONSTRAINT [PK_ProductionPlanDaily] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[ProductionPlanDaily]  WITH CHECK ADD  CONSTRAINT [FK_ProductionPlanDaily_ProductionPlanDetail] FOREIGN KEY([ProductionPlanDetailId])
REFERENCES [dbo].[ProductionPlanDetail] ([Id])
GO
ALTER TABLE [dbo].[ProductionPlanDaily] CHECK CONSTRAINT [FK_ProductionPlanDaily_ProductionPlanDetail]
GO
